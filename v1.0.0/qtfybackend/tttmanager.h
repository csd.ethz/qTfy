/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TTTMANAGER_H
#define TTTMANAGER_H


// Project includes
#include "qtfydata/experiment.h"
//#include "qtfydata/trackpoint.h"
#include "tttpositionmanager.h"
//#include "exception.h"

// Qt includes
#include <QString>
#include <QPointF>
#include <QWidget>
#include <QHash>
#include <QDomDocument>




///how many pictures should be loaded at the begin out of the selected pictures?
const int LOAD_ALL_SELECTED_PICTURES = -1; //== all selected

///the default output name of the TAT information file
///BS 2010/02/25 new: the filename is no longer just TATexp.xml, but contains the experiment name as well
///                   example: 090901PH2_TATexp.xml
const QString DEFAULT_TAT_FILENAME = "TATexp.xml";



/**
@author Bernhard

This class handles the core machinery of displaying/tracking and is global for all positions.
It contains the current timepoint, display settings, file arrays, tree, track, ...
All timepoint changing events are managed here, thus no recursive signal problems can occur.
(for this still necessary: semi-static inst and position identifier as private attribute)
TTTManager also contains the central menu bar, which is inserted into both frmTracking and all frmMovies.

Additionally:
For each position there is one TTTPositionManager.
*/
class TTTManager : public QObject {

	Q_OBJECT

public:

	/**
	* returns the static instance of this class; if it does not yet exist, it is created and initialized
	* @return the (unique) TTTManager instance
	*/
	static TTTManager& getInst();

	~TTTManager();
	
	// Delete TTTManager instance
	void destroy();

	/**
	 * sets whether the new coordinate scheme should be used (experiment relative, not position dependent).
	 * this is only possible if the tat-xml file is present!
	 * @param _useNewPositions true, if the tat file was there; must be false otherwise
	 */
	void setUSE_NEW_POSITIONS(bool _useNewPositions) {
		use_NEW_POSITIONS = _useNewPositions;
	}
	
	/**
	 * @return whether the new experiment relative coordinate scheme is used
	 */
	bool USE_NEW_POSITIONS() const {
		return use_NEW_POSITIONS;
	}
	
	/**
	* adds an experiment to the experiment hash
	* @param _key the unique experiment key (experiment base name)
	* @param _exp pointer to a Experiment instance
	*/
	void addExperiment(const QString &_key, Experiment *_exp);
	
	/**
	* returns the Experiment instance for the desired experiment base name (&_key)
	* @param _key the unique experiment key (experiment base name)
	* @return a pointer to a Experiment instance
	*/
	Experiment* getExperiment(const QString &_key) const;
	
	/**
	* @deprecated
	*
	* adds a position manager to the current dictionary
	* @param _key the unique position key
	* @param _pm the position manager
	*/
	void addPositionManager (const QString &_key, TTTPositionManager *_pm) 
	{
		if(m_positionManagers.contains(_key))
			m_positionManagers.remove(_key);
		m_positionManagers.insert(_key, _pm);
	}

	/**
	* @deprecated 
	*
	* returns the PositionManager instance for the desired position
	* NOTE: very important: must have constant access time
	* @param _key the unique position key
	* @return a pointer to a PositionManager instance
	*/
	TTTPositionManager* getPositionManager (const QString &_key) const 
	{
		return m_positionManagers.value(_key, nullptr);
	}


	/**
	* returns all position managers sorted alphabetically according to their index
	* @return a pointer to a QPtrVector instance; the values can be obtained via an iterator and are passed in the sort order
	*/
	std::vector<TTTPositionManager*> getAllPositionManagersSorted() const;

	/**
	* sets whether the current folders do not contain pictures at all
	* Necessary to be able to load ttt files without pics
	* @param _loadOnlyTTTFiles true, if only ttt files (without pictures) should be loadable
	*/
	void setLoadTTTFilesOnly (bool _loadOnlyTTTFiles) {
		loadOnlyTTTFiles = _loadOnlyTTTFiles;
	}

	/**
	* @return whether the current folders do not contain pictures at all (but ttt files should be loadable in spite)
	*/
	bool loadTTTFilesOnly() const {
		return loadOnlyTTTFiles;
	}

	/**
	* sets the TTT files folder 
	* @param _tttfilesfolder the path to the ttt files folder
	*/
	void setTTTFilesFolder (const QString &_tttfilesfolder)
	{
		TTTfilesFolderPath = _tttfilesfolder;
	}

	/**
	* @return the path to the ttt files folder
	*/
	const QString getTTTFilesFolderPath()
	{
		if (TTTfilesFolderPath.right (1) == "/")
			return TTTfilesFolderPath;
		else
			return TTTfilesFolderPath + "/";
	}

	/**
	* sets the TTT files folder 
	* @param _tttfilesfolder the path to the ttt files folder
	*/
	void setExperimentPath (const QString &_experimentfolderpath)
	{
		ExperimentPath = _experimentfolderpath;
	}

	/**
	* @return the path to the ttt files folder
	*/
	const QString getExperimentPath()
	{
		if (ExperimentPath.right (1) == "/")
			return ExperimentPath;
		else
			return ExperimentPath + "/";
	}



	/**
	 * quits TTT, optionally displaying a QMessageBox with the provided message before closing
	 * @param _message the message to be displayed
	 * @param _returnCode the return code to be returned by main(): 0 is success, !=0 is erroneous
	 */
	void quitProgram (const QString &_message = "", int _returnCode = 0);


	/**
	* sets the microscope ocular factor
	* @param _of the ocular factor (5, 10 or 20)
	*/
	void setOcularFactor (int _of) {
		ocularFactor = _of;
	}

	/**
	* sets the microscope TV adapter factor
	* @param _tvf the TV adapter magnifying factor (e.g. 0.63 or 1.0)
	*/
	void setTVFactor (float _tvf) {
		tvFactor = _tvf;
	}

	/**
	* sets both microscope factors at once
	* @param _ocularFactor the ocular factor (5, 10 or 20)
	* @param _tvFactor the TV adapter magnifying factor (either 0.63 or 1.0)
	*/
	void setDisplayFactors (int _ocularFactor, float _tvFactor) {
		ocularFactor = _ocularFactor;
		tvFactor = _tvFactor;
	}

	/**
	* @return the ocular factor (5, 10 or 20)
	*/
	int getOcularFactor() const {
		return ocularFactor;
	}

	/**
	* @return the TV adapter magnifying factor (either 0.63 or 1.0)
	*/
	float getTVFactor() const {
		return tvFactor;
	}

	/**
	* sets whether the coordinate system is inverted
	* @param _inverted true, if the coordinate system is inverted (positive axes are up and left); false, if it is normal (positive axes are down and right)
	*/
	void setInvertedCoordinateSystem (bool _inverted) {
		coordinateSystemInverted = _inverted;
	}

	/**
	* @return sets whether the coordinate system is inverted (= true, if the coordinate system is inverted (positive axes are up and left); false, if it is normal (positive axes are down and right))
	*/
	bool coordinateSystemIsInverted() const {
		return coordinateSystemInverted;
	}

	/**
	* sets the filename of the experiment comment
	* @param _commentFN the filename of the file that contains the comment
	*/
	void setExperimentCommentFilename (const QString& _commentFN) {
		experimentCommentFilename = _commentFN;
	}

	/**
	* @return the filename of the experiment comment
	*/
	QString getExperimentCommentFilename() const {
		return experimentCommentFilename;
	}

	void setExperimentBasename(const QString& theValue)	{	
		experimentBasename = theValue;
	}

	QString getExperimentBasename() const {	
		return experimentBasename;
	}

	void setExperimentNamePosMarker(const QString& theValue)	{	
		experimentName_posMarker = theValue;
	}

	QString getExperimentNamePosMarker() const {	
		return experimentName_posMarker;
	}



private:

	//the static global instance (unique in the complete program)
	static TTTManager *inst;

	// Private Constructors to prevent instatiation outside of getInst()
	TTTManager();	
	//~TTTManager();

	// Private Copy Constructor to prevent copying
	TTTManager(const TTTManager &);
	TTTManager& operator=(const TTTManager &);

	//global flag:
	///whether the new experiment relative coordinates should be used (only if the tat-xml file is present) (=> true)
	///or not (=> false)
	///set in TTTMainWindow when the tat-xml file is read (if available => true)
	bool use_NEW_POSITIONS;

	///contains the basename of the experiment
	///e.g. "051104-2"
	QString experimentBasename;


	QHash<QString, TTTPositionManager*> m_positionManagers;


	QHash<QString, Experiment*> experiments;

	///the current position key
	int m_currentPositionKey;

	//@deprecated has no special meaning anymore
	//
	///the base position key (the key of the position in which tracking was started)
	///necessary for frmTracking, Tree, TreeDisplay, ...
	int m_basePositionKey;


	///the path where the ttt files can be found
	QString TTTfilesFolderPath;


	// the path where the experiment images can be found
	QString ExperimentPath;

	/**
	 * experiment name for correct index in TATXMLParser::startElement(...)
	 * basicaly the index for the position
	 */
	QString experimentName_posMarker;


	///contains the factor of the microscope tv adapter (either 0.63 or 1.0)
	float tvFactor;

	///contains the ocular factor of the microscope (5, 10 or 20)
	int ocularFactor;

	///whether the coordinate system is inverted 
	///true => the coordinate system is inverted (positive axes are up and left)
	///false => normal (positive axes are down and right)
	bool coordinateSystemInverted;

	///contains the filename of the experiment comment (is displayed in the statistics window)
	QString experimentCommentFilename;

	///whether the current folders do not contain pictures at all
	bool loadOnlyTTTFiles;


};

#endif
