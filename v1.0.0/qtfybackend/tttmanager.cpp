/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "tttmanager.h"


// PROJECT
#include "tools.h"
// #include "picturearray.h"

// QT
#include <QMessageBox>


// STL
#include <algorithm>

TTTManager* TTTManager::inst (0);

TTTManager::TTTManager() 
{
	//initialize variables
	//currentTracks = 0;
	//currentTrackPoints = 0;
	//trackingCellProperties.Set_FreeFloating = true;
	//trackingCellProperties.Set_NonAdherent = true;
	//trackingCellProperties.Set_Tissues = true;
	//trackingCellProperties.Set_Wavelengths = true;
	//m_currentPositionKey = -1;
	//m_basePositionKey = -1;
	//tree = 0;
	//currentTrack = 0;
	//NASDrive = "";
	//Tracking = false;
	//backwardTrackingMode = false;
	//TrackingStartFirstTimepoint = 0;
	//TrackingEndFirstTimepoint = 0;
	//stopReason = TS_NONE;
	use_NEW_POSITIONS = false;
	tvFactor = 1.0f;
	ocularFactor = 5;
	//miningResults = 0;
	//externalTracks = 0;
	//statisticsMode =  false;
	//keyboardGrabber = 0;
	//m_framesToSkipWhenTracking = 0;

	//// Differentiation: none
	//trackingCellProperties.tissueType = TT_NONE;
	//trackingCellProperties.cellGeneralType = CGT_NONE;
	//trackingCellProperties.cellLineage = 0;

	//// Adherence: adherent
	//trackingCellProperties.NonAdherent = false;
	//trackingCellProperties.FreeFloating = false;

	//// Endomitosis: off
	//trackingCellProperties.EndoMitosis = false;
}

TTTManager::~TTTManager()
{
	// Delete position managers
	for(auto it = m_positionManagers.begin(); it != m_positionManagers.end(); ++it)
		delete (*it);
}

TTTManager& TTTManager::getInst()
{
	if (! inst) {
		inst = new TTTManager();
		// inst->createForms();
		// inst->connectForms();
	}
	
	return *inst;
}

void TTTManager::addExperiment(const QString &_key, Experiment *_exp)
{
        //if there is already an experiment with this key, delete this first
        experiments.remove(_key);

        if ( ! _exp )
                return;

        experiments.insert(_key, _exp);
}


Experiment* TTTManager::getExperiment(const QString &_key) const
{
        return experiments[_key];
}


//
//std::vector<TTTPositionManager*> TTTManager::getAllPositionManagersSorted() const
//{
//	// Result
//	std::vector<TTTPositionManager*> result(m_positionManagers.size());
//	
//	// Copy pointers
//	int i = 0;
//	for(auto it = m_positionManagers.constBegin(); it != m_positionManagers.constEnd(); ++it)
//		result[i++] = (*it);
//
//	// Sort
//	std::sort(result.begin(), result.end(), [](TTTPositionManager* a, TTTPositionManager* b) { return a->positionInformation.getIndex() < b->positionInformation.getIndex(); });
//
//	// Done
//	return result;
//}




void TTTManager::quitProgram (const QString &_message, int _returnCode)
{
	if (! _message.isEmpty()) {
		QMessageBox::critical (0, "Fatal error", _message);
	}
	
	//all qApp methods do not work!!!??
/*	qApp->exit (_returnCode);
	qApp->quit();
	qApp->processEvents();*/
	
	exit (_returnCode);
}


void TTTManager::destroy()
{
	if(inst) {
		delete inst;
		inst = 0;
	}
}













