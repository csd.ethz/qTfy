/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef itrack_h__
#define itrack_h__

// QT
#include <QMap>
#include <QString>

/**
 * Global track stop reasons enumeration
 */
enum TrackStopReason {	
	TS_NONE = 0,
	TS_DIVISION = 1,
	TS_APOPTOSIS = 2,
	TS_LOST = 3
};


/**
 * Global CellTypes
 * 
 * Necessary for selecting cells of certain types both in the cell editor and the position statistics
 */
enum CellType {
	CT_NOCELLTYPE = 0L, 
	CT_ALLTYPES = 1L, 
	CT_DIVIDINGTYPE = 2L, 
	CT_APOPTOTICTYPE = 4L, 
	CT_LOSTTYPE = 8L, 
	CT_INTERRUPTEDTYPE = 16L, 
	CT_DIVIDINGCHILDRENTYPE = 32L,
    CT_DIFFERENTCHILDRENSTOPREASONTYPE = 64L, 
	CT_DIFFERENTSISTERSTOPREASONTYPE = 128L, 
	CT_NOLOSTCHILDRENTYPE = 256L
};



// Forward declarations
class ITrackPoint;
class ITree;
class QVariant;


/**
 * Interface for classes representing tracks (or cells) of a cell  lineage tree
 */
class ITrack {

public:

	/**
	 * @return child 1 or 0 if not available (track with 2*number of this track)
	 */
	virtual ITrack *getChild1() const = 0;

	/**
	 * @return child 2 or 0 if not available (track with 2*number+1 of this track)
	 */
	virtual ITrack *getChild2() const = 0;

	/**
	 * @return mother track or 0 if not available
	 */
	virtual ITrack *getMother() const = 0;


	/**
	 * @return true if track has children
	 */
	virtual bool hasChildren() const = 0;

	/**
	 * @return first timepoint of this track or -1 if not available
	 */
	virtual int getFirstTimePoint() const = 0;

	/**
	 * @return last timepoint of this track or -1 if this track contains no trackpoints or if not available
	 */
	virtual int getLastTimePoint() const = 0;

	/**
	 * get trackpoint at a certain timepoint with a certain wavelength
	 * @param _timePoint the timepoint, does not have to be valid
	 * @return trackpoint at _timePoint and _waveLength or 0 this track contains no such trackpoint
	*/
	virtual ITrackPoint* getTrackPointByTimePoint(int _timePoint) const = 0;

	/**
	 * get all trackpoints between startTP and endTP
	 * @param _startTP the starting timepoint, first available assumed if not given
	 * @param _endTP the ending timepoint, last available assumed if not given
	 * @return QMap<int, ITrackPoint*> all trackpoints in range sorted by the trackpoint number
	 */
	virtual QMap<int, ITrackPoint*> getTrackPointsRange(int _startTP = -1, int _endTP = -1) const = 0;

	/**
	 * @return the track number of this track or -1 if not available
	 */
	virtual int getTrackNumber() const = 0;

	/**
	 * @return stop reason for this track
	 */
	virtual TrackStopReason getStopReason() const = 0;

	/**
	 * @return tree of this track or 0 if not available
	 */
	virtual ITree* getITree() const = 0;

	/**
	 * @return number of seconds between the first trackpoint of this track and the given trackpoint
	 */
	virtual int getSecondsForTimePoint(int _timePoint) const = 0;

	/**
	 * @return number of seconds between the very first timePoint and the given one.
	 */
	virtual int getAbsoluteSecondsForTP(int timePoint) const = 0;

	/**
	 * Access method to many (all?) track properties (=cell properties)
	 * Because of the QVariant return type many datatypes are supported.
	 * @parm _propName the name of the property
	 * @return the value of the requested property
	 */
	virtual QVariant getTrackProperty(QString _propName) const = 0;

	/**
	 * Destructor, necessary to allow proper destruction of derived classes 
	 */
	virtual ~ITrack() {}

public:
	/**
	 * Property names for getTrackProperty method (initialization in itrack.cpp)
	 */
	static const QString TRACK_LIFETIME;	// The lifetime in seconds
	static const QString TRACK_AVG_SPEED;
	static const QString TRACK_DISTANCE;
	static const QString TRACK_TYPE;		// The celltype of this track/cell
	static const QString TRACK_GENERATION;	// The (zero based) generation this track belongs to
	static const QString TRACK_PREINCUBATION_TIME;	// The preincubation time in seconds before the experimen

};



#endif // itrack_h__
