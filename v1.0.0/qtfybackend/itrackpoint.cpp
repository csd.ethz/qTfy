/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "itrackpoint.h"

#include "qtfydata/trackpoint.h"

const QString ITrackPoint::TP_FREEFLOATING = OPT_FREEFLOATING;
const QString ITrackPoint::TP_SEMIADHERENCE = OPT_SEMIADHERENCE;
const QString ITrackPoint::TP_ADHERENCE = OPT_ADHERENCE;

const QString ITrackPoint::TP_X = OPT_X;
const QString ITrackPoint::TP_Y = OPT_Y;
const QString ITrackPoint::TP_TIMEPOINT = OPT_TIMEPOINT;
