/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "fileinfoarray.h"

#include <QMessageBox>


FileInfoArray::~FileInfoArray()
{
	// Delete entries
	for(QHash<PictureIndex, FileInfo*>::iterator it = infoArray.begin(); it != infoArray.end(); ++it) {
		delete it.value();
	}
//        for (int i = 0; i < (int)infoArray.size(); i++)
//                if (infoArray.find (i))
//                        delete infoArray.find (i);
}

//void FileInfoArray::resize (int _newSize)
//{
//        infoArray.resize (_newSize);
//}
//
bool FileInfoArray::exists (int _timePoint, int _zIndex, int _waveLength) const
{
	// Determine wl and z-index ranges to look at
	int startWl, stopWl, startZ, stopZ;
	indexToRange(_zIndex, _waveLength, startZ, stopZ, startWl, stopWl);

	// Look if we can find anything
	for(int z = startZ; z <= stopZ; ++z) {
		for(int wl = startWl; wl <= stopWl; ++wl) {
			if(infoArray.contains(PictureIndex(_timePoint, z, wl)))
				return true;
		}
	}

	// Nothing found
	return false;

	/*
	int index = 0;

	if (_waveLength == -1) {
		for (int i = 0; i <= MAX_WAVE_LENGTH; i++) {
			index = calcIndex (_timePoint, i);
			if (infoArray.contains (index))
				break;
		}
	}
	else {
		index = calcIndex (_timePoint, _waveLength);
	}

	if (infoArray.contains (index))
		return index;
	else
		return 0;
		*/
}

const FileInfo* FileInfoArray::find (int _timePoint, int _zIndex, int _waveLength) const
{
	// Determine wl and z-index ranges to look at
	int startWl, stopWl, startZ, stopZ;
	indexToRange(_zIndex, _waveLength, startZ, stopZ, startWl, stopWl);

	// Look if we can find anything
	for(int z = startZ; z <= stopZ; ++z) {
		for(int wl = startWl; wl <= stopWl; ++wl) {
			if(infoArray.contains(PictureIndex(_timePoint, z, wl)))
				return infoArray.value(PictureIndex(_timePoint, z, wl));
		}
	}

	// Nothing found
	return 0;
	/*
	int index = calcIndex (_timePoint, _waveLength);

	if (_waveLength == -1) {
		for (int i = 0; i <= MAX_WAVE_LENGTH; i++) {
			index = calcIndex (_timePoint, i);
			if (infoArray.contains (index))
				break;
		}
	}

	if (infoArray.contains (index))
		return *(infoArray [index].data());
	else
		return dummy;
		*/
}

bool FileInfoArray::insert (const QString& _fileName, int _timePoint, int _zIndex, int _waveLength, const QDateTime& _creationTime, bool &_firstTimeSet)
{

	// qDebug() << "Timepoint: " << _timePoint <<"Creation time: " << _creationTime.time().toString() << ", First time: " << firstTime.time().toString();
	// Set first time point
	_firstTimeSet = false;
	if ((firstTime.isNull()) || (_creationTime.secsTo (firstTime) > 0)) {
		//the currently provided creation time is before the earliest creation time (until now) -> update it
		firstTime = _creationTime;
		_firstTimeSet = true;
	}

	// Create FileInfo object
	// create tmp in the stack // Laura 
	FileInfo* tmp = new FileInfo (_fileName, _timePoint, _waveLength, _creationTime);
	infoArray.insert(PictureIndex(_timePoint, _zIndex, _waveLength), tmp);

	// Delta times to first time point
	int dist = firstTime.secsTo (_creationTime);
	times.insert(PictureIndex(_timePoint, _zIndex, _waveLength), dist);

	
	return true;	//whether the insertion was successful
}

QDateTime FileInfoArray::getRealTime (int _timePoint, int _zIndex, int _waveLength, bool _roundUp) const
{
	// Determine wl and z-index ranges to look at
	int startWl, stopWl, startZ, stopZ;
	indexToRange(_zIndex, _waveLength, startZ, stopZ, startWl, stopWl);
	int startTp = _timePoint,
		stopTp = _timePoint;
	if(_roundUp)
		stopTp += 10;

	// Look if we can find anything
	for(int tp = startTp; tp <= stopTp; ++tp) {
		for(int z = startZ; z <= stopZ; ++z) {
			for(int wl = startWl; wl <= stopWl; ++wl) {
				FileInfo* fi = infoArray.value(PictureIndex(tp, z, wl));
				if(fi)
					return fi->CreationTime;
			}
		}
	}

	return QDateTime();

	/*
	int index = exists (_timePoint, _waveLength);
	if (index > 0)
		return infoArray [index]->CreationTime;
	else {
		if (_roundUp) {
			///@todo BS: use the last existing timepoint; currently not available here
			for (int i = _timePoint; i < _timePoint + 10; ++i) {
				index = exists (i, -1);
				if (index > 0)
					return infoArray [i]->CreationTime;
			}
		}
		else
			return QDateTime();
	}

	return QDateTime();
	*/
}

int FileInfoArray::calcSeconds (int _timePoint1, int _timePoint2, bool _round, int _zIndex1, int _zIndex2, int _waveLength1, int _waveLength2) const
{
	if (_timePoint2 > 1e5)	//note: timePoint is NOT seconds!!
		return -1;

	if(_zIndex1 == -1)
		_zIndex1 = 1;
	if(_zIndex2 == -1)
		_zIndex2 = 1;
	if(_waveLength1 == -1)
		_waveLength1 = 0;
	if(_waveLength2 == -1)
		_waveLength2 = 0;

	//BS 2010/03/22 patch
	//instead of returning 0 when timepoint2 is greater than the last timepoint, it is assumed that the last timepoint is sufficient (thus providing more consistent results)
	if (_timePoint2 > lastTimepoint)
		_timePoint2 = lastTimepoint;

	//int dist = getRealTime (_timePoint1, _waveLength1).secsTo (getRealTime (_timePoint2, _waveLength2));
	/*
	int index1 = calcIndex (_timePoint1, _waveLength1);
	int index2 = calcIndex (_timePoint2, _waveLength2);
	*/
	int dist = -1;

	// Timepoint1 must exist
	int time1 = times.value(PictureIndex(_timePoint1, _zIndex1, _waveLength1), -1);
	if(time1 < 0)
		return 0;

	// Check if timepoint2 exists
	int time2 = times.value(PictureIndex(_timePoint2, _zIndex2, _waveLength2), -1);
	if(time2 != -1) {
		//dist = times.value(PictureIndex(_timePoint2, _zIndex2, _waveLength2)) - times.value(PictureIndex(_timePoint1, _zIndex1, _waveLength1));
		dist = time2 - time1;
	}
	else {
		// Timepoint2 does not exist
		if (_round /*&& (dist < 0)*/) {
			//the timepoint does not exist
			//-> take the next lower timepoint and add 1 second

			//if (_timePoint2 > _t
			dist = -1;
			for (int i = _timePoint2 - 1; i > _timePoint1; --i)
			{
				time2 = times.value(PictureIndex(i, _zIndex2, _waveLength2), -1);
				if (time2 != -1) {
					dist = time2 - time1;

					////dist = getRealTime (_timePoint1).secsTo (getRealTime (i)) + 1;
					//index2 = calcIndex (i, _waveLength2);
					//if(times.contains(index2) && times.contains(index1))
					//	//if ((index2 >= 0) & (index2 < (int)times.size()))
					//	dist = times [index2] - times [index1];

					break;
				}
			}
		}
	}

	//note: if dist is still -1, something must have went wrong before
	return dist;
}

int FileInfoArray::calcTimePoint (int _base, long int _seconds, bool _lower) const
{
	int tmp = _base;
	long int secs = 0;

	//note: useless upper limit; just not to have an endless loop
	//note: i runs across timepoints, not seconds
	for (int i = 0; i < (int)infoArray.size(); i++) {

		if (exists (_base + i, 1, -1)) {
			secs = calcSeconds (_base, _base + i);

			if (! _lower) {
				if (secs >= _seconds)
					return _base + i;
			}
			else {
				if (secs >= _seconds)
					//return last timepoint, whose difference to _base is just below seconds
					return tmp;

				if (secs > -1)
					tmp = _base + i;
			}
		}
	}

	return -1;		//no timepoint found
}

int FileInfoArray::findTimePointByAbsTime (int _startTp, int _endTp, int _zIndex, int _wavelength, const QDateTime &_absTime) const
{
	for (int tp = _startTp; tp <= _endTp; tp++) {
		FileInfo* fi = infoArray.value(PictureIndex(tp, _zIndex, _wavelength));
		if(fi && fi->CreationTime == _absTime)
			return tp;

		/*
		int i = calcIndex (tp, _wavelength);

		if (i < (int)infoArray.size() && infoArray.contains (i)) {
			if (infoArray [i]->CreationTime == _absTime)
				return tp;
		}
		*/
	}

	return -1;
}

int FileInfoArray::getExistingTimepointsBetween (int _startTp, int _endTp) const
{
	int count = 0;

	for (int tp = _startTp; tp <= _endTp; tp++) {
		if (exists (tp, 1, 0))
			count++;
	}

	return count;
}

void FileInfoArray::setTimepoints (int _ftp, int _ltp)
{
    firstTimepoint = _ftp;
    lastTimepoint = _ltp;
}
