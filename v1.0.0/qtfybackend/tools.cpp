/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


// PROJECT
#include "tools.h"
#include "qtfyio/tttfilehandler.h"
#include "qtfyio/fastdirectorylisting.h"
#include "qtfybackend/tttpositionmanager.h"

// QT
#include <QAbstractButton>
#include <QPushButton>
#include <QMessageBox>
#include <QProcess>


// Other static variables
int Tools::numDigitsForPosIndex = -1;
int Tools::numDigitsForWlIndex = -1;
int Tools::numDigitsForZIndex = -1;


Tools::Tools()
{
}

Tools::~Tools()
{
}

QString Tools::convertIntPositionToString( int _posNumber )
{
	// Determine num digits
	int numDigits = numDigitsForPosIndex;
	if(numDigits == -1)
		numDigits = 4;		// assume 4

	// Convert
	return QString("%1").arg(_posNumber, numDigits, 10, QChar('0'));
}

bool Tools::isValidNumber (double _value) 
{
	if (_value != _value){
		// "NaN";
		return false;
	}
	else if (_value > std::numeric_limits<qreal>::max()){
		// "+Inf";
		return false;
	}
	else if (_value < -std::numeric_limits<qreal>::max()){
		// "-Inf";
		return false;
	}
	else
		return true;

}



//int Tools::floatEquals (float _number1, float _number2, int _places)
//{
//	float nr1 = (float)MathFunctions::Round (_number1, _places);
//	float nr2 = (float)MathFunctions::Round (_number2, _places);
//	
//	float diff = nr1 - nr2;
//	float tolerance = 1.0f / (float)pow (10, _places + 1);
//	
//	if (fabs (diff) <= tolerance)
//		return 0; 		//numbers are equal within the given tolerance
//	else if (diff < 0.0f)
//		return 1;		//number 1 < number 2
//	else
//		return -1;		//number 1 > number 2
//}
//
////double Tools::Round (double _number, int _places)
////{
////    double v[] = { 1, 10, 1e2, 1e3, 1e4, 1e5, 1e6, 1e7, 1e8 };  // possibly enlarge
////    return floor(_number * v[_places] + 0.5) / v[_places];
////}
//
//QString Tools::readTextFile (QString _filename)
//{
//	//std::ifstream loader (_filename, std::ios_base::in);
//	//
//	//if (! loader)
//	//	return QString();			//opening for input failed
//	//
//	//loader.seekg (0, std::ios_base::beg);
//	//
//	//QString result = "";
//	//char zack = 0;
//	//while (! loader.eof()) {
//	//	loader.read (&zack, 1);
//	//	result += zack;
//	//}
//	//
//	//return result;
//
//	// --------- Oliver -----------
//	// reimplemented using qt4 objects for better performance
//
//	// Open file
//	QFile file(_filename);
//	if(!file.open(QFile::ReadOnly))
//		return QString();
//
//	// Create string and reserve enough space for file
//	QString result;
//	qint64 size = file.size();
//	result.reserve(size);
//
//	// Read file
//	QTextStream in(&file);
//	while(1) {
//		char ch;
//		in >> ch;
//
//		if(in.status() != QTextStream::Ok)
//			break;
//
//		result.append(ch);
//	}
//
//	return result;
//}
//
//bool Tools::writeTextFile (const QString &_filename, const QString &_content)
//{
//	if (_filename.isEmpty())
//		return false;
//	
//	QFile file;
//	
//	file.setFileName (_filename);
//	if (! file.open (QIODevice::WriteOnly)) 
//		return false;		//opening the file for output failed
//	
//	//associate the stream with the file
//        QTextStream writer;
//	writer.setDevice (&file);
//	
//	writer << _content;
//	
//	file.close();
//	
//	return true;
//}
//
//int Tools::pow (int _base, int _exp)
//{
//	if (_exp < 0)
//		return 0;
//	
//	int result = 1;
//	for (int i = 1; i <= _exp; i++)
//		result *= _base;
//	
//	return result;
//}
//
QString Tools::getPositionNumberFromString(const QString &_posString)
{
	if (_posString.isEmpty())
		return "";

	if (_posString.indexOf (POSITION_MARKER) < 0)
		return "";

	QString result = _posString.mid (_posString.lastIndexOf (POSITION_MARKER) + POSITION_MARKER.length());

	// 18.02.2011-OH: Support for 3 and 4 digit positions required -> check 4th character
	if(result.length() > 3 && result[3].isDigit())
		result = result.left (4);
	else
		result = result.left (3);

	return result;
}

const QString Tools::getPositionIndex (const QString &_filename)
{
	QFileInfo fileInfo(_filename);
	QString treeName(fileInfo.fileName());
	QString positionIndex = treeName.left(treeName.indexOf("-"));
	return positionIndex;

	/*QString result = getPositionNumberFromString(_filename);
	QString experimentName = _filename.mid(_filename.lastIndexOf("/") + 1);
	experimentName = experimentName.section(POSITION_MARKER, 0, 0);
	result = QString("%1%2%3").arg(experimentName).arg(POSITION_MARKER).arg(result); 	
	return result;*/
}


QString Tools::readTTTFileWithCsvQuantification(const QString& tttFilePath, const TTTPositionManager* tttpm, Tree& outTree, CellLineageTree& outTreeAndQuantification)
{
	QString returnMsg;

	try {
		/**
		 * Open ttt file
		 */
		int trackCount, firstTP, lastTPA;
		// tttreader.readFile (file, tree, trackCount, firstExpTP, lastExpTP, &tttpm->positionInformation, false, true)
		int err = TTTFileHandler::readFile (tttFilePath, &outTree, trackCount, firstTP, lastTPA, &tttpm->positionInformation, false, true);
		if(err != TTT_READING_SUCCESS)
			throw QString("Cannot read .ttt file '%1' - error code: %2").arg(tttFilePath).arg(err);

		/**
		 * Convert Tree to CellLineageTree
		 */

		// Check first if clt is empty
		if(outTreeAndQuantification.getFrameIndexElements().size())
			throw QString("Error in readTTTFileWithCsvQuantification(): outTreeAndQuantification must be empty");

		if(!outTree.convertToCellLineageTree(outTreeAndQuantification))
			throw QString("Could not convert Tree to CellLineageTree.");
		if(outTreeAndQuantification.getFrameIndexElements().size() != 4 || outTreeAndQuantification.getFrameIndexElements()[0] != "TimePoint" || outTreeAndQuantification.getFrameIndexElements()[1] != "FieldOfView")
			throw QString("Unexpected frame index elements.");

		/**
		 * Look for quantification csv files
		 */

		// Extract path and filename without extension
		int sepIndex = QDir::fromNativeSeparators(tttFilePath).lastIndexOf('/');
		QString path, fileName;
		if(sepIndex >= 0) {
			path = tttFilePath.left(sepIndex);
			fileName = tttFilePath.mid(sepIndex+1);
		}
		else {
			path = '.';
			fileName = tttFilePath;
		}
		QString fileNameWithoutExtension = fileName.left(fileName.length() - 4);
		
		// List csv files
		QStringList csvFiles = FastDirectoryListing::listFiles(QDir(path), QStringList() << ".csv", QStringList() << fileNameWithoutExtension, false);

		// Load csv files
		for(auto itCsvFile = csvFiles.begin(); itCsvFile != csvFiles.end(); ++itCsvFile) {

			QString quantName = itCsvFile->mid(fileNameWithoutExtension.length()+1);
			quantName = quantName.left(quantName.length()-4);
			if (!quantName.contains("QTFy"))
				continue;

			// Read data
			QFile f(path + '/' + *itCsvFile);
			if(!f.open(QIODevice::ReadOnly))
				throw QString("Cannot open csv file '%1' - error: %2").arg(*itCsvFile).arg(f.errorString());
			QString csvData = f.readAll();

			// Parse data
			QVector<QVector<double>> rows;
			QVector<QString> columnNames;
			QVector<QString> columnsToFind;
			columnsToFind.push_back("TrackNumber");
			columnsToFind.push_back("TimePoint");
			columnsToFind.push_back("Position");
			QVector<int> columnsToFindIndexes;
			err = parseCsvDataDouble(csvData, ",", rows, &columnNames, &columnsToFind, &columnsToFindIndexes);
			if(err)
				throw QString("Cannot parse file '%1' - error code: %2").arg(*itCsvFile).arg(err);
			int notFoundColumn = columnsToFindIndexes.indexOf(-1);
			if(notFoundColumn >= 0)
				throw QString("Missing column '%1' in file '%2' - error code: %2").arg(columnsToFind[notFoundColumn]).arg(*itCsvFile).arg(err);

			// Columns (excluding track and time point)
			QVector<QString> columnNamesQuantificationChunk;
			QVector<CellLineageTree::DataType> columnTypesQuantificationChunk;
			for(int iCol = 0; iCol < columnNames.size(); ++iCol) {
				if(columnNames[iCol] != "TrackNumber" && columnNames[iCol] != "TimePoint" && columnNames[iCol] != "Position") {
					columnNamesQuantificationChunk.push_back(columnNames[iCol]);
					columnTypesQuantificationChunk.push_back(CellLineageTree::Double);
				}
			}

			// Create quantification chunk
			int qIndex = outTreeAndQuantification.insertQuantificationChunk();
			outTreeAndQuantification.setQuantificationName(qIndex, quantName);
			outTreeAndQuantification.setQuantificationSoftware(qIndex, "QTFy");

			// Find segmentation method and detection wavelength from the chunk name
			int indexDw = quantName.indexOf("DetectionCh");
			int indexSegm = quantName.indexOf("SegMethod");

			QString detectionWL = quantName.mid(indexDw,indexSegm-indexDw).remove("DetectionCh");
			QString segmentationMethod = quantName.right(quantName.length() - indexSegm).remove("SegMethod");

			bool conversionStateDetect = false;
			int detectionWLInt = detectionWL.toInt(&conversionStateDetect);

			bool conversionStateMethod = false;
			int segmentationMethodInt = segmentationMethod.toInt(&conversionStateMethod);

			if (conversionStateDetect && conversionStateMethod) {
				QString remark = QString("Based on segmentation method: %1, produced from the images of channel: %2").arg(segmentationMethod).arg(detectionWL);
				outTreeAndQuantification.setQuantificationRemarks(qIndex, remark);
			}

			if(!outTreeAndQuantification.setQuantificationColumns(qIndex, columnNamesQuantificationChunk, columnTypesQuantificationChunk))
				throw QString("Cannot initialize quantification columns of file %1").arg(*itCsvFile);

			// Put data into it
			QSet<int> knownCells;
			CellLineageTree::FrameIndex frameIndex;
			frameIndex.index.resize(outTreeAndQuantification.getFrameIndexElements().size());
			frameIndex[2] = 1;	// Z-Index

			if (conversionStateDetect)
				frameIndex[3] = detectionWLInt;	// Detection Channel
			else
				frameIndex[3] = 0;	// Default Channel

			QVector<QVariant> dataRow;
			dataRow.resize(columnNamesQuantificationChunk.size());
			for(int iRow = 0; iRow < rows.size(); ++iRow) {
				// Get row
				const QVector<double>& curRow = rows[iRow];
				int cell = curRow[columnsToFindIndexes[0]];
				if(!knownCells.contains(cell)) {
					knownCells.insert(cell);
					if(!outTreeAndQuantification.insertQuantificationCell(qIndex, cell))
						throw QString("Cannot add cell %1 to quantification of tree %2").arg(cell).arg(*itCsvFile);
				}
				int tp = curRow[columnsToFindIndexes[1]];
				int pos = curRow[columnsToFindIndexes[2]];
				frameIndex[0] = tp;
				frameIndex[1] = pos;
				int srcCol = 0;
				int dstCol = 0;
				while(srcCol < curRow.size() && dstCol < dataRow.size()) {
					if(!columnsToFindIndexes.contains(srcCol)) {
						dataRow[dstCol++] = curRow[srcCol];
					}
					++srcCol;
				}
				
				// Add row
				if(!outTreeAndQuantification.setQuantificationDataPoint(qIndex, cell, frameIndex, dataRow))
					throw QString("Cannot process row %1 of quantification file %2").arg(iRow).arg(*itCsvFile);
			}
		}
	}
	catch(QString error) {
		// Something went wrong
		returnMsg = error;
		outTree.reset();
		outTreeAndQuantification.clear();
	}
	catch(...) {
		returnMsg = "Unhandled exception while reading tree and quantification.";
		outTree.reset();
		outTreeAndQuantification.clear();
	}

	// Done
	return returnMsg;
}

QString Tools::writeTTTFileWithCsvQuantification(const QString& tttFilePath, const TTTPositionManager* tttpm, Tree& tree, const CellLineageTree& treeAndQuantification)
{
	QString returnMsg;

	try {
		/**
		 * Save ttt file
		 */
		if(!TTTFileHandler::saveFile(tttFilePath, &tree, tttpm->getFirstTimePoint(), tttpm->getLastTimePoint()))
			throw QString("Cannot save .ttt file '%1'").arg(tttFilePath);

		/**
		 * Save quantification from CellLineageTree
		 */

		QString sep = ",";

		if(treeAndQuantification.getFrameIndexElements().size() != 4 || treeAndQuantification.getFrameIndexElements()[0] != "TimePoint" || treeAndQuantification.getFrameIndexElements()[1] != "FieldOfView")
			throw QString("Unexpected frame index elements.");

		// Extract path and filename without extension
		int sepIndex = QDir::fromNativeSeparators(tttFilePath).lastIndexOf('/');
		QString path, fileName;
		if(sepIndex >= 0) {
			path = tttFilePath.left(sepIndex);
			fileName = tttFilePath.mid(sepIndex+1);
		}
		else {
			path = '.';
			fileName = tttFilePath;
		}
		QString fileNameWithoutExtension = fileName.left(fileName.length() - 4);

		// Save each quantification chunk in a csv file
		for(int iQuant = 0; iQuant < treeAndQuantification.getNumberOfQuantificationChunks(); ++iQuant) {
			// Create file
			QString fileNameQuant = fileNameWithoutExtension + "_" + treeAndQuantification.getQuantificationName(iQuant) + ".csv";
			QFile fCsv(path + "/" + fileNameQuant);
			if(!fCsv.open(QIODevice::WriteOnly))
				throw QString("Error: cannot open file '%1' - error: %2").arg(fileNameQuant).arg(fCsv.errorString());

			// Write header
			QTextStream s(&fCsv);
			s << "TrackNumber"<< sep << "TimePoint"<< sep << "Position";
			QVector<QString> cols = treeAndQuantification.getQuantificationColumnNames(iQuant);
			for(auto itCol = cols.begin(); itCol != cols.end(); ++itCol)
				s << sep << *itCol;
			s << "\n";

			// Write rows for each cell
			QList<int> cellIds = treeAndQuantification.getCellIds();
			for(auto itCell = cellIds.begin(); itCell != cellIds.end(); ++itCell) {
				QList<CellLineageTree::FrameIndex> frameIndexes = treeAndQuantification.getQuantificationFrameIndexes(iQuant, *itCell);
				qSort(frameIndexes);
				for(auto itFrameIndex = frameIndexes.begin(); itFrameIndex != frameIndexes.end(); ++itFrameIndex) {
					const CellLineageTree::FrameIndex& frameIndex = *itFrameIndex;
					
					// track number, time point, position
					s << *itCell << sep << frameIndex.index[0] << sep << frameIndex.index[1];

					// data
					QVector<QVariant> row = treeAndQuantification.getQuantificationDataPoint(iQuant, *itCell, *itFrameIndex);
					for(int iCol = 0; iCol < row.size(); ++iCol) {
						bool ok;
						s << sep << row[iCol].toDouble(&ok);
						if(!ok)
							throw QString("Cannot convert quantification value '%1' to double in file '%2'").arg(row[iCol].toString()).arg(fileNameQuant);
					}
					s << "\n";
				}
			}
		}
	}
	catch(QString error) {
		// Something went wrong
		returnMsg = error;
	}
	catch(...) {
		returnMsg = "Unhandled exception while saving tree and quantification.";
	}

	// Done
	return returnMsg;
}



QString Tools::writeCsvQuantification(const QString& tttFilePath, const CellLineageTree& treeAndQuantification)
{
	QString returnMsg;

	try {
		
		/**
		 * Save quantification from CellLineageTree
		 */

		if(treeAndQuantification.getFrameIndexElements().size() != 4 || treeAndQuantification.getFrameIndexElements()[0] != "TimePoint" || treeAndQuantification.getFrameIndexElements()[1] != "FieldOfView")
			throw QString("Unexpected frame index elements.");

		// Extract path and filename without extension
		int sepIndex = QDir::fromNativeSeparators(tttFilePath).lastIndexOf('/');
		QString path, fileName;
		if(sepIndex >= 0) {
			path = tttFilePath.left(sepIndex);
			fileName = tttFilePath.mid(sepIndex+1);
		}
		else {
			path = '.';
			fileName = tttFilePath;
		}
		QString fileNameWithoutExtension = fileName.left(fileName.length() - 4);

		QString sep = ",";

		// Save each quantification chunk in a csv file
		for(int iQuant = 0; iQuant < treeAndQuantification.getNumberOfQuantificationChunks(); ++iQuant) {
			// Create file
			QString fileNameQuant = fileNameWithoutExtension + "_" + treeAndQuantification.getQuantificationName(iQuant) + ".csv";
			QFile fCsv(path + "/" + fileNameQuant);
			if(!fCsv.open(QIODevice::WriteOnly))
				throw QString("Error: cannot open file '%1' - error: %2").arg(fileNameQuant).arg(fCsv.errorString());

			// Write header
			QTextStream s(&fCsv);
			s << "TrackNumber"<< sep << "TimePoint" << sep << "Position";
			QVector<QString> cols = treeAndQuantification.getQuantificationColumnNames(iQuant);
			for(auto itCol = cols.begin(); itCol != cols.end(); ++itCol)
				s << sep << *itCol;
			s << "\n";

			// Write rows for each cell
			QList<int> cellIds = treeAndQuantification.getCellIds();
			for(auto itCell = cellIds.begin(); itCell != cellIds.end(); ++itCell) {
				QList<CellLineageTree::FrameIndex> frameIndexes = treeAndQuantification.getQuantificationFrameIndexes(iQuant, *itCell);
				qSort(frameIndexes);
				for(auto itFrameIndex = frameIndexes.begin(); itFrameIndex != frameIndexes.end(); ++itFrameIndex) {
					const CellLineageTree::FrameIndex& frameIndex = *itFrameIndex;
					
					// track number, time point, position
					s << *itCell << sep << frameIndex.index[0] << sep << frameIndex.index[1];

					// data
					QVector<QVariant> row = treeAndQuantification.getQuantificationDataPoint(iQuant, *itCell, *itFrameIndex);
					for(int iCol = 0; iCol < row.size(); ++iCol) {
						bool ok;
						s << sep << row[iCol].toDouble(&ok);
						if(!ok)
							throw QString("Cannot convert quantification value '%1' to double in file '%2'").arg(row[iCol].toString()).arg(fileNameQuant);
					}
					s << "\n";
				}
			}
		}
	}
	catch(QString error) {
		// Something went wrong
		returnMsg = error;
	}
	catch(...) {
		returnMsg = "Unhandled exception while saving tree and quantification.";
	}

	// Done
	return returnMsg;
}


bool Tools::isTreeQuantifiedBefore(const QString& tttFilePath)
{
	// Extract path and filename without extension
	int sepIndex = QDir::fromNativeSeparators(tttFilePath).lastIndexOf('/');
	QString path, fileName;
	if(sepIndex >= 0) {
		path = tttFilePath.left(sepIndex);
		fileName = tttFilePath.mid(sepIndex+1);
	}
	else {
		path = '.';
		fileName = tttFilePath;
	}
	QString fileNameWithoutExtension = fileName.left(fileName.length() - 4);

	// List csv files
	QStringList csvFiles = FastDirectoryListing::listFiles(QDir(path), QStringList() << ".csv", QStringList() << fileNameWithoutExtension, false);

	if (csvFiles.empty())
		return false;
	else
		return true;

}


//void Tools::startTimeMeasure()
//{
//	measureTime = QTime::currentTime();
//	timingStarted = true;
//}
//
//int Tools::stopTimeMeasure()
//{
//	if (! timingStarted)
//		return -1;
//	
//	QTime stopTime = QTime::currentTime();
//	timingStarted = false;
//	
//	return measureTime.secsTo (stopTime);
//}
//
//bool Tools::readLogFile (TTTPositionManager *_tttpm, int &_ltp, int &_ftp, /*const QString &_pictureSuffix,*/ bool _showMessage)
//{
//	//create a new log file reader and read the file
//	
//	if (! _tttpm)
//		return false;
//	
//	bool success = false;
//	
//	if (_ltp == 0)
//		_ltp = -1;		//force setting in log file reader
//	
//	QString basename = _tttpm->getBasename();
//	
//	LogFileReader *logInput = new LogFileReader (_tttpm->getPictureDirectory() + "/" + basename + ".log", basename, _ltp, _ftp/*, _pictureSuffix*/);
//	
//	if ((! logInput->LogFileUpToDate() & (! TTTManager::getInst().loadTTTFilesOnly())) || (! logInput->LoadingSuccesful())) {
//		if (_showMessage)
//			QMessageBox::information (0, "Loading error", "Fsi file " + basename + ".log" + " does not correspond to picture files in directory \nPlease redo log file conversion \nPosition will not be available.", QMessageBox::Ok);
//		
//		_tttpm->lockPosition();
//		success = false;
//	}
//	else {
//		//associate the FileInfoArray for public access
//		_tttpm->setExperimentTimepoints (_ftp, _ltp);
//		_tttpm->setFileInfoArray (logInput->getArray());
//		success = true;
//	}
//
//    //BS 2010/08/05
//    //Datenkonsistenz ueberpruefen: erstes Bilddatum muss vor Systemdatum liegen!
//    QDate sd = SystemInfo::getSystemTime().date();
//
//	//OH: in some cases getFiles returns null
//    //QDate fpd = _tttpm->getFiles()->getRealTime (_ftp, -1, true).date();
//	const FileInfoArray* files = _tttpm->getFiles();
//	if(files) {
//		QDate fpd = files->getRealTime (_ftp, 1, -1, true).date();
//
//		if (sd.daysTo (fpd) > 0) {
//				QString botschaft = "Position error due to flawed pixel data (code " + QString ("%1").arg (0) + ").\n";
//				QMessageBox::warning (0, "Loading position failed", botschaft);
//
//				_tttpm->lockPosition();
//				success = false;
//		}
//	}
//
//	
//	if (logInput)
//		//now the LogFileReader is not needed anymore!
//		delete logInput;
//
//	return success;
//}
//
///*
//void Tools::exp_collListViewItems (Q3ListViewItem *_lvi, bool _expand, bool _recursive)
//{
//	if (! _lvi)
//		return;
//	
//	_lvi->setOpen (_expand);
//	
//	///@todo expands/collapses too much... (the current and all following children)
//	
//	if (_recursive) {
//		if (_lvi->childCount() > 0) {
//			
//			Q3ListViewItemIterator iter (_lvi->firstChild());
//			
//			while (iter.current()) {
//				Q3ListViewItem *item = iter.current();
//				item->setOpen (_expand);
//				
//				++iter;
//			}
//		}
//	}
//}
//
//void Tools::parseDOMDocumentIntoListview (QDomDocument &_domDoc, Q3ListView *_listView)
//{
//	if (! _listView)
//		return;
//	
//	_listView->clear();
//	
//	for (int col = 0; col < _listView->columns(); col++)
//		_listView->removeColumn (col);
//	
//	_listView->addColumn ("Element / Attribute");
//	_listView->addColumn ("Value");
//	
//	Q3ListViewItem *lvi = new Q3ListViewItem (_listView);
//	
//	//allow the user to open the root item
//	_listView->setRootIsDecorated (true);
//	
//	QDomElement docElem = _domDoc.documentElement();
//	
//	parseDOMElementIntoListviewItem (docElem, lvi);
//	
//	//expand the root item only
//	lvi->setOpen (true);
//}
//
//void Tools::parseDOMElementIntoListviewItem (QDomElement &_domEl, Q3ListViewItem *_lvi)
//{
//	if (_domEl.isNull())
//		return;
//	
//	QDomNodeList children = _domEl.childNodes();
//	
//	//write tag name and value
//	_lvi->setText (0, _domEl.tagName());
//	
//	if (children.count() == 1) {
//		//only write the text if exactly one text child is present, otherwise the text would be repeated
//		QDomNode ele = children.item (0);
//		//if this element has no children, then the text can be displayed
//		if (ele.childNodes().count() == 0)
//			_lvi->setText (1, _domEl.text());
//	}
//	else {
//		_lvi->setText (1, "");
//	}
//	
//	//loop over the tag's attributes
//	QDomNamedNodeMap attrs = _domEl.attributes();
//	
//        for (int i = 0; i < attrs.count(); i++) {
//		QDomNode n = attrs.item (i);
//		QDomAttr a = n.toAttr();
//		if (! a.isNull()) {
//			(void) new Q3ListViewItem (_lvi, a.name(), a.value());
//			
//		}
//	}
//	
//	//continue recursively with this node's children
//	if (children.count() > 0) {
//                for (int i = 0; i < children.count(); i++) {
//			QDomNode n = children.item (i);
//			QDomElement e = n.toElement();
//			if (! e.isNull()) {
//				parseDOMElementIntoListviewItem (e, new Q3ListViewItem (_lvi));
//			}
//		}
//	}
//	
//}
//*/
//QString Tools::extractValueFromDOMDocument (const QDomDocument &_dom, const QString &_elementName)
//{
//        //NOTE: does not work for multiple items with the same name!
//
//        //works for the following configurations:
//        //<tag attr="desired value" />
//        //<tag>value</tag>
//
//        QDomElement docElem = _dom.documentElement();
//
//        //traverse all elements of the document whether its name corresponds to the provided one
//        return extractValueFromDOMElement (docElem, _elementName);
//}
//
//QString Tools::extractValueFromDOMElement (const QDomElement &_domEl, const QString &_elementName)
//{
//        if (_domEl.isNull())
//                return "";
//
//        QDomNodeList children = _domEl.childNodes();
//
//        if (_domEl.tagName() == _elementName) {
//
//                //loop over the tag's attributes, until one non-zero is found
//                QDomNamedNodeMap attrs = _domEl.attributes();
//
//                for (int i = 0; i < attrs.count(); i++) {
//                        QDomNode n = attrs.item (i);
//                        QDomAttr a = n.toAttr();
//                        if (! a.isNull()) {
//                                return a.value();
//                        }
//                }
//
//                //if nothing was found in the attributes: check if this node has only one child -> return its text
//                if (children.count() == 1) {
//                        //if exactly one text child is present, use its text
//                        QDomNode ele = children.item (0);
//                        //if this element has no children, it's O.K.
//                        if (ele.childNodes().count() == 0)
//                                return _domEl.text();
//                }
//
//        }
//        else {
//                //continue recursively with this node's children
//                if (children.count() > 0) {
//                        QString tmp = "";
//                        for (int i = 0; i < children.count(); i++) {
//                                QDomNode n = children.item (i);
//                                QDomElement e = n.toElement();
//                                if (! e.isNull()) {
//                                        tmp = extractValueFromDOMElement (e, _elementName);
//                                        if (tmp > "") {
//                                            return tmp;
//                                        }
//                                }
//                        }
//                }
//        }
//
//        //nothing found
//        return "";
//}
//
//bool Tools::setValueInDOMDocument (const QDomDocument &_dom, const QString &_elementName, const QString &_elementValue)
//{
//    //NOTE: does not work for multiple items with the same name! (simply takes the first encountered)
//
//    //works for the following configurations:
//    //<tag attr="desired value" />
//    //<tag>value</tag>
//
//    QDomElement docElem = _dom.documentElement();
//
//	if(docElem.isNull())
//		return false;
//
//    //traverse all elements of the document whether its name corresponds to the provided one
//    return setValueInDOMElement (docElem, _elementName, _elementValue   );
//}
//
//bool Tools::setValueInDOMElement (const QDomElement &_domEl, const QString &_elementName, const QString &_elementValue)
//{
//        if (_domEl.isNull())
//                return false;
//
//        QDomNodeList children = _domEl.childNodes();
//
//        if (_domEl.tagName() == _elementName) {
//
//                //loop over the tag's attributes, until one non-zero is found
//                QDomNamedNodeMap attrs = _domEl.attributes();
//
//                for (int i = 0; i < attrs.count(); i++) {
//                        QDomNode n = attrs.item (i);
//                        QDomAttr a = n.toAttr();
//                        if (! a.isNull()) {
//                                a.setValue (_elementValue);
//                                return true;
//                        }
//                }
//
//                //if nothing was found in the attributes: check if this node has only one child -> return its text
//                if (children.count() == 1) {
//                        //if exactly one text child is present, use its text
//                        QDomNode ele = children.item (0);
//                        //if this element has no children, it's O.K.
//                        if (ele.childNodes().count() == 0) {
//                                ele.setNodeValue (_elementValue);
//                                return true;
//                        }
//                }
//
//        }
//        else {
//                //continue recursively with this node's children
//                if (children.count() > 0) {
//                        for (int i = 0; i < children.count(); i++) {
//                                QDomNode n = children.item (i);
//                                QDomElement e = n.toElement();
//                                if (! e.isNull()) {
//                                        if (setValueInDOMElement (e, _elementName, _elementValue))
//                                            return true;
//                                }
//                        }
//                }
//        }
//
//        //element not found
//        return false;
//}
//
//QString Tools::exportAllTreesOfPositionToImages (TTTPositionManager *_tttpm, bool _showMessage)
//{
//	//export a snapshot of all trees in this position
//	//agenda:
//	//- create a new TreeDisplay object in the background
//	//- for each tree:
//	//  - load each tree into the tree view
//	//  - save complete tree in unstretched format to its corresponding filename
//
//	QString folder;
//	QString us;
//	QString basename;
//	QString basename_without_pos;
//	
//#ifndef TREEANALYSIS // -------------------- Konstantin --------------
//	
//	if (! _tttpm)
//		return QString();
//	
//	_tttpm->initialize();
//	
//	TreeDisplay *td = new TreeDisplay (0);
//	
//	bool completeTree = true;
//	bool unstretched = true;
//	
//	QString filename = "";
//	
//	//create subdirectory for current experiment
//	folder = TTTManager::getInst().getNASDrive() + TREE_EXPORT_FOLDER + "/";
//	us = UserInfo::getInst().getUsersign();
//	
//	QStringList tttFiles = _tttpm->getAllTTTFiles();
//	
//	Tree *tree = 0;
//	for (QStringList::Iterator iter = tttFiles.begin(); iter != tttFiles.end(); ++iter)
//	{
//		tree = new Tree();
//		
//		int ftp = -1, ltp = -1, numTracks = -1;
//
//                if (TTTFileHandler::readFile (*iter, tree, numTracks, ftp, ltp, &_tttpm->positionInformation) != TTT_READING_SUCCESS)
//                        continue;
//		
//		td->init (tree, _tttpm);
//		
//		basename = _tttpm->getBasename();
//		basename_without_pos = basename.left (basename.lastIndexOf (POSITION_MARKER));
//		QString tree_folder = folder + us + "/" + basename_without_pos + "/" + basename;
//		
//		SystemInfo::checkNcreateDirectory (tree_folder, true, true);
//		
//		QString tree_filename_without_suffix = tree->getFilename();
//		tree_filename_without_suffix = tree_filename_without_suffix.left (tree_filename_without_suffix.length() - 4);		//cut off .ttt suffix
//		tree_filename_without_suffix = tree_filename_without_suffix.mid (tree_filename_without_suffix.lastIndexOf ("/") + 1);	//cut off path
//		
//		//filename = tree_folder + "/" + CurrentColonyName.left (CurrentColonyName.length() - 4) + "_" + QDateTime::currentDateTime().toString ("yyyyMMdd_hhmmss");
//		QString filename = tree_folder + "/" + tree_filename_without_suffix + "_" + QDateTime::currentDateTime().toString ("yyyyMMdd_hhmmss");
//		
//		td->exportTree (filename + "-tree.jpg", completeTree, unstretched, true, tree_filename_without_suffix, true, "JPEG");
//		
//		if (tree)
//			delete tree;
//	}
//	
//	if (_showMessage) 
//		Tools::displayMessageBoxWithOpenFolder("Please make sure to delete your exported trees after (ab)using them\nbecause they consume quite a lot of disk space on the net.", "Tree export completed.", folder);
//		//QMessageBox::information (0, "Saving storage space", "Please make sure to delete your exported trees after (ab)using them\nbecause they consume quite a vast amount of storage on the net.", "Surely I will...");
//	
//	if (td)
//		delete td;
//
//#endif // -------------------- Konstantin --------------
//
//	return folder + us + "/" + basename_without_pos;
//}
//
//
//#ifndef TREEANALYSIS 
//IntensityData Tools::measureCellIntensity (const TrackPoint &_trackPoint, int _wavelength, int _zIndex, bool _measureBackground)
//{
//
//
//	TTTPositionManager *tttpm = 0;
//	
//	//new version (Bernhard, 2009/11/23):
//	//the position where the trackpoint was set is stored, so we can use it directly
//	//yet, for older ones, this is not stored, so we have to calculate the position
//	// NOTE: set as comment for old movies without real positions
//	QString position_key = _trackPoint.Position;
//	if (position_key == "" || position_key == "000") {
//		tttpm = TTTManager::getInst().getPositionAt (_trackPoint.X, _trackPoint.Y);
//	}
//	else {
//		//position is stored in trackpoint
//		tttpm = TTTManager::getInst().getPositionManager (position_key);
//	}
//	
//	if (! tttpm)
//		return IntensityData();
//	
//	if (! tttpm->isInitialized())
//		tttpm->initialize();
//	
//	
//	if (! tttpm->getFiles()->exists (_trackPoint.TimePoint, _zIndex, _wavelength)) {
//		//picture at the desired timepoint with the desired wavelength does not exist
//		return IntensityData();
//	}
//	
//	int timePoint = _trackPoint.TimePoint;
//	bool picLoaded = tttpm->getPictures()->isLoaded (timePoint, _wavelength, 1);
//	if (! picLoaded) {
//		tttpm->getPictures()->loadOnePicture(PictureIndex(timePoint, _zIndex, _wavelength));
//		//tttpm->getPictures()->setLoading (timePoint, _zIndex, _wavelength, true);
//		//tttpm->getPictures()->loadPictures (timePoint, timePoint, -1, false);
//	}
//	
//	//cell intensity
//	int PixelCount = 0;
//	float MaxValueCell = 0;
//	float MinValueCell = 255;
//	float ValueIntegralCell = 0;
//	//int SumOfSquaresCell = 0;
//	//int StdDevCell = 0.0f;
//	float AvgCell = 0.0f;
//	int PixelColor = 0;
//	
//	//calculate picture coordinates
//    QPointF middle (_trackPoint.X, _trackPoint.Y);
//    middle = tttpm->getDisplays().at (_wavelength).calcTransformedCoords (middle, &tttpm->positionInformation);
//
//    int radius = (int)(_trackPoint.CellDiameter / 2.0f);
//	
//	for (int x = (int)(middle.x() - radius); x <= (int)(middle.x() + radius); x++) {
//		for (int y = (int)(middle.y() - radius); y <= (int)(middle.y() + radius); y++) {
//			
//			//check if point is inside the region of the cell (circle!) via Pythagoras
//			int dx = x - (int)middle.x();
//			int dy = y - (int)middle.y();
//			if ((dx*dx + dy*dy) <= radius*radius) {
//				PixelCount++;
//				
//				PixelColor = tttpm->getPictures()->readPixel (_trackPoint.TimePoint, _zIndex, _wavelength, x, y);
//				
//				if (PixelColor > MaxValueCell)
//					MaxValueCell = PixelColor;
//				if (PixelColor < MinValueCell)
//					MinValueCell = PixelColor;							
//				
//				ValueIntegralCell += PixelColor;
//				//SumOfSquaresCell += Tools::pow (PixelColor, 2);
//			}
//		}
//	}
//	
//	if (PixelCount > 0)
//		AvgCell = (float)ValueIntegralCell / (float)PixelCount;
//	
//	
//	float MaxValueBackground = 0;
//	float MinValueBackground = 255;
//	float ValueIntegralBackground = 0;
//	//SumOfSquaresBackground = 0;
//	//StdDevBackground = 0.0f;
//	float AvgBackground = 0.0f;
//	
//	if (_measureBackground && (_trackPoint.backgroundPoint().x() != -1.0f)) {
//		//background intensity
//		PixelCount = 0;
//		
//		//calculate picture coordinates
//		middle.setX (_trackPoint.XBackground);
//		middle.setY (_trackPoint.YBackground);
//                middle = tttpm->getDisplays().at (_wavelength).calcTransformedCoords (middle, &tttpm->positionInformation);
//		
//		for (int x = (int)(middle.x() - radius); x <= (int)(middle.x() + radius); x++) {
//			for (int y = (int)(middle.y() - radius); y <= (int)(middle.y() + radius); y++) {
//				
//				//check if point is inside the region of the cell (circle!) via Pythagoras
//				int dx = x - (int)middle.x();
//				int dy = y - (int)middle.y();
//				if ((dx*dx + dy*dy) <= radius*radius) {
//					PixelCount++;
//					
//					PixelColor = tttpm->getPictures()->readPixel (_trackPoint.TimePoint, _zIndex, _wavelength, x, y);
//					
//					if (PixelColor > MaxValueBackground)
//						MaxValueBackground = PixelColor;
//					if (PixelColor < MinValueBackground)
//						MinValueBackground = PixelColor;							
//					
//					ValueIntegralBackground += PixelColor;
//					//SumOfSquaresBackground += Tools::pow (PixelColor, 2);
//				}
//			}
//		}
//		if (PixelCount > 0)
//			AvgBackground = (float)ValueIntegralBackground / (float)PixelCount;
//	}
//	
//	return IntensityData (AvgCell, AvgBackground, MinValueCell, MaxValueCell, MinValueBackground, MaxValueBackground, ValueIntegralCell, ValueIntegralBackground);
//}
//#endif
//
//QCursor Tools::createCircleCursor (int _diameter, QColor _color, int _thickness)
//{
//        int diam = _diameter;
//
//        //there seems to be no restriction on cursor icon sizes... (neither Windows nor Linux)
//        //thus, simply create a cursor with the desired width
//        //add 2 to receive a nice circle; otherwise flattened at the right & bottom margin
//        int width = diam + 2;
//
//        //create pixmap with a transparency mask
//        QPixmap mousePix (width, width);
//        mousePix.fill (Qt::transparent);
//        QPainter p (&mousePix);
//        p.setPen (QPen (_color, _thickness));  //non-transparent
//        p.drawArc (QRect (0, 0, width - 2, width - 2), 0, 5760);
//        p.drawPoint (diam / 2, diam / 2);         //draw center point
//        p.end();
//
//        //set all pixels transparent but these on the circle line with the given radius
//        QBitmap mask (width, width);
//        mask.fill (Qt::color0); //transparent
//        p.begin (&mask);
//        p.setPen (QPen (Qt::color1, _thickness));  //non-transparent
//        p.drawArc (QRect (0, 0, width - 2, width - 2), 0, 5760);
//        p.drawPoint (diam / 2, diam / 2);         //draw center point
//        p.end();
//
//        mousePix.setMask (mask);
//
//        //the hot spot is set in the middle automatically
//        return QCursor (mousePix);
//}
//
//// ----------Oliver----------
//// Not used now
////QString Tools::encodeDate (int _year, int _month, int _day)
////{
////        //@todo @todo
////        return QString ("%1.%2.%3").arg (_year).arg (_month).arg (_day);
////}
//
//// ----------Oliver----------
//// Not used now
////QString Tools::decodeDate (const QString &_codedDate)
////{
////        //@todo @todo
////        QStringList listl = _codedDate.split (".");
////
////        int year = 0;
////        int month = 0;
////        int day = 0;
////
////        QStringList::const_iterator constIterator;
////        int i = 0;
////        for ( constIterator = listl.constBegin(); constIterator != listl.constEnd(); ++i, ++constIterator) {
////                switch (i) {
////                        case 0:
////                                year = (*constIterator).toInt();
////                                break;
////                        case 1:
////                                month = (*constIterator).toInt();
////                                break;
////                        case 2:
////                                day = (*constIterator).toInt();
////                                break;
////                        default:
////                                ;
////                }
////        }
////
////        //return QString ("%1%2%3").arg (year, 4, 10, '0').arg (month, 2, 10, '0').arg (day, 2, 10, '0');
////        return QDate (year, month, day).toString ("yyyyMMdd");
////}
//
//// ----------Oliver----------
//bool Tools::readIDFile(const QString& _fileName, QDate& _installDate)
//{
//	// Open file
//	QFile file(_fileName);
//	if (! file.open (QIODevice::ReadOnly))
//		return false;
//	
//	// Read data
//	int y, m, d;
//    QDataStream reader(&file);
//	reader >> y >> m >> d;
//
//	// Return data
//	if(!_installDate.setDate(y, m, d))
//		return false;
//
//	return true;
//}
//
//// ----------Oliver----------
//bool Tools::readDDFile(const QString& _fileName, QDate& _ddDate, int& _tttStartCount)
//{
//	// Open file
//	QFile file(_fileName);
//	if (! file.open (QIODevice::ReadOnly))
//		return false;
//	
//	// Read data
//	int y, m, d, count;
//    QDataStream reader(&file);
//	reader >> y >> m >> d >> count;
//
//	// Return data
//	_tttStartCount = count;
//	if(!_ddDate.setDate(y, m, d))
//		return false;
//	
//	return true;
//}
//
//// ----------Oliver----------
//QString Tools::getXDFilesPath()
//{
//	// Check if files exist locally
//#ifdef Q_WS_WIN
//	// Get path to profile of "All Users"
//	QString secPath(getenv("ALLUSERSPROFILE"));
//
//	if(secPath.isEmpty()) {
//		return QString();
//	}
//
//	// Append "System"
//	if(secPath.right(1) != "/" && secPath.right(1) != "\\")
//		secPath += "/";
//
//	secPath += "System/";
//#else
//	QString secPath = SystemInfo::homeDirectory() + XDFILES_PATH;
//#endif
//
//	// Check if files exist
//	QString ddFile = secPath + XDFILES_DDFILENAME;
//	QString idFile = secPath + XDFILES_IDFILENAME;
//	if(QFile::exists(ddFile) && QFile::exists(idFile))
//		return secPath;
//
//	// Now look if [appPath]/../../System exists and contains the files
//
//	// Get current directory. Canot use SystemInfo::getExecutablePath(),
//	// as it depends on QApplication object, which has not been created yet,
//	// when this function is called. So do not call 
//	QString appPath = SystemInfo::getExecutablePath();
//
//	if(appPath.right(1) != "/")
//		appPath += '/';
//
//	// Look in ../../System
//	appPath += "../../System";
//	QDir networkSystemPath(appPath);
//
//	// Check if folder exists
//	if(networkSystemPath.exists()) {
//		QString absPath = networkSystemPath.absolutePath();
//		if(absPath.right(1) != "/")
//			absPath += '/';
//
//		// Check if files exist
//		QString ddFile = absPath + XDFILES_DDFILENAME;
//		QString idFile = absPath + XDFILES_IDFILENAME;
//		if(QFile::exists(ddFile) && QFile::exists(idFile))
//			return absPath;
//	}
//
//	return "";
//}
//
//// ----------Oliver----------
//bool Tools::writeIDFile(const QString& _fileName, QDate _id)
//{
//	// Open file
//	QFile file(_fileName);
//	if (! file.open (QIODevice::WriteOnly))
//		return false;
//	
//	// Write data
//    QDataStream writer(&file);
//	writer << _id.year() << _id.month() << _id.day();
//	
//	if(!file.flush())
//		return false;
//	
//	return true;
//}
//
//// ----------Oliver----------
//bool Tools::writeDDFile(const QString& _fileName, QDate _dd, int _useCount)
//{
//	// Open file
//	QFile file(_fileName);
//	if (! file.open (QIODevice::WriteOnly)) {
//		return false;
//	}
//	
//	// Write data
//    QDataStream writer(&file);
//	writer << _dd.year() << _dd.month() << _dd.day() << _useCount;
//
//	if(!file.flush())
//		return false;
//
//	return true;
//}
//
//bool Tools::pruefeAnfangsDaten()
//{
//        int errorType = 0;
//        int az = 0;
//
//        unsigned int zufallsSamen = 1000;
//
//        //Anmerkung: try/catch gibts nicht, daher die umstaendliche Variante einer for-Schleife mit Abbruechen
//        for (int i = 0; i <= 0; i++) {
//
//
//                if (QDate::currentDate().daysTo (expirationDate) <= 0) {
//                        errorType = 6;
//						//QMessageBox::critical (0, "Version expired!", "Your current version of the application has expired. \nPlease contact Dr. Timm Schroeder, providing this message.");
//                        break;
//                }
//
//
//                //std::cout << SystemInfo::homeDirectory().toStdString() << std::endl;
//				
//				// ------------ Oliver -------------------
//                //if (! SystemInfo::checkNcreateDirectory (SystemInfo::homeDirectory() + XDFILES_PATH, false, false)) {
//                //        errorType = 5;
//                //        break;
//                //}
//
//				
//				// Format of xd files changed to binary and location changed on windows systems
//				// therefore file reading and checks had to be changed
//
//                //QString idfilePath = SystemInfo::homeDirectory() + XDFILES_PATH + XDFILES_IDFILENAME;
//                //QString ddfilePath = SystemInfo::homeDirectory() + XDFILES_PATH + XDFILES_DDFILENAME;
//
//                //QString idContent = Tools::readTextFile (idfilePath);
//                //QString ddContent = Tools::readTextFile (ddfilePath);
//
//                ////lies id
//                //if (idContent == "") {
//                //        errorType = 1;
//                //        break;
//                //}
//
//                //QStringList listl = idContent.split ("\n");
//
//                //if (listl.size() < 5) {
//                //        errorType = 1;
//                //        break;
//                //}
//
//                //QString id = listl.at (4);
//                //id = id.mid (3);
//                //id = decodeDate (id);
//
//                //if (id == "") {
//                //        errorType = 1;
//                //        break;
//                //}
//
//                ////lies dd & az
//                //if (ddContent == "") {
//                //        errorType = 2;
//                //        break;
//                //}
//
//                //QStringList listl2 = ddContent.split ("\n");
//
//                //if (listl2.size() < 6) {
//                //        errorType = 2;
//                //        break;
//                //}
//
//                ////std::cout << ddContent.toStdString() << std::endl;
//
//                //QString dd = listl2.at (4);
//                ////std::cout << dd.toStdString() << std::endl;
//                //dd = dd.mid (3);
//                //dd = decodeDate (dd);
//                //if (dd == "") {
//                //        errorType = 2;
//                //        break;
//                //}
//
//                //QString aztmp = listl2.at (5);
//                //az = aztmp.mid (3).toInt();
//
//                //QString sd = SystemInfo::getSystemDateAsString ("yyyyMMdd");
//
//                ////Vergleiche Daten
//                //if (sd < id) {
//                //        errorType = 3;
//                //        break;
//                //}
//                //else if (sd < dd) {
//                //        errorType = 4;
//                //        break;
//                //}
//                //else if (az >= XDFILES_MAXAUFRUFE) {
//                //        errorType = 7;
//                //        break;
//                //}
//
//				// Init xdPath if not happened yet
//				if(xdPath.isEmpty())
//					 xdPath = getXDFilesPath();
//
//				// Get filenames
//				QString secPath = xdPath;
//				if(secPath.isEmpty()) {
//					errorType = 8;
//					break;
//				}
//				
//				// Check if dir exists
//                if (! SystemInfo::checkNcreateDirectory (secPath, false, false)) {
//                        errorType = 5;
//                        break;
//                }
//
//				QString idfilePath = secPath + XDFILES_IDFILENAME;
//                QString ddfilePath = secPath + XDFILES_DDFILENAME;
//
//				// Get content
//				QDate ddDate;
//				QDate idDate;
//
//				// Read id file
//				if(!readIDFile(idfilePath, idDate)) {
//					errorType = 1;
//                    break;
//				}
//				
//				// Read dd file
//				if(!readDDFile(ddfilePath, ddDate, az)) {
//					errorType = 2;
//                    break;
//				}
//
//				// Get current Date
//				QDate sysDate = QDate::currentDate();
//
//                //Vergleiche Daten
//                if (sysDate < idDate) {
//                        errorType = 3;
//                        break;
//                }
//                else if (sysDate < ddDate) {
//                        errorType = 4;
//                        break;
//                }
//                else if (az >= XDFILES_MAXAUFRUFE) {
//                        errorType = 7;
//                        break;
//                }
//
//				// <------------ Oliver ------------------
//
//				// ------------ Oliver -------------------
//                //if (errorType == 0) {
//                //        //falls bis hierher in Ordnung, ueberpruefe noch, ob dies evtl. eine neuere Version von TTT ist als in ID gespeichert
//                //        QString internesDatum = internalDate;
//                //        if (internesDatum > id) {
//                //                int year = internesDatum.left (4).toInt();
//                //                int month = internesDatum.mid (4, 2).toInt();
//                //                int day = internesDatum.mid (6, 2).toInt();
//								
//                //                QString content = XDFILES_COMMENT + "\n";
//                //                content += "ID=" + encodeDate (year, month, day) + "\n";
//                //                writeTextFile (idfilePath, content);
//								
//                //        }
//                //}
//
//                if (errorType == 0) {
//                        //falls bis hierher in Ordnung, ueberpruefe noch, ob dies evtl. eine neuere Version von TTT ist als in ID gespeichert
//                        QString internesDatum = internalDate;
//						int year = internesDatum.left (4).toInt();
//                        int month = internesDatum.mid (4, 2).toInt();
//                        int day = internesDatum.mid (6, 2).toInt();
//						QDate internalDate(year, month, day);
//
//                        if (internalDate > idDate) 
//								writeIDFile(idfilePath, internalDate);
//                }
//				// <------------ Oliver ------------------
//
//        }
//
//        //Fehlerauswertung
//        //Hinweis: die Fehlermeldung sollte das Problem (kodiert!) charakterisieren, um im berechtigten Fall Hilfe leisten zu koennen
//        //ACHTUNG: einmal gesetzte Zahlen nicht mehr aendern!!!! Nur hinten anfuegen! (Sonst keine Problemzuordnung in alten Versionen mehr moeglich)
//        QString botschaft = "";
//
//        zufallsSamen = errorType * (az + 3) * (SystemInfo::getSystemTime().time().second() + 2);
//        srand (zufallsSamen);
//
//        switch (errorType) {
//                case 0:
//                        //Kein Fehler
//                        UserInfo::getInst().setAZ (az);
//                        return true;
//                case 1:
//                        //Installationsdatum nicht lesbar
//                        if (botschaft == "") {
//                                botschaft = "Internal error in segment 1x" + QString ("%1").arg (rand()) + ".\n";
//                        }
//
//                case 2:
//                        //Dateidatum nicht lesbar
//                        if (botschaft == "") {
//                                botschaft = "Internal error in segment 2x" + QString ("%1").arg (rand()) + ".\n";
//                        }
//
//                case 3:
//                        //Installationsdatum groesser als Systemdatum
//                        if (botschaft == "") {
//                                botschaft = "Internal error in segment 3x" + QString ("%1").arg (rand()) + ".\n";
//                        }
//
//                case 4:
//                        //Dateidatum groesser als Systemdatum
//                        if (botschaft == "") {
//                                botschaft = "Internal error in segment 4x" + QString ("%1").arg (rand()) + ".\n";
//                        }
//
//                case 5:
//                        //Ordner mit Dateien existiert nicht
//                        if (botschaft == "") {
//                                botschaft = "Internal error in segment 5x" + QString ("%1").arg (rand()) + ".\n";
//                        }
//
//                case 6:
//                        //Version abgelaufen
//
//                        if (botschaft == "") {
//                                botschaft = "Internal error in segment 6x" + QString ("%1").arg (rand()) + ".\n";
//                        }
//
//                case 7:
//                        //Maximale Anzahl Aufrufe ueberschritten
//                        if (botschaft == "") {
//                                botschaft = "Internal error in segment 7x" + QString ("%1").arg (rand()) + ".\n";
//                        }
//
//				case 8:
//						// Internal error
//                        if (botschaft == "") {
//                                botschaft = "Internal error in segment 8x" + QString ("%1").arg (rand()) + ".\n";
//                        }
//
//                        QMessageBox::critical (0, "Internal problem", botschaft + "Please contact Dr. Timm Schroeder, providing this message.");
//                        return false;
//                default:
//                        return false;
//        }
//
//}
//
//bool Tools::schreibeAnfangsDaten()
//{
//        //gleiche Checks wie zu Begin...
//        if (! pruefeAnfangsDaten())
//                return false;
//
//        int errorType = 0;
//        int az = 0;
//
//        unsigned int zufallsSamen = 1000;
//
//        //Anmerkung: try/catch gibts nicht, daher die umstaendliche Variante einer for-Schleife mit Abbruechen
//        for (int i = 0; i <= 0; i++) {
//                //QString ddfilePath = SystemInfo::homeDirectory() + XDFILES_PATH + XDFILES_DDFILENAME;
//				// Get filenames
//				QString secPath = xdPath;
//				if(secPath.isEmpty()) {
//					errorType = 8;
//					break;
//				}
//                QString ddfilePath = secPath + XDFILES_DDFILENAME;
//
//                if (! QFile::exists (ddfilePath)) {
//                        errorType = 1;
//                        break;
//                }
//
//                //QDate sd = SystemInfo::getSystemTime().date();
//				QDate sd = QDate::currentDate();
//
//                az = UserInfo::getInst().getAZ();
//                az++;
//
//				// Write dd file
//				if(!writeDDFile(ddfilePath, sd, az)){
//                        errorType = 2;
//                        break;
//                }
//
//                //QString content = XDFILES_COMMENT + "\n";
//                //content += "DD=" + encodeDate (sd.year(), sd.month(), sd.day()) + "\n";
//                //content += "AZ=" + QString ("%1").arg (az) + "\n";
//                //if (! writeTextFile (ddfilePath, content)) {
//                //        errorType = 2;
//                //        break;
//                //}
//        }
//
//        //Fehlerauswertung
//        //Hinweis: die Fehlermeldung sollte das Problem (kodiert!) charakterisieren, um im berechtigten Fall Hilfe leisten zu koennen
//        //ACHTUNG: einmal gesetzte Zahlen nicht mehr aendern!!!! Nur hinten anfuegen! (Sonst keine Problemzuordnung in alten Versionen mehr moeglich)
//        QString botschaft = "";
//
//        zufallsSamen = errorType * (az + 3) * (SystemInfo::getSystemTime().time().minute() + 2);
//        srand (zufallsSamen);
//
//        //Fehlerauswertung
//        switch (errorType) {
//                case 0:
//                        //Kein Fehler
//                        return true;
//
//                case 1:
//                        //Dateidatumsdatei nicht vorhanden
//                        if (botschaft == "") {
//                                botschaft = "Internal error in segment 1y" + QString ("%1").arg (rand()) + ".\n";
//                        }
//
//                case 2:
//                        //Dateidatum nicht schreibbar
//                        if (botschaft == "") {
//                                botschaft = "Internal error in segment 2y" + QString ("%1").arg (rand()) + ".\n";
//                        }
//
//				case 3:
//						// Internal error
//                        if (botschaft == "") {
//                                botschaft = "Internal error in segment 3y" + QString ("%1").arg (rand()) + ".\n";
//                        }
//
//                        QMessageBox::critical (0, "Internal problem", botschaft + "Please contact Dr. Timm Schroeder, providing this message.");
//                        return false;
//                default:
//                        return false;
//        }
//	return true;
//}
//
////QDateTime Tools::readPictureCreationDate (const QString &_pictureFilename)
////{
////        //
////        //thus, the jpg/tiff creation date is read from the logfile (which already happened before)
////
////        QString xmlFilename = _pictureFilename.append (PICTURE_META_APPENDIX);
////
////        if (QFile::exists (xmlFilename)) {
////                //meta xml exists - read creation date from this file
////
////        }
////        else {
////                //meta xml does not exist - try to get experiment basename
////
////        }
////
////
////}
//
//bool Tools::stringToInt(const QString& str, int& i, int base)
//{
//	bool ok;
//	i = str.toInt(&ok, base);
//	return ok;
//}
//
void Tools::displayMessageBoxWithOpenFolder( QString _message, QString _caption, QString _folder, bool _folderIncludesFile, const QWidget* _parent )
{
	// Show messagebox with possibility to open folder
	QMessageBox msgBox;

	// Default caption
	if(_caption.isEmpty())
		_caption = "QTFy";

	// Include message
	msgBox.setWindowTitle(_caption);
	msgBox.setText(_message);

	// Add buttons
	QAbstractButton *okButton = msgBox.addButton("Ok", QMessageBox::AcceptRole);
	msgBox.setDefaultButton( (QPushButton*)okButton);
	QAbstractButton *openFolderButton = msgBox.addButton("Open Folder", QMessageBox::ActionRole);

	// Display messagebox
	msgBox.exec();

	// Open folder if desired
	if(msgBox.clickedButton() == openFolderButton) {
		QString nativeMovieFolder = QDir::toNativeSeparators(_folder);
		QString commandLineArguments;
		if(_folderIncludesFile)
			// Opens the folder and selects the file
			commandLineArguments = QString("/select,") + nativeMovieFolder;
		else
			// Just open the folder
			commandLineArguments = nativeMovieFolder;

		//QProcess::execute("Explorer.exe", QStringList(commandLineArguments));
		QProcess::execute(QString("Explorer.exe ") + commandLineArguments);
	}
}

int Tools::getNumOfPositionDigits()
{
	return numDigitsForPosIndex;
}

void Tools::setNumOfPositionDigits( int _numDigits )
{
	// Issue a warning if this was already set
	if(numDigitsForPosIndex != -1)
		qWarning() << "TTT warning: Tools::setNumOfPositionDigits() called, but numOfDigitsForPosIndex is already set.";

	numDigitsForPosIndex = _numDigits;
}

//QString Tools::convertIntPositionToString( int _posNumber )
//{
//	// Determine num digits
//	int numDigits = numDigitsForPosIndex;
//	if(numDigits == -1)
//		numDigits = 4;		// assume 4
//
//	// Convert
//	return QString("%1").arg(_posNumber, numDigits, 10, QChar('0'));
//}
//
//int Tools::getNumOfWavelengthDigits()
//{
//	// Issue a warning if this is not set
//	if(numDigitsForWlIndex == -1)
//		qWarning() << "TTT warning: Tools::getNumOfWavelengthDigits() called, but numDigitsForWlIndex is not set.";
//
//	return numDigitsForWlIndex;
//}
//
//void Tools::setNumOfWavelengthDigits( int _numDigits )
//{
//	// Issue a warning if this was already set
//	if(numDigitsForWlIndex != -1)
//		qWarning() << "TTT warning: Tools::setNumOfWavelengthDigits() called, but numDigitsForWlIndex is already set.";
//
//	numDigitsForWlIndex = _numDigits;
//}
//
//int Tools::getNumOfZDigits()
//{
//	// Issue a warning if this is not set
//	if(numDigitsForZIndex == -1)
//		qWarning() << "TTT warning: Tools::getNumOfZDigits() called, but numDigitsForZIndex is not set.";
//
//	return numDigitsForZIndex;
//}
//
//void Tools::setNumOfZDigits( int _numDigits )
//{
//	// Issue a warning if this was already set
//	if(numDigitsForZIndex != -1)
//		qWarning() << "TTT warning: Tools::setNumOfZDigits() called, but numDigitsForZIndex is already set.";
//
//	numDigitsForZIndex = _numDigits;
//}
//
//QImage Tools::applyBackgroundCorrection( const QImage&  originalImage, const unsigned short* backgroundImage, const unsigned short* gainImage )
//{
//	// Basic input checks
//	if(originalImage.isNull() || !backgroundImage || !gainImage)
//		return QImage();
//
//	// Create output image
//	int sizeX = originalImage.width(),
//		sizeY = originalImage.height();
//	QImage retImage(sizeX, sizeY, QImage::Format_Indexed8);
//	retImage.setColorTable(getColorTableGrayIndexed8());
//	unsigned char* outData = retImage.bits();
//	int outBpl = retImage.bytesPerLine();
//
//	// Run correction
//	const unsigned char* inData = originalImage.constBits();
//	int inBpl = originalImage.bytesPerLine();
//	/*
//	int minValue;
//	int maxValue;
//	int* buffer = new int[sizeX*sizeY];
//	*/
//	double minValue, maxValue;	// Debug
//	double minAllowedResult = -100.0;
//	double maxAllowedResult = 100.0;
//	for(int y = 0; y < sizeY; ++y) {
//		for(int x = 0; x < sizeX; ++x) {
//			// Map input, backround and gain to double in [0;1]
//			double inValue = inData[y*inBpl+x];
//			inValue /= 255.0;
//			double bgr = backgroundImage[y*sizeX + x];
//			bgr /= 65535.0;
//			double gain = gainImage[y*sizeX + x];
//			gain /= 65535.0;
//
//			// Make sure not to divide by 0
//			if(gain == 0.0)
//				gain = 1.0;
//
//			// Calculate new value
//			double newVal = (inValue - bgr) / gain;
//
//			// Debug
//			if(y == 0 && x == 0) {
//				minValue = newVal;
//				maxValue = newVal;
//			}
//			else {
//				minValue = std::min(minValue, newVal);
//				maxValue = std::max(maxValue, newVal);
//			}
//
//			// Make sure value is in [minResult;maxResult]
//			newVal = std::max(newVal, minAllowedResult);
//			newVal = std::min(newVal, maxAllowedResult);
//
//			// Map to [0,255] and store result
//			outData[y*outBpl + x] = (newVal - minAllowedResult) * (255.0 / (maxAllowedResult - minAllowedResult));
//
//			/*
//			buffer[y*sizeX+x] = newVal;
//			if(y == 0 && x == 0) {
//				minValue = newVal;
//				maxValue = newVal;
//			}
//			else {
//				minValue = std::min(minValue, newVal);
//				maxValue = std::max(maxValue, newVal);
//			}
//			*/
//		}
//	}
//	//qDebug() << "min: " << minValue << ", max: " << maxValue;
//
//	// Map data from [minValue, maxValue] to [0, 255]
//	/*
//	int offset = -minValue;
//	float scaling = 255.0f/static_cast<float>(maxValue - minValue);
//	for(int y = 0; y < sizeY; ++y) {
//		for(int x = 0; x < sizeX; ++x) {
//			outData[y*outBpl + x] = scaling * static_cast<float>(buffer[y*sizeX+x] + offset);
//		}
//		}
//		delete[] buffer;
//	*/
//	
//	// Done
//	return retImage;
//}
