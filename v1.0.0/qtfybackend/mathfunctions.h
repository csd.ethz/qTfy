/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MATHFUNCTIONS_H
#define MATHFUNCTIONS_H



// QT
#include <QVector>
#include <QMap>
#include <qmath.h>

// QWT
#include <qwt/qwt_interval_data.h>


/**
@author Konstantin

MathFunctions provides mathematic functions as average, standard deviation, maximum, minimum calculations and so on
*/

struct MathFunctions {

	//template <class T>
	//static const T& min ( const T& a, const T& b ) {
	//        return (a<b)?a:b;
	//}

	//template <class T>
	//static const T& max ( const T& a, const T& b ) {
	//        return (a>b)?a:b;
	//}

	/**
	* @param _number number that should be calculated as 2 to the power of n (see return). So _number is 1, 2, 4, 8, 16, 32... For another numbers
	this method will return -1
	* @return the power of 2
	*/
	template<class T>
	static T log_2(T _number)
	{
		//int power = 1;
		//while ( ! ((_number/(pow(2.0,power))) == 1 ) ){
		//	power++;
		//	if ( pow(2.0, power) > _number )
		//		return -1;
		//}
		//return power;
		return log(_number)/log(2.0);
	}

	/**
	* @param _values vector with values
	* @return maximal value of a vector
	*/
	static int getMaxValue(QVector<int> _values)
	{
		int maxValue = 0;
		for ( int i = 0; i < _values.size(); i++)	{
			if ( (_values [i]) > maxValue ) {
				maxValue = _values [i];
			}
		}
		return maxValue;
	}

	/**
	* @param _values vector with values
	* @return minimal value of a vector
	*/
	static int getMinValue(QVector<int> _values)
	{
		int minValue = 0;
		for ( int i = 0; i < _values.size(); i++)	{
			if ( (_values [i]) < minValue ) {
				minValue = _values [i];
			}
		}
		return minValue;
	}

	/**
	* @param _values vector with values
	* @return maximal value of a vector
	*/
	static double getMaxValue(QVector<double> _values)
	{
		double maxValue = 0.0;
		for ( int i = 0; i < _values.size(); i++)	{
			if ( (_values [i]) > maxValue ) {
				maxValue = _values [i];
			}
		}
		return maxValue;
	}

	/**
	* @param _values vector with values
	* @return minimal value of a vector
	*/
	static double getMinValue(QVector<double> _values)
	{
		double minValue = 0;
		for ( int i = 0; i < _values.size(); i++)	{
			if ( (_values [i]) < minValue ) {
				minValue = _values [i];
			}
		}
		return minValue;
	}


	/**
	* rounds a number to the given number of decimal places
	* @param _number the number to be rounded
	* @param _places the number of decimal places that the result should have
	* @return the rounded number
	*/
	static double Round (double _number, int _places)
	{
		double v[] = { 1, 10, 1e2, 1e3, 1e4, 1e5, 1e6, 1e7, 1e8 };  // possibly enlarge
		return floor(_number * v[_places] + 0.5) / v[_places];
	}

	/**
	* accumulates the values of input vector: the value of element i is the sum of values of elements 0..i-1 plus current value of i
	* @param _vector
	*/
	static void accumulateNumberVector (QVector<double>& _vector)
	{
		for (int i = 1; i < _vector.size(); i++) {
			_vector[i] += _vector[i-1];
		}
	}

	/**
	* @return average and standard deviation of values in vector
	*/
	static void calculateAverageStdDev(const QVector<double>& _vector, double *_average, double *_stdDev)
	{
		if ( ! (_vector.size() == 0) ) {
			double accumulator = 0.0;
			int numberOfElements = _vector.size();

			for (int i = 0; i < numberOfElements; i++)
				accumulator += _vector[i];

			*_average = accumulator/(double)numberOfElements;
			//*_average = Round(*_average, 2);

			double diffsum_squares = 0.0;
			double diff = 0.0;
			for (int i = 0; i < numberOfElements; i++) {
				if ( _vector[i] != 0 ) {
					diff = _vector[i] - *_average;
					diffsum_squares += diff * diff;
				}
			}
			if ( numberOfElements != 1 ) {
				*_stdDev = sqrt (diffsum_squares / (double)(numberOfElements - 1));
				//*_stdDev = Round(*_stdDev, 2);
			}
			else
				*_stdDev = 0.0;
		}
		else {
			*_average = 0.0;
			*_stdDev = 0.0;
		}
	}

	/**
	* @param _valuesBelongToDataSet indicates if mean, lower quartile and upper quartile should belong to data set
	* @return box plot values: min value, lower quartile, median, upper quartile, max value and list of outliers
	*/
	static void calculateBoxPlotValues(const QVector<double>& _vector, double *_min, double *_lQuartile, double *_median, double *_uQuartile, double *_max, QVector<double>& _outlier/*, bool _valuesBelongToDataSet*/)
	{
		int numberOfValues = _vector.size();

		if ( ! (numberOfValues == 0) ) {
			QMap<double, double> sortedValuesMap; // With QMap, the items are always sorted by key. Value has no matter in this case

			for (int i = 0; i < numberOfValues; i++) {
				sortedValuesMap.insertMulti(_vector[i], _vector[i]); // insertMulti because the same values should not be owerwritten
			}

			QList<double> sortedValues = sortedValuesMap.keys(); // sorted list of values

			*_min = sortedValues[0];
			*_max = sortedValues[numberOfValues - 1];

			calculateMedian(sortedValues, _median);

			calculateLowerAndUpperQuartiles(sortedValues, _lQuartile, _uQuartile);

			double iqr = *_uQuartile - *_lQuartile; // inter quartile region;

			bool ifLowerOutlier = false;
			for (int i = 0; i <= numberOfValues/4 + 1; i++){
				if ( sortedValues[i] < *_lQuartile - iqr ) {
					ifLowerOutlier = true;
					_outlier.append(sortedValues[i]);
				}
			}
			if ( ifLowerOutlier )
				*_min = *_lQuartile - iqr;

			bool ifUpperOutlier = false;
			for (int i = numberOfValues*3/4 - 1; i < numberOfValues; i++){
				if ( sortedValues[i] > *_uQuartile + iqr ) {
					ifUpperOutlier = true;
					_outlier.append(sortedValues[i]);
				}
			}
			if ( ifUpperOutlier )
				*_max = *_uQuartile + iqr;

		}
		else {
			*_min = 0.0;
			*_lQuartile = 0.0;
			*_median = 0.0;
			*_uQuartile = 0.0;
			*_max = 0.0;
		}
	}

	static void calculateMedian(const QList<double>& _list, double *_median)
	{
		int numberOfValues = _list.size();

		if ( numberOfValues - (numberOfValues/2)*2 == 0 ) { // in case of even number of values, for example 6, 7, 15, 36, 39, 40
			*_median = (double)(_list[numberOfValues/2 - 1] + _list[numberOfValues/2]) / 2.0;
		}
		else { // in case of uneven number of values, for example 6, 7, 15, 36, 39, 40, 41
			*_median = (double)_list[numberOfValues/2 ];
		}
	}

	static void calculateLowerAndUpperQuartiles(const QList<double>& _list, double *_lQuartile, double *_uQuartile)
	{
		int numberOfValues = _list.size();

		// Use the median to divide the ordered data set into two halves. Do not include the median into the halves.
		// The lower quartile value is the median of the lower half of the data. The upper quartile value is the median 
		// of the upper half of the data.
		if ( numberOfValues - (numberOfValues/2)*2 == 0 ) { // in case of even number of values 
			calculateMedian(_list.mid(0, numberOfValues/2), _lQuartile);
			calculateMedian(_list.mid(numberOfValues/2), _uQuartile);

		}
		else { // in case of uneven number of values 
			calculateMedian(_list.mid(0, numberOfValues/2), _lQuartile);
			calculateMedian(_list.mid(numberOfValues/2 + 1), _uQuartile);
		}
	}

	/**
	* Author: Laura Skylaki
	* gets the value that corresponds to the x% percentile
	* calculation of percentile with linear interpolation between closest ranks
	* @param _values of a curve
	* @param _percentile to be found
	* @return the point where the x% percentile lies
	*/

	/*static qreal getPercentile (QVector< qreal> _values, double _percentile){
	qSort(_values);
	int rank = qRound((_percentile/100)*(double)(_values.size()));
	return _values.value(rank);
	}*/

	static qreal getPercentile (QVector< qreal> _values, double _percentile){
		qSort(_values);
		// formula: i= n*pi/100 + 0.5, here we subtract 1 because i is supposed to start from 1 rather than 0 (first index of vector)
		double order = (_percentile/100)*(double)(_values.size())+0.5-1;
		if (((double)qRound(order))==order){
			return _values.value(qRound(order));
		}
		// linear interpolation 
		else {
			qreal interpolatedPercentile = _values.value(qFloor(order))+ (order-qFloor(order))*(_values.value(qCeil(order))-_values.value(qFloor(order)));
			return interpolatedPercentile;
		}
	}

	/**
	* overloaded function. searches the maximal x-value and maximal y-value and returns they as point
	*/
	static QwtDoublePoint getMaxValue(QwtArray<QwtDoublePoint> _values)
	{
		double maxXPoint = 0.0;
		double maxYPoint = 0.0;

		for ( int i = 0; i < (int)_values.size(); i++ ) {
			if ( _values[i].x() > maxXPoint )
				maxXPoint = _values[i].x();
			if ( _values[i].y() > maxYPoint )
				maxYPoint = _values[i].y();
		}

		return QwtDoublePoint(maxXPoint, maxYPoint);
	}	

	static QwtDoublePoint getMinValue(QwtArray<QwtDoublePoint> _values)
	{
		double minXPoint = 0.0;
		double minYPoint = 0.0;

		for ( int i = 0; i < (int)_values.size(); i++ ) {
			if ( _values[i].x() < minXPoint )
				minXPoint = _values[i].x();
			if ( _values[i].y() < minYPoint )
				minYPoint = _values[i].y();
		}

		return QwtDoublePoint(minXPoint, minYPoint);
	}	

	/**
	* overloaded function. searches the maximal x-value and maximal y-value and returns they as point
	*/
	static QwtDoublePoint getMaxValue(QVector< QVector<QPointF> > _values)
	{
		double maxXPoint = 0.0;
		double maxYPoint = 0.0;

		QVector<QPointF> currentPointVector;
		for ( int i = 0; i < _values.size(); i++ ) {
			currentPointVector = _values[i];
			for ( int j = 0; j < (int)currentPointVector.size(); j++ ) {
				if ( currentPointVector[j].x() > maxXPoint )
					maxXPoint = currentPointVector[j].x();
				if ( currentPointVector[j].y() > maxYPoint )
					maxYPoint = currentPointVector[j].y();
			}
		}

		return QwtDoublePoint(maxXPoint, maxYPoint);
	}

	/**
	* overloaded function. searches the minimal x-value and maximal y-value and returns they as point
	*/
	static QwtDoublePoint getMinValue(QVector< QVector<QPointF> > _values)
	{
		double minXPoint = 0.0;
		double minYPoint = 0.0;

		QVector<QPointF> currentPointVector;
		for ( int i = 0; i < _values.size(); i++ ) {
			currentPointVector = _values[i];
			for ( int j = 0; j < (int)currentPointVector.size(); j++ ) {
				if ( currentPointVector[j].x() < minXPoint )
					minXPoint = currentPointVector[j].x();
				if ( currentPointVector[j].y() < minYPoint )
					minYPoint = currentPointVector[j].y();
			}
		}

		return QwtDoublePoint(minXPoint, minYPoint);
	}
};

#endif

