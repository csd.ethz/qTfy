/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef FILEINFOARRAY_H
#define FILEINFOARRAY_H

#include "qtfydata/fileinfo.h"
#include "qtfybackend/pictureindex.h"

#include <QHash>
#include <QVector>
#include <QSharedPointer>

/**
@author Bernhard

        This class contains an array of FileInfo objects and provides access to it.
        The most important functions are exists() and calcSeconds().
*/

class FileInfoArray
{
public:

        /**
         * constructs a default array (nothing set)
         */
        FileInfoArray ()
		{}

        ~FileInfoArray();

        /* *
         * resizes the attribute array
         * @param _newSize the new size of the array (should be number of timepoints times number of wavelengths)
         */
//	void resize (int _newSize);

        /**
         * returns whether an info struct exists for that timepoint, z-index and wavelength
         * @param _timePoint the query timepoint
		 * @param _zIndex the query z-index (-1 => checks for z-index - IMPORTANT: should be avoided due to bad performance)
         * @param _waveLength the query wavelength (-1 => checks for any wavelength at that timepoint)
         * @return the index within the internal QValueVector (> 0) or 0, if nothing found
         */
        bool exists (int _timePoint, int _zIndex, int _waveLength) const;

        /**
         * returns the FileInfo for the picture with _timePoint, _zIndex and _waveLength
         * @param _timePoint the query timepoint
		 * @param _zIndex the query z-index
         * @param _waveLength the query wavelength
         * @return a FileInfo object that contains the information for the picture at _timePoint, _zindex and _waveLength or NULL if nothing was found
         */
        const FileInfo* find (int _timePoint, int _zIndex, int _waveLength) const;

        /**
         * inserts a new FileInfo object with the specified settings into Array at
         * @param _fileName the complete filename of the picture
         * @param _timePoint the timepoint of the picture
		 * @param _zIndex the z-index of the picture
         * @param _waveLength the wavelength of the picture
         * @param _creationTime the creation time of the picture
         * @param _firstTimeSet reference parameter: true -> first creation time was updated with the current timepoint, false -> nothing happened
         * @return success
         */
        bool insert (const QString& _fileName, int _timePoint, int _zIndex, int _waveLength, const QDateTime& _creationTime, bool &_firstTimeSet);

        /* *
         * returns the FileInfo with index _index (array relative!)
         * => _index has to be calculated by caller correctly!
         * only used by PictureIndex, where the _index parameter was previously obtained by exists()
         * @param _index the internal index
         * @return a FileInfo object, taken from the array at _index
         */
//	FileInfo *at (int _index) const
//		{return infoArray.find (_index);}

  //      /**
  //       * returns the index in the array for _timePoint and _waveLength
  //       * @param _timePoint the timepoint
  //       * @param _waveLength the wavelength
  //       * @return the internal array index
  //       */
		//int calcIndex (int _timePoint, int _zIndex, int _waveLength) const
		//{	
		//	if (_waveLength == -1)		//take 0, as this should always exist
		//		return _timePoint * (MAX_WAVE_LENGTH + 1);
		//	else
		//		return _timePoint * (MAX_WAVE_LENGTH + 1) + _waveLength;
		//}

        /**
         * returns the timepoint, included in the filename _d
         * @param _d the filename of the picture (containing section _t00000)
         * @return the timepoint within the filename
         */
		static int CalcTimePointFromFilename (const QString& _d) {
			// 5 digits assumed
			int pos_t = _d.indexOf (TIMEPOINT_MARKER);
			return _d.mid(pos_t + 2, 5).toInt();
		}

        /**
         * returns the wavelength, included in the filename _d
         * @param _d the filename of the picture (containing section _w0)
         * @return the wavelength within the filename
         */
		static int CalcWaveLengthFromFilename (const QString& _d) {
			//BS 11.03.2010: wavelength is between _w and ., but not necessarily 1 digit
			int pos_w = _d.indexOf (WAVELENGTH_MARKER);
			int pos_point = _d.indexOf (".", pos_w);
			int len = pos_point - pos_w - 2;
			return _d.mid (pos_w + 2, len).toInt();
		}

        /**
         * returns the z-index, included in the filename _d
         * @param _d the filename of the picture
         * @return the z-index within the filename or -1 if no _z exists or number is invalid
         */
		static int CalcZIndexFromFileName(const QString& _d) {
			// Find z marker
			int index = _d.lastIndexOf("_z");
			if(index == -1)
				return -1;

			// Try 3 digits
			QString sub = _d.mid(index + 2, 3);
			bool ok;
			int ret = sub.toInt(&ok);
			if(!ok) 
				return -1;

			return ret;
		}

        /**
         * returns the acquistion time for the picture at timePoint with _waveLength
         * @param _timePoint the timepoint of the picture
		 * @param _zIndex the z-index of the picture (-1 => take first existing, usually 0)
         * @param _waveLength the wavelengt of the picture (-1 => take first existing, usually 0)
         * @param _round whether for a nonexisting timepoint the NEXT existing one should be taken (default = false)
         * @return a QDateTime object with the acquisition time of the image, or empty if the timepoint does not exist
         */
        QDateTime getRealTime (int _timePoint, int _zIndex = -1, int _waveLength = -1, bool _roundUp = false) const;

        /**
         * returns the seconds between the times at the two time points
         * @param _timePoint1 the timepoint 1
         * @param _timePoint2 the timepoint 2
         * @param _round whether timepoint 2 should be decremented to receive the next lower (but really existing) one
		 * @param _zIndex1 the z-index at timepoint 1 (-1 => 1 assumed)
		 * @param _zIndex2 the z-index at timepoint 2 (-1 => 1 assumed)
         * @param _waveLength1 the wavelength at timepoint 1 (-1 => 0 assumed)
         * @param _waveLength2 the wavelength at timepoint 2 (-1 => 0 assumed)
         * @return the difference in seconds between timepoint 1 and 2
         */
        int calcSeconds (int _timePoint1, int _timePoint2, bool _round = true, int _zIndex1 = -1, int _zIndex2 = -1, int _waveLength1 = -1, int _waveLength2 = -1) const;

        /**
         * returns the timepoint that is _seconds away from _base
         * if there is none for this time, the next lower/higher is returned
         * @param _base the base timepoint (usually the first timepoint)
         * @param _seconds the difference in seconds for which the timepoint should be calculated
         * @param _lower whether the seconds should be decremented, if a timepoint does not exist (otherwise incremented)
         * @return the timepoint, if anyone found (otherwise -1)
         */
        int calcTimePoint (int _base, long int _seconds, bool _lower) const;

        /**
         * Returns the timepoint that corresponds to the provided absolute time. \n
         * Performs a simple linear search on the local array.
         * @param _startTp the timepoint from where the search should start
         * @param _endTp the timepoint until which the search should be performed
		 * @param _zIndex the z-index of the picture
         * @param _wavelength the wavelength which should be searched
         * @param _absTime the absolute time
         * @return the timepoint that has _absTime as its absolute time; -1 if none such exists
         */
        int findTimePointByAbsTime (int _startTp, int _endTp, int _zIndex, int _wavelength, const QDateTime &_absTime) const;

        /**
         * Returns the number of really existing pictures (of wavelength 0) between the two specified timepoints
         * @note Slow method!
         * @param _startTp the start timepoint
         * @param _endTp the end timepoint
         * @return the number of real timepoints between _startTp and _endTp (both included)
         */
        int getExistingTimepointsBetween (int _startTp, int _endTp) const;


        /////empty FileInfo structure; returned by find() if the queried file does not exist
        //FileInfo dummy;

        ///**
        // * @return the array of FileInfo objects
        // */
        //const QHash<int, FileInfo>& getArray() const
        //        {return infoArray;}

        /**
         * sets the first and the last timepoint (called by logfilereader)
         * @param _ftp the first timepoint of the experiment
         * @param _ltp the last timepoint of the experiment
         */
        void setTimepoints (int _ftp, int _ltp);

private:

	/**
	 * Helper function that transforms z- and wavelength-indexes into ranges. If the z-index (or wavelength) is
	 * set (i.e. not equal to -1) the corresponding range will contain only that single value. Otherwise it will
	 * contain all values from 0 to MAX_ZINDEX (or MAX_WAVE_LENGTH).
	 */
	static void indexToRange(int _zIndex, int _waveLength, int& zStart, int& zStop, int& wlStart, int& wlStop) {
		if(_zIndex != -1) {
			zStart = _zIndex;
			zStop = _zIndex;
		}
		else {
			zStart = 0;
			zStop = MAX_ZINDEX;
		}
		if(_waveLength != -1) {
			wlStart = _waveLength;
			wlStop = _waveLength;
		}
		else {
			wlStart = 0;
			wlStop = MAX_WAVE_LENGTH;
		}
	}

    ///the array that contains the file information as FileInfo objects for each file (ToDo: maybe change to vector<FileInfo*> and Hash<PicIndex,int> to make e.g. getRealTime() or exists() more efficient?)
    QHash<PictureIndex, FileInfo*> infoArray;

    ///stores the time differences between the first and each other timepoint for fast access
    ///a new value is added with every call of insert()
    ///with its aid, calcSeconds() is very fast
    ///note: needs to be calculated only once, at the beginning
	///OH 06.07.2011: [changed to QHash, keys are indices as calculated by calcIndex(),] values are distances in seconds to first timepoint
    QHash<PictureIndex, int> times;

    ///holds the creation time of the first timepoint
    QDateTime firstTime;

    ///hold the first/last timepoint for this array
    ///set when creating the array
    int firstTimepoint;
    int lastTimepoint;

};

#endif // FILEINFOARRAY_H
