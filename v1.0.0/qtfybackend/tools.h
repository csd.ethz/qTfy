/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TOOLS_H
#define TOOLS_H
 
// PROJECT
#include "qtfydata/systeminfo.h"
#include "qtfydata/tree.h"
#include "qtfyclt/celllineagetree.h"


// C standard library includes
#include <math.h>

// STL
#include <fstream>

// QT
#include <QVector>
#include <QString>
#include <QList>
#include <QDate>
#include <QCursor>
#include <qrgb.h>


// Forward declaration
class TTTPositionManager;



///a format for dates displaying date and time
const QString COMPLETE_DATE_FORMAT = "yyyy/MM/dd; hh:mm:ss";

///used for confusion... see usage for details
#ifdef Q_WS_WIN
    const QString XDFILES_PATH = "systemauth/"; //in user dir
#else
    const QString XDFILES_PATH = ".systemauth/"; //in home dir
#endif

const QString XDFILES_DDFILENAME = "dd_auth";
const QString XDFILES_IDFILENAME = "id_ref";
//const QString XDFILES_COMMENT = "#(C)opyright TDX Software, 2010\n#\n# DO NOT CHANGE THE CONTENTS OF THIS FILE!\n# THIS MIGHT DAMAGE YOUR CONFIGURATION AND CAUSE THE SYSTEM NOT TO START PROPERLY.";
const int XDFILES_MAXAUFRUFE = 10000;


/**
	@author Bernhard Schauberger and Konstantin Azadov
	
	
	This class holds many functions for diverse purposes...
*/

class Tools{

public:
	Tools();
	
	~Tools();
	
	///the starting time of a time measuring process
	static QTime measureTime;
	
	///whether the time measuring process was started
	static bool timingStarted;
	
	
	/**
	 * Parse csv data. For numeric data, consider using Tools::parseCsvDataDouble() instead.
	 * @param data string containing csv data.
	 * @param sep separator used in csv file.
	 * @param outDataRows data, numbers are automatically parsed.
	 * @param outColumnNames if not null, entries of first row will be returned as column names.
	 * @param columnsToFind can be used to provide column names to look for, the corresponding indexes will be stored in outColumnsToFindIndexes.
	 * @param outColumnsToFindIndexes if column names to look for were provided in columnsToFind, their column indexes in the returned data will be stored here (or -1 for not found columns).
	 * @return 0 if successful or error code.
	 */
	static int parseCsvData(QString& data, 
							const QString sep, 
							QVector<QVector<QVariant>>& outDataRows, 
							QVector<QString>* outColumnNames = nullptr, 
							const QVector<QString>* columnsToFind = nullptr,
							QVector<int>* outColumnsToFindIndexes = nullptr)
	{
		QTextStream s(&data, QIODevice::ReadOnly);
		s.setLocale(QLocale("C"));

		// Parse data line by line
		bool parseHeader = outColumnNames != nullptr;
		int numColumns = -1;
		QVector<QVariant> firstDataLine;
		while(!s.atEnd()) {
			// Read line, remove whitespaces
			QString line = s.readLine().trimmed();
			if(line.isEmpty())
				continue;

			// Split it
			QList<QString> split = line.split(sep, QString::KeepEmptyParts, Qt::CaseSensitive);
			if(numColumns == -1) {
				numColumns = split.size();
			}
			else {
				if(split.size() != numColumns)
					// Line has invalid number of columns
					return 1;
			}

			// Parse line
			if(parseHeader) {
				// Parse header
				parseHeader = false;
				outColumnNames->resize(split.size());
				for(int c = 0; c < split.size(); ++c) {
					split[c] = split[c].trimmed();
					(*outColumnNames)[c] = split[c];
				}
			}
			else {
				// Parse data row
				outDataRows.push_back(QVector<QVariant>());
				QVector<QVariant>& newLine = outDataRows.back();
				newLine.resize(split.size());
				for(int c = 0; c < split.size(); ++c) {
					split[c] = split[c].trimmed();
					if(firstDataLine.size()) {
						// Use format of first line
						if(firstDataLine[c].type() == QVariant::Double) {
							// Treat as double
							bool ok = false;
							double d = split[c].toDouble(&ok);
							if(!ok)
								// Value should be double but cannot be parsed
								return 2;
							newLine[c] = d;
						}
						else {
							// Must be string
							newLine[c] = split[c];
						}
					}
					else {
						// Determine format
						bool isDouble = false;
						double d = split[c].toDouble(&isDouble);
						if(isDouble)
							// Treat as double
							newLine[c] = d;
						else
							// Treat as string
							newLine[c] = split[c];
					}
				}
				if(firstDataLine.size() == 0)
					firstDataLine = newLine;
			}
		}

		// Look for columns
		if(outColumnNames && columnsToFind && outColumnsToFindIndexes) {
			outColumnsToFindIndexes->resize(columnsToFind->size());
			for(int c = 0; c < columnsToFind->size(); ++c) {
				(*outColumnsToFindIndexes)[c] = outColumnNames->indexOf((*columnsToFind)[c]);
			}
		}	

		return 0;
	}

	/**
	 * Parse csv data that contains only doubles (i.e. only numeric values).
	 * @param data string containing csv data.
	 * @param sep separator used in csv file.
	 * @param outDataRows data, numbers are automatically parsed.
	 * @param outColumnNames if not null, entries of first row will be returned as column names.
	 * @param columnsToFind can be used to provide column names to look for, the corresponding indexes will be stored in outColumnsToFindIndexes.
	 * @param outColumnsToFindIndexes if column names to look for were provided in columnsToFind, their column indexes in the returned data will be stored here (or -1 for not found columns).
	 * @return 0 if successful or error code.
	 */
	static int parseCsvDataDouble(QString& data, 
							const QString sep, 
							QVector<QVector<double>>& outDataRows, 
							QVector<QString>* outColumnNames = nullptr, 
							const QVector<QString>* columnsToFind = nullptr,
							QVector<int>* outColumnsToFindIndexes = nullptr)
	{
		QTextStream s(&data, QIODevice::ReadOnly);
		s.setLocale(QLocale("C"));

		// Parse data line by line
		bool parseHeader = outColumnNames != nullptr;
		int numColumns = -1;
		while(!s.atEnd()) {
			// Read line, remove whitespaces
			QString line = s.readLine().trimmed();
			if(line.isEmpty())
				continue;

			// Split it
			QList<QString> split = line.split(sep, QString::KeepEmptyParts, Qt::CaseSensitive);
			if(numColumns == -1) {
				numColumns = split.size();
			}
			else {
				if(split.size() != numColumns)
					// Line has invalid number of columns
					return 1;
			}

			// Parse line
			if(parseHeader) {
				// Parse header
				parseHeader = false;
				outColumnNames->resize(split.size());
				for(int c = 0; c < split.size(); ++c) {
					split[c] = split[c].trimmed();
					(*outColumnNames)[c] = split[c];
				}
			}
			else {
				// Parse data row
				outDataRows.push_back(QVector<double>());
				QVector<double>& newLine = outDataRows.back();
				newLine.resize(split.size());
				for(int c = 0; c < split.size(); ++c) {
					split[c] = split[c].trimmed();

					// Treat as double
					bool ok = false;
					double d = split[c].toDouble(&ok);
					if(!ok)
						// Value should be double but cannot be parsed
						return 2;
					newLine[c] = d;
				}
			}
		}

		// Look for columns
		if(outColumnNames && columnsToFind && outColumnsToFindIndexes) {
			outColumnsToFindIndexes->resize(columnsToFind->size());
			for(int c = 0; c < columnsToFind->size(); ++c) {
				(*outColumnsToFindIndexes)[c] = outColumnNames->indexOf((*columnsToFind)[c]);
			}
		}	

		return 0;
	}


	/**
	 * Open TTT file and corresponding csv files with quantification. Returns Tree object with tracking data only and CellLineageTree object
	 * with tracking data as well as quantification data. Note that changing the tracking data later can lead to inconsistencies.
	 * Returns empty string in case of no error, or error description otherwise.
	 */
	static QString readTTTFileWithCsvQuantification(const QString& tttFilePath, const TTTPositionManager *tttpm, Tree& outTree, CellLineageTree& outTreeAndQuantification);

	/**
	 * Write TTT file and corresponding csv files with quantification.
	 * Returns empty string in case of no error, or error description otherwise.
	 */
	static QString writeTTTFileWithCsvQuantification(const QString& tttFilePath, const TTTPositionManager* tttpm, Tree& tree, const CellLineageTree& treeAndQuantification);

	/**
	 * Writes the corresponding csv files with quantification.
	 * Returns empty string in case of no error, or error description otherwise.
	 */
	static QString writeCsvQuantification(const QString& tttFilePath, const CellLineageTree& treeAndQuantification);

	/**
	 * Look in the directory where the tree is located for csv files containing the Tree name in their filename
	 * Returns true if a file has been found (but does not check whether the contents of the file are valid)
	 */
	static bool isTreeQuantifiedBefore(const QString& tttFilePath);


//	/**
//	 * reads a text file and returns the contents
//	 * @param _filename the (absolute) path of the filename
//	 * @return the contents of the file (QString::null, if the file does not exist)
//	 */
//	static QString readTextFile (QString _filename);
//	
//	/**
//	 * writes a text file with the given name and content
//	 * @param _filename the complete filename
//	 * @param _content the content string
//	 * @return success
//	 */
//	static bool writeTextFile (const QString &_filename, const QString &_content);
//	
//	/**
//	 * calculates _base ^ _exp iteratively on integers
//	 * the advantage of the fast integer arithmetic is used, and rounding is not necessary
//	 * yet, intended for rather small values as all parameters are of type int
//	 * @param _base the base
//	 * @param _exp the exponent/power
//	 * @return _base ^ _exp
//	 */
//	static int pow (int _base, int _exp);

	/**
	 * Returns the position number in a string by looking for POSITION_MARKER
	 * and returning the next 3 (or 4 if available) digits
	 * @param _posString the string to search in
	 * @return the (three/four-letter) position index if found, empty QString otherwise
	 */
	static QString getPositionNumberFromString(const QString &_posString);
	
	/**
	 * extracts the position index from the provided filename/folder name
	 * it searches for the pattern in POSITION_MARKER
	 *
	 * NOTE: it returns different values fro TTT and TTTStats
	 *       TTT: 001 or 0001 (same as getPositionNumberFromString())
	 *       TTTStats: 080527MR2_p001 or 080527MR2_p0001
	 * @param _filename the filename (of a ttt file, jpg file, ...)
	 * @return the (three/four-letter) position index, if found; QString::null otherwise
	 */
	static const QString getPositionIndex (const QString &_filename);

	/**
	 * checks if a number is NaN, +/-Inf and returns true if not
	 */

	static bool isValidNumber (double _value) ;

//	///**
//	// * rounds a number to the given number of decimal places
//	// * @param _number the number to be rounded
//	// * @param _places the number of decimal places that the result should have
//	// * @return the rounded number
//	// */
//	//static double Round (double _number, int _places);
//	
//	/**
//	 * Decides whether two floats are equal, rounding them to a given number of decimal places before
//	 * @param _number1 the first number
//	 * @param _number2 the second number
//	 * @param _places the number of decimal places
//	 * @return 0 => equal, -1 => nr1 < nr2, 1 => nr1 > nr2
//	 */
//	static int floatEquals (float _number1, float _number2, int _places = 0);
//	
//	/**
//	 * starts measuring an arbitrary time (simply stores the current time)
//	 */
//	static void startTimeMeasure();
//	
//	/**
//	 * stops measuring an arbitrary time (by doing nothing :-))
//	 * @return the difference between the current time and the time stored by startTimeMeasure() in SECONDS
//	 */
//	static int stopTimeMeasure();
//	
	/**
	 * joins two value vectors of arbitrary type T
	 * @param _vect1 the first vector
	 * @param _vect2 the second vector (is appended to the first vector, at the end)
	 * @return the combined vector (_vect1 ++ _vect2)
	 */
	template <class T>
    static QVector<T> joinValueVectors(const QVector<T> &_vect1, const QVector<T> &_vect2);
	
//	///**
//	// * joins two pointer lists of arbitrary type T
//	// * @param _list1 the first pointer list
//	// * @param _list2 the second pointer list (is appended to the first list, at the end)
//	// * @return the combined pointer list (_list1 ++ _list2)
//	// */
//	//template <class T>
//	//static Q3PtrList<T> joinPointerLists(/*const*/ Q3PtrList<T> &_list1, /*const*/ Q3PtrList<T> &_list2);

	/**
	 * joins two lists of arbitrary type T
	 * @param _list1 the first list
	 * @param _list2 the second list (is appended to the first list, at the end)
	 * @return the combined list (_list1 ++ _list2)
	 */
	template <class T>
	static QList<T> joinLists(/*const*/ QList<T> &_list1, /*const*/ QList<T> &_list2);
	
//	/**
//	 * reads the log file for the provided position manager
//	 * @param _tttpm the position manager (must have the picture directory and basename set)
//	 * @param _ltp reference parameter: the last timepoint, is set
//	 * @param _ftp reference parameter: the first timepoint, is set
//	 * @param _pictureSuffix the suffix of the pictures (e.g. 'jpg', without point)
//	 * @param _showMessage whether a message box should be displayed if the log file is corrupt
//	 * @return reading success (whether the log file is correct (up to date and could be read))
//	 */
//	static bool readLogFile (TTTPositionManager *_tttpm, int &_ltp, int &_ftp, /*const QString &_pictureSuffix,*/ bool _showMessage = true);
//	
//	
//	/**
//	 * exports all trees of the provided position into images
//	 * @param _tttpm the position manager of which the trees should be exported
//	 * @param _showMessage whether a "finished" message should be displayed
//	 * @return destination folder or empty string in case of error.
//	 */
//	static QString exportAllTreesOfPositionToImages (TTTPositionManager *_tttpm, bool _showMessage = true);
//
//    /**
//     * extract the value of an element inside a DOM document
//     * it just looks for the first occurence and returns this, if there are multiple occurrences
//     * for further details, confer the code
//     * @param _dom the DOM document to be searched for
//     * @param _elementName the name of the element that is desired
//     */
//    static QString extractValueFromDOMDocument (const QDomDocument &_dom, const QString &_elementName);
//
//    /**
//     * extract the value of an element inside a DOM element (recursive)
//     * it just looks for the first occurence and returns this, if there are multiple occurrences
//     * for further details, confer the code
//     * @param _domEl the DOM element to be searched for
//     * @param _elementName the name of the element that is desired
//     */
//    static QString extractValueFromDOMElement (const QDomElement &_domEl, const QString &_elementName);
//
//    /**
//     * sets a value in a DOM document; the element with the provided name must exist
//     * @param _dom the DOM document to be edited
//     * @param _domEl the DOM element to be searched for
//     * @param _elementName the name of the element that should be set (must be existing somewhere in the document)
//     * @param _elementValue the value to be set for the element
//     * @return true, if an element with the provided name was found; false otherwise
//     */
//    static bool setValueInDOMDocument (const QDomDocument &_dom, const QString &_elementName, const QString &_elementValue);
//
//    /**
//     * sets a value in a DOM element; the element with the provided name must exist (recursive)
//     * @param _domEl the DOM element to be searched for
//     * @param _elementName the name of the element that should be set (must be existing somewhere in the document)
//     * @param _elementValue the value to be set for the element
//     * @return true, if an element with the provided name was found; false otherwise
//     */
//    static bool setValueInDOMElement (const QDomElement &_domEl, const QString &_elementName, const QString &_elementValue);
//
//    /**
//     * creates a circle shaped cursor with the given radius, color and line thickness
//     * @param _diameter the diameter of the circle (tested up to 150px on Linux & Windows, no problem encountered)
//     * @param _color the color of the circle line
//     * @param _thickness the thickness of the circle line
//     * @param the newly created cursor
//     */
//    static QCursor createCircleCursor (int _diameter, QColor _color, int _thickness);
//
//	/**
//	* Convert a string into an integer
//	* @param _str the string
//	* @param _i the integer where value will be saved
//	* @param _base base
//	* @return true if successful
//	*/
//	static bool stringToInt(const QString& _str, int& _i, int _base = 10);
//
	/**
	 * Display a messagebox with a button to open a specified folder (useful for exports of any kind)
	 * @param _message the message to display
	 * @param _caption the caption to display
	 * @param _folder the absolute path of the directory to be opened
	 * @param _folderIncludesFile if set to true, folder will be opened and file will be selected
	 * @param _parent parent window if desired
	 */
	static void displayMessageBoxWithOpenFolder(QString _message, QString _caption, QString _folder, bool _folderIncludesFile = false, const QWidget* _parent = 0);

	/**
	 * Get number of digits for position index strings
	 * @return number of digits or -1 if it is not set
	 */
	static int getNumOfPositionDigits();

	/**
	 * Set number of digits for position index strings
	 * @param number of digits
	 */
	static void setNumOfPositionDigits(int _numDigits);

//	/**
//	 * Get number of digits for wavelength strings
//	 * @return number of digits or -1 if it is not set
//	 */
//	static int getNumOfWavelengthDigits();
//
//	/**
//	 * Set number of digits for wavelength strings
//	 * @param number of digits
//	 */
//	static void setNumOfWavelengthDigits(int _numDigits);
//
//	/**
//	 * Get number of digits for Z-indexes
//	 * @return number of digits, -1 if it is not set or 0 if images have no z-index 
//	 */
//	static int getNumOfZDigits();
//
//	/**
//	 * Set number of digits for Z-indexes
//	 * @param number of digits
//	 */
//	static void setNumOfZDigits(int _numDigits);
//
	/**
	 * Convert integer position number into string representation (e.g. 32 becomes '0032' or '032') with respect to number of digits for positions
	 * @param _posNum position number as integer
	 * @return position number as string
	 */
	static QString convertIntPositionToString(int _posNumber);

//	// ----------Oliver----------
//	// Not used now
//    ///**
//    // * encodes a date into a format that can be decoded by decodeDate()
//    // */
//    //static QString encodeDate (int _year, int _month, int _day);
//
//	// ----------Oliver----------
//	// Not used now
//    ///**
//    // * decodes a date that was encoded by encodeDate()
//    // * @return a string representing the date in the format "yyyyMMdd"
//    // */
//    //static QString decodeDate (const QString &_codedDate);
//
//    //kein Kommentar... (false -> fail)
//    static bool pruefeAnfangsDaten();
//
//    //kein Kommentar... (false -> fail)
//    static bool schreibeAnfangsDaten();
//
//	// ----------Oliver----------
//    /**
//     * read id file
//     * @param _fileName complete filename of id file
//     * @param _installDate id will be put here
//     * @return true if successful
//     */
//	static bool readIDFile(const QString& _fileName, QDate& _installDate);
//	
//	// ----------Oliver----------
//    /**
//     * read dd file
//     * @param _fileName complete filename of dd file
//     * @param _ddDate dd will be put here
//	 * @param _tttStartCount startcount will be put here
//     * @return true if successful
//     */
//	static bool readDDFile(const QString& _fileName, QDate& _ddDate, int& _tttStartCount);
//
//	// ----------Oliver----------
//    /**
//     * write id file
//     * @param _fileName complete filename of id file
//     * @param _id id date to save
//     * @return true if successful
//     */
//	static bool writeIDFile(const QString &_fileName, QDate _id);
//
//	// ----------Oliver----------
//    /**
//     * write dd file
//     * @param _fileName complete filename of dd file
//     * @param _dd dd date to save
//	 * @param _useCount use count to save
//     * @return true if successful
//     */
//	static bool writeDDFile(const QString &_fileName, QDate _dd, int _useCount);
//
//	// ----------Oliver----------
//    /**
//	* get path to dd and id files
//	* @return path if successful or empty string
//	*/
//	static QString getXDFilesPath();
//
//	// Return a color table for grayscale QImage pictures using image format Indexed8
//	static QVector<QRgb> getColorTableGrayIndexed8()
//	{
//		static QVector<QRgb> colorTableGrayIndexed8;
//		if(colorTableGrayIndexed8.size() == 0) {
//			colorTableGrayIndexed8.resize(256);
//			for(unsigned int i = 0; i < colorTableGrayIndexed8.size(); ++i)
//				colorTableGrayIndexed8[i] = qRgb(i,i,i);
//		}
//
//		return colorTableGrayIndexed8;
//	}
//
//	/**
//	 * Apply background correction.
//	 * @param originalImage image to apply background correction to.
//	 * @param backgroundImage 16bit background image, must have same size as originalImage.
//	 * @param gainImage 16bit gain image, must have same size as originalImage.
//	 * @return 8-bit background corrected image or null-image if error.
//	 */
//	static QImage applyBackgroundCorrection(const QImage& originalImage, const unsigned short* backgroundImage, const unsigned short* gainImage);
//
////        /**
////         * reads the creation date of the picture provided by filename
////         * it tries various ways, if none succeeds, an invalid time is returned
////         */
////        static QDateTime readPictureCreationDate (const QString &_pictureFilename);
//
private:

	// Remember XD files path
	static QString xdPath;

	// Number of digits for position, wavelength and z-index
	static int numDigitsForPosIndex;
	static int numDigitsForWlIndex;
	static int numDigitsForZIndex;
};


 template <class T>
QVector<T> Tools::joinValueVectors(const QVector<T> &_vect1, const QVector<T> &_vect2)
{
	QVector<T> result (_vect1);

	//for (int i = 0; i < _vect2.size(); i++){
	//	result.push_back(_vect2[i]);
	//}
	result += _vect2;

	return result;
}

////template <class T>
////Q3PtrList<T> Tools::joinPointerLists(/*const*/ Q3PtrList<T> &_list1, /*const*/ Q3PtrList<T> &_list2) // with 'const' it doesn't work
////{
////	Q3PtrList<T> result (_list1);
////	
////        for (uint i = 0; i < _list2.count(); i++ )
////		result.append(_list2.at(i));
////		
////	return result;
////}
//
template <class T>
QList<T> Tools::joinLists(/*const*/ QList<T> &_list1, /*const*/ QList<T> &_list2) // with 'const' it doesn't work
{
	QList<T> result (_list1);
	
    for (int i = 0; i < _list2.count(); i++ )
		result.append(_list2.at(i));
		
	return result;
}


#endif
