/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PICTUREINDEX_H
#define PICTUREINDEX_H


#include "qtfydata/systeminfo.h"

#include <QDateTime>

/**
@author Bernhard

	This structure is the record of a timepoint, wavelength and z-index attribute and various
	operations, including receiving the real timepoint of the picture,
	as every picture is always indicated by timepoint, z-index and wavelength, which are used 
	to define a total order for picture indexes.
*/

struct PictureIndex
{
	/**
	 * constructs a default PictureIndex object
	 */
	PictureIndex () : TimePoint (-1), WaveLength (-1), zIndex(-1) 
	{}
	
	/**
	 * constructs a PictureIndex object with the given information
	 * @param _timePoint the timepoint of the picture
	 * @param _zIndex the z-index of the picture
	 * @param _waveLength the wavelength of the picture
	 */
	PictureIndex (int _timePoint, int _zIndex, int _waveLength) : TimePoint (_timePoint), WaveLength (_waveLength), zIndex(_zIndex) 
	{}
	
	/**
	 * returns the acquistion time of the image associated with this object
	 * @param _files the file info array that contains the desired information
	 * @return a QDateTime object with the acquisition time
	 */
	//const QDateTime absTime (FileInfoArray *_files) const
	//{	
	//	if (_files->exists (TimePoint, WaveLength/*, zIndex*/))
	//		return _files->find (TimePoint, WaveLength/*, zIndex*/).CreationTime;
	//	return QDateTime();
	//}
	
	///**
	// * returns the complete filename of the image associated with this object
	// * @param _files the file info array that contains the desired information
	// * @return a QString with the absolute path for this picture
	// */
	//const QString FileName (FileInfoArray *_files) const
 //               {	if (_files->exists (TimePoint, WaveLength))
 //                               return _files->find (TimePoint, WaveLength).FileName;
	//	}
	
	
	bool operator== (const PictureIndex &_pi) const { 
		return TimePoint == _pi.TimePoint && zIndex == _pi.zIndex && WaveLength == _pi.WaveLength;
	}

	bool operator!= (const PictureIndex &_pi) const { 
		return (! (*this == _pi));
	}
	
	bool operator< (const PictureIndex &_pi) const { 
		return (TimePoint < _pi.TimePoint) || 
			(TimePoint == _pi.TimePoint && zIndex < _pi.zIndex) ||
			(TimePoint == _pi.TimePoint && zIndex == _pi.zIndex && WaveLength < _pi.WaveLength);
	}

	bool operator> (const PictureIndex &_pi) const	{ 
		return (TimePoint > _pi.TimePoint) || 
			(TimePoint == _pi.TimePoint && zIndex > _pi.zIndex) ||
			(TimePoint == _pi.TimePoint && zIndex == _pi.zIndex && WaveLength > _pi.WaveLength);
	}

	bool operator<= (const PictureIndex &_pi) const	{ 
		return ((*this == _pi) || (*this < _pi));
	}

	bool operator>= (const PictureIndex &_pi) const	{ 
		return ((*this == _pi) || (*this > _pi));
	}
	
	
	PictureIndex& operator= (const PictureIndex &_pi) {
		//protection from self-assignment
		if (this != &_pi) {			
			TimePoint = _pi.TimePoint;
			WaveLength = _pi.WaveLength;
			zIndex = _pi.zIndex;
		}
		
		return *this;
	}

	///the timepoint of the picture
	int TimePoint;
	
	///the z-index of the picture
	int zIndex;

	///the wavelength of the picture
	int WaveLength;	
};

/**
 * Hash function.
 */
inline uint qHash(const PictureIndex& pi) {
	return qHash(pi.TimePoint) ^ qHash(pi.WaveLength) ^ qHash(pi.zIndex);
}

#endif
