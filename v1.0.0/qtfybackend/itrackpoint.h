/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef itrackpoint_h__
#define itrackpoint_h__

#include <QString>
#include <QVariant>
#include <QString>

// Forward declarations
class ITrack;

/**
* @author Oliver Hilsenbeck
*
* Interface for classes representing a trackpoint in a cell lineage tree
 */
class ITrackPoint  {

public:

	/**
	 * @return x coordinate in global micrometer coordinates
	 */
	virtual float getX() const = 0;

	/**
	 * @return y coordinate in global micrometer coordinates
	 */
	virtual float getY() const = 0;

	/**
	 * @return timepoint
	 */
	virtual int getTimePoint() const = 0;

	/**
	 * check for wavelength
	 * @param _waveLength the wavelength
	 * @return true if wavelength is set 
	 */
	virtual bool checkForWavelength(int _waveLength) const = 0;

	/**
	 * @return addition bits for this tp
	 * e.g. used for special tracking keys
	 */
	virtual short int getAddonBits(int bitback) const = 0;

	/**
	 * @return track this trackpoint belongs to
	 */
	virtual ITrack* getITrack() const = 0;

	/**
	 * Return the number of the position where this trackpoint has been set
	 * @return number of position or -1 if not available
	 */
	virtual int getPositionNumber() const = 0;

	/**
	 * Only for autotracking results:
	 * @return confidence level of this trackpoint, i.e. a value in [0,1] or -1 if not available
	 */
	virtual float getConfidenceLevel() const = 0;

	/**
	 * Returns the value of the requested trackpoint property. Only
	 * values returned by getAvailableProperties
	 */
	virtual QVariant getTPProperty(QString propertyName) const = 0;

	/**
	 * Returns all available trackpoint properties
	 */
	virtual QVector<QString> getAvailableProperties() const = 0;

	/**
	 * Destructor, necessary to allow proper destruction of derived classes 
	 */
	virtual ~ITrackPoint() {}

	/**
	 * Property names for getTPProperty method (initialization in itrackpoint.cpp)
	 */
	static const QString TP_FREEFLOATING;
	static const QString TP_SEMIADHERENCE;
	static const QString TP_ADHERENCE;
	
	static const QString TP_X;
	static const QString TP_Y;
	static const QString TP_TIMEPOINT;
};


#endif // itrackpoint_h__
