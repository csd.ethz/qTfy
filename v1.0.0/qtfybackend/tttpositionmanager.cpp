/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "tttpositionmanager.h"

// Project includes
#include "tttmanager.h"
//#include "fileinfoarray.h"
//#include "symbolhandler.h"
//#include "tttgui/statusbar.h"
//#include "tttio/fastdirectorylisting.h"
//#include "tttbackend/picturearray.h"

// Qt includes
#include <QPair>



TTTPositionManager::TTTPositionManager()
{
	// TreeWindowHeightFactor = 120;
	FirstTimePoint = 0;
	LastTimePoint = 0;
	Seconds = 0;
	// pictures = 0;
	files = 0;
	// ATPfirstTrackPointsOnly = false;
	// currentPI = PictureIndex (0, 1, 0);
	// maxAvailableWavelength = -1;
	
	initialized = false;
	locked = false;
	available = true;
	
	//availableWavelengths.clear();
	
	treeCount = -1;		//not calculated yet
}


TTTPositionManager::~TTTPositionManager()
{
	if (files)
		delete files;
	
	/*if (pictures)
		delete pictures;*/
}




void TTTPositionManager::setPictureDirectory (const QString &_cd)
{
	// Set picture directory
	pictureDirectory = QDir::fromNativeSeparators(_cd);
	if (pictureDirectory.right (1) == "/")
		pictureDirectory = pictureDirectory.left (pictureDirectory.length() - 1);
}


void TTTPositionManager::setTTTFileDirectory (const QString &_cd)
{
	if (_cd.right (1) == "/")
		tttDirectory = _cd;
	else
		tttDirectory = _cd + "/";
}



//// Called in TTTPositionLayout only (basically)
//bool TTTPositionManager::initialize ()
//{
//	if (isInitialized())
//		//method already run before
//		return true;
//
//	//the maximum of possible wavelengths
//	int WaveLengthCount = MAX_WAVE_LENGTH + 1;
//
//	int ftp = getFirstTimePoint();
//	int ltp = getLastTimePoint();
//
//	
//	/**
//	 * INIT PICTURES
//	 */
//
//	if (! getPictures()) 
//		setPictures (new PictureArray (this, (ltp - ftp + 1) * WaveLengthCount));
//
//	if (! getPictures()) {
//		//close TTT - no sense continuing for allocation failed most probably because of too less memory
//		TTTManager::getInst().quitProgram ("TTT ran out of memory and will close now.", 5);
//	}
//
//	getPictures()->init();
//
//	/**
//	 * READ LOG FILE
//	 */
//	bool logFileReadingSuccess = true;
//	if (! getFiles()) {
//		logFileReadingSuccess = Tools::readLogFile (this, ltp, ftp);
//
//		if (! logFileReadingSuccess) {
//			//reading the log file failed due to some reason
//			lockPosition();
//			return false;
//		}
//	}
//	else {
//		setFileInfoArray (0);	//just update attributes, do not delete file associations
//	}
//
//	// Initialization completed
//	setInitialized (true);	
//	return true;
//}


void TTTPositionManager::setFileInfoArray (FileInfoArray *_fileInfoArray)
{
	if (_fileInfoArray)
		files = _fileInfoArray;
}

//void TTTPositionManager::setTWHF (int _twhf)
//{
//	TreeWindowHeightFactor = _twhf;
//	emit TWHFChanged (TreeWindowHeightFactor);
//}
//
//void TTTPositionManager::setTimepoint (int _timepoint, int _wavelength, int _zIndex, bool _setGlobal)
//{
//	if (_timepoint == currentPI.TimePoint)
//		return;
//	
//	int oldtp = currentPI.TimePoint;
//	setTimepoint_withoutSignal (_timepoint, _wavelength, _zIndex, _setGlobal);
//	
//	// emit signal only when position is initialized
//	if(initialized)
//		emit timepointSet (currentPI.TimePoint, oldtp);
//}
//
//void TTTPositionManager::setTimepoint_withoutSignal (int _timepoint, int _wavelength, int _zIndex, bool _setGlobal)
//{
//	if (_timepoint == currentPI.TimePoint)
//		return;
//	
//	PictureIndex tmpPI (_timepoint, _zIndex, _wavelength);
//	setPictureIndex_withoutSignal (tmpPI, _setGlobal);
//	
//	if (_setGlobal)
//		//pass the timepoint in order to have a global setting
//		//this method is NOT called again due to the entry check for timepoint equality
//		TTTManager::getInst().setTimepoint (currentPI.TimePoint);
//}
//
//void TTTPositionManager::setPictureIndex (PictureIndex _pi, bool _setGlobal)
//{
//	if (_pi.TimePoint == currentPI.TimePoint)
//		return;
//	
//	int oldtp = currentPI.TimePoint;
//	setPictureIndex_withoutSignal (_pi, _setGlobal);
//	
//	emit timepointSet (currentPI.TimePoint, oldtp);
//}
//
//void TTTPositionManager::setPictureIndex_withoutSignal (PictureIndex _pi, bool _setGlobal)
//{
//	if (_pi.TimePoint == currentPI.TimePoint)
//		return;
//	
//	currentPI.TimePoint = _pi.TimePoint;
//	if (_pi.WaveLength != -1)
//		currentPI.WaveLength = _pi.WaveLength;
//	if(_pi.zIndex != -1)
//		currentPI.zIndex = _pi.zIndex;
//	
//	////update "all tracks" field, if visible
//	//if (AllTrackPoints.count() > 0) {
//	//	readAllTrackPoints (AllTracks_firstTracks, AllTracks_reallyAll, _pi.TimePoint, true);
//	//}
//	
//	if (_setGlobal)
//		//pass on the timepoint to become a global setting
//		//this method is NOT called again due to the entry check for timepoint equality
//		TTTManager::getInst().setTimepoint (currentPI.TimePoint);
//}
//
//void TTTPositionManager::setWavelength (int _wavelength)
//{
//	currentPI.WaveLength = _wavelength;
//}

void TTTPositionManager::setExperimentTimepoints (int _firstTimePoint, int _lastTimePoint)
{
	FirstTimePoint = _firstTimePoint;
	LastTimePoint = _lastTimePoint;
	Seconds = 0;
}

//long TTTPositionManager::getExperimentSeconds()
//{
//	if (! files)
//		return 0;
//	
//	if (Seconds == 0)
//		Seconds = files->calcSeconds (FirstTimePoint, LastTimePoint);
//	
//	return Seconds;
//}
//
//void TTTPositionManager::clearExternalTrees()
//{
//	// Delete trees from memory
//	for(QList<Tree* >::iterator it = externalTrees.begin(); it != externalTrees.end(); ++it)
//		delete *it;
//
//	// Clear array
//	externalTrees.clear();
//}
//
//void TTTPositionManager::readExternalTrees()
//{
//#ifndef TREEANALYSIS
//
//	// Change to wait cursor
//	QApplication::changeOverrideCursor(Qt::WaitCursor);
//
//	// Remove trees
//	clearExternalTrees();
//
//	// Get external tracks mode
//	TTTMovie::DisplayExternalTracksMode mode = frmMovie->getExternalTracksMode();
//
//	// Get folders list
//	QList<QPair<QString, PositionInformation* > > foldersWithPosInfos;
//	if(mode == TTTMovie::ALL_ALL_POS || mode == TTTMovie::FIRST_ALL_POS) {
//		// Iterate over all positions
//		QHash<int, TTTPositionManager*>& allPositions = TTTManager::getInst().getAllPositionManagers();
//		for (auto iter = allPositions.begin(); iter != allPositions.end(); ++iter) {
//			QPair<QString, PositionInformation* > nextVal((*iter)->getTTTFileDirectory(), &((*iter)->positionInformation));
//			foldersWithPosInfos.append(nextVal);
//		}
//	}
//	else {
//		// Only look in current position
//		QPair<QString, PositionInformation* > nextVal(getTTTFileDirectory(), &positionInformation);
//		foldersWithPosInfos.append(nextVal);
//	}
//
//	// Iterate over tree folders
//	for(QList<QPair<QString, PositionInformation* > >::const_iterator it = foldersWithPosInfos.constBegin(); it != foldersWithPosInfos.constEnd(); ++it) {
//		// Get current folder
//		QString curPath = it->first;
//		if(curPath.right(1) != "/") 
//			curPath.append('/');
//
//		// Get .ttt-files
//		const QStringList tttFiles = FastDirectoryListing::listFiles(curPath, QStringList(".ttt"));
//	
//		// Iterate over trees
//		for(QStringList::const_iterator itFile = tttFiles.constBegin(); itFile != tttFiles.constEnd(); ++itFile) {
//			const QString curFile = curPath + *itFile;
//
//			// Read ttt file
//			int trackCount, firstTP, lastTP;
//			Tree *curTree = new Tree();
//			curTree->reset();
//			if (TTTFileHandler::readFile (curFile, curTree, trackCount, firstTP, lastTP, it->second) != TTT_READING_SUCCESS) {
//				delete curTree;
//				continue;
//			}
//
//			// Add tree
//			externalTrees.append(curTree);
//		}
//	}
//
//	// Restore cursor
//	QApplication::restoreOverrideCursor();
//
//#endif
//}
//
//QLinkedList<TrackPoint> TTTPositionManager::getExternalTrackpoints(int _tp)
//{
//	// Return variable
//	QLinkedList<TrackPoint> ret;
//
//#ifndef TREEANALYSIS
//	// Get external tracks mode
//	TTTMovie::DisplayExternalTracksMode mode = frmMovie->getExternalTracksMode();
//
//	// Add one trackpoint with timepoint -10 to indicate a new colony (necessary for color change)
//	TrackPoint ColonyChangeTrackPoint;
//	ColonyChangeTrackPoint.TimePoint = -10;
//
//	// Iterate over trees
//	for(QList<Tree* >::const_iterator it = externalTrees.constBegin(); it != externalTrees.constEnd(); ++it) {
//		// Extract filename
//		const QString fileName = (*it)->getFilename();
//
//		// Get colony number
//		int colNr = fileName.mid (fileName.lastIndexOf (COLONY_MARKER) + 1, 3).toInt();
//
//		// Get position index
//		const QString positionIndexFromFolder = Tools::getPositionIndex (fileName);;
//		int posIndex = positionIndexFromFolder.toInt();
//
//		// Only first trackpoint?
//		if(mode == TTTMovie::FIRST_ALL_POS || mode == TTTMovie::FIRST_CUR_POS) {
//			TrackPoint tmpTrackPoint;
//			tmpTrackPoint = (*it)->getBaseTrack()->getTrackPointByNumber (1);
//			tmpTrackPoint.tmpCellNumber = 1;
//			tmpTrackPoint.tmpColonyNumber = colNr;
//			tmpTrackPoint.tmpPositionIndex = posIndex;
//
//			ret.append(tmpTrackPoint);
//		}
//		else {
//			// Trackpoints from correct timepoint
//			QLinkedList<TrackPoint> *tmpTrackPoints = (*it)->timePointTrackPoints (_tp);
//
//			for (QLinkedList<TrackPoint>::const_iterator iter = tmpTrackPoints->constBegin(); iter != tmpTrackPoints->constEnd(); ++iter) {
//				if (iter->X != -1) {
//					//enrich this trackpoint with information about its origin (cell number is already written there)
//					TrackPoint tmpTrackPoint = *iter;
//					tmpTrackPoint.tmpColonyNumber = colNr;
//					if (mode == TTTMovie::ALL_ALL_POS)
//						tmpTrackPoint.tmpPositionIndex = posIndex;
//
//					ret.append (tmpTrackPoint);
//				}
//			}
//		}
//
//		ret.append (ColonyChangeTrackPoint);
//	}
//
//#endif
//
//	return ret;
//}
//
//
////bool TTTPositionManager::readAllTrackPoints (bool _firstTracks, bool _reallyAll, int _timepoint, bool _withStatusBar)
////{
////	//if _reallyAll == false, this is only a shorthand for the other version
////	//if _reallyAll == true, we need to iterate over all position managers to find out about their trees, too
////
////	// -------- Oliver ----------
////	// clear old ones
////	AllTrackPoints.clear();
////
////	//store current parameter values for calls without knowledge about them
////	AllTracks_firstTracks = _firstTracks;
////	AllTracks_reallyAll = _reallyAll;
////	
////	if (! _reallyAll) {
////		//read from both old and new folders
////		readAllTrackPoints (getTTTFileDirectory(), _firstTracks, _timepoint, _withStatusBar);
////		readAllTrackPoints (getTTTFileDirectory (false, true), _firstTracks, _timepoint, _withStatusBar, true);
////	}
////	else {
////		//read tracks from all position managers
////		StatusBar *sb = 0;
////		if (_withStatusBar) {
////			sb = new StatusBar (0, "Reading all ttt files...");
////			sb->display (true);
////		}
////		
////		bool notFirstPM = false;
////		Q3DictIterator<TTTPositionManager> iter (TTTManager::getInst().getAllPositionManagers());
////		for ( ; iter.current(); ++iter) {
////			
////			//NOTE: does not heed old/new folders anymore!
////			readAllTrackPoints (iter.current()->getTTTFileDirectory(), _firstTracks, _timepoint, false, notFirstPM, true);
////			
////			if (sb)
////				sb->update();
////			
////			notFirstPM = true;
////		}
////		
////		if (sb) {
////			sb->display (false);
////			delete sb;
////		}
////	}
////	
////	return true;
////	
////}
////
////bool TTTPositionManager::readAllTrackPoints (const QString &_folder, bool _firstTracks, int _timepoint, bool _withStatusBar, bool _append, bool _reallyAll)
////{
////        QVector<QString> files = SystemInfo::listFiles (_folder, QDir::Files, "*" + TTT_FILE_SUFFIX, false);
////	
////	if (files.count() == 0)
////		return false;
////	
////	QString posIndex = Tools::getPositionIndex (_folder);
////	int positionIndexFromFolder = posIndex.toInt();
////	
////	//normally, it is "this", but not necessarily...
////	TTTPositionManager *tttpm = TTTManager::getInst().getPositionManager (posIndex);
////	
////	Tree tree;
////	int firstTP = getFirstTimePoint(), lastTP = getLastTimePoint();
////	int TrackCount = 0;
////	
////	if (! _append)
////		AllTrackPoints.clear();
////	
////	Q3ValueList<TrackPoint> tmpTrackPoints;
////	
////	TrackPoint ColonyChangeTrackPoint;
////	ColonyChangeTrackPoint.TimePoint = -10;
////	TrackPoint tmpTrackPoint;
////	
////	StatusBar *sb = 0;
////	if (_withStatusBar) {
////		sb = new StatusBar (0, "Loading tree files...");
////		//sb.setPosition (150, 150, 400, 80);
////		sb->display (true);
////		sb->setBounds (0, files.size());
////	}
////	
////	//int counter = 0;
////	for (QVector<QString>::const_iterator iterFile = files.constBegin(); iterFile != files.constEnd(); ++iterFile) {
////		if (sb)
////			sb->update();
////
////		tree.reset();
////		if (TTTFileHandler::readFile (*iterFile, &tree, TrackCount, firstTP, lastTP, &tttpm->positionInformation) != TTT_READING_SUCCESS)
////			continue;
////
////		int colNr = (*iterFile).mid ((*iterFile).lastIndexOf (COLONY_MARKER) + 1, 3).toInt();
////
////		if (tree.getBaseTrack()) {
////			//reading the tree/ttt-file was successful
////			//insert the trackpoints at the current timepoint into array
////
////
////			if (_firstTracks) {
////				//take only the first trackpoint of the base track of each tree
////				tmpTrackPoints.clear();
////				tmpTrackPoint = tree.getBaseTrack()->getTrackPointByNumber (1);
////				tmpTrackPoint.tmpCellNumber = 1;
////				tmpTrackPoint.tmpColonyNumber = colNr;
////				tmpTrackPoint.tmpPositionIndex = positionIndexFromFolder;
////				tmpTrackPoints.push_back (tmpTrackPoint);
////			}
////			else {
////				Q3ValueList<TrackPoint> *tmpl = tree.timePointTrackPoints (_timepoint);
////				if (tmpl)
////					tmpTrackPoints = *tmpl;
////			}
////
////			for (Q3ValueList<TrackPoint>::const_iterator iter = tmpTrackPoints.constBegin(); iter != tmpTrackPoints.constEnd(); ++iter) {
////
////				if ((*iter).X != -1) {
////					if (! _firstTracks) {
////						//enrich this trackpoint with information about its origin (cell number is already written there)
////						tmpTrackPoint = *iter;
////						tmpTrackPoint.tmpColonyNumber = colNr;
////						if (_reallyAll)
////							tmpTrackPoint.tmpPositionIndex = positionIndexFromFolder;
////						else
////							tmpTrackPoint.tmpPositionIndex = 0;
////
////						AllTrackPoints.push_back (tmpTrackPoint);
////					}
////					else
////						AllTrackPoints.push_back (*iter);
////
////					//counter++;
////				}
////			}
////
////			//add one trackpoint with timepoint -10 to indicate a new colony (necessary for color change)
////			AllTrackPoints.push_back (ColonyChangeTrackPoint);
////		}
////
////	}
////	
////	if (sb)
////		sb->display (false);
////	
////	//SystemInfo::report (QString ("Position %1, Colonies: %2").arg (positionIndexFromFolder).arg (counter));
////	return true;
////}
//
//	
//
//void TTTPositionManager::setWavelengthAvailable (int _wavelength, bool _available)
//{
//	if (_available) {
//		availableWavelengths.insert (_wavelength);
//		if (_wavelength > maxAvailableWavelength)
//			maxAvailableWavelength = _wavelength;
//	}
//	else {
//		// Remove
//		availableWavelengths.remove(_wavelength);
//		
//		////! delete key !
//		//if (availableWavelengths.contains (_wavelength))
//		//	availableWavelengths.remove (_wavelength);
//		
//		maxAvailableWavelength = -1;		//not known now!
//	}
//}
//
//int TTTPositionManager::getMaxAvailableWavelength()
//{
//	//note: it would be nonsense to recalculate it every time, so recalc is only performed if the wavelengths are not yet set or were reset
//	if ((maxAvailableWavelength == -1) && (availableWavelengthsSet())) {
//		
//		// Find highest wavelength
//		for (QSet<int>::const_iterator iter = availableWavelengths.constBegin(); iter != availableWavelengths.constEnd(); ++iter) {
//			if (*iter > maxAvailableWavelength)
//				maxAvailableWavelength = *iter;
//		}
//	}
//	
//	return maxAvailableWavelength;
//}
//
//void TTTPositionManager::resetAvailableWavelengths()
//{
//	setAvailableWavelengthsSet (false);
//	availableWavelengths.clear();
//	maxAvailableWavelength = -1;
//}

const QString TTTPositionManager::getTTTFileDirectory (bool _createIfNotExists, bool _useAlternativeDirectory) const
{
	QString tmpdir = tttDirectory;
	
	if (_useAlternativeDirectory) {
		//get alternative directory
		QString basename = getBasename();
		if (! basename.isEmpty())
			tmpdir = SystemInfo::getTTTFileFolderFromBaseName (getBasename(), _createIfNotExists, _useAlternativeDirectory);
		else
			//not defined!!
			tmpdir = "";
	}
	
	if (_createIfNotExists)
		SystemInfo::checkNcreateDirectory (tmpdir, true, true);
	
	return tmpdir;
}

//QStringList TTTPositionManager::getAllTTTFiles() const
//{
//	QStringList resultList;
//	
//	int run = 1;
//	
//	while (run <= 2) {
//	
//		//try new folder...
//		QString directory = getTTTFileDirectory();
//		
//		if (run == 2)
//			//...and old folder
//			directory = getTTTFileDirectory (false, true);
//
//		QVector<QString> fileArray = SystemInfo::listFiles (directory, QDir::Files, "*" + TTT_FILE_SUFFIX);
//
//		for (QVector<QString>::const_iterator iter = fileArray.constBegin(); iter != fileArray.constEnd(); ++iter) {
//			if (! iter->isEmpty()) {
//				const QString filename = (*iter);
//				resultList.append (filename);
//			}
//		}
//
//		run++;
//	}
//	
//	return resultList;
//}
//
//int TTTPositionManager::getTreeCount (bool _recalc)
//{
//	if ((treeCount > -1) && (! _recalc))
//		return treeCount;
//	else {
//		//calculate tree count
//		
//		treeCount = 0;
//		
//		//try new folder...
//                QVector<QString> files = SystemInfo::listFiles (getTTTFileDirectory(), QDir::Files, "*" + TTT_FILE_SUFFIX, false);
//		treeCount += files.count();
//		
//		//...and old folder
//		files = SystemInfo::listFiles (getTTTFileDirectory (false, true), QDir::Files, "*" + TTT_FILE_SUFFIX, false);
//		treeCount += files.count();
//		
//		return treeCount;
//	}
//}
//
//void TTTPositionManager::lockPosition()
//{
//#ifndef TREEANALYSIS // ----------------- Konstantin -------------------------
//	locked = true;
//	if (posThumbnailInTTTPosLayout) {
//		posThumbnailInTTTPosLayout->lock();
//		posThumbnailInTTTPosLayout->updateDisplay();
//	}
//#endif  // ----------------- Konstantin -------------------------
//}
//
//QRectF TTTPositionManager::getGlobalImageRect() const
//{
//	// Get width and height
//	const WavelengthInformation& wlInfo = TATInformation::getInst()->getWavelengthInfo (0);
//	int widthPixels = wlInfo.getWidth();
//	int heightPixels = wlInfo.getHeight();
//
//	// Set default size if necessary (necessary if there is no tatxml)
//	if(widthPixels <= 0)
//		widthPixels = 1388;
//	if(heightPixels <= 0)
//		heightPixels = 1040;
//
//	// Calc wl0 image rect
//	float mmpp = TATInformation::getInst()->getWavelengthInfo (0).getMicrometerPerPixel();
//	float w = widthPixels * mmpp,
//		h = heightPixels * mmpp;
//
//	// Check if system is inverted
//	if(TTTManager::getInst().coordinateSystemIsInverted()) 
//		return QRectF(positionInformation.getLeft()-w, positionInformation.getTop()-h, w, h);
//	else
//		return QRectF(positionInformation.getLeft(), positionInformation.getTop(), w, h);
//}
//
//void TTTPositionManager::setZIndex( int _zIndex )
//{
//	if(_zIndex < 0 || _zIndex > MAX_ZINDEX)
//		return;
//
//	// Change
//	currentPI.zIndex = _zIndex;
//}
//
//
