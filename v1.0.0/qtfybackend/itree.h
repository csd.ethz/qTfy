/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef itree_h__
#define itree_h__

// Qt
#include <QString>
#include <QVariant>

// Forward declarations
class ITrack;
class ITrackPoint;
class CellLineageTree;


/**
 * @author Oliver Hilsenbeck
 *
 * Interface for classes representing cell lineage trees, currently (5.7.2011) these are Tree, TreeFragment and LittleTree (interface by that class not implemented yet)
 * Used for example in TreeViewTreeItem
 */
class ITree {

public:

	///**
	// * @return pointer to root node or 0 if root node is not available (remains valid only as long as tree object exists)
	// */
	virtual ITrack* getRootNode() const = 0;

	///**
	// * @param _number the track number, can be invalid
	// * @return track with specified number or 0 if not available (remains valid only as long as tree object exists)
	// */
	virtual ITrack* getTrackByNumber(int _number) const = 0;

	///**
	// * @return max track number or -1 if not available
	// */
	virtual int getMaxTrackNumber() const = 0;

	///**
	// * @return number of tracks in this tree or -1 if not available
	// */
	virtual int getNumberOfTracks() const = 0;

	/**
	 * @return tree name or description (whatever this is) or empty string if not available
	 */
	virtual QString getTreeName() const = 0;

	/**
	 * @return experiment name or empty string if not available
	 */
	virtual QString getExperimentName() const = 0;


	/**
	 * @returns the position index of the current tree
	 */
	virtual QString getPositionIndex() const = 0;


	/**
	 * @return the (absolute) filename of this tree
	 */
	virtual QString getFilename() const = 0;

	///**
	// * @return if tree object has been modified (this value is generally not guaranteed to be correct)
	// */
	//virtual bool getModified() const = 0;

	/**
	 * Get trackpoints at specified timepoint
	 * @param _timePoint the timepoint
	 * @return list of pointers to ITrackPoint objects (remain valid only as long as tree object exists)
	 */
	virtual QList<ITrackPoint*> getTrackPointsAtTimePoint(int _timePoint) const = 0;


	/**
	* @ return the vector of all tracks of the cells in the current tree
	*/
	virtual QVector<ITrack*> getTracksInTree() const = 0;

	
	///**
	// * Get a specific property of this tree
	// */
	virtual QVariant getTreeProperty(QString _propName) const = 0;

	///**
	// * Get a list of all possible properties.
	// * The returned values are suitable arguments for getTreeProperty
	// */
	//virtual QVector<QString> getAvailableProperties() const = 0;

	/**
	 * Destructor, necessary to allow proper destruction of derived classes 
	 */
	virtual ~ITree() {}

	/**
	 * Get the CellLineageTree object of this tree
	 */

	virtual CellLineageTree* getCellLineageTree() const = 0;

public:
	/**
	 * Property names for getTreeProperty method (initialization in itree.cpp)
	 */
	static const QString TREE_OPT_START_TP;		// First TrackPoint of this tree, in seconds
	static const QString TREE_OPT_STOP_TP;		// Last TrackPoint of this tree, in seconds
	static const QString TREE_OPT_LIFETIME;		// Lifetime of this tree, in seconds
	static const QString TREE_OPT_NUMBER_OF_CELLS;		// Number of Cells in this tree
	static const QString TREE_OPT_NUMBER_OF_GENERATIONS;// Number of Generations in this tree
	static const QString TREE_EXPERIMENT_NAME;	// Name of the experiment this tree belongs to
	static const QString TREE_PREINCUBATION_TIME;	// The preincubation time for this tree (=preincubation time for the experiment)
};


#endif // itree_h__
