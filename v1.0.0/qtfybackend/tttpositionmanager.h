/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TTTPOSITIONMANAGER_H
#define TTTPOSITIONMANAGER_H


// PROJECT
#include "qtfydata/positioninformation.h"
#include "qtfydata/fileinfo.h"
#include "qtfybackend/pictureindex.h"
#include "qtfybackend/fileinfoarray.h"
//#include "qtfydata/trackpoint.h"
//#include "displaysettings.h"
//#include "symbolhandler.h"


// QT includes
#include <QString>
#include <QStringList>
#include <QDir>
#include <QVector>
#include <QLinkedList>


/**
	@author Bernhard
	
	This class manages the position-specific global attributes of TTT.
	All experiment global attributes are in TTTManager.
*/

class TTTPositionManager  : public QObject {

	Q_OBJECT

public:
//	
//#ifndef TREEANALYSIS
//	///for each position we have a separate movie window, display settings, ...
//	TTTMovie *frmMovie;
//	TTTRegionSelection *frmRegionSelection;
//    TTTGammaAdjust *frmGammaAdjust;		// 05/09/11-OH: moved to movie window, this is just another pointer
//
//	///the thumbnail of the position in the TTTPositionLayout (and only there)
//	PositionThumbnail *posThumbnailInTTTPosLayout;
//	
//	// 28/03/11-OH: the need for this has been eliminated.
//	/////...we also have a backup pointer, if necessary; for easy access, it's public (I know that it ain't nice!)
//	//PositionThumbnail *backupPositionThumbnail;
//#endif
//	
	///the global object containing all information about the position
	PositionInformation positionInformation;
//	
//	///the thumbnail of the position in the layout window; new: and other windows as well, thus...
////	PositionThumbnail *positionThumbnail;  // ---------------------- Konstantin: comment and paste into #ifndef
//	
//	///...we also have a backup pointer, if necessary; for easy access, it's public (I know that it ain't nice!)
////	PositionThumbnail *backupPositionThumbnail;  // ---------------------- Konstantin: comment and paste into #ifndef
//	
//	
	TTTPositionManager();
	
	~TTTPositionManager();



		/**
	 * sets the current picture directory
	 * @param _cd the path of the current picture directory
	 */
	void setPictureDirectory (const QString &_cd);

	/**
	 * @return the current picture directory path (without "/" at the end)
	 *         usually at "~/250GB/..."
	 */
	const QString& getPictureDirectory() const;

	/**
	* sets the current ttt file directory (will always have a '/' at the end)
	* @param _cd the path of the current ttt file directory
	*/
	void setTTTFileDirectory (const QString &_cd);

	/**
	* Sets the position's basename.
	* @param _baseName the basename, e.g. 051104_p025
	*/
	void setBasename (const QString &_baseName);

		
	/**
	 * @param _addTimepointMarker whether the timepoint suffix (_t) should be added
	 * @return the basename of the position without (or with) timepoint and wavelength markers, e.g. 051104_p025
	 */
	const QString getBasename (bool _addTimepointMarker = false) const
	{
		if (_addTimepointMarker)
			return baseName + TIMEPOINT_MARKER;
		else
			return baseName;
	}


		/**
	 * @param _createIfNotExists whether the directory should be created if it does not exist (default = false)
	 * @param _useAlternativeDirectory whether the alternative tttfile directory should be used
	 *                                 (the one the user did NOT choose to use, i.e. the old one without user sign or the new one with it)
	 * @return the current ttt file directory path (with "/" at the end)
	 */
	const QString getTTTFileDirectory (bool _createIfNotExists = false, bool _useAlternativeDirectory = false) const;
	
	/**
	* @return the (program unique) FileInfoArray instance
	*/
	const FileInfoArray* getFiles() const;

	/**
	* sets the file info array
	* @param _fileInfoArray the (program unique) FileInfoArray instance
	*/
	void setFileInfoArray (FileInfoArray *_fileInfoArray);

	    /**
         * initializes the parameters of the tree
         * @param _firstTimePoint the first timepoint of the experiment, usually 1
         * @param _lastTimePoint the last timepoint of the experiment, either set initially by reading the jpg's or by reading a ttt-file
         */
	void setExperimentTimepoints (int _firstTimePoint, int _lastTimePoint);
	
        /**
         * @return the first timepoint of the experiment
         */
	int getFirstTimePoint() const;
	
        /**
         * @return the last timepoint of the experiment
         */
	int getLastTimePoint() const;


	/**
	* sets whether this position is available
	* @param _available 
	*/
	void setAvailability (bool _available);

	/**
	* @return whether this position is currently available
	*/
	bool isAvailable() const;




private:
	///the folder of the pictures of this position (without "/")
	QString pictureDirectory;

	///the folder of the ttt files of this position (without "/")
	QString tttDirectory;

	///the basename of the experiment + position
	///example: if we have the experiment from 051104 and position 25, the basename is 051104_p025
	QString baseName;

	///whether this position is available
	///NOT available means: not displayed in the position layout and no selection possible
	bool available;
		
	/**
	 * initializes this position manager (reads the log file, creates the forms, ...)
	 //* @param _pictureSuffix the suffix of the picture files in this folder (merely the length is important)
	 * @return success
	 */
	bool initialize ();
	
	
	/**
	 * sets whether this manager is already initialized (files and pictures set)
	 * @param _initialized whether all necessary attributes are set
	 */
	void setInitialized (bool _initialized);
	
	/**
	 * @return whether this manager is already initialized (files and pictures set)
	 */
	bool isInitialized() const;
	
	/**
	 * locks this position from tracking (locking cannot be removed)
	 */
	void lockPosition();
	
	/**
	 * @return whether this position is locked (can be set by calling lockPosition())
	 */
	bool isLocked();
	

//	/**
//	 * sets the picture array
//	 * @param _pictures a pointer to a PictureArray instance
//	 */
//	void setPictures (PictureArray *_pictures);
//	
//	/**
//	 * @return the picture array (considers all pictures in the current folder!)
//	 */
//	PictureArray* getPictures();
//	
//	///@return the tree window height factor (seconds per pixel)
//	int getTWHF() const;
//	
//	///sets the tree window height factor
//	///@param _twhf the factor (_twhf seconds correspond to 1 pixel)
//	void setTWHF (int _twhf);
//	

//	/**
//	 * sets the field with all display settings
//	 * @param _displays 
//	 */
//	void setDisplays (const DisplaySettings& _displays);
//	
//	/**
//	 * @return the current display settings
//	 */
//	DisplaySettings& getDisplays();
//	
//	/**
//	 * sets a new timepoint and sends a signal that the timepoint was changed
//	 * NOTE: this method is only called by TTTManager! (and should not be called by any other class)
//	 * @param _timepoint the timepoint of the movie
//	 * @param _wavelength the wavelength
//	 * @param _setGlobal whether the set timepoint should be passed on to the global settings
//	 */
//	void setTimepoint (int _timepoint, int _wavelength = -1, int _zIndex = -1, bool _setGlobal = false);
//	
//	/**
//	 * sets a new timepoint without sending the update signal
//	 * @param _timepoint the timepoint of the movie
//	 * @param _zIndex the z-index
//	 * @param _wavelength the wavelength
//	 * @param _setGlobal whether the set timepoint should be passed on to the global settings
//	 */
//	void setTimepoint_withoutSignal (int _timepoint, int _wavelength = -1, int _zIndex = -1, bool _setGlobal = false);
//	
//	/**
//	 * sets a new picture index (tp & wl) and sends a signal that the timepoint was changed
//	 * @param _pi the movie picture index
//	 * @param _setGlobal whether the set timepoint should be passed on to the global settings
//	 */
//	void setPictureIndex (PictureIndex _pi, bool _setGlobal = false);
//	
//	/**
//	 * sets a new picture index (tp & wl) without sending the update signal
//	 * @param _pi the movie picture index
//	 * @param _setGlobal whether the set timepoint should be passed on to the global settings
//	 */
//	void setPictureIndex_withoutSignal (PictureIndex _pi, bool _setGlobal = false);
//	
//	/**
//	 * sets the current wavelength (does not refresh the displayed image or anything)
//	 * @param _wavelength the current wavelength
//	 */
//	void setWavelength (int _wavelength);
//
//	/**
//	 * sets the current z-index (does not refresh the displayed image or anything)
//	 * @param _zIndex the new z-index
//	 */
//	void setZIndex (int _zIndex);
//	
//	/**
//	 * @return the current timepoint for this position
//	 */
//	int getTimepoint() const {
//		return currentPI.TimePoint;
//	}
//
//	/**
//	 * @return the current z-index for this position.
//	 */
//	int getZIndex() const {
//		return currentPI.zIndex;
//	}
//	
//	/**
//	 * @return the current wavelength for this position
//	 */
//	int getWavelength() const {
//		return currentPI.WaveLength;
//	}
//	
//	/**
//	 * @return the current PictureIndex (tp & wl) for this position
//	 */
//	PictureIndex getPictureIndex() const;
	
    
	
//	/**
//	 * calculates and returns the seconds between the first and the last timepoint of the experiment
//	 * @return a long value which contains the lasting period of the current experiment in seconds
//	 */
//	long getExperimentSeconds();
//	

//
//	/**
//	 * Set the current background directory.
//	 * @param _cd the path of the current background directory
//	 */
//	void setBackgroundDirectory(const QString &_cd) 
//	{
//		// Set background directory
//		backgroundDirectory = QDir::fromNativeSeparators(_cd);
//		if (backgroundDirectory.right (1) == "/")
//			backgroundDirectory = backgroundDirectory.left (backgroundDirectory.length() - 1);
//	}
//
//	/**
//	 * @return background directory.
//	 */
//	QString getBackgroundDirectory() const 
//	{
//		return backgroundDirectory;
//	}
//	

//	/**
//	 * sets the current ttt file directory (will always have a '/' at the end)
//	 * @param _cd the path of the current ttt file directory
//	 */
//	void setTTTFileDirectory (const QString &_cd)
//	{
//		if (_cd.right (1) == "/")
//			tttDirectory = _cd;
//		else
//			tttDirectory = _cd + "/";
//	}
//	

//	/**
//	 * @return a list of all ttt files currently in this pm's directory
//	 */
//	QStringList getAllTTTFiles() const;
//	
//	/**
//	 * returns the current picture directory as QDir object
//	 * @param _pictureFolder whether the folder for the pictures (= true) or for the ttt files (= false) should be returned
//	 * @return a QDir instance of the current directory
//	 */
//	QDir getQDir (bool _pictureFolder) const;
	

//	QLinkedList<TrackPoint> getExternalTrackpoints(int _tp);
//	void readExternalTrees();
//	void clearExternalTrees();
//
//	/* *
//	 * sets the available wavelengths as a whole
//	 * @param _avWL the available wls (only these need to be specified!)
//	 */
///*	void setAvailableWavelengths (const QMap<int, bool> &_avWL)
//		{availableWavelengths = _avWL;}*/
//	
//	/**
//	 * @return all available wavelengths as a QList (creates a deep copy).
//	 */
//	QList<int> getAvailableWavelengths() const {
//		return availableWavelengths.values();
//	}
//
//	/**
//	 * @return all available wavelengths as const reference to a QSet (valid as long as TTTPositionManager exists).
//	 */
//	const QSet<int>& getAvailableWavelengthsByRef() const {
//		return availableWavelengths;
//	}
//	
//	/**
//	 * returns whether the provided wavelength is available (at least one picture with this wavelength exists in the position folder)
//	 * @param _wavelength the wavelength
//	 * @return wl available?
//	 */
//	bool isWavelengthAvailable (int _wavelength) const {
//		return availableWavelengths.contains (_wavelength);
//	}
//	
//	/**
//	 * sets the provided wavelength to be available
//	 * @param _wavelength the wavelength
//	 * @param _available whether it is available (default = yes)
//	 */
//	void setWavelengthAvailable (int _wavelength, bool _available = true);
//	
//	/**
//	 * sets whether all wavelengths are set (to be available or not) for this position
//	 * @param _set 
//	 */
//	void setAvailableWavelengthsSet (bool _set)
//		{availableWavelengths_set = _set;}
//	
//	/**
//	 * @return whether all wavelengths are set (available or not) for this position
//	 */
//	bool availableWavelengthsSet() const
//		{return availableWavelengths_set;}
//	
//	/**
//	 * @return the highest available wavelength for this experiment
//	 */
//	int getMaxAvailableWavelength();
//	
//	/**
//	 * resets all wavelengths to "not available" (clean up)
//	 */
//	void resetAvailableWavelengths();
//	
	/**
	 * Calculates the number of trees in the position; if this was done before and _recalc is false (the default), the previous result is returned
	 * @param _recalc whether the number should be re-calculated
	 * @return the (last valid) number of trees for this position
	 */
	int getTreeCount (bool _recalc = false);
	
//    /**
//        * @return a reference to the current symbol handler
//        */
//    SymbolHandler& getSymbolHandler()
//            {return symbolHandler;}
//
//    /**
//        * sets the current symbol handler
//        */
//    void setSymbolHandler (SymbolHandler &_sh)
//            {symbolHandler = _sh;}
//
//	/**
//	 * Get image rect in global micrometer coordinates.
//	 */
//	QRectF getGlobalImageRect() const;
//
//signals:
//	
//	///emitted when TreeWindowHeightFactor changed, used for updates in timescale
//	///@param _twhf the new height factor 
//	void TWHFChanged (int _twhf);
//	
//	/**
//	 * emitted when setTimepoint() was called for this position (and position is initialized)
//	 * All forms that care about the timepoint need to connect this signal to update slots
//	 * @param _currentTimepoint the current (new) timepoint
//	 * @param _oldTimepoint the old timepoint
//	 */
//	void timepointSet (int _currentTimepoint, int _oldTimepoint);
//	
//
//private:

	///whether this manager is initialized (was told what to do...)
	bool initialized;
	
	///whether this position is locked => not available for tracking
	///happens, if either
	///- the log file could not be read
	bool locked;
	

//	
//	///the picture array
//	PictureArray *pictures;
//	
//	///the ratio between seconds and pixel (twhf = 120 => 120 seconds correspond to 1 pixel)
//	int TreeWindowHeightFactor;
	
	///contains the meta information about the picture files
	///in TTTMainWindow, LogFileReader reads the log-file and stores all information,
	/// then its array is copied to local Files, which is now the only storage point
	/// and grants public global access, since this class is known to all classes
	FileInfoArray *files;
	
//	///contains the display settings for each wavelength
//	DisplaySettings displays;
//	
//	///the current timepoint and wavelength for this position
//	///can be modified both here and globally in TTTManager
//	///NOTE: this is the only place where the current timepoint is stored (once for each position)!
//	///      no 'global' timepoint exists!
//	PictureIndex currentPI;
	
	///the first timepoint of this position
	long FirstTimePoint;
	
	///the last timepoint of this position
	long LastTimePoint;
	
	///contains the number of seconds between the first and the last timepoint
	///calculated on first request, then the stored value is used
	long Seconds;
	
	
//
//	///the folder of the background images of this position (without "/")
//	QString backgroundDirectory;
//	

//	

//	
//	///the array for the tracks from all files at the current timepoint
//	///filled when the user clicks the "display all tracks" button
//	///@Deprecated
//	QVector<TrackPoint *> AllTrackPoints;
//	
//	///Array with external trackpoints
//	///@Deprecated
//	QList<Tree* > externalTrees;
//
//	///whether only the first trackpoint for the first track was loaded into the array AllTrackPoints
//	bool ATPfirstTrackPointsOnly;
//	
//	///stores the available wavelengths for this position
//	///this is important for not having to read the pictures in the folder every time we need this info!
//	///the data is filled only once, when reading the picture files of this folder (usually while selecting a position in the initial position layout)
//	QSet<int> availableWavelengths;
//	
//	///whether all wavelengths are set (to be available or not) for this position
//	bool availableWavelengths_set;
//	
//	///the maximum available wavelength (if not known yet, it is -1)
//	int maxAvailableWavelength;
	
	///the number of trees in this position (set by getTreeCount())
	int treeCount;
	
//	///the temporary parameters for reading all/first tracks, used by setTimepoint() to update all tracks
//	bool AllTracks_firstTracks;
//	bool AllTracks_reallyAll;
//
//        ///the instance of SymbolHandler to deal with the symbol arrays for this position
//        SymbolHandler symbolHandler;

};
//
////inliners

inline void TTTPositionManager::setAvailability (bool _available)
{
	available = _available;
	if (! _available)
		lockPosition();
}

inline bool TTTPositionManager::isAvailable() const
{
	return available;
}

inline const QString& TTTPositionManager::getPictureDirectory() const
{
	return pictureDirectory;
}
//
//inline int TTTPositionManager::getTWHF() const
//{
//	return TreeWindowHeightFactor;
//}
//
//inline PictureArray* TTTPositionManager::getPictures()
//{
//	return pictures;
//}
//
inline const FileInfoArray* TTTPositionManager::getFiles() const
{
	return files;
}
//
//inline DisplaySettings& TTTPositionManager::getDisplays()
//{
//	return displays;
//}
//
inline int TTTPositionManager::getFirstTimePoint() const
{
	return FirstTimePoint;
}

inline int TTTPositionManager::getLastTimePoint() const
{
	return LastTimePoint;
}

inline void TTTPositionManager::setInitialized (bool _initialized)
{
	initialized = _initialized;
}

inline bool TTTPositionManager::isInitialized() const
{
	return initialized;
}
	
//inline void TTTPositionManager::setPictures (PictureArray *_pictures)
//{
//	pictures = _pictures;
//}
//
//inline void TTTPositionManager::setDisplays (const DisplaySettings& _displays)
//{
//	displays = _displays;
//}
//
//inline QDir TTTPositionManager::getQDir (bool _pictureFolder) const
//{
//	if (_pictureFolder)
//		return QDir (pictureDirectory);
//	else
//		return QDir (tttDirectory);
//}
//
inline void TTTPositionManager::setBasename (const QString &_baseName)
{
	baseName = _baseName;
}

//inline void TTTPositionManager::setATPfirstTrackPointsOnly (bool _ftpOnly)
//{
//	ATPfirstTrackPointsOnly = _ftpOnly;
//}
//
//inline bool TTTPositionManager::getATPfirstTrackPointsOnly()
//{
//	return ATPfirstTrackPointsOnly;
//}

//inline bool TTTPositionManager::isLocked()
//{
//	return locked;
//}
//
//inline PictureIndex TTTPositionManager::getPictureIndex() const
//{
//	return currentPI;
//}


#endif
