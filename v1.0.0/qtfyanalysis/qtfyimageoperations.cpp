/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Laura Skylaki
 * @date 14/08/2015
 */


// PROJECT
#include "QTFyImageOperations.h"
#include "qtfydata\tatinformation.h"
#include "qtfybackend\tttmanager.h"
#include "watershedsegmenter.h"

//STD
#include "fstream"


// QT
#include <QDebug>
#include <QFile>
#include <QStringList>
#include <QInputDialog>
#include <QImageWriter>



QTFyImageOperations::QTFyImageOperations()
{
}

QTFyImageOperations::~QTFyImageOperations()
{
}

Mat QTFyImageOperations::loadDetectionWavelengthImage(const QString _pathtoExperiment, const QString _expName, const QString _pos, const int _timepoint, const int _zindex, const int _wl)
{
	QString pathToImage = _pathtoExperiment + "\\"  + _pos + "\\";

	QString imageFileName = getImageFileName(_pos, _timepoint, _zindex, _wl, 3, 2, "png");
	QString imagePath = pathToImage + imageFileName;

	//// find the image file
	//QFile fileImageRaw(imagePath);

	////if it doesn't exists check for different image file formats
	//if (!fileImageRaw.exists()) {
	//	imagePath = checkForImageFileFormat(imagePath);
	//	if(imagePath.isEmpty())
	//		return Mat();
	//}
	//get correct imagePath, with the correct image file format suffix (defualt is png)
	imagePath = checkForImageFileFormat(imagePath);
	//if there is no image file
	if(imagePath.isEmpty())
		return Mat();

	//else{
	Mat wl = cv::imread(imagePath.toStdString(), CV_LOAD_IMAGE_ANYDEPTH | CV_LOAD_IMAGE_GRAYSCALE);
	// convert image format
	int imgType = wl.type();

	Mat imgOut;
	wl.convertTo(imgOut, CV_8UC1);
		
	return imgOut;
	//}

}

Mat QTFyImageOperations::loadWavelengthImage(const QString _pathtoExperiment, const QString _expName, const QString _pos, const int _timepoint, const int _zindex, const int _wl)
{
	QString pathToImage = _pathtoExperiment + "\\"  + _pos + "\\";

	QString imageFileName = getImageFileName(_pos, _timepoint, _zindex, _wl, 3, 2, "png");
	QString imagePath = pathToImage + imageFileName;

	//// find the image file
	//QFile fileImageRaw(imagePath);

	////if it doesn't exists check for different image file formats
	//if (!fileImageRaw.exists()) {
	//	imagePath = checkForImageFileFormat(imagePath);
	//	if(imagePath.isEmpty())
	//		return Mat();
	//}

	//get correct imagePath, with the correct image file format suffix (default is png)
	imagePath = checkForImageFileFormat(imagePath);
	//if there is no image file
	if(imagePath.isEmpty())
		return Mat();

	//else {
		Mat wl = cv::imread(imagePath.toStdString(), CV_LOAD_IMAGE_ANYDEPTH | CV_LOAD_IMAGE_GRAYSCALE);
		// convert image format
		int imgType = wl.type();
		Mat imgOut;
		imgOut = convertTo64F(wl);
		
		return imgOut;
	//}
	
}

Mat QTFyImageOperations::loadBackgroundImage(const QString _pathtoExperiment, const QString _expName, const QString _pos, const int _timepoint, const int _zindex, const int _wl)
{
	QString pathToImage = _pathtoExperiment + "\\background\\" + _pos + "\\";

	QString imageFileName = getImageFileName(_pos, _timepoint, _zindex, _wl, 3, 2, "png");
	QString imagePath = pathToImage + imageFileName;

	//// find the image file
	//QFile fileImageRaw(imagePath);

	////if it doesn't exists check for different image file formats
	//if (!fileImageRaw.exists()) {
	//	imagePath = checkForImageFileFormat(imagePath);
	//	if(imagePath.isEmpty())
	//		return Mat();
	//}

	//get correct imagePath, with the correct image file format suffix (defualt is png)
	imagePath = checkForImageFileFormat(imagePath);
	//if there is no image file
	if(imagePath.isEmpty())
		return Mat();
	//else {
		Mat backgroundImg = cv::imread(imagePath.toStdString(), CV_LOAD_IMAGE_ANYDEPTH | CV_LOAD_IMAGE_GRAYSCALE);
		// convert image format
		int imgType = backgroundImg.type();

		Mat imgOut;
		imgOut = convertTo64F(backgroundImg);

		return imgOut;
	//}
}

Mat QTFyImageOperations::loadGainImage(const QString _pathtoExperiment, const QString _expName, const QString _pos, const int _wl)
{
	QString pathToImage = _pathtoExperiment +  "\\background\\" + _pos + "\\";

	QString imageFileName = QString("gain_w%1.%2").arg(_wl, 2, 10, QChar('0')).arg("png");
	QString imagePath = pathToImage + imageFileName;

	//// find the image file
	//QFile fileImageRaw(imagePath);

	////if it doesn't exists check for different image file formats
	//if (!fileImageRaw.exists()) {
	//	imagePath = checkForImageFileFormat(imagePath);
	//	if(imagePath.isEmpty())
	//		return Mat();
	//}

	//get correct imagePath, with the correct image file format suffix (defualt is png)
	imagePath = checkForImageFileFormat(imagePath);
	//if there is no image file
	if(imagePath.isEmpty())
		return Mat();
	//else {

		// IT EXPECTS A 16-bit gain image from Michi's script
		Mat gainImg = cv::imread(imagePath.toStdString(), CV_LOAD_IMAGE_ANYDEPTH | CV_LOAD_IMAGE_GRAYSCALE);
		// convert image format
		int imgType = gainImg.type();


		// Instructions from Michi Schwarzfischer 
		// Gain is only divided by scaled /2^16-1 (as 16-bit image is expected) and multiplied with *255 according to Michi (because of the way the image has been calculated)
		Mat imgOut, imgOut2;
		

		Mat imgConv;
		gainImg.convertTo(imgConv, CV_64F);

		cv::divide(imgConv, /*2^16-1*/ 65535, imgOut);
		
		cv::multiply(imgOut, 255, imgOut2);

		return imgOut2;
	//}
}

Mat QTFyImageOperations::loadSegmentationImage (const QString _pathtoExperiment, const QString _expName, const QString _pos, const int _timepoint, const int _zindex, const int _segmentationMethod, const int _wlSeg)
{

	QString pathToImage = _pathtoExperiment +  "\\" + _pos + "\\";

	//load segmentation image
	QString pathToSegmentationImage = pathToImage + "segmentation\\";

	QString segImageFileName = getSegmentationImageFileName(_pos, _timepoint, _zindex, _wlSeg, _segmentationMethod, 3, 2, "png");
	QString segImagePath = pathToSegmentationImage + segImageFileName;

	QFile fileImageSeg(segImagePath);

	Mat segmentationImage;

	//if it doesn't exists check for different image file formats
	if (!fileImageSeg.exists()) {
		segImagePath = checkForImageFileFormat(segImagePath);
	}

	//if invalid image path
	if(segImagePath.isEmpty())
		QString message = "Looking for file: \n" + segImageFileName + "\nin\n" + pathToImage + "Segmentation\\" + "\nThe file cannot be found. Generating an empty segmentation image instead...";
	else
		segmentationImage = cv::imread(segImagePath.toStdString(), CV_LOAD_IMAGE_ANYDEPTH | CV_LOAD_IMAGE_GRAYSCALE);

	return segmentationImage;
	//if (!fileImageSeg.exists()) {

	//	QString message = "Looking for file: \n" + segImageFileName + "\nin\n" + pathToImage + "Segmentation\\" + "\nThe file cannot be found. Generating an empty segmentation image instead...";

	//}
	//else {
	//	segmentationImage = cv::imread(segImagePath.toStdString(), CV_LOAD_IMAGE_ANYDEPTH | CV_LOAD_IMAGE_GRAYSCALE);
	//}

	//return segmentationImage;

}

QString QTFyImageOperations::getImageFileName( QString m_baseName, int timePoint, int zIndex, int wl, int ZTagsLength, int WlTagsLength, QString ImageExtension )
{
	return QString("%1_t%2_z%3_w%4.%5").arg(m_baseName).arg(timePoint, 5, 10, QChar('0')).arg(zIndex, ZTagsLength, 10, QChar('0')).arg(wl, WlTagsLength, 10, QChar('0')).arg(ImageExtension);
}

QString QTFyImageOperations::getSegmentationImageFileName( QString m_baseName, int timePoint, int zIndex, int wl, int segMeth, int ZTagsLength, int WlTagsLength, QString ImageExtension )
{
	return QString("%1_t%2_z%3_w%4_m%5_mask.%6").arg(m_baseName).arg(timePoint, 5, 10, QChar('0')).arg(zIndex, ZTagsLength, 10, QChar('0')).arg(wl, WlTagsLength, 10, QChar('0')).arg(segMeth, 2, 10, QChar('0')).arg(ImageExtension);
}

vector<vector<Point> > QTFyImageOperations::getImageContours (Mat& _mask, int _minSize, int _maxSize, bool _filterMin, bool _filterMax) {

	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;

	Mat copyMat;
	_mask.copyTo(copyMat);
	findContours( copyMat, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE );


	// store here only contours that pass the filtering of area size
	vector<vector<Point> > contoursReturn;

	int contourNumber = 0;
	for( int i = 0; i< contours.size(); i++ )
	{
		double area = contourArea(contours.at(i));

		// try possible combinations of filtering for the contours
		// filter for both min and max size
		if (_filterMin && _filterMax) { 
			if (area>=_minSize && area<=_maxSize) {
				contoursReturn.push_back(contours[i]);
			}
		}
		// filter for min size only
		else if (_filterMin && !_filterMax) {
			if (area>=_minSize) {
				contoursReturn.push_back(contours[i]);
			}
		}
		// filter for max size only
		else if (!_filterMin && _filterMax) {
			if (area<=_maxSize) {
				contoursReturn.push_back(contours[i]);
			}
		}
		// don't filter and return all contours
		else if (!_filterMin && !_filterMax) {
			contoursReturn.push_back(contours[i]);
		}

	}

	// return the contours that passed the filtering criteria
	return contoursReturn;
}

QPointF QTFyImageOperations::calculateContourCentroid(vector<Point> &_contour) 
{
	Moments mu = moments(_contour,false);
	// the centroid
	Point2f massCenter = Point2f( mu.m10/mu.m00, mu.m01/mu.m00 );
	float x = massCenter.x;  // the x coordinate of the mass center
	float y = massCenter.y;  // the y coordinate of the mass center

	QPointF contourCentroid(x,y);
	return contourCentroid;
}

vector<Point> QTFyImageOperations::getClosestContour (vector<vector<Point> > _contours, Point _trackpoint,  double _maxDist)
{	
	if (_contours.empty())
		vector<Point>();

	int contourAtMinDist = -1; 

	for (int j=0; j < _contours.size(); j++){

		if (_contours.at(j).size() < 5)
			continue;

		double distance = pointPolygonTest(_contours.at(j), _trackpoint, true);

		// if the distance of the point is positive or O then the point belongs to the contour
		// it is actually within the contour area
		if (distance >= 0)
			return _contours.at(j);

		// if the distance is negative find the contour with the minimun absolute distance from point
		// (when the trackpoint has been placed outside of the contour)

		if (std::abs(distance)<_maxDist) {
			_maxDist = std::abs(distance);
			contourAtMinDist = j;
		}
	}

	if (contourAtMinDist<0)
		return vector<Point>();

	return _contours.at(contourAtMinDist);
}


contourGraphicsItem* QTFyImageOperations::getClosestContour (QList<contourGraphicsItem*> _contours, Point _trackpoint,  double _maxDist)
{

	double minDist = std::numeric_limits<double>::max();  
	int contourAtMinDist = -1; 

	for (int j=0; j < _contours.size(); j++){

		vector<Point> contourVector = _contours.at(j)->getContourPoints();

		if (contourVector.empty())
			continue;

		if (contourVector.size() < 5)
			continue;

		double distance = pointPolygonTest(contourVector, _trackpoint, true);

		// if the distance of the point is positive or O then the point belongs to the contour
		if (distance >= 0)
			return _contours.at(j);

		// if the distance is negative find the contour with the minimum absolute distance from point
		// (when the trackpoint has been placed outside of the contour)

		if (std::abs(distance) < _maxDist) {
			minDist = std::abs(distance);
			contourAtMinDist = j;
		}
	}

	if (contourAtMinDist<0)
		return NULL;

	return _contours.at(contourAtMinDist);
}


QPoint QTFyImageOperations::calcTransformedCoords (QPointF _absCoords, PositionInformation *_positionInformation, bool coordinateSystemIsInverted, bool _inMicrometer, bool _oldInvertedSystemReading) 
{


	if (! _positionInformation)
		return QPoint (-1, -1);

	if (! _positionInformation->coordinatesSet())
		return QPoint (-1, -1);

	//use new coordinate scheme
	//unit is micrometer and the coordinates of all tracks are global in the experiment

	float left = _positionInformation->getLeft();
	float top = _positionInformation->getTop();


	//two steps:
	//1) transform the micrometer coordinates to picture global pixels with the information about the position location
	//2) calculate the coordinates in pixel as before (the coordinate origin is the topleft corner of the position)

	//1)
	//subtract position offsets, but regard if coordinate system is inverted
	float x = 0;
	float y = 0;


	///if an elder experiment is inverted, and the track points are stored in the wrong way
	/// (x/y mess with inverted systems), then
	///  - read the file in the old mode
	///  - calculate transcoords for each trackpoint in old mode (without the if-query here)
	///  - calculate abscoords for each trackpoint in new mode (no change in program)
	///  - save the file

	//bool coordinateSystemIsInverted = TTTManager::getInst().coordinateSystemIsInverted();
	//bool coordinateSystemIsInverted = true;

	if ( coordinateSystemIsInverted && (! _oldInvertedSystemReading)) {
		//inverted style: x/y increase to left/top
		x = left - _absCoords.x();
		y = top - _absCoords.y();
	}
	else {
		//normal style: x/y increase to right/bottom
		x = _absCoords.x() - left;
		y = _absCoords.y() - top;
	}

	// check here that the microscope doesn't use inverted coordinate system
	// if the coordinates are negative change the value in the TTTManager
	// and obtain the transformed coordinated with the new setting
	if (x < 0 || y < 0) {

		TTTManager::getInst().setInvertedCoordinateSystem(!coordinateSystemIsInverted);
		return calcTransformedCoords (_absCoords, _positionInformation, !coordinateSystemIsInverted, _inMicrometer, _oldInvertedSystemReading);

	}
	

	//note: wavelength 0 can be used as all other wavelengths are stretched to have the same size as 0, if necessary
	//      thus the picture global coordinates always refer (at least indirectly) to wavelength 0
	//      -> we do not need to care about other binning factor of the different wavelengths
	float mmpp = TATInformation::getInst()->getWavelengthInfo (0).getMicrometerPerPixel();
	x = x / mmpp;
	y = y / mmpp;


	return QPoint (x, y);

}

Mat QTFyImageOperations::getSingleContourMask(vector<Point> &_contour, int _width, int _height)
{
	// create a black image
	Mat singleContourMask = Mat::zeros(_width, _height, CV_8UC1);
	std::vector<std::vector<cv::Point> > contourVec;
	contourVec.push_back(_contour);

	// draw the contour on the black image to create a mask image
	cv::drawContours( singleContourMask, contourVec, 0, Scalar(255, 255,255), CV_FILLED, 8, vector<Vec4i>(), 2);

	return singleContourMask.clone();
}

bool QTFyImageOperations::getSingleContourImage(Mat _rawImage, Mat _singleContourMask, Mat& _singleContourImage, bool bf) 
{
	if (!_rawImage.empty()) {
		//if (bf) {
		//	Mat temp = _rawImage.clone();
		//	// normalise background image ?
		//	double minVal, maxVal;
		//	minMaxLoc(_rawImage, &minVal, &maxVal); //find minimum and maximum intensities

		//	Mat src_U8;
		//	//temp.convertTo(src_U8, CV_8UC1, 255.0/(maxVal - minVal), -minVal * 255.0/(maxVal - minVal));
		//	temp.convertTo(src_U8, CV_64F, 255.0/(maxVal - minVal), -minVal * 255.0/(maxVal - minVal));
		//	int test2 = src_U8;
		//	// AND the normalized BF image and the contour mask image 
		//	// bitwise_and(src_U8, singleContourMask, singleContourImage);
		//	int test = temp.depth();
		//	temp.copyTo(_singleContourImage, _singleContourMask);
		//	src_U8.copyTo(_singleContourImage, _singleContourMask);
		//	
		//}
		//else
			_rawImage.copyTo(_singleContourImage, _singleContourMask);
	}

	return true;
}

QMap <QString, double> QTFyImageOperations::calculateIntensityFeaturesWtBackground( bool bf, vector<Point> &_contour, Mat _src, Mat _backgroundImg, Mat _gainImg)
{
	// intensity related
	// check if image is a valid image

	// of raw image
	double Sum = std::numeric_limits<double>::quiet_NaN();
	double Average = std::numeric_limits<double>::quiet_NaN();
	double StdDev = std::numeric_limits<double>::quiet_NaN();
	double CoefficientVariation = std::numeric_limits<double>::quiet_NaN();

	// background correction: (pixel_value/ backgroundPixel_value) - 1
	double SumImgDivByBgMinus1 = std::numeric_limits<double>::quiet_NaN();
	double AverageImgDivByBgMinus1 = std::numeric_limits<double>::quiet_NaN();
	double StdDevImgDivByBgMinus1 = std::numeric_limits<double>::quiet_NaN();
	double CVImgDivByBgMinus1 = std::numeric_limits<double>::quiet_NaN();

	// background correction: pixel_value - backgroundPixel_value
	double SumImgMinusBg = std::numeric_limits<double>::quiet_NaN();
	double AverageImgMinusBg = std::numeric_limits<double>::quiet_NaN();
	double StdDevImgMinusBg = std::numeric_limits<double>::quiet_NaN();
	double CVImgMinusBg = std::numeric_limits<double>::quiet_NaN();

	// background correction: (pixel_value - backgroundPixel_value) / gainPixel_value
	double SumBgCorrected = std::numeric_limits<double>::quiet_NaN();
	double AverageBgCorrected = std::numeric_limits<double>::quiet_NaN();
	double StdDevBgCorrected = std::numeric_limits<double>::quiet_NaN();
	double CVBgCorrected = std::numeric_limits<double>::quiet_NaN();


	if (!_src.empty()) {

		// No background correction
		Mat singleContourMask =  getSingleContourMask(_contour, _src.rows, _src.cols);
		Mat singleContourImage = Mat::zeros(_src.rows, _src.cols, CV_64F); 			
		getSingleContourImage(_src, singleContourMask, singleContourImage, bf);
		getIntensityFeatures(singleContourImage, singleContourMask, Sum, Average, StdDev, CoefficientVariation);


		//  background correction: (pixel_value/ backgroundPixel_value) - 1
		if (!_backgroundImg.empty()) {	


			Mat numerator1, adjustedImg;
			cv::divide(_src, _backgroundImg, numerator1);
			cv::subtract(numerator1, 1, adjustedImg);

			singleContourImage = Mat::zeros(_src.rows, _src.cols,  CV_64F); 			
			getSingleContourImage(adjustedImg, singleContourMask, singleContourImage, bf);
			getIntensityFeatures(singleContourImage, singleContourMask, SumImgDivByBgMinus1, AverageImgDivByBgMinus1, StdDevImgDivByBgMinus1, CVImgDivByBgMinus1);

			// background correction: pixel_value - backgroundPixel_value
			Mat adjustedImg2;
			cv::subtract(_src, _backgroundImg, adjustedImg2);
			singleContourImage = Mat::zeros(_src.rows, _src.cols,  CV_64F); 			
			getSingleContourImage(adjustedImg2, singleContourMask, singleContourImage, bf);
			getIntensityFeatures(singleContourImage, singleContourMask, SumImgMinusBg, AverageImgMinusBg, StdDevImgMinusBg, CVImgMinusBg);
		}

		// background correction: (pixel_value - backgroundPixel_value) / gainPixel_value
		if (!_gainImg.empty() && !_backgroundImg.empty()) {

			Mat numerator, adjustedImg3;

			cv::subtract(_src, _backgroundImg, numerator);
			cv::divide(numerator, _gainImg, adjustedImg3);
 			
			singleContourImage = Mat::zeros(_src.rows, _src.cols, CV_64F); 			
			getSingleContourImage(adjustedImg3, singleContourMask, singleContourImage, bf);
			getIntensityFeatures(singleContourImage, singleContourMask, SumBgCorrected, AverageBgCorrected, StdDevBgCorrected, CVBgCorrected);
		}

	} 

	QMap <QString, double> quantifications; 

	quantifications.insert("SumNoBgCorrected", Sum);
	quantifications.insert("MeanNoBgCorrected", Average);
	quantifications.insert("StdDevNoBgCorrected", StdDev);
	quantifications.insert("CVNoBgCorrected", CoefficientVariation);

	quantifications.insert("SumImgDivByBgMinus1", SumImgDivByBgMinus1);
	quantifications.insert("MeanImgDivByBgMinus1", AverageImgDivByBgMinus1);
	quantifications.insert("StdDevImgDivByBgMinus1", StdDevImgDivByBgMinus1);
	quantifications.insert("CVImgDivByBgMinus1", CVImgDivByBgMinus1);

	quantifications.insert("SumImgMinusBg", SumImgMinusBg);
	quantifications.insert("MeanImgMinusBg", AverageImgMinusBg);
	quantifications.insert("StdDevImgMinusBg", StdDevImgMinusBg);
	quantifications.insert("CVImgMinusBg", CVImgMinusBg);

	quantifications.insert("SumBgCorrected", SumBgCorrected);
	quantifications.insert("MeanBgCorrected", AverageBgCorrected);
	quantifications.insert("StdDevBgCorrected", StdDevBgCorrected);
	quantifications.insert("CVBgCorrected", CVBgCorrected);
	

	return quantifications;
}


QMap <QString, double> QTFyImageOperations::calculateMorphologyFeatures( vector<Point> &_contour)
{
	//initilize values with nan
	double Area = std::numeric_limits<double>::quiet_NaN();
	double Perimeter = std::numeric_limits<double>::quiet_NaN();

	if(!_contour.empty()){
		Area = contourArea(_contour);  // the area
		Perimeter = arcLength(_contour, true);  // the perimeter
	}

	QMap <QString, double> quantifications; 
	quantifications.insert("AreaMorphology", Area);
	quantifications.insert("PerimeterMorphology", Perimeter);

	return quantifications;
}


bool QTFyImageOperations::getIntensityFeatures(Mat& _contourImage, Mat& _contourMask, double& _Sum, double& _Average, double& _StdDev, double& _CV) 
{		
	if (_contourImage.empty() || _contourMask.empty())
		return false;

	if (_contourImage.channels() > 1)
		cvtColor(_contourImage, _contourImage, CV_RGB2GRAY);

	// get the sum
	bool first = true;

	// there shouldn't be an image with different depth here
	if (_contourImage.depth() == CV_64F) {
		for (int i = 0; i < _contourImage.rows; i++) {
			for (int j = 0; j < _contourImage.cols; j++) {

				uchar maskPixelValue = _contourMask.at<uchar>(i,j);
				if ( maskPixelValue != 0) {
					double pixelIntensity = _contourImage.at<double>(i,j); // the label
					if (first) {
						_Sum = pixelIntensity;  
						first = false;
					} 
					else {
						_Sum = _Sum + pixelIntensity;
					}      
				}     
			}
		}
	}

	// get the sum
	// _Sum = cv::sum(_contourImage)[0];

	//qDebug() << singleContourImage.type();
	// a little check
	// the sum divided by the number of non-zero elements should be equal to the mean
	/*int numOfNonZeros = countNonZero(_contourImage);
	_Average = _Sum / numOfNonZeros;*/

	Scalar     mean;
	Scalar     stddev;
	cv::meanStdDev(_contourImage, mean, stddev, _contourMask);

	// get the mean
	_Average = mean.val[0];

	// get the std
	_StdDev = stddev.val[0];

	// get the coefficient of variation
	_CV = (_StdDev / _Average) * 100;

	return true;
}

Mat QTFyImageOperations::convertTo64F(Mat& _src)
{
	Mat imgOut;

	int imageType = _src.type();
	int imageDepth = _src.depth();

	double scale = 1;

	// A.convertTo(B,CV_8U,255.0/(Max-Min),-255.0/Min);
	// the beta field should be -255.0*Min/(Max-Min) instead of -255.0/Min??

	if ( imageType == 0) { 
		// CV_8U
		scale = 1.0/255.0;
	}

	else if ( imageType == 1) { 
		// CV_8S
		scale = 1.0/128.0;  
	}

	else if ( imageType == 2) { 
		// CV_16U
		scale = 1.0/65535.0;  
	}

	else if ( imageType == 3) {
		// CV_16S
		// TODO this is wrong, add constant -32768 and divide image by 2
		scale = 1.0/32768.0;  
	}

	else if ( imageType == 4) { 
		// CV_32S
		scale =  1.0/2147483648.0;  
	}

	else if ( imageType == 5) { 
		// CV_32F
		// TODO this is wrong, add constant 3.40*10E-38 and divide image by 2
		scale =  1.0/3.40*10E-38;  
	}

	else if ( imageType == 6) {
		// CV_64F
		return _src;
	}

	else { 
		// Process for all other cases.
		return Mat();

	}

	_src.convertTo(imgOut, CV_64F, scale);

	return imgOut;

}

Mat QTFyImageOperations::thresholdROI (Mat& _imgFL, QMap<QString, QString> _options)
{
	if (_imgFL.empty())
		return Mat::zeros(_imgFL.rows, _imgFL.cols, CV_8UC1);

	Mat dst, output, outputF, normAdj;
	equalizeHist( _imgFL, normAdj );

	int kernelSize = _options.value("gaussianBlurKernelSize").toInt();
	double sigma = _options.value("gaussianBlurSigma").toDouble();
	//GaussianBlur( normAdj, dst, Size(kernelSize, kernelSize ), sigma, 0 );
	
	if (_options.value("useSmoothing") == "true")	
		blur( normAdj, dst, Size( kernelSize, kernelSize ), Point(-1,-1));
	else
		dst = normAdj;

	//GaussianBlur( normAdj, dst, Size( 5, 5 ), 0, 0 );

	//namedWindow( "Test window size", WINDOW_AUTOSIZE );// Create a window for display.
	//imshow("Test window size", dst);

	//waitKey(2000);      
	//destroyWindow("Test window size");

	int thresh = 0;
	if (_options.value("useAutomaticThreshold") == "true") {
		thresh = entropySplit(dst);
	}
	else {
		thresh = _options.value("intensityThreshold").toInt();
	}

	// threshold image
	cv::threshold(dst, output, thresh, 255, CV_THRESH_BINARY);

	int operation = cv::MORPH_OPEN;
	int morph_size = 1;
	int morph_elem = MORPH_ELLIPSE;
	Mat diamond5 = getStructuringElement(  morph_elem, Size( 2*morph_size + 1, 2*morph_size+1 ), Point( morph_size, morph_size ) );

	morphologyEx(output,outputF,operation,diamond5);

	if (_options.value("splitMergedCells") == "true") {
		//QTFyImageOperations::splitCells(outputF, QList<Point>(), _options);
		Mat markers = watershedROI(outputF);
		return markers;
	}

	return outputF;
}


Mat QTFyImageOperations::watershedROI (Mat& _src)
{
	// the _scr image is already a binary mask 
	// make sure it has three channels as required by the watershed method
	Mat markerMask, img, imgGray;

	_src.copyTo(img); // 1 channel

	Mat img3c;
	cvtColor(img, img3c, cv::COLOR_GRAY2BGR);

	// get the distance transform of the binary image
	Mat dist;
	cv::distanceTransform(img, dist, CV_DIST_L2, 3);
	cv::normalize(dist, dist, 0, 1., cv::NORM_MINMAX);

	// threshold to obtain the peaks
	cv::threshold(dist, dist, .5, 1., CV_THRESH_BINARY);

	// Dilate a bit the dist image
	//Mat kernel1 = Mat::ones(3, 3, CV_8UC1);  // TODO put kernel size in the options??
	//erode(dist, dist, kernel1);
	//imshow("Peaks", dist);


	//namedWindow( "Test window size", WINDOW_AUTOSIZE );// Create a window for display.
	//imshow("Test window size", dist);

	//waitKey(2000);      
	//destroyWindow("Test window size");

	// Create the CV_8U version of the distance image
	// It is needed for cv::findContours()
	cv::Mat dist_8u;
	dist.convertTo(dist_8u, CV_8U);

	// Find total markers
	std::vector<std::vector<cv::Point> > contours;
	cv::findContours(dist_8u, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
	
	// Total objects
	int ncomp = contours.size();

	if( contours.empty() )
		return _src;

	cv::Mat markers = cv::Mat::zeros(dist.size(), CV_32SC1);
	for (int i = 0; i < ncomp; i++)
		cv::drawContours(markers, contours, i, cv::Scalar::all(i+1), -1);
	
	// put a marker for the background near the image border
	// cv::circle(markers, cv::Point(5,5), 3, CV_RGB(255,255,255), -1);

	watershed( img3c, markers );

	// Generate random colors
	std::vector<cv::Vec3b> colors;
	for (int i = 0; i < ncomp; i++)
	{
		int b = cv::theRNG().uniform(0, 255);
		int g = cv::theRNG().uniform(0, 255);
		int r = cv::theRNG().uniform(0, 255);

		colors.push_back(cv::Vec3b((uchar)b, (uchar)g, (uchar)r));
	}

	//// Create the result image
	//cv::Mat dst = cv::Mat::zeros(markers.size(), CV_8UC3);

	//// Fill labeled objects with random colors
	//for (int i = 0; i < markers.rows; i++)
	//{
	//	for (int j = 0; j < markers.cols; j++)
	//	{
	//		int index = markers.at<int>(i,j);
	//		if (index > 0 && index <= ncomp)
	//			dst.at<cv::Vec3b>(i,j) = colors[index-1];
	//		else
	//			dst.at<cv::Vec3b>(i,j) = cv::Vec3b(0,0,0);
	//	}
	//}

	//cv::imshow("dst", dst);
	return markers;
}


//Mat QTFyImageOperations::thresholdROI (Mat& _imgFL, QMap<QString, QString> _options)
//{
//	if (_imgFL.empty())
//		return Mat::zeros(_imgFL.rows, _imgFL.cols, CV_8UC1);
//
//	//int imgType =  _imgFL.type();
//
//	namedWindow( "Test window size", WINDOW_AUTOSIZE );// Create a window for display.
//	imshow("Test window size", _imgFL);
//
//	waitKey(2000);      
//	destroyWindow("Test window size");
//
//	// TODO:: decide and clean up!!!
//	// first option for segmentation
//	//// equalizeHist
//	//Mat dst, normAdj, finalThres, dilatedThres;
//	//equalizeHist( _imgFL, dst );
//
//	////namedWindow( "Test window size", WINDOW_AUTOSIZE );// Create a window for display.
//	////imshow("Test window size", dst);
//
//	////waitKey(2000);      
//	////destroyWindow("Test window size");
//
//	//// apply contrast adjustment
//	//normAdj = QTFyImageOperations::applyCLAHE(dst, _options.value("claheWinSize").toDouble(),  _options.value("claheClipLimit").toDouble());
//
//	////namedWindow( "Test window size", WINDOW_AUTOSIZE );// Create a window for display.
//	////imshow("Test window size", normAdj);
//
//	////waitKey(2000);      
//	////destroyWindow("Test window size");
//
//	//int thresh = entropySplit(normAdj);
//
//	//// threshold image
//	//cv::threshold(normAdj, finalThres, thresh, 255, CV_THRESH_BINARY);
//
//	////namedWindow( "Test window size", WINDOW_AUTOSIZE );// Create a window for display.
//	////imshow("Test window size", finalThres);
//
//	////waitKey(2000);      
//	////destroyWindow("Test window size");
//
//	//// dilate
//	//int dilation_size = 1;
//	//int dilation_type = MORPH_ELLIPSE; 
//
//	//Mat element = getStructuringElement( dilation_type,
//	//	Size( 2*dilation_size + 1, 2*dilation_size+1 ),
//	//	Point( dilation_size, dilation_size ) );
//
//	///// Apply the erosion operation
//	//erode( finalThres, dilatedThres, element );
//
//	//namedWindow( "Test window size", WINDOW_AUTOSIZE );// Create a window for display.
//	//imshow("Test window size", dilatedThres);
//
//	//waitKey(2000);      
//	//destroyWindow("Test window size");
//
//
//	// test 2nd option
//	Mat dist, output, outputF, normAdj;
//	// apply contrast adjustment
//	// normAdj = QTFyImageOperations::applyCLAHE(_imgFL, _options.value("claheWinSize").toDouble(),  _options.value("claheClipLimit").toDouble());
//	equalizeHist( _imgFL, normAdj );
//
//	namedWindow( "Test window size", WINDOW_AUTOSIZE );// Create a window for display.
//	imshow("Test window size", normAdj);
//
//	waitKey(2000);      
//	destroyWindow("Test window size");
//
//	GaussianBlur( normAdj, dist, Size( 5, 5 ), 0, 0 );
//
//	namedWindow( "Test window size", WINDOW_AUTOSIZE );// Create a window for display.
//	imshow("Test window size", dist);
//
//	waitKey(2000);      
//	destroyWindow("Test window size");
//
//	// output = _imgFL < dist;
//
//	//cv::adaptiveThreshold(_imgFL, output, 255, cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY, 30, 1.5);
//	
//	//cv::threshold(dist, output, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU	);
//
//	int thresh = entropySplit(dist);
//
//	// threshold image
//	cv::threshold(dist, output, thresh, 255, CV_THRESH_BINARY);
//
//	namedWindow( "Test window size", WINDOW_AUTOSIZE );// Create a window for display.
//	imshow("Test window size", output);
//
//	waitKey(2000);      
//	destroyWindow("Test window size");
//
//	int operation = cv::MORPH_OPEN;
//	int morph_size = 1;
//	int morph_elem = MORPH_ELLIPSE;
//	Mat diamond5 = getStructuringElement(  morph_elem, Size( 2*morph_size + 1, 2*morph_size+1 ), Point( morph_size, morph_size ) );
//
//	morphologyEx(output,outputF,operation,diamond5);
//
//	namedWindow( "Test window size", WINDOW_AUTOSIZE );// Create a window for display.
//	imshow("Test window size", outputF);
//
//	waitKey(2000);      
//	destroyWindow("Test window size");
//
//	// return dilatedThres;
//	return outputF;
//}


void QTFyImageOperations::plotContours (Mat& _mask, vector<vector<Point> > _contours, bool _isFilled, Scalar _color) 
{
	for( int i = 0; i< _contours.size(); i++ )
	{
		// Scalar colorOfContour( 255, 255, 255 );
		if (_isFilled)
			cv::drawContours( _mask, _contours, i, _color, CV_FILLED, 8);
		else
			cv::drawContours( _mask, _contours, i, _color, 2, 8);
	}
}


 Mat QTFyImageOperations::splitCells(Mat& _src, QList<Point> _markers, QMap<QString, QString> _options)
 {
	 int markSize = 5;

	 Mat distTransform, labels;
	 cv::distanceTransform(_src, distTransform, labels, CV_DIST_L1, markSize);

	 double min, max;
	 cv::minMaxLoc(distTransform, &min, &max);
	 
	 Mat fg, fg32s;
	 threshold(distTransform, fg, 0.7*max, 255, CV_THRESH_BINARY);

	 //namedWindow( "Test window size", WINDOW_AUTOSIZE );// Create a window for display.
	 //imshow("Test window size", distTransform);

	 //waitKey(2000);      
	 //destroyWindow("Test window size");

	 // use the threshold image to create the markers for watershed
	 vector<vector<Point> > contours;
	 vector<Vec4i> hierarchy;

	 Mat fg8u2;
	 fg.convertTo(fg8u2, CV_8UC1);
	 findContours( fg8u2, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	 Mat markers(fg.size(), CV_32S);
	 markers = Scalar::all(0);
	 int idx = 0;
	 int compCount = 0;

	 for( ; idx >= 0; idx = hierarchy[idx][0], compCount++ ) {
		 double area0 = contourArea(contours.at(idx));
		 // exclude very small markers as likely artifacts (number 5 here is arbritary - think of a better rule?)
		 if (area0 >= 5) {
			Scalar contourLabel( rand()&255, rand()&255, rand()&255 );
			drawContours(markers, contours, idx, Scalar::all(compCount+1), -1, 8, hierarchy, INT_MAX);
		 }
	 }

	 // convert to appropriate image type for watershed method
	 Mat src8c3;
	 _src.convertTo(src8c3, CV_8UC3);
	 cv::cvtColor(src8c3, src8c3, COLOR_GRAY2RGB);

	 // Create watershed segmentation object
	 WatershedSegmenter segmenter;

	 // Set markers and process
	 segmenter.setMarkers(markers);
	 Mat out = segmenter.process(src8c3);

	
	 // Display watersheds
	 //cv::namedWindow("Watersheds");
	 //cv::imshow("Watersheds",segmenter.getWatersheds());


	 Mat wshed(out.size(), CV_8UC3);
	 // paint the watershed image
	 for( int i = 0; i < out.rows; i++ ) {
		 for( int j = 0; j < out.cols; j++ )
		 {
			 int index = out.at<int>(i,j);
			 if( index <= 0 || index > compCount )
				 wshed.at<Vec3b>(i,j) = Vec3b(0,0,0);
			 else
				 wshed.at<Vec3b>(i,j) = Vec3b(255,255,255);
		 }
	 }

	 // dilate the contour from watershed

	 wshed = wshed*0.5 + src8c3*0.5;
	 
	 /*Mat wshed8UC1;
	 wshed.convertTo(wshed8UC1, CV_8UC1);*/

	 cv::cvtColor(wshed, wshed, COLOR_BGR2GRAY);

	 // threshold the watershed result
	 Mat finalThres, threshDilate;
	 cv::threshold(wshed, finalThres, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);
	 
	 // erode the contour from watershed
	 // erode
	 int dilation_size = 1;
	 int dilation_type = MORPH_ELLIPSE; 

	 Mat element = getStructuringElement( dilation_type,
		 Size( 2*dilation_size + 1, 2*dilation_size+1 ),
		 Point( dilation_size, dilation_size ) );

	 erode( finalThres, threshDilate, element );

	 //cv::imshow("Dilation", threshDilate);

	 //imshow( "watershed transform", finalThres );

	 markers.convertTo(markers, CV_8UC1);
	 normalize(markers, markers, 0, 255, NORM_MINMAX);
	 //cv::imshow("Labels", markers);

	 return threshDilate;

 }

Mat QTFyImageOperations::getImageROI (const Mat& _src, const Point _marker, const double _roiSize)
{

	// if plotting the initial contour image is required
	Mat out = Mat::zeros( _roiSize, _roiSize, CV_8UC1);

	if (_src.empty() ) 
		return out; // TODO:: Put error message here

	Rect rect;

	int x = _marker.x - _roiSize/2;
	int y = _marker.y - _roiSize/2;
	int width = _roiSize;
	int height = _roiSize;

	// rect is out of the image boundaries (causes crash)
	if ((x + _roiSize) > _src.size().width)
		width = _src.size().width - x;

	if ((y + _roiSize) > _src.size().height)
		height = _src.size().height - y;

	if (x > _src.size().width || y > _src.size().height)
		return out;

	if (x < 0)
		x = 0;

	if (y < 0)
		y = 0;

	rect = Rect( x, y, width, height);

	out = _src(rect);

	return out;
}


cv::Mat QTFyImageOperations::QImageToMat(QImage const& src, int format)
{
	// 1st attempt
	//   cv::Mat tmp(src.height(), src.width(), format, (uchar*)src.bits(), src.bytesPerLine());
	//   cv::Mat result; // deep copy just in case (my lack of knowledge with open cv)
	//   // cvtColor(tmp, result,CV_BGR2RGB);
	//   // return result;
	//return tmp;

	// 2nd attempt
	// cv::Mat  mat( src.height(), src.width(), format, const_cast<uchar*>(src.bits()), src.bytesPerLine() );

	// 3rd attempt
	bool inCloneImageData = true;

	int imgFormat = src.format();

	switch (imgFormat)
	{
		// 8-bit, 4 channel
	case QImage::Format_RGB32:
		{
			cv::Mat  mat( src.height(), src.width(), CV_8UC4, const_cast<uchar*>(src.bits()), src.bytesPerLine() );
			Mat imageOut;
			cvtColor(mat, imageOut, COLOR_BGR2GRAY);
			return (inCloneImageData ? imageOut.clone() : imageOut);
		}

		// 8-bit, 3 channel
	case QImage::Format_RGB888:
		{
			if ( !inCloneImageData )
				qWarning() << "ASM::QImageToCvMat() - Conversion requires cloning since we use a temporary QImage";

			QImage   swapped = src.rgbSwapped();

			return cv::Mat( swapped.height(), swapped.width(), CV_8UC3, const_cast<uchar*>(swapped.bits()), swapped.bytesPerLine() ).clone();
		}

		// 8-bit, 1 channel
	case QImage::Format_Indexed8:
		{
			cv::Mat  mat( src.height(), src.width(), CV_8UC1, const_cast<uchar*>(src.bits()), src.bytesPerLine() );

			return (inCloneImageData ? mat.clone() : mat);
		}

	default:
		qWarning() << "ASM::QImageToCvMat() - QImage format not handled in switch:" << imgFormat;
		break;
	}

	return cv::Mat();

}



bool QTFyImageOperations::saveQImageToFile(const QImage& img, const QString& filename)
{
	QImageWriter imagefile;
	imagefile.setFileName(filename);
	int index = filename.indexOf(".");
	QString suffix = filename;
	suffix = suffix.right(filename.size() - (index + 1));
	//imagefile.setFormat("png");
	QByteArray arr;
	arr.append(suffix);//converting string to QByteArray
	imagefile.setFormat(arr);
	imagefile.setQuality(100);
	imagefile.write(img);
	return true;	
}

bool QTFyImageOperations::saveSegmentationImage (const Mat _scr, const QString _pathtoExperiment, const QString _expName, const QString _pos, const int _timepoint, 
	const int _zindex, const int _segmentationMethod, const int _wlSeg)
{
	if (_scr.empty())
		return false;

	// get the segmentation image filename
	QString filename = getSegmentationImageFileName(_pos, _timepoint, _zindex, _wlSeg, _segmentationMethod, 3, 2, "png");
	QString pathToImage = _pathtoExperiment + "\\" + _pos + "\\";
	// look if the segmentation dir exists and if not create it
	QString outputfileDir = pathToImage + "segmentation\\";

	QDir dir(outputfileDir);
	if (!dir.exists()) {
		if ( !dir.mkpath(outputfileDir))
			return false;
	}
	QString outputfilePathImage =  pathToImage + "segmentation\\" + filename;
	
	bool outSuccess = cv::imwrite(outputfilePathImage.toStdString(), _scr);
	return outSuccess;
}

/**
* code modified from Stavroula Skylaki from here:
* Automatic thresholding technique based on the entopy of the histogram.
* See: P.K. Sahoo, S. Soltani, K.C. Wong and, Y.C. Chen "A Survey of
* Thresholding Techniques", Computer Vision, Graphics, and Image
* Processing, Vol. 41, pp.233-260, 1988.
*
* @author Jarek Sacha
*/


/**
  * Calculate maximum entropy split of a histogram.
  *
  * @param hist histogram to be thresholded.
  *
  * @return index of the maximum entropy split.`
  */
 int QTFyImageOperations::entropySplit(const Mat& _src) 
 {

	 /// Establish the number of bins
	 int histSize = 256;

	 /// Set the ranges 
	 float range[] = { 0, 256 } ;
	 const float* histRange = { range };

	 bool uniform = true; bool accumulate = false;

	 Mat hist;
	 calcHist( &_src, 1, 0, Mat(), hist, 1, &histSize, &histRange, uniform, accumulate );

	 // Normalize histogram, that is makes the sum of all bins equal to 1.
	 double sum = 0;
	 for (int i = 0; i < histSize; ++i) {
		 sum += ((float*) hist.data)[i];
	 }
	 if (sum == 0) {
		 // This should not normally happen, but...
		 return -1;
	 }

	 QVector<float> normalizedHist;

	 for (int i = 0; i < histSize; i++) {
		 normalizedHist.append(((float*) hist.data)[i] / sum);
	 }

	 //
	 QVector<float> pT ;
	 pT.insert(0, normalizedHist.at(0));
	 for (int i = 1; i <histSize; i++) {
		 pT.insert(i, pT.at(i - 1) + normalizedHist.at(i) );
	 }

	 // Entropy for black and white parts of the histogram
	 float epsilon = 0.0;
	 QVector<float> hB ;
	 QVector<float> hW ;
	 for (int t = 0; t < histSize; t++) {
		 // Black entropy
		 if (pT.at(t) > epsilon) {
			 double hhB = 0;
			 for (int i = 0; i <= t; i++) {
				 if (normalizedHist.at(i) > epsilon) {
					 hhB -= normalizedHist.at(i) / pT.at(t) * std::log(normalizedHist.at(i) / pT.at(t));
				 }
			 }
			 hB.insert(t, hhB);
		 } else {
			 hB.insert(t, 0);
		 }

		 // White  entropy
		 double pTW = 1 - pT.at(t);
		 if (pTW > epsilon) {
			 double hhW = 0;
			 for (int i = t + 1; i < histSize; ++i) {
				 if (normalizedHist.at(i) > epsilon) {
					 hhW -= normalizedHist.at(i) / pTW * std::log(normalizedHist.at(i) / pTW);
				 }
			 }
			 hW.insert(t, hhW);
		 } else {
			 hW.insert(t, 0);
		 }
	 }

	 // Find histogram index with maximum entropy
	 double jMax = hB.at(0) + hW.at(0);
	 int tMax = 0;
	 for (int t = 1; t < histSize; ++t) {
		 double j = hB.at(t) + hW.at(t);
		 if (j > jMax) {
			 jMax = j;
			 tMax = t;
		 }
	 }

	 // return value at position of identified index
	 // return ((float*) hist.data)[tMax];
	 return tMax;
 }


 Mat QTFyImageOperations::applyCLAHE (Mat& _src, int _windowSize, int _clipLimit)
 {
	 // apply the CLAHE algorithm
	 cv::Ptr<cv::CLAHE> clahe = cv::createCLAHE();
	 clahe->setClipLimit(_clipLimit);
	 clahe->setTilesGridSize(Size(_windowSize, _windowSize));
	 cv::Mat dst;
	 clahe->apply(_src, dst);

	 return dst;

 }


 
vector<Point> QTFyImageOperations::getAdjustedContourFromROI(const vector<Point> _srcContour, const int _scrImgWidth, const int _scrImageHeight, const int _roiSize, const Point _mrkPoint)
{
	int x = _mrkPoint.x - _roiSize/2;
	int y = _mrkPoint.y - _roiSize/2;
	int width = _roiSize;
	int height = _roiSize;

	// rect is out of the image boundaries (causes crash)
	if ((x + _roiSize) > _scrImgWidth)
		width = _scrImgWidth - x;

	if ((y + _roiSize) > _scrImageHeight)
		height = _scrImageHeight - y;

	if (x < 0)
		x = 0;

	if (y < 0)
		y = 0;

	if (_srcContour.size() < 3)
		return vector<Point>();

	vector<Point> adjSuggestedContour;

	// need to place the new contour at the right position of the original image
	foreach(Point p, _srcContour) {

		int adjX = p.x + x;
		int adjY = p.y + y;

		Point newP(adjX, adjY);
		adjSuggestedContour.push_back(newP);

	}

	return adjSuggestedContour;

}


QImage QTFyImageOperations::MatToQImage(const Mat& mat)
{
	// 8-bits unsigned, NO. OF CHANNELS=1
	if(mat.type()==CV_8UC1)
	{
		// Set the color table (used to translate colour indexes to qRgb values)
		QVector<QRgb> colorTable;
		for (int i=0; i<256; i++)
			colorTable.push_back(qRgb(i,i,i));
		// Copy input Mat
		const uchar *qImageBuffer = (const uchar*)mat.data;
		// Create QImage with same dimensions as input Mat
		QImage img(qImageBuffer, mat.cols, mat.rows, mat.step, QImage::Format_Indexed8);
		img.setColorTable(colorTable);
		return img;
	}
	// 8-bits unsigned, NO. OF CHANNELS=3
	else if(mat.type()==CV_8UC3)
	{
		// Copy input Mat
		const uchar *qImageBuffer = (const uchar*)mat.data;
		// Create QImage with same dimensions as input Mat
		QImage img(qImageBuffer, mat.cols, mat.rows, mat.step, QImage::Format_RGB888);
		return img.rgbSwapped();
	}
	else if (mat.type()==CV_32FC1)
	{
		Mat test;
		mat.convertTo(test, CV_8UC1, 255, 0);
		return MatToQImage(test);
	}
	else if (mat.type() == CV_32F)
	{
		Mat test;
		mat.convertTo(test, CV_8UC1, 255, 0);
		return MatToQImage(test);
	}
	else if (mat.type() == CV_16U)
	{
		Mat test;
		mat.convertTo(test, CV_8UC1, 255, 0);
		return MatToQImage(test);
	}
	else
	{
		qDebug() << "ERROR: Mat could not be converted to QImage.";
		return QImage();
	}
}


vector<vector <Point>> QTFyImageOperations::getLabeledComponentsContours(Mat _src, int _minSize, int _maxSize , bool _filterMin , bool _filterMax )
{
	// if watershed has been applied the thresholdROI image is a labeled object image 
	
	vector<vector <Point>> contoursAll;

	if (_src.depth() == CV_8U) {

		QList<uchar> visitedLabels;
		for (int i = 0; i < _src.rows; i++)	{
			for (int j = 0; j < _src.cols; j++)	{
				uchar index = _src.at<uchar>(i,j); // the label
				if (!visitedLabels.contains(index)) {
					visitedLabels.append(index);
				}
			}
		}

		// sort the indexes and remove smaller value as possibly the background
		qSort(visitedLabels);
		visitedLabels.removeAt(0);

		foreach(uchar index, visitedLabels) {
			cv::Mat regionIndex = _src == index; // get a mask of one labeled component only

			// find contours in ROI image
			// should be only one
			std::vector< std::vector<Point> > contoursIndex = getImageContours(regionIndex, _minSize, _maxSize, _filterMin, _filterMax);
			contoursAll.insert(contoursAll.end(), contoursIndex.begin(), contoursIndex.end());
		}
	}

	if (_src.depth() == CV_16U) {

		QList<ushort> visitedLabels;
		for (int i = 0; i < _src.rows; i++)	{
			for (int j = 0; j < _src.cols; j++)	{
				ushort index = _src.at<ushort>(i,j); // the label
				if (!visitedLabels.contains(index)) {
					visitedLabels.append(index);
				}
			}
		}

		// sort the indexes and remove smaller value as possibly the background
		qSort(visitedLabels);
		visitedLabels.removeAt(0);

		foreach(ushort index, visitedLabels) {
			cv::Mat regionIndex = _src == index; // get a mask of one labeled component only

			// find contours in ROI image
			// should be only one
			std::vector< std::vector<Point> > contoursIndex = getImageContours(regionIndex, _minSize, _maxSize, _filterMin, _filterMax);
			contoursAll.insert(contoursAll.end(), contoursIndex.begin(), contoursIndex.end());
		}
	}

	if (_src.depth() == CV_32S) {

		QList<int> visitedLabels;
		for (int i = 0; i < _src.rows; i++)
		{
			for (int j = 0; j < _src.cols; j++)
			{
				int index = _src.at<int>(i,j); // the label
				if (!visitedLabels.contains(index)) {
					visitedLabels.append(index);
				}
			}
		}

		// sort the indexes and remove smaller value as possibly the background
		qSort(visitedLabels);
		visitedLabels.removeAt(0);

		foreach(int index, visitedLabels) {
			cv::Mat regionIndex = _src == index; // get a mask of one labeled component only

			// find contours in ROI image
			// should be only one
			std::vector< std::vector<Point> > contoursIndex = getImageContours(regionIndex, _minSize, _maxSize, _filterMin, _filterMax);
			contoursAll.insert(contoursAll.end(), contoursIndex.begin(), contoursIndex.end());
		}
	}

	return contoursAll;
}


bool  QTFyImageOperations::isImageBinary(Mat& _src)
{
	return true;
}

bool  QTFyImageOperations::isImageLabeledComponents(Mat& _src)
{
	return true;
}


vector<uchar> QTFyImageOperations::getImageIntensityLevels(Mat& _src)
{
	// accept only char type matrices
	int test = _src.depth();
	CV_Assert(_src.depth() == CV_8U);

	vector<uchar> levels;

	const int channels = _src.channels();
	switch(channels)
	{
	case 1:
		{
			MatIterator_<uchar> it, end;
			for( it = _src.begin<uchar>(), end = _src.end<uchar>(); it != end; ++it) {
				if(!(std::find(levels.begin(), levels.end(), *it) != levels.end())) {
					/* levels contains *it */
					levels.push_back(*it);
				} 
			}				
			break;
		}
	case 3:
		{
			// something went wrong
			return vector<uchar>();
			/*MatIterator_<Vec3b> it, end;
			for( it = _src.begin<Vec3b>(), end = _src.end<Vec3b>(); it != end; ++it)
			{
				(*it)[0] = table[(*it)[0]];
				(*it)[1] = table[(*it)[1]];
				(*it)[2] = table[(*it)[2]];
			}*/
		}
	}

	return levels;
}

QString QTFyImageOperations::checkForImageFileFormat(QString _imagePath){

	// try to find image with png suffix first
	if(!QFile(_imagePath).exists()){
		//if it doesn't exist with jpeg suffix 
		//remove png suffix from image path
		QString path = _imagePath.remove("png");
		if (!QFile(path + "jpeg").exists()) {
			//with jpg suffix 
			if(!QFile(path + "jpg").exists()){
				//with tiff suffix
				if(!QFile(path + "tiff").exists()){
					//with tif suffix
					if(!QFile(path + "tif").exists())
						//image not found
						return 0;
					else
						return (path + "tif");
				}
				else
					return (path + "tiff");
			}
			else 
				return (path + "jpg");
		}
		else
			return (path + "jpeg");
	}else
		return (_imagePath);
}
