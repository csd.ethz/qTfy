/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Laura Skylaki
 * @date 14/08/2015
 */


#ifndef QTFyImageOperations_H
#define QTFyImageOperations_H

// PROJECT
#include "qtfydata/positioninformation.h"
#include "qtfysegeditor/contourGraphicsItem.h"

// QT
#include "QHash"
#include "QPointF"
#include "QImage"


// OPENCV
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"

using namespace cv;

class QTFyImageOperations
{

public:

	QTFyImageOperations();
	~QTFyImageOperations();

	/************************************************************************/
	/* methods for image loading                                            */
	/************************************************************************/
	// the following loading method also converts the image to CV_8UC1
	static Mat loadDetectionWavelengthImage(const QString _pathtoExperiment, const QString _expName, const QString _pos, const int _timepoint, const int _zindex, const int _wl);
	// the following loading method also converts the image to CV_64F
	static Mat loadWavelengthImage(const QString _pathtoExperiment, const QString _expName, const QString _pos, const int _timepoint, const int _zindex, const int _wl);
	// the following loading method also converts the image to CV_64F
	static Mat loadBackgroundImage(const QString _pathtoExperiment, const QString _expName, const QString _pos, const int _timepoint, const int _zindex, const int _wl);
	// the following loading method also converts the image to CV_64F
	static Mat loadGainImage(const QString _pathtoExperiment, const QString _expName, const QString _pos, const int _wl);
	
	/**
	Returns the segmentation image for the defined experiment, position, timepoint, segmentation method
	@param expName the experiment name
	@param pos the position as a text
	@param timepoint the timepoint
	@param zindex normally 1, almost never used so far (02.2015)
	@param segmentationMethod 0 for CAT segmentation, 2 for ESCseg segmentation
	@param wlSeg this is the wavelength from which the segmentation is derived
	@return the Mat image that fits the specified criteria or an empty Mat() if not available
	*/
	static Mat loadSegmentationImage (const QString _pathtoExperiment, const QString _expName, const QString _pos, const int _timepoint, const int _zindex, const int _segmentationMethod, const int _wlSeg);


	// returns the filename of a raw image
	// @param m_baseName the experiment basename
	// @param _timePoint the timepoint of the image
	// @param _zIndex the zindex of the image
	// @param _wl the wavelength (also called channel) of the image
	// @param _ZTagsLength how many integers are used for the z-stack tag (i.e. 3 for _z001)?
	// @param _WlTagsLength how many integers are used for the wavelength tag (i.e. 2 for _w00)?
	// @param _ImageExtension i.e. "png"
	// @return QString the image filename
	static QString getImageFileName( QString m_baseName, int _timePoint, int _zIndex, int _wl, int _ZTagsLength, int _WlTagsLength, QString _ImageExtension );

	// returns the filename of a segmentation image
	// @param m_baseName the experiment basename
	// @param _timePoint the timepoint of the image
	// @param _zIndex the zindex of the image
	// @param _wl the wavelength (also called channel) of the image
	// @param _segMeth the segmentation method id (i.e. _m00 for CAT, _m02 for ESCseg)
	// @param _ZTagsLength how many integers are used for the z-stack tag (i.e. 3 for _z001)?
	// @param _WlTagsLength how many integers are used for the wavelength tag (i.e. 2 for _w00)?
	// @param _ImageExtension i.e. "png"
	// @return QString the image filename
	static QString getSegmentationImageFileName( QString m_baseName, int _timePoint, int _zIndex, int _wl, int _segMeth, int _ZTagsLength, int _WlTagsLength, QString ImageExtension );

	/************************************************************************/
	/* methods for image saving                                             */
	/************************************************************************/
	
	/**
	Returns the segmentation image for the defined experiment, position, timepoint, segmentation method
	@param _src the Mat image that fits the specified criteria or an empty Mat() if not available
	@param expName the experiment name
	@param pos the position as a text
	@param timepoint the timepoint
	@param zindex normally 1, almost never used so far (02.2015)
	@param segmentationMethod 0 for CAT segmentation, 2 for ESCseg segmentation
	@param wlSeg this is the wavelength from which the segmentation is derived
	@return true if the image has been saved successfully
	*/
	static bool saveSegmentationImage (const Mat _scr, const QString _pathtoExperiment, const QString _expName, const QString _pos, const int _timepoint, const int _zindex, const int _segmentationMethod, const int _wlSeg);

	static bool saveQImageToFile(const QImage& img, const QString& filename);

	/************************************************************************/
	/* methods for contour handling and identification                      */
	/************************************************************************/
	
	// returns the identified contours in a binary mask image file
	// @param mask the mask binary image 
	// @param minSize the minimum acceptable size of a contour i.e. to filter noise 
	// @param maxSize the maximum acceptable size of a contour i.e. to filter large artifacts
	// @param filterMin should the minimum filter be applied?
	// @param filterMax should the maximum filter be applied?
	// @return vector<vector<Point> > the collection of the identified contours
	static vector<vector<Point> > getImageContours (Mat& _mask, int _minSize = 0, int _maxSize = 0, bool _filterMin = false, bool _filterMax = false);

	// get contour centroid
	// @param contour the collection of points that represent the contour
	// @return QPointf the centroid of the contour
	static QPointF calculateContourCentroid(vector<Point> &_contour); 

	// get the closest contour to a point
	// @param _contours the possible contours
	// @param _trackpoint the point to be considered
	// @param _maxDist the maximum distance that makes sense to consider i.e. no more than 500 pixels
	// @return vector<Point> the selected contour
	static vector<Point> getClosestContour (vector<vector<Point> > _contours, Point _trackpoint, double _maxDist =  std::numeric_limits<double>::max());

	static contourGraphicsItem* getClosestContour (QList<contourGraphicsItem*> _contours, Point _trackpoint, double _maxDist =  std::numeric_limits<double>::max());

	// returns the identified contours in a labeled components segmentation image
	// @param _src the labeled components image 
	// @return vector<vector<Point> > the collection of the identified contours
	static vector<vector <Point>> getLabeledComponentsContours(Mat _src, int _minSize = 0, int _maxSize = 0, bool _filterMin = false, bool _filterMax = false);

	// returns the relative coordinates within the current display (i.e. within the current position) (transforms the point with loaded and displayed region and zoom)
	// old@param _absCoords the logical coordinates (in the current picture coordinate system)
	// @param _absCoords the experiment relative coordinates in micrometer
	// @param _positionInformation the position information object (necessary for the position location within the experiment)
	// * @param _withoutDisplayShift whether the coordinates should NOT be shifted additionally by the bounds of the displayed region (default = false)
	// @param _inMicrometer whether the coordinates should be returned in micrometer (works correct only in combination with _withoutDisplayShift=true)
	// @param _oldInvertedSystemReading whether the old (wrong!) inverted system coordinates should be read (used only for converting the incorrect files)
	// @return the screen coordinates, regarding the current movie player settings (shift, zoom factor, displayed region, ...)
	//
	// new: these two methods only care about the picture translation (left/top corner), not at all about the display stuff	
	static QPoint calcTransformedCoords (QPointF _absCoords, PositionInformation *_positionInformation, bool coordinateSystemIsInverted, bool _inMicrometer = false, bool _oldInvertedSystemReading = false);

	// for intensity quantifications
	// draws the contour on a black image to create a mask image with only one contour
	// @param _contour the contour 
	// @param _width the width of the new image
	// @param _height the height of the new image
	// @return the new image
	static Mat getSingleContourMask(vector<Point> &_contour, int _width, int _height);

	// for intensity quantifications
	// modifies the _singleContourImage so it is black except the pixels that are members of the contour
	// @param _rawImage the raw image
	// @param _singleContourMask the mask file where only one contour exists, the one of the object to be quantified
	// @param _singleContourMask the mask image where only the pixels belong to the contour are 1	
	static bool getSingleContourImage(Mat _rawImage, Mat _singleContourMask, Mat& _singleContourImage, bool bf);

	// cell splitting using the watershed algorithm
	// uses the trackpoints as markers for the watershed
	// @param _src the raw image
	// @param _markers a list with the trackpoint locations on the image
	// @param _options the segmentation options from the interface
	// @return the output image with cell splitted
	static Mat splitCells(Mat& _src, QList<Point> _markers, QMap<QString, QString> _options);

	static Mat watershedROI (Mat& _src);

	// plots a vector of contours on an image
	// @param mask the image where the contours will be plotted, it is altered
	// @param _contours the list of contours
	static void plotContours (Mat& _mask, vector<vector<Point> > _contours, bool _isFilled = true, Scalar _color = Scalar( 255, 255, 255 ));

	// map the contour that has been generated in a ROI back to the original image
	// @param _srcContour the contour from the ROI
	// @param _scrImgWidth the width of the source image
	// @param _scrImageHeight the height of the source image
	// @param _roiSize the size of the ROI
	// @param _mrkPoint the marker point (defines the center of the ROI)
	// @return the adjusted contour points
	static vector<Point> getAdjustedContourFromROI(const vector<Point> _srcContour, const int _scrImgWidth, const int _scrImageHeight, const int _roiSize, const Point _mrkPoint);

	/************************************************************************/
	/* methods for quantification                                           */
	/************************************************************************/
	
	// performs the quantification of an object with identified contour
	// if background image and gain image are not available the equivalent features will be nan
	// @param _contour the contour of the cell
	// @param _wlImage the image of the wavelength (channel) to be quantified
	// @param _backgroundImg the background image for this wavelength (it needs to be in the background folder/position folder)
	// @param _gainImg the gain image for this wavelength (it needs to be in the background folder/position folder)
	// @return QMap <QString, double> returns a map with the quantified features - see method to recover feature names
	static QMap <QString, double> calculateIntensityFeaturesWtBackground(bool bf, vector<Point> &_contour, Mat _wlImage, Mat _backgroundImg = Mat(), Mat _gainImg = Mat());

	
	// calculates the morphology features of an object with identified contour
	// @param _contour the contour of the cell
	// @return QMap <QString, double> returns a map with the quantified features - see method to recover feature names
	static QMap <QString, double> calculateMorphologyFeatures(vector<Point> &_contour);/*, Mat _wlImage, Mat _backgroundImg = Mat(), Mat _gainImg = Mat());*/


	// calculates the intensity quantifications
	// it returns the sum, average, standard deviation and coefficient of variation for the intensities within the contour 
	// @param _contourImage the raw image where all pixels are black besides the one within the contour that have the original intensities
	// @param _contourMask a mask image where only the pixels within the contour are white
	// @param _Sum the sum of intensities of the pixels within the contour
	// @param _Average the average of intensities of the pixels within the contour
	// @param _StdDev the standard deviation of intensities of the pixels within the contour
	// @param _CV the coefficient of variation of intensities of the pixels within the contour
	static bool getIntensityFeatures( Mat& _contourImage, Mat& _contourMask, double& _Sum, double& _Average, double& _StdDev, double& _CV);

	/************************************************************************/
	/* methods for image categorisation                                     */
	/************************************************************************/
	static bool isImageBinary(Mat& _src);

	static bool isImageLabeledComponents(Mat& _src);

	static vector<uchar> getImageIntensityLevels(Mat& _src);

	/************************************************************************/
	/* methods for image conversion                                         */
	/************************************************************************/

	// identifies the type of the _src image and converts it to 64F for quantification 
	static Mat convertTo64F(Mat& _src);

	// QImage to Mat
	static Mat QImageToMat(QImage const& img, int format);

	// Mat to QImage
	static QImage MatToQImage(const Mat& mat);

	/************************************************************************/
	/* methods for segmentation                                             */
	/************************************************************************/

	// segments an image 
	// @param _imgFL the original image to be segmented
	// @param _options the options for segmentation
	static Mat thresholdROI (Mat& _imgFL, QMap<QString, QString> _options);

	// thresholding based on Max Entropy of the image histogram
	static int entropySplit(const Mat& _src) ; 
	
	static Mat applyCLAHE (Mat& _src, int _windowSize, int _clipLimit);

	// returns the ROI of an image based on the size of window around the marker point 
	// @param _src the original image 
	// @param _marker the center of the ROI
	// @param _roiSize the size of the window
	// @return the roi image
	static Mat getImageROI (const Mat& _src, const Point _marker, const double _roiSize);

	/************************************************************************/
	/* methods for allowing different file format usage                     */
	/************************************************************************/	
	// returns the path of the image, in the path added the correct image format suffix, 
	// or an empty string id the image wasn't fount
	// @param _imagePath the default image path (with the "png" suffix)
	static QString checkForImageFileFormat(QString _imagePath);
};

#endif // QTFyImageOperations_H
