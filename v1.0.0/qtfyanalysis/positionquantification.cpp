/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Stavroula Skylaki
 * @date 15/08/2014
 */

//Project
#include "positionquantification.h"
#include "qtfybackend\tttmanager.h"
#include "qtfybackend\tools.h"
// #include "qtfydata\userinfo.h"


QMap<QString, QString> PositionQuantification::options = QMap<QString,QString>();
QMap<int, QList<int>> PositionQuantification::wlCombos= QMap<int, QList<int>>();


PositionQuantification::PositionQuantification(QString _position, QString _pathToExperiments)
{
	this->positionIndex = _position;
	this->quantificationTerminationFlag = false;
	this->isSetInspectedMode = false;
}

PositionQuantification::~PositionQuantification()
{
	this->preloadedImages.clear();
}

void PositionQuantification::initializeData(QMap<int, QVector<ITrack*>> _cellsPerTimepoint, /*QMap<ITrack*, QVector<int>> _cellsWithMissingTimepoints,*/ QList<QSharedPointer<ITree>> _treesOfThisPosition)
{
	this->cellsPerTimepoint = _cellsPerTimepoint;
	//this->missingTimepoints = _cellsWithMissingTimepoints;
	this->treesForQuantification = _treesOfThisPosition;

}

//void PositionQuantification::setSegmentationInformation(QString _segMethod, int segWavelength)
//{
//	this->segMethod = _segMethod;
//	this->segWavelength = segWavelength;
//}

void PositionQuantification::run()
{

	bool quantifyOnlyMissingTimepoints;
	if(missingTimepoints.isEmpty())
		quantifyOnlyMissingTimepoints = false;
	else
		quantifyOnlyMissingTimepoints = true;

	// get the path to the image files
	QString path = TTTManager::getInst().getExperimentPath();
	QString experimentName;
	QString treeName;
	int cellID;
	int zStackIndex = 1;
	int segMethodID = 1;

	QMap<QString, QList<Mat>> imgToQuantify;

	experimentName = (positionIndex.split("_")).at(0);

	QList<int> tpnNumbers = this->cellsPerTimepoint.keys();
	
	foreach (int detectionWL, this->wlCombos.keys()) 
		checkAndUpdateDwSm(detectionWL, segMethodID);

	foreach (int timepoint, tpnNumbers) {

		// has the quantification been requested to stop from user?
		if (!this->quantificationTerminationFlag) {

			// get the cells that are present in this timepoint
			QVector<ITrack*> cells = this->cellsPerTimepoint.value(timepoint);

			// for each detection wavelength
			foreach (int detectionWL, this->wlCombos.keys()) {

				imgToQuantify = this->getQuantificationChannelsImages(detectionWL, path, experimentName, positionIndex, timepoint, zStackIndex);
				
				// if no wavelength image has been found
				// don't bother looking for segmentation image or create one
				if (imgToQuantify.isEmpty())
					continue;			

				vector<vector<Point> > imgContours = this->getContoursFromDetectionImage(path, experimentName, positionIndex, timepoint, zStackIndex, detectionWL, segMethodID, cells);

				if (imgContours.empty())
					continue;

				foreach (ITrack* cell, cells) {

					// sanity check for each cell
					// make sure that the cell hasn't moved to a different position
					ITrackPoint* tp = cell->getTrackPointByTimePoint(timepoint);
					int cellPosNum = tp->getPositionNumber();
					int currentPosNum = Tools::getPositionNumberFromString(positionIndex).toInt();

					// if the position of the cell in the current timepoint is different from the position of the tree the cell belongs to
					// it means that the cell has moved to a different position
					// we need to get different images for this cell!!
					if (cellPosNum != currentPosNum) {

						QString newPositionIndex = TTTManager::getInst().getExperimentBasename() + "_p" +  Tools::convertIntPositionToString(cellPosNum);

						QMap<QString, QList<Mat>> imgToQuantify2 = this->getQuantificationChannelsImages(detectionWL, path, experimentName, newPositionIndex, timepoint, zStackIndex);

						// if no wavelength image has been found
						// don't bother looking for segmentation image or create one
						if (imgToQuantify2.isEmpty())
							continue;			

						vector<vector<Point> > imgContours2 = this->getContoursFromDetectionImage(path, experimentName, newPositionIndex, timepoint, zStackIndex, detectionWL, segMethodID, cells);

						if (imgContours.empty())
							continue;

						//quantify cell with updated position-timepoint images after position jump
						quantifyCell(cell, timepoint, imgContours2, imgToQuantify2, detectionWL, segMethodID);
					}
					else {
						//quantify cell with default position-timepoint images
						quantifyCell(cell, timepoint, imgContours, imgToQuantify, detectionWL, segMethodID);

					}
				}
				
			} // end for each detection wavelength

		} // end of if termination flag is on

		emit updateProgressBar();

	} // end for each timepoint

	if (this->quantificationTerminationFlag)
		emit updateMsgLog(QString("> Warning: Quantification of position %1 has been stopped...").arg(positionIndex), Qt::darkBlue);
	else
		emit updateMsgLog(QString("> Quantification of position %1 is done...").arg(positionIndex), Qt::darkGreen);
	
	
	emit finished();
}

QMap<QString, QList<Mat>> PositionQuantification::getQuantificationChannelsImages(int _detectionWl, QString _path, QString _experimentName, QString _positionIndex, int _timepoint, int _zStackIndex )
{
	QMap<QString, QList<Mat>> imgToQuantify;
	QVector<Mat> BFimages;

	// get the wavelengths for quantification
	QList<int> qWavelengths = this->wlCombos.value(_detectionWl);
	foreach (int quantificationWL, qWavelengths) {

		Mat wavelengthImage;
		if (this->isSetInspectedMode && !this->preloadedImages.empty()) {
			QString wlID = QString::number(quantificationWL);
			if (this->preloadedImages.contains(wlID)) {
				Mat intermediate = this->preloadedImages.value(wlID);
				wavelengthImage = QTFyImageOperations::convertTo64F(intermediate);
			}
		}

		if (wavelengthImage.empty())
			wavelengthImage = QTFyImageOperations::loadWavelengthImage(_path, _experimentName, _positionIndex, _timepoint, _zStackIndex, quantificationWL);

		Mat backgroundImage = QTFyImageOperations::loadBackgroundImage(_path, _experimentName, _positionIndex, _timepoint, _zStackIndex, quantificationWL);
		Mat gainImage = QTFyImageOperations::loadGainImage(_path, _experimentName, _positionIndex, quantificationWL);

		// check if image exist
		// not all images will exist for all wavelengths etc...
		if (wavelengthImage.empty()) { 
			continue;
		}

		QString quantID = QString("Ch%1").arg(quantificationWL, 2, 10, QChar('0'));
		QList<Mat> wlImages;
		wlImages.append (wavelengthImage);
		wlImages.append (backgroundImage);  // no background image for brightfield
		wlImages.append (gainImage);
		imgToQuantify.insert(quantID, wlImages);
	}

	return imgToQuantify;
}

vector<vector<Point> > PositionQuantification::getContoursFromDetectionImage(const QString _path, 
	const QString _experimentName, 
	const QString _positionIndex, 
	const int _timepoint, 
	const int _zStackIndex, 
	const int _detectionWL, 
	const int _segmentationMethodID,
	const QVector<ITrack*> _cellsInTimepoint)
{
	// also creates and saves a new segmentation image if no segmentation image is available
	Mat segmentationImg;

	if (this->isSetInspectedMode && !this->preloadedImages.empty()) {

		if (this->preloadedImages.contains("seg"))
			segmentationImg = this->preloadedImages.value("seg");
	}

	if (segmentationImg.empty())
		segmentationImg = getSegmentationImageFromDetectionImage(_path, _experimentName, _positionIndex, _timepoint, _zStackIndex, _detectionWL, _segmentationMethodID, _cellsInTimepoint);

	if (segmentationImg.empty()) // no contour has been found // no quantification will be produced for this detection wavelength and this frame index
		return vector<vector<Point> >();

	// use the segmentation image for quantification
	if (segmentationImg.channels() > 1)
		cv::cvtColor(segmentationImg, segmentationImg, CV_BGR2GRAY);

	// get the contours in the segmentation image
	vector<vector<Point> > imgContours;
	// not using the max size here - correct method in segmentation help
	if (segmentationImg.depth() == CV_8U )
		imgContours = QTFyImageOperations::getImageContours(segmentationImg, 5, 10000, false, false); //TODO:: correct min and max filter false?
	else if (segmentationImg.depth() == CV_16U)
		imgContours = QTFyImageOperations::getLabeledComponentsContours(segmentationImg, 5, 10000, false, false);

	return imgContours;
}


void PositionQuantification::checkAndUpdateDwSm(int &_detectionWl, int &_segMethodID)
{
	// if the quantification comes from the segmentation editor
	// keep the settings that have been defined from the quantification chunk
	// that has been already produced
	if (this->isSetInspectedMode) {
		_detectionWl = this->inspectedDetectionWL;
		_segMethodID = this->inspectedSegmentationMethodID;
		return;
	}

	// check if using existing segmentation is allowed
	if (PositionQuantification::options.value("useExistingSegmentation") == "true") {

		// check if the default options are going to be used for existing segmentation
		// default means that the detection wavelength and segmentation method are defined by the combination
		// that the user has selected, segmentation method in this scenario is always Otsu Local (mehtodID = 1)
		if (PositionQuantification::options.value("useDefaultExistingSegmentationOptions") == "false") {
			_detectionWl = PositionQuantification::options.value("replaceDetectionWL").toInt();
			_segMethodID = PositionQuantification::options.value("replaceDetectionMethod").toInt();
		}
	}
}

Mat PositionQuantification::getSegmentationImageFromDetectionImage 
	(const QString _path, 
	const QString _experimentName, 
	const QString _positionIndex, 
	const int _timepoint, 
	const int _zStackIndex, 
	const int _detectionWL, 
	const int _segmentationMethodID,
	const QVector<ITrack*> _cellsInTimepoint)
{

	Mat maskImg;

	// check here if there is a segmentation image available already for the given options
	// and try to load it
	// check if using existing segmentation is allowed
	if (PositionQuantification::options.value("useExistingSegmentation") == "true") {
		maskImg  = QTFyImageOperations::loadSegmentationImage(_path, _experimentName, _positionIndex, _timepoint, _zStackIndex, _segmentationMethodID, _detectionWL);

		QString segImageFilename = QTFyImageOperations::getSegmentationImageFileName(_positionIndex, _timepoint, _zStackIndex, _detectionWL, _segmentationMethodID, 3, 2, "png");

		if (!maskImg.empty()) {
			emit updateMsgLog(QString("> Found existing segmentation image: %1...").arg(segImageFilename), Qt::black);

			// check segmentation image type
			if (maskImg.depth() == CV_8U || maskImg.depth() == CV_16U)
				return maskImg;
			else
				emit updateMsgLog(QString("> Existing segmentation image %1 type not supported. Generating new segmentation image...").arg(segImageFilename), Qt::darkBlue);
		} 
		else {//check if differend file format than png exists

			emit updateMsgLog(QString("> Existing segmentation image %1 not available. Generating new segmentation image...").arg(segImageFilename), Qt::darkBlue);
		}
	}


	// get the detection wavelength image
	Mat detectionWavelengthImage = QTFyImageOperations::loadWavelengthImage(_path, _experimentName, _positionIndex, _timepoint, _zStackIndex, _detectionWL);
	QString wlImageFilename = QTFyImageOperations::getImageFileName(_positionIndex, _timepoint, _zStackIndex, _detectionWL, 3, 2, "png");
	
	// is there a background image for the detection wavelength?
	// if yes use to normalize
	Mat backgroundImage = QTFyImageOperations::loadBackgroundImage(_path, _experimentName, _positionIndex, _timepoint, _zStackIndex, _detectionWL);

	if (!backgroundImage.empty())
		detectionWavelengthImage = detectionWavelengthImage - backgroundImage;

	// could not find the detection wavelength image for this timepoint
	if (detectionWavelengthImage.empty()) {
		emit updateMsgLog(QString("> Loading detection image: %1 has failed. Image not available in position folder...").arg(wlImageFilename), Qt::darkMagenta);
	}
	else {
		emit updateMsgLog(QString("> Loading detection image: %1...").arg(wlImageFilename), Qt::black);
	}

	// new segmentation
	maskImg = getLocalOtsuForTrackpoints(detectionWavelengthImage, _cellsInTimepoint, _timepoint);

	if (maskImg.empty()) {
		emit updateMsgLog(QString("> No cells were identified. No segmentation image will be generated..."), Qt::darkMagenta);
	} 
	else {
		// save image file in folder
		if( QTFyImageOperations::saveSegmentationImage(maskImg, _path, _experimentName, _positionIndex, _timepoint, _zStackIndex, _segmentationMethodID, _detectionWL))
			emit updateMsgLog(QString("> Saving segmentation image for image: %1...").arg(wlImageFilename), Qt::black);
		else
			emit updateMsgLog(QString("> Error during saving of segmentation image...").arg(wlImageFilename), Qt::darkMagenta);
	}

	
	

	return maskImg;
}


void PositionQuantification::quantifyCell(const ITrack* _cell, const int _timepoint, const vector<vector<Point> > _imgContours, const QMap<QString, QList<Mat>> _imgToQuantify, const int _segWL, const int _segMethod)
{
	// find the contour that is closest to the cells trackpoint
	// get the trackpoint coordinates
	// get the trackpoint for this timepoint
	
	Point pnt = getTrackPointOfCellInTimepoint(_cell, _timepoint);
		
	double maxDistance = PositionQuantification::options.value("maxDistanceFromTrackpoint").toInt();
	// find the closest contour to the trackpoint
	vector<Point> closestContour = QTFyImageOperations::getClosestContour(_imgContours, pnt, maxDistance);

	/*if (closestContour.empty()) {
		qDebug() << "Couldn't find a closest contour...";
		return;
	}*/


	QList<QList<Mat>> listOfPairs = _imgToQuantify.values();
	QList<QString> listOfIds = _imgToQuantify.keys();

	for (int i = 0; i < listOfIds.size(); i++) {

		QString prefix = listOfIds.at(i);
		Mat wavelengthImg, backgroundImg, gainImage;
		wavelengthImg = listOfPairs.at(i).at(0);
		backgroundImg = listOfPairs.at(i).at(1);
		gainImage= listOfPairs.at(i).at(2);
		bool bf = false;
		if(prefix.contains("BF"))
			bf = true;

		//intensity features
		QMap<QString, double> values = QTFyImageOperations::calculateIntensityFeaturesWtBackground( bf, closestContour, wavelengthImg, backgroundImg, gainImage);
		

		//morphology features
		QMap<QString, double> morphValues = QTFyImageOperations::calculateMorphologyFeatures(closestContour);

		//add vectors
		for(QMap<QString, double>::iterator it = morphValues.begin(); it != morphValues.end(); ++it){
			values.insert(it.key(), it.value());
		}

		updateCellInCLT(_cell, _timepoint, values, prefix, _segWL, _segMethod);
	}
	
}

void PositionQuantification::updateCellInCLT(const ITrack* _cell, const int _timepoint, const QMap<QString, double> _values, const QString _prefix, const int _segWL, const int _segMethod)
{
	//get the data of the clt file
	CellLineageTree* cltData = _cell->getITree()->getCellLineageTree();
	
	int cellID = _cell->getTrackNumber();
	QString positionIndex = _cell->getITree()->getPositionIndex();
	QString positionToken = positionIndex.split("_").at(1);
	int positionNumber = positionToken.remove("p").toInt();

	// create the relevant frame index for this timepoint
	int zIndex = 1; // default z index
	QVector<int> frameIndex;
	//frame index matrix
	//TimePoint
	frameIndex << _timepoint;
	//FieldOfView(pos)
	frameIndex << positionNumber;
	//ZIndex
	frameIndex << zIndex;
	//ImagingChannel - this is the wavelength of the segmentation image
	frameIndex << _segWL;

	// for each attribute 
	QVector<QString> quantificationAttrs = _values.keys().toVector();

	// check if quantification chunk is available in the cell lineage tree
	// and if not create it
	int qfyChunkIndex = checkForQuantificationChunk(cltData, _segWL, _segMethod);

	// add the columns to the quantification chunk if they are not there already 
	addColsToQuantificationChunk(cltData, qfyChunkIndex, quantificationAttrs, _prefix);

	// if the current cell does not exist in the chunk add the cell to the chunk
	QList<CellLineageTree::FrameIndex> availTimepoints = cltData->getQuantificationFrameIndexes(qfyChunkIndex, cellID);

	int availTimepointsSize = availTimepoints.size();
	
	if (availTimepoints.empty())
		initializeCellInChunk(cltData, qfyChunkIndex, cellID);

	// get chunk column names
	QVector<QString> chunkColumns = cltData->getQuantificationColumnNames(qfyChunkIndex);
	
	QVector<QVariant> dataRow;

	// if the current frame index does not exist in the quantification chunk 
	// initialize the row with default values
	if (!availTimepoints.contains(frameIndex)) {
		// Make sure it has the right number of columns and fill null values
		for(int iCol = 0; iCol < chunkColumns.size(); ++iCol) {
			dataRow << Tree::defaultValue(cltData->getQuantificationColumnTypes(qfyChunkIndex)[iCol]);
		}
		int colActive = chunkColumns.indexOf("active");
		dataRow.replace(colActive, 1.0); // set the timepoint active in the first instance
		int colInspected = chunkColumns.indexOf("inspected");
		dataRow.replace(colInspected, 0.0); // set the timepoint not inspected in the first instance
	}

	// add the initialized timepoint for the cell
	cltData->setQuantificationDataPoint(qfyChunkIndex, cellID, frameIndex, dataRow);
	
	foreach (QString attribute, quantificationAttrs){

		// create the official name using the prefix 
		QString newAttributeName = attribute + _prefix; 

		// get the quantification value
		double currentValue = _values.value(attribute);

		// find the index of the current column
		int colIndx = chunkColumns.indexOf(newAttributeName);


		// get the data in the row with frameIndex
		dataRow = cltData->getQuantificationDataPoint(qfyChunkIndex, cellID, frameIndex);
		dataRow.replace(colIndx, currentValue);
		//// add the attribute for the cell
		cltData->setQuantificationDataPoint(qfyChunkIndex, cellID, frameIndex, dataRow);

	}

	// check if the cell should be set as inspected
	if (this->isSetInspectedMode) {
		// find the index of the inspected column
		int colIndx = chunkColumns.indexOf("inspected");
		dataRow = cltData->getQuantificationDataPoint(qfyChunkIndex, cellID, frameIndex);
		dataRow.replace(colIndx, 1.0);
		//// add the attribute for the cell
		cltData->setQuantificationDataPoint(qfyChunkIndex, cellID, frameIndex, dataRow);
	}


}


void PositionQuantification::initializeCellInChunk(CellLineageTree* _cltData, int _chunkIndex, int _cellID)
{
	// create the header for the cell
	_cltData->insertQuantificationCell(_chunkIndex, _cellID);

}



Mat PositionQuantification::getLocalOtsuForTrackpoints(const Mat& _wavelengthImage, const QVector<ITrack*> _cells, int _timepoint)
{
	int roiSize = PositionQuantification::options.value("windowSize").toInt();

	if (roiSize < 5 )
		return Mat();  // TODO:: throw a message here that the window is really small???

	Mat wavelengthImage8bit;
	cv::normalize(_wavelengthImage, wavelengthImage8bit, 0, 255, NORM_MINMAX, CV_8UC1);
	
	std::vector< std::vector<Point> > ouputSegmentationContours;
	
	// get the trackpoints
	foreach (ITrack* cell, _cells) {

		Point pnt = getTrackPointOfCellInTimepoint(cell, _timepoint);

		// get roi of window around trackpoint
		Mat detectionROI = QTFyImageOperations::getImageROI(wavelengthImage8bit, pnt, roiSize);
		Mat thresholdROI = QTFyImageOperations::thresholdROI(detectionROI, PositionQuantification::options);

		// if the mask file is empty, continue to the next cell
		if (countNonZero(thresholdROI) < 1 ) {
			continue;
		}
		
		// all the contours that are present in this ROI
		std::vector< std::vector<Point> > contoursAll;

		if (PositionQuantification::options.value("splitMergedCells") == "false") {
			// find contours in ROI image
			contoursAll = QTFyImageOperations::getImageContours(thresholdROI, PositionQuantification::options.value("minCellSize").toInt(), PositionQuantification::options.value("maxCellSize").toInt(), true, true);			
		}
		else {
			contoursAll = QTFyImageOperations::getLabeledComponentsContours(thresholdROI, PositionQuantification::options.value("minCellSize").toInt(), PositionQuantification::options.value("maxCellSize").toInt(), true, true);	
		}

		// if at least one contour has been found
		if (!contoursAll.empty()) {
			// find contour containing or within distance from trackpoint
			// TODO:: make the distance here user selectable?
			// distance depends on the size of the window, winSize/2
			double maxDistance = PositionQuantification::options.value("maxDistanceFromTrackpoint").toInt();
			// need to adjust the trackpoint to the ROI image

			Point pntAdj (roiSize/2, roiSize/2);
			std::vector<Point> theContour = QTFyImageOperations::getClosestContour(contoursAll, pntAdj, maxDistance);

			// if at least one contour has been found
			if (!theContour.empty()) {
				std::vector<Point> adjContour = QTFyImageOperations::getAdjustedContourFromROI(theContour, _wavelengthImage.rows, _wavelengthImage.cols, roiSize, pnt);
				ouputSegmentationContours.push_back(adjContour);
			}	
		}
	}

	// nothing has been found
	if (ouputSegmentationContours.empty()) {
		emit updateMsgLog(QString("> No cells have been identified in timepoint %1...").arg(_timepoint), Qt::darkBlue);
		return Mat();
	}

	// create segmentation image and plot all identified contours on the image
	Mat outMat = Mat::zeros(wavelengthImage8bit.rows, wavelengthImage8bit.cols, CV_8UC1);
	
	QTFyImageOperations::plotContours(outMat, ouputSegmentationContours);

	return outMat;
}


Point PositionQuantification::getTrackPointOfCellInTimepoint (const ITrack* _cell, int _timepoint)
{
	ITrackPoint* tp = _cell->getTrackPointByTimePoint(_timepoint);


	// attention!! the position index from the timepoint may be different than the current position analyzed
	// if for example the cell has moved to a neighboring position
	QString positionIndex = TTTManager::getInst().getExperimentBasename() + "_p" + Tools::convertIntPositionToString(tp->getPositionNumber());
	
	// get the X,Y coordinates from the track data
	int Xcoord = tp->getX();
	int Ycoord = tp->getY();

	QPoint absCoordPoint(Xcoord, Ycoord);

	TTTPositionManager* tttpm = TTTManager::getInst().getPositionManager(positionIndex);

	// set whether the coordinated system is inverted or not 
	bool coordinateSystemIsInverted = TTTManager::getInst().coordinateSystemIsInverted();
	QPointF markPoint = QTFyImageOperations::calcTransformedCoords (absCoordPoint, &tttpm->positionInformation, coordinateSystemIsInverted);

	Point pnt (markPoint.x(), markPoint.y());

	return pnt;
}


int PositionQuantification::checkForQuantificationChunk(CellLineageTree* _clt, const int _detectionWL, const int _segMethod)
{
	// get number of available quantification chunks
	int quantNum = _clt->getNumberOfQuantificationChunks();
	int quantIndex = -1; // store here the index of the relevant chunk when found

	for (int i = 0; i < quantNum; i++){

		QString quantSoftware = _clt->getQuantificationSoftware(i);
		QString quantRemarks = _clt->getQuantificationRemarks(i);

		if(quantSoftware == "QTFy" && quantRemarks.contains(QString("channel: %1").arg(_detectionWL)) && quantRemarks.contains(QString("segmentation method: %1").arg(_segMethod))){
			quantIndex = i;
			return quantIndex;
		}
	}

	// if the qtfy chunk has not been found
	// we need to create it

	int newIndexForQTFyChunk = _clt->insertQuantificationChunk();
	//set the new chunk
	QString chunkName = QString("QTFyDetectionCh%1SegMethod%2").arg(_detectionWL).arg(_segMethod);
	_clt->setQuantificationName(newIndexForQTFyChunk, chunkName);
	_clt->setQuantificationSoftware(newIndexForQTFyChunk, "QTFy");
	QString remark = QString("Based on segmentation method: %1, produced from the images of channel: %2").arg(_segMethod).arg(_detectionWL);
	_clt->setQuantificationRemarks(newIndexForQTFyChunk, remark);

	return newIndexForQTFyChunk;

}

void PositionQuantification::addColsToQuantificationChunk(CellLineageTree* _clt, const int _chunkIndex, const QVector<QString> _quantificationAttrs, const QString _prefix)
{
	// get the column names
	QVector<QString> chunckColumns = _clt->getQuantificationColumnNames(_chunkIndex);

	// add the column names to the chunk if not available 
	foreach (QString attribute, _quantificationAttrs){
		// create the official name using the prefix 
		QString newAttributeName = attribute + _prefix; 
		// the new column name is not available - needs to be added
		if (!chunckColumns.contains(newAttributeName)) {
			// add the column
			if(!_clt->addQuantificationColumn(_chunkIndex, newAttributeName, CellLineageTree::Double, std::numeric_limits<double>::quiet_NaN()))
				qDebug()<<"Error with adding quantification column " + newAttributeName + ", updateCell method in PositionQuantification class, quantIndex = -1 case.";
		}
	}

	// check and add the active and inspected columns
	int indexColActive = chunckColumns.indexOf("active");
	// if active column has not been created
	if (indexColActive < 0){
		if(!_clt->addQuantificationColumn(_chunkIndex, "active", CellLineageTree::Double, false))
			qDebug()<<"Error with adding quantification column active, updateCell method in PositionQuantification class, quantIndex = -1 case.";
	}

	int indexColInspected = chunckColumns.indexOf("inspected");
	// if inspected column has not been created
	if (indexColInspected < 0){
		if(!_clt->addQuantificationColumn(_chunkIndex, "inspected", CellLineageTree::Double, false))
			qDebug()<<"Error with adding quantification column inspected, updateCell method in PositionQuantification class, quantIndex = -1 case.";
	}

}

QMap<int, QList<int>> PositionQuantification:: getQuantificationCombinationsFromChunk(ITrack* _cell, int _detectionWL)
{
	//get the data of the clt file
	CellLineageTree* cltData = _cell->getITree()->getCellLineageTree();

	int quantIndex = getQuantificationChunkID(cltData, _detectionWL);

	if (quantIndex < 0)
		return QMap<int, QList<int>>();

	QVector<QString> chunckColumns = cltData->getQuantificationColumnNames(quantIndex);

	// go through the column names and identify the quantification wavelengths present
	// TODO!! could it be a problem in the future if wl are represented with more than two digits
	QList<int> identifiedQWL;
	foreach(QString attrName, chunckColumns) {
		QString quantiWL = attrName.right(2);

		bool conversionState = false;
		int quantiWLInt = quantiWL.toInt(&conversionState);

		if (!identifiedQWL.contains(quantiWLInt) && conversionState == true)
			identifiedQWL.append(quantiWLInt);
	}

	QMap<int, QList<int>> myCombo;
	myCombo.insert(_detectionWL, identifiedQWL);

	return myCombo;

}

QMap<int, QList<int>> PositionQuantification:: getQuantificationCombinationsFromChunks(ITrack* _cell)
{
	QMap<int, QList<int>> myCombo;

	//get the data of the clt file
	CellLineageTree* cltData = _cell->getITree()->getCellLineageTree();

	QList<int> quantIndeces = getQuantificationChunkIDs(cltData);

	foreach(int quantIndex, quantIndeces) {

		QVector<QString> chunckColumns = cltData->getQuantificationColumnNames(quantIndex);

		// go through the column names and identify the quantification wavelengths present
		// TODO!! could it be a problem in the future if wl are represented with more than two digits
		QList<int> identifiedQWL;
		foreach(QString attrName, chunckColumns) {
			QString quantiWL = attrName.right(2);

			bool conversionState = false;
			int quantiWLInt = quantiWL.toInt(&conversionState);

			if (!identifiedQWL.contains(quantiWLInt) && conversionState == true)
				identifiedQWL.append(quantiWLInt);
		}

		QString quantRemarks = cltData->getQuantificationRemarks(quantIndex);
		int detectionWL = 0;
		myCombo.insert(detectionWL, identifiedQWL);
	}
	
	return myCombo;

}


int PositionQuantification::getQuantificationChunkID(CellLineageTree* _cltData, int _detectionWL)
{
	// get the available chunks in the tree
	// which are derived from the detectionWL used from the current segmentation editor
	// get number of available quantification chunks
	int quantNum = _cltData->getNumberOfQuantificationChunks();
	int quantIndex = -1; // store here the index of the relevant chunk when found

	for (int i = 0; i < quantNum; i++){

		QString quantSoftware = _cltData->getQuantificationSoftware(i);
		QString quantRemarks = _cltData->getQuantificationRemarks(i);

		if(quantSoftware == "QTFy" && quantRemarks.contains(QString("channel: %1").arg(_detectionWL))){
			quantIndex = i;
		}
	}

	return quantIndex;
}


QList<int> PositionQuantification::getQuantificationChunkIDs(CellLineageTree* _cltData)
{
	// get the available chunks in the tree
	// which are derived from the detectionWL used from the current segmentation editor
	// get number of available quantification chunks
	int quantNum = _cltData->getNumberOfQuantificationChunks();
	int quantIndex = -1; // store here the index of the relevant chunk when found
	QList<int> quantIndeces;

	for (int i = 0; i < quantNum; i++){

		QString quantSoftware = _cltData->getQuantificationSoftware(i);
		QString quantRemarks = _cltData->getQuantificationRemarks(i);

		if(quantSoftware == "QTFy" && quantRemarks.contains(QString("channel: "))){
			quantIndeces.append(i);
		}
	}

	return quantIndeces;
}

QVector<Mat> PositionQuantification::loadBFImages(const QString _path, const QString _expName, const QString _pos, const int _timepoint, const int _zindex)
{

	QString pathToImage = _path + "\\" + _pos + "\\";
	

	// get the image for each wavelength
	QList<QString> wls;
	wls << this->options.value("wavelengthAbove")<< this->options.value("wavelengthBelow");
	QList<QString>::iterator it;

	QVector<Mat> BFimages;

	for(it = wls.begin(); it != wls.end(); it++)
	{

		int wlID = (*it).toInt();
		QString imageFileName = QTFyImageOperations::getImageFileName(_pos, _timepoint, _zindex, wlID, 3, 2, "png");
		QString imagePath = pathToImage + imageFileName;

		// find the image file
		QFile fileImageRaw(imagePath);

		// if it doesn't exist
		if (!fileImageRaw.exists()) {
			qDebug() << "BF image file cannot be found";
		}
		else {
			Mat bfImg = cv::imread(imagePath.toStdString(), CV_LOAD_IMAGE_ANYDEPTH | CV_LOAD_IMAGE_GRAYSCALE);
			int imgType = bfImg.type();

			Mat imgOut;
			imgOut = QTFyImageOperations::convertTo64F(bfImg);

			BFimages.append(imgOut);
		}
	}

	return BFimages;

}

Mat PositionQuantification::normaliseBF (const QVector<Mat> BFimages)
{
	Mat imgP, imgM, imgP_float, imgM_float;
	imgP = BFimages.at(0);
	imgM = BFimages.at(1);

	//// convert to grayscale
	//if (imgP.channels() > 1)
	//	cv::cvtColor(imgP, imgP, COLOR_BGR2GRAY);
	//
	//if (imgM.channels() > 1)
	//	cv::cvtColor(imgM, imgM, COLOR_BGR2GRAY);


	//// convert to floating-point image
	//imgP.convertTo(imgP_float, CV_32F, 1.0/255.0);
	//imgM.convertTo(imgM_float, CV_32F, 1.0/255.0);

	Mat diff10, norm10;

	//cv::subtract(imgP_float,imgM_float,diff10);
	//images have same size and number of channels so their difference can be calculated by a simple subtraction
	diff10 = imgM - imgP;

	cv::normalize(diff10, norm10, 0.0, 1.0, NORM_MINMAX, -1);

	return norm10;
}