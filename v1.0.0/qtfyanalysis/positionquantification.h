/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * @author Stavroula Skylaki
 * @date 15/08/2015
 *
 * This class will quantify a given position in a thread.
 *
 */

#ifndef POSITIONQUANTIFICATION_H
#define POSITIONQUANTIFICATION_H

//Project
#include "qtfybackend/itrack.h"
#include "qtfybackend/itree.h"
#include "QTFyImageOperations.h"
#include "qtfyclt/celllineagetree.h"
#include "qtfybackend/itrackpoint.h"

//Qt
#include <QString>
#include <QRunnable>
#include <QMap>
#include <QVector>
#include <QColor>

// OPENCV
#include <opencv2/core/core.hpp>

using namespace cv;


class PositionQuantification : public QObject, public QRunnable
{
	Q_OBJECT

public:

	/*
	 * The constructor.
	 */

	PositionQuantification(QString _position, QString _pathToExperiments);

	/*
	 * The destructor.
	 */

	~PositionQuantification();

	// the quantification options from the options tab 
	static QMap<QString, QString> options;

	// get the combinations from the table
	// intially stored as qstrings but then test if they are integers
	static QMap<int, QList<int>> wlCombos;

	static QMap<int, QList<int>> getQuantificationCombinationsFromChunk(ITrack* _cell, int _detectionWL);

	static QMap<int, QList<int>> getQuantificationCombinationsFromChunks(ITrack* _cell);

	// get the id of the quantification chunk that corresponds to detection wavelength _detectionWL
	static int getQuantificationChunkID(CellLineageTree* _cltData, int _detectionWL);

	// get the ids of all the quantification chunks irrespectively of detection wavelength used
	static QList<int> getQuantificationChunkIDs(CellLineageTree* _cltData);

	// create new parameters/ cols in the specific quantification chunk
	static void addColsToQuantificationChunk(CellLineageTree* _clt, const int _chunkIndex, const QVector<QString> _quantificationAttrs, const QString _prefix);

	/*
	 * Initialization of the necessary information for the quantification.
	 */

	void initializeData(QMap<int, QVector<ITrack*>> _cellsPerTimepoint, /*QMap<ITrack*, QVector<int>> _cellsWithMissingTimepoints,*/ QList<QSharedPointer<ITree>> _treesOfThisPosition);

	void setSegmentationInformation(QString _segMethod, int segWavelength);

	void setInspectedMode(bool _isInspectedMode){
		this->isSetInspectedMode = _isInspectedMode;
	}

	void setInspectedDetectionWL(int _inspectedDetectionWL){
		this->inspectedDetectionWL = _inspectedDetectionWL;
	}

	void setInspectedSegmentationMethodID(int _inspectedSegmentationMethodID){
		this->inspectedSegmentationMethodID = _inspectedSegmentationMethodID;
	}

	void setPreloadedImages (QMap<QString, Mat> _loadedImages){
		this->preloadedImages= _loadedImages;
	}

	void setCombinations(QMap<int, QList<int>> _combos){
		PositionQuantification::wlCombos = _combos;
	}

	QMap<int, QList<int>> getCombinations() {
		return PositionQuantification::wlCombos;
	}
	
	public slots:
		void run();

		// sets the flag for termination of loading
		void setQuantificationTerminationRequest() {
			this->quantificationTerminationFlag = true;
		}

		void setQuantificationOptions(QMap<QString, QString> _options) {
			PositionQuantification::options = _options;
		}

signals:
		void finished();
		void error(QString err);

		// updates the message log text box in the loading form
		void updateMsgLog(QString, QColor);

		// updates the progress bar in the loading form
		void updateProgressBar();


private:

	Mat getSegmentationImageFromDetectionImage 
		(const QString _path, 
		const QString _experimentName, 
		const QString _positionIndex, 
		const int _timepoint, 
		const int _zStackIndex, 
		const int _detectionWL, 
		const int _segmentationMethodID,
		const QVector<ITrack*> _cellsInTimepoint);

	vector<vector<Point> > getContoursFromDetectionImage(const QString _path, 
		const QString _experimentName, 
		const QString _positionIndex, 
		const int _timepoint, 
		const int _zStackIndex, 
		const int _detectionWL, 
		const int _segmentationMethodID,
		const QVector<ITrack*> _cellsInTimepoint);

	// produces the segmentation image using Otsu local
	// it returns the segmentation image
	// @param _wavelengthImage the detection wavelength image
	// @param _cells the cells that are present in the image
	// @param _timepoint the timepoint that is being analysed
	// @return the segmentation image as a Mat object
	Mat getLocalOtsuForTrackpoints(const Mat& _wavelengthImage, const QVector<ITrack*> _cells, int _timepoint);

	// returns a list of images for each quantification channel (including background and gain image if available)
	// @param _detectionWl the detection wavelength id
	// @param _path the path to the experiment
	// @param _experimentName the experiment name
	// @param _positionIndex the position that is being analyzed
	// @param _timepoint the timepoint that is being analyzed
	// @return a Map with the list of necessary images per quantification wavelength
	QMap<QString, QList<Mat>> getQuantificationChannelsImages(int _detectionWl, QString _path, QString _experimentName, QString _positionIndex, int _timepoint, int _zStackIndex);

	void checkAndUpdateDwSm(int &_detectionWl, int &_segMethodID);

	// produces the segmentation image using Otsu local
	// it returns the segmentation image
	// @param _wavelengthImage the detection wavelength image
	// @param _cells the cells that are present in the image
	// @return the segmentation image as a Mat object
	Point getTrackPointOfCellInTimepoint (const ITrack* _cell, int _timepoint);

	void initializeCellInChunk(CellLineageTree* _cltData, int _chunkIndex, int _cellID);

	void quantifyCell(const ITrack* _cell, const int _timepoint, const vector<vector<Point> > _imgContours, const QMap<QString, QList<Mat>> _imgToQuantify, const int _segWL, const int _segMethod);

	void updateCellInCLT(const ITrack* _cell, const int _timepoint, const QMap<QString, double> _values, const QString _prefix, const int _segWL, const int _segMethod);

	// checks if there is a quantification chunck for the given detection wl at the given tree
	// and if it is not it creates it
	// @return the index of the chunk
	int checkForQuantificationChunk(CellLineageTree* _clt, const int _detectionWL, const int _segMethod);

	QVector<Mat> loadBFImages(const QString _expName,const QString _path, const QString _pos, const int _timepoint, const int _zindex);
	Mat normaliseBF (const QVector<Mat> BFimages);

	// if true, no more ttt files will be loaded
	bool quantificationTerminationFlag;

	bool isSetInspectedMode;
	int inspectedDetectionWL;
	int inspectedSegmentationMethodID;

	// preloaded images coming from the segmentation editor
	QMap<QString, Mat> preloadedImages;


	QVector<int> wavelengthsToQuantify;



	//The position to be quantified
	QString positionIndex;

	QMap<int, QVector<ITrack*>> cellsPerTimepoint;

	QMap<ITrack*, QVector<int>> missingTimepoints;

	// the collection of trees to be quantified in this position
	QList<QSharedPointer<ITree>> treesForQuantification;


};
#endif //POSITIONQUANTIFICATION_H
