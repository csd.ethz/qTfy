/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
// PROJECT
#include "logfilereader.h"

// QT
#include <QDebug>


LogFileReader::LogFileReader ()
{
	
	UpToDate = false;
	NumberOfWaveLengths = 0;
	FSIArrayLength = 0;
	MaxTimePoint = 0;
	MinTimePoint = 100000;
}


LogFileReader::~LogFileReader()
{
}


void LogFileReader::startLogFileReader (QString _filename, QString _baseName, int &_lastTimePoint, int &_firstTimePoint)
{
	// check if file exists
	if (!QFile(_filename).exists()) {

		QString msg = QString("Error: Log file %1 not available. Loading tree is aborted...").arg(_filename);
		emit updateMsgLog(msg, Qt::red);
		LoadingOK = false;
		return;

	}

	LoadingOK = read (_filename, _baseName/*, _suffix*/);
	
	if (_lastTimePoint == -1) {
		_lastTimePoint = MaxTimePoint;
		_firstTimePoint = MinTimePoint;
	}
	
	if (LoadingOK)
		if (FSIArrayLength == _lastTimePoint + 1)
			UpToDate = true;
		//otherwise the ZEISS files needs to be converted again, since the number of pictures
		// in the current folder changed

	// Display some error information
	if(!UpToDate) {
		QString msg = QString("Error: Failed reading log file: %1. Loading tree is aborted...").arg(_filename);
		emit updateMsgLog(msg, Qt::red);
	}


}


bool LogFileReader::read (QString _filename, QString _baseName/*, QString _suffix*/)
{
	//structure of log file
	//*********************
	//4 bytes : FSIArrayLength
	//4 bytes : NumberOfWaveLengths
	//
	//rest: not reliable for VB creates strings with variable length!!

	
	///@NOTE due to an encountered problem (experiment 100304KK2) with overwritten pictures and somehow corrupted logfile,
	///      the current politics is as follows:
	///      the first timepoint is the one with the earliest time, not necessary the lowest by integer comparison
	
	std::ifstream log (_filename.toStdString().c_str(), std::ios_base::binary);
	
	if (! log) 
		//failure reading file
		return false;

	// Read first 4 bytes to distinguish old log files from new log files
	quint32 first4Bytes;
	log.read(reinterpret_cast<char *>(&first4Bytes), sizeof(quint32));
	if(first4Bytes == SIGNATURE) {
		/**
		 * New log file format
		 */

		// Fileversion
		quint32 fileVersion;
		log.read(reinterpret_cast<char *>(&fileVersion), sizeof(quint32));

		// Number of FSIArrayLength
		log.read(reinterpret_cast<char *>(&FSIArrayLength), sizeof(quint32));

		// Number of wavelengths
		log.read(reinterpret_cast<char *>(&NumberOfWaveLengths), sizeof(quint32));

		// Number of entries
		quint32 numEntries;
		log.read(reinterpret_cast<char *>(&numEntries), sizeof(quint32));

		// Check for errors
		if(FSIArrayLength <= 0 || NumberOfWaveLengths <= 0 || numEntries == 0)
			return false;

		// Create array
		Files = new FileInfoArray();

		// Read entries
		//int deltaSeconds = 0;	// If daylight saving time change occurred during experiment, dates can be wrong
		QDateTime previousTimeStamp; // Last dateTime, used to check for log file errors in which date and month were mixed up for some time points
		int previousTimePoint = 0;
		for(int i = 0; i < numEntries; ++i) {
			// Timepoint
			quint32 timePoint;
			log.read(reinterpret_cast<char *>(&timePoint), sizeof(quint32));

			// Wavelength
			quint16 wavelength;
			log.read(reinterpret_cast<char *>(&wavelength), sizeof(quint16));

			// Z-index
			qint16 zIndex;
			log.read(reinterpret_cast<char *>(&zIndex), sizeof(quint16));
			if(zIndex < 0)
				zIndex = 1;

			// Timestamp
			quint64 timeStamp;
			log.read(reinterpret_cast<char *>(&timeStamp), sizeof(quint64));

			// Calc date
			QDateTime dateTime;
			dateTime.setTimeSpec(Qt::UTC);
			dateTime.setMSecsSinceEpoch(timeStamp);

			// Check time stamps 
			if(previousTimeStamp.isValid()) {
				QDate prevDate = previousTimeStamp.date();
				QDate curDate = dateTime.date();
				if(/*(dateTime.secsTo(previousTimeStamp) > 60*60 && timePoint > previousTimePoint)	// error 1: this time point is more than one hour before the last time point
				   ||*/ (prevDate.month() != curDate.month() && prevDate.day() == curDate.day())		// error 2: month changed, but day stayed the same
				   || (dateTime.secsTo(previousTimeStamp) > 0 && timePoint > previousTimePoint)) {		// new: do not allow any negative delta times
					   //QMessageBox::critical(0, "Log file error", QString("Log file entries appear to be invalid: \nBad timestamps: %1 (tp=%2) - %3 (tp=%4) \n\nReado log file conversion. If this does not solve the problem report this error immediately.").arg(dateTime.toString()).arg(timePoint).arg(previousTimeStamp.toString()).arg(previousTimePoint));
					   QString msg = QString("Log file entries appear to be invalid: \nBad timestamps: %1 (tp=%2) - %3 (tp=%4) \n\nReado log file conversion. If this does not solve the problem report this error immediately.").arg(dateTime.toString()).arg(timePoint).arg(previousTimeStamp.toString()).arg(previousTimePoint);
					   emit updateMsgLog(msg, Qt::red);
					   return false;
				}

				//// ToDo: Check for daylight saving time problems
				//if(timePoint > previousTimePoint && dateTime.secsTo(previousTimeStamp) > 0 && dateTime.secsTo(previousTimeStamp) <= 60 * 60) {
				//	deltaSeconds = 3600;

				//	//qDebug() << "Fo: " << timePoint << " - wl: " << wavelength << " - Z: " << zIndex << " - Time:" << dateTime.toString();
				//	dateTime = dateTime.addSecs(deltaSeconds);
				//}
			}
			previousTimeStamp = dateTime;
			previousTimePoint = timePoint;

			//// Debug
			//qDebug() << "Tp: " << timePoint << " - wl: " << wavelength << " - Z: " << zIndex << " - Time:" << dateTime.toString();

			//// Calc filearray index
			//int index = Files->calcIndex (timePoint, zIndex, wavelength);

			bool firstTimeSet = false;

			// Create FileInfo object
			if (! Files->insert ("", timePoint, zIndex, wavelength, dateTime, firstTimeSet))
				//report erroneous line
				qWarning() << QString ("timepoint == %1, wavelength == %2\n")/*.arg (index)*/.arg (timePoint).arg (wavelength);

			if (firstTimeSet)
				MinTimePoint = timePoint;
			if (timePoint > MaxTimePoint)
				MaxTimePoint = timePoint;

		}

		// Set min/max timepoints
		Files->setTimepoints (MinTimePoint, MaxTimePoint);
	}
	else {
		/**
		 * Old log file format
		 */

		// 21.02.2011-OH: Support for 2 digit wls needed -> filelength now handled via suffixlength
		////note: the length of the filenames change between different files (but not within one file)
		////note: it is assumed that the logfile was created from the files to load, so the ending is the same
		//int filenameLength = _baseName.length() + 11 + _suffix.length(); //_t(5)_w(1).(suffix)
	
		//log.read(reinterpret_cast<char *>(&FSIArrayLength), sizeof(int));
		FSIArrayLength = first4Bytes;
		log.read(reinterpret_cast<char *>(&NumberOfWaveLengths), sizeof(int));
	
		if (FSIArrayLength == 0 || NumberOfWaveLengths == 0)
			//some failure
			return false;
	
		Files = new FileInfoArray();
	
	//	//the array normally starts with index 0, but element 0 is not used
	//	//Files->Array.resize (FSIArrayLength * NumberOfWaveLengths + 1);
	//	Files->resize (FSIArrayLength * (MAX_WAVE_LENGTH + 1) + 1);
	
		char c;
		QString tmp;
		int j;
		//int i = 0;						// //start with index 1
		int index = 1;
		int timePoint = 0, waveLength = 0;
		QDate base (1899, 12, 30);		//the base for vb dates
		double x, y;					//necessary for time calculation
		int dayadd, hour, min, sec;		//necessary for time calculation

		bool bFileNameNotStartingYet = false;

		//const int suffixLength = _suffix.length();
		const int suffixLength = 3;						// A bold assumption?
	
		//read blanks/... until a number or character appears
		//this is the begin of the 27(+/-) bytes of the filename
		while (log.get (c)) {
			if (c && (c >= 48 && c <= 57) 		//digit
				|| (c >= 65 && c <= 90) 	//uppercase letter
				|| (c >= 97 && c <= 122)) { 	//lowercase letter
				//i++;
				//begin of a filename - read [filenameLength] bytes (the first one is already read -> [-1] left)
				tmp = c;
				j = 0;
				bFileNameNotStartingYet = false;
			
				// For filename length handling
				int suffixCharsCount = 0;
				bool readingSuffix = false;

				while (log.get (c)) {
					if(c == 0) {		// Encountered 0 char, so reading of filename started too early
						bFileNameNotStartingYet = true;

						tmp = "";

						break;
					} 
					else if(c == '.') {
						readingSuffix = true;
					}
					else if(readingSuffix) {
						suffixCharsCount++;
					}

					tmp += c;
					//if (++j == filenameLength - 1)
					//	break;
					if(suffixCharsCount == suffixLength)
						break;
				}

				if(bFileNameNotStartingYet)
					continue;

				//qDebug() << tmp;
				//for(int i = 0; i < tmp.size(); ++i) 
				//	qDebug() << tmp.at(i);

			
				//if there is still a letter left (no number!), then the filename still continues => read on
				//we have to use peek(), as get would shift the pointer, which would cause a false date, if no letter is remaining
	/*			char next = log.peek();
				if ((next == 102 || next == 70) && tmp.right (3) == "tif") {
				//while ((next >= 65 && next <= 90) || (next >= 97 && next <= 122)) {	//is a letter
					tmp += next;
					log.get (next);
					//next = log.peek();
				}*/

				// Skip all pics with z index <> 1 (should never happen as experiments with z-indexes have new log file format)
				if(Files->CalcZIndexFromFileName(tmp) > 1)
					continue;
			
			
				timePoint = Files->CalcTimePointFromFilename (tmp);
				waveLength = Files->CalcWaveLengthFromFilename (tmp);
				//index = Files->calcIndex (timePoint, 1, waveLength);
			
			
				//NOTE: a date is stored by visual basic in a double (64 bit)
				//		the number before the point is the days since 1899/12/30
				//		the number after the point represents the hours since midnight (e.g. 0.5 <=> 12:00:00)
			
				//read 8 bytes for creation date
				log.read(reinterpret_cast<char *>(&x), sizeof(double));
			
			
				//modf(): splits the double x into integral value (stored in y) and floating point value (returned to x)
				x = modf (x, &y);			
				dayadd = (int)y;
			
				hour = (int) (24 * x);
				x = modf (24 * x, &y);
				min = (int) (60 * x);
				x = modf (60 * x, &y);
				sec = (int) (60 * x);

				// Debug
				//qDebug() << "Tp: " << timePoint << " - wl: " << waveLength << " - Time:" << QDateTime (base.addDays (dayadd), QTime (hour, min, sec)).toString();
			
			
				//BS 2010/03/19
				//why 30000? due to the special date format, the normal dayadd values are 35,000 or more - so we can safely check if something went wrong
				//introduced due to an error in the position 080418ED3_p029, where at the end of the log file a fake date appeared
				if (dayadd > 30000) {
					//seems to be a valid date
				
					bool firstTimeSet = false;
				
					//SystemInfo::report (tmp);
				
					if (! Files->insert (tmp, timePoint, 1, waveLength, QDateTime (base.addDays (dayadd), QTime (hour, min, sec)), firstTimeSet))
						//report erroneous line
						/*SystemInfo::report (QString ("index == %1, timepoint == %2, wavelength == %3\n")
											.arg (index)
											.arg (timePoint)
											.arg (waveLength)
										, true);*/
				
					if (firstTimeSet)
						MinTimePoint = timePoint;
					if (timePoint > MaxTimePoint)
						MaxTimePoint = timePoint;
				}
			
				//the next bytes are not of interest until the next number/character appears
			}
		
		
		}
	
		Files->setTimepoints (MinTimePoint, MaxTimePoint);
	}
	
	log.close();

/*	//debug:
	const QIntDict<FileInfo> files = Files->getArray();
	
	for (uint i = 0; i < files.size(); i++) {
		//SystemInfo::report (QString);
		if (files [i]) {
			SystemInfo::report (QString ("filename == %1, timepoint == %2, wavelength == %3\n")
									.arg (files [i]->FileName)
									.arg (files [i]->TimePoint)
									.arg (files [i]->WaveLength));
		}
	}*/
	
	
	return true;
}

