/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @authors Bernhard Schauberger, Konstantin Azadov
 * @modifiedby Stavroula Skylaki
 * @datemodified 07/10/2015
 */


#ifndef XMLHANDLER_H
#define XMLHANDLER_H

#include "tatxmlparser.h"
/*
#include "userinfoxmlparser.h"
#include "tttdata/userinfo.h"
#include "stylesheetxmlparser.h"
#include "tttbackend/miningresults.h"
#include "miningresultsxmlparser.h"

#ifndef TTT_ONLY
#include "tttstatsio/treesetxmlparser.h"
#include "tttstatsio/treefilterxmlparser.h"
#include "tttstatsio/cellfilterxmlparser.h"
#endif*/

#include <qstring.h>
#include <qxml.h>
#include <qfile.h>
#include <qdom.h>


/**
	@author Bernhard
	
	This class reads and writes various XML files.
	- the tat xml file, output by the TAT program.
	- the user specific configuration file
	- the style sheet
	- the tree set (Konstantin)
	
	Most (but not all) of the xml files are read with the SAX2 interface, because the tree structure is (until currently) not necessary in ttt.
	It stores the attributes in classes designed to hold them.
	
	Methods necessary for parsing any xml document (in SAX2) are provided.
*/

class XMLHandler{

public:
	
	XMLHandler();
	
	~XMLHandler();
	
	
	/**
	 * reads a xml file of TAT via SAX2 and stores its values into the (unique) instance of TATInformation
	 * @param _filename the filename of the tatxml file
	 * @return whether reading was successful
	 */
	static bool readTATXML (const QString &_filename);
	
//	
//	/**
//	 * Reads the file in _filename and sets the values stored there into the provided stylesheet
//	 * @param _filename the stylesheet filename
//	 * @param _styleSheet the stylesheet to be set (must not be 0)
//	 * @return reading success
//	 */
//	static bool readStyleSheet (const QString &_filename, StyleSheet *_styleSheet);
//	
//	/**
//	 * Writes the provided style sheet to a XML file
//	 * @param _filename the filename
//	 * @param _styleSheet the stylesheet to be written
//	 * @return writing success
//	 */
//	static bool writeStyleSheet (const QString &_filename, const StyleSheet &_styleSheet);
//	
//	/**
//	 * reads the mining explanation file
//	 * @param _filename the filename
//	 * @param _miningResults the result object to be modified
//	 * @return reading success
//	 */
//	static bool readMiningExplanations (const QString &_filename, MiningResults *_miningResults);
//	
//	
//	//------------------- Konstantin --------------------------
//	/**
//	 * Writes the tree set to a XML file
//	 * @param _filename the filename
//	 * @param _treeSet the tree set to be written
//	 * @return writing success
//	 */
//#ifndef TTT_ONLY
//	static bool writeTreeSet (const QString &_filename, const TreeSet &_treeSet);
//#endif
//	//------------------- Konstantin --------------------------
//		
//	/**
//	 * Reads the file in _filename and sets the values stored there into the provided tree set
//	 * @param _filename the tree set filename
//	 * @param _styleSheet the tree set to be set (must not be 0)
//	 * @return reading success
//	 */
//#ifndef TTT_ONLY
//	static bool readTreeSet (const QString &_filename, TreeSet *_treeSet);
//#endif
//
//	//------------------- Konstantin --------------------------
//    /**
//     * Writes the tree filter to a XML file
//     * @param _filename the filename
//     * @param _treeSet the tree filter to be written
//     * @return writing success
//     */
//#ifndef TTT_ONLY
//	static bool writeTreeFilter (const QString &_filename, const TreeFilter &_treeFilter);
//#endif
//
//	//------------------- Konstantin --------------------------
//    /**
//     * Reads the file in _filename and sets the values stored there into the provided tree and cell filter list
//     * @param _filename the cell filter filename
//	 * @param _tfContainer pointer to tree filter list to be set (must not be 0)
//     * @param _cfContainer pointer to cell filter list to be set (must not be 0)
//     * @return reading success
//     */
//#ifndef TTT_ONLY
//	static bool readTreeFilter (const QString &_filename, QHash<QString, TreeFilter*> *_tfContainer, QHash<QString, CellFilter*> *_cfContainer);
//#endif
//
//	
//    //------------------- Konstantin --------------------------
//    /**
//     * Writes the cell filter to a XML file
//     * @param _filename the filename
//     * @param _treeSet the cell filter to be written
//     * @return writing success
//     */
//#ifndef TTT_ONLY
//	static bool writeCellFilter (const QString &_filename, const CellFilter &_cellFilter);
//#endif
//
//    //------------------- Konstantin --------------------------
//    /**
//     * Reads the file in _filename and sets the values stored there into the provided cell filter list
//     * @param _filename the cell filter filename
//     * @param _cfContainer pointer to cell filter list to be set (must not be 0)
//     * @return reading success
//     */
//#ifndef TTT_ONLY
//	static bool readCellFilter (const QString &_filename, /*CellFilter *_cellFilter*/ QHash<QString, CellFilter*> *_cfContainer);
//#endif
//
//general methods	
	/**
	 * returns the value of the attribute from the attribute list whose name corresponds to _attrName
	 * @param _attrs the attribute array
	 * @param _attrName the name of the attribute to be read
	 * @return the value of the attribute (QString::null if the attribute is not present)
	 */
	static QString getAttributeValue (const QXmlAttributes &_attrs, const QString &_attrName);

	/**
	 * converts the provided string into a boolean value
	 * @param _value the string to be converted
	 * @return true, if the uppercase of _value equals "TRUE", false otherwise
	 */
	static bool stringToBool (const QString &_value);
	
	/**
	 * converts a boolean value to a string (corresponding to stringToBool())
	 * @param _value the boolean value
	 * @return "TRUE", if _value is true, "FALSE" otherwise
	 */
	static const QString boolToString (bool _value);
	
	/**
	 * reads any xml file into a dom document
	 * @param _filename the filename (absolute path) of the xml file
	 * @param _domDoc reference parameter: the dom document to be filled with the data
	 * @return reading success
	 */
	static bool readXML2DOM (const QString &_filename, QDomDocument &_domDoc);

    /**
     * writes the provided dom document into the file specified by _filename
     * @return writing success
     */
    static bool writeDOMDocument (const QString &_filename, const QDomDocument &_domDoc);
	
};

#endif
