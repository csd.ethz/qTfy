/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @authors Bernhard Schauberger, Konstantin Azadov
 * @modifiedby Stavroula Skylaki
 * @datemodified 07/10/2015
 */


// PROJECT
#include "xmlhandler.h"
#include "qtfybackend/tttmanager.h"

// QT
#include <QTextStream>

XMLHandler::XMLHandler()
{}


XMLHandler::~XMLHandler()
{}

bool XMLHandler::readTATXML (const QString &_filename)
{
	TATXMLParser handler;
	
	QFile file (_filename);
	if (! file.exists())
		return false;
	
	QXmlInputSource source (&file);
	
	QXmlSimpleReader reader;
	reader.setContentHandler (&handler);
	
	bool success = reader.parse (source);
	
	//SystemInfo::report (TATInformation::getInst()->toString());
	if (success) {
		TATInformation::getInst()->setXMLFilename (_filename);
	}
	
	return success;
}


//bool XMLHandler::readStyleSheet (const QString &_filename, StyleSheet *_styleSheet)
//{
//	if (! _styleSheet)
//		return false;
//	
////BS 2010/03/31 reading of old style files is now discontinued
//
//
//	StyleSheetXMLParser handler (_styleSheet);
//	
//	QFile file (_filename);
//	if (! file.exists())
//		return false;
//	
//	QXmlInputSource source (&file);
//	
//	QXmlSimpleReader reader;
//	reader.setContentHandler (&handler);
//	
//	return reader.parse (source);
//	
////	}
//}
//
//bool XMLHandler::writeStyleSheet (const QString &_filename, const StyleSheet &_styleSheet)
//{
//	if (_filename.isEmpty())
//		return false;
//	
//	QFile file (_filename);
//	
//	if (! file.open (QIODevice::WriteOnly))
//		return false;
//	
//	QDomDocument doc = _styleSheet.toDomDocument();
//	
//        QTextStream ts (&file);
//	ts << doc.toString();
//	
//	file.close();
//	
//	return true;
//}
//
//bool XMLHandler::readMiningExplanations (const QString &_filename, MiningResults *_miningResults)
//{
//	MiningResultsXMLParser handler (_miningResults);
//	
//	QFile file (_filename);
//	if (! file.exists())
//		return false;
//	
//	QXmlInputSource source (&file);
//	
//	QXmlSimpleReader reader;
//	reader.setContentHandler (&handler);
//	
//	return reader.parse (source);	
//	
//}
//
//#ifndef TTT_ONLY
////------------------- Konstantin --------------------------
//bool XMLHandler::writeTreeSet (const QString &_filename, const TreeSet &_treeSet)
//{
//#ifndef TTT_ONLY
//	if (_filename.isEmpty())
//		return false;
//	
//	QFile file (_filename);
//	
//	if (! file.open (QIODevice::WriteOnly))
//		return false;
//	
//	QDomDocument doc = _treeSet.toDomDocument();
//	
//        QTextStream ts (&file);
//	ts << doc.toString();
//	
//	file.close();
//#endif
//	
//	return true;
//}
//#endif 
//
//#ifndef TTT_ONLY
////------------------- Konstantin --------------------------
//bool XMLHandler::readTreeSet (const QString &_filename, TreeSet *_treeSet)
//{
//#ifndef TTT_ONLY
//	TreeSetXMLParser handler (_treeSet);
//	
//	QFile file (_filename);
//	if (! file.exists())
//		return false;
//	
//	QXmlInputSource source (&file);
//	
//	QXmlSimpleReader reader;
//	reader.setContentHandler (&handler);
//	
//	return reader.parse (source);	
//#else
//	return false;
//#endif
//}
//#endif 
//
//#ifndef TTT_ONLY
////------------------- Konstantin --------------------------
//bool XMLHandler::writeTreeFilter (const QString &_filename, const TreeFilter &_treeFilter)
//{
//#ifndef TTT_ONLY
//    if (_filename.isEmpty())
//            return false;
//
//    QFile file (_filename);
//
//    if (! file.open (QIODevice::WriteOnly))
//            return false;
//
//    QDomDocument doc = _treeFilter.toDomDocument();
//
//    QTextStream ts (&file);
//    ts << doc.toString();
//
//    file.close();
//
//    return true;
//#else
//	return false;
//#endif
//}
//#endif 
//
//#ifndef TTT_ONLY
////------------------- Konstantin --------------------------
//bool XMLHandler::readTreeFilter (const QString &_filename, QHash<QString, TreeFilter*> *_tfContainer, QHash<QString, CellFilter*> *_cfContainer)
//{
//#ifndef TTT_ONLY
//	TreeFilterXMLParser handler (_tfContainer, _cfContainer);
//
//	QFile file (_filename);
//	if (! file.exists())
//		return false;
//
//	QXmlInputSource source (&file);
//
//	QXmlSimpleReader reader;
//	reader.setContentHandler (&handler);
//
//	return reader.parse (source);
//#else
//	return false;
//#endif
//}
//#endif 
//
//
//#ifndef TTT_ONLY
////------------------- Konstantin --------------------------
//bool XMLHandler::writeCellFilter (const QString &_filename, const CellFilter &_cellFilter)
//{
//#ifndef TTT_ONLY
//    if (_filename.isEmpty())
//            return false;
//
//    QFile file (_filename);
//
//    if (! file.open (QIODevice::WriteOnly))
//            return false;
//
//    QDomDocument doc = _cellFilter.toDomDocument();
//
//    QTextStream ts (&file);
//    ts << doc.toString();
//
//    file.close();
//
//    return true;
//#else
//	return false;
//#endif
//}
//#endif 
//
//#ifndef TTT_ONLY
////------------------- Konstantin --------------------------
//bool XMLHandler::readCellFilter (const QString &_filename, /*CellFilter *_cellFilter*/ QHash<QString, CellFilter*> *_cfContainer)
//{
//#ifndef TTT_ONLY
//    //CellFilterXMLParser handler (_cellFilter);
//    CellFilterXMLParser handler (_cfContainer);
//
//    QFile file (_filename);
//    if (! file.exists())
//            return false;
//
//    QXmlInputSource source (&file);
//
//    QXmlSimpleReader reader;
//    reader.setContentHandler (&handler);
//
//    return reader.parse (source);
//#else
//	return false;
//#endif
//}
//#endif 
//
QString XMLHandler::getAttributeValue (const QXmlAttributes &_attrs, const QString &_attrName)
{
	for (int i = 0; i < _attrs.count(); i++) {
		if (_attrs.localName (i) == _attrName)
			return _attrs.value (i);
	}
	
	return QString::null;
}

bool XMLHandler::stringToBool (const QString &_value)
{
	if (_value.toUpper() == "TRUE")
		return true;
	else
		return false;
}
//
//const QString XMLHandler::boolToString (bool _value)
//{
//	if (_value)
//		return "TRUE";
//	else
//		return "FALSE";
//}
//
//bool XMLHandler::readXML2DOM (const QString &_filename, QDomDocument &_domDoc)
//{
//	//note: with DOM, the actual parsing of the document happens at demand!
//	
//	QFile file (_filename);
//	
//	if (! file.open( QIODevice::ReadOnly ))
//		return false;
//	
//	if (! _domDoc.setContent (&file)) {
//		file.close();
//		return false;
//	}
//	
//	file.close();
//	
//	return true;
//}
//
//bool XMLHandler::writeDOMDocument (const QString &_filename, const QDomDocument &_domDoc)
//{
//        if (_filename.isEmpty())
//                return false;
//
//        QFile file (_filename);
//
//        if (! file.open (QIODevice::WriteOnly))
//                return false;
//
//        QTextStream ts (&file);
//        ts << _domDoc.toString();
//
//        file.close();
//
//        return true;
//}
//
