/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Stavroula Skylaki
 * @date 06/10/2015
 */


// PROJECT
#include "loadingoperations.h"
#include "qtfybackend/tttmanager.h"
#include "qtfybackend/tttpositionmanager.h"
#include "qtfybackend/tools.h"
#include "qtfydata/systeminfo.h"
#include "qtfydata/tree.h"
#include "qtfyio/xmlhandler.h"
#include "qtfyio/tttfilehandler.h"

// QT
#include <QApplication>



LoadingOperations::LoadingOperations()
{
	errMsg = "";
	this->terminationFlag = false;
}


LoadingOperations::~LoadingOperations()
{
}


// SLOT
void LoadingOperations::startLoading()
{
	TTTManager::getInst().setTTTFilesFolder(tttFilesFolder);
	emit updateMsgLog(QString("> Setting the TTTfiles folder path to: %1...").arg(tttFilesFolder), Qt::black);

	TTTManager::getInst().setExperimentPath(experimentsFilesFolder);
	emit updateMsgLog(QString("> Setting the experiment folder path to: %1...").arg(experimentsFilesFolder), Qt::black);

	QString experimentBasename = QDir(experimentsFilesFolder).dirName();
	TTTManager::getInst().setExperimentBasename(experimentBasename);

	// set the experiment basename for the TATXML readElement
	TTTManager::getInst().setExperimentNamePosMarker(experimentBasename + POSITION_MARKER);

	// check if experiment folder contains a TTT experiment
	emit updateMsgLog(QString("> Checking if experiment is valid..."), Qt::black);
	
	if (!isValidExperiment(experimentsFilesFolder)) {
		emit updateMsgLog(getErrMsg(), Qt::red);
		emit finished();
		return;
	}
	
	if (!isValidTTTFilesFolder(tttFilesFolder, experimentBasename)) {
		emit updateMsgLog(getErrMsg(), Qt::red);
		emit finished();
		return;
	}
	
	// try to load experiment info
	emit updateMsgLog(QString("> Loading experiment info..."), Qt::black);
	if (!loadSelectedExperimentInfo()) {
		emit updateMsgLog(getErrMsg(), Qt::red);
		emit finished();
		return;
	}

	emit updateMsgLog("> Experiment valid...", Qt::darkGreen);

	// get the number of positions and update progress bar
	int numberOfPositions = TTTManager::getInst().getExperiment(experimentBasename)->getPositionManagerIndices().size();
	emit updateMsgLog(QString("> Found %1 positions...").arg(numberOfPositions), Qt::black);

	emit setProgressBarMax(numberOfPositions + 2);
	emit updateProgressBar(2);

	emit updateMsgLog("> Looking for cell lineage trees (.ttt files) in position folders", Qt::black);
	QStringList mytrees = getlistTTTtrees(experimentBasename);

	emit updateMsgLog(QString("> Found %1 trees...").arg(mytrees.size()), Qt::black);
	emit treesFound(mytrees);

	emit updateProgressBar(numberOfPositions + 2);
	emit finished();
}



bool LoadingOperations::isValidExperiment(const QString _experimentPath)
{

	// Check folder of it exists
	if (_experimentPath.isEmpty() || !QDir(_experimentPath).exists()) {
		errMsg = "> Error: The experiment path is empty or not valid.";
		return false;
	}
		

	// check if the user accidentally chose a position folder
	if ( _experimentPath.indexOf (POSITION_MARKER) > 0 ) { // ! (this is already a position folder)
		// QMessageBox::information (0, "Loading not possible", "You selected a position folder as experiment folder.\nPlease select the parent directory of your current directory.", QMessageBox::Ok);
		errMsg = "> Loading Error: You selected a position folder as experiment folder. Please select the parent directory of your current directory.";
		return false;
	}

	// check if the experiment contains at least one position folders
	QStringList positionFolders;
	QVector<QString> posDirs = SystemInfo::listFiles (_experimentPath, QDir::Dirs);

	for (int i = 2; i < posDirs.size(); i++) {	// Start at 2, because 0 and 1 are . and .. folders
		if (posDirs[i].indexOf (POSITION_MARKER) > 0) {
			positionFolders << posDirs[i];
			break;
		}
	}

	//the current folder is most probably no experiment folder (no position folders present)
	if ( positionFolders.size() == 0 ) {
		// QMessageBox::information (0, "Loading not possible", "The current folder is possibly no experiment folder, as it contains no position folders.\nPlease select a different folder.", QMessageBox::Ok);
		errMsg = "> Loading Error: The current folder is possibly no experiment folder, as it contains no position folders. Please select a different folder.";
		return false;
	}

	// check if there is a TATxml
	QString basename = TTTManager::getInst().getExperimentBasename();
	QString tatXmlFilename = basename + "_" + DEFAULT_TAT_FILENAME;
	QString tatXmlFilepath = TTTManager::getInst().getExperimentPath() + tatXmlFilename;

	if (!QFile(tatXmlFilepath).exists()) {
		// QMessageBox::information (0, "TAT xml missing", "The current folder is possibly no experiment folder, as it does not contain a TAT xml.\nPlease select a different folder.", QMessageBox::Ok);
		errMsg = "> Loading Error: The current folder is possibly no experiment folder, as it does not contain a TAT xml. Please make sure a TAT xml is present in the folder.";
		return false;
	}

	return true;
}


bool LoadingOperations::isValidTTTFilesFolder(const QString _tttFilesFolder, const QString _experimentBasename)
{

	// Check folder of it exists
	if (_tttFilesFolder.isEmpty() || !QDir(_tttFilesFolder).exists()) {
		errMsg = "> Error: The TTTfiles path is empty or TTTfiles folder cannot be found.";
		return false;
	}


	// find the experiment year
	QString year = _experimentBasename.left (4);			//note: not the current year, but the one of the experiment!
	//BS 2010/03/22: in some experiments, the year has only two digits: thus we have test, if the year starts with "0" or "1"
	//note: there will definitely be a problem in 2020...
	if ((year.left (1) == "0") || (year.left (1) == "1")) {
		//year has only two digits - fill it with "20" and dismiss the month
		year = "20" + year.left (2);
	}

	QString folder = _tttFilesFolder + "/" + year + "/" + _experimentBasename + "/";

	// Check folder of it exists
	if (folder.isEmpty() || !QDir(folder).exists()) {
		errMsg = QString("> Error: Cannot find folder %1. Invalid TTTfiles folder path...").arg(folder);
		return false;
	}

	return true;
}

bool LoadingOperations::loadSelectedExperimentInfo()
{

	int numberOfPositions = 0;
	TTTPositionManager *tttpm = 0;

	QString folder = TTTManager::getInst().getExperimentPath();

	// get the experiment basename from the folder path
	QString expBasename = TTTManager::getInst().getExperimentBasename();


	// Checks if this experiment was already loaded (for example one tree set from this experiment 
	// could be loaded before). In this case this experiment will not be loaded again, because otherwise
	// position managers from this experiment will be overwritten without information from log files,
	// that could be loaded in updateView() before
	Experiment* exp = TTTManager::getInst().getExperiment(expBasename);

	// the experiment has not been selected before
	if ( ! exp ) {

		exp = new Experiment();					
		QDir currentQDir(folder);
		QVector<QString> posDirs = SystemInfo::listFiles (currentQDir.path(), QDir::Dirs);

		// Attention only works with new folder structure!
		QString tttFolder = SystemInfo::getTTTFileFolderFromBaseName (expBasename + "_p000", false);

		if (tttFolder == "") {
			errMsg = "> Error in TTTfiles folder. Please specify the correct TTTfiles folder path...";
			return false;
		}

		tttFolder = tttFolder.left (tttFolder.lastIndexOf ("/", -2));	//remove "000" position

		//stored for usage in picture size gaining (see below); can be any position
		TTTPositionManager *anyTTTPM = 0;

		// indices of position managers of current experiment are stored here for usage in picture size gaining (see below)
		QVector<QString> pmIndices;

		// Check all subfolders if they are position folders, and for each add a new PositionManager
		for (QVector<QString>::iterator dir = posDirs.begin(); dir != posDirs.end(); ++dir) {

			if ((*dir).indexOf (POSITION_MARKER) > 0) {
				//parse position index from directory name
				QString index = (*dir);
				index = index.mid (index.indexOf (POSITION_MARKER) + 2);

				TTTPositionManager *tttpm = new TTTPositionManager();

				if (! anyTTTPM)
					anyTTTPM = tttpm;

				QString basename = expBasename + POSITION_MARKER + index; // as example 080527MR2_p001

				tttpm->positionInformation.setIndex (basename);
				tttpm->setPictureDirectory ( folder + "/" + basename);
				tttpm->setTTTFileDirectory (tttFolder + "/" + basename);
				tttpm->setBasename (basename);
				//qDebug(QString("Processing position: %1").arg(basename));
				TTTManager::getInst().addPositionManager (basename, tttpm);

				pmIndices.push_back(basename);
			}
		}

		// for the correct index in TATXMLParser::startElement
		QString basename = TTTManager::getInst().getExperimentBasename();
		QString tatXmlFilename = basename + "_" + DEFAULT_TAT_FILENAME;

		//read information of the TAT info file (XML) in the current directory, if present
		//BS 2010/02/25 new: the filename is no longer just TATexp.xml, but contains the experiment name as well
		//                   example: 090901PH2_TATexp.xml
		//thus, an attempt to read a file with the new name is started, if it fails, the old name is tried; if this fails, too, there is none
		bool tatxml_read = XMLHandler::readTATXML (TTTManager::getInst().getExperimentPath() + "/" + tatXmlFilename);
		if (! tatxml_read) { 
			// QMessageBox::information (0, "Failed to read TAT xml.", "The experiment TAT xml appears to be invalid.\nPlease select a different experiment.", QMessageBox::Ok);
			errMsg = "> Failed to read TAT xml: The experiment TAT xml appears to be invalid. Please select an experiment with a valid TAT xml.";
			return false;
		}

		// Use information from TAT file
		if (! TATInformation::getInst()->wavelengthInfoAvailable()) {
			//the wavelength section is missing in the TAT file (old version) -> manually read picture sizes
			//set size of pictures into wavelength information
			//for this we really need to read the first available position folder
			QImage img;
			for (int wl = 0; wl <= MAX_WAVE_LENGTH; wl++) {
				//find first picture file in folder
				QString pattern = QString ("*_w%1.").arg (wl);
				pattern = pattern + "jpg" + ";" + pattern + "tif";
				QVector<QString> pics = SystemInfo::listFiles (anyTTTPM->getPictureDirectory(), QDir::Files, pattern, false);
				QVector<QString>::Iterator iterFiles = pics.begin();

				if ( ! pics.empty() ) {//if (iterFiles)
					QString picfilename = *iterFiles;

					//load picture into memory and determine size
					if (img.load (picfilename))
						TATInformation::getInst()->setWavelengthInfo (wl, WavelengthInformation (wl, img.width(), img.height()));
				}
				else
					TATInformation::getInst()->setWavelengthInfo (wl, WavelengthInformation (wl, -1, -1));
			}
		}

		QVectorIterator<QString> i(pmIndices);
		while (i.hasNext()){
			tttpm = TTTManager::getInst().getPositionManager (i.next());
			if (! tttpm->positionInformation.coordinatesSet()) {
				//position not found in the TAT file
				
				// TODO!!! SHow a log file output to the user??????
			}
			else {
				tttpm->positionInformation.setIs_New_Position(true);

			}
		}

		exp->setUseNewPositions(true);

		if ( TATInformation::getInst()->cellsAndConditionInfoAvailable() ) {
			int preIncTime = TATInformation::getInst()->getCellsAndConditions().HoursBeforeMovieStart * 60 * 60;
			exp->setPreincubationTimeInSeconds(preIncTime);
			// preIncubationMsg += expBasename + QString(": %1 \n").arg(TATInformation::getInst()->getCellsAndConditions().HoursBeforeMovieStart);
			
		} else {
			exp->setPreincubationTimeInSeconds(0);       
			// preIncubationMsg += expBasename + QString(": %1 \n").arg(0);
			
		}


		// SEGEDITOR SPECIFIC! : set the ocular factor and TV for identificiation of pixel coordinates in the TTTmanager (at this point they are set only in the TATInformation)
		TTTManager::getInst().setOcularFactor(TATInformation::getInst()->getOcularFactor());
		TTTManager::getInst().setTVFactor(TATInformation::getInst()->getTVAdapterFactor());
		TTTManager::getInst().setInvertedCoordinateSystem (false);
		// end of segeditor specific


		exp->setPositionManagerIndices(pmIndices);
		TTTManager::getInst().addExperiment(expBasename, exp);
	}

	
	return true;

}

QStringList LoadingOperations::getlistTTTtrees(const QString _expBasename)
{

	QStringList availTrees;

	// go through all the available positions from the TTTManager
	Experiment* exp = TTTManager::getInst().getExperiment(_expBasename);

	// indices of position managers of current experiment are stored here for usage in picture size gaining (see below)
	QVector<QString> pmIndices;
	pmIndices = exp->getPositionManagerIndices();

	int progressCounter = 2;

	// for each position
	foreach (QString positionIndex, pmIndices) {

		if (!terminationFlag) {
			// get the position manager
			// If there is no positionManager skip this position
			TTTPositionManager *tttpm = TTTManager::getInst().getPositionManager (positionIndex);
			if ( ! tttpm ) {
				continue;
			}

			QString positionComment = tttpm->positionInformation.getComment();

			// Set number of digits used for position, if not set previously
			if (Tools::getNumOfPositionDigits()<0) {
				QString FindPos = Tools::getPositionNumberFromString(positionIndex);
				Tools::setNumOfPositionDigits(FindPos.length());
			}

			// go to the TTTfilesfolder
			// get the names of ttt files available in the position folder
			QVector<QString> filesNew = SystemInfo::listFiles (tttpm->getTTTFileDirectory(), QDir::Files, "*" + TTT_FILE_SUFFIX, false);
			QVector<QString> filesOld = SystemInfo::listFiles (tttpm->getTTTFileDirectory(false, true), QDir::Files, "*" + TTT_FILE_SUFFIX, false);
			QVector<QString> files = Tools::joinValueVectors(filesNew, filesOld);

			progressCounter++;
			emit updateProgressBar(progressCounter);

			availTrees.append(files.toList());

		}
	}

	return availTrees;
}


