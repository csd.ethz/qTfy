/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Stavroula Skylaki
 * @date 19/10/2015
 */


#ifndef EXPORTWORKER_H
#define EXPORTWORKER_H


// PROJECT
#include "qtfybackend/itree.h"

// QT
#include <QString>
#include <QColor>
#include <QObject>
#include <QSharedPointer>
#include <QVector>


class ExportWorker : public QObject
{
	Q_OBJECT


public:
	ExportWorker();
	~ExportWorker();

	
	/**
    * @brief checks if the given experiment path corresponds to a valid TTT experiment
	* checks if there are position folders with expected format
	* and whether a TATxml exists
	* @return true if experiment contains position folders and a TAT xml file (not necessarily a valid one)
	*/
	bool isValidExperiment(const QString _experimentPath);

	public slots:

		// starts the loading operations
		void startExporting();

		// sets the flag for termination of loading
		void setTerminationRequest()
		{
			this->terminationFlag = true;
		}

		void setTreesForExport(QVector<QSharedPointer<ITree>> _trees) {
			this->trees = _trees;
		}


signals:

	// updates the message log text box in the loading form
	void updateMsgLog(QString, QColor);

	// updates the progress bar in the loading form
	void updateProgressBar();

	// updates the maximum value of the progress bar in the loading form
	void setProgressBarMax(int);

	// loading done
	void finished();

private:

	// the trees whose quantification is going to be exported
	QVector<QSharedPointer<ITree>> trees;

	// if true, no more directories will be parsed
	bool terminationFlag;
	

};

#endif // EXPORTWORKER_H
