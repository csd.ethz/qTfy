/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Stavroula Skylaki
 * @date 08/10/2015
 */


// PROJECT
#include "tttloadingoperations.h"
#include "qtfybackend/tttmanager.h"
#include "qtfybackend/tttpositionmanager.h"
#include "qtfybackend/tools.h"
#include "qtfydata/systeminfo.h"
#include "qtfyio/xmlhandler.h"
#include "qtfyio/tttfilehandler.h"
#include "qtfyio/logfilereader.h"

// QT
#include <QApplication>
#include <QMessageBox>



TTTLoadingOperations::TTTLoadingOperations()
{
	// required by qt to pass the signals
	qRegisterMetaType<TreePointers>("TreePointers");

	terminationFlag = false;
}


TTTLoadingOperations::~TTTLoadingOperations()
{
}

// SLOT
void TTTLoadingOperations::startTTTLoading()
{
	// the tree pointers for the selected trees
	QVector<QSharedPointer<ITree>> treePointers;

	int progressCounter = 2;
	emit setProgressBarMax(this->selectedTrees.size() + 2);
	emit updateProgressBar(progressCounter);

	int trackCount = 0;
	QString listOfCorruptedTrees;

	foreach(QString file, this->selectedTrees)
	{		
		if (!terminationFlag) {

			QFileInfo fileInfo(file);
			QString treeName(fileInfo.fileName());

			emit updateMsgLog(QString("> Loading tree: %1...").arg(treeName), Qt::black);

			//QString positionIndex = treeName.left(treeName.indexOf("-"));
			QString positionIndex = Tools::getPositionIndex (file); // for example 080527MR2_p001

			// get the position manager of the tree
			// If there is no positionManager skip this position
			TTTPositionManager *tttpm = TTTManager::getInst().getPositionManager (positionIndex);
			if ( ! tttpm ) {
				emit updateMsgLog(QString("Warning: Position %1 is skipped. Position folder could not be found in experiment folder...").arg(positionIndex), Qt::darkBlue);
				continue;
			}

			// Read file
			// Load the tree
			/*Tree *tree = new Tree();
			int firstExpTP = -1, lastExpTP = -1;
			TTTFileHandler tttreader;
			if ( tttreader.readFile (file, tree, trackCount, firstExpTP, lastExpTP, &tttpm->positionInformation, false, true) != TTT_READING_SUCCESS ) {
				if ( tttreader.readFile (file, tree, trackCount, firstExpTP, lastExpTP, &tttpm->positionInformation, true, true) != TTT_READING_SUCCESS ) {
					listOfCorruptedTrees += QString("\n%1").arg(file);
					emit updateMsgLog(QString("Read error: Failed to read file: %1...").arg(file), Qt::red);
					continue;
				}
			}	*/

			Tree *tree = new Tree();
			CellLineageTree  *quant = new CellLineageTree();
			QString err = Tools::readTTTFileWithCsvQuantification(file, tttpm, *tree, *quant);
			
			if (!err.isEmpty()) {
				emit updateMsgLog(QString("Read error: Failed to read file: %1... %2").arg(file).arg(err), Qt::red);
				continue;
			}

			// give tree the ownership of the cell lineage tree
			tree->setCellLineageTree(quant);

			// Initialize the TreeWrapper and shared Pointer
			tree->setExperimentname(positionIndex.left(positionIndex.indexOf(POSITION_MARKER)));
			ITree *t2 = new TreeWrapper(*tree);
			QSharedPointer<ITree> treePointer(t2);
			int firstExpTP = -1, lastExpTP = -1;

			if (!tttpm->getFiles()) {
				//log file for this position was not yet read - so do it

				lastExpTP = -1;
				firstExpTP = -1;

				// picture directory and basename were set in tttstatistics.cpp -> showExperiments
				LogFileReader *logInput = new LogFileReader();

				connect(logInput, SIGNAL(updateMsgLog(QString, QColor)), this, SIGNAL(updateMsgLog(QString, QColor)));

				logInput->startLogFileReader(tttpm->getPictureDirectory() + "/" + tttpm->getBasename() + ".log", tttpm->getBasename(), lastExpTP, firstExpTP/*, "jpg"*/);

				if ((! logInput->LogFileUpToDate()) | (! logInput->LoadingSuccesful())) {
					// QMessageBox::information (0, "Note!!!", QString("Log file for position %1 could not be found\nSo no trees from this position can be regarded!").arg(tttpm->getBasename()), QMessageBox::Ok);
					//log file could not be read - continue with next file

					// tttpm->lockPosition();
					continue;
				}

				tttpm->setExperimentTimepoints (firstExpTP, lastExpTP);
				tttpm->setFileInfoArray (logInput->getArray());	
			}

			// Signal that there is a new tree
			treePointers.append(treePointer);

			// TODO: this signal is used in stattts to load the attributes
			// emit treeLoaded(treePointer);


			progressCounter ++;
			emit updateProgressBar(progressCounter);
		}
	}

	emit treesLoadedSuccess(treePointers);
	emit updateProgressBar(this->selectedTrees.size() + 2);
	emit finished();
}

