/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @authors Bernhard Schauberger, Konstantin Azadov
 * @modifiedby Stavroula Skylaki
 * @datemodified 07/10/2015
 */


// PROJECT
#include "tatxmlparser.h"

#include "xmlhandler.h"
#include "qtfybackend/tttmanager.h"


#include <iostream>

TATXMLParser::TATXMLParser()
 : QXmlDefaultHandler()
{
	coordinateTop = 0.0f;
	coordinateLeft = 0.0f;
	coordinateBottom = 0.0f;
	coordinateRight = 0.0f;
	
	coordinatesSet = false;
	
	TATInformation::getInst()->positionDataRead = false;
	TATInformation::getInst()->tv_ocular_factorsRead = 0;		//none yet read	
	TATInformation::getInst()->wavelengthInfoRead = false; 	
	TATInformation::getInst()->cellsAndConditionsInfoRead = false; 
}


TATXMLParser::~TATXMLParser()
{
}

bool TATXMLParser::startDocument()
{
	//initialize all tag variables
	inTATSetting = false;
	inPositionData = false;
	inWavelengthData = false;
	inCellsAndConditions = false;
	inCNC_CTs_CellType = false;
	// cellType.reset();
	
	return true;
}


bool TATXMLParser::startElement (const QString &/*_namespaceURI*/, const QString &/*_localName*/, const QString &_name, const QXmlAttributes &_attrs) // const QString &/*_namespaceURI*/, const QString &/*_localName*/,
{

	if ((! inTATSetting) && _name == "TATSettings")
		inTATSetting = true;
	else if (inTATSetting) {
		
		if (_name == "PositionData") {
			inPositionData = true;
		}
		else if (_name == "WavelengthData") {
			inWavelengthData = true;
		}
		else if (_name == "CellsAndConditions") {
			inCellsAndConditions = true;
		}
		else if (_name == "CNC_CTs_CellType") {
			inCNC_CTs_CellType = true;
		}
		else {
			if (inPositionData) {
				//data line: <PosInfoDimension index="001" posX="10" posY="5" width="2000" height="1300"/>
				
				if (_name == "PosInfoDimension") {
					QString index = XMLHandler::getAttributeValue (_attrs, "index");
					QString x = XMLHandler::getAttributeValue (_attrs, "posX");
					QString y = XMLHandler::getAttributeValue (_attrs, "posY");
					QString comment = XMLHandler::getAttributeValue (_attrs, "comments");
					
					//replace a comma by a decimal point (was created by TAT converter)
					x.replace (",", ".");
					y.replace (",", ".");
					
					float posX = x.toFloat();
					float posY = y.toFloat();

					updateCoordinateBounds (posX, posY);

					QString expName_posMarker = TTTManager::getInst().getExperimentNamePosMarker();
					index = QString("%1%2").arg(expName_posMarker).arg(index); // as example 080527MR2_p001


					TTTPositionManager *tttpm = TTTManager::getInst().getPositionManager (index);
					if (! tttpm) {
						tttpm = new TTTPositionManager();
						if (tttpm) {
							tttpm->positionInformation.setIndex (index);
							TTTManager::getInst().addPositionManager (index, tttpm);
						}
					}
					if (tttpm) {
						tttpm->positionInformation.setMeasures ((int)posX, (int)posY); //, width, height);
						tttpm->positionInformation.setComment (comment);
					}
					
					//at least one position was truly read
					TATInformation::getInst()->positionDataRead = true;
				}
				
			}
			else if (inWavelengthData) {
				if (_name == "WLInfo") {
					//example line:
					//Name="0" SettingDuring="all light off" SettingAfter="all light off" ExpTime="0.614" Interval="1" BlackPoint="0" WhitePoint="0.25" Gamma="1" Compression="100" ImageType="tif" UseBetweenPositionsSetting="false" width="2776" height="2080" DelayAtPosition="0"
					
					int wl = XMLHandler::getAttributeValue (_attrs, "Name").toInt();
					int width = XMLHandler::getAttributeValue (_attrs, "width").toInt();
					int height = XMLHandler::getAttributeValue (_attrs, "height").toInt();
					QString comment = XMLHandler::getAttributeValue (_attrs, "Comment");
					
                    if ((wl >= 0) & (wl <= MAX_WAVE_LENGTH)) {
						TATInformation::getInst()->setWavelengthInfo (wl, WavelengthInformation (wl, width, height, comment));
						TATInformation::getInst()->wavelengthInfoRead = true;
					}
					
				}
			}
			///@todo new section: CellsAndConditions (see new xml files or tat xml code)
			else if (inCellsAndConditions & (! inCNC_CTs_CellType)) {	
			
				TATInformation::getInst()->cellsAndConditionsInfoRead = true; // -------------- Konstantin ---------------
				
				if (_name == "NumberOfCellTypes") {
					cellsAndConditions.NumberOfCellTypes = XMLHandler::getAttributeValue (_attrs, "value").toInt();
				}
				else if (_name == "Temperature") {
					cellsAndConditions.Temperature = XMLHandler::getAttributeValue (_attrs, "value").toInt();
				}
				else if (_name == "CO2") {
					cellsAndConditions.CO2_Percentage = XMLHandler::getAttributeValue (_attrs, "value").toInt();
				}
				else if (_name == "Flask") {
					cellsAndConditions.Flask = XMLHandler::getAttributeValue (_attrs, "value");
				}
				else if (_name == "Medium") {
					cellsAndConditions.Medium = XMLHandler::getAttributeValue (_attrs, "value");
				}
				else if (_name == "Incubators") {
					cellsAndConditions.Incubators = XMLHandler::getAttributeValue (_attrs, "value");
				}
				else if (_name == "Serum") {
					cellsAndConditions.Serum = XMLHandler::getAttributeValue (_attrs, "value");
				}
				else if (_name == "SerumPercentage") {
					cellsAndConditions.SerumPercentage = XMLHandler::getAttributeValue (_attrs, "value").toInt();
				}
				else if (_name == "MediumAdditions") {
					cellsAndConditions.MediumAdditions = XMLHandler::getAttributeValue (_attrs, "value");
				}
				else if (_name == "ReflectedLightSource") {
					cellsAndConditions.ReflectedLightSource = XMLHandler::getAttributeValue (_attrs, "value");
				}
				else if (_name == "HalogenVoltage") {
					cellsAndConditions.HalogenVoltage = XMLHandler::getAttributeValue (_attrs, "value").toInt();
				}
				else if (_name == "NeutralFilter") {
					cellsAndConditions.NeutralFilter = XMLHandler::getAttributeValue (_attrs, "value").toInt();
				}
				//<TVAdapter value="0.63"/>
				else if (_name == "IlluminationComment") {
					cellsAndConditions.IlluminationComment = XMLHandler::getAttributeValue (_attrs, "value");
				}
				else if (_name == "AgeFluorescentLamp") {
					cellsAndConditions.AgeFluorescentLamp = XMLHandler::getAttributeValue (_attrs, "value").toInt();
				}
				else if (_name == "HoursBeforeMovieStart") {
					cellsAndConditions.HoursBeforeMovieStart = XMLHandler::getAttributeValue (_attrs, "value").toInt();
				}
			}
			else if (inCNC_CTs_CellType) {
				if (_name == "PrimaryCell") {
					cellType.PrimaryCell = (XMLHandler::getAttributeValue (_attrs, "value").trimmed() == "true");
				}
				else if (_name == "Name") {
					cellType.CellName = XMLHandler::getAttributeValue (_attrs, "value");
				}
				else if (_name == "Species") {
					cellType.Species = XMLHandler::getAttributeValue (_attrs, "value");
				}
				else if (_name == "Sex") {
					cellType.Sex = XMLHandler::getAttributeValue (_attrs, "value");
				}
				else if (_name == "Organ") {
					cellType.Organ = XMLHandler::getAttributeValue (_attrs, "value");
				}
				else if (_name == "Age") {
					cellType.Age = XMLHandler::getAttributeValue (_attrs, "value").toInt();
				}
				else if (_name == "Purification") {
					cellType.Purification = XMLHandler::getAttributeValue (_attrs, "value");
				}
				else if (_name == "Comment") {
					cellType.Comment = XMLHandler::getAttributeValue (_attrs, "value");
				}
			}
			else {
				if (_name == "PositionCount") {
					TATInformation::getInst()->positionCount = XMLHandler::getAttributeValue (_attrs, "count").toInt();
				}
				else if (_name == "WavelengthCount") {
					TATInformation::getInst()->wavelengthCount = XMLHandler::getAttributeValue (_attrs, "count").toInt();
				}
				else if (_name == "TimepointCount") {
					TATInformation::getInst()->timepointCount = XMLHandler::getAttributeValue (_attrs, "count").toInt();
				}
				else if (_name == "Interval") {
					TATInformation::getInst()->interval = XMLHandler::getAttributeValue (_attrs, "value").toInt();
				}
				else if (_name == "UseBetweenTimepointsSetting") {
					TATInformation::getInst()->useBTPS = XMLHandler::stringToBool (XMLHandler::getAttributeValue (_attrs, "value"));
				}
				else if (_name == "StaggerWavelengths") {
					TATInformation::getInst()->hasStaggerWls = XMLHandler::stringToBool(XMLHandler::getAttributeValue(_attrs, "value"));
				}
				else if (_name == "CurrentObjectiveMagnification") {
					QString tmp = XMLHandler::getAttributeValue (_attrs, "value");
					tmp.replace (",", ".");
					TATInformation::getInst()->ocularFactor = tmp.toFloat();
					
					if (tmp.toFloat() != 0.0f)
						TATInformation::getInst()->tv_ocular_factorsRead++;
				}
				else if (_name == "CurrentTVAdapterMagnification") {
					QString tmp = XMLHandler::getAttributeValue (_attrs, "value");
					tmp.replace (",", ".");
					TATInformation::getInst()->tvFactor = tmp.toFloat();
					TATInformation::getInst()->microscopeFactorsSet = true;
					
					if (tmp.toFloat() != 0.0f)
						TATInformation::getInst()->tv_ocular_factorsRead++;
				}
				/*OH-140715: added support for directly specifying micrometer per pixel*/
				else if(_name == "MicrometerPerPixel") {
					QString tmp = XMLHandler::getAttributeValue (_attrs, "value");
					tmp.replace (",", ".");
					bool ok;
					double mmpp = tmp.toDouble(&ok);
					if(ok && mmpp > 0.0)
						TATInformation::getInst()->microMeterPerPixel = mmpp;
				}
			}
		}
		
	}
	
	return true;
}



bool TATXMLParser::endElement (const QString &/*_namespaceURI*/, const QString &/*_localName*/, const QString &_name)
{
	
	if (_name == "TATSettings"){
		inTATSetting = false;
	}
	else if (_name == "PositionData") {
		inPositionData = false;
		TATInformation::getInst()->setCoordinateBounds (coordinateLeft, coordinateTop, coordinateRight, coordinateBottom);
	}
	else if (_name == "WavelengthData")
		inWavelengthData = false;
	else if (_name == "CellsAndConditions") {
		inCellsAndConditions = false;
		
		//TATInformation::getInst()->cellsAndConditions = cellsAndConditions;
	}
	else if (_name == "CNC_CTs_CellType") {
		inCNC_CTs_CellType = false;
		
		//cellsAndConditions.addCellType (cellType);
		//cellType.reset();
	}
	
	return true;
}

void TATXMLParser::updateCoordinateBounds (float _posX, float _posY)
{
	if (! coordinatesSet) {
		coordinateLeft = _posX;
		coordinateRight = _posX;
		coordinateTop = _posY;
		coordinateBottom = _posY;
		
		coordinatesSet = true;
	}
	
	if (_posX < coordinateLeft)
		coordinateLeft = _posX;
	if (_posX > coordinateRight)
		coordinateRight = _posX;
	if (_posY < coordinateTop)
		coordinateTop = _posY;
	if (_posY > coordinateBottom)
		coordinateBottom = _posY;
}

