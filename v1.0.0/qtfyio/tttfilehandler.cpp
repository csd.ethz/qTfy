/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
* @authors Bernhard Schauberger, Konstantin Azadov
* @modifiedby Stavroula Skylaki
* @datemodified 08/10/2015
*/

#include "tttfilehandler.h"

// PROJECT
#include "qtfybackend/tttmanager.h"
#include "qtfybackend/tools.h"



TTTFileHandler::TTTFileHandler()
{
}


TTTFileHandler::~TTTFileHandler()
{
}

int TTTFileHandler::readFile (const QString &_filename, Tree* _tree, int &_numTracks, int &_firstTimePoint, int &_lastTimePoint, const PositionInformation *_positionInformation, bool _read4MoreBytes, bool _doNotCheckCoordinateSystem)
{
	if (! _positionInformation)
		return false;

	if (! _tree) {
		_tree = new Tree();
	}
	else
		_tree->reset();

	if (! _tree)
		return false;

	int readRet = readLinuxFile (_filename, _tree, _numTracks, _firstTimePoint, _lastTimePoint, _positionInformation, _read4MoreBytes, _doNotCheckCoordinateSystem);

	if (readRet == TTT_READING_SUCCESS) {
		_tree->setFilename (_filename);
		_tree->setPositionIndex(_positionInformation->getIndex());
	}

	return readRet;
}

int TTTFileHandler::readLinuxFile (const QString &_filename, Tree* _tree, int &_numTracks, int &_firstTimePoint, int &_lastTimePoint, const PositionInformation *_positionInformation, bool _read4MoreBytes, bool _doNotCheckCoordinateSystem)
{

	/**
	* VERY IMPORTANT NOTE:
	* 
	* if in the loading method something is changed to due different ttt file versions, the same distinction needs to take place in the saving method as well!
	* otherwise, there are inconsistent ttt files (for some parts new version, for other parts the old one)!!!
	* 
	* This also implies that, if reading a file fails, try to remove some fileversion decisions (on x/y/..., radius, wavelength, ...) and see if it works.
	* To identify the point where it fails, walk stepwise through the loading procedure with breakpoints.
	* 
	*/

	///former problems with the long datatype: on 64-bit systems its 8-byte, on 32-bit systems its 4-byte; thus, the files between these systems were incompatible
	///thus, if loading a file fails, try this as well
	bool READ_4_MORE_BYTES = _read4MoreBytes;

	//is true iff reading failed due to a wrong byte count number
	//if this happens, try reading the file with READ_4_MORE_BYTES == true
	bool byteCountError = false;



	std::ifstream loader (_filename.toStdString().c_str(), std::ios_base::binary | std::ios_base::in);
	//the file stream for the track comments
	std::ifstream commentLoader ((_filename + QString("comments")).toStdString().c_str(), std::ios_base::in);

	if (! loader)
		return TTT_READING_FILE_NOT_FOUND;			//opening for input failed
	bool LoadComments = true;
	if (! commentLoader)
		LoadComments = false;

	//general statement for reading binary data:
	//loader.read(reinterpret_cast<char *>(&), sizeof(int));

	int FileVersion = 0;
	_firstTimePoint = -1;		//has to be set in calling method
	loader.read(reinterpret_cast<char *>(&FileVersion), sizeof(int));			//file version number

	// Check if file version is too new
	if(TTTFileVersion < FileVersion) {
		QString errMsg = QString("Cannot open ttt file, because its file version (%1) is higher than the file version (%2) supported by QTFy.").arg(FileVersion).arg(TTTFileVersion);
		qDebug() << errMsg;

		return TTT_READING_OTHER_ERROR;
	}

	_tree->setFileVersion (FileVersion);

	//file version is not linux readable
	if (FileVersion < TTTFileFirstLinuxVersion)
		return TTT_READING_OTHER_ERROR;
	

	// Get num of digits for pos indices
	int numDigitsPosIndex = Tools::getNumOfPositionDigits();
	if(numDigitsPosIndex == -1)
		numDigitsPosIndex = 4;	// assume 4 if we don't know

	loader.read(reinterpret_cast<char *>(&_lastTimePoint), sizeof(int));		//last timepoint


	int numTracks = 0;
	loader.read(reinterpret_cast<char *>(&numTracks), sizeof(int));
	_numTracks = numTracks;

	unsigned long long maxPossibleTrackNumber = pow(2, ceil( ((float)numTracks)/2.0f ));

	//new in version 18
	short int maxWavelength = 0;
	if (FileVersion >= 18) {
		loader.read(reinterpret_cast<char *>(&maxWavelength), sizeof(short int));
		_tree->setMaxWavelength (maxWavelength);
	}
	else {
		//in file version 17 or lower, the max wavelength was 5
		maxWavelength = 5;
	}

	if (FileVersion >= 19) {
		//BS 2010/08/11: new tree finished attribute
		short int tmpShortInt = 0;
		loader.read (reinterpret_cast<char *>(&tmpShortInt), sizeof(short int));
		_tree->setFinished ((tmpShortInt == 0) ? false : true);
	}
	else
		_tree->setFinished (false);

	//OH 2011/04/26: new in version 20: flag if we use new position coordinates. Only used for versions >= 20
	bool useNewPositionsFlag = true;
	if (FileVersion >= 20) {		
		char tmpChar;
		loader.read (reinterpret_cast<char *>(&tmpChar), sizeof(char));

		if(tmpChar == 0)
			useNewPositionsFlag = false;
	}

	// If we convert this tree, add log message, but not for every trackpoint
	bool conversionLogged = false;

	Track *track = 0, *mother = 0;

	int number = 0;
	int numberHelper = 0;
	int start = 0, stop = 0;
	short int stopReason = 0;
	int numberTrackPoints = 0;
	short int tmpTimePoint;

	short int oldTmpX, oldTmpY, oldTmpZ, oldTmpXBackground, oldTmpYBackground;
	//since file version 15 we need int for the micrometer coordinates
	//since file version 16 the coordinates are now stored with float values for better accuracy
	float tmpX, tmpY, tmpZ, tmpXBackground, tmpYBackground;

	short int tmpNonAdherent, tmpFreeFloating, tmpEndoMitosis;
	char tmpTissueType, tmpCellGeneralType, tmpCellLineage;

	char oldCellRadius;
	float tmpCellDiameter;

	short int tmpWaveLength[MAX_WAVE_LENGTH + 1];

	//BS 2010/08/10: changed to int due to file reading problems (long is 8-byte on 64-bit systems)
	int addon1 = 0;
	short int tmpPosition = 0;
	CellProperties props;

	//transformation factor: micrometer per pixel
	//note: wavelength 0 can be used as all other wavelengths are stretched to have the same size as 0, if necessary
	//      thus the picture global coordinates always refer (at least indirectly) to wavelength 0
	//      -> we do not need to care about other binning factor of the different wavelengths
	float mmpp = TATInformation::getInst()->getWavelengthInfo (0).getMicrometerPerPixel();

	// ---------Oliver---------- 
	// TODO: REMOVE WHEN PROBLEM IS FIXED
	//
	// 28.09.10: Make it possible to read trees that contain
	// invalid trackpoints at the end of some tracks. Invalid
	// means that every value of the trackpoint has the default
	// value set in Track class, including a timepoint of -1
	// which will be used, to identify these special trackpoints.
	// After any invalid trackpoint, no valid trackpoint can follow.
	// This is a quick fix until the problem is solved.
	int invalidTrackPointsCounter = 0;

	for (int i = 1; i <= numTracks; i++) {
		
		//if numTracks is bigger than the real number of tracks stored in this file, empty loop runs are performed
		//in order to prevent the last track to be overwritten with void data, tracks are only filled if the read number
		// actually indicates a track!

		numberHelper = 0;
		loader.read(reinterpret_cast<char *>(&numberHelper), sizeof(int));

		
		// OH-081111: Found old trees with 0 as track numbers that are actually valid -> added support for that
		if(FileVersion <= 12 && numberHelper == 0) {
			continue;
		}

		if ((numberHelper < number) || (numberHelper > maxPossibleTrackNumber)) {
			//something went wrong - try reading with READ_4_MORE_BYTES == true
			byteCountError = true;
			qWarning() << "Error: Cannot open " << _filename << ". (byteCountError1," << numberHelper << "," << number << "," << maxPossibleTrackNumber << ")";
			break;
		}

		if (numberHelper > 0)
			number = numberHelper;

		if (numberHelper > 0) {

			loader.read(reinterpret_cast<char *>(&start), sizeof(int));
			loader.read(reinterpret_cast<char *>(&stop), sizeof(int));
			loader.read(reinterpret_cast<char *>(&stopReason), sizeof(short int));

			//if (start < _lastTimePoint) {
			if (stop != 0) {
				if (! track)
					//the first track is read - the mother of all cells
					track = new Track (0, 0, _lastTimePoint, start);
				else {
					track = 0;		//must not be deleted as tree manages the memory
					mother = _tree->getTrack (number / 2);
					//note: all track with even index are child 1, with odd index child 2
					track = new Track (mother, (number % 2) + 1, _lastTimePoint, start);
					if (mother)
						mother->setChildTrack ((number % 2) + 1, track);
				}

				if (track) {
					track->setLastTrace (stop);
					track->setStopReason (track->recoverStopReason (stopReason));
					track->setNumber (number);
					_tree->insert (track, false);
				}
			}

			loader.read(reinterpret_cast<char *>(&numberTrackPoints), sizeof(int));

			if (track) {

				short int oldTimepoint = -1;

				// TODO: REMOVE WHEN PROBLEM IS FIXED
				bool foundInvalidTrackPoint = false;

				for (int j = 0; j < numberTrackPoints; j++) {
					
					loader.read(reinterpret_cast<char *>(&tmpTimePoint), sizeof(short int));

					// TODO: REMOVE WHEN PROBLEM IS FIXED
					if(tmpTimePoint == -1) {
						foundInvalidTrackPoint = true;
						invalidTrackPointsCounter++;
					}
					else if( foundInvalidTrackPoint ) {
						// No valid trackpoint is allowed after an invalid trackpoint
						byteCountError = true;
						qWarning() << "Error: Cannot open " << _filename << ". (byteCountError2)";
						break;
					}
					else if ((tmpTimePoint < oldTimepoint) || (tmpTimePoint > _lastTimePoint)) {

						//if ((tmpTimePoint < oldTimepoint) || (tmpTimePoint > _lastTimePoint)) {

						//something went wrong - try reading with READ_4_MORE_BYTES == true
						qWarning() << "Error: Cannot open " << _filename << ". (byteCountError3," << tmpTimePoint << "," << oldTimepoint << "," << _lastTimePoint << ")";
						byteCountError = true;
						break;
					}
					oldTimepoint = tmpTimePoint;

					tmpX = 0;

					//distinguish coordinate reading
					switch (FileVersion) {
					case 12:
					case 13:
					case 14: {
						loader.read(reinterpret_cast<char *>(&oldTmpX), sizeof(short int));
						loader.read(reinterpret_cast<char *>(&oldTmpY), sizeof(short int));
						loader.read(reinterpret_cast<char *>(&oldTmpZ), sizeof(short int));
						loader.read(reinterpret_cast<char *>(&oldTmpXBackground), sizeof(short int));
						loader.read(reinterpret_cast<char *>(&oldTmpYBackground), sizeof(short int));

						if (! TTTManager::getInst().USE_NEW_POSITIONS() || _doNotCheckCoordinateSystem) {
							//do not transform coordinates as there is no position layout file
							tmpX = (int)oldTmpX;
							tmpY = (int)oldTmpY;
							tmpZ = oldTmpZ;
							tmpXBackground = (int)oldTmpXBackground;
							tmpYBackground = (int)oldTmpYBackground;
						}
						else {
							//transform old pixel coordinates to new micrometer scheme
							int left = _positionInformation->getLeft();
							int top = _positionInformation->getTop();

							if (TTTManager::getInst().coordinateSystemIsInverted()) {
								//inverted style: x/y increase to left/top
								tmpX = left - (int)((float)oldTmpX * mmpp);
								tmpY = top - (int)((float)oldTmpY * mmpp);
								tmpZ = oldTmpZ;
								tmpXBackground = left - (int)((float)oldTmpXBackground * mmpp);
								tmpYBackground = top - (int)((float)oldTmpYBackground * mmpp);
							}
							else {
								tmpX = (int)((float)oldTmpX * mmpp) + left;
								tmpY = (int)((float)oldTmpY * mmpp) + top;
								tmpZ = oldTmpZ;
								tmpXBackground = (int)((float)oldTmpXBackground * mmpp) + left;
								tmpYBackground = (int)((float)oldTmpYBackground * mmpp) + top;
							}
						}

						break;
							 }
					case 15: {
						//since version 15 experiment global micrometers are stored rather than picture global pixels

						int tmphula = 0;
						loader.read(reinterpret_cast<char *>(&tmphula), sizeof(int));
						tmpX = (float)tmphula;
						loader.read(reinterpret_cast<char *>(&tmphula), sizeof(int));
						tmpY = (float)tmphula;
						loader.read(reinterpret_cast<char *>(&tmphula), sizeof(int));
						tmpZ = (float)tmphula;
						loader.read(reinterpret_cast<char *>(&tmphula), sizeof(int));
						tmpXBackground = (float)tmphula;
						loader.read(reinterpret_cast<char *>(&tmphula), sizeof(int));
						tmpYBackground = (float)tmphula;
						break;
							 }
					default: {
						//since version 16 coordinates float values for better accuracy

						loader.read(reinterpret_cast<char *>(&tmpX), sizeof(float));
						loader.read(reinterpret_cast<char *>(&tmpY), sizeof(float));
						loader.read(reinterpret_cast<char *>(&tmpZ), sizeof(float));
						loader.read(reinterpret_cast<char *>(&tmpXBackground), sizeof(float));
						loader.read(reinterpret_cast<char *>(&tmpYBackground), sizeof(float));
							 }
					}

					// Maybe we have to convert the coordinates here (todo: conversion only works if we have the right _positionInformation object, which is not guaranteed..)
					if(FileVersion >= 20 && !_doNotCheckCoordinateSystem) {
						// this only happens if the experiment now has a tatxml file it didn't have before (this should never happen anyways)
						if(!useNewPositionsFlag && TTTManager::getInst().USE_NEW_POSITIONS()) {
							// .ttt file was saved with old coordinates, but experiment uses new coordinates -> convert
							if(!conversionLogged) {
								qDebug() << "TTT Notification: Converting tree to new coordinate system: " << _filename;
								conversionLogged = true;
							}

							//transform old pixel coordinates to new micrometer scheme
							int left = _positionInformation->getLeft();
							int top = _positionInformation->getTop();

							if (TTTManager::getInst().coordinateSystemIsInverted()) {
								//inverted style: x/y increase to left/top
								tmpX = left - (int)(tmpX * mmpp);
								tmpY = top - (int)(tmpY * mmpp);

								// Convert background if background tracks exist
								if(abs(tmpXBackground - -1.0) > 0.001) {
									tmpXBackground = left - (int)(tmpXBackground * mmpp);
									tmpYBackground = top - (int)(tmpYBackground * mmpp);
								}
							}
							else {
								tmpX = (int)(tmpX * mmpp) + left;
								tmpY = (int)(tmpY * mmpp) + top;

								// Convert background if background tracks exist
								if(abs(tmpXBackground - -1.0) > 0.001) {
									tmpXBackground = (int)(tmpXBackground * mmpp) + left;
									tmpYBackground = (int)(tmpYBackground * mmpp) + top;
								}
							}
						}
						// this only happens if the experiment has lost its tatxml file -> something went wrong
						else if(useNewPositionsFlag && !TTTManager::getInst().USE_NEW_POSITIONS()) {
							// we cannot convert in this case
							qDebug() << "Cannot open ttt file, because it uses new coordinates, but the experiment does not. (TATexp.xml deleted?)";
							loader.close();
							return TTT_READING_OTHER_ERROR;
						}
					}


					loader.read(reinterpret_cast<char *>(&tmpTissueType), sizeof(char));
					loader.read(reinterpret_cast<char *>(&tmpCellGeneralType), sizeof(char));
					loader.read(reinterpret_cast<char *>(&tmpCellLineage), sizeof(char));
					loader.read(reinterpret_cast<char *>(&tmpNonAdherent), sizeof(short int));
					loader.read(reinterpret_cast<char *>(&tmpFreeFloating), sizeof(short int));

					//distinguish radius reading
					//radius is of type float from version 16 on
					if (FileVersion <= 15) { 
						loader.read(reinterpret_cast<char *>(&oldCellRadius), sizeof(char));
						tmpCellDiameter = (float)oldCellRadius;
					}
					else
						loader.read(reinterpret_cast<char *>(&tmpCellDiameter), sizeof(float));


					loader.read(reinterpret_cast<char *>(&tmpEndoMitosis), sizeof(short int));

					//distinguish wavelength reading (5 from version 13+)
					switch (FileVersion) {
					case 12:
						{
							for (int k = 1; k <= 3; k++)
								loader.read(reinterpret_cast<char *>(&tmpWaveLength[k]), sizeof(short int));
							for (int k = 4; k <= MAX_WAVE_LENGTH; k++)
								tmpWaveLength [k] = 0;

							break;
						}
					case 13:
					case 14:
					case 15:
					case 16:
					case 17: 
						{
							//until version 17, the max wavelength was 5
							int old_mwl = 5;
							for (int k = 1; k <= old_mwl; k++)
								loader.read(reinterpret_cast<char *>(&tmpWaveLength[k]), sizeof(short int));
							for (int k = old_mwl + 1; k <= MAX_WAVE_LENGTH; k++)
								tmpWaveLength [k] = 0;
							break;
						}
					default:
						{
							char tmpWL = 0;
							for (int k = 1; k <= maxWavelength; k++) {
								loader.read(reinterpret_cast<char *>(&tmpWL), sizeof(char));
								tmpWaveLength[k] = (short int)tmpWL;
							}
							break;
						}
					}

					//distinguish additional long attribute reading (from version 14+)
					switch (FileVersion) {
					case 12:
					case 13:
						addon1 = 0;
						break;
					case 14:
					case 15:
					case 16:
					case 17:
					case 18: {
						//note: as all files up to version 18 in the ISF lab were created on a 64bit Linux, we could assume a long to have 8 byte
						// - but this does not hold for other labs, so leave it this way...
						long tmpAddon1 = 0;
						loader.read(reinterpret_cast<char *>(&tmpAddon1), sizeof(long));
						addon1 = tmpAddon1;
						break;
							 }
					default:
						//version 19+: changed to int due to file reading problems (long is 8-byte on 64-bit systems)
						loader.read(reinterpret_cast<char *>(&addon1), sizeof(int));

					}

					if (READ_4_MORE_BYTES) {
						//sometimes, additional 4 bytes are stored after a trackpoint
						//(they originate from an incompatibilty of the long type from the addon attribute)
						//thus, they have to be read if the file was stored in version 14-18 on a 64-bit system, but this one is 32-bit
						int tmpInt = 0;
						loader.read(reinterpret_cast<char *>(&tmpInt), sizeof(int));
					}

					//distinguish if position of trackpoint was stored (from version 17+)
					switch (FileVersion) {
					case 12:
					case 13:
					case 14:
					case 15:
					case 16:
						tmpPosition = 0;
						break;
					default:
						//read position in which the trackpoint was set
						//format in TTT: "000", but stored as a short int
						//NOTE even if a file is already in 17+ format, the stored position can still be 0!
						loader.read(reinterpret_cast<char *>(&tmpPosition), sizeof(short int));
					}

					// TODO: REMOVE WHEN PROBLEM IS FIXED
					if(!foundInvalidTrackPoint) {
						//can be < 0 for inverted coordinate systems!
						if (tmpX != 0.0f) {
							QString pos;
							QTextStream posStream(&pos);
							posStream.setFieldWidth(numDigitsPosIndex);
							posStream.setPadChar('0');
							posStream.setFieldAlignment(QTextStream::AlignRight);
							posStream << tmpPosition;

							track->addMark (tmpTimePoint, tmpX, tmpY, 0, false, 0, pos);

							//set track properties
							props.Z = tmpZ;
							props.XBackground = tmpXBackground;
							props.YBackground = tmpYBackground;
							//type conversion from char/int to enum is illegal (only the other way is possible)
							//=> loop is necessary
							for (int k = 0; k < 7 /*TissueType.count*/; k++)
								if ((int)tmpTissueType == TissueType(k))
									props.tissueType = TissueType (k);
							for (int k = 0; k < 9 /*CellGeneralTpe.count*/; k++)
								if ((int)tmpCellGeneralType == CellGeneralType(k))
									props.cellGeneralType = CellGeneralType (k);
							//props.tissueType = (int)tmpTissueType;
							//props.cellGeneralType = (int)tmpCellGeneralType;
							props.cellLineage = tmpCellLineage;
							props.NonAdherent = (tmpNonAdherent > 0);
							props.FreeFloating = tmpFreeFloating;
							props.CellDiameter = tmpCellDiameter;
							props.EndoMitosis = tmpEndoMitosis;
							for (int k = 1; k <= MAX_WAVE_LENGTH; k++)
								props.WaveLength[k] = tmpWaveLength[k];
							props.AddOn1 = addon1;
							props.Set_Addon = true;

							//props.Position = QString ().sprintf ("%03d", tmpPosition);
							props.Position = pos;

							//store properties for the already inserted track
							track->setProperties (tmpTimePoint, props, false);
						}
					}
				}	

				if (byteCountError)
					//something failed within - stop reading
					break;
			}
		}
	}

	// Read Changelog
	if(FileVersion >= 20) {
		// Changelog
		QList<QPair<QDate, QByteArray> > changeLog;

		// Read number of changelog entries
		unsigned int numChangelogEntries = 0;
		loader.read(reinterpret_cast<char *>(&numChangelogEntries), sizeof(unsigned int));

		// Get current year for sanity checks
		const int curYear = QDate::currentDate().year();

		for(unsigned int i = 0; i < numChangelogEntries; ++i) {
			// Parse date of current changelog entry
			int year, month, day;
			unsigned char tmpChar;

			// Read year
			loader.read(reinterpret_cast<char *>(&tmpChar), sizeof(unsigned char));
			year = tmpChar;
			year += 2000;

			// Sanity check
			if(year < 2005 || year > curYear + 1) {
				byteCountError = true;
				break;
			}

			// Read month
			loader.read(reinterpret_cast<char *>(&tmpChar), sizeof(unsigned char));
			month = tmpChar;

			// Sanity check
			if(month == 0 || month > 12) {
				byteCountError = true;
				break;
			}

			// Read day
			loader.read(reinterpret_cast<char *>(&tmpChar), sizeof(unsigned char));
			day = tmpChar;

			// Sanity check
			if(day == 0 || day > 31) {
				byteCountError = true;
				break;
			}

			// Create QDate object
			QDate date(year, month, day);

			// Parse username
			unsigned short userNameLength;
			loader.read(reinterpret_cast<char *>(&userNameLength), sizeof(unsigned short));

			// Sanity check
			if(userNameLength > 1000) {
				byteCountError = true;
				break;
			}

			// Create userName object with correct length to save time
			QByteArray userName(userNameLength, '?');

			// Read username
			for(unsigned int i = 0; i < userNameLength; ++i) {
				char tmpChar;
				loader.read(reinterpret_cast<char *>(&tmpChar), sizeof(char));
				userName[i] = tmpChar;
			}

			// Add changelog entry
			changeLog.append(QPair<QDate, QByteArray>(date, userName));
		}

		// Add changelog to tree
		_tree->setChangelog(changeLog);
	}

	loader.close();

	if (byteCountError) {
		//reading failed due to a wrong number of bytes (a byte frameshift occurred)
		//inform the user, try again with another setting
		return TTT_READING_BYTE_COUNT_ERROR;
	}

	//just emits the tree size update signal (was not sent before)
	_tree->insert (0, true);


	//load comments, if the comment file exists
	if (LoadComments) {
		commentLoader.seekg (0, std::ios_base::end);
		int commentSize = commentLoader.tellg();
		commentLoader.seekg (0, std::ios_base::beg);
		if (commentSize > 0) {
			//NOTE: a comment can itself contain new lines!
			QString tmpComment = "";
			char zack = 0;
			int trackNumber = 0, pos = 0;
			while (! commentLoader.eof()) {
				while ((tmpComment.indexOf ("|||") == -1) && (! commentLoader.eof())) {	//read until the delimiter is read
					commentLoader.read (&zack, 1);
					tmpComment += zack;
				}
				pos = tmpComment.indexOf (":::");
				if (pos > -1) {
					trackNumber = tmpComment.left (pos).toInt();
					tmpComment = tmpComment.mid (pos + 3, tmpComment.length() - pos - 6);
					if (_tree->getTrack (trackNumber)) {
						if (! tmpComment.isEmpty())
							_tree->getTrack (trackNumber)->setCompleteComment (tmpComment);
					}
				}
				tmpComment = "";
				//proceed 1 byte
				commentLoader.read (&zack, 1);
			}
		}		
		commentLoader.close();
	}

	// Tree not modified
	_tree->setModified(false);

	// TODO: REMOVE WHEN PROBLEM IS FIXED
	if(invalidTrackPointsCounter > 0) {
		QString msg = QString("Errors occurred opening the tree (%1).TTT has removed %2 invalid trackpoints.\n\nPlease make sure the tree is not damaged, then save it again. If it is damaged, do not save it and contact a programmer.").arg(_filename).arg(invalidTrackPointsCounter);
		qDebug() << msg;
		qWarning() << QString("TTT-Warning: Errors occurred opening the tree (%1).TTT has removed %2 invalid trackpoints.").arg(_filename).arg(invalidTrackPointsCounter);
	}

	return TTT_READING_SUCCESS;
}


bool TTTFileHandler::saveFile (const QString &_filename, Tree* _tree, int _firstTimePoint, int _lastTimePoint, int _withVersion, bool _forceOldPositions) //, QString _format)
{
	if (! _tree)
		return false;
	
	TTTFileHandler fh;
	bool success = fh.saveLinuxFile (_filename, _tree, _firstTimePoint, _lastTimePoint, _withVersion, _forceOldPositions);
	
	if (success)
		_tree->setFilename (_filename);
	
	return success;
}


//note: _firstTimePoint is not used at the moment
bool TTTFileHandler::saveLinuxFile (const QString &_filename, /*const */Tree* _tree, int , int _lastTimePoint, int _withVersion, bool _forceOldPositions)
{	
	/**
	 * VERY IMPORTANT NOTE:
	 * 
	 * @see readLinuxFile()
	 * 
	*/
	
	
	std::ofstream saver (_filename.toStdString().c_str(), std::ios_base::binary | std::ios_base::out);
	//the file stream for the track comments (ascii file)
    std::ofstream commentSaver ((_filename + QString("comments")).toStdString().c_str(), std::ios_base::out);
	
    if (! saver)
		return false;			//opening for output failed
	bool SaveComments = true;
    if (! commentSaver)
		SaveComments = false;

	// !-> necessary for writing binary output
        //saver.write(reinterpret_cast<char const *>(&), sizeof());
	
	
	//why this???
	//commented out on 2008/10/09 by BS
	//TTTManager::getInst().setUSE_NEW_POSITIONS(true);
	
	
	int tmpVersion = TTTFileVersion;

	// OH 04/26/11: New in version 20: USE_NEW_POSITIONS now saved as flag in .ttt file
	/*
	if (! TTTManager::getInst().USE_NEW_POSITIONS())
		tmpVersion = 14;		//old positions are stored
	*/

	char useNewPositionsFlag = 1;
	if (!TTTManager::getInst().USE_NEW_POSITIONS() || _forceOldPositions)
		useNewPositionsFlag = 0;

	if (_withVersion != -1)
		tmpVersion = _withVersion;
	
    saver.write(reinterpret_cast<char const *>(&tmpVersion), sizeof(int));		//4 bytes
	
	_tree->setFileVersion (tmpVersion);
	
	int x = _lastTimePoint;

	// OH 120330: found tree where lastTimePoint was way too low -> tree could not be opened again
	x = std::max(x, _tree->getMaxTrackedTimePoint()+1);

    saver.write(reinterpret_cast<char const *>(&x), sizeof(int));			//4 bytes
	
    int numTracks = _tree->getMaxPossibleTracks();
	int realNum = _tree->recalculateNumberOfTracks();
    saver.write(reinterpret_cast<char const *>(&realNum), sizeof(int));		//4 bytes
	
	//new in version 18
	short int maxWavelength = MAX_WAVE_LENGTH;
	if (_tree->getMaxWavelength() > 0)
		maxWavelength = (short int)(_tree->getMaxWavelength());
	
	if ( tmpVersion >= 18 )
        saver.write(reinterpret_cast<char const *>(&maxWavelength), sizeof(short int));
	else
		//BS 2010/04/11 set maxWavelength to 5 for file versions < 18 (this bug caused the problems with Rebecca's files (Priscilla's lab))
		maxWavelength = 5;

    if (tmpVersion >= 19) {
        //BS 2010/08/11: new tree finished attribute
        short int tmpShortInt = (_tree->isFinished() ? 1 : 0);
        saver.write(reinterpret_cast<char const *>(&tmpShortInt), sizeof(short int));
    }

	if (tmpVersion >= 20) {		
		saver.write(reinterpret_cast<char const *>(&useNewPositionsFlag), sizeof(char));
	}
	
	Track *track = 0;
	TrackPoint trackPoint;
	
	int number = 0;
	int start = 0, stop = 0;
	short int stopReason = 0;
	int numberTrackPoints = 0;
	short int position = 0;
	
	//note: loop must run until the maximal possible track number, not only to the number of tracks
	//      e.g. with 5 tracks you could already reach a number 7 track
    for (int i = 1; i <= numTracks; i++) {
		
		track = _tree->getTrack (i);
		
		if (track) {
			//save the comment in the comment file
			//format: (TrackNumber):::(Comment)|||(new line)
			if (SaveComments)
				if (! track->getCompleteComment().isEmpty())
                                        commentSaver << track->getNumber() << ":::" << track->getCompleteComment().toStdString().c_str() << "|||" << std::endl;
			
			//save track data
			number = track->getNumber();
			start = track->getFirstTrace();			
			stop = track->getLastTrace();
			stopReason = track->getStopReasonShortInt();
			
            saver.write(reinterpret_cast<char const *>(&number), sizeof(int));
            saver.write(reinterpret_cast<char const *>(&start), sizeof(int));
            saver.write(reinterpret_cast<char const *>(&stop), sizeof(int));
            saver.write(reinterpret_cast<char const *>(&stopReason), sizeof(short int));
			
			//save the number of trackpoints for this track
			numberTrackPoints = track->getMarkCount();		
                        saver.write(reinterpret_cast<char const*>(&numberTrackPoints), sizeof(int));
			
			// //the trackpoints have to be stored in timepoint order
			// //-> sort 'em
			//track->sortTrackPoints();
			
			//save trackpoint data
			for (int j = 0; j < numberTrackPoints; j++) {
				
				trackPoint = track->getTrackPointByNumber (j+1);
				
				//save attributes
                                saver.write(reinterpret_cast<char const *>(&trackPoint.TimePoint), sizeof(short int));
				
				if (tmpVersion <= 14) {
					short int tmp = 0;
					
					tmp = (short int)trackPoint.X;
					saver.write(reinterpret_cast<char const *>(&tmp), sizeof(short int));
					tmp = (short int)trackPoint.Y;
					saver.write(reinterpret_cast<char const *>(&tmp), sizeof(short int));
					tmp = (short int)trackPoint.Z;
					saver.write(reinterpret_cast<char const *>(&tmp), sizeof(short int));
					tmp = (short int)trackPoint.XBackground;
					saver.write(reinterpret_cast<char const *>(&tmp), sizeof(short int));
					tmp = (short int)trackPoint.YBackground;
					saver.write(reinterpret_cast<char const *>(&tmp), sizeof(short int));
				}
				else {
					//changed to int since file version 15 for micrometer coordinates
					//changed to float for accuracy since version 16
                    saver.write(reinterpret_cast<char const *>(&trackPoint.X), sizeof(float));
                    saver.write(reinterpret_cast<char const *>(&trackPoint.Y), sizeof(float));
                    saver.write(reinterpret_cast<char const *>(&trackPoint.Z), sizeof(float));
                    saver.write(reinterpret_cast<char const *>(&trackPoint.XBackground), sizeof(float));
                    saver.write(reinterpret_cast<char const *>(&trackPoint.YBackground), sizeof(float));
				}
				
                saver.write(reinterpret_cast<char const *>(&trackPoint.tissueType), sizeof(char));
                saver.write(reinterpret_cast<char const *>(&trackPoint.cellGeneralType), sizeof(char));
                saver.write(reinterpret_cast<char const *>(&trackPoint.cellLineage), sizeof(char));
                saver.write(reinterpret_cast<char const *>(&trackPoint.NonAdherent), sizeof(short int));
                saver.write(reinterpret_cast<char const *>(&trackPoint.FreeFloating), sizeof(short int));
				
				
				//radius is of type float from version 16 on
				if (tmpVersion <= 15) {
                    char tmpDiameter = (char)trackPoint.CellDiameter;
                    saver.write(reinterpret_cast<char const *>(&tmpDiameter), sizeof(char));
				}
				else
                    saver.write(reinterpret_cast<char const *>(&trackPoint.CellDiameter), sizeof(float));
				
				
                saver.write(reinterpret_cast<char const *>(&trackPoint.EndoMitosis), sizeof(short int));
				
				if (tmpVersion <= 17) {
					//17--: wavelengths stored as short int
					for (int k = 1; k <= maxWavelength; k++)
                                                saver.write(reinterpret_cast<char const *>(&trackPoint.WaveLength[k]), sizeof(short int));
				}
				else {
					//18++: more wavelengths, and stored as char
					for (int k = 1; k <= maxWavelength; k++) {
						char tmp = (char)(trackPoint.WaveLength[k]);
                                                saver.write(reinterpret_cast<char const *>(&tmp), sizeof(char));
					}
				}
				
                //distinguish additional long attribute reading (from version 14+)
                if (tmpVersion >= 14) {
                    if (tmpVersion <= 18) {
                        long tmpAddon = trackPoint.AddOn1;
                        saver.write(reinterpret_cast<char const *>(&tmpAddon), sizeof(long));
                    }
                    else
                        //version 19+: int instead of long due to size changes between 32 and 64 bit
                        saver.write(reinterpret_cast<char const *>(&trackPoint.AddOn1), sizeof(int));
                }
				
				
				if (tmpVersion >= 17) {
					//write position in which the trackpoint was set
					//format: "000" -> exactly three digits, thus store it as a number
					position = trackPoint.Position.toShort();
                    saver.write(reinterpret_cast<char const *>(&position), sizeof(short int));
				}
			}
		}
	}

	// Save change log
	if(tmpVersion >= 20) {
		const QList<QPair<QDate, QByteArray> >& changeLog = _tree->getChangelog();

		// Write number of changelog entries
		unsigned int numChangelogEntries = changeLog.length();
		saver.write(reinterpret_cast<char *>(&numChangelogEntries), sizeof(unsigned int));

		for(QList<QPair<QDate, QByteArray> >::const_iterator it = changeLog.constBegin(); it != changeLog.constEnd(); ++it) {
			const QPair<QDate, QByteArray>& currentPair = *it;

			// Write date of current change log entry
			unsigned char tmpChar;

			// Write year
			tmpChar = (unsigned char)(currentPair.first.year() - 2000);
			saver.write(reinterpret_cast<char *>(&tmpChar), sizeof(unsigned char));

			// Read month
			tmpChar = (unsigned char)currentPair.first.month();
			saver.write(reinterpret_cast<char *>(&tmpChar), sizeof(unsigned char));

			// Write day
			tmpChar = (unsigned char)currentPair.first.day();
			saver.write(reinterpret_cast<char *>(&tmpChar), sizeof(unsigned char));

			// Write username
			unsigned short userNameLength = currentPair.second.length();
			saver.write(reinterpret_cast<char *>(&userNameLength), sizeof(unsigned short));

			// Write username
			for(unsigned int i = 0; i < userNameLength; ++i) {
				char tmpChar = currentPair.second[i];
				saver.write(reinterpret_cast<char *>(&tmpChar), sizeof(char));
			}
		}
	}

    saver.close();
    if (commentSaver && SaveComments)
        commentSaver.close();
	
	return true;
}

