/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Stavroula Skylaki
 * @date 06/10/2015
 */


#ifndef LOADINGOPERATIONS_H
#define LOADINGOPERATIONS_H


// PROJECT

// QT
#include <QString>
#include <QColor>
#include <QObject>


class LoadingOperations : public QObject
{
	Q_OBJECT


public:
	LoadingOperations();
	~LoadingOperations();

	
	/**
    * @brief loads the selected experiment based on the user input paths
	* uses the TTTManager to find the required info for paths/experiment name etc
	* @return true if experiment is loaded successfully
	*/
	bool loadSelectedExperimentInfo();

	/**
    * @brief looks in the TTTfilesfolder in each position for tracked trees (ttt-files)
	* @param QString _expBasename the name of the experiment (i.e. 151002TF)
	* @return QStringList the list of available ttt files
	*/
	QStringList getlistTTTtrees(const QString _expBasename);

	/**
    * @brief checks if the given experiment path corresponds to a valid TTT experiment
	* checks if there are position folders with expected format
	* and whether a TATxml exists
	* @return true if experiment contains position folders and a TAT xml file (not necessarily a valid one)
	*/
	bool isValidExperiment(const QString _experimentPath);

	/**
    * @brief checks if the given tttfiles folder path corresponds to a valid TTTfiles folder
	* checks if there is a year folder for the experiment as well
	* @return true if tttfiles folder contains year folder and experiment folder
	*/
	bool isValidTTTFilesFolder(const QString _tttFilesFolder, const QString _experimentBasename);

	// return the error message
	QString getErrMsg (){
		return this->errMsg;
	}


	void setLogFilesFolder (QString _logFilesFolder){
		this->logFilesFolder = _logFilesFolder;
	}

	void setTTTFilesFolder (QString _tttFilesFolder){
		this->tttFilesFolder = _tttFilesFolder;
	}

	void setExperimentsFilesFolder (QString _experimentsFilesFolder){
		this->experimentsFilesFolder = _experimentsFilesFolder;
	}

	public slots:

		// starts the loading operations
		void startLoading();

		// sets the flag for termination of loading
		void setTerminationRequest()
		{
			this->terminationFlag = true;
		}


signals:

	// updates the message log text box in the loading form
	void updateMsgLog(QString, QColor);

	// updates the progress bar in the loading form
	void updateProgressBar(int);

	// updates the maximum value of the progress bar in the loading form
	void setProgressBarMax(int);

	// searching for trees is over pass the trees to the main form
	void treesFound (QStringList);

	// loading done
	void finished();

private:

	//// the paths to the files
	//// initialized to user's document folders
	QString logFilesFolder;
	QString tttFilesFolder;
	QString experimentsFilesFolder;

	bool tttFilesPathValid;
	bool experimentPathValid;

	// if true, no more directories will be parsed
	bool terminationFlag;
	
	// loading has encountered an error, error message
	QString errMsg;


};

#endif // LOADINGOPERATIONS_H
