/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @authors Bernhard Schauberger, Konstantin Azadov
 * @modifiedby Stavroula Skylaki
 * @datemodified 08/10/2015
 */


#ifndef TTTFILEHANDLER_H
#define TTTFILEHANDLER_H


#include "qtfydata/tree.h"
//#include "qtfydata/trackpoint.h"
//#include "qtfydata/track.h"
#include "qtfydata/cellproperties.h"
#include "qtfydata/positioninformation.h"
#include "qtfydata/tatinformation.h"

#include <qstring.h>

#include <fstream>


///indicates the ttt-file version (until 11: VB-format)
///changes here imply changes in the loading/saving routines in TTTFileHandler!
///12: 3 wavelengths (initial version)
///13: 5 wavelengths
///14: additional long attribute
///15: new position system with micormeters and multi-position tracking
///16: CellDiameter of type float for exact measuring of the fluorescence
//     X,Y,Z,XBackground,YBackground also float now
///17: the position in which the trackpoint was set is now stored for each TrackPoint (2009/11/23)
///18: arbitrary wavelength count (the actual number of wavelengths is stored in each file) (2010/03/18)
///19: changed addon attribute type to int (long is 8-byte on 64-bit systems but 4-byte on 32-bit; int, however, is always 4-byte)
//     new attribute: tree finished, stored at the beginning

//----------------------------------------------------------------
//----------------------------------------------------------------
///soon to come:
///method for storing arbitrary wavelengths
///- store maximum wavelength for the current tree (can be determined while loading an experiment(!, not a position!))
///- store values for all wavelengths between 0 and max_wl (incl.)
///  (note: of course there could be gaps, but we do not assume that as we trust on the user's responsibility or 
///         just say it is his problem...)
///- when reading a ttt file, the maximum wavelength can be adapted (only to higher values)
///   -> thus store it in TTTManager and provide a global method for setting (needs to tune and initialize variables)
///- reading an old file (till 17) then means max:=5
///- ? the wavelengths are now not anymore stored as simple two-byte values (short int), but are compressed into (at least) one
///  for the detailed method, see compressWavelengths() and decompressWavelengths()
///  but NOTE this makes the reading harder for non-ttt users!
///
//----------------------------------------------------------------
//----------------------------------------------------------------

///***************************************************************************************************************************************
///@todo: maybe abandon the sizeof() operator in saving/loading, as it is a compile-time operator, thus even more depending on the machine...
//        to make sure that reading/saving works an just any machine, try reading a binary file with truly fixed sizes (4 instead of sizeof(int) -> then use it, if it works!
///***************************************************************************************************************************************

///NOTE:
///@WARNING: if you add a new file version, you MUST also change the routine for saving, as it is still possible to store files in version 14 (if there is no xml file for the positions)
const int TTTFileVersion = 20;

///indicates the first Linux file version
const int TTTFileFirstLinuxVersion = 12;

const QString TTT_FILE_SUFFIX = ".ttt";

///possible return values of readFile()
const int TTT_READING_SUCCESS = 0;
const int TTT_READING_FILE_NOT_FOUND = 1;
const int TTT_READING_BYTE_COUNT_ERROR = 2;
const int TTT_READING_OTHER_ERROR = 3;


/**
@author Bernhard
	
	This class provides functionality to read and write TTT files 
	It can read and write #12 format files
	The older Windows files (#10 + #11) cannot neither be read nor stored
	Conversion routines are included in the windows version.
*/

class TTTFileHandler{
public:
	TTTFileHandler();
	
	~TTTFileHandler();
	
	/**
	 * Read .ttt file.
	 * @param _filename filename of the .ttt file to be read
	 * @param _tree pointer to a Tree instance, into which tree will be read. Modified flag of _tree will be set to false
	 * @param _numTracks reference to int, number of tracks will be saved in there
	 * @param _lastTimePoint reference to int, last timepoint of experiment will be saved in there
	 * @param _positionInformation only used to convert old coordinates (fileversion <= 14 or newly created TATXML file) to new coordinates
	 * @param _read4MoreBytes deprecated, read 4 more bytes to read invalid ttt files
	 * @param _doNotCheckCoordinateSystem do not check if experiment uses new coordinates (i.e. TTTManager::USE_NEW_POSITIONS() returns true, i.e. tatxml exists) (Important for TTTStats, as stats does not load tatxml)
	 * @return TTT_READING_SUCCESS, TTT_READING_FILE_NOT_FOUND, TTT_READING_BYTE_COUNT_ERROR or TTT_READING_OTHER_ERROR
	 */
    static int readFile (const QString &_filename, Tree* _tree, int &_numTracks, int &_firstTimePoint, int &_lastTimePoint, const PositionInformation *_positionInformation, bool _read4MoreBytes = false, bool _doNotCheckCoordinateSystem = false);
	
	/**
	 * Save Tree to disk
	 * @param _filename of the .ttt file in which tree should be saved
	 * @param _tree the tree to be saved. Modified state of _tree will NOT be changed (this could be an automatic backup or so)
	 * @param _firstTimePoint deprecated, ignored
	 * @param _lastTimePoint last timepoint of experiment
	 * @param _withVersion if <> -1, this value will be used as fileversion instead of the TTTFileVersion constant (deprecated)
	 * @param _forceOldPositions if useNewPositionsFlag should be set to false no matter what TTTManager::USE_NEW_POSITIONS() returns
	 * @return true if successful
	 */
	static bool saveFile (const QString &_filename, Tree* _tree, int _firstTimePoint, int _lastTimePoint, int _withVersion = -1, bool _forceOldPositions = false);//, QString _format = "Linux");
	
private:

    static int readLinuxFile (const QString &_filename, Tree* _tree, int &_numTracks, int &_firstTimePoint, int &_lastTimePoint, const PositionInformation *_positionInformation, bool _read4MoreBytes, bool _doNotCheckCoordinateSystem);
	
	static bool saveLinuxFile (const QString &_filename, /*const*/ Tree* _tree, int _firstTimePoint, int _lastTimePoint, int _withVersion, bool _forceOldPositions);

};

/*	----------------------------------
	*List of corresponding data types*
	----------------------------------
	
	VB						Linux				Size (VB6 + gcc)
	------------------------------------------------------------
	Integer					short int				2
	Long					int					4
	Byte					char					1
	Boolean					short int				2
	------------------------------------------------------------
*/

	//TTT FILE VERSION #12
	//
	//the format for (binary) ttt files:
	//
	//4 bytes: ttt file version
	//4 bytes: last time point currently loaded
	//4 bytes: number of tracks 
	//
	// bytes: Track() As Track						//-> ...
	//		
	//		+ Number As Long
	//		Start As Long							//4 bytes
	//		Stop As Long							//4 bytes
	//		StopReason As Integer					//2 bytes
	//		+ NumTrackPoints As Long				//4 bytes
	//		TrackPoint() As TrackPoint				//-> ...
	//
	//			+ TimePoint As Integer
	//			X As Integer
	//			Y As Integer
	//			Z As Integer
	//			XBackground As Integer
	//			YBackground As Integer
	//			TissueType As Byte					//1 byte
	//			CellGeneralType As Byte
	//			CellLineage As Byte
	//			NonAdherent As Boolean				//2 bytes
	//			FreeFloating As Boolean 
	//			CellRadius As Byte 
	//			Endomitosis As Boolean
	//			Wavelength(1 To 3) As Integer
	//	
	//note:
	//only the tracks/cells & trackpoints that really exist are stored
	//the comments are stored in an extra file to avoid storing strange strings in track data




#endif
