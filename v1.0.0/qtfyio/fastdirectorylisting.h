/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef fastdirectorylisting_h__
#define fastdirectorylisting_h__

// Qt includes
#include <QStringList>
#include <QDir>
#include <QByteArray>

/**
 * @author Oliver
 *
 * This class provides functions to list files as fast as possible, especially
 * faster than QDir::entryList() functions.
 * Implemented using windows API
 */
class FastDirectoryListing {
public:

	/**
	 * Return unsorted list of files or directories
	 * @param _path the path to search in, e.g. "C:\someFolder"
	 * @param _fileExtensions case-insensitive file extensions to use as filter, e.g. (".cpp",".h") (this means you can specify arbitrary file endings, like w_0.jpg, and not just parts of the actual file extension)
	 * @param _onlyDirs if true only directories are listed, if false only files are listed
	 * @return unsorted(!) list of found filenames (without path)
	 */
	static QStringList listFiles(QDir path, const QStringList fileExtensions, const QStringList filePrefix, bool onlyDirs = false);

	/**
	 * Convinient method for listFiles with an empty filePrefixes list.
	 */
	static QStringList listFiles(QDir path, QStringList fileExtensions, bool onlyDirs = false);

private:

	/**
	 * Helper function to check extension and prefix of a filename
	 * @param _fileName the file name
	 * @param _fileExtensions the accepted file extensions
	 * @param filePrefix the accepted file prefix
	 * @return true if _fileName ends with a string provided in _fileExtensions
	 */
	static bool FastDirectoryListing::checkFile( const QByteArray& fileName, const QStringList& fileExtensions, const QStringList& filePrefix);

	/**
	 * Function based on QDir::entrylist, used in case of errors
	 * @param _path the path to search in
	 * @param _fileExtensions file extensions to use as filter
	 * @param filePrefix file prefix used as filter
	 * @return UNSORTED list of filenames without path that have been found
	 */
	static QStringList listFilesWithQDir(QDir path, const QStringList fileExtensions, const QStringList filePrefix);
};


#endif // fastdirectorylisting_h__