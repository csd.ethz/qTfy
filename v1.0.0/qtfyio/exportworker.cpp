/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Stavroula Skylaki
 * @date 19/10/2015
 */


// PROJECT
#include "exportworker.h"
#include "qtfybackend/tools.h"


ExportWorker::ExportWorker()
{
	this->terminationFlag = false;
}


ExportWorker::~ExportWorker()
{
}


// SLOT
void ExportWorker::startExporting()
{
	emit setProgressBarMax(this->trees.size());

	foreach(QSharedPointer<ITree> tree, this->trees) {

		QString file = tree->getFilename();
		

		//emit updateMsgLog(QString("> Exporting quantification for tree: %1...").arg(treeName), Qt::black);

		// code snippet from Olli - also loads the cell lineage tree csv if available
		CellLineageTree  *quant = tree->getCellLineageTree();
		QString err = Tools::writeCsvQuantification(file, *quant);

		if (!err.isEmpty()) {
			emit updateMsgLog(QString(">Write error: Failed to export quantification for tree: %1... %2").arg(file).arg(err), Qt::red);
			continue;
		}
		else {
			// check if experiment folder contains a TTT experiment
			emit updateMsgLog(QString("> Successful exporting of quantification for tree: %1...").arg(file), Qt::darkGreen);
		}

		emit updateProgressBar();
	}
	emit finished();
}

