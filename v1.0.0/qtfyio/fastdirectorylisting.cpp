/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "fastdirectorylisting.h"


// This code will not work if windows api expects UNICODE strings (TCHAR becomes WCHAR)
#ifdef UNICODE
	#error TTT must be compiled without the UNICODE symbol!
#endif

#include <Windows.h>

// Qt Includes
#include <QDebug>



QStringList FastDirectoryListing::listFiles( QDir _path, QStringList fileExtensions, const QStringList filePrefix, bool onlyDirs /* = false */ )
{
	// Check if dir exists
	if(!_path.exists())
		return QStringList();

	// Convert extensions to lower case
	for(QStringList::iterator it = fileExtensions.begin(); it != fileExtensions.end(); ++it)
		*it = it->toLower();

	// Extract path as native ascii string
	QByteArray tmp = QDir::toNativeSeparators(_path.absolutePath()).toAscii();

	// Append '\'
	if(tmp.right(1) != "\\")
		tmp += '\\';

	// Append '*'
	tmp += '*';

	// Note: path becomes invalid as soon as tmp gets destroyed
	const char* path = tmp.constData();

	// Make sure path is shorter than MAX_PATH
	if(tmp.length() >= MAX_PATH) {
		qWarning() << "listFiles(): Error 1";
		return listFilesWithQDir(_path, fileExtensions, filePrefix);
	}

	// Api data
	WIN32_FIND_DATA findData;
	HANDLE hFindFile;

	// Return value
	QStringList ret;

	// Current file
	QByteArray curFile;

	// Find first file
	hFindFile = FindFirstFile(path, &findData);
	if(hFindFile == INVALID_HANDLE_VALUE) {
		qWarning() << "listFiles(): Error 2 - " << path;
		return listFilesWithQDir(_path, fileExtensions, filePrefix);
	}

	// Add first file if it is no directory and has the right extension
	curFile = findData.cFileName;
	bool isDir = (findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY);
	if(((onlyDirs && isDir)||(!onlyDirs && !isDir)) && checkFile(curFile, fileExtensions, filePrefix))
		ret.append(findData.cFileName);

	// Find next files
	while(FindNextFile(hFindFile, &findData)) {
		// Add next file if it is no directory and has the right extension
		curFile = findData.cFileName;
		isDir = (findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY);
		if(((onlyDirs && isDir)||(!onlyDirs && !isDir)) && checkFile(curFile, fileExtensions, filePrefix))
			ret.append(findData.cFileName);
	}
	
	return ret;
}

QStringList FastDirectoryListing::listFiles(QDir path, QStringList fileExtensions, bool onlyDirs)
{
	return listFiles(path, fileExtensions, QStringList(), onlyDirs);
}

QStringList FastDirectoryListing::listFilesWithQDir( QDir path, const QStringList fileExtensions, const QStringList filePrefix )
{
	// Convert fileExtensions and filePrefix to QDir::entryList filters
	QStringList filters;
	for(int i = 0; i < fileExtensions.size(); ++i) {
		for(int j = 0; j < filePrefix.size(); j++) {
			filters.append(filePrefix[j] + "*" + fileExtensions[i]);
		}
	}

	// Use QDir::entryList
	return path.entryList(filters, QDir::Files, QDir::Name);
}

/**
 * Checks if the given _fileName has one of the file extensions and starts with one of the 
 */
bool FastDirectoryListing::checkFile( const QByteArray& fileName, const QStringList& fileExtensions, const QStringList& filePrefix)
{
	// Return true if no extensions and no prefix are given
	if((fileExtensions.size() == 0) && (filePrefix.size() == 0))
		return true;

	// Iterate over all provided file extensions
	bool extensionFound = false;
	for(int i = 0; i < fileExtensions.size(); ++i) {
		// Get provided file extension
		const QString curExt = fileExtensions[i];

		// Get file extension of _fileName
		const QString curFileExt = fileName.right(curExt.length()).toLower();
		if(curFileExt == curExt) {
			extensionFound = true;
			break;
		}
	}

	// If no extension matches return false
	if(!extensionFound) 
		return false;

	// Extension was found, so return true if no prefixes were specified
	if(filePrefix.size() == 0)
		return true;

	// Iterate over all prefix
	bool prefixFound = false;
	for(int i = 0; i < filePrefix.size(); i++) {

		// Check current prefix
		if(fileName.startsWith(filePrefix[i].toAscii())) {
			prefixFound = true;
			break;
		}
	}

	return prefixFound;
}
