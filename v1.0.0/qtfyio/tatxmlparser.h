/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @authors Bernhard Schauberger, Konstantin Azadov
 * @modifiedby Stavroula Skylaki
 * @datemodified 07/10/2015
 */

#ifndef TATXMLPARSER_H
#define TATXMLPARSER_H


// PROJECT
#include "qtfydata/tatinformation.h"
#include "qtfydata/cellsandconditions.h"

// QT
#include <qxml.h>


/**
	@author Bernhard
	
	This subclass of QXmlDefaultHandler is designed to react on the tags in a
	TAT XML file.
	
*/

class TATXMLParser : public QXmlDefaultHandler
{
public:
	TATXMLParser();
	
	~TATXMLParser();
	
	/**
	 * called when the reader starts to parse, before any tags are processed
	 * @return must return true, otherwise the reader stops
	 */
	bool startDocument();
	
  	/**
  	 * called when the reader parses a begin tag
  	 * @param _namespaceURI the namespace
  	 * @param _localName the name without the namespace
  	 * @param _name the name of the tag
  	 * @param _atts 
  	 * @return must return true, otherwise the reader stops
  	 */
  	bool startElement (const QString &_namespaceURI, const QString &_localName, const QString &_name, const QXmlAttributes &_attrs);
  	
	/**
	 * called when the reader parses an end tag
	 * always called with the same arguments as the last startElement() call
  	 * @param _namespaceURI the namespace
  	 * @param _localName the name without the namespace
	 * @param _name the name of the tag
  	 * @return must return true, otherwise the reader stops
	 */
	bool endElement (const QString &_namespaceURI, const QString &_localName, const QString &_name);
	
private:
	
	///following three bools: whether the reader is currently in the ... tag
	bool inTATSetting;
	bool inPositionData;
	bool inWavelengthData;
	bool inCellsAndConditions;
	bool inCNC_CTs_CellType;
	
	///these four attributes store the (current) maximum value for each border
	///necessary to reformat the coordinate system for display
	float coordinateTop;
	float coordinateLeft;
	float coordinateBottom;
	float coordinateRight;
	
	///whether the four coordinate bounds (attributes above) have already been set at least once
	bool coordinatesSet;
	
	///cells'n'conditions: local instances, to be copied to TATInformation later (when all values are set)
	CellsAndConditions cellsAndConditions;
	CNC_CellType cellType;
	
	
//private methods
	
	
	/**
	 * updates the bounds of the coordinate system
	 * @param _posX the abscisse of the current position
	 * @param _posY the ordinate of the current position
	 */
	void updateCoordinateBounds (float _posX, float _posY);
	
};

#endif
