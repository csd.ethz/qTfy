/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Stavroula Skylaki
 * @date 08/10/2015
 */


#ifndef TTTLOADINGOPERATIONS_H
#define TTTLOADINGOPERATIONS_H


// PROJECT
#include "qtfydata/tree.h"
#include "qtfydata/treewrapper.h"

// QT
#include <QString>
#include <QColor>
#include <QObject>

// required by qt to pass the signals
// for some reason using QVector<QSharedPointer<ITree>> confuses the meta object system
typedef QVector<QSharedPointer<ITree>> TreePointers;

class TTTLoadingOperations : public QObject
{
	Q_OBJECT


public:
	TTTLoadingOperations();
	~TTTLoadingOperations();
	
	/**
    * @brief sets the list of ttt files that have been selected by the user
	* @param QStringList the list of files - the file path 
	*/
	void setlistTTTtrees(const QStringList _selectedTrees){
		this->selectedTrees = _selectedTrees;
	}


	// return the error message
	QString getErrMsg (){
		return this->errMsg;
	}


	void setLogFilesFolder (QString _logFilesFolder){
		this->logFilesFolder = _logFilesFolder;
	}

	void setTTTFilesFolder (QString _tttFilesFolder){
		this->tttFilesFolder = _tttFilesFolder;
	}

	void setExperimentsFilesFolder (QString _experimentsFilesFolder){
		this->experimentsFilesFolder = _experimentsFilesFolder;
	}

	public slots:

		// starts the loading operations
		void startTTTLoading();

		// sets the flag for termination of loading
		void setTerminationRequest()
		{this->terminationFlag = true;}
		

signals:

	// updates the message log text box in the loading form
	void updateMsgLog(QString, QColor);

	// updates the progress bar in the loading form
	void updateProgressBar(int);

	// updates the maximum value of the progress bar in the loading form
	void setProgressBarMax(int);

	// searching for trees is over pass the trees to the main form
	void treesFound (QStringList);

	// loading done
	void finished();

	// when the reading of trees and log files is over
	// passes the pointers to the successfully loaded trees to the main thread
	void treesLoadedSuccess(TreePointers);

private:

	//// the paths to the files
	//// initialized to user's document folders
	QString logFilesFolder;
	QString tttFilesFolder;
	QString experimentsFilesFolder;

	bool tttFilesPathValid;
	bool experimentPathValid;

	// if true, no more ttt files will be loaded
	bool terminationFlag;
		
	// loading has encountered an error, error message
	QString errMsg;

	// the list of tree filepaths the user has selected
	QStringList selectedTrees;

};

#endif // TTTLOADINGOPERATIONS_H
