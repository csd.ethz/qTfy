/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LOGFILEREADER_H
#define LOGFILEREADER_H

// PROJECT
#include "qtfybackend/fileinfoarray.h"
#include "qtfydata/systeminfo.h"

// QT
#include <QString>
#include <QDateTime>
#include <QColor>
#include <QObject>

// STL
#include <fstream>
#include <math.h>

/**
@author Bernhard
*	This class reads the data of the logfile provided in the constructor.
*	The contents in Files are allocated in PictureArray so they can be read in 
*	all classes that demand them.
*	NOTE the log file is still written by a VB program which outputs it in proprietary format -> reading is very involved!
*	
*/

class LogFileReader : public QObject
{
	Q_OBJECT

public:


	LogFileReader ();	
	~LogFileReader();


	/**
	* constructs a log file reader object based on the parameters
	* @param _fileName the filename of the log file
	* @param _baseName the basename of the experiment (necessary for deciphering the length of the filename and its boundaries in the file)
	* @param _lastTimePoint reference parameter, set to the last timepoint of the experiment
	* @param _firstTimePoint reference parameter, set to the first timepoint of the experiment
	*/
	void startLogFileReader (QString _filename, QString _baseName, int &_lastTimePoint, int &_firstTimePoint);

	/**
	* @return whether the log file could be loaded
	*/
	bool LoadingSuccesful() const
	{return LoadingOK;}

	/**
	* @return whether the log file is up to date (number of wl0 pictures == last timepoint)
	*/
	bool LogFileUpToDate() const
	{return UpToDate;}

	/**
	* returns a pointer to the internal array
	* @return a FileInfoArray object that contains all information about the pictures
	*/
	FileInfoArray* getArray() const
	{return Files;}

signals:
	// updates the message log text box in the loading form
	void updateMsgLog(QString, QColor);


private:
	
	///the array containing all file information
	FileInfoArray *Files;
	
	///whether the log file could be loaded
	bool LoadingOK;
	
	///whether the fsi array length from the log file corresponds to the pictures in the folder
	bool UpToDate;
	
	///the number of wavelengths (obtained from logfile)
	int NumberOfWaveLengths;
	
	///the number of wavelength 0 pictures (obtained from logfile)
	int FSIArrayLength;
	
	///the highest/lowest timepoint read from file
	int MaxTimePoint;
	int MinTimePoint;
	
	/**
	 * reads the log file from disk
	 * @param _filename the filename of the log file
	 * @param _baseName the basename of the experiment
    	 * @param _suffix the file extension (jpg, tiff, ...)
	 * @return success
	 */
	bool read (QString _filename, QString _baseName/*, QString _suffix*/);

	// New binary log files magic number (inverted order due to little endian)
	static const quint32 SIGNATURE = 0x66676F6C;
};

#endif



/*
* extraction of VB
* ****************

* reading part
* ************
    
    Get #StringFileNum, , FSIArrayLength		//long -> 4 bytes
    Get #StringFileNum, , NumberOfWavelengths	//long -> 4 bytes
    
    LastRecordedTimePoint = Val(Mid$(FileList.List(FileList.ListCount - 1), 1, InStrRev(FileList.List(FileList.ListCount - 1), "_w") - 1))
    FirstRecordedTimePoint = Val(Mid$(FileList.List(0), 1, InStrRev(FileList.List(0), "_w") - 1))
    NumberOfDigitsInFilename = InStr(1, FileList.List(0), "_w") - 1
    If FSIArrayLength = LastRecordedTimePoint + 1 Then 'if number of imagefiles still same as when Zeisslogfile conversion was done
        ReDim FileInfo(FSIArrayLength) As TimepointInformation

        For counter = 0 To UBound(FileInfo) - 1
            ReDim FileInfo(counter).Wavelength(NumberOfWavelengths) As FileSelectionAndLoadingStatus
        Next
        
        Get #StringFileNum, , FileInfo()
    Else 'redo logfileconversion!
        MsgBox "fsi file does not correspond to jpg files in directory" & vbCrLf & "Please redo log file conversion" & vbCrLf & "Program will close"
        btnCancel_Click
        Exit Sub
    End If


* structure of FileInfo()
* ***********************

Public FileInfo() As TimepointInformation


Public Type TimepointInformation
    Wavelength() As FileSelectionAndLoadingStatus
End Type

Public Type FileSelectionAndLoadingStatus
    Filename As String				-> 10 + (length) bytes		=> QString filled with char
    CreationTime As Date			-> 8 bytes		=> QDateTime filled with char
    Selected As Boolean				-> 2 bytes		=> does not have to be filled!
    Loaded As Boolean				-> 2 bytes		=> does not have to be filled!
    MemoryBitmapPointer As Long		-> 4 bytes		=> does not have to be filled!
    Plane As Long					-> 4 bytes		=> does not have to be filled!

End Type


*/


