/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef FILEINFO_H
#define FILEINFO_H


// PROJECT
#include "systeminfo.h"

// QT
#include <QDateTime>
#include <QString>



///the marker for the timepoint section within a picture filename (continues till WAVELENGTH_MARKER)
const QString TIMEPOINT_MARKER = "_t";

///the marker for the wavelength section within a picture filename (continues till ".")
const QString WAVELENGTH_MARKER = "_w";

///the additional suffix to mark jpg/tiff meta information (created by TAT)
const QString PICTURE_META_APPENDIX = "_meta.xml";

/**
@author Bernhard
	
	This struct contains the filename, its timepoint and wavelength and the real time
	for a picture. For each picture there is exactly one FileInfo object.
*/

struct FileInfo
{
	/**
	 * constructs a default FileInfo object
	 */
	FileInfo() 
		: 	FileName (""), TimePoint (0), WaveLength (0), CreationTime () {}
	
	/**
	 * constructs a FileInfo object with the provided settings
	 * @param _fileName the complete filename
	 * @param _timePoint the timepoint of this image
	 * @param _waveLength the wavelength of this image
	 * @param _creationTime the acquisition time of this image
	 */
	FileInfo (const QString& _fileName, int _timePoint, int _waveLength, const QDateTime& _creationTime)
		: 	FileName (_fileName), TimePoint (_timePoint), 
			WaveLength (_waveLength), CreationTime (_creationTime) {}
	
	// @deprecated
	// Contains empty string!
	QString FileName;
	
	int TimePoint;
	int WaveLength;
	
	QDateTime CreationTime;

};


#endif
