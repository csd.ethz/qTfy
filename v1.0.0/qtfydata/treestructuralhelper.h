/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TREESTRUCTURALHELPER_H
#define TREESTRUCTURALHELPER_H


// PROJECT
#include "qtfybackend/itrack.h"

// QT
#include <QVector>


// Forward declarations
class ITree;


/**
 * A helper class which offers methods for structural tasks. E.g getting all
 * Tracks in a generation.
 *
 * Only static methods are used here and therefore no object of this type can
 * be created.
 */
class TreeStructuralHelper
{
public:

	/**
	 * @param _track for which the progeny has to be calculated
	 * @param _firstGeneration the lower (inclusive) bound of relative generations to be considered
	 * @param _lastGeneration the upper (inclusive) bound of relative generations to be considered
	 * @return all progeny tracks of the given _track between _firstGeneration and _lastGeneration
	 */
	static QVector<ITrack*> getProgenyInGenerations(const ITrack* _track, int _firstGeneration, int _lastGeneration);

	/**
	 * @param _track for which the ancestors have to be calculated
	 * @param _firstGeneration the lower (inclusive) bound of relative generations to be considered
	 * @param _lastGeneration the upper (inclusive) bound of relative generations to be considered
	 * @return all ancestors of the given _track between _firstGeneration and _lastGeneration
	 */
	static QVector<ITrack*> getAncestorsInGenerations(const ITrack* _track, int _firstGeneration, int _lastGeneration);

	/**
	 * @param _firstGeneration the lower (inclusive) bound of absolute generations to be considered
	 * @param _lastGeneration the upper (inclusive) bound of absolute generations to be considered
	 * @return all tracks which are in generation [_firstGeneration, _lastGeneration]
	 */
	static QVector<ITrack*> getTracksInGenerations(const ITree* _tree, int _firstGeneration, int _lastGeneration);

	/**
	 * tests whether the track is of type _cellType
	 * for the standard cell types (dividing, lost, interrupted, apoptotic) it is better to use getCellType(), but for composed types
	 *  only the result of this method is correct (e.g. CT_DIVIDINGCHILDRENTYPE)
	 * @param _track the track to be tested
	 * @param _cellType the queried cell type
	 * @return true, if the type of this cell corresponds to _cellType, false otherwise
	 */
	static bool isCellOfType(const ITrack* _track, const CellType _cellType);

	/**
	 * returns the cell types that occur in this tree
	 * the value of 4 bytes has to be interpreted:
	 * each type has a dual value corresponding to one bit in the long; so if you want to know if e.g. apoptotic cells are contained,
	 * then check for ([result] & CT_APOPTOTICTYPE != 0)
	 * @return a long value coding the cell types in this tree (see above) 
	 */
	static long getCellTypes(const ITree* _tree);
	static long getCellTypesRecursive (const ITrack *_track, long _result);

	/**
	 * Returns the sister cell, or 0 if there is none!
	 */
	static ITrack* getSister(const ITrack* cell);

	/**
	 * Returns the last trackpoint in the given tree!
	 */
	static int getLastTreeTimePoint(const ITree* tree);

private:
	TreeStructuralHelper() { };
	~TreeStructuralHelper() { };

};

#endif // TREESTRUCTURALHELPER_H
