/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
* @authors Bernhard Schauberger, Konstantin Azadov
* @modifiedby Stavroula Skylaki
* @datemodified 08/10/2015
*/


#ifndef TRACK_H
#define TRACK_H


#include "comment.h"
#include "qtfybackend/itrack.h"
#include "qtfybackend/itree.h"
#include "cellproperties.h"
#include "trackpoint.h"

#include <QString>
#include <QPoint>
#include <QPair>
#include <QVariant>




///dummy trackpoint, necessary for functions returng TrackPoint references
const TrackPoint DUMMYTRACKPOINT;

const QString TRACK_OPT_START_TP = "StartTP"; //--------------------- Konstantin ------------------------
const QString TRACK_OPT_STOP_TP = "StopTP"; //--------------------- Konstantin ------------------------
const QString TRACK_OPT_ENTIRE_DISTANCE = "EntireDistance"; // Has to be the same as ITrack::TRACK_DISTANCE
const QString TRACK_OPT_LIFETIME = "CellLifetime"; // Has to be the same as  ITrack::TRACK_LIFETIME
const QString TRACK_OPT_AVERAGE_SPEED = "AverageSpeed"; // Has to be the same as  ITrack::TRACK_AVG_SPEED


//class TTTManager; 
class TTTPositionManager;
class Tree;



/**
@author Bernhard

This class contains all information about one single cell.
All access functions are provided, like single TrackPoint access, pointers to
the mother and daughter cells (if existent) and various attributes belonging to
each cell.
*/


class Track : public ITrack
{
public:
	Track (Track *_motherTrack, int _childIndex, int _maxTimepoint, int _firstTrace = -1);

	~Track();

	/// sets pointer to tree this track comes from
	void setTree(Tree* _tree)
	{
		tree = _tree;
	}
	
	Tree* getTree() const
	{
		return tree;
	}

	///adds a track mark to the list at _timepoint
	///_mouseButtons codes for Adherent / FreeFloating
	///@param _timepoint the timepoint for which the trackpoint should be inserted (if it already exists, it is updated)
	///@param _x the x coordinate of the trackpoint
	///@param _y the y coordinate of the trackpoint
	///@param _diameter the diameter of the cell circle
	///@param _setBackground whether x/y apply to the background circle or to the cell itself
	///@param _mouseButtons shortcut for the cell state (spyhop/freefloating/adherent)
	///@param _position the index of the position in which the trackpoint was set
	// @return added/changed trackpoint or 0 in case of error
	TrackPoint* addMark (int _timepoint, float _x, float _y, float _diameter = 0, bool _setBackground = false, short int _mouseButtons = 0, QString _position = "");

	///**
	//* adds the provided trackpoint to the trackpoint list.
	//* if there already exists one at the given timepoint and _replaceIfExists is true, it is replaced by _tp
	//* @param _tp the trackpoint to be added
	//* @param _replaceIfExists set to false to forbid replacing of existing trackpoint
	//* @return success
	//*/
	//bool addTrackPoint (const TrackPoint &_tp, bool _replaceIfExists = true);

	/////deletes the trackpoint at _timepoint (if it exists)
	/////@param _timepoint the timepoint where the trackpoint should be deleted
	/////@return success (false, if there was no trackpoint to delete)
	//bool deleteMark (int _timepoint);

	/////delets all marks/trackpoints
	//void deleteMarks();

	///sets MaxTimePoint (necessary for various purposes)
	///@param _maxTimepoint the maximal timepoint in the tree, i.e. LastTimePoint)
	void setMaxTimePoint (int _maxTimePoint) ;

	///sets the child tracks
	///@param _number whether child 1 or 2 should be set
	///@param _track the child track (should be an already existing track, or 0)
	void setChildTrack (int _number, Track *_track);

	///getter & setter for the first/last trace	(Note: FirstTrace does not necessarily have a trackpoint, even if it is a valid timePoint!)
	int getFirstTrace() const;
	void setFirstTrace (int _firstTrace) 
	{updateFirstTrace (_firstTrace);}
	int getLastTrace() const;
	void setLastTrace (int _lastTrace);

	///whether _timePoint lies between first and last tracked timepoint
	///@param _timePoint the timepoint which should be checked
	///@return true if the cell is alive
	bool aliveAtTimePoint (int _timePoint) const;

	/////returns the number of trackpoints that are really tracked
	/////@return the number of trackpoints
	int getMarkCount() const;

	///returns the mother track
	/// is 0 only for the base cell
	///@return a pointer to the track object which is THIS cell's mother cell
	Track* getMotherTrack() const;

	///returns the child track, either child 1 or child 2
	/// can be 0!
	///@param _child which child track is desired (1 or 2)
	///@return a pointer to the track object which is THIS cell's child cell (1 or 2)
	Track* getChildTrack (int _child = 1) const;

	/////returns a TrackPoint object at _timepoint
	/////@param _timepoint the query timepoint
	/////@param _roundUp if a trackpoint at _timepoint does not exist, it decides whether 
	/////                 the lowest timepoint which is higher than the desired should be returned
	/////@return the TrackPoint object at _timepoint or the next above
	//const TrackPoint& getTrackPoint (int _timepoint, bool _roundUp = false) const;

	/////returns a pointer to the TrackPoint object at _timepoint
	/////@param _timepoint the query timepoint
	/////@param _roundUp if a trackpoint at _timepoint does not exist, it decides whether 
	/////                 the lowest timepoint which is higher than the desired should be returned
	/////@return the TrackPoint object pointer at _timepoint or the next above; 0 if none exists
	/////@warning should be used very limited!!!
	//TrackPoint* getRefTrackPoint (int _timepoint, bool _roundUp = false) const;

	///**
	//* @return all trackpoints as a constant(!) vector
	//*/
	const QVector<TrackPoint*> getAllTrackpoints() const
	{return Positions;}

	///returns the _number-th TrackPoint in the array where TimePoint > 0
	///note: _number starts with 1!
	///@param _number the counter for the trackpoints (starts with 1)
	///@return the TrackPoint object which is the number-th where TimePoint > 0
	const TrackPoint& getTrackPointByNumber (int _number); //const;

	//setter for the stop reason
	void setStopReason (TrackStopReason _stopReason);

	/////returns the stop reason as string / short int 
	/////necessary for display / storage
	QString getStopReasonString() const;
	/////@return the stop reason coded as integer
	short int getStopReasonShortInt() const;

	
	///returns the generation of the track (base track has generation 0)
	///@return the generation of the cell
	int getGeneration() const;

	///returns the child index
	///@return 1 or 2, depending on whether this child is left or right of its mother cell
	int getChildIndex() const;

	/**
	* @return the sibling of this cell, either left or right; e.g. 2 -> 3, 9 -> 8, ... (can be 0!)
	*/
	Track *getSibling() const;

	///getter & setter for the track number (linear ascending)
	///the base track has number 1
	int getNumber() const;
	void setNumber (int _number);

	///returns true if at _timepoint a tracking point was set by the user
	///@param _timepoint the timepoint which should be checked
	///@return true, if a track at _timepoint exists, false otherwise
	bool trackPointExists (int _timepoint) const;

	///sets the track comment for the specified timepoint
	///@param _comment the comment for this timepoint
	///@param _timePoint the timepoint for which the comment should be set
	void setComment (QString _comment, int _timePoint) 
	{comment.setComment (_timePoint, _comment);}

	///returns the comment for the desired timepoint
	///@param _timePoint the timepoint
	///@return a QString object that contains the comment at _timePoint
	const QString getComment (int _timePoint) const
	{return comment.getComment (_timePoint);}

	///sets the complete track comment
	///@param _comment the comment for this track, for ALL timepoints
	void setCompleteComment (QString _comment)
	{comment.set (_comment);}

	///returns the complete comment (for ALL timepoints) for this track
	///@return a QString object which holds all comments entered at any timepoint for this cell
	const QString getCompleteComment() const
	{return comment.get();}

	///whether the cell was already tracked
	///@return true, if the track has track points anywhere in its lifetime, otherwise false
	bool tracked() const;

	/////whether the background of this cell was already tracked
	/////@param _wavelength the wavelength for the background track should be checked (currently not used)
	/////@return true, if this track has a background tracked for any timepoint
	//bool backgroundTracked (int _wavelength = -1) const;

	///sets the specified properties at the specified timePoint
	///@param _timePoint the timepoint for which the properties should be set
	///@param _props the CellProperties object that holds the properties to be set
	///@param _propertyform: whether this method was called after the user set properties (== true)
	///			       or while loading a ttt file (== false)
	///@param _doNotOverwriteEndomitosis if true, endomitosis will not be changed.
	///@param _addWavelengthsOnly if true, wavelengths will be enabled according to _props, but not disabled, i.e. wavelengths will only be added
	///@return: success (whether a trackpoint at _timePoint exists)
	bool setProperties (int _timePoint, const CellProperties& _props, bool _propertyForm, bool _doNotOverwriteEndomitosis = false, bool _addWavelengthsOnly = false);

	/////deletes all trackpoints that are before the first or after the last trace
	//void cutOffTrackPoints();

	///sets the attribute SecondsFromBegin (used for fast calculations)
	///is called anytime this value changes in TreeDisplay
	///@param _sfb the difference between the global first time point and the first trace of this cell
	void setSecondsFromBegin (int _sfb);

	///returns the value of the attribute SecondsFromBegin
	///@return the difference between the global first time point and the first trace of this cell
	long int getSecondsFromBegin() const;

	/////check if THIS track is a child of _track over any generation count
	/////@param _track the supposed mother track
	/////@return true, if _track is an ancestor (in any generation) of this track, otherwise false
	//bool childOf (const Track *_track) const;

	/////check if THIS track is an ancestor of _track
	/////@param _track the supposed child track
	/////@return: true, if _track is a child (in any generation) of this track, otherwise false
	//bool ancestorOf (const Track *_track) const;


	////--------------------Konstantin----------------------------
	///// returns a list of a progeny tracks in generation _gen for a current track
	///// @param _gen generation in which the progeny should be founded
	//QList<Track*> getProgenyInGeneration(const int _gen) const;



	/////returns a list of all x/y pairs (as QPointF objects) between _startTP and _endTP
	///// of this track (in picture coordinates)
	/////@param _startTP: if -1, the first trace is assumed
	/////@param _endTP: if -1, the last trace is assumed
	/////@param _background whether the background coordinates should be returned (true) rather than the track coordinates (false, default)
	/////@return a QValueVector<QPair<int, QPointF> > of all positions where the track was actually tracked(!) with the respective timepoint
	//QHash<int, QPointF> getPositions (int _startTP = -1, int _endTP = -1, bool _background = false) const;


	/**
	* get all trackpoints between startTP and endTP
	* @param _startTP the starting timepoint, first available assumed if not given
	* @param _endTP the ending timepoint, last available assumed if not given
	* @return QMap<int, ITrackPoint*> all trackpoints in range sorted by the trackpoint number
	*/
	QMap<int, ITrackPoint*> getTrackPointsRange(int _startTP = -1, int _endTP = -1) const;

	/**
	* @return whether this track has children
	* NOTE: it is assumed that these always occur in doubles, never alone
	* 	 thus, if just one child is present, false is returned
	*/
	bool hasChildren() const;

	///**
	//* @deprecated
	//* calculates the radius within which the cell is moving during its whole lifetime
	//* @param _startTP the first timepoint that should be considered for the calculation (-1 => first time point of the cell)
	//* @param _endTP the last timepoint that should be considered for the calculation (-1 => last time point of the cell)
	//* @return the action radius (in pixel)
	//*/
	//int calcActionRadius (int _startTP = -1, int _endTP = -1) const;

	///**
	//* @deprecated
	//* calculates the total covered distance of the cell during its whole lifetime
	//* @param _startTP the first timepoint that should be considered for the calculation (-1 => first time point of the cell)
	//* @param _endTP the last timepoint that should be considered for the calculation (-1 => last time point of the cell)
	//* @return the covered distance (in pixel)
	//*/
	//int calcTotalDistance (int _startTP = -1, int _endTP = -1) const;

	///**
	//* calculates the radius within which the cell is moving during its whole lifetime
	//* calculates the total covered distance of the cell during its whole lifetime
	//* returns the calculations as reference parameters
	//* @param _cellActionRadius the action radius in pixel (reference return)
	//* @param _cellTotalDistance the total distance in pixel (reference return)
	//* @param _distanceDT the ratio of distance between first and last timepoint divided by entire distance (reference return)
	//* @param _MSD the mean square displacement (reference return)
	//* @param _startTP the first timepoint that should be considered for the calculation (-1 => first time point of the cell)
	//* @param _endTP the last timepoint that should be considered for the calculation (-1 => last time point of the cell)
	//*/
	//void calcMovements (float &_actionRadius, float &_totalDistance,  float &_distanceDT, float &_MSD, int _startTP = -1, int _endTP = -1) const;

	////--------------------- Konstantin ------------------------
	///**
	//* calculates the continuous attribute values (distance, speed) between all neighbouring timepoints between _startTP and _endTP
	//* @param _nameAttr name of the continuous attribute
	//* @param _nameTimeSpecification name of time specification (experiment time, cell lifetime or time point normalized with cell lifetime)
	//* @param _tttpm position manager of position, in which was the start of tracking of cell
	//* @param _startTP the first timepoint that should be considered for the calculation (-1 => first time point of the cell)
	//* @param _endTP the last timepoint that should be considered for the calculation (-1 => last time point of the cell)
	//* @return Vector with points (time from _startTP, continuous value)   in case _ifXAbsoluteTime = false
	//*                            (absolute time, continuous value)        in case _ifXAbsoluteTime = true
	//*         The first value is (0, 0)               in case _ifXAbsoluteTime = false
	//*                            (FirstTrace, 0)      in case _ifXAbsoluteTime = true
	//*/
	////QVector<QPointF> getContinuousValuesVector(QString _nameAttr, QString _nameTimeSpecification, const TTTPositionManager *_tttpm , int _startTP = -1, int _endTP = -1) const;

	///returns the value of track attributes (cf. constants above)
	///@param _name name of the track attribute
	///@return value of track attribute (in seconds for Lifetime) (float because of speed)
	float getOptValue(QString _name, const TTTPositionManager *_tttpm) const;

	/**
	* @return the type of this cell as CellType value
	*/
	CellType getCellType() const;

	///**
	//* tests whether this track is of one of the types provided in _cellTypes (or all of the types if _all is true)
	//* @param _cellTypes the cell types that should be checked, OR-ed together
	//* @return true, if this cell subsumes at least one (or all, confer _all) of the provided cell types
	//*/
	////* @param _all whether it should be checked, if this cell subsumes ALL of the types (true), or if already one is enough (false, default)
	//bool subsumesCellTypes (long _cellTypes) const; //, bool _all = false) const;

	///**
	//* returns the average of the desired attribute
	//* @param _attributeName the name of the attribute (cf. constants in trackpoint.h)
	//* @param _select additional parameter to specify the attribute (e.g. for wavelenghts)
	//* @return the average of the desired value (all are represented as int)
	//*/
	//int getAverage (const QString &_attributeName, int _select = -1) const;

	///**
	//* returns the timepoint of the first occurence of the desired attribute
	//* @param _attributeName the name of the attribute (cf. constants in trackpoint.h)
	//* @param _select additional parameter to specify the attribute (e.g. for wavelenghts)
	//* @return the timepoint of the first occurence
	//*/
	//int getFirstOccurence (const QString &_attributeName, int _select = -1) const;

	////------------------ Konstantin ----------------
	///**
	//* returns the timepoint of the first occurence of one of 15 add-on attributes (15 tracking keys) of according Bitpack
	//* @param _attributeName the name of the attribute (cf. constants in trackpoint.h)
	//* @param _bitpack number of bitpack (0-7)
	//* @param _addOnValue number of add-on attribute (1-15 for tracking keys)
	//* @return the timepoint of the first occurence
	//*/
	//int getFirstOccurenceBitpack (const QString &_attributeName, int _bitpack, int _addOnValue) const;

	///**
	//* returns the timepoint of the last occurence of the desired attribute
	//* @param _attributeName the name of the attribute (cf. constants in trackpoint.h)
	//* @param _select additional parameter to specify the attribute (e.g. for wavelenghts)
	//* @return the timepoint of the last occurence
	//*/
	//int getLastOccurence (const QString &_attributeName, int _select = -1) const;

	////------------------ Konstantin ----------------
	///**
	//* returns the timepoint of the last occurence of one of 15 add-on attributes (15 tracking keys) of according Bitpack
	//* @param _attributeName the name of the attribute (cf. constants in trackpoint.h)
	//* @param _bitpack number of bitpack (0-7)
	//* @param _addOnValue number of add-on attribute (1-15 for tracking keys)
	//* @return the timepoint of the last occurence
	//*/
	//int getLastOccurenceBitpack (const QString &_attributeName, int _bitpack, int _addOnValue) const;


	////------------------ Konstantin ----------------
	///**
	//* returns (in seconds) how long one attribute occurs in track
	//* NOTE: not the same as (getLastOccurence - getFirstOccurence) since one attribute can occur several times during lifetime
	//*       (e.g. change between semiadnerence and adherence)
	//* @param _attributeName the name of the attribute (cf. constants in trackpoint.h)
	//* @param _select additional parameter to specify the attribute (e.g. for wavelenghts)
	//* @return durability of occurence
	//*/
	//int getAttributeOccurenceDurability (const QString &_attributeName, int _select, const TTTPositionManager *_tttpm) const;

	////------------------ Konstantin ----------------
	///**
	//* returns (in seconds) how long one of 15 add-on attributes (15 tracking keys) of according Bitpack occurs in track
	//* NOTE: not the same as (getLastOccurenceBitpack - getFirstOccurenceBitpack) since one attribute can occur several times during lifetime
	//*       (e.g. change between semiadnerence and adherence)
	//* @param _attributeName the name of the attribute (cf. constants in trackpoint.h)
	//* @param _bitpack number of bitpack (0-7)
	//* @param _addOnValue number of add-on attribute (1-15 for tracking keys)
	//* @return durability of occurence
	//*/
	//int getBitpackAttributeOccurenceDurability (const QString &_attributeName, int _bitpack, int _addOnValue, const TTTPositionManager *_tttpm) const;

	///**
	//* returns the value of the desired attribute at the first trace of this cell
	//* @param _attributeName the name of the attribute (cf. constants in trackpoint.h)
	//* @param _select additional parameter to specify the attribute (e.g. for wavelenghts)
	//* @return a void pointer (normally int/char/...) that contains the desired value
	//*/
	//int getStartSetting (const QString &_attributeName, int _select = -1) const;

	///**
	//* returns the value of the desired attribute at the last trace of this cell
	//* @param _attributeName the name of the attribute (cf. constants in trackpoint.h)
	//* @param _select additional parameter to specify the attribute (e.g. for wavelenghts)
	//* @return a void pointer (normally int/char/...) that contains the desired value
	//*/
	//int getEndSetting (const QString &_attributeName, int _select = -1) const;

	///**
	//* @deprecated
	//* sets the given x/y coordinates at the given timepoint (must already exist)
	//* NOTE: its use is stronly @deprecated ! and should only be used if no other way exists !
	//* @param _tp the timepoint (a trackpoint there must exist, otherwise nothing is done)
	//* @param _x the x
	//* @param _y the y
	//*/
	//void setTPXY (int _tp, float _x, float _y);

	///**
	//* erases all position indices stored while tracking
	//* note: this does not affect the coordinates, the positions are then calculated as before
	//* necessary if by some other error the positions where the trackpoint was set is not correct anymore
	//*/
	//bool eraseStoredPositions();

	///**
	//* Try to append the provided _tree to this track.
	//* Function is optimized for safety, i.e. no existing trackpoints or tracks will be replaced unless _allowOverwriteOfTrackPoints is true.
	//* @param _tree pointer to tree object
	//* @param _allowOverwriteOfTrackPoints if it should be allowed to overwrite existing trackpoints (i.e. if appending an overlapping fragment should be allowed)
	//* @param _errorMsg optional pointer to a string object where error message will be placed in if this function fails
	//* @return true if successful
	//*/
	//bool appendTree(Tree* _tree, bool _allowOverwriteOfTrackPoints, QString* _errorMsg = 0);

	///**
	//* Get last position of this track at or before specified timepoint. If this track contains no trackpoints at timepoint or before,
	//* it is looked for the last position of the mother track. If no position is found, a QPointF(0,0) object is returned
	//* @param _timePoint the timepoint
	//* @return QPointF object
	//*/
	//QPointF getLastPositionAtOrBefore(int _timePoint) const;

	///**
	//* Cut track, i.e. create new tree starting with this track as root track after the specified timepoint
	//* and remove trackpoints and tracks from new tree from this tree
	//* @param _timepoint the first timepoint for the new tree, last timepoint of this track will be _timepoint - 1
	//* @return pointer to new tree or 0 if operation failed
	//*/
	//QSharedPointer<Tree> cutTrack(int _timePoint);

	///**
	//* Prune tree, i.e. cut off and discard all track points and child tracks after (excluding)
	//* the given time point.
	//* @return true if successful.
	//*/
	//bool pruneTree(int _timePoint);

	////////////////////////////////////////////////////////////////////////////
	//// ITrack interface
	////////////////////////////////////////////////////////////////////////////

	/**
	* @return child 1 or 0 if not available (track with 2*number of this track)
	*/
	Track *getChild1() const {
		return ChildTrack1;
	}

	/**
	* @return child 2 or 0 if not available (track with 2*number+1 of this track)
	*/
	Track *getChild2() const {
		return ChildTrack2;
	}

	/**
	* @return child 2 or 0 if not available (track with 2*number+1 of this track)
	*/
	bool hasChildren() {

		if (ChildTrack1 || ChildTrack2)
			return true;

		return false;
	}

	/**
	* @return mother track or 0 if not available
	*/
	Track *getMother() const {
		return MotherTrack;
	}

	/**
	* Note: FirstTrace does not necessarily have a trackpoint, even if it is a valid timePoint!
	* @return first timepoint of this track or -1 if not available
	*/
	int getFirstTimePoint() const {
		return FirstTrace;
	}

	/**
	* @return last timepoint of this track or -1 if this track contains no trackpoints or if not available
	*/
	int getLastTimePoint() const {
		return LastTrace;
	}

	/**
	* get trackpoint at a certain timepoint
	* @param _timePoint the timepoint, does not have to be valid
	* @return trackpoint at _timePoint or 0 this track contains no such trackpoint
	*/
	TrackPoint* getTrackPointByTimePoint(int _timePoint) const;

	///**
	//* Get last trackpoint before the provided timepoint.
	//* @param _timePoint last track point before this time point will be returned.
	//* @param _scanMother if search should be continued in mother tracks.
	//* @return pointer to found track point or nullptr.
	//*/
	//TrackPoint* getLastTrackPointBefore(int _timePoint, bool _scanMother) const;

	/**
	* @return the track number of this track or -1 if not available
	*/
	int getTrackNumber() const {
		if(Number)
			return Number;
		return -1;
	}

	/**
	* @return the tree this track belongs to
	*/
	ITree* getITree() const 
	{
		return (ITree*)tree;
	}

	/**
	* @return stop reason for this track
	*/
	TrackStopReason getStopReason() const;

	/**
	* @return seconds between first and given trackpoint
	* NOT IMPLEMENTED HERE BECAUSE WE NEED THE POSITIONMANAGER
	*/
	int getSecondsForTimePoint(int timepoint) const
	{
		qDebug() << "Warning getSecondsForTimePoint called on raw track object!";
		return -1;
	}

	/**
	* Same as above, only implemented in CellWrapper
	*/
	int getAbsoluteSecondsForTP(int timepoint) const
	{
		qDebug() << "Warning getAbsoluteSecondsForTP called on raw track object!";
		return -1;
	}

	/**
	* get certain track properties.
	* Not implemented here, because we need e.g. the PositionManager
	*/
	QVariant getTrackProperty(QString _propName) const
	{
		qDebug() << "Warning getTrackProperty called on raw track object!";
		return QVariant();
	}


	///**
	//* @Deprecated (still used in Tree::getCellTypesRecursive) 
	//*
	//* tests whether this track is of type _cellType
	//* for the standard cell types (dividing, lost, interrupted, apoptotic) it is better to use getCellType(), but for composed types
	//*  only the result of this method is correct (e.g. CT_DIVIDINGCHILDRENTYPE)
	//* @param _cellType the queried cell type
	//* @return true, if the type of this cell corresponds to _cellType, false otherwise
	//*/
	//bool isOfCellType (CellType _cellType) const;

	////static methods	

	/////returns the enum StopReason from short int _stopReason
	/////@param _stopReason the stop reason as number (as returned by getStopReasonShortInt())


	///**
	//* @return number of cell types except 'CT_NOCELLTYPE' and  'CT_ALLTYPES'
	//*/
	//static int getNumberOfCellTypes();

	//static long getLastCellType()
	//{
	//	return CT_NOLOSTCHILDRENTYPE;
	//}

	static TrackStopReason recoverStopReason (short int _stopReason);

	///**
	//* converts a value of the enum CellType to long (by returning its assigned value)
	//* @param _cellType the cell type
	//* @return a long value representing this cell type uniquely
	//*/
	//static long cellTypeToLong (CellType _cellType);

	///**
	//* converts a long value to a value of the enum CellType (by looking for the type corresponding to the provided number)
	//* @param _cellType the cell type as long
	//* @return the CellType which matches the provided long _cellType; if none such exists, CT_NOCELLTYPE is returned
	//*/
	//static CellType longToCellType (long _cellType);

	///**
	//* converts a value of the enum CellType to string
	//* @param _cellType the cell type
	//* @return a string representing this cell typ
	//*/
	//static const QString cellTypeToString (CellType _cellType);

	///**
	//* converts a string to a value of the enum CellType
	//* @param _cellType the cell type encoded as string (should be obtained via cellTypeToString())
	//* @return a cell type enum value
	//*/
	//static CellType stringToCellType (const QString &_cellType);

	///**
	//* merges two trees, into each other or into a new tree
	//* consistency and possibility checks are performed
	//* @param _treeFrom the tree which should be inserted (or at least parts of it)
	//* @param _treeInto the tree into which should be inserted
	//* @param _cellFrom the cell of _treeFrom which should be inserted into _treeInto as ...
	//* @param _cellInto ... this cell number (must not have children!)
	//* @param _newTree if !=0, the two trees are merged into this one rather than into each other
	//* @return success
	//*/
	//static bool mergeTrees (Tree *_treeFrom, Tree *_treeInto, int _cellFrom, int _cellInto, Tree *_newTree);

private:
	/// pointer to tree, from which this track comes
	/// NOTE: this value is not stored in ttt file and is set in TTTStatistics::updateView()
	///       to make the calculations faster
	// OH: Now also set in Tree::insert() and initialized with 0
	Tree* tree; 

	///the number of the track
	///corresponding to the old windows cell numbers
	///that means: base cell of a colony is number 1
	///children are linear ascending
	int Number;

	///the array of tracking points (x/y-pairs + properties)
	QVector<TrackPoint*> Positions;

	///the track comment
	///special format to store timepoint specific comments
	Comment comment;

	///the number of trackpoints
	int MarkCount;

	// the timepoint of the first and last tracking point 
	// the FirstTrace is not necessarily user set (e.g. track could contain no trackpoint at FirstTrace 
	// if the user has not set a trackpoint after the division of the parent cell)!
	int FirstTrace;
	int LastTrace;

	///the last possible timepoint 
	///(necessary for updating FirstTrace if all tracking points were deleted)
	int MaxTimePoint;

	///the stop reason
	TrackStopReason StopReason;

	///the pointer to the mother track
	Track *MotherTrack;

	///the pointers to the child tracks
	Track *ChildTrack1;
	Track *ChildTrack2;

	///ChildIndex is important for many backtracking routines
	///specifies for THIS track whether it is a child 1 or 2 of the mother track (0, if THIS is a base track)
	int ChildIndex;

	///stores the seconds that are passed since global first time point and the first
	/// trace of THIS track
	///this storage aids better time behavior when drawing the tree, so that this does
	/// not always have to be calculated again
	///only re-calculated when == 0
	///if the first trace of the track changes, it is set to 0
	long int SecondsFromBegin;

	

	////private methods

	///sets the specified settings in TrackPoint
	///@param _tp the track point object which should be updated by the future parameters
	///@param _timepoint the timepoint of this trackpoint
	///@param _x the x position
	///@param _y the y position
	///@param _diameter the cell diameter
	///@param _setBackground whether the x/y pair should be used for normal coordinates (false) or the background coordinates (true)
	///@param _nonAdherent whether the cell is not adherent
	///@param _freeFloating whethre the cell is free floating
	///@param _position the index of the position in which the trackpoint was set
	void setTrackPoint (TrackPoint &_tp, int _timepoint, float _x, float _y, float _diameter, bool _setBackground, bool _nonAdherent, bool _freeFloating, QString _position);

	/////returns whether _track1 is in any generation a child of _track2
	/////@param _track1 the supposed mother track
	/////@param _track2 the supposed child track
	/////@return true, if _track1 is an ancestor over any number of generations of _track2
	//bool childOf (const Track *_track1, const Track *_track2) const;

	
	/// returns a list of a progeny tracks in generation _gen for a track _track
	/// @param _track1 track for which the progeny should be founded
	/// @param _gen generation in which the progeny should be founded
	QList<Track*> getProgenyInGeneration(const Track *_track, const int _gen) const;

	///calculates the internal array index from the timepoint
	///needs to implement a bijective, ascending function f: timepoint |-> index
	///note: changes here also affect updateFirstTrace()
	///@param _timepoint the real timepoint
	///@return the internal index for Positions for this timepoint
	int calcIndex (int _timepoint) const {
		return _timepoint - FirstTrace;
	}

	/**
	* sets the attribute FirstTrace to the provided new value and shifts all tracks in the internal array,
	*  as the index shifts after a new first trace is set (cf. also calcIndex())
	* i.e. this method maintains the mapping  calcIndex: timepoint |-> pointer
	* note: changes here also affect calcIndex()
	* @param _newFirstTrace the new first trace of this cell
	* @return success (whether all tracks could be updated)
	*/
	bool updateFirstTrace (int _newFirstTrace);

	///**
	//* Recursively append child tracks
	//* @param _child1 left child
	//* @param _child2 right child
	//*/
	//void recursivelyAppendChildren(Track* _childToAppend, bool _leftChild);

	///**
	//* Recursively cut track (see cutTree() for more details)
	//* @param _track track of this tree, will be moved to _newTree
	//* @param _oldTree pointer to old tree (needed since Track::tree is sometimes not set)
	//* @param _newTree new tree to move _track to
	//* @param _newParent new parent for _track (can be old parent, with a different track number though)
	//* @param _newTrackNumber new track number
	//*/
	//void recursivelyCutTree(QSharedPointer<Track> _track, Tree* _oldTree, Tree* _newTree, Track* _newParent, int _newTrackNumber);	

	///**
	//* Get child track as shared pointer.
	//* @param _child which child track is desired (1 or 2).
	//* @return shared pointer, can be null.
	//*/
	//QSharedPointer<Track> getChildTrackShared(int _child);

};


inline int Track::getFirstTrace() const 
{
	return FirstTrace;
}

inline int Track::getLastTrace() const 
{
	return LastTrace;
}

inline int Track::getNumber() const
{
	return Number;
}

inline void Track::setMaxTimePoint (int _maxTimePoint) 
{
	MaxTimePoint = _maxTimePoint;
}

inline void Track::setLastTrace (int _lastTrace) 
{
	LastTrace = _lastTrace;
}

inline bool Track::aliveAtTimePoint (int _timePoint) const
{
	return (FirstTrace <= _timePoint) & (LastTrace >= _timePoint);
}

inline int Track::getMarkCount() const
{
	return MarkCount;
}

inline Track* Track::getMotherTrack() const
{
	return MotherTrack;
}

inline Track* Track::getChildTrack (int _child) const
{
	if (_child == 1) return ChildTrack1; 
	else return ChildTrack2;
}

inline TrackStopReason Track::getStopReason() const
{
	return StopReason;
}

inline int Track::getChildIndex() const
{
	return ChildIndex;
}

inline void Track::setNumber (int _number) 
{
	Number = _number;
}


inline bool Track::tracked() const
{
	return (MarkCount > 0);
}


#endif
