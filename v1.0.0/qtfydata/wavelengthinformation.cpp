/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @authors Bernhard Schauberger
 * @modifiedby Stavroula Skylaki
 * @datemodified 07/10/2015
 */


// PROJECT
#include "wavelengthinformation.h"
#include "qtfybackend/tttmanager.h"
#include "qtfydata/tatinformation.h"

WavelengthInformation::WavelengthInformation()
	: wavelength (-1), width (-1), height (-1), scanBinFactor (-1)
{
}

WavelengthInformation::WavelengthInformation (int _wavelength, int _width, int _height, const QString& _comment)
	: wavelength (_wavelength), width (_width), height (_height), comment(_comment)
{
	scanBinFactor = getScanFromSize (width);
}


WavelengthInformation::~WavelengthInformation()
{
}

float WavelengthInformation::getMicrometerPerPixel() const
{
	// If micrometer per pixel were specified directly, use that value
	if(TATInformation::getInst()->getMicroMeterPerPixelIfDirectlySpecifiedInTatXml() > 0.0)
		return TATInformation::getInst()->getMicroMeterPerPixelIfDirectlySpecifiedInTatXml();

	// Calculate value
	return getMicrometerPerPixel (TTTManager::getInst().getOcularFactor(), TTTManager::getInst().getTVFactor(), scanBinFactor);
}

float WavelengthInformation::getMicrometerPerPixel (int _ocularFactor, float _tvAdapter, int _scanBin)
{
	float result = (float)BASE_ANGSTROOM_PER_PIXEL;
	
	if (_ocularFactor == -1)
		_ocularFactor = 5;
	if (_tvAdapter == -1.0f)
		_tvAdapter = 1.0f;
	if (_scanBin == -1)
		_scanBin = 1;
	
	
	//ocular factor
	//base value is correct for objective magnification (=ocular factor) 20, the rest is calculated lineary
	result = result * 20.0f / (float)(_ocularFactor);
	
	
	
	if (_tvAdapter == 1.0f)
		//already ok in base value
		;
	else if (_tvAdapter == 0.0f)
		//TV adapter 1.0 assumed => as in base value
		;
	else
		result /= _tvAdapter;
	
	switch (_scanBin) {
		case 1:
			result *= 3.0f;
			break;
		case 2:
			result *= 1.5f;
			break;
		case 3:
			//already ok in base value
			break;
		case 4:
			result *= 6.0f;
			break;
		case 9:
			result *= 9.0f;
			break;
		case 16:
			result *= 12.0f;
			break;
		case 25:
			result *= 15.0f;
			break;
		default:
			//scanning 1 assumed
			result *= 3.0f;
	}
	
	//return in micrometer rather than Angstroom
	result /= 10000.0;
	
	return result;
}

int WavelengthInformation::getScanFromSize (int _width) //, int _height)
{
	switch (_width) {
		case 1388:		//1388x1040
			//base size: scan factor 1.0
			return 1;
		case 694:		//694x520
			return 4;
		case 462:		//462x346
			return 9;
		case 346:		//346x260
			return 16;
		case 276:		//276x208
			return 25;
		
		case 2776:		//2776x2080
			return 2;
		case 4164:		//4164x3120
			return 3;
		
		default:
			return -1;
	}
}

QString WavelengthInformation::getComment() const
{
	return comment;
}


