/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CELLPROPERTIES_H
#define CELLPROPERTIES_H


#include "systeminfo.h"

enum TissueType {Blood=1, Stroma=2, Endothelium=3, Heart=4, Nerve=5, Endoderm=6, Mesoderm=7, TT_NONE=0};
enum CellGeneralType {	Myeloid=1, Lymphoid=2, HSC=3, Primitive=4, Primary=5, CellLine=6, Neuro=7, Glia=8, CGT_NONE=0};
// enum CellLineage 	 { 	Megakaryoid, Erythroid, Makrophage, Neutrophil, Eosinophil, Basophil, Mast, Dendritic,
// 						B, T, NK,
// 						LTR, STR, MPP, 
// 						Whole_Bone_Marrow, Osteoblast, Endothelial, 
// 						AFT024, _2018, BFC, OP9, PA6,
// 						CL_NONE};



/**
@author Bernhard

	This class contains properties of a cell, used as parameter during tracking mode
	to carry settings between GUI and memory
*/



struct CellProperties {

	/**
	 * constructs a default property object (nothing set to a useful value)
	 */
	CellProperties () :
		Z (-1.0f), XBackground (-1.0f), YBackground (-1.0f), tissueType (TT_NONE), 
		cellGeneralType (CGT_NONE), cellLineage (0), NonAdherent (false),
                FreeFloating (false), CellDiameter (0.0f), EndoMitosis (false), AddOn1 (0), Position ("")
	{
		Set_NonAdherent = false;
		Set_FreeFloating = false;
		Set_Addon = false;
		Set_Tissues = true;
		Set_Wavelengths = true;
		
		for (int i = 0; i <= MAX_WAVE_LENGTH; i++)
			WaveLength [i] = 0;
	}
	
	~CellProperties();
	
	
	//attributes: cf. TrackPoint definition
	
///due to micrometer positions since file version 15, short int is not enough
///from version 16 on: float values
/*	short int Z;
	short int XBackground;
	short int YBackground;*/
	float Z;
	float XBackground;
	float YBackground;
	
	///indicates whether the tissue, general type and lineage should be set
	///default = true
	bool Set_Tissues;
	
	///indicates whether the wavelength parameters should be set; default = true
	bool Set_Wavelengths;
	
	
	TissueType tissueType;
	CellGeneralType cellGeneralType;
	//CellLineage cellLineage;
	char cellLineage;
	
	///indicate whether the values should be set in Track::setProperties() in every case
	///default = false
	bool Set_NonAdherent;
	bool Set_FreeFloating;
	
	bool NonAdherent;
	bool FreeFloating;

        //from version 16: float
        float CellDiameter;
	
	bool EndoMitosis;
	
	short int WaveLength[MAX_WAVE_LENGTH+1];

	///indicates whether the AddOn attributes should be set
	bool Set_Addon;
	
	///additional attributes, cf. TrackPoint for details
        //BS 2010/08/10: changed to int due to file reading problems (long is 8-byte on 64-bit systems)
        int AddOn1;
	
	///the position in which the trackpoint was set
	QString Position;
};

#endif
