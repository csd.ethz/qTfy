/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
* @authors Bernhard Schauberger, Konstantin Azadov
* @modifiedby Stavroula Skylaki
* @datemodified 08/10/2015
*/

#include "tree.h"

// PROJECT
#include "qtfybackend/tttmanager.h"
#include "qtfybackend/tttpositionmanager.h"

// QT
#include <QList>
#include <QVector>

Tree::Tree(): /*QObject (0, 0),*/ BaseTrack (0)
{
	//TrackArray.setAutoDelete (true);
	
	NumTracks = 0;
	
	MaxTrackNumber = 0;
	
	filename = "";
	
	setMaxWavelength (MAX_WAVE_LENGTH);
        setFinished (false);

	this->m_cellLineageTree = new CellLineageTree();
}

Tree::~Tree()
{
	// also destroy the attached cell lineage tree
	delete this->m_cellLineageTree;
}

void Tree::insert (Track *_track, bool _sendSignal)
{
	// Delegate
	insert(QSharedPointer<Track>(_track), _sendSignal);
}

void Tree::insert (QSharedPointer<Track> _track, bool _sendSignal)
{
	//NOTE: shallow copy, the data is not copied, only the pointer

	if (! _track)
		return;

	_sendSignal = false;

	int trackNumber = _track->getNumber();

	// If this is a new track, increase track count
	if(!tracks.contains(trackNumber))
		NumTracks++;

	// Insert
	tracks.insert(trackNumber, _track);

	// Check if this is root
	if (trackNumber == 1)
		BaseTrack = _track.data();

	// Set this to _tracks tree
	_track->setTree(this);


	if ((uint)_track->getNumber() > MaxTrackNumber) {
		int fmpt = getMaxPossibleTracks();

		MaxTrackNumber = _track->getNumber();

		if (getMaxPossibleTracks() != fmpt)
			//sending the size update signal is only of worth, if the canvas size really changes
			//this is not the case, if the value of getMaxPossibleTracks() is not changed
			//note: the result of getMaxPossibleTracks() depends on the value of MaxTrackNumber
			_sendSignal = true;
	}
}

//bool Tree::deleteTrack (Track *_track)
//{
//	//note: deleting a track is different from deleting a stop reason!
//	
//	//children - there are two cases:
//	// - no children (including not tracked children) -> delete track immediately
//	// - living children                              -> ask for confirmation
//	
//	if (! _track)
//		return false;
//	
//	//if this is the base track, the tree has to be reset afterwards
//	if (_track == BaseTrack) {
//		delTrack (_track);
//		reset();
//	}
//	else
//		delTrack (_track);
//
//	return true;
//}
//
//bool Tree::insertComplexTrack (QSharedPointer<Track> _track, bool _sendSignal)
//{
//	if (! _track)
//		return true;
//	
//	insert (_track, _sendSignal);
//	
//	//insert this track's children as well (if there are none, the recursion stops)
//	bool success = insertComplexTrack (_track->getChildTrackShared (1), _sendSignal);
//	success &= insertComplexTrack (_track->getChildTrackShared (2), _sendSignal);
//	
//	return success;
//}
//
//QVector<Track*>* Tree::timePointTracks (int _timePoint) const
//{
//	if (tracks.isEmpty()) 
//		return NULL;
//	
//	QVector<Track*> tmp;
//
//	int j = 0;
//	bool anything = false;
//	
//	//for (int i = 0; i < (int)TrackArray.size(); i++) {
//	//	if (TrackArray [i]) 
//	//		if (TrackArray [i]->aliveAtTimePoint (_timePoint)) {
//	//			anything = true;
//	//			tmp->insert (j, TrackArray [i]);
//	//			j++;
//	//			if (j >= (int)tmp->size())
//	//				tmp->resize (j + 10);			//allocate more than necessary for faster inserting
//	//		}
//	//}
//
//	for(tracks_const_iterator it = tracks.constBegin(); it != tracks.constEnd(); ++it) {
//		Track* curTrack = it->data();
//		if (curTrack->aliveAtTimePoint (_timePoint)) {
//			anything = true;
//			tmp.append(curTrack);
//			j++;
//			if (j >= (int)tmp.size())
//				tmp.resize (j + 10);			//allocate more than necessary for faster inserting
//		}
//	}
//	
//	if (anything)
//		return &tmp;
//	else {
//		delete &tmp;
//		return 0;
//	}
//}
//
//QList<TrackPoint>* Tree::timePointTrackPoints (int _timePoint) const
//{
//	if (tracks.isEmpty())
//		return 0;
//	
//	QList<TrackPoint> *tmp = new QList<TrackPoint>();
//	
//	TrackPoint tmpTrackPoint;
//	bool anything = false;
//	
//	//for (int i = 0; i < (int)TrackArray.size(); i++) {
//	//	anything = true;
//	//	if (TrackArray [i]) {
//	//		tmpTrackPoint = TrackArray [i]->getTrackPoint (_timePoint);
//	//		if (tmpTrackPoint.TimePoint > -1) {
//	//			tmpTrackPoint.tmpCellNumber = TrackArray [i]->getNumber();
//	//			tmp->append (tmpTrackPoint);
//	//		}
//	//	}
//	//}
//
//	for(tracks_const_iterator it = tracks.constBegin(); it != tracks.constEnd(); ++it) {
//		Track* curTrack = it->data();
//		anything = true;
//		tmpTrackPoint = curTrack->getTrackPoint (_timePoint);
//		if (tmpTrackPoint.TimePoint > -1) {
//			tmpTrackPoint.tmpCellNumber = curTrack->getNumber();
//			tmp->append (tmpTrackPoint);
//		}
//	}
//	
//	if (anything)
//		return tmp;
//	else {
//		delete tmp;
//		return 0;
//	}
//}
//
//bool Tree::trackExists (int _timePoint) const
//{
//	//for (int i = 0; i < (int)TrackArray.size(); i++) 
//	//	if (TrackArray [i])
//	//		if (TrackArray [i]->trackPointExists (_timePoint))
//	//			return true;
//
//	for(tracks_const_iterator it = tracks.constBegin(); it != tracks.constEnd(); ++it) {
//		Track* curTrack = it->data();
//
//		if (curTrack->trackPointExists (_timePoint))
//			return true;
//	}
//	
//	return false;		//no track exists at that timepoint
//}

int Tree::getMaxPossibleTracks() const
{
        return getMaxPossibleTracks (MaxTrackNumber);
}

int Tree::getMaxPossibleTracks (int _maxTrackNumber)
{
        //return the next highest power of 2 minus 1, greater or equal to MaxTrackNumber
        //e.g. if the highest track number in the tree is 13, 15 is returned
        //but if MaxTrackNumber == 15, also 15 is returned

        //note: each change in here also affects the ttt file handling routines!

        for (int i = 0; ; i++)
                if ((int)pow (2.0, (double)i) > _maxTrackNumber)
                        return (int)(pow (2.0, (double)i)) - 1;

        return 0;
}

int Tree::recalcTrackCountInternal (Track *_track)
{
	if (! _track)
		return 0;
	else {
		int sum1 = recalcTrackCountInternal (_track->getChildTrack (1));
		int sum2 = recalcTrackCountInternal (_track->getChildTrack (2));
		
		//TrackArray.insert (_track->getNumber(), _track);
		
		return 1 + sum1 + sum2; 
	}
}

int Tree::recalculateNumberOfTracks()
{
	//count true number of tracks and also reorganize the TrackArray
	
	NumTracks = recalcTrackCountInternal (BaseTrack);	
	return NumTracks;
}

Track* Tree::getTrack (int _trackNumber) const
{
	if(tracks.contains(_trackNumber))
		return tracks[_trackNumber].data();

	// Not found
	return 0;
}

//recursive
void Tree::delTrack (Track *_track)
{
	if (! _track)
		return;
	
	//if the children do not exist, the control is immediately returned to this call
	delTrack (_track->getChildTrack (1));
	delTrack (_track->getChildTrack (2));
	
	if (_track->getMotherTrack())
		//delete the mother's reference to this track - very important!
		_track->getMotherTrack()->setChildTrack (_track->getChildIndex(), 0);
	
	
	//@ todo this is not yet correct, but its purpose is fulfilled hopefully ;-)
	if ((uint)_track->getNumber() == MaxTrackNumber)
		MaxTrackNumber--;
	
	////note: _track is also deleted on the heap, as autoDelete is enabled
	//TrackArray.remove (_track->getNumber());

	// note: _track is also deleted on the heap, because of QSharedPointer
	tracks.remove(_track->getNumber());
	
	NumTracks--;
}

void Tree::reset()
{
	if (tracks.isEmpty())
		//tree is still in reset state
		return;
	
	//delete all tracks cascadeously
	delTrack (BaseTrack);

	// OTODO:
	//emit tracksDeleted();
	
	//TrackArray.resize (17);
	//TrackArray.setAutoDelete (true);
	
	NumTracks = 0;
	
	MaxTrackNumber = 0;
	BaseTrack = 0;

	changeLog.clear();

	// New tree
	modified = true;
}

//QVector<Track*> Tree::coExistingTracks (int _timePoint, Track *_track) const
//{
//	
//	QVector<Track*> tmp;
//	
//	if (tracks.isEmpty()) 
//		return tmp;
//	if (! _track)
//		return tmp;
//	
//	tmp.resize (MaxTrackNumber);
//	int number = _track->getNumber();
//	
//	//for (int i = 0; i < (int)TrackArray.size(); i++) {
//	//	Track * tmpTrack = TrackArray [i];
//	//	if (tmpTrack) 
//	//		if ( (tmpTrack->getFirstTrace() <= _timePoint) &&
//	//		 	 (tmpTrack->getLastTrace()  >= _timePoint) ) {
//	//			
//	//			if (tmpTrack->getNumber() < number)
//	//				//insert ascending
//	//				tmp.insert (tmpTrack->getNumber(), tmpTrack);
//	//			else if (tmpTrack->getNumber() > number)
//	//				//insert descending
//	//				tmp.insert (MaxTrackNumber + number - tmpTrack->getNumber(), tmpTrack);
//	//		}
//	//}
//
//	for(tracks_const_iterator it = tracks.constBegin(); it != tracks.constEnd(); ++it) {
//		Track * tmpTrack = it->data();
//
//		if ( (tmpTrack->getFirstTrace() <= _timePoint) &&
//			(tmpTrack->getLastTrace()  >= _timePoint) ) {
//
//			if (tmpTrack->getNumber() < number)
//				//insert ascending
//				tmp.insert (tmpTrack->getNumber(), tmpTrack);
//			else if (tmpTrack->getNumber() > number)
//				//insert descending
//				tmp.insert (MaxTrackNumber + number - tmpTrack->getNumber(), tmpTrack);
//		}
//	}
//	
//	return tmp;	
//}
//
////recursive
//void Tree::assignNewNumber (Track *_startTrack, int _newNumber)
//{
//	if (! _startTrack)
//		return;
//		
//	_startTrack->setNumber (_newNumber);
//	
//	if ((uint)_newNumber > MaxTrackNumber)
//		MaxTrackNumber = _newNumber;
//	
//	//recursive loop through all children
//	//the new number for the child tracks is calculated by their parent number
//	//-> children of track x have numbers x*2 & x*2+1
//	
//	if (_startTrack->getChildTrack (1))
//		assignNewNumber (_startTrack->getChildTrack (1), _newNumber * 2);
//	if (_startTrack->getChildTrack (2))
//		assignNewNumber (_startTrack->getChildTrack (2), _newNumber * 2 + 1);
//}
//
//bool Tree::shiftTrackNumbers (Track *_startTrack, int _newNumber)
//{
//	if (! _startTrack)
//		return false;
//	
//	//assign new number to each track recursively
//	//the association between mother and child tracks is kept and no tracks are written to a new position in the array
//	assignNewNumber (_startTrack, _newNumber);
//	
//	//reorder the number |-> track hash
//	//---------------------------------
//	//1) copy current vector to backup
//	//Q3PtrVector<Track> TrackArrayBackup (TrackArray);
//	QHash<int, QSharedPointer<Track> > tracksBackup(tracks);
//	
//	//as the track vector is a hash which has the track number as key, the association must be updated
//	//=> resize the hash, either smaller or larger
//	
//	////clear old values of track array
//	//TrackArray.setAutoDelete (false);
//	//for (uint i = 0; i < TrackArray.size(); i++)
//	//	TrackArray.insert (i, 0);
//	////TrackArray.clear();
//	//TrackArray.setAutoDelete (true);
//	//
//	//TrackArray.resize (MaxTrackNumber + 10);
//	tracks.clear();
//	
//	//reorder track vector (begin with the highest number)
//	//method: copy each track to the array position its number indicates
//	//not anymore relevant: //as we start with the highest number, the new place is always free and no tracks are overwritten
//	//for (int i = TrackArrayBackup.size() - 1; i > 0; i--) {
//	//	Track *track = TrackArrayBackup.take (i);
//	//	if (track) {
//	//		TrackArray.insert (track->getNumber(), track);
//	//	}
//	//}
//
//	for(tracks_const_iterator it = tracksBackup.constBegin(); it != tracksBackup.constEnd(); ++it) {
//		QSharedPointer<Track> track = *it;
//		tracks.insert(track->getNumber(), track);
//	}
//	
//	return true;
//}
//

int Tree::getMaxTrackedTimePoint() const
{
	//loop through all tracks; the highest LastTrace is the result
	//note: if written without loop (values are updated with insert() and delete()), then
	//       the tree has to be notified about every change in a Track object (which can take
	//       place outside the Tree's methods!)
	
	int max = 0;
	
	//for (int i = 0; i < (int)TrackArray.size(); i++) {
	//for (uint i = 1; i <= MaxTrackNumber; i++) {
	//	if (TrackArray [i])
	//		if (TrackArray [i]->getLastTrace() > max)
	//			max = TrackArray [i]->getLastTrace();
	//}
	for(tracks_const_iterator it = tracks.constBegin(); it != tracks.constEnd(); ++it) {
		Track *curTrack = it->data();
		if (curTrack->getLastTrace() > max)
			max = curTrack->getLastTrace();
	}
	
	return max;
}

int Tree::getMinTrackedTimePoint () const
{
	//loop through all tracks; the lowest FirstTrace is the result
	//cf. note in getMaxTrackedTimePoint()
	
	// -------------Konstantin: comment the next 3 lines, because 'TTTManager::getInst().getBasePositionManager()' is always false and 'min' stay 0
// 	int min = 0;
// 	if (TTTManager::getInst().getBasePositionManager()) 
// 		min = TTTManager::getInst().getBasePositionManager()->getLastTimePoint();

	////for (int i = 0; i < (int)TrackArray.size(); i++) {
	//int min = TrackArray [1]->getFirstTrace(); // --------------- Konstantin -----------------
	//for (uint i = 2; i <= MaxTrackNumber; i++) { // ------------- Konstantin: i = 2 instead of i = 1 ----------------
	//	if (TrackArray [i])
	//		if (TrackArray [i]->getFirstTrace() < min)
	//			min = TrackArray [i]->getFirstTrace();
	//}

	int min = 0xFFFFFF;
	for(tracks_const_iterator it = tracks.constBegin(); it != tracks.constEnd(); ++it) {
		Track *curTrack = it->data();
		if (curTrack->getFirstTrace() < min)
			min = curTrack->getFirstTrace();
	}
	
	return min;
}

//// Disabled, because getTreeProperty in TreeWrapper has the same functionality
////--------------------- Konstantin ------------------------
////int Tree::getOptValue(const QString _name, const TTTPositionManager *_tttpm) const
////{
////	if ( _name == TREE_OPT_LIFETIME) {
////		return _tttpm->getFiles()->calcSeconds (getMinTrackedTimePoint(), getMaxTrackedTimePoint());
////	}
////	if ( _name == TREE_OPT_START_TP ) {
////		return _tttpm->getFiles()->calcSeconds (_tttpm->getFirstTimePoint(), getMinTrackedTimePoint());
////	}
////	if ( _name == TREE_OPT_STOP_TP ) {
////		return _tttpm->getFiles()->calcSeconds (_tttpm->getFirstTimePoint(), getMaxTrackedTimePoint());
////	}
////	if ( _name == TREE_OPT_NUMBER_OF_CELLS )
////		return getTrackCount();
////
////    if ( _name == TREE_OPT_NUMBER_OF_GENERATIONS )
////        return getNumberOfGenerations();
////
////	return 0;
////}
//
//QVector<Track*> Tree::getAllTracks (int _generations) const
//{
//	QVector<Track*> tmp (17); 
//	uint number = 0;
//	
//	////data starts with 1
//	//for (uint i = 1; i <= MaxTrackNumber; i++) {
//	//	if (TrackArray [i]) {
//	//		
//	//		if ((_generations == -1) ||
//	//			(childrenGenerationsCount (TrackArray [i]) == _generations)) {
//	//		
//	//			number = i; 
//	//			if (number >= tmp.size())
//	//				tmp.resize (number + 10);
//	//			tmp.replace (number, TrackArray [i]);
//	//		}
//	//	}
//	//}
//
//	for(tracks_const_iterator it = tracks.constBegin(); it != tracks.constEnd(); ++it) {
//		Track *curTrack = it->data();
//
//		if ((_generations == -1) ||
//			(childrenGenerationsCount (curTrack) == _generations)) {
//
//				number = curTrack->getNumber(); 
//				if (number >= tmp.size())
//					tmp.resize (number + 10);
//				tmp.replace (number, curTrack);
//		}
//	}
//	
//	return tmp;
//}

//recursive
int Tree::childrenGenerationsCount (Track *_track, int _count) const
{
	if (! _track)
		return 0;
	
	int depth1 = _count, depth2 = _count;
	
	if (_track->getChildTrack (1))
		depth1 = childrenGenerationsCount (_track->getChildTrack (1), _count + 1);
	if (_track->getChildTrack (2))
		depth2 = childrenGenerationsCount (_track->getChildTrack (2), _count + 1);
	
	return qMax (depth1, depth2);
}

int Tree::getNumberOfGenerations() const
{
	if (! BaseTrack)
		return 0;
	else
		return childrenGenerationsCount (BaseTrack);
}

//bool Tree::eraseStoredPositions()
//{
//	bool result = true;
//	
//	//for (uint i = 1; i <= MaxTrackNumber; i++) {
//	//	if (TrackArray [i]) {
//	//		result &= TrackArray [i]->eraseStoredPositions();
//	//		
//	//		if (! result)
//	//			break;
//	//	}
//	//}
//
//	for(tracks_const_iterator it = tracks.constBegin(); it != tracks.constEnd(); ++it) {
//		Track* curTrack = it->data();
//
//		result &= curTrack->eraseStoredPositions();
//
//		if (! result)
//			break;
//	}
//	
//	return result;
//}
//
//const QPair<QDate,QByteArray>* Tree::getLastChangelogEntry() const
//{
//	// Check if we have one
//	if(changeLog.size() == 0)
//		return 0;
//
//	// Get last one
//	QList<QPair<QDate,QByteArray> >::const_iterator it = changeLog.constEnd();
//	--it;
//
//	// Return pointer
//	return &(*it);
//}
//
QString Tree::getTreeName() const
{
	// Return filename without path
	QString ret = filename;
	int i = ret.lastIndexOf('/');
	if(i != -1) {
		ret = ret.mid(i + 1);
	}

	return ret;
}

QString Tree::getPositionIndex() const
{
	// Return position name
	return this->positionIndex;
}


QString Tree::getExperimentName() const
{
	// Return position name
	return this->experimentName;
}


QList<ITrackPoint*> Tree::getTrackPointsAtTimePoint( int _timePoint ) const
{
	QList<ITrackPoint*> ret;

	// Iterate over all tracks
	for(tracks_const_iterator it = tracks.constBegin(); it != tracks.constEnd(); ++it) {
		Track* curTrack = it->data();

		// Check if _timepoint exists at provided _timePoint
		if(TrackPoint* tp = curTrack->getTrackPointByTimePoint(_timePoint))
			ret.append(tp);
	}

	return ret;
}

QVector<ITrack*> Tree::getTracksInTree() const 
{	
	QVector<ITrack*> cells;

	for (int i = 1; i <= this->getMaxTrackNumber(); i++) 
	{
		ITrack *track = this->getTrackByNumber(i);
		if (! track)
			continue;			
		
		cells.append(track);
	}
	
	return cells;
}

//
//
//long Tree::getCellTypes() const
//{
//	if (! BaseTrack)
//		return CT_NOCELLTYPE;
//	else
//		return getCellTypesRecursive (BaseTrack, CT_NOCELLTYPE);
//}
//
//long Tree::getCellTypesRecursive (Track *_track, long _result) const
//{
//	if (! _track)
//		return _result;
//
//	for (long ct = CT_DIVIDINGTYPE; ct <= CT_NOLOSTCHILDRENTYPE; ct *= 2) {
//		CellType cellType = Track::longToCellType (ct);
//		if (_track->isOfCellType (cellType))
//			_result |= ct;
//	}
//
//	_result |= getCellTypesRecursive (_track->getChildTrack (1), _result);
//	_result |= getCellTypesRecursive (_track->getChildTrack (2), _result);
//
//	return _result;
//}


void Tree::setCellLineageTree(CellLineageTree* _clt)
{
	this->m_cellLineageTree =  _clt;
}

CellLineageTree* Tree::getCellLineageTree() const
{
	return this->m_cellLineageTree;
}



bool Tree::convertToCellLineageTree(CellLineageTree& clt) const
{
	// Check if clt is empty
	if(clt.getFrameIndexElements().size())
		return false;

	// Init clt
	QVector<QString> frameIndexElements;
	frameIndexElements.push_back("TimePoint");
	frameIndexElements.push_back("FieldOfView");
	frameIndexElements.push_back("ZIndex");
	frameIndexElements.push_back("ImagingChannel");
	if(!clt.setFrameIndexElements(frameIndexElements))
		return false;
	if(!clt.setUseZCoordinates(false))
		return false;
	if(TTTManager::getInst().USE_NEW_POSITIONS())
		clt.setCoordinatesType(CellLineageTree::GlobalMicroMeter);
	else
		clt.setCoordinatesType(CellLineageTree::LocalPixel);

	// Add tracking information about every cell and check if TTT quantification exists (i.e. any value that is not set to default)
	bool diameterSet = false;
	bool backgroundCoordinatesSet = false;
	bool cellTypeSet = false;
	bool adherenceSet = false;
	bool endoMitosisSet = false;
	int maxSetWl = -1;
	bool additionalAttributeSet = false;
	QList<Track*> tracksQueue;
	if(!BaseTrack)
		return false;
	tracksQueue.push_back(BaseTrack);
	while(!tracksQueue.isEmpty()) {
		// Get next track
		Track* curTrack = tracksQueue.takeFirst();
		int curTrackNumber = curTrack->getNumber();

		// Get parent track number and cell fate
		int parentTrackNumber = curTrackNumber / 2;
		if(parentTrackNumber <= 0)
			parentTrackNumber = -1;
		QString cellFateStr;
		switch(curTrack->getStopReason()) {
		case TS_DIVISION:
			cellFateStr = "Division";
			break;
		case TS_APOPTOSIS:
			cellFateStr = "Death";
			break;
		case TS_LOST:
			cellFateStr = "IdentityLost";
			break;
		case TS_NONE:
		default:
			cellFateStr = "";
		}

		// Add cell to tracking data
		if(!clt.insertCell(curTrackNumber, cellFateStr, parentTrackNumber))
			return false;

		// Add track points
		QVector<int> idx(clt.getFrameIndexElements().size());
		const QVector<TrackPoint*> allTrackPoints = curTrack->getAllTrackpoints();
		for(auto it = allTrackPoints.begin(); it != allTrackPoints.end(); ++it) {
			TrackPoint* curTp = *it;
			if(!curTp)
				continue;

			// Update FrameIndex
			idx[0] = curTp->getTimePoint();
			idx[1] = curTp->getPositionNumber();
			idx[2] = CLT_DEFAULT_ZINDEX;	// ZIndex
			idx[3] = CLT_DEFAULT_WAVELENGTH;	// Wavelength

			// Set tracking data of track point
			if(!clt.setTrackingDataPoint(curTrackNumber, idx, curTp->getX(), curTp->getY()))
				return false;

			// Check available qualitative TTT quantifications
			diameterSet = diameterSet || curTp->CellDiameter != 25.0f;
			backgroundCoordinatesSet = backgroundCoordinatesSet || (curTp->XBackground != -1.0f || curTp->YBackground != -1.0f);
			cellTypeSet = cellTypeSet || (curTp->tissueType != 0 || curTp->cellGeneralType != 0 || curTp->cellLineage != 0);
			adherenceSet = adherenceSet || (curTp->NonAdherent != 0 || curTp->FreeFloating != 0);
			endoMitosisSet = endoMitosisSet || (curTp->EndoMitosis != 0);
			for(int wl = 0; wl <= MAX_WAVE_LENGTH; ++wl) {
				if(curTp->WaveLength[wl] != 0)
					maxSetWl = std::max(maxSetWl, wl);
			}
			additionalAttributeSet = additionalAttributeSet || (curTp->AddOn1 != 0);
		}

		// Enqueue child tracks
		if(curTrack->getChild1())
			tracksQueue.push_back(curTrack->getChild1());
		if(curTrack->getChild2())
			tracksQueue.push_back(curTrack->getChild2());
	}

	// Create TTT quantification chunk, if anything has been annotated
	// get user
	QString treeName = this->getTreeName();
	QString user = treeName.remove(this->getPositionIndex()).remove(".ttt").remove("-").remove(QRegExp("\\d*")); // TODO: get this from the tree name //UserInfo::getInst().getUsersign(false, "", true);
	int qIndexTTT = -1;
	if(diameterSet || backgroundCoordinatesSet || cellTypeSet || adherenceSet || endoMitosisSet || maxSetWl >= 0 || additionalAttributeSet) {
		qIndexTTT = clt.insertQuantificationChunk();
		if(qIndexTTT < 0)
			return false;
		clt.setQuantificationName(qIndexTTT, "TTTQuantification");
		clt.setQuantificationSoftware(qIndexTTT, "TTT");
		clt.setQuantificationRemarks(qIndexTTT, "Qualitative annotation of cell lineage, adherence status and fluorescence signals manually added in TTT.");
		if(!user.isEmpty())
			clt.setQuantificationUserName(qIndexTTT, user);
		if(diameterSet)
			clt.addQuantificationColumn(qIndexTTT, "Diameter", CellLineageTree::Double, QVariant(0.0));
		if(backgroundCoordinatesSet) {
			clt.addQuantificationColumn(qIndexTTT, "XBackground", CellLineageTree::Double, QVariant(0.0));
			clt.addQuantificationColumn(qIndexTTT, "YBackground", CellLineageTree::Double, QVariant(0.0));
		}
		if(cellTypeSet) {
			clt.addQuantificationColumn(qIndexTTT, "TissueType", CellLineageTree::Int32, QVariant(0));
			clt.addQuantificationColumn(qIndexTTT, "GeneralType", CellLineageTree::Int32, QVariant(0));
			clt.addQuantificationColumn(qIndexTTT, "Lineage", CellLineageTree::Int32, QVariant(0));
		}
		if(adherenceSet) {
			clt.addQuantificationColumn(qIndexTTT, "NonAdherent", CellLineageTree::Bool, QVariant(false));
			clt.addQuantificationColumn(qIndexTTT, "FreeFloating", CellLineageTree::Bool, QVariant(false));
		}
		if(endoMitosisSet) {
			clt.addQuantificationColumn(qIndexTTT, "Endomitosis", CellLineageTree::Bool, QVariant(false));
		}
		if(maxSetWl >= 0) {
			for(int wl = 0; wl < maxSetWl; ++wl) {
				QString colName = QString("ImagingChannel_%1").arg(wl, 2, 10, QChar('0'));
				clt.addQuantificationColumn(qIndexTTT, colName, CellLineageTree::Bool, QVariant(false));
			}
		}
		if(additionalAttributeSet) {
			clt.addQuantificationColumn(qIndexTTT, "AdditionalAttribute", CellLineageTree::Int32, QVariant(0));
		}

		// Set quantification data
		tracksQueue.clear();
		tracksQueue.push_back(BaseTrack);
		while(!tracksQueue.isEmpty()) {
			// Get next track
			Track* curTrack = tracksQueue.takeFirst();
			int curTrackNumber = curTrack->getNumber();

			// Add cell to quantification
			if(!clt.insertQuantificationCell(qIndexTTT, curTrackNumber))
				return false;

			// Add track points
			QVector<int> idx(clt.getFrameIndexElements().size());
			const QVector<TrackPoint*> allTrackPoints = curTrack->getAllTrackpoints();
			for(auto it = allTrackPoints.begin(); it != allTrackPoints.end(); ++it) {
				TrackPoint* curTp = *it;
				if(!curTp)
					continue;

				// Update FrameIndex
				idx[0] = curTp->getTimePoint();
				idx[1] = curTp->getPositionNumber();
				idx[2] = 1;	// ZIndex
				idx[3] = 0;	// Wavelength

				// Generate row vector with TTT quantifications
				QVector<QVariant> qRow;
				if(diameterSet)
					qRow.push_back(double(curTp->CellDiameter));
				if(backgroundCoordinatesSet) {
					qRow.push_back(double(curTp->XBackground));
					qRow.push_back(double(curTp->YBackground));
				}
				if(cellTypeSet) {
					qRow.push_back(qint32(curTp->tissueType));
					qRow.push_back(qint32(curTp->cellGeneralType));
					qRow.push_back(qint32(curTp->cellLineage));
				}
				if(adherenceSet) {
					qRow.push_back(bool(curTp->NonAdherent));
					qRow.push_back(bool(curTp->FreeFloating));
				}
				if(endoMitosisSet) {
					qRow.push_back(bool(curTp->EndoMitosis));
				}
				if(maxSetWl >= 0) {
					for(int wl = 0; wl < maxSetWl; ++wl) {
						qRow.push_back(bool(curTp->WaveLength[wl] != 0));
					}
				}
				if(additionalAttributeSet) {
					qRow.push_back(qint32(curTp->AddOn1));
				}
				if(!clt.setQuantificationDataPoint(qIndexTTT, curTrackNumber, idx, qRow))
					return false;
			}

			// Enqueue child tracks
			if(curTrack->getChild1())
				tracksQueue.push_back(curTrack->getChild1());
			if(curTrack->getChild2())
				tracksQueue.push_back(curTrack->getChild2());
		}

	}

	// Create quantitative quantification chunks, if data exists
	for(int iQuantChunk = 0; iQuantChunk < quantificationChunkDefinitions.size(); ++iQuantChunk) {
		// Create new chunk
		int quantChunkId = clt.insertQuantificationChunk();
		if(quantChunkId < 0)
			return false;

		// Set meta information
		clt.setQuantificationMetaInformation(quantChunkId, quantificationChunkDefinitions[iQuantChunk].metaInformation);

		// Set column specification
		clt.setQuantificationColumns(quantChunkId, quantificationChunkDefinitions[iQuantChunk].dataColumnNames, quantificationChunkDefinitions[iQuantChunk].dataColumnTypes, quantificationChunkDefinitions[iQuantChunk].dataColumnDescriptions);

		// Add quantification data
		QList<Track*> tracksStack;
		tracksStack.push_back(getRootNode());
		while(tracksStack.size()) {
			// Take next track
			Track* curTrack = tracksStack.takeFirst();
			if(!curTrack)
				continue;

			// Add cell to quantification
			if(!clt.insertQuantificationCell(quantChunkId, curTrack->getTrackNumber()))
				return false;

			// Iterate over track points
			const QVector<TrackPoint*>& curTrackPoints = curTrack->getAllTrackpoints();
			for(int iTrackPoint = 0; iTrackPoint < curTrackPoints.size(); ++iTrackPoint) {
				TrackPoint* curTrackPoint = curTrackPoints[iTrackPoint];
				if(!curTrackPoint)
					continue;

				// Add quantification of track point if available
				// TODO!!!
				//if(curTrackPoint->additionalQuantificationData.size() > iQuantChunk) {
				//	// Get data and fill missing columns
				//	QVector<QVariant> dataPoint = curTrackPoint->additionalQuantificationData[iQuantChunk];

				//	// Make sure it has the right number of columns and fill null values
				//	dataPoint.resize(quantificationChunkDefinitions[iQuantChunk].dataColumnTypes.size());
				//	for(int iCol = 0; iCol < dataPoint.size(); ++iCol) {
				//		if(dataPoint[iCol].isNull())
				//			dataPoint[iCol] = defaultValue(quantificationChunkDefinitions[iQuantChunk].dataColumnTypes[iCol]);
				//	}

				//	// Set data
				//	QVector<int> idx;
				//	idx.resize(4);
				//	idx[0] = curTrackPoint->TimePoint;
				//	idx[1] = curTrackPoint->Position.toInt();
				//	idx[2] = Tree::CLT_DEFAULT_ZINDEX;	
				//	idx[3] = Tree::CLT_DEFAULT_WAVELENGTH;
				//	if(!clt.setQuantificationDataPoint(quantChunkId, curTrack->getTrackNumber(), idx, dataPoint))
				//		return false;
				//}
			}

			// Add child tracks to tracksStack
			if(curTrack->getChild1())
				tracksStack.push_back(curTrack->getChild1());
			if(curTrack->getChild2())
				tracksStack.push_back(curTrack->getChild2());
		}
	}



	return true;
}
