/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SYSTEMINFO_H
#define SYSTEMINFO_H

// QT
#include <QFile>
#include <QDir>
#include <QTextStream>
#include <iostream>
#include <QVector>

#include <QtDebug>
#include <errno.h>

//// If not in debugmode, disable qt debug output (qDebug(blabla) and qDebug()<<blabla)
//#ifndef DEBUGMODE
//#define QT_NO_DEBUG_OUTPUT
//#endif
// 05.01.11 OH: Not disabled as debug output is now logged


/////whether ttt is in debug mode (modifies some things: - report output, )
//const bool DEBUG_MODE = false;
// <- use symbol DEBUGMODE instead

///whether the new ttt file folder system should be used
///@WARNING this setting overrides the user setting, so delete the user setting!
const bool USE_NEW_TTTFILE_FOLDER_SYSTEM = true;

///marks a file to be located in an old folder
const QString OLD_FOLDER_TTTFILE_MARKER = "@";

///the maximum possible wavelength (not depending on the index calculation scheme!)
const int MAX_WAVE_LENGTH = 99;

// the maximum possible z-index (possible values range from 0 to this value, so there are MAX_ZINDEX+1 possible values)
const int MAX_ZINDEX = 999;

///the token that marks a folder to be a position (usually "_p")
const QString POSITION_MARKER = "_p";

///the token that separates the colony number (usually a hyphen)
const QString COLONY_MARKER = "-";

///the default folder for the NAS drive (added to the current user's home directory)
//const QString DEFAULT_NAS = "ISF/Daten/TTT/TTTexport/";//"ISF/Daten/ISF-H/TTTexport/";
#ifdef Q_WS_WIN
        const QString DEFAULT_NAS = "S:/group/schroeder/Timelapse/";
#else
   		#ifdef Q_OS_LINUX
            	const QString DEFAULT_NAS = "TTTexport/";
    	#endif
#endif

///the folder for the ttt files (still divided into many subfolders)
const QString TTTFILES_FOLDER = "TTTfiles";

//------------------ Konstantin-------------------
///the folder for the ttt log files (for TTTStats)
const QString TTTLOGFILES_FOLDER = "TTTlogfiles";

///the folder with the available experiments for the original image files (for TTTStats)
const QString TTTEXPERIMENTSFILES_FOLDER = "Timelapsedata";


//the minimum amount of physical memory (without swap) that has to be kept free
///specified in Megabyte (this value has been found by try-and-error)
const unsigned int MINIMUM_RAM = 350;	//Megabytes

/**
@author Bernhard
	
	A class which provides information about the system, like free RAM, the current
	user's home directory or the execute() method which executes bash commands.
	It also holds the central message reporting system via the standard output streams
	or a user defined QTextStream.
*/


class SystemInfo {

public:
	///the default output stream for the method report()
        static QTextStream out;
	
	///the output file for the method report()
	static QFile file;
	
	
	/**
	 * constructs a default object (not necessary, since all methods are static)
	 */
	SystemInfo();
	
	~SystemInfo();
	
	/**
	 * returns the size of the currently free RAM memory, optionally including the size of the free swap partition
	 * Not supported: (@param _withSwap whether the free swap space should be added)
	 * @param _unit specifies whether in Byte ('b'), KB ('k'), MB ('m'), GB ('g')
	 * @return the sum of free RAM in the desired unit
	 */
	static unsigned int getFreeRAM ( /*bool _withSwap = false,*/ char _unit = 'k');

	/**
	 * checks if available physical or virtual memory are below MINIMUM_RAM
	 * @return true if short of memory
	 */
	static bool shortOfMemory();

	/**
	 * executes the provided command as shell command
	 * @WARNING: never execute a command that depends on user interaction; if this cannot be avoided, use options that override interactive commands
	 * @param _command the command to be executed (e.g. "ls")
	 * @param _output reference parameter, takes the output of the command
	 * @return true, if the command could be executed, otherwise false
	 */
	static bool execute (QString _command, QString &_output);
	
	/**
	 * returns the home directory path of the current user (final slash is always included)
	 * example: /home/administrator/
	 * @return the home directory as absolute path with slash at the end
	 */
	static QString homeDirectory();
	
	/* *
	 * prints the provided message to the provided text stream
	 * @param _text the text to be printed
	 * @param _ts the (assigned and open) text stream, to which the _text is written
	 */
	static void report (QString _text, QTextStream _ts);
	
	/**
	 * prints the provided message to the file with the provided _filename
	 * @param _text the text to be printed
	 * @param _filename the filename of the reporter file (log file)
	 * @param _append whether the message should be appended (true, default) or the file newly created
	 */
	static void report (QString _text, QString _filename, bool _append = true);
	
	/**
	 * assign the output to the provided filename
	 * @param _filename the filename of the file to which all report strings should be printed
	 */
	static void setReportStream (QString _filename);
	
	/**
	 * unbind the output stream from the file (now output is to standard streams)
	 */
	static void unsetReportStream();
	
	/**
	 * lists all files which correspond to _filter and _nameFilter in _directory
	 * @param _directory the query directory (in which should be searched)
	 * @param _filter the filter (e.g. QDir::Files)
	 * @param _nameFilter the filename pattern (e.g. "*.ttt")
	 * @param _recursive whether directories should be traversed recursively
	 * @return a QValueVector of strings with the filenames, not sorted
	 */
	static QVector<QString> listFiles (QString _directory, QDir::Filters _filter, QString _nameFilter = QString::null, bool _recursive = true);
	
	/**
	 * returns the default path of the mounted network (Network Attached Storage)
	 * @return the absolute path of the default NAS
	 */
	static QString defaultNAS();
	
	/**
	 * Returns the absolute path of the ttt file folder, depending on the provided base name
	 * If the folder does not exist and _createFolder is true, then the folder is created
	 * @param _fileName the basename of the ttt file (e.g. 051104PH2_p000)
	 * @param _createFolder whether a non-existent folder should be created automatically (default = false)
	 * @param _useAlternativePath whether the alternative path (user choice switched) should be returned (default = false)
	 * @return the absolute path of the queried folder (with a / at the end)
	 */
	static QString getTTTFileFolderFromBaseName (const QString &_baseName, bool _createFolder = false, bool _useAlternativePath = false);
	
	/**
	 * Checks whether the directory exists, and if not creates it (on demand)
	 * @param _dirName the ABSOLUTE(!) path of the directory
	 * @param _create whether the provided directory should be created, if it does not exist
	 * @param _recursive whether directories should be created recursively (a path with depth > 1) (default = false)
	 * @return _create == false => whether the directory exists; _create == true => whether the creation was successful
	 */
	static bool checkNcreateDirectory (const QString &_dirName, bool _create = true, bool _recursive = false);
	
	/**
	 * Checks whether the directory exists, and if not creates it (on demand)
	 * The path of the directory is calculated via _currentDir.absPath() + _dirName
	 * @param _currentDir the current directory in which a subdirectory is queried
	 * @param _dirName the absolute path of the directory
	 * @param _create whether the provided directory should be created, if it does not exist
	 * @param _recursive whether directories should be created recursively (a path with depth > 1) (default = false)
	 * @return _create == false => whether the directory exists; _create == true => whether the creation was successful
	 */
	static bool checkNcreateDirectory (QDir &_currentDir, const QString &_dirName, bool _create = true, bool _recursive = false);
	
	/**
	 * @return the path of the ttt executable (is not trapped by static links)
	 */
	static const QString getExecutablePath();

    /**
     * @return the current system time
     */
    static QDateTime getSystemTime();

    /**
     * @param _format the format (default = "yyyy/MM/dd", thus only the date)
     * @return the current system date/time as string
     */
    static QString getSystemDateAsString (const QString &_format = "yyyy/MM/dd");

	/**
	 * @return number of physical cores or -1 if not detectable
	 */
	static int getNumPhysicalCores();
};

#endif
