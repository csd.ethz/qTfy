/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "cellwrapper.h"

#include <QVariant>
#include <QStringList>
#include "qtfybackend/tools.h"
#include "qtfydata/experiment.h"

/**
 * The contructor. Takes a pointer to a ttt track.
 * This class has the responsibility to delete the track.
 */
CellWrapper::CellWrapper(Track &_track, TreeWrapper &_tree) :
	track(_track), tree(_tree)
{
	// Get the correct PostionManager
	QString index = Tools::getPositionIndex (track.getTree()->getFilename());
	this->tttpm = TTTManager::getInst().getPositionManager (index);
}

/**
  * Destructor.
  */
CellWrapper::~CellWrapper()
{
}

QVariant CellWrapper::getTrackProperty(QString _propName) const 
{
	if(_propName == ITrack::TRACK_LIFETIME) {
		// Get cell lifetime from the track
		float lifetime = track.getOptValue(TRACK_OPT_LIFETIME, tttpm);
		if((track.getGeneration() == 0) && Experiment::isUsePreincubationTime()) 
			lifetime += tree.experiment->getPreincubationTimeInSeconds();
		return lifetime;
	} 
	else if(_propName == ITrack::TRACK_AVG_SPEED) {
		return track.getOptValue(TRACK_OPT_AVERAGE_SPEED, tttpm);
	}
	else if(_propName == ITrack::TRACK_DISTANCE) {
		return track.getOptValue(TRACK_OPT_ENTIRE_DISTANCE, tttpm);
	}
	else if(_propName == ITrack::TRACK_GENERATION) {
		// Get the generation of the track
		return track.getGeneration();
	}
	else if(_propName == ITrack::TRACK_TYPE) {
		// Get the cell type
		return track.getCellType();
	}
	else if(_propName == ITrack::TRACK_PREINCUBATION_TIME) {
		// Get the preincubation time
		if(track.getGeneration() != 0) return 0;
		return tree.experiment->getPreincubationTimeInSeconds();
	}
	else {
		// Unknown cell property
		qDebug() << "Unknown TrackProperty: " << _propName;
		return QVariant();
	}
}

QVector<QString> CellWrapper::getAvailableProperties() const
{
	return CellWrapper::alwaysAvailableAttributes;
}

// Init the attributes with initAvailableAttributes!
QVector<QString> CellWrapper::alwaysAvailableAttributes = initAvailableAttributes();

// Used to init alwaysAvailableAttributes
QVector<QString> CellWrapper::initAvailableAttributes()
{
	QVector<QString> properties;
	properties.append(ITrack::TRACK_LIFETIME);
	properties.append(ITrack::TRACK_AVG_SPEED);
	properties.append(ITrack::TRACK_DISTANCE);
	properties.append(ITrack::TRACK_GENERATION);
	properties.append(ITrack::TRACK_TYPE);
	return properties;
}