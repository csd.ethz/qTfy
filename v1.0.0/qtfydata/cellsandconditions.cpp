/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @authors Bernhard Schauberger
 * @modifiedby Stavroula Skylaki
 * @datemodified 07/10/2015
 */


// PROJECT
#include "cellsandconditions.h"

CNC_CellType::CNC_CellType()
{
	reset();
}

void CNC_CellType::reset()
{
	PrimaryCell = false;
	CellName = "";
	Species = "";
	Sex = "";
	Organ = "";
	Age = 0;
	Purification = "";
	Comment = "";
}

const QString CNC_CellType::toString() const
{
	QString result = "";
	
	result += PrimaryCell + "\t";
	result += CellName + "\t";
	result += Species + "\t";
	result += Sex + "\t";
	result += Organ + "\t";
	result += QString ("%1").arg (Age) + "\t";
	result += Purification + "\t";
	result += Comment + "\n";
	
	return result;
}


CellsAndConditions::CellsAndConditions()
	: 	NumberOfCellTypes (0), Temperature (0), CO2_Percentage (0), Flask (""),	Medium (""),
		Incubators (""), Serum (""), SerumPercentage (0), MediumAdditions (""), ReflectedLightSource (""),
		HalogenVoltage (0), NeutralFilter (0), IlluminationComment (""), AgeFluorescentLamp (0), HoursBeforeMovieStart (0), 
		cellTypeCount (0)

{
}


void CellsAndConditions::addCellType (const CNC_CellType &_cellType)
{

	//if ((int)cellTypeCount >= cellTypes.size()) {
	//	//note: times 2 is an overkill, but given the small amount of cell types it is ok
	//	cellTypes.resize (cellTypeCount * 2);
	//}
	//cellTypes [cellTypeCount] = (_cellType);
	cellTypes.append(_cellType);
	cellTypeCount++;
}

CellsAndConditions& CellsAndConditions::operator= (const CellsAndConditions& _cacs)
{
	NumberOfCellTypes = _cacs.NumberOfCellTypes;
	Temperature = _cacs.Temperature;
	CO2_Percentage = _cacs.CO2_Percentage;
	Flask =_cacs. Flask;
	Medium = _cacs.Medium;
	Incubators = _cacs.Incubators;
	Serum = _cacs.Serum;
	SerumPercentage = _cacs.SerumPercentage;
	MediumAdditions = _cacs.MediumAdditions;
	ReflectedLightSource = _cacs.ReflectedLightSource;
	HalogenVoltage = _cacs.HalogenVoltage;
	NeutralFilter = _cacs.NeutralFilter;
	IlluminationComment = _cacs.IlluminationComment;
	AgeFluorescentLamp = _cacs.AgeFluorescentLamp;
	HoursBeforeMovieStart = _cacs.HoursBeforeMovieStart;
	
	cellTypeCount = 0;
	for (uint i = 0; i < _cacs.cellTypeCount; i++) {
		addCellType (_cacs.cellTypes [i]);
	}
	
	return *this;
}

const QString CellsAndConditions::toString() const
{
	QString result = "";
	
	result += QString ("%1").arg (NumberOfCellTypes) + "\t";
	result += QString ("%1").arg (Temperature) + "\t";
	result += QString ("%1").arg (CO2_Percentage) + "\t";
	result += Flask + "\t";
	result += Medium + "\t";
	result += Incubators + "\t";
	result += Serum + "\t";
	result += QString ("%1").arg (SerumPercentage) + "\t";
	result += MediumAdditions + "\t";
	result += ReflectedLightSource + "\t";
	result += QString ("%1").arg (HalogenVoltage) + "\t";
	result += QString ("%1").arg (NeutralFilter) + "\t";
	result += IlluminationComment + "\t";
	result += QString ("%1").arg (AgeFluorescentLamp) + "\t";
	result += QString ("%1").arg (HoursBeforeMovieStart) + "\t";
	
	result += QString ("%1").arg (cellTypeCount) + "\n";
	
	for (uint i = 0; i < cellTypeCount; i++) {
		result += cellTypes [i].toString() + "\n";
	}
	
	return result;
}

