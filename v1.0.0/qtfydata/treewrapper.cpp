/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "treewrapper.h"

#include <QVariant>
#include <QStringList>
#include "qtfybackend/tools.h"
#include "qtfybackend/tttmanager.h"
#include "qtfybackend/tttpositionmanager.h"
#include "qtfydata/experiment.h"
#include "qtfydata/treewrapper.h"
#include "qtfydata/cellwrapper.h"

/**
 * The contructor. Takes a pointer to a ttt track.
 * This class has the responsibility to delete the tree.
 */
TreeWrapper::TreeWrapper(Tree &_tree) :
	tree(_tree) 
{
	// Get the correct PostionManager
	QString index = Tools::getPositionIndex(_tree.getFilename());
	this->tttpm = TTTManager::getInst().getPositionManager (index);
	this->experiment = TTTManager::getInst().getExperiment(tree.getExperimentname());
}

/**
  * Destructor. Deletes the internal ttt track
  */
TreeWrapper::~TreeWrapper()
{
	// Delete the tree
	delete &tree;

	// Delete wrappedTracks
	CellWrapper* track;
	foreach(track, wrappedTracks) {
		delete track;
	}
}

/**
  * Returns the desired property. The return type is wrapped by
  * QVariant, but for one property it is always the same type.
  * Internaly it uses the methods of tree and positonManager.
  */
QVariant TreeWrapper::getTreeProperty(QString _propName) const 
{
	if ( _propName == TREE_OPT_LIFETIME) {
		int lifetime = tttpm->getFiles()->calcSeconds (tree.getMinTrackedTimePoint(), tree.getMaxTrackedTimePoint());
		if(Experiment::isUsePreincubationTime()) lifetime += experiment->getPreincubationTimeInSeconds();
		return lifetime;
	}
	else if (_propName == TREE_OPT_START_TP) {
		return tttpm->getFiles()->calcSeconds (tttpm->getFirstTimePoint(), tree.getMinTrackedTimePoint());
	}
	else if ( _propName == TREE_OPT_STOP_TP ) {
		return tttpm->getFiles()->calcSeconds (tttpm->getFirstTimePoint(), tree.getMaxTrackedTimePoint());
	}
	else if ( _propName == TREE_OPT_NUMBER_OF_CELLS ) {
		return tree.getTrackCount();
	}
	else if ( _propName == TREE_OPT_NUMBER_OF_GENERATIONS ) {
        return tree.getNumberOfGenerations();
	}
	else if ( _propName == TREE_EXPERIMENT_NAME ) {
		return tree.getExperimentname();
	} 
	else if (_propName == ITrack::TRACK_PREINCUBATION_TIME) {
		// Get the preincubation time
		return this->experiment->getPreincubationTimeInSeconds();
	}
	else {
		// Unknown cell property
		QString q = QString("Unknown TrackProperty: ") + _propName;
		qDebug(q.toStdString().c_str());
		return QVariant();
	}
}

/*
 * @return a list of valid parameters of getTreeProperty
 */
QVector<QString> TreeWrapper::getAvailableProperties() const
{
	QVector<QString> properties;
	properties.append(TREE_OPT_LIFETIME);
	properties.append(TREE_OPT_START_TP);
	properties.append(TREE_OPT_STOP_TP);
	properties.append(TREE_OPT_NUMBER_OF_CELLS);
	properties.append(TREE_OPT_NUMBER_OF_GENERATIONS);
	properties.append(TREE_EXPERIMENT_NAME);
	return properties;
}

/**
 * Manages the wrappedTrack cache. Constructs
 * if not already done a new instance of CellWrapper.
 *
 * @param _num number of the requested track
 * @return wrapped version of track number _num or 0 if not existing
 */
ITrack* TreeWrapper::getWrappedTrack(int _num) const
{
	CellWrapper *wrapper = wrappedTracks.value(_num);

	// If not existing create a new wrapped version and save it in the cache
	if(! wrapper )
	{
		Track *track = tree.getTrackByNumber(_num);

		// Return 0 if the track does not exist
		if( ! track ) 
			return 0;

		// Else create a wrapper, save and return it!
		TreeWrapper* tw = (TreeWrapper*)this;
		wrapper = new CellWrapper(*track, *tw);
		wrappedTracks.insert(_num, wrapper);
	}

	return wrapper;
}
