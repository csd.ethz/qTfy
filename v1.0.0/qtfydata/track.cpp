/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
* @authors Bernhard Schauberger, Konstantin Azadov
* @modifiedby Stavroula Skylaki
* @datemodified 08/10/2015
*/

// PROJECT
#include "track.h"

#include "qtfybackend/tttpositionmanager.h" 
#include "qtfybackend/mathfunctions.h"
#include "qtfybackend/tools.h" 
#include "qtfydata/tatinformation.h" 
#include "tree.h"


Track::Track (Track *_motherTrack, int _childIndex, int _maxTimepoint, int _firstTrace)
{
	
	if (_firstTrace > -1)
		FirstTrace = _firstTrace;
	else
		FirstTrace = _maxTimepoint;
	MaxTimePoint = _maxTimepoint;
	LastTrace = -1;
	StopReason = TS_NONE;
	MarkCount = 0;
	// background_tracked = false;
	tree = 0;
	
	MotherTrack = _motherTrack;
	if (_motherTrack) {
		MotherTrack->setChildTrack (_childIndex, this);
		MotherTrack->setStopReason(TS_DIVISION);
	}
	ChildTrack1 = 0;
	ChildTrack2 = 0;
	ChildIndex = _childIndex;
	
	comment.set("");
	SecondsFromBegin = 0;
	/*clusterID = -1;*/
}

Track::~Track()
{
	//do not delete ChildTrack1 and ChildTrack2, as they may still be referenced at other locations!
}
//
//bool Track::addTrackPoint (const TrackPoint &_tp, bool _replaceIfExists)
//{
//	
//	//if a track at the provided timepoint already exists, no new one is added, the former is altered
//	int index = calcIndex (_tp.TimePoint);
//	bool exists = false;
//	TrackPoint *tp = 0;
//        if ((index >= 0) & (index < (int)Positions.size())) {
//		tp = Positions [index];
//		exists = (tp != 0);
//	}
//	
//	if (! exists) {
//		//add new trackpoint
//			
//		if (index >= (int)Positions.size())
//			Positions.resize ((int)((float)index * 1.5));
//		else if (index < 0)
//			//this problem occurs because the first trace is not yet set correctly; but after setting it, the index would be 0 nonetheless
//			index = 0;
//		
//		if (_tp.TimePoint > LastTrace) 
//			LastTrace = _tp.TimePoint;
//
//		if (_tp.TimePoint < FirstTrace /*|| MarkCount == 0*/) { 
//			updateFirstTrace (_tp.TimePoint);
//
//			// Make sure index is correct
//			index = calcIndex(_tp.TimePoint);
//			if(index < 0 || index >= Positions.size())
//				return false;
//
//			SecondsFromBegin = 0;
//		}
//		
//		//note: must be after updateFirstTrace() cos otherwise the first trackpoint is doubled (if the first trace is updated and index = FirstTrace)
//		TrackPoint* tmp = new TrackPoint (_tp);
//		tmp->track = this;
//		Positions.insert (index, tmp);
//		
//		MarkCount++;
//		
//	}
//	else {			
//		// Forbidden?
//		if(!_replaceIfExists)
//			return false;
//
//		//modify existing
//		*tp = _tp;
//		tp->track = this;
//	}
//	
///*	SystemInfo::report ("");
//	for (int i = 0; i < Positions.size(); i++)
//		if (Positions [i])
//			SystemInfo::report (QString ("%1").arg (Positions [i]->TimePoint));*/
//	
//	return true;
//}
//
void Track::setTrackPoint (TrackPoint &_tp, int _timepoint, float _x, float _y, float _diameter, bool _setBackground, bool _nonAdherent, bool _freeFloating, QString _position)
{
	if (_timepoint > 0)
		_tp.TimePoint = _timepoint;
	if (! _setBackground) {
		_tp.X = _x;
		_tp.Y = _y;
		//radius is only set for normal position
                if (_diameter > 0.0f)
                        _tp.CellDiameter = _diameter;
	}
	else {
		_tp.XBackground = _x;
		_tp.YBackground = _y;
		//background_tracked = true;
	}
	if (_nonAdherent)
		_tp.NonAdherent = 1;
	else
		_tp.NonAdherent = 0;
	if (_freeFloating)
		_tp.FreeFloating = 1;
	else
		_tp.FreeFloating = 0;
	
	_tp.Position = _position;
}

TrackPoint* Track::addMark (int _timepoint, float _x, float _y, float _diameter, bool _setBackground, short int _mouseButtons, QString _position)
{
	//check if adding mark at this timepoint is possible
	//NOT possible, if one of the following holds
	// - _timepoint < FirstTrace (this is the minimum possible!) & MotherTrack != 0
	//    (reverse tracking is possible for the base cell)
	// - this track has at least one tracked child & _timepoint >= childX.LastTrace
	//
	
	if (MotherTrack)
		if (_timepoint < FirstTrace)
			return 0;
	int maxTP = MaxTimePoint;
	if (ChildTrack1)
		if (ChildTrack1->LastTrace > 0)
			if (ChildTrack1->LastTrace < maxTP)
				maxTP = ChildTrack1->LastTrace - 1;
	if (ChildTrack2)
		if (ChildTrack2->LastTrace > 0)
			if (ChildTrack2->LastTrace < maxTP)
				maxTP = ChildTrack2->LastTrace - 1;
	if (_timepoint > maxTP)
		return 0;

	//if a mark at _timepoint already exists, no new is added, the former is altered
	int index = calcIndex (_timepoint);
	bool exists = false;
	TrackPoint *tp = 0;
	if ((index >= 0) && (index < (int)Positions.size())) {
		tp = Positions [index];
		exists = (tp != 0);
	}
	bool LeftMouseDown = _mouseButtons & 1;
	bool RightMouseDown = _mouseButtons & 2;
	//bool MiddleMouseDown = _mouseButtons & 4;

	if ((! _setBackground) && (! exists)) {
		//add new trackpoint

		if (index >= (int)Positions.size())
			Positions.resize ((int)((float)index * 1.5));

		tp = new TrackPoint();
		setTrackPoint (*tp, _timepoint, _x, _y, _diameter, _setBackground, LeftMouseDown | RightMouseDown, RightMouseDown, _position);
		tp->track = this;

		if (index < 0)
			//this problem occurs because the first trace is not yet set correctly; but after setting it, the index would be 0 nonetheless
			index = 0;

		if (_timepoint > LastTrace) 
			LastTrace = _timepoint;

		if (_timepoint < FirstTrace /*|| MarkCount == 0*/) { 
			updateFirstTrace (_timepoint);

			// Make sure index is still right
			index = calcIndex(_timepoint);
			if(index < 0 || index >= Positions.size())
				return false;

			SecondsFromBegin = 0;
		}

		//note: must be after updateFirstTrace() cos otherwise the first trackpoint is doubled (if the first trace is updated and index = FirstTrace)

		Positions.insert (index, tp);

		MarkCount++;

	}
	else {														
		if (exists) {
			//modify existing
			setTrackPoint (*tp, _timepoint, _x, _y, _diameter, _setBackground, LeftMouseDown | RightMouseDown, RightMouseDown, _position);
			tp->track = this;
		}
	}

	return tp;
}

//void Track::setTPXY (int _tp, float _x, float _y)
//{
//	int index = calcIndex (_tp);
//	bool exists = false;
//	TrackPoint *tp = 0;
//        if ((index >= 0) & (index < (int)Positions.size())) {
//		tp = Positions [index];
//		exists = (tp != 0);
//	}
//	
//	if (exists) {
//		tp->X = _x;
//		tp->Y = _y;
//	}
//}
//
//bool Track::deleteMark (int _timepoint)
//{
//	if (! Positions.isEmpty()) {
//		//find mark at _timepoint
//		int index = calcIndex (_timepoint);
//		if ((index >= 0) & (index < (int)Positions.size())) {
//			if (Positions [index]) {
//				//delete only if there exists a mark!
//
//				Positions.remove (index);
//
//				MarkCount--;
//
//				//update FirstTrace and LastTrace
//				if (Positions.isEmpty() /*LastTrace == FirstTrace && LastTrace == _timepoint*/) {
//					//this was the last mark -> set to initial values
//					LastTrace = -1;
//					if(!MotherTrack)
//						FirstTrace = MaxTimePoint;
//					//LastTrace = -1;
//					//FirstTrace = MaxTimePoint;
//					//SecondsFromBegin = 0;
//				}
//				else {
//					// Update first trace if the first trackpoint was removed and if we have no parent track
//					if (_timepoint == FirstTrace && !MotherTrack) {
//						for (uint i = 0; i < Positions.size(); i++) {
//							if (Positions [i])
//								if (Positions [i]->TimePoint > -1) {
//									updateFirstTrace (Positions [i]->TimePoint);
//									SecondsFromBegin = 0;
//									break;
//								}
//						}
//					}
//					if (_timepoint == LastTrace) {
//						for (int i = (int)Positions.size() - 1; i >= 0; i--) {
//							if (Positions [i])
//								if (Positions [i]->TimePoint > -1) {
//									LastTrace = Positions [i]->TimePoint;
//									break;
//								}
//						}
//					}
//				}
//
//				return true;
//			}
//			else
//				return false;
//		}
//		else
//			return false;
//	}
//	else
//		return false;
//}
//
//void Track::deleteMarks()
//{
//	if (Positions.isEmpty())
//		return;
//	
//	for (uint i = 0; i < Positions.size(); i++)
//		if (Positions [i])
//			Positions.remove (i);
//	
//	MarkCount = 0;
//	LastTrace = -1;
//	background_tracked = false;
//}
//
//const TrackPoint& Track::getTrackPoint (int _timepoint, bool _roundUp) const
//{
//	if (Positions.isEmpty())
//		return DUMMYTRACKPOINT;
//	
//	int index = calcIndex (_timepoint);
//        if ((index >= 0) & (index < (int)Positions.size())) {
//		if (Positions [index])
//			return *(Positions [index]);
//		else {
//			if (_roundUp) {
//				for (uint i = index; i < Positions.size(); i++)
//					if (Positions [i])
//						return *(Positions [i]);
//			}
//		}
//	}
//	
//	return DUMMYTRACKPOINT;
//}

TrackPoint* Track::getTrackPointByTimePoint( int _timePoint ) const
{
	// Look for trackpoint
	int index = calcIndex (_timePoint);
	if(index >= 0 && index < Positions.size())
		return Positions[index];

	// Trackpoint not found
	return 0;
}

//TrackPoint* Track::getLastTrackPointBefore( int _timePoint, bool _scanMother ) const
//{
//	// Look in this track
//	for(int tp = std::min(LastTrace, _timePoint - 1); tp >= FirstTrace; --tp) {
//		int index = calcIndex(tp);
//		if(index >= 0 && index < Positions.size() && Positions[index])
//			return Positions[index];
//	}
//
//	// Look in mother
//	if(_scanMother && MotherTrack)
//		return MotherTrack->getLastTrackPointBefore(_timePoint, true);
//
//	// Nothing found
//	return nullptr;
//}
//
//
//TrackPoint* Track::getRefTrackPoint (int _timepoint, bool _roundUp) const
//{
//	if (Positions.isEmpty())
//		return 0;
//	
//	int index = calcIndex (_timepoint);
//        if ((index >= 0) & (index < (int)Positions.size())) {
//		if (Positions [index])
//			return Positions [index];
//		else {
//			if (_roundUp) {
//				for (uint i = index; i < Positions.size(); i++)
//					if (Positions [i])
//						return Positions [i];
//			}
//		}
//	}
//	
//	return 0;
//}
//
const TrackPoint& Track::getTrackPointByNumber (int _number) //const
{
	int num = 0;
	
	for (uint i = 0; i < Positions.size(); i++) {
		if (Positions [i]) {
			num++;
			if (num == _number) {
				Positions [i]->tmpCellNumber = Number;
				return *(Positions [i]);
			}
		}
	}
	
	return DUMMYTRACKPOINT;		//nothing found
}

void Track::setChildTrack (int _number, Track *_track)
{
	if (_number == 1) {
		ChildTrack1 = _track;
		if (_track) {
			_track->ChildIndex = 1;
			_track->MotherTrack = this;
		}
	}
	else if (_number == 2) {
		ChildTrack2 = _track;
		if (_track) {
			_track->ChildIndex = 2;
			_track->MotherTrack = this;
		}
	}
}

int Track::getGeneration() const
{

	double gen = 0.0;
	modf(log((double)Number) / log(2.0), &gen);
	return (int)gen;
}

bool Track::trackPointExists (int _timepoint) const
{
	int index = calcIndex (_timepoint);
	if ((index >= 0) & (index < (int)Positions.size()))
		return (Positions [index] != 0);
	else
		return false;
}

QString Track::getStopReasonString() const
{
	switch (StopReason) {
		case TS_NONE:
			return "None";
		case TS_DIVISION:
			return "Division";
		case TS_APOPTOSIS:
			return "Apoptosis";
		case TS_LOST:
			return "Lost";
		default:
			return "";
	}
}

short int Track::getStopReasonShortInt() const
{
	switch (StopReason) {
	case TS_NONE:
		return 0;
	case TS_DIVISION:
		return 1;
	case TS_APOPTOSIS:
		return 2;
	case TS_LOST:
		return 3;
	default:
		return -1;
	}
}

////------------------ Konstantin ----------------
//int Track::getNumberOfCellTypes()
//{
//	return MathFunctions::log_2((double)CT_NOLOSTCHILDRENTYPE);
////	return log(CT_NOLOSTCHILDRENTYPE)/log(2); // log2(CT_NOLOSTCHILDRENTYPE)
//}
//
TrackStopReason Track::recoverStopReason (short int _stopReason)
{
	switch (_stopReason) {
	case 0:
		return TS_NONE;
	case 1:
		return TS_DIVISION;
	case 2:
		return TS_APOPTOSIS;
	case 3:
		return TS_LOST;
	default:
		return TS_NONE;
	}
}

bool Track::setProperties (int _timePoint, const CellProperties& _props, bool _propertyForm, bool _doNotOverwriteEndomitosis,bool _addWavelengthsOnly)
{
	//note:
	//the _timePoint must exist!
	//normally this is canonical as setProperties() is called directly after a track point
	// was set by the user

	//if (Positions.isEmpty()) 
	//	return false;
	//
	//int index = calcIndex (_timePoint);
	//if (index < 0 || index >= (int)Positions.size())
	//	return false;

	//if (! Positions [index])
	//	return false;

	//TrackPoint *tp = Positions [index];

	////set properties in TrackPoint
	//if (_props.Set_Tissues) {
	//	tp->tissueType = _props.tissueType;
	//	tp->cellGeneralType = _props.cellGeneralType;
	//	tp->cellLineage = _props.cellLineage;
	//}

	//if (_props.Set_Wavelengths) {
	//	for (int i = 0; i <= MAX_WAVE_LENGTH; i++) {
	//		if(!_addWavelengthsOnly || _props.WaveLength[i])
	//			tp->WaveLength[i] = _props.WaveLength[i];
	//	}
	//}

	////note: EndoMitosis is not set in the property form either, yet
	////		 before every call of setProperties, the provided _props object is 
	////		 set to the correct value
	//if(!_doNotOverwriteEndomitosis)
	//	tp->EndoMitosis = (_props.EndoMitosis ? 1 : 0);

	//if (! _propertyForm) {
	//	//these attributes are not set in property form
	//	if (_props.XBackground != -1) {
	//		tp->XBackground = _props.XBackground;
	//		tp->YBackground = _props.YBackground;
	//	}
	//	if (_props.CellDiameter > 0.0f)
	//		tp->CellDiameter = _props.CellDiameter;

	//	tp->Z = _props.Z;
	//	tp->NonAdherent = (_props.NonAdherent ? 1 : 0);
	//	tp->FreeFloating = (_props.FreeFloating ? 1 : 0);

	//	//tp->AddOn1 = _props.AddOn1;
	//	tp->Position = _props.Position;

	//}

	//if(_props.Set_Addon)
	//	tp->AddOn1 = _props.AddOn1;
	//if (_props.Set_NonAdherent)
	//	tp->NonAdherent = (_props.NonAdherent ? 1 : 0);
	//if (_props.Set_FreeFloating)
	//	tp->FreeFloating = (_props.FreeFloating ? 1 : 0);

	return true;
}

//void Track::cutOffTrackPoints()
//{
//	//no security options!
//	
//	for (int i = 1; i < FirstTrace; i++)
//		deleteMark (i);
//	for (int i = LastTrace + 1; i < MaxTimePoint; i++)
//		deleteMark (i);
//}
//
////recursive
//bool Track::childOf (const Track *_track1, const Track *_track2) const
//{
//	if (! _track1)
//		return false;
//	if (! _track2)
//		return false;
//	
//	if (_track1 == _track2)
//		return false;
//	
//	if (_track2 == _track1->MotherTrack)
//		return true;
//	else
//		return childOf (_track1->MotherTrack, _track2);
//}
//
//bool Track::childOf (const Track *_track) const
//{
//	return childOf (this, _track);
//}
//
//bool Track::ancestorOf (const Track *_track) const
//{
//	return childOf (_track, this);
//}
//
////--------------------Konstantin----------------------------
////recursive
//QList<Track*> Track::getProgenyInGeneration(const Track *_track, const int _gen) const
//{
//	QList<Track*> progeny;
//	progeny.clear();
//	
//	if (! _track)
//		return progeny;
//		
//	if ( _gen <= _track->getGeneration() )
//		return progeny;
//		
//	if ( ! _track->hasChildren() )
//		return progeny;
//		
//	if ( (_track->getChildTrack(1)->getGeneration() == _gen) || (_track->getChildTrack(2)->getGeneration() == _gen) ) {
//		progeny.append(_track->getChildTrack(1));
//		progeny.append(_track->getChildTrack(2));
//	}
//	else {
//		QList<Track*> progenyChild1 = getProgenyInGeneration (_track->getChildTrack(1), _gen);
//		QList<Track*> progenyChild2 = getProgenyInGeneration (_track->getChildTrack(2), _gen);
//
//		progeny = Tools::joinLists<Track*> (progenyChild1, progenyChild2);
//	}
//	
//	return progeny;
//}
//
//
////--------------------Konstantin----------------------------
//QList<Track*> Track::getProgenyInGeneration(const int _gen) const
//{
//	return getProgenyInGeneration(this, _gen);
//}
//
//
//QHash<int, QPointF> Track::getPositions (int _startTP, int _endTP, bool _background) const
//{
//	//sortTrackPoints();
//	
//        QHash<int, QPointF> result;
//	
//	if (! tracked()) 
//		return result;
//	
//	if (_startTP == -1)
//		_startTP = FirstTrace;
//	if (_endTP == -1)
//		_endTP = LastTrace;
//	
//	//loop through each timepoint, if a trackpoint is present, append it
//	//note: calcIndex not necessary here, as getTrackPoint() is called
//	//note: a background point is added if it does not exist, as the loop iterates over the normal coordinates
//	for (int i = _startTP; i <= _endTP; i++) {
//		TrackPoint tp = getTrackPoint (i);
//		if (tp.X != -1) {
//			if (! _background)
//				result.insert (i, tp.point());
//			else
//				result.insert (i, tp.backgroundPoint());
//		}
//	}
//	
//	return result;
//}
//
QMap<int, ITrackPoint*> Track::getTrackPointsRange (int _startTP, int _endTP) const
{
	QMap<int, ITrackPoint*> result;
	
	if (! tracked()) 
		return result;
	
	if (_startTP == -1)
		_startTP = FirstTrace;
	if (_endTP == -1)
		_endTP = LastTrace;
	
	//loop through each timepoint, if a trackpoint is present, append it
	//note: calcIndex not necessary here, as getTrackPoint() is called
	//note: a background point is added if it does not exist, as the loop iterates over the normal coordinates
	for (int i = _startTP; i <= _endTP; i++) {
		TrackPoint* tp = getTrackPointByTimePoint (i);
		if (tp) {
				result.insert (i, tp);
		}
	}	
	return result;
}


//
//bool Track::backgroundTracked (int /*_wavelength*/) const
//{
//	if (Positions.isEmpty())
//		return false;
//	
//	return background_tracked;
//}
//
bool Track::updateFirstTrace (int _newFirstTrace)
{

	if(MarkCount > 0) {
		int diff = FirstTrace - _newFirstTrace;

		//shift all track assignments to maintain the mapping  calcIndex: timepoint |-> pointer
		if (diff > 0) {
			//the new first trace is before the old first trace

			// Resize Postitions if required
			if (LastTrace - _newFirstTrace >= (int)Positions.size())
				Positions.resize ((int)((float)(LastTrace - _newFirstTrace) * 1.5));


			//if (LastTrace - _newFirstTrace >= (int)Positions.size())
				//Positions.resize ((int)((float)(LastTrace - _newFirstTrace) * 1.5));

			//shifting has to be topdown
			for (int i = LastTrace - FirstTrace; i >= 0; i--) {
				Q_ASSERT(i < Positions.size());
			
				if (Positions [i]) {
					TrackPoint *tp = Positions.at (i);//new TrackPoint (*(Positions [i]));
					Positions.insert (i + diff, tp);
					//Positions.remove (i);
				}
			}
		}
		else {
			//the new first trace is after the old first trace

		
			//shifting has to be bottomup
			//note: diff < 0
			for (int i = 0; i <= LastTrace - _newFirstTrace; i++) {
				Q_ASSERT(i - diff < Positions.size());

				if (Positions [i - diff]) {
					TrackPoint *tp = Positions.at (i - diff); //new TrackPoint (*(Positions [i - diff]));

					Q_ASSERT(i < Positions.size());

					// 04/11/11-OH: Fix: Here we could implicitly remove existing trackpoints (in the interval [FirstTrace,_newFirstTrace[), so we have to update markCount
					if(Positions[i] && Positions[i]->TimePoint != -1) {
						TrackPoint *oldTrackPoint = Positions.at(i);
						delete oldTrackPoint;
						--MarkCount;
					}

					Positions.insert (i, tp);
					//Positions.remove (i - diff);
				}
			}
		}
	}
	
	FirstTrace = _newFirstTrace;
	
	return true;
}

bool Track::hasChildren() const
{
	if (ChildTrack1)
		if (ChildTrack2)
			return true;
	
	return false;
}

//int Track::calcActionRadius (int _startTP, int _endTP) const
//{
//	if (_startTP == -1)
//		_startTP = FirstTrace;
//	if (_endTP == -1)
//		_endTP = LastTrace;
//	
//	//calculation scheme:
//	//take position of the cell at _startTP and calculate all distances to its later trackpoints (up to _endTP) and return the maximum of all these distances
//	
//	int maxRadius = 0;
//	int distance = 0;
//	TrackPoint p, firstP = getTrackPoint (_startTP, true);
//	
//	for (int tp = _startTP; tp <= _endTP; tp++) {
//		p = getTrackPoint (tp);
//		if (p.isSet()) {
//			if (firstP.isSet()) {
//				distance = (int)p.distanceTo (firstP);
//				if (distance > maxRadius)
//					maxRadius = distance;
//			}
//		}
//	}
//	
//	return maxRadius;
//}
//	
//int Track::calcTotalDistance (int _startTP, int _endTP) const
//{
//	if (_startTP == -1)
//		_startTP = FirstTrace;
//	if (_endTP == -1)
//		_endTP = LastTrace;
//	
//	//calculation scheme:
//	//add up all distances between _startTP and _endTP and return the sum
//	
//	int distanceSum = 0;
//	int distance = 0;
//	TrackPoint p, oldP;
//	
//	for (int tp = _startTP; tp <= _endTP; tp++) {
//		p = getTrackPoint (tp);
//		if (p.isSet()) {
//			if (oldP.isSet()) {
//				distance = (int)p.distanceTo (oldP);
//				distanceSum += distance;
//			}
//			oldP = p;
//		}
//	}
//	
//	return distanceSum;
//}
//
//void Track::calcMovements (float &_actionRadius, float &_totalDistance, float &_distanceDT, float &_MSD,  int _startTP, int _endTP) const
//{
//	if (_startTP == -1)
//		_startTP = FirstTrace;
//	if (_endTP == -1)
//		_endTP = LastTrace;
//
//	//calculation scheme:
//	//take position of the cell at _startTP and calculate all distances to its later trackpoints (up to _endTP) and return the maximum of all these distances
//
//	int maxRadius = 0;
//	int distance = 0;
//	TrackPoint p, oldP, firstP = getTrackPoint (_startTP, true);
//	int distanceSum = 0;
//
//	for (int tp = _startTP; tp <= _endTP; tp++) {
//		p = getTrackPoint (tp);
//
//		if (p.isSet()) {
//			//action radius
//			if (firstP.isSet()) {
//				distance = p.distanceTo (firstP);
//				if (distance > maxRadius)
//					maxRadius = distance;
//			}
//		}
//
//		if (p.isSet()) {
//			//total distance
//			if (oldP.isSet()) {
//				distance = p.distanceTo (oldP);
//				distanceSum += distance;
//			}
//			oldP = p;
//		}
//	}
//
//
//	_actionRadius = maxRadius;
//	_totalDistance = distanceSum;
//
//	// calculate directionality D/T: (distance between last and first trackpoint) / (entire distance)
//	// TrackPoint lastTrackpoint = getTrackPoint (oldP, true);
//	float distanceFirstToLast = oldP.distanceTo(firstP);
//	_distanceDT = distanceFirstToLast / _totalDistance;
//
//}
//

float Track::getOptValue(QString _name, const TTTPositionManager *_tttpm) const
{
	if ( _tttpm ) {

		if ( _name == TRACK_OPT_START_TP )
			//		return getFirstTrace();
			return _tttpm->getFiles()->calcSeconds (_tttpm->getFirstTimePoint(), getFirstTrace());
		if ( _name == TRACK_OPT_STOP_TP )
			//		return getLastTrace();
			return _tttpm->getFiles()->calcSeconds (_tttpm->getFirstTimePoint(), getLastTrace());
		if ( _name == TRACK_OPT_LIFETIME )
			//return 0;
			return _tttpm->getFiles()->calcSeconds (getFirstTrace(), getLastTrace());


		float mmpp = TATInformation::getInst()->getWavelengthInfo (0).getMicrometerPerPixel();

		float actionRadius = 0;
		float totalDist = 0;
		float distanceDT = 0.0;
		float MSD = 0.0;

		// calcMovements (actionRadius, totalDist, distanceDT, MSD);

		if ( _name == TRACK_OPT_ENTIRE_DISTANCE ) {
			return totalDist;
		}

	}

	return 0;

}

CellType Track::getCellType() const
{
	switch (StopReason) {
		case TS_DIVISION:
			return CT_DIVIDINGTYPE;
		case TS_APOPTOSIS:
			return CT_APOPTOTICTYPE;
		case TS_LOST:
			return CT_LOSTTYPE;
		case TS_NONE:
			return CT_INTERRUPTEDTYPE;
	}
	
	return CT_NOCELLTYPE;
}

//bool Track::subsumesCellTypes (long _cellTypes) const //, bool _all) const
//{
//	if ( _cellTypes  & cellTypeToLong (CT_DIFFERENTSISTERSTOPREASONTYPE)) {
//		if ( MotherTrack )
//			return (MotherTrack->getChildTrack(1)->getStopReason() != MotherTrack->getChildTrack(2)->getStopReason());
//		else
//			return false; 
//	}
//
//	switch (StopReason) {
//		case TS_DIVISION:
//			if (_cellTypes & cellTypeToLong (CT_DIVIDINGTYPE))
//				return true;
//			
//			if (hasChildren()) {
//				//this track has both children
//				
//				if ((_cellTypes & cellTypeToLong (CT_DIVIDINGCHILDRENTYPE)) &&
//				    (ChildTrack1->getStopReason() == TS_DIVISION) &&
//				    (ChildTrack2->getStopReason() == TS_DIVISION))
//					return true;
//				
//				if ((_cellTypes & cellTypeToLong (CT_DIFFERENTCHILDRENSTOPREASONTYPE)) &&
//				    (ChildTrack1->getStopReason() != ChildTrack2->getStopReason()))
//					return true;
//				
//				if ((_cellTypes & cellTypeToLong (CT_NOLOSTCHILDRENTYPE)) &&
//				    (ChildTrack1->getStopReason() != TS_LOST) &&
//				    (ChildTrack2->getStopReason() != TS_LOST))
//				    	return true;
//			}
//			
//			return false;
//		case TS_APOPTOSIS:
//			return (_cellTypes & cellTypeToLong (CT_APOPTOTICTYPE));
//			break;
//		case TS_LOST:
//			return (_cellTypes & cellTypeToLong ( CT_LOSTTYPE));
//			break;
//		case TS_NONE:
//			return (_cellTypes & cellTypeToLong ( CT_INTERRUPTEDTYPE));
//			break;
//		default:
//			;
//	}
//	
//	return (_cellTypes & cellTypeToLong (CT_NOCELLTYPE));
//}
//
//long Track::cellTypeToLong (CellType _cellType)
//{
//	return _cellType;
//}
//	
//CellType Track::longToCellType (long _cellType)
//{
//	switch (_cellType) {
//		case CT_NOCELLTYPE:
//			return CT_NOCELLTYPE;
//		case CT_ALLTYPES:
//			return CT_ALLTYPES;
//		case CT_DIVIDINGTYPE:
//			return CT_DIVIDINGTYPE;
//		case CT_APOPTOTICTYPE:
//			return CT_APOPTOTICTYPE;
//		case CT_LOSTTYPE:
//			return CT_LOSTTYPE;
//		case CT_INTERRUPTEDTYPE:
//			return CT_INTERRUPTEDTYPE;
//		case CT_DIVIDINGCHILDRENTYPE:
//			return CT_DIVIDINGCHILDRENTYPE;
//		case CT_DIFFERENTCHILDRENSTOPREASONTYPE:
//			return CT_DIFFERENTCHILDRENSTOPREASONTYPE;
//		case CT_DIFFERENTSISTERSTOPREASONTYPE:
//			return CT_DIFFERENTSISTERSTOPREASONTYPE;
//		case CT_NOLOSTCHILDRENTYPE:
//			return CT_NOLOSTCHILDRENTYPE;
//		default:
//			return CT_NOCELLTYPE;
//	}
//}
//	
//const QString Track::cellTypeToString (CellType _cellType)
//{
//	switch (_cellType) {
//		case CT_NOCELLTYPE:
//			return "NoType";
//		case CT_ALLTYPES:
//			return "All types";
//		case CT_DIVIDINGTYPE:
//			return "Dividing";
//		case CT_APOPTOTICTYPE:
//			return "Apoptotic";
//		case CT_LOSTTYPE:
//			return "Lost";
//		case CT_INTERRUPTEDTYPE:
//			return "Interrupted";
//		case CT_DIVIDINGCHILDRENTYPE:
//			return "Dividing children";
//		case CT_DIFFERENTCHILDRENSTOPREASONTYPE:
//			return "Different offspring stop reason";
//		case CT_DIFFERENTSISTERSTOPREASONTYPE:
//			return "Different sister stop reason";
//		case CT_NOLOSTCHILDRENTYPE:
//			return "No lost children";
//		default:
//			return "";
//	}
//}
//	
//CellType Track::stringToCellType (const QString &_cellType)
//{
//	//use the inversion function and find the assigned cell type
//	
//	for (long ct = CT_ALLTYPES; ct <= CT_NOLOSTCHILDRENTYPE; ct *= 2) {
//		CellType cellType = Track::longToCellType (ct);
//		if (cellTypeToString (cellType) == _cellType)
//			return cellType;
//	}
//	
//	return CT_NOCELLTYPE;
//}
//
//int Track::getAverage (const QString &_attributeName, int _select) const
//{
//	int value = 0;
//	int sum = 0;
//	int count = 0;
//	
//	for (uint i = 0; i < Positions.size(); i++) {
//		if (Positions [i]) {
//			count++;
//			
//			value = (Positions [i]->getValue (_attributeName, _select));
//			sum += value;
//		}
//	}
//	
//	if (count > 0)
//		return (int) ((float)sum / (float)count);
//	else
//		return -1;
//}
//
//int Track::getFirstOccurence (const QString &_attributeName, int _select) const
//{
//	int index = 0;
//	int value = 0;
//	
//	for (int tp = FirstTrace; tp <= LastTrace; tp++) {
//		index = calcIndex (tp);
//		
//                if (((index >= 0) & (index < (int)Positions.size())) && (Positions [index] != 0)) {
//			value = (Positions [index]->getValue (_attributeName, _select));
//			if (value > 0) {
//				return tp;
//			}
//		}
//	}
//	
//	return -1;
//}
//
//// ------------------ Konstantin ----------------
//int Track::getFirstOccurenceBitpack (const QString &_attributeName, int _bitpack, int _addOnValue) const
//{
//	int index = 0;
//	int value = 0;
//	
//	for (int tp = FirstTrace; tp <= LastTrace; tp++) {
//		index = calcIndex (tp);
//		
//                if (((index >= 0) & (index < (int)Positions.size())) && (Positions [index] != 0)) {
//			value = (Positions [index]->getValue (_attributeName, _bitpack));
//			if (value == _addOnValue) {
//				return tp;
//			}
//		}
//	}
//	
//	return -1;
//	
//}
//
//int Track::getLastOccurence (const QString &_attributeName, int _select) const
//{
//	int index = 0;
//	int value = 0;
//	
//	for (int tp = LastTrace; tp >= FirstTrace; tp--) {
//		index = calcIndex (tp);
//		
//                if (((index >= 0) & (index < (int)Positions.size())) && (Positions [index] != 0)) {
//			value = (Positions [index]->getValue (_attributeName, _select));
//			if (value > 0) {
//				return tp;
//			}
//		}
//	}
//	
//	return -1;
//}
//
//// ------------------ Konstantin ----------------
//int Track::getLastOccurenceBitpack (const QString &_attributeName, int _bitpack, int _addOnValue) const
//{
//	int index = 0;
//	int value = 0;
//	
//	for (int tp = LastTrace; tp >= FirstTrace; tp--) {
//		index = calcIndex (tp);
//		
//                if (((index >= 0) & (index < (int)Positions.size())) && (Positions [index] != 0)) {
//			value = (Positions [index]->getValue (_attributeName, _bitpack));
//			if (value == _addOnValue) {
//				return tp;
//			}
//		}
//	}
//	
//	return -1;
//}
//
//// Count how long this track has a value > 0 for the given attribute
// int Track::getAttributeOccurenceDurability (const QString &_attributeName, int _select, const TTTPositionManager *_tttpm) const
// {
//    int duration = 0;	// Counter for duration in seconds
//
//    int curTP = -1;
//	int curValue = -1;
//	for(int nextTP = FirstTrace; nextTP <= LastTrace; nextTP++) 
//	{
//        int nextIndex = calcIndex(nextTP);
//			
//		if(Positions[nextIndex] == 0)	// Ignore empty entries
//			continue;
//
//        int nextValue = Positions[nextIndex]->getValue(_attributeName, _select);
//
//        //if((nextValue > 0) && (curValue > 0)) {
//		if(curValue > 0) {	// Easy version, all time is counted for the last available TP
//			duration +=  _tttpm->getFiles()->calcSeconds(curTP, nextTP);
//		}
//
//		curTP = nextTP;
//		curValue = nextValue;
//                
//    }
//
//    return duration;
// }
//
// // Count how long this track has the value _addObValue for the given attribute
// int Track::getBitpackAttributeOccurenceDurability (const QString &_attributeName, int _bitpack, int _addOnValue, const TTTPositionManager *_tttpm) const
// {
//    int duration = 0;	// Counter for duration in seconds
//
//    int curTP = -1;
//	int curValue = -1;
//	for(int nextTP = FirstTrace; nextTP <= LastTrace; nextTP++) 
//	{
//        int nextIndex = calcIndex(nextTP);
//			
//		if(Positions[nextIndex] == 0)	// Ignore empty entries
//			continue;
//
//        int nextValue = Positions[nextIndex]->getValue(_attributeName, _bitpack);
//
//        //if((nextValue == _addOnValue) && (curValue == _addOnValue)) {
//		if(curValue == _addOnValue) {	// Easy version, all time is counted on the last available TP
//			duration +=  _tttpm->getFiles()->calcSeconds(curTP, nextTP);
//		}
//
//		curTP = nextTP;
//		curValue = nextValue;
//                
//    }
//
//    return duration;
//}
//
//int Track::getStartSetting (const QString &_attributeName, int _select) const
//{
//	int index = calcIndex (FirstTrace);
//	
//    if (((index >= 0) & (index < (int)Positions.size())) && (Positions [index] != 0)) {
//		return (Positions [index]->getValue (_attributeName, _select));
//	}
//	
//	return 0;
//}
//
//int Track::getEndSetting (const QString &_attributeName, int _select) const
//{
//	int index = calcIndex (LastTrace);
//	
//    if (((index >= 0) & (index < (int)Positions.size())) && (Positions [index] != 0)) {
//		return (Positions [index]->getValue (_attributeName, _select));
//	}
//	
//	return 0;
//}
//
//Track* Track::getSibling() const
//{
//	if (MotherTrack) {
//		if (ChildIndex == 1)
//			return MotherTrack->getChildTrack (2);		//can be 0!
//		else
//			return MotherTrack->getChildTrack (1);		//can be 0!
//	}
//	
//	return 0;	//has no sibling, as it is the base cell
//}
//
//bool Track::eraseStoredPositions()
//{
//	for (int i = FirstTrace; i <= LastTrace; i++) {
//		TrackPoint *tp = getRefTrackPoint (i);
//		if (tp)
//			tp->Position = "";
//	}
//	
//	return true;
//}
//
//bool Track::appendTree( Tree* _tree, bool _allowOverwriteOfTrackPoints, QString* _errorMsg )
//{
//#ifdef TTT_ONLY	
//	// Debug information variable defined in main.cpp
//	g_debugInformation = 106;
//#endif
//
//	// Check tree
//	if(!_tree) {
//		if(_errorMsg) 
//			*_errorMsg = "_tree is NULL.";
//		return false;
//	}
//
//	// If we have child tracks, appending is not allowed
//	if(ChildTrack1 || ChildTrack2) {
//		if(_errorMsg) 
//			*_errorMsg = "Attempted to append tree to a track that already has child tracks.";
//		return false;
//	}
//
//	// Get root track of _tree
//	Track* root = _tree->getRootNode();
//	if(!root) {
//		if(_errorMsg) 
//			*_errorMsg = "Could not get first cell (root track) of tree to append.";
//		return false;
//	}
//
//	// Get first timepoint
//	int firstTp = _tree->getMinTrackedTimePoint();
//	if(firstTp == -1 || firstTp >= 0xFFFF) {
//		if(_errorMsg) 
//			*_errorMsg = "Could not get MinTrackedTimePoint of tree to append: " + QString("%1").arg(firstTp);
//		return false;
//	}
//
//	// FirstTp must equal root nodes first timepoint or something strange is going on
//	if(firstTp != root->getFirstTimePoint()) {
//		if(_errorMsg) 
//			*_errorMsg = "Consistence check failed (1): " + QString("%1").arg(firstTp) + QString("%1").arg(root->getFirstTimePoint()); 
//		return false;
//	}
//
//	// Get my last trace
//	int myLastTrace = LastTrace;
//	if(myLastTrace <= 0) {
//		if(Track* mother = getMotherTrack()) {
//			myLastTrace = mother->getLastTrace();
//		}
//	}
//	if(myLastTrace <= 0) {
//		if(_errorMsg) 
//			*_errorMsg = "Could not get LastTrace of this Track: " + QString("%1").arg(myLastTrace);
//		return false;
//	}
//
//	if(_allowOverwriteOfTrackPoints) {
//		// Compare first timepoints
//		if(FirstTrace > firstTp) {
//			if(_errorMsg) 
//				*_errorMsg = "Tree to append starts before this track starts: " + QString("%1").arg(FirstTrace) + " > " + QString("%1").arg(firstTp);
//			return false;
//		}
//	}
//	else {
//		// Compare first timepoint with last trace
//		if(firstTp <= myLastTrace) {
//			if(_errorMsg) 
//				*_errorMsg = "Tree to append starts before this track ends: " + QString("%1").arg(myLastTrace) + " > " + QString("%1").arg(firstTp);
//			return false;
//		}
//	}
//
//	// Now we should be able to safely append the _tree
//
//	// Trackpoints of first track
//	for(int i = 0; i < root->Positions.size(); ++i) {
//		if(!root->Positions[i])
//			continue;
//
//		// Try to add
//		if(!addTrackPoint(*(root->Positions[i]), _allowOverwriteOfTrackPoints)) {
//			if(_errorMsg) 
//				*_errorMsg = QString("Failed to append TrackPoint to tree (TP: %1)").arg(root->Positions[i]->TimePoint);
//			return false;
//		}
//	}
//
//	// Other tracks
//	if(root->ChildTrack1 && root->ChildTrack2) {
//		// We have children..
//		setStopReason(TS_DIVISION);
//
//		recursivelyAppendChildren(root->ChildTrack1, true);
//		recursivelyAppendChildren(root->ChildTrack2, false);
//	}
//
//	// Mark tree as edited
//	if(tree) 
//		tree->setModified(true);
//
//	return true;
//}
//
//void Track::recursivelyAppendChildren( Track* _childToAppend, bool _leftChild)
//{
//#ifdef TTT_ONLY	
//	// Debug information variable defined in main.cpp
//	g_debugInformation = 105;
//#endif
//
//	// Abort if parameter is 0
//	if(!_childToAppend)
//		return;
//
//	// Calc child number
//	int childNumber;
//	if(_leftChild)
//		childNumber = 2 * Number;
//	else
//		childNumber = 2 * Number + 1;
//
//	// Create track
//	Track* newTrack = new Track(this, _leftChild ? 1 : 2, MaxTimePoint, _childToAppend->getFirstTrace());
//	newTrack->setNumber(childNumber);
//
//	// Copy trackpoints
//	for(int i = 0; i < _childToAppend->Positions.size(); ++i) {
//		if(!_childToAppend->Positions[i])
//			continue;
//
//		// Try to add
//		if(!newTrack->addTrackPoint(*(_childToAppend->Positions[i]), true)) {
//			//if(_errorMsg) 
//			//	*_errorMsg = QString("Failed to append TrackPoint to tree (TP: %1)").arg(_childToAppend->Positions[i]->TimePoint);
//			return;
//		}
//	}
//
//	// Insert into tree
//	tree->insert(newTrack, false);
//
//	// Recursive call
//	if(_childToAppend->ChildTrack1 && _childToAppend->ChildTrack2) {
//		// We have children..
//		setStopReason(TS_DIVISION);
//
//		newTrack->recursivelyAppendChildren(_childToAppend->ChildTrack1, true);
//		newTrack->recursivelyAppendChildren(_childToAppend->ChildTrack2, false);
//	}
//}
//
//QPointF Track::getLastPositionAtOrBefore( int _timePoint ) const
//{
//#ifdef TTT_ONLY	
//	// Debug information variable defined in main.cpp
//	g_debugInformation = 104;
//#endif
//
//	TrackPoint* trackPoint = 0;
//
//	// Make sure _timePoint is not bigger than lastTrace in order to save time
//	if(LastTrace > 0) {
//		_timePoint = qMin(_timePoint, LastTrace);
//
//		// Try to find TrackPoint at or before _timePoint
//		for(int tp = _timePoint; tp >= FirstTrace; --tp) {
//			trackPoint = getTrackPointByTimePoint(tp);
//			if(trackPoint)
//				break;
//		}
//	}
//
//	// Check if trackpoint was found
//	if(!trackPoint) {
//		// Try to get mother's last trackpoint
//		if(MotherTrack)
//			trackPoint = MotherTrack->getTrackPointByTimePoint(MotherTrack->getLastTrace());
//		// Still no trackpoint
//		if(!trackPoint)
//			return QPointF();
//	}
//
//	// Return position of trackpoint
//	return trackPoint->point();
//}
//
//QSharedPointer<Tree> Track::cutTrack( int _timePoint )
//{
//#ifdef TTT_ONLY	
//	// Debug information variable defined in main.cpp
//	g_debugInformation = 103;
//#endif
//
//	// Track must have at least one trackpoint
//	if(LastTrace < 0 || LastTrace < FirstTrace || MarkCount == 0) {
//		qWarning() << "cut track failed because mark count is zero";
//		return QSharedPointer<Tree>();
//	}
//
//	// _timePoint must be within lifetime of this track
//	if(_timePoint < FirstTrace || _timePoint > LastTrace) {
//		qWarning() << "cut track failed because time point outside lifetime";
//		return QSharedPointer<Tree>();
//	}
//
//	// Tree must be set
//	if(!tree) {
//		qWarning() << "cut track failed because tree not set";
//		return QSharedPointer<Tree>();
//	}
//
//	// Create new tree
//	Tree* tmp = new Tree();
//	tmp->reset();
//	tmp->setModified(true);
//	QSharedPointer<Tree> newTree(tmp);
//
//	// Create new root track
//	Track* newRoot = new Track(0, 0, MaxTimePoint, _timePoint);
//	newRoot->setNumber(1);
//	newRoot->setTree(newTree.data());
//
//	// Add to tree
//	newTree->insert(newRoot, false);
//
//	// Move trackpoints
//	for(int tp = _timePoint; tp <= LastTrace; ++tp) {
//		// Calc Positions index
//		int idx = calcIndex(tp);
//		if(idx < 0 || idx >= Positions.size())
//			continue;
//
//		// Get trackpoint
//		TrackPoint* trackPoint = Positions[idx];
//		if(trackPoint) {
//			// Add to new root
//			if(!newRoot->addTrackPoint(*trackPoint, false)) {
//				qWarning() << "TTT Warning: addTrackPoint() failed in Track::cutTrack()";
//				return QSharedPointer<Tree>();
//			}
//
//			// Remove from this track
//			if(!deleteMark(tp)) {
//				qWarning() << "TTT Warning: deleteMark() failed in Track::cutTrack()";
//				return QSharedPointer<Tree>();
//			}
//		}
//	}
//
//	// Process child tracks
//	if(ChildTrack1)
//		recursivelyCutTree(tree->tracks[ChildTrack1->Number], tree, newTree.data(), newRoot, 2);
//	if(ChildTrack2)
//		recursivelyCutTree(tree->tracks[ChildTrack2->Number], tree, newTree.data(), newRoot, 3);
//
//	// Mark this tree as modified
//	tree->setModified(true);
//
//	// Finished
//	return newTree;
//}
//
//void Track::recursivelyCutTree(QSharedPointer<Track> _track, Tree* _oldTree, Tree* _newTree, Track* _newParent, int _newTrackNumber)
//{
//#ifdef TTT_ONLY	
//	// Debug information variable defined in main.cpp
//	g_debugInformation = 102;
//#endif
//
//	// _track should not be 0
//	if(_track.isNull()) {
//		qWarning() << "TTT Warning: _track.isNull() in Track::recursivelyCutTree()";
//		return;
//	}
//
//	// _track must not be root
//	if(_oldTree->BaseTrack == _track.data()) {
//		qWarning() << "TTT Warning: _track is base track in Track::recursivelyCutTree()";
//		return;
//	}
//
//	// Remove from old tree
//	_oldTree->tracks.remove(_track->Number);
//	
//	// Remove from old mother track
//	Track* mother = _track->getMotherTrack();
//	if(mother) {
//		mother->setChildTrack(_track->getChildIndex(), 0);
//	}
//
//	// Set number
//	_track->setNumber(_newTrackNumber);
//
//	// Insert into new tree
//	_newTree->insert(_track, false);
//
//	// Add to new mother track
//	if(_newParent) {
//		_newParent->setChildTrack(_track->getChildIndex(), _track.data());
//	}
//
//	// Process children
//	if(_track->ChildTrack1)
//		recursivelyCutTree(_oldTree->tracks[_track->ChildTrack1->Number], _oldTree, _newTree, _track.data(), _newTrackNumber * 2);
//	if(_track->ChildTrack2)
//		recursivelyCutTree(_oldTree->tracks[_track->ChildTrack2->Number], _oldTree, _newTree, _track.data(), _newTrackNumber * 2 + 1);
//}
//
//bool Track::pruneTree( int _timePoint )
//{
//#ifdef TTT_ONLY	
//	// Debug information variable defined in main.cpp
//	g_debugInformation = 101;
//#endif
//
//	// _timePoint must be within lifetime of this track
//	if(_timePoint < FirstTrace || _timePoint > LastTrace) {
//		qWarning() << "pruneTree failed because time point outside lifetime";
//		return false;
//	}
//
//	// Tree must be set
//	if(!tree) {
//		qWarning() << "pruneTree failed because tree not set";
//		return false;
//	}
//	
//	// Remove trackpoints
//	for(int tp = _timePoint + 1; tp <= LastTrace; ++tp) {
//		deleteMark(tp);
//	}
//
//	// Remove children
//	std::vector<int> tracksToRemove;
//	if(ChildTrack1) {
//		tracksToRemove.push_back(Number * 2);
//		ChildTrack1 = nullptr;
//	}
//	if(ChildTrack2) {
//		tracksToRemove.push_back(Number * 2 + 1);
//		ChildTrack2 = nullptr;
//	}
//	while(tracksToRemove.size() > 0) {
//		// Get track to remove
//		int curTrack = tracksToRemove[tracksToRemove.size() - 1];
//		tracksToRemove.pop_back();
//		QSharedPointer<Track> trackToRemove = tree->tracks.value(curTrack, QSharedPointer<Track>());
//		if(trackToRemove) {
//			// Check if has children
//			if(trackToRemove->ChildTrack1)
//				tracksToRemove.push_back(trackToRemove->Number * 2);
//			if(trackToRemove->ChildTrack2)
//				tracksToRemove.push_back(trackToRemove->Number * 2 + 1);
//		}
//
//		// Remove from hash
//		tree->tracks.remove(curTrack);
//	}
//	return true;
//}
//
void Track::setStopReason (TrackStopReason _stopReason) 
{
	if(StopReason != _stopReason) {
		StopReason = _stopReason;

		// Mark tree as modified
		if(tree)
			tree->setModified(true);
	}
}

//
//bool Track::isOfCellType (CellType _cellType) const
//{
//	if ( _cellType == CT_DIFFERENTSISTERSTOPREASONTYPE ) {
//		if ( MotherTrack )
//			return (MotherTrack->getChildTrack(1)->getStopReason() != MotherTrack->getChildTrack(2)->getStopReason());
//		else 
//			return false;
//	}
//
//	switch (StopReason) {
//	case TS_DIVISION:
//		if (_cellType == CT_DIVIDINGTYPE)
//			return true;
//
//		if (hasChildren()) {
//			//this track has both children
//
//			if ((_cellType == CT_DIVIDINGCHILDRENTYPE) &&
//				(ChildTrack1->getStopReason() == TS_DIVISION) &&
//				(ChildTrack2->getStopReason() == TS_DIVISION))
//				return true;
//
//			if ((_cellType == CT_DIFFERENTCHILDRENSTOPREASONTYPE) &&
//				(ChildTrack1->getStopReason() != ChildTrack2->getStopReason()))
//				return true;
//
//			if ((_cellType == CT_NOLOSTCHILDRENTYPE) &&
//				(ChildTrack1->getStopReason() != TS_LOST) &&
//				(ChildTrack2->getStopReason() != TS_LOST))
//				return true;
//		}
//
//		return false;
//	case TS_APOPTOSIS:
//		return (_cellType == CT_APOPTOTICTYPE);
//		break;
//	case TS_LOST:
//		return (_cellType == CT_LOSTTYPE);
//		break;
//	case TS_NONE:
//		return (_cellType == CT_INTERRUPTEDTYPE);
//		break;
//	default:
//		;
//	}
//
//	return (_cellType == CT_NOCELLTYPE);
//}
//
//
//
//bool Track::mergeTrees (Tree *_treeFrom, Tree *_treeInto, int _cellFrom, int _cellInto, Tree *_newTree)
//{
//#ifdef TTT_ONLY	
//	// Debug information variable defined in main.cpp
//	g_debugInformation = 100;
//#endif
//
//	if (! _treeFrom)
//		return false;
//	if (! _treeInto)
//		return false;
//
//	if (_newTree) {
//		//merge _treeFrom and _treeInto into the new tree _newTree
//
//		//1) get base tracks of both trees
//		//Track *trackA1 = _treeFrom->getTrack (1);
//		//Track *trackB1 = _treeInto->getTrack (1);
//		QSharedPointer<Track> trackA1 = _treeFrom->tracks.value(1);
//		QSharedPointer<Track> trackB1 = _treeInto->tracks.value(1);
//		Track *baseTrackNew = _newTree->getBaseTrack();
//
//		if (! trackA1)
//			return false;
//		if (! trackB1)
//			return false;
//		if (! baseTrackNew)
//			return false;
//
//		//2) recursively shift track numbers of both trees up 
//		_treeFrom->shiftTrackNumbers (trackA1.data(), 2);
//		_treeFrom->shiftTrackNumbers (trackB1.data(), 3);
//
//		//3) append both source cells to new tree
//
//		//get the earliest appearance of both daugther cells
//		int minFirstTrack = trackA1->getFirstTrace();	
//		if (minFirstTrack > trackB1->getFirstTrace()) {
//			minFirstTrack = trackB1->getFirstTrace();
//			trackA1->setFirstTrace (minFirstTrack);
//		}
//		else {
//			trackB1->setFirstTrace (minFirstTrack);
//		}
//
//		baseTrackNew->setFirstTrace (minFirstTrack);
//
//		baseTrackNew->setStopReason (TS_DIVISION);
//		baseTrackNew->setChildTrack (1, trackA1.data());
//		baseTrackNew->setChildTrack (2, trackB1.data());
//
//		_newTree->insertComplexTrack (trackA1, false);
//		_newTree->insertComplexTrack (trackB1, false);
//
//	}
//	else {
//		//merge _treeFrom into _treeInto, taking only the desired cells
//
//		Track *trackFrom = _treeFrom->getTrack (_cellFrom);
//		Track *trackInto = _treeInto->getTrack (_cellInto);
//
//		if (! trackFrom)
//			return false;
//		if (! trackInto)
//			return false;
//
//		//check if destination cell has no children yet
//		if (trackInto->hasChildren())
//			return false;
//
//		//check for timepoint consistency: 
//		//- the starting timepoint of _cellFrom in _treeFrom needs to be later than the starting timepoint of _cellInto in _treeInto
//		//- the end timepoint of _cellFrom needs to be higher (later) than the end timepoint of _cellInto
//		if (trackFrom->getFirstTrace() < trackInto->getFirstTrace())
//			return false;
//		if (trackFrom->getLastTrace() < trackInto->getLastTrace())
//			return false;
//
//
//		//steps to merge:
//		//1) merge trackpoints of the two cells
//		//2) shift track numbers of source tree
//		//3) append children of source cell on destination cell
//
//
//		//1) merge trackpoints of the two cells
//		//   if there is an overlapping region, take the destination tracks
//		for (int tp = trackInto->getLastTrace() + 1; tp <= trackFrom->getLastTrace(); tp++) {
//			TrackPoint trackPoint = trackFrom->getTrackPoint (tp, false);
//
//			if (trackPoint.TimePoint != -1) {
//				trackInto->addTrackPoint (trackPoint);
//			}
//		}
//
//		//2) shift track numbers of source tree recursively
//		_treeFrom->shiftTrackNumbers (trackFrom, trackInto->getNumber());
//
//		//3) append children of source cell to destination cell
//		trackInto->setStopReason (TS_DIVISION);
//		trackInto->setChildTrack (1, trackFrom->getChildTrack (1));
//		trackInto->setChildTrack (2, trackFrom->getChildTrack (2));
//
//		_treeInto->insertComplexTrack (trackFrom->getChildTrackShared (1), false);
//		_treeInto->insertComplexTrack (trackFrom->getChildTrackShared (2), false);
//
//		// //the number of tracks in the destination tree must be recalculated
//		//_treeInto->recalculateNumberOfTracks();
//	}
//
//	return true;
//}
//
//QSharedPointer<Track> Track::getChildTrackShared( int _child )
//{
//	if(_child == 1)
//		return tree->tracks.value(Number * 2);
//	else if(_child == 2)
//		return tree->tracks.value(Number * 2 + 1);
//	return QSharedPointer<Track>();
//}
