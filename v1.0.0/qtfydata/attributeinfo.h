/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ATTRIBUTEINFO_H
#define ATTRIBUTEINFO_H

#include <QString>
#include <QStringList>

#include "qwt/qwt_symbol.h"

/**
	@author Konstantin Azadov
	
        ATTRIBUTES handles the available attributes (tree and cell attributes, colors etc.) that the user can select for display in the tree
*/

const QString DEFAULT_COLOR = "#F4F4F4";
const QString BLACK = "#000000";
const QString WHITE = "#FFFFFF";
const QString BLUE = "#0000FF";
const QString GREEN = "#00FF00";
const QString RED = "#FF0000";
const QString YELLOW = "#FFFF00";
const QString MAGENTA = "#FF00FF";
const QString CYAN = "#00FFFF";
const QString DARK_GREEN = "#008000";
const QString GRAY = "#A0A0A4";
const QString DARK_RED = "#800000";
const QString LIGHT_GRAY = "#C0C0C0";

// ColorBrewer qualitative colorscheme Paired-11
// Laura Skylaki

//colorblind-friendly colors
const QString CBPaired_CB_BLACK = "#000000";
const QString CBPaired_CB_ORANGE = "#E69F00";
const QString CBPaired_CB_SKY_BLUE = "#56B4E9";
const QString CBPaired_CB_BLUISH_GREEN = "#009E73";
const QString CBPaired_CB_BLUE = "#0072B2";
const QString CBPaired_CB_VERMILION = "#D55E00";
const QString CBPaired_CB_REDDISH_PURPLE =  "#CC79A7";
const QString CBPaired_CB_YELLOW = "#F0E442";
const QString CBPaired_CB_GREY = "#999999";

//regular colors
const QString CBPaired_LIGHT_BLUE = "#A6CEE3";
const QString CBPaired_DARK_BLUE = "#1F78B4";
const QString CBPaired_LIGHT_GREEN = "#B2DF8A";
const QString CBPaired_DARK_GREEN = "#33A02C";
const QString CBPaired_LIGHT_RED = "#FB9A99";
const QString CBPaired_DARK_RED = "#E31A1C";
const QString CBPaired_LIGHT_ORANGE = "#FDBF6F";
const QString CBPaired_DARK_ORANGE = "#FF7F00";
const QString CBPaired_LIGHT_BLUEPURPLE = "#CAB2D60";
const QString CBPaired_DARK_BLUEPURPLE = "#6A3D9A";
const QString CBPaired_LIGHT_YELLOW = "#FFFF99";



/// the type of relation
const QString REL_Equal = "==";
const QString REL_NonEqual = "!=";
const QString REL_StrictLower = "<";
const QString REL_Lower = "<=";
// const QString REL_StrictGreater = ">";
// const QString REL_Greater = ">=";

const int DEFAULT_TEXT_SIZE = 14;

// unit specifications
const QString US_HOURS = "Hour";
const QString US_MINUTES = "Min";
const QString US_SECONDS = "Sec";
const QString US_PERCENT_OF_LIFETIME = "% of Lifetime";
const QString US_PERCENT_OF_MOTHER_LIFETIME = "% of Mother Lifetime";
const QString US_PERCENT_OF_EXPERIMENT_TIME = "% of Experiment Time";
const QString US_SPEED_PIX_SEC = "Pix/Sec";
const QString US_SPEED_PIX_HOUR = "Pix/Hour";
const QString US_SPEED_UM_SEC = "Um/Sec";
const QString US_SPEED_UM_HOUR = "Um/Hour";
const QString US_DISTANCE_OLD = "Pix";
const QString US_DISTANCE_NEW = "Um";
const QString US_ANGLE = "Degrees";
const QString US_GENERATION = "Generation";
const QString US_AMT = "AMT";
const QString US_CLT = "CLT";
const QString US_NAN = "NaN";

// occurrence specification
const QString FIRST_OCCURRENCE = "First Occurrence";
const QString LAST_OCCURRENCE = "Last Occurrence";

// symbol styles for plot
const QString RECTANGLE = "Rectangle";
const QString TRIANGLE = "Triangle";
const QString STAR = "Star";
const QString ELLIPSE = "Ellipse";


class AttributeInfo {

public:
	///@param _relation: current relation (==, !=, <, <=,)
	///@return: next relation (==, !=, <, <=,)
	static QString nextRelationRight(QString _relation);
	
	///@param _relation: current relation (<, <=)
	///@return: next relation (<, <=)
	static QString nextRelationLeft(QString _relation);

    /**
     * returns next symbol style name for plot
     */
    static QString nextPlotSymbolStyleName(QString _symbolStyle);

    /**
     * returns plot symbol style from the name
     */
    static QwtSymbol::Style getPlotSymbolStyle(QString _symbolStyle);

    /**
     * returns the name of plot symbol style
     */
    static QString getPlotSymbolStyleName( QwtSymbol::Style _style);

	///@return: names of considered tree attributes
	static QVector<QString> getNamesOfTreeAttributes();

	/// @return: rgb codes for standard colors
	static QVector<QString> getNamesOfStandardColors();

	static QColor getColor(int index);

	static QString stringToHtml(const QString& _st);

private:
	
	AttributeInfo();

	static QVector<QString> standardColors;
	static QVector<QString> initStandardColors();
};

#endif

