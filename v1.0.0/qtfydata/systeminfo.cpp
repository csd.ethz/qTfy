/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


// PROJECT
#include "systeminfo.h"
//#include "windowstools.h"
// #include "userinfo.h"
#include "qtfybackend/tttmanager.h"

//QT
#include <QTextStream>
#include <QDesktopServices>
#include <QDateTime>
#include <QApplication>

#ifdef Q_WS_WIN
// Windows
#include <Windows.h>

// Pointer to GetLogicalProcessorInformation API
typedef BOOL (WINAPI *LPFN_GLPI)(PSYSTEM_LOGICAL_PROCESSOR_INFORMATION, PDWORD);
#endif

// -----Oliver-----
// popen is called _popen in MS Visual Studio
#ifdef Q_WS_WIN
        #define popen _popen
#endif

//initialize static variables
QFile SystemInfo::file ("~/test");
QTextStream SystemInfo::out (&SystemInfo::file);

SystemInfo::SystemInfo()
{
}


SystemInfo::~SystemInfo()
{
}
//
//unsigned int SystemInfo::getFreeRAM (char _unit)
//{
//#ifdef Q_WS_WIN
//	unsigned long long freeRamInBytes = WindowsTools::getFreePhysicalRam();
//
//	switch (_unit) {
//		case 'b':
//			return freeRamInBytes;
//		case 'k':
//			return freeRamInBytes / 1024;
//		case 'm':
//			return freeRamInBytes / (1024*1024);
//		case 'g':
//			return freeRamInBytes / (1024*1024*1024);
//		default:
//			return freeRamInBytes;
//	}
//#else
//	//read the virtual file /proc/meminfo
//	//-> all memory information is in there
//	
//	std::ifstream reader ("/proc/meminfo", std::ios_base::in);
//	
//	if (! reader)
//		return -1;
//	
//	reader.seekg (0, std::ios_base::beg);
//	
//	//4 values are together the total free space:
//	//MemFree + Buffers + Cached + SwapFree
//	//SwapFree is included only if the parameter _withSwap is true
//	
//	char tmp = ' ';
//	int counter = 0;
//	QString denotifier;
//	QString stringValue;
//	int value = 0;
//	int freeRAM = 0;
//	
//	while (! reader.eof()) {
//		//reader >> denotifier >> value >> std::endl;
//		
//		counter = 0;
//		denotifier = "";
//		stringValue = "";
//		while ((tmp != ':') & (counter++ < 20)) {
//			reader.read (&tmp, 1);
//			denotifier += tmp;
//		}
//		//counter = 0;
//		while ((tmp != 'k') & (counter++ < 27)) {
//			reader.read (&tmp, 1);
//			if (tmp >= '0' && tmp <= '9')
//				stringValue += tmp;
//		}
//		value = stringValue.toInt();
//		
//		if (denotifier.find ("MemFree") > 0)
//			freeRAM += value;
//		if (denotifier.find ("Buffers") > 0)
//			freeRAM += value;
//		if (denotifier.find ("Cached") > 0)
//			freeRAM += value;
//		if (denotifier.find ("SwapFree") > 0)
//			if (_withSwap)
//				freeRAM += value;
//		
//	}
//	
//	reader.close();
//	
//	switch (_unit) {
//		case 'b':
//			return freeRAM * 1024;
//		case 'k':
//			return freeRAM;
//		case 'm':
//			return freeRAM / 1024;
//		case 'g':
//			return freeRAM / 1048576;
//		default:
//			return freeRAM;
//	}
//#endif
//}
//
//bool SystemInfo::shortOfMemory()
//{
//#ifdef Q_WS_WIN
//	unsigned int physicalMemory = WindowsTools::getFreePhysicalRam() / (1024*1024),
//		virtualMemory = WindowsTools::getFreeVirtualRam() / (1024*1024);
//	
//	return (physicalMemory < MINIMUM_RAM) || (virtualMemory < MINIMUM_RAM);
//#else
//	return getFreeRAM('m') < MINIMUM_RAM;
//#endif
//}

bool SystemInfo::execute (QString _command, QString &_output)
{	
	FILE *fp;
	char line [130];
	_command += " 2>&1";
	fp = popen (_command.toAscii(), "r");

	if (! fp) {
		return false;
	}
	else {
		while (fgets (line, sizeof line, fp)) {
			_output.append ( QString (line));
		}
		return true;
	}
}

QString SystemInfo::homeDirectory()
{
	QString tmpHP = QDir::homePath();
	if (tmpHP.right (1) == "/")
		return tmpHP;
	else
		return tmpHP + "/";
}

	
void SystemInfo::report (QString _text, QString _filename, bool _append)
{
	QFile lf (_filename);
	if (_append) {
		if (! lf.open (QIODevice::WriteOnly | QIODevice::Append))
			return;		//opening the file for output failed
	}
	else {
		if (! lf.open (QIODevice::WriteOnly))
			return;		//opening the file for output failed
	}
	
        QTextStream ts (&lf);
	
	ts << _text;
}

void SystemInfo::setReportStream (QString _filename)
{
	file.setFileName (_filename);
	
	if (! file.open (QIODevice::WriteOnly | QIODevice::Append))
		return;		//opening the file for output failed
	
	out.setDevice (&file);
}


QVector<QString> SystemInfo::listFiles (QString _directory, QDir::Filters _filter, QString _nameFilter, bool _recursive)
{
    QVector<QString> fileArray;
	
	if (_directory.isEmpty() || (_directory.right (2) == "..") || (_directory.right (1) == "."))
		return fileArray;
		
	QDir dir (_directory);
	if (_recursive) 
		dir.setFilter(QDir::AllDirs | QDir::Files);
	else 
		dir.setFilter(QDir::Dirs | QDir::Files);
	
	if (! dir.exists())
		return fileArray;

	if (! _recursive)
		_filter = QDir::Files;
	
    QFileInfoList list;
	if (_nameFilter.isEmpty())
        list = dir.entryInfoList (_filter, QDir::DirsFirst | QDir::Name);
	else{
		QStringList qsl(_nameFilter);
		list = dir.entryInfoList (qsl, _filter, QDir::DirsFirst | QDir::Name);
	}
    if (list.isEmpty())
		return fileArray;

    QList<QFileInfo>::const_iterator iter = list.constBegin();
    QFileInfo info;

    while (iter != list.constEnd()) {
        info = *iter;
	
        if (info.isDir() & (_filter != QDir::Dirs)) {
		//entry is a directory - call function recursively (only if files are desired!)
            QVector<QString> sub = listFiles (info.filePath(), _filter, _nameFilter);
		
			//add all entries in the subdirectory in the current array
			for (QVector<QString>::Iterator iter = sub.begin(); iter != sub.end(); ++iter) {
				fileArray.push_back (*iter);
			}
		}
		else {
			//entry is a ttt file - add it to the list
			QString entry = info.absoluteFilePath();
			fileArray.push_back (entry);
	
		}
	
		++iter;
	}
	
	return fileArray;
}


QString SystemInfo::getTTTFileFolderFromBaseName (const QString &_baseName, bool _createFolder, bool _useAlternativePath)
{
	///@note assumption:
	///the basename is of the format (year:4)(month:2)(day:2)(usersign)(setup number)_p(position)
	///e.g. 20100322BS1_p001
	
	//BS 2009/12/21 new: take the user set directory instead of the default one
	QString tttfilesFolderPath = TTTManager::getInst().getTTTFilesFolderPath();
	QString folder = "";
	
	//check for TTTfiles folder
	QDir tttfileDir(tttfilesFolderPath);

	if (!tttfileDir.exists())
		return folder;
	
	//bool newFolderStructure = UserInfo::getInst().getStyleSheet().getUseNewTTTFileFolderStructure();
	bool newFolderStructure = USE_NEW_TTTFILE_FOLDER_SYSTEM;
	if (_useAlternativePath)
		newFolderStructure = ! newFolderStructure;
		
	if (newFolderStructure) {
		//structure (new):
		//.../TTTexport/TTTfiles/(year)/(experiment)/(position)/(files)
		
		//@WARNING DO NOT INCLUDE USER SIGN (OTHERWISE EACH PERSON COULD OPEN ONLY HIS PERSONAL TREES - NOT SUITABLE!)
		//QString us = UserInfo::getInst().getUsersign();
		
		QString year = _baseName.left (4);			//note: not the current year, but the one of the experiment!
		//BS 2010/03/22: in some experiments, the year has only two digits: thus we have test, if the year starts with "0" or "1"
		//note: there will definitely be a problem in 2020...
		if ((year.left (1) == "0") || (year.left (1) == "1")) {
			//year has only two digits - fill it with "20" and dismiss the month
			year = "20" + year.left (2);
		}
		
		QString basename_without_pos = _baseName.left (_baseName.lastIndexOf (POSITION_MARKER));
		
		folder = tttfileDir.absolutePath() + "/" + year + "/" + basename_without_pos + "/" + _baseName + "/";
		if (checkNcreateDirectory (folder, true, true))
			return folder;
		else
			return "";
		
	}
	else {
		//structure (old):
		//.../TTTexport/TTTfiles/(pos.name)/(files)
		folder = tttfileDir.absolutePath() + "/" + _baseName + "/";
		
		if ((_createFolder) & (! tttfileDir.cd (_baseName))) {
			tttfileDir.mkdir (_baseName);
		}
		
		return folder;
	}
}

bool SystemInfo::checkNcreateDirectory (const QString &_dirName, bool _create, bool _recursive)
{
	if (_dirName.isEmpty())
		return false;
	
	QDir dir (_dirName);
	
	if (! _create)
		return dir.exists();
	else {
		if (dir.exists())
			return true;
		else {
			//create directory
			//note: mkDir is not static, but nonetheless it accepts absolute paths as well
			
			if (! _recursive)
				return dir.mkdir (_dirName);
			else {
				//create a path of directories (depth >= 1)
				//for this we need to go up until the first existing directory and then create the cascade recursively
				
				//NOTE this only works with absolute paths - thus, if the current one is not absolute (starts with a "/"), nothing is done
				if (QDir::isRelativePath (_dirName))
					return false;
				
				QString new_dirname = _dirName;
				QString nextDir = "";
				
				//level up to first existing
				while (! dir.exists()) {
					//cut off last directory
					int posSlash = new_dirname.lastIndexOf ("/");
					nextDir = new_dirname.mid (posSlash + 1);
					new_dirname = new_dirname.left (posSlash);
					dir = new_dirname;
				}
				
				if (! nextDir.isEmpty()) {
					//create next one
					dir.mkdir (nextDir);
					
					//recursive call - this call will go up one level less than now => thus it terminates
					return checkNcreateDirectory (_dirName, true, true);
				}
				else
					return true;
			}
		}
	}
}

bool SystemInfo::checkNcreateDirectory (QDir &_currentDir, const QString &_dirName, bool _create, bool _recursive)
{
	return checkNcreateDirectory (_currentDir.absolutePath() + "/" + _dirName, _create, _recursive);
}

const QString SystemInfo::getExecutablePath()
{
	if (! qApp)
		return "";
	else
		return qApp->applicationDirPath();
}

QDateTime SystemInfo::getSystemTime()
{
	return QDateTime::currentDateTime();
}

QString SystemInfo::getSystemDateAsString (const QString &_format)
{
	return QDateTime::currentDateTime().toString (_format);
}



int SystemInfo::getNumPhysicalCores()
{
#ifdef Q_WS_WIN

	LPFN_GLPI glpi;
	BOOL done = FALSE;
	PSYSTEM_LOGICAL_PROCESSOR_INFORMATION buffer = NULL;
	PSYSTEM_LOGICAL_PROCESSOR_INFORMATION ptr = NULL;
	DWORD returnLength = 0;
	DWORD processorCoreCount = 0;
	DWORD byteOffset = 0;
	PCACHE_DESCRIPTOR Cache;

	// Get GetLogicalProcessorInformation() function pointer
	glpi = (LPFN_GLPI) GetProcAddress(
		GetModuleHandle(TEXT("kernel32")),
		"GetLogicalProcessorInformation");
	if (glpi == NULL) 
		return -1;

	// Call api
	while (!done)
	{
		DWORD rc = glpi(buffer, &returnLength);

		if (rc == FALSE) 
		{
			if (GetLastError() == ERROR_INSUFFICIENT_BUFFER) 
			{
				if (buffer) 
					free(buffer);

				buffer = (PSYSTEM_LOGICAL_PROCESSOR_INFORMATION)malloc(returnLength);

				if (NULL == buffer) 
					return -1;
			} 
			else 
			{
				return -1;
			}
		} 
		else
		{
			done = TRUE;
		}
	}

	// Count physical cores
	ptr = buffer;
	while (byteOffset + sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION) <= returnLength) 
	{
		switch (ptr->Relationship) 
		{
		case RelationProcessorCore:
			processorCoreCount++;
			break;
		default:
			break;
		}

		byteOffset += sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION);
		ptr++;
	}


	free(buffer);

	return processorCoreCount;

#else
	return QThread::idealThreadCount();
#endif
}
