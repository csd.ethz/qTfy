/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/// TTTStats only

#ifndef EXPERIMENT_H
#define EXPERIMENT_H

#include <QVector>
#include <QString>


/**
        @author Konstantin

        This class manages the experiment-specific global attributes of TTT.
        All experiment global attributes are in TTTManager.
*/
class Experiment
{
public:
        Experiment();

        void setUseNewPositions(bool _set)
        {
              ifNewPositions = _set;
        }
        bool getUseNewPositions()
        {
                return ifNewPositions;
        }

        void setPreincubationTimeInSeconds(int _set)
        {
                preincubationTime = _set;
        }

        int getPreincubationTimeInSeconds()
        {
                return preincubationTime;
        }

		void setPositionManagerIndices(QVector<QString> _pmIndices)
		{
			pmIndices = _pmIndices;
		}

		QVector<QString> getPositionManagerIndices()
		{
			return pmIndices;
		}

		static void setUsePreincubationTime(bool usePreincTime)
		{
			Experiment::usePreincubationTime = usePreincTime;
		}

		static bool isUsePreincubationTime()
		{
			return Experiment::usePreincubationTime;
		}

private:
        bool ifNewPositions;

        int preincubationTime;

		// indices of position managers that belong to this experiment
		QVector<QString> pmIndices;

		static bool usePreincubationTime;
};

#endif // EXPERIMENT_H
