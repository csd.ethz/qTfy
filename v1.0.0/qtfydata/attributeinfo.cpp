/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "attributeinfo.h"

#include "qtfybackend/tools.h"

#include "treewrapper.h"
#include "cellwrapper.h"
#include "treestructuralhelper.h"


AttributeInfo::AttributeInfo() {

}

QVector<QString> AttributeInfo::standardColors = initStandardColors();


QVector<QString> AttributeInfo::getNamesOfStandardColors()
{
	return standardColors;
}

QVector<QString> AttributeInfo::initStandardColors()
{
	QVector<QString> namesOfStandardColors;

	//First colors to display, colorblind-friendly colors (9)
	namesOfStandardColors.push_back(CBPaired_CB_BLACK);
	namesOfStandardColors.push_back(CBPaired_CB_ORANGE);
	namesOfStandardColors.push_back(CBPaired_CB_SKY_BLUE);
	namesOfStandardColors.push_back(CBPaired_CB_BLUISH_GREEN);
	namesOfStandardColors.push_back(CBPaired_CB_BLUE);
	namesOfStandardColors.push_back(CBPaired_CB_VERMILION);
	namesOfStandardColors.push_back(CBPaired_CB_REDDISH_PURPLE);
	namesOfStandardColors.push_back(CBPaired_CB_YELLOW);
	namesOfStandardColors.push_back(CBPaired_CB_GREY);

	//Then regular colors (11)
	namesOfStandardColors.push_back(CBPaired_LIGHT_BLUE);
	namesOfStandardColors.push_back(CBPaired_DARK_BLUE);
	namesOfStandardColors.push_back(CBPaired_LIGHT_GREEN);
	namesOfStandardColors.push_back(CBPaired_LIGHT_YELLOW);
	namesOfStandardColors.push_back(CBPaired_LIGHT_RED);
	namesOfStandardColors.push_back(CBPaired_DARK_RED);
	namesOfStandardColors.push_back(CBPaired_LIGHT_ORANGE);
	namesOfStandardColors.push_back(CBPaired_DARK_ORANGE);
	namesOfStandardColors.push_back(CBPaired_LIGHT_BLUEPURPLE);
	namesOfStandardColors.push_back(CBPaired_DARK_BLUEPURPLE);
	namesOfStandardColors.push_back(MAGENTA);

	return namesOfStandardColors;
}

QString AttributeInfo::nextRelationRight(QString _relation)
{
	QList<QString> tmpL;
	
	tmpL.push_back (REL_Equal);
	tmpL.push_back (REL_NonEqual);
	tmpL.push_back (REL_StrictLower);
	tmpL.push_back (REL_Lower);
// 	tmpL.push_back (REL_StrictGreater);
// 	tmpL.push_back (REL_Greater);	
	
    int ind = tmpL.indexOf (_relation);

    if ( ind < 0 )
            ind = 0;
    else {
            ++ind;

            if ( ind == tmpL.count() )
                    ind = 0;
    }

    return tmpL[ind];

}

QString AttributeInfo::nextRelationLeft(QString _relation)
{
	QList<QString> tmpL;
	
	tmpL.push_back (REL_StrictLower);
	tmpL.push_back (REL_Lower);
	
    int ind = tmpL.indexOf (_relation);

    if ( ind < 0 )
            ind = 0;
    else {
            ++ind;

            if ( ind == tmpL.count() )
                    ind = 0;
    }

    return tmpL[ind];

}

QString AttributeInfo::nextPlotSymbolStyleName(QString _symbolStyle)
{
        QList<QString> symStyles;

        symStyles.push_back (STAR);
        symStyles.push_back (RECTANGLE);
        symStyles.push_back (TRIANGLE);
        symStyles.push_back (ELLIPSE);

        int ind = symStyles.indexOf (_symbolStyle);

        if ( ind < 0 )
                ind = 0;
        else {
                ++ind;

                if ( ind == symStyles.count() )
                        ind = 0;
        }

        return symStyles[ind];
}

QwtSymbol::Style AttributeInfo::getPlotSymbolStyle(QString _symbolStyle)
{
        if ( _symbolStyle == STAR )
                return QwtSymbol::Star1;

        if ( _symbolStyle == RECTANGLE )
                return QwtSymbol::Rect;

        if ( _symbolStyle == TRIANGLE )
                return QwtSymbol::Triangle;

        if ( _symbolStyle == ELLIPSE )
                return QwtSymbol::Ellipse;

        return QwtSymbol::Star1;
}

QString AttributeInfo::getPlotSymbolStyleName( QwtSymbol::Style _style)
{
        if ( _style == QwtSymbol::Star1 )
                return STAR;

        if ( _style == QwtSymbol::Rect )
                return RECTANGLE;

        if ( _style == QwtSymbol::Triangle )
                return TRIANGLE;

        if ( _style == QwtSymbol::Ellipse )
                return ELLIPSE;

        return STAR;
}

QVector<QString> AttributeInfo::getNamesOfTreeAttributes()
{
	QVector<QString> treeAttributes;
	treeAttributes.push_back(TREE_OPT_LIFETIME);
	treeAttributes.push_back(TREE_OPT_START_TP);
	treeAttributes.push_back(TREE_OPT_STOP_TP);
	treeAttributes.push_back(TREE_OPT_NUMBER_OF_CELLS);
    treeAttributes.push_back(TREE_OPT_NUMBER_OF_GENERATIONS);
	
	return treeAttributes;
}


QColor AttributeInfo::getColor(int i)
{
	if ( i < standardColors.size() )
		return QColor(standardColors[i]);
	else {
		int r = (rand()%256)+1;
		int g = (rand()%256)+1;
		int b = (rand()%256)+1;
		return QColor(r,g,b);
	}
}

QString AttributeInfo::stringToHtml(const QString& _st)
{
	QString htmlString = "";

	if ( _st == "\n" )
		htmlString = "<br>";
	if ( _st == REL_StrictLower )
		htmlString = "&lt;";

	if ( _st == REL_Lower )
		htmlString = "&le;";

	return htmlString;
}
