/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TRACKPOINT_H
#define TRACKPOINT_H

// Project
#include "cellproperties.h"
#include "qtfybackend/itrackpoint.h"
#include "qtfybackend/itrack.h"

// QT
#include <QPointF>
#include <QVariant>

// STL
#include <math.h>

// Forward declarations
class Track;


const QString OPT_X = "X";
const QString OPT_Y = "Y";
const QString OPT_Z = "Z";
const QString OPT_XBACKGROUND = "XBackground";
const QString OPT_YBACKGROUND = "YBackground";
const QString OPT_TISSUETYPE = "TissueType";
const QString OPT_GENERALTYPE = "GeneralType";
const QString OPT_LINEAGE = "Lineage";
const QString OPT_NONADHERENT = "NonAdherent";
const QString OPT_FREEFLOATING = "FreeFloating";
const QString OPT_ADHERENCE = "AdherenceStatus";
const QString OPT_SEMIADHERENCE = "SemiAdherenceStatus";
const QString OPT_ADH_SPY_FREE_STATE = "AdhSpyFree_State";
const QString OPT_CELLDIAMETER = "CellDiameter";
const QString OPT_ENDOMITOSIS = "EndoMitosis";
const QString OPT_WAVELENGTH_X = "Wavelength";
const QString OPT_ADDON1 = "AddOn1";
const QString OPT_POSITION = "Position";
const QString OPT_TIMEPOINT = "Timepoint";

///adherence state
enum ADHERENCE_STATE {	AS_ADHERENT = 0,
			AS_SPYHOP = 1,
			AS_FREEFLOATING = 2,
			AS_NONE = -1};


///these values denote the bitpacks of the additional attribute
const short int ENVIRONMENT_NUCLEAR = 0;		//Annemjike
const short int ENVIRONMENT_CELL = 1;			//Erin

// class TrackPoint;
// const TrackPoint dummyTrackPoint;


/**
	TrackPoint contains all data that belong to one trackpoint
*/


class TrackPoint : public ITrackPoint 
{
public:

	//static const TrackPoint dummyTrackPoint;
	
	//NOTE:
	//it is very important to initialize ALL attributes!
	
	/**
	 * constructs a default trackpoint, detectable via TimePoint == -1
	 */
	TrackPoint () : TimePoint (-1), X (-1.0f), Y (-1.0f), Z (0.0f),
					XBackground (-1.0f), YBackground (-1.0f), tissueType (0),
					cellGeneralType (0), cellLineage (0), NonAdherent (0),
                                        FreeFloating (0), CellDiameter (0.0f), EndoMitosis (0), AddOn1 (0),
					Position (""), tmpCellNumber (0), tmpColonyNumber (0), tmpPositionIndex (0), track(0), confidence(-1.0f)
		{
			for (int i = 0; i <= MAX_WAVE_LENGTH; i++) 
				WaveLength[i] = 0;
		}
	
	/**
	 * Constructs a copy of _tp
	 * @param _tp the TrackPoint to be copied
	 */
	TrackPoint (const TrackPoint &_tp)
	{
		*this = _tp;
	}
	
	/**
         * @return the x/y pair of the track point as QPointF object
	 */
        QPointF point() const
                {return QPointF (X, Y);}
	
	/**
         * @return the x/y pair of the background point as QPointF object
	 */
        QPointF backgroundPoint() const
                {return QPointF (XBackground, YBackground);}
	
	/**
	 * calculates the distance to another trackpoint
	 * @param _tp the trackpoint to which the distance should be calculated
	 * @return the euclidian distance in pixels or micrometers (depending on the experiment)
	 */
	float distanceTo (const TrackPoint &_tp) const
		{
			return sqrt ( (_tp.X - X) * (_tp.X - X) + (_tp.Y - Y) * (_tp.Y - Y) );
		}
	
	/**
	 * Returns the value of the additional attribute which can take arbitrary settings
	 * It is split into packs of 4-bit, the first one has index 0, the last one has index 7
         * This is valid as sizeof(int) == 4, which is default for gcc.
	 * Thus, every pack can take 16 values
	 * @param _bitpack the index of the bit pack (0 - 7)
	 * @return the value encoded in the addon attribute (between 0 and 15)
	 */
	short int getAddOn1 (short int _bitpack) const {
		return ((AddOn1 & (15 << _bitpack * 4)) >> _bitpack * 4);
	}

	// Same as above but for the ITrackPoint interface!
	short int getAddonBits(int bitback) const {
		return getAddOn1((short int)bitback);
	}
	
	/**
	 * Sets the value for the specified bitpack in the additional attribute.
	 * @see getAddOn1 for further information.
	 * @param _bitpack the index of the bitpack (0 - 7)
	 * @param _value the value to be set (0 - 15)
	 */
	void setAddOn1 (short int _bitpack, short int _value) {
		AddOn1 |= (_value << _bitpack * 4);
	}
	
	
	/**
	 * Assigns this trackpoint from another one
	 * @param _tp the trackpoint to be copied
	 * @return this object
	 */
	TrackPoint& operator=(const TrackPoint &_tp)
	{
		if (this != &_tp) {
			TimePoint = _tp.TimePoint;
			X = _tp.X;
			Y = _tp.Y;
			Z = _tp.Z;
			XBackground = _tp.XBackground;
			YBackground = _tp.YBackground;
			tissueType = _tp.tissueType;
			cellGeneralType = _tp.cellGeneralType;
			cellLineage = _tp.cellLineage;
			NonAdherent = _tp.NonAdherent;
			FreeFloating = _tp.FreeFloating;
                        CellDiameter = _tp.CellDiameter;
			EndoMitosis = _tp.EndoMitosis;
			for (int i = 0; i <= MAX_WAVE_LENGTH; i++) 
				WaveLength [i] = _tp.WaveLength [i];
			AddOn1 = _tp.AddOn1;
			Position = _tp.Position;
			
			tmpCellNumber = _tp.tmpCellNumber;
			tmpColonyNumber = _tp.tmpColonyNumber;
			tmpPositionIndex = _tp.tmpPositionIndex;

			confidence = _tp.confidence;
		}
		return *this;
	}
	
	/**
	 * returns whether this trackpoint is smaller in relation to timepoint than another one
	 * must be implemented to be able to sort according to timepoint
	 * @param _tp the trackpoint to which this is compared
	 * @return true, if this' timepoint is lower than _tp's timepoint
	 */
	bool operator< (const TrackPoint &_tp)
	{
		return (this->TimePoint < _tp.TimePoint);
	}
	
	/**
	 * Conversion method
	 * @return a CellProperties object with the settings stored here
	 */
	CellProperties getPropObject() const
	{
		CellProperties tmpCP;
		tmpCP.Z = Z;
		tmpCP.XBackground = XBackground;
		tmpCP.YBackground = YBackground;
		for (int k = 0; k < 7 /*TissueType.count*/; k++)
			if ((int)tissueType == TissueType(k)) {
				tmpCP.tissueType = TissueType (k);
				break;
			}
		for (int k = 0; k < 9 /*CellGeneralTpe.count*/; k++)
			if ((int)cellGeneralType == CellGeneralType(k)) {
				tmpCP.cellGeneralType = CellGeneralType (k);
				break;
			}
		tmpCP.cellLineage = cellLineage;
		tmpCP.NonAdherent = NonAdherent;
		tmpCP.FreeFloating = FreeFloating;
                tmpCP.CellDiameter = CellDiameter;
		tmpCP.EndoMitosis = EndoMitosis;
		for (int i = 0; i <= MAX_WAVE_LENGTH; i++)
			tmpCP.WaveLength [i] = WaveLength [i];
		tmpCP.AddOn1 = AddOn1;
		tmpCP.Position = Position;
		
		return tmpCP;
	}
	
	/**
	 * Returns any desired value.
	 * note: the return value has to be converted to the corresponding type!
	 * @param _name the name of the attribute (cf. constants above)
	 * @param _select additional parameter e.g. for specifying the wavelength
	 * @return the value associated with _name
	 */
	int getValue (QString _name, int _select = -1) const
	{
		if (_name == OPT_X)
			return (float)X;
		else if (_name == OPT_Y)
			return (float)Y;
		else if (_name == OPT_XBACKGROUND)
			return (int)YBackground;
		else if (_name == OPT_YBACKGROUND)
			return (int)XBackground;
		else if (_name == OPT_Z)
			return (int)Z;
		else if (_name == OPT_TISSUETYPE)
			return (int)tissueType;
		else if (_name == OPT_GENERALTYPE)
			return (int)cellGeneralType;
		else if (_name == OPT_LINEAGE)
			return (int)cellLineage;
		else if (_name == OPT_NONADHERENT)
			return NonAdherent;
		else if (_name == OPT_FREEFLOATING)
			return FreeFloating;
		else if (_name == OPT_ADHERENCE) {
			int tmpAttribute = 0;
			if ((NonAdherent == 0) & (FreeFloating == 0))
				tmpAttribute = 100;				//adherent
/*			else if (NonAdherent == 1 & FreeFloating == 0)
				tmpAttribute = 2;				//spyhop
			else if (NonAdherent == 1 & FreeFloating == 1)
				tmpAttribute = 3;				//free floating*/
			
			return tmpAttribute;
		}
		else if (_name == OPT_SEMIADHERENCE) {
			int tmpAttribute = 0;
			if ((NonAdherent == 0) & (FreeFloating == 0))
				tmpAttribute = 0;				//adherent
			else if ((NonAdherent == 1) & (FreeFloating == 0))
				tmpAttribute = 100;				//spyhop
			else if ((NonAdherent == 1) & (FreeFloating == 1))
				tmpAttribute = 0;				//free floating*/
			
			return tmpAttribute;
		}
		else if (_name == OPT_ADH_SPY_FREE_STATE) {
			int tmpAttribute = 0;
			if ((NonAdherent == 0) & (FreeFloating == 0))
				tmpAttribute = AS_ADHERENT;				//adherent -> 0
			else if ((NonAdherent == 1) & (FreeFloating == 0))
				tmpAttribute = AS_SPYHOP;				//spyhop -> 1
			else if ((NonAdherent == 1) & (FreeFloating == 1))
				tmpAttribute = AS_FREEFLOATING;				//free floating -> 2
			else if ((NonAdherent == 0) & (FreeFloating == 1))
				tmpAttribute = AS_NONE;				//undefined -> -1
			
			return tmpAttribute;
		}
                else if (_name == OPT_CELLDIAMETER) {
                        if (CellDiameter <= 0.0f)
				return 25;
			else
                                return (int)CellDiameter;
		}
		else if (_name == OPT_ENDOMITOSIS)
			return EndoMitosis;
		else if (_name == OPT_WAVELENGTH_X) {
			if ((_select >= 0 )& (_select <= MAX_WAVE_LENGTH))
				return WaveLength [_select];
		}
		else if (_name == OPT_ADDON1) {
			if ((_select >= 0) & (_select <= 7)) {
				int tmpAttribute = getAddOn1 ((short int)_select);
				return tmpAttribute;
			}
		}
		else if (_name == OPT_POSITION) {
			return Position.toInt();
		}

		else if (_name == OPT_TIMEPOINT) {
			return (int)getTimePoint();
		}
		
		qDebug() << "Warning: Unknown trackpoint property requested: " + _name;
		return 0;
	}
	
	/**
	 * @return whether the trackpoint was set
	 */
	inline bool isSet() const
	{
		return (TimePoint > -1);
	}

	~TrackPoint()
	{
		//qDebug() << "Destroying trackpoint at " << TimePoint;
	}
	
	//general:
	//short int     consumes 2 bytes (gcc)
	//(long) int    consumes 4 bytes (gcc)
	//char 		consumes 1 byte (gcc)
	//bool 		consumes 1 byte (gcc)
	//			=> in VB its only 2 bytes, so int is used
	//			=> differences in load/save!!
	//short int is also used for saving boolean values to have unique identifieres for them
	// (0 == false, > 0 == true)
	
	//NOTE: it is important that TimePoint is the FIRST attribute, as qHeapSort() just sorts according to the first entry
	
	int TimePoint;			///the timepoint (is NOT equal to the index in its containing array!)
/*	short int X;			///the x position relative to top left corner of the complete image
	short int Y;			///...z position
	short int Z;			//not yet used
	short int XBackground;		///the background x position (for fluorescence measurement)
	short int YBackground;		///...y position*/

///due to micrometer positions since file version 15, short int is not enough
///from version 16 on: all coordinates are floats!
	
	float X;				///the x position relative to top left corner of the complete image
	float Y;				///...z position
	float Z;				//not yet used
	float XBackground;		///the background x position (for fluorescence measurement)
	float YBackground;		///...y position
	
	///TissueType:
	/// 1 == blood;		2 == stroma;		3 == endothelium;
	/// 4 == heart;		5 == nerve;		6 == endoderm;
	char tissueType;		///the tissue type (blood, nerve, ...)
	char cellGeneralType;		///the general cell type (myeloid, ...)
	char cellLineage;		///the lineage choice of the cell (its final fate)
	
	
	///(both boolean)
	///4 combinations:
	///nonadherent = false & freefloating = false: cell is adherent
	///nonadherent = true & freefloating = false: cell is spyhop
	///nonadherent = false & freefloating = true: does not exist (spyhop)
	///nonadherent = true & freefloating = true: cell is free floating
	short int NonAdherent;		///whether the cell is adherent to others
	short int FreeFloating;		///whether the cell is free floating across the stroma
					///NonAdherent == false && FreeFloating == false => normal cell
					///NonAdherent == true && FreeFloating == true => loosely attached / spyhop
	
        float CellDiameter;		///the diameter of the cell (in micrometer or pixel, dependent on the current experiment)
	
	short int EndoMitosis;		///whether the cell is in endomitosis stadium
	
	///(boolean) contains whether the cell is visible in the wavelength[]
	short int WaveLength[MAX_WAVE_LENGTH + 1];
	
	///additional attribute storing 32 bits (as long is 4 byte)
	///it is split into packages of 4 bit, i.e. 8 packs
	///(bits counting from the right, i.e. bit 0 is the rightmost)
	///access functions are getAddOn1() and setAddOn1()
	///
	///currently included
	///- bitpack 0: Annemjike's wish to distinguish nuclear/cytoplasmic environment (bits 0-3) => 16 values
	///- bitpack 1: Erin's desire to track cell environments (bits 4-7) => 16 values
	///
	///unused: bitpacks 2-7
        //BS 2010/08/10: changed to int due to file reading problems (long is 8-byte on 64-bit systems)
        int AddOn1;
	
	
	///new in file version 17:
	///the position in which the trackpoint was set
	///needs to be stored in the ttt files to be able to reliably deduct the x/y position of a trackpoint in overlapping regions
	///the position is stored with its unique index (usually three or four digits)
	QString Position;
	
	
	///not stored attributes (and only temporarly used)
	uint tmpCellNumber;
	uint tmpColonyNumber;
	uint tmpPositionIndex;

	// Track
	Track* track;

	// Only for trees that have been generated from autotracking: confidence level (see getConfidenceLevel() for explanation)
	float confidence;

	//////////////////////////////////////////////////////////////////////////
	// ITrackPoint interface
	//////////////////////////////////////////////////////////////////////////
	
	/**
	 * @return x coordinate in global micrometer coordinates
	 */
	float getX() const {
		return X;
	}

	/**
	 * @return y coordinate in global micrometer coordinates
	 */
	float getY() const {
		return Y;
	}

	/**
	 * @return timepoint
	 */
	int getTimePoint() const {
		return TimePoint;
	}

	/**
	 * check for wavelength
	 * @param _waveLength the wavelength
	 * @return true if wavelength is set 
	 */
	bool checkForWavelength(int _waveLength) const {
		return WaveLength[_waveLength] != 0;
	}

	/**
	 * @return track this trackpoint belongs to
	 */
	ITrack* getITrack() const;

	/**
	 * Only for autotracking results:
	 * @return confidence level of this trackpoint, i.e. a value in [0,1] or -1 if not available
	 */
	float getConfidenceLevel() const {
		return confidence;
	}

	/**
	 * Return the number of the position where this trackpoint has been set
	 * @return number of position or -1 if not available
	 */
	int getPositionNumber() const {
		// If we have a position index string, convert it to int
		int posInt = -1;
		if(!Position.isEmpty()) {
			bool ok;
			posInt = Position.toInt(&ok);
			if(!ok)
				posInt = -1;
		}

		return posInt;
	}

	/**
	 * Returns the value of the requested trackpoint property. Only
	 * values returned by getAvailableProperties
	 */
	QVariant getTPProperty(QString propertyName) const
	{
		return getValue(propertyName);
	}

	/**
	 * Returns all available trackpoint properties
	 */
	QVector<QString> getAvailableProperties() const
	{
		QVector<QString> props;
		props.append(OPT_FREEFLOATING);
		props.append(OPT_SEMIADHERENCE);
		props.append(OPT_ADHERENCE);
		return props;
	}
};

#endif
