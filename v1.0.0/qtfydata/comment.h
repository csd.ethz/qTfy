/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef COMMENT_H
#define COMMENT_H


#include <qstring.h>

///the timepoint separation expression in sprintf() format
// /each comment is flanked by twice its timepoint in this format
///file version #12
//const QString SEPARATOR = "::%05d::";
const QString SEPARATOR_BEGIN = "TIMEPOINT %05d:";
const QString SEPARATOR_END =   "TIMEPOINT ";

/**
@author Bernhard
	
	A class for storing comments in an efficient way
	Provides methods for handling timepoint specific comments
*/

class Comment{
public:
	/**
	 * constructs an empty comment
	 */
	Comment() : comment ("") {};
	
	~Comment();
	
	/**
	 * returns the complete comment
	 * equivalent to get()
	 * @return a QString that contains the complete comment for all timepoints
	 */
	QString toString() const
		{return comment;}
	
	/**
	 * sets the complete comment
	 * @param _comment the comment for all timepoints as QString
	 */
	void set (QString _comment) 
		{comment = _comment;}
	
	/**
	 * returns the complete comment
	 * equivalent to toString()
	 * @return a QString that contains the complete comment for all timepoints
	 */
	QString get() const
		{return comment;}
	
	/**
	 * returns the comment for the desired timepoint
	 * @param _timePoint the timepoint for which the comment should be read
	 * @return a QString object containing only the comment at _timePoint
	 */
	QString getComment (int _timePoint) const;
	
	/**
	 * inserts the comment for _timePoint; if it already exists, the old one is replaced
	 * @param _timePoint the timepoint for which the commment should be set
	 * @param _comment the comment as QString
	 */
	void setComment (int _timePoint, QString _comment);
	
private:

	///the complete comment
	QString comment;
	
	/**
	 * returns the position of the comment at _timePoint
	 * the return values are stored in _start and _stop
	 * the returned string is the timepoint expression for _timePoint, according to the set pattern (see above)
	 * @param _timePoint the timepoint which should be read
	 * @param _start reference parameter which is set to the beginning of the timepoint comment within the complete comment
	 * @param _stop reference parameter which is set to the end of the timepoint comment within the complete comment
	 * @return a QString object that is SEPARATOR_BEGIN with %1 replaced by the timepoint
	 */
	QString getPosition (int _timePoint, int &_start, int &_stop) const;
};

#endif
