/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "comment.h"


Comment::~Comment()
{
}

QString Comment::getPosition (int _timePoint, int &_start, int &_stop) const
{
	QString query = QString().sprintf (SEPARATOR_BEGIN.toStdString().c_str(), _timePoint);
	_start = comment.indexOf (query);
	_stop = comment.indexOf (SEPARATOR_END, _start + 1);
	return query;
}

QString Comment::getComment (int _timePoint) const
{
	int f, l;
	
	if (comment.isEmpty() | (_timePoint == 0))
		return "";
	
	//set start and end position of the comment
	getPosition (_timePoint, f, l);
	
	if (f > -1) {
		f += SEPARATOR_BEGIN.length();
		if (l > 0)
			return comment.mid (f+1, l - f - 1);
		else
			return comment.mid (f+1);
	}
	else
		return "";
	
}

void Comment::setComment (int _timePoint, QString _comment)
{
	int f, l;
	
	//see if a comment for this timepoint already exists
	QString tp = getPosition (_timePoint, f, l);
	QString add = tp + _comment; // + tp;
	
	if (f < 0) {
		//add new at the end
		comment += add;
	}
	else {
		//change existing
		if (l < 0)
			comment.replace (f, f+add.length(), add);
		else
			comment.replace (f, l-f + tp.length(), add);
	}
}

