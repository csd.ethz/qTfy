/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @authors Bernhard Schauberger
 * @modifiedby Stavroula Skylaki
 * @datemodified 07/10/2015
 */


#ifndef WAVELENGTHINFORMATION_H
#define WAVELENGTHINFORMATION_H

#include <QSize>
#include <QString>

///the following 6 values tell how many micrometers there are per pixel for the different settings

///the smallest possible value (in setting: TV adapter = 1.0, objective = 20, scan = 3x)
///NOTE now (2010/02/25) not anymore the smallest, but this base value is used nonetheless
///the unit is Angstroom / pixel to have int values; for micrometers just divide by 10,000
const int BASE_ANGSTROOM_PER_PIXEL = 1075;

///there are 42 different values: (2 TV adapters) * (3 objectives) * (7 scan/binning settings) = 42
///the TV adapters used are 1.0 and 0.63 (the higher, the less micrometer per pixel)
///the objectives used are 5,10 and 20 (the higher, the less micrometer per pixel)
///the scan/binnings used are scan: 1x,2x,3x and bin: 2x2, 3x3, 4x4, 5x5 (scanning decreases micrometer per pixel, binning increases)
///
///all values are lineary dependent on the base values
///the values are the same for both width and height in all settings
///
///BS 2010/02/25: new objective magnification factors (40x, 63x, 100x)
///
///the measure table as provided by Michael and Andrea on 2008/03/31
///unit is micrometer per pixel
///
///(1x scan)
///TV / objective    5x               10x               20x          40x         63x         100x
///------------------------------------------------------------------------------------------------
///0.5               2.58             1.29              0.645        0.3225      0.20476     0.129
///1.0               1.29             0.645             0.3225       0.16125     0.10238     0.0645
///------------------------------------------------------------------------------------------------
///0.63              -> calculated lineary
///
///the values (um/pixel) for the different scanning/binning settings, given only for the setting TV adapter = 0.5, objective = 5x
///note: the picture size in pixel is independent from the tv/ocular settings!
///
///scan/bin          value            picture size in pixel
///---------------------------------------------------------
///1x scan           2.58             1388x1040
///2x scan           1.29             2776x2080
///3x scan           0.86             4164x3120
///
///2x2 bin           5.16             694x520
///3x3 bin           7.74             462x346
///4x4 bin           10.32            346x260
///5x5 bin           12.9             276x208
///
///
///all micrometer per pixel values refer to both x and y axis


///the base size in pixel (scan factor = 1.0); independent of obejctive/TV adapter settings!
const int BASE_PIXEL_WIDTH = 1388;
const int BASE_PIXEL_HEIGHT = 1040;



/**
	@author Bernhard
	
	This class stores the common information about all wavelengths, obtained by reading the xml file output of TAT.
	If this is not available, at least the sizes are determined by reading the first picture of each wavelength (of any position).
*/

class WavelengthInformation{

public:
	
	/**
	 * default constructor, all values are -1 => uninitialized
	 */
	WavelengthInformation();
	
	/**
	 * constructs a new WavelengthInformation object with the provided data
	 * @param _wavelength the wavelength index
	 * @param _width the width (in pixel)
	 * @param _height the height (in pixel)
	 * @param _comment the comment to it
	 */
	WavelengthInformation (int _wavelength, int _width, int _height, const QString& _comment = "");
	
	~WavelengthInformation();

	/**
	 * @return the number id of the wavelength
	 */
	int getWavelengthNumber() const;
	
	/**
	 * @return true, if this object is initialized (wavelength != -1)
	 */
	bool isInitialized() const;
	
	/**
	 * @return the width of the wavelength (in pixel)
	 */
	int getWidth() const;
	
	/**
	 * @return the height of the wavelength (in pixel)
	 */
	int getHeight() const;
	
	/**
	 * @return the size of the wavelength (both x/y in pixel)
	 */
	QSize getSize() const;
	
	/**
	 * returns the micrometer per pixel, given the settings for this wavelength \n
	 * @return the conversion factor for pixel -> micrometer for this wavelength
	 */
	float getMicrometerPerPixel() const;

	/**
	 * @return the comment for this wavelength
	 */
	QString getComment() const;
	
	/**
	 * returns the micrometer per pixel for the provided settings \n
	 * if -1 is set for any parameter, its 'default' value is assumed
	 * @param _ocularFactor the ocular factor (either 5, 10 or 20); default: 5
	 * @param _tvAdapter the TV adapter (either 0.63 or 1.0); default: 1.0
	 * @param _scanBin the scanning/binning value (1,2,3 for scanning, 4,9,16,25 for binning); default: 1
	 * @return the conversion factor for pixel -> micrometer
	 */
	static float getMicrometerPerPixel (int _ocularFactor, float _tvAdapter, int _scanBin);
	
	/**
	 * calculates the scanning/binning from the provided picture size
	 * @param _width the picture width
	 * @return the scanning/binning factor which can be used for other purposes in the program
	 */
	 //* @param _height the picture height
	static int getScanFromSize (int _width); //, int _height);
	
private:
	
	///the wavelength index (usually, 0 is phase contrast, all above are fluorescent)
	int wavelength;
	
	///the width of the wavelength (in pixel)
	int width;
	
	///the height of the wavelength (in pixel)
	int height;

	// Comment
	QString comment;
	
	///the scanning/binning factor
	///set in the constructor
	int scanBinFactor;
};


inline int WavelengthInformation::getWavelengthNumber() const
{
	return wavelength;
}

inline bool WavelengthInformation::isInitialized() const
{
	return (wavelength != -1);
}

inline int WavelengthInformation::getWidth() const
{
	return width;
}


inline int WavelengthInformation::getHeight() const
{
	return height;
}

inline QSize WavelengthInformation::getSize() const
{
	return QSize (width, height);
}

#endif
