/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// PROJECT
#include "treestructuralhelper.h"
#include "qtfydata/track.h"
#include "qtfybackend/itree.h"

/**
* @param _track for which the progeny has to be calculated
* @param _firstGeneration the lower (inclusive) bound of relative generations to be considered
* @param _lastGeneration the upper (inclusive) bound of relative generations to be considered
* @return all progeny tracks of the given _track between _firstGeneration and _secondGeneration
*
* progeny of cell n in generation x after cell are cells [n * pow(2, x), .., (n + 1) * pow(2, i) - 1]
*/
QVector<ITrack*> TreeStructuralHelper::getProgenyInGenerations(const ITrack* _track, int _firstGen, int _lastGen)
{
	// The result vector
	QVector<ITrack*> result;

	// Iterator over the generations and check if there is a valid track
	int n = _track->getTrackNumber();
	ITree* tree = _track->getITree();
	for (int i = _firstGen; i <= _lastGen; i++) {
		for (int trackNumber = n * pow(2.0, i); trackNumber <= (n + 1)*pow(2.0, i) - 1; ++trackNumber) {
			ITrack* tmpTrack = tree->getTrackByNumber(trackNumber);

			if (tmpTrack)
				result.append(tmpTrack);
		}
	}

	// Return the result
	return result;
}

/**
* @param _track for which the ancestors have to be calculated
* @param _firstGeneration the lower (inclusive) bound of relative generations to be considered
* @param _lastGeneration the upper (inclusive) bound of relative generations to be considered
* @return all ancestors of the given _track between _firstGeneration and _lastGeneration
*
* ancestor of cell n in generation x before cell is n / pow(2,x) (rounded down, nach unten gerundet)
*/
QVector<ITrack*> TreeStructuralHelper::getAncestorsInGenerations(const ITrack* _track, int _firstGen, int _lastGen)
{
	QVector<ITrack*> result;

	// Iterator over the generations and check if there is a valid track
	int n = _track->getTrackNumber();
	ITree* tree = _track->getITree();
	for ( int i = _firstGen; i <= _lastGen; i++ ) {
		int trackNumber = n / pow(2.0, i);
		ITrack* tmpTrack = tree->getTrackByNumber(trackNumber);

		if (tmpTrack)
			result.append(tmpTrack);
	}

	return result;
}

/**
* @param _firstGeneration the lower (inclusive) bound of absolute generations to be considered
* @param _lastGeneration the upper (inclusive) bound of absolute generations to be considered
* @return all tracks which are in generation [_firstGeneration, _secondGeneration}
*/
QVector<ITrack*> TreeStructuralHelper::getTracksInGenerations(const ITree* _tree, int _firstGeneration, int _lastGeneration)
{
	const ITrack* root = _tree->getRootNode();

	// If there is no track, return an empty vector
	if( !root )
		return QVector<ITrack*>(0);

	// Else use the getProgenyInGenerations method
	return getProgenyInGenerations(root, _firstGeneration, _lastGeneration);
}


bool TreeStructuralHelper::isCellOfType (const ITrack* _track, const CellType _cellType)
{
	if (_cellType == CT_DIFFERENTSISTERSTOPREASONTYPE) 
	{
		QVector<ITrack*> mothers = getAncestorsInGenerations(_track, 1, 1);
		if (mothers.size() != 1)
			return false;

		ITrack* mother = mothers.at(0);
		return (mother->getChild1()->getStopReason() != mother->getChild2()->getStopReason());
	}

	switch (_track->getStopReason()) {
		case TS_DIVISION:
			if (_cellType == CT_DIVIDINGTYPE)
				return true;
			
			if (_track->getChild1() && _track->getChild2()) {
				//this track has both children
				ITrack* child1 = _track->getChild1();
				ITrack* child2 = _track->getChild2();

				if ((_cellType == CT_DIVIDINGCHILDRENTYPE) &&
					(child1->getStopReason() == TS_DIVISION) &&
					(child2->getStopReason() == TS_DIVISION))
					return true;
				
				if ((_cellType == CT_DIFFERENTCHILDRENSTOPREASONTYPE) &&
				    (child1->getStopReason() != child2->getStopReason()))
					return true;
				
				if ((_cellType == CT_NOLOSTCHILDRENTYPE) &&
				    (child1->getStopReason() != TS_LOST) &&
				    (child2->getStopReason() != TS_LOST))
				    return true;
			}
			
			return false;
		case TS_APOPTOSIS:
			return (_cellType == CT_APOPTOTICTYPE);
			break;
		case TS_LOST:
			return (_cellType == CT_LOSTTYPE);
			break;
		case TS_NONE:
			return (_cellType == CT_INTERRUPTEDTYPE);
			break;
		default:
			;
	}
	
	return (_cellType == CT_NOCELLTYPE);
}


long TreeStructuralHelper::getCellTypes(const ITree* _tree)
{
	ITrack* root = _tree->getRootNode();
	if (!root)
		return CT_NOCELLTYPE;
	
	return getCellTypesRecursive (root, CT_NOCELLTYPE);
}

long TreeStructuralHelper::getCellTypesRecursive(const ITrack *_track, long _result)
{
	if (! _track)
		return _result;
	
	for (long ct = CT_DIVIDINGTYPE; ct <= CT_NOLOSTCHILDRENTYPE; ct *= 2) {
		CellType cellType = (CellType)ct;
		if (isCellOfType(_track, cellType))
			_result |= ct;
	}
	
	_result |= getCellTypesRecursive (_track->getChild1(), _result);
	_result |= getCellTypesRecursive (_track->getChild2(), _result);
	
	return _result;
}

// Returns the sister of the given cell
ITrack* TreeStructuralHelper::getSister(const ITrack* cell)
{
	ITrack* mother = cell->getMother();
	if(!mother)
		return 0;

	if(mother->getChild1() == cell)
		return mother->getChild2();
	else
		return mother->getChild1();
}

int TreeStructuralHelper::getLastTreeTimePoint(const ITree* tree)
{
	int lastTP = 0;
	int trackCount = tree->getMaxTrackNumber();
	for(int i = 1; i <= trackCount; i++) 
	{
		ITrack *track = tree->getTrackByNumber(i);
		if(!track) continue;

		if(track->getLastTimePoint() > lastTP)
			lastTP = track->getLastTimePoint();
	}

	return lastTP;
}