/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TREEWRAPPER_H
#define TREEWRAPPER_H

#include "qtfybackend/itree.h"
#include "qtfybackend/itrack.h"
#include "qtfydata/tree.h"

// Forward declarations
class TTTPositionManager;
class CellWrapper;
class Experiment;
class CellLineageTree;

/**
 * This is a wrapper class for trees from ttt to implement the 
 * getTreeProperty method of the ITree interface and e.g. manage everything
 * related to PositionManager, TreeAttributes, ... . Only the ITree
 * interface is exported, but this maybe change in the future. 
 *
 * It allows the attributes of stats to be very general and not specific 
 * programmed against the ttt tracks.
 *
 * This Wrapper also guarantees that only tracks wrapped with
 * CellWrapper are returned. Therefore it implements a cache of
 * wrapped Tracks.
 *
 * This class takes the ownership of the given tree object. So it
 * is responsible for deletion!
 */
class TreeWrapper : public ITree
{

public:
	// Constructor with a ttt tree
	TreeWrapper(Tree &_tree);

	// Destructor, deletes the ttt tree
	~TreeWrapper();

	// Copy Constructor
	TreeWrapper(const TreeWrapper & wrapper) :
		tree(wrapper.tree) 
	{
		qDebug() << "TreeWrapper wird kopiert!";
	}

	// Methods of the itree interface, forwarded to the original tree
	// and if required a wrapped track is returned.
	virtual ITrack* getRootNode() const { 
		return getWrappedTrack(tree.getRootNode()->getNumber()); 
	}
	
	virtual ITrack* getTrackByNumber(int _number) const { 
		return getWrappedTrack(_number); 
	}

	virtual QVector<ITrack*> getTracksInTree() const {
		return tree.getTracksInTree();
	}
	
	virtual int getMaxTrackNumber() const { 
		return tree.getMaxTrackNumber(); 
	}
	
	virtual int getNumberOfTracks() const { 
		return tree.getNumberOfTracks(); 
	}
	
	virtual QString getTreeName() const { 
		return tree.getTreeName(); 
	}

	virtual QString getExperimentName() const { 
		return tree.getExperimentName(); 
	}
	
	virtual bool getModified() const { 
		return tree.getModified(); 
	}
	
	virtual QString getFilename() const { 
		return tree.getFilename(); 
	}
	
	virtual QList<ITrackPoint*> getTrackPointsAtTimePoint(int _timePoint) const { 
		return tree.getTrackPointsAtTimePoint(_timePoint); 
	}

	virtual QString getPositionIndex() const { 
		return tree.getPositionIndex();
	}

	
	/**
	 * Getter for CellLineageTree
	 */
	virtual CellLineageTree* getCellLineageTree() const{
		return tree.getCellLineageTree();
	}

	/**
	 * To add more properties
	 */
	virtual QVariant getTreeProperty(QString _propName) const;
	virtual QVector<QString> getAvailableProperties() const;

	/**
	 * Getter for track and tttpm.
	 * Only until the code is changed everywhere
	 */
	Tree* getRealTree() const { return &tree; }
	TTTPositionManager* getRealTTTPM() const { return tttpm; }

	// Allow cellWrapper to access the cached wrappedTracks
	friend class CellWrapper;

private:

	// Returns a wrapped track for the real track with number _num.
	// Used internally a cache to always return the same object!
	ITrack* getWrappedTrack(int _num) const;

	// The tree which is wrapped
	Tree &tree;

	// The positionManager responsible for this tree
	TTTPositionManager *tttpm;

	// The experiment of this tree
	Experiment* experiment;

	// The cache for wrapped tracks
	mutable QHash<int, CellWrapper*> wrappedTracks;
};

#endif // TREEWRAPPER_H
