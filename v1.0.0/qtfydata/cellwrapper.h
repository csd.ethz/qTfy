/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CELLWRAPPER_H
#define CELLWRAPPER_H

#include "qtfybackend/itrack.h"
#include "qtfydata/track.h"
#include "qtfybackend/tttmanager.h"

#include "treewrapper.h"


/**
 * This is a wrapper class for tracks from ttt to implement the 
 * getCellProperty method of the ITrack interface and e.g. manage everything
 * related to PositionManager, CellAttributes, ... . Only the Itrack
 * interface is exported, but this maybe change in the future. 
 *
 * It allows the attributes of stats to be very general and not specific 
 * programmed against the ttt tracks.
 *
 * Also it ensures that only wrapped tracks are returned. For this it
 * uses the wrappedTrack cache of treeWrapper.
 */
class CellWrapper : public ITrack
{

public:
	// Constructor with a ttt track
	CellWrapper(Track &_track, TreeWrapper &_treeWrapper);

	// Destructor, deletes the ttt track
	~CellWrapper();

	// Methods of the itrack interface, forwarded to the original track
	// and if required return a wrapped version if the track
	virtual ITrack *getChild1() const 
	{	
		if(track.getChild1() == 0) return 0;
		return tree.getWrappedTrack(track.getChild1()->getNumber()); 
	}
	virtual ITrack *getChild2() const 
	{
		if(track.getChild2() == 0) return 0;
		return tree.getWrappedTrack(track.getChild2()->getNumber()); 
	}
	virtual bool hasChildren() const 
	{
		if(track.getChild2() == 0 && track.getChild1() == 0) return false;
		return true; 
	}

	virtual ITrack *getMother() const
	{
		if(track.getMother() == 0) return 0;
		return tree.getWrappedTrack(track.getMother()->getNumber()); 
	}
	virtual int getFirstTimePoint() const { return track.getFirstTimePoint(); }
	virtual int getLastTimePoint() const { return track.getLastTimePoint(); }
	virtual ITrackPoint* getTrackPointByTimePoint(int _timePoint) const { return track.getTrackPointByTimePoint(_timePoint); }
	virtual QMap<int, ITrackPoint*> getTrackPointsRange(int _startTP = -1, int _endTP = -1) const { return track.getTrackPointsRange(_startTP, _endTP); }
	virtual int getTrackNumber() const { return track.getTrackNumber(); }
	virtual TrackStopReason getStopReason() const { return track.getStopReason(); }
	virtual ITree* getITree() const { return &tree; }
	virtual int getSecondsForTimePoint(int timepoint) const { 
		return tttpm->getFiles()->calcSeconds(track.getFirstTimePoint(), timepoint);
	}
	virtual int getAbsoluteSecondsForTP(int timePoint) const {
		//return tttpm->getFiles()->calcSeconds(1, timePoint);//maybe the first timepoint is not the the timepoint 1, doesn't work always ::1st try
		//return tttpm->getFiles()->calcSeconds(0, timePoint); //sets first timepoint to be timepoint 0 , doesn't work always ::2nd try

		//test1 for finding the tree's first timepoint
		ITree* mtree = track.getITree();
		ITrack* motherTrack = mtree->getRootNode();
		return tttpm->getFiles()->calcSeconds(motherTrack->getFirstTimePoint(), timePoint);

	}

	/**
	 * To add more properties
	 */
	virtual QVariant getTrackProperty(QString _propName) const;
	virtual QVector<QString> getAvailableProperties() const;

	/**
	 * Getter for track and tttpm.
	 * Only until the code is changed everywhere
	 */
	Track* getRealTrack() const { return &track; }
	TTTPositionManager* getRealTTTPM() const { return tttpm; }

private:
	// The track which is wrapped
	Track &track;

	// The positionManager responsible for this track
	TTTPositionManager *tttpm;

	// The wrapped tree for this track
	TreeWrapper &tree;

	// List with attributes which are always available
	static QVector<QString> alwaysAvailableAttributes;
	// Used to init alwaysAvailableAttributes
	static QVector<QString> initAvailableAttributes();
};

#endif // CELLWRAPPER_H
