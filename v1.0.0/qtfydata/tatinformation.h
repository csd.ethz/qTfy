/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @authors Bernhard Schauberger, Konstantin Azadov
 * @modifiedby Stavroula Skylaki
 * @datemodified 07/10/2015
 */

#ifndef TATINFORMATION_H
#define TATINFORMATION_H


// PROJECT
#include "qtfydata/positioninformation.h"
#include "qtfydata/wavelengthinformation.h"
#include "qtfydata/cellsandconditions.h"

//QT
#include <QVector>
#include <QRect>
#include <QPoint>
#include <QPointF>
#include <QString>



class TATXMLParser;

///dummy wl info, necessary for functions returng references
const WavelengthInformation DUMMYWAVELENGTHINFORMATION;


/**
	@author Bernhard
	
	This class stores the annotation/metadata that the researcher has entered in TAT before movie acquisition.
	The class TATXMLParser is a friend.
*/

class TATInformation{
public:
	
	static TATInformation *inst;
	
	/**
	 * @return the static instance of the TATInformation class
	 */
	static TATInformation* getInst();
	
	
	TATInformation()
		: xmlFilename (""), positionCount (0), wavelengthCount (0), timepointCount (0), interval (0), useBTPS (false),
		  ocularFactor (0.0f), tvFactor (0.0f), microscopeFactorsSet (false), wavelengthInfoRead (false), cellsAndConditionsInfoRead(false)
	{
			coordinateTop = 0;
			coordinateLeft = 0;
			coordinateBottom = 0;
			coordinateRight = 0;
			microMeterPerPixel = 0.0;
	}
	
	~TATInformation()
		{}
	
	/**
	 * @return a string representation of the current instance
	 */
	const QString toString();
	
	/**
	 * sets the information for the specified wavelength
	 * @param _wavelength the wavelength
	 * @param _wlInfo the wavelength info object 
	 */
	 void setWavelengthInfo (int _wavelength, const WavelengthInformation &_wlInfo);
	
	/**
	 * returns the information object for the specified wavelength
	 * @param _wavelength the wavelength for which the information is desired
	 * @return a WavelengthInformation object containing all relevant data for the specified wavelength
	 */
	 const WavelengthInformation& getWavelengthInfo (int _wavelength);

	/**
	 * returns whether a valid wavelengthinformation object exists for _wavelength
	 * @param  _wavelength the wavelength
	 * @return true if so
	 */
	bool wavelengthInformationExistsFor(int _wavelength);

	/**
	 * returns all the available wavelengthinformation objects for all wavelengths of TAT xml
	 * @return the vector with the wavelength information
	 */
	const QVector<WavelengthInformation> getAvailableWavelengthInfo ()
	{
		return wavelengthsInfo;
	}
	
	/**
	 * sets the bounds of the coordinate system; obtained from the TAT xml file
	 * note: no care is taken whether the coordinate system is normal or inverted
	 * note: the size of a picture has to be added to one of the corner values
	 * @param _left the leftmost value
	 * @param _top the topmost value
	 * @param _right the rightmost value
	 * @param _bottom the bottommost value
	 */
	void setCoordinateBounds (float _left, float _top, float _right, float _bottom);
	
	/**
	 * returns the bounds of the coordinate system; obtained from the TAT xml file
	 * note: no care is taken whether the coordinate system is normal or inverted
	 * note: the size of a picture has to be added to one of the corner values
	 * @return a QRect instance with the four values left,top,right,bottom
	 */
	QRect getCoordinateBounds() const;
	
	int getOcularFactor() const;
	
	float getTVAdapterFactor() const;
	
	bool isMicroscopeFactorSet() const;
	
	
	void setXMLFilename(const QString& theValue)
		{xmlFilename = theValue;}
	
	QString getXMLFilename() const
		{return xmlFilename;}
	
	
	/**
	 * subsequent three methods: whether the indicated data section was read
	 */
	bool wavelengthInfoAvailable() const;

	bool cellsAndConditionInfoAvailable() const;
	
	bool positionDataAvailable() const;
	
	bool factorsRead() const;
	
	
	CellsAndConditions getCellsAndConditions()
	{
		return cellsAndConditions;
	}

	// Returns micrometer per pixel that are given directly (i.e. not over tv-factor etc.) in tatexp.xml (or 0.0 if not directly specified)
	double getMicroMeterPerPixelIfDirectlySpecifiedInTatXml() const 
	{
		return microMeterPerPixel;
	}

	// Return if micrometer per pixel have been read from the tat xml (similar to factorsRead())
	bool microMeterPerPixelRead() const 
	{
		return microMeterPerPixel > 0.0;
	}
	
	
	//Return if the experiment has stagger wavelengths
	bool staggerWavelengths();
private:
	
	///the (absolute) filename of the tat xml file from which this configuration was read
	QString xmlFilename;
	
	int positionCount;
	int wavelengthCount;
	int timepointCount;
	int interval;
	bool useBTPS;
	
	bool hasStaggerWls;
	
	QVector<PositionInformation> positionsInfo;

	QVector<WavelengthInformation> wavelengthsInfo;
	
	///these four attributes store the (current) minimum (for left and top) and maximum (for bottom and right) value for 
	///each border (experiment global micrometer), necessary to reformat the coordinate system for display
	float coordinateTop;
	float coordinateLeft;
	float coordinateBottom;
	float coordinateRight;
	
	///the microscope settings
	///these are used as default settings in the startup, yet the user can still override them
	float ocularFactor;
	float tvFactor;

	///in some experiments, micrometer per pixel are given directly (otherwise this has value 0.0)
	double microMeterPerPixel;
	
	///whether the above two values were set
	bool microscopeFactorsSet;
	
	///whether the wavelength information was available in the TAT file
	///this is not the case in older versions
	///if missing, the wavelength sizes have to be read manually
	bool wavelengthInfoRead;
	
	///whether the cells and condition information was available in the TAT file
	///this is not the case in older versions
	///if missing, this information (hoursBeforeMovieStart and so on) should be set manually
	bool cellsAndConditionsInfoRead;
	
	///following flags: whether the indicated section/value was read
	///if this is not the case for at least one of them, a warning should be displayed by the calling program, as this problem is supposed to be critical
	bool positionDataRead;
	int tv_ocular_factorsRead;	//0: none, 1: one, 2: both read; 	thus, only 2 is fine, all the rest is bad
	
	///the cells and condition settings
	CellsAndConditions cellsAndConditions;
	
	friend class TATXMLParser;
	
};

inline QRect TATInformation::getCoordinateBounds() const
{
	return QRect (QPoint ((int)coordinateLeft, (int)coordinateTop), QPoint ((int)coordinateRight, (int)coordinateBottom));
}

inline int TATInformation::getOcularFactor() const
{
	return (int)ocularFactor;
}

inline float TATInformation::getTVAdapterFactor() const
{
	return tvFactor;
}

inline bool TATInformation::isMicroscopeFactorSet() const
{
	return microscopeFactorsSet;
}

inline bool TATInformation::wavelengthInfoAvailable() const
{
	return wavelengthInfoRead;
}

inline bool TATInformation::cellsAndConditionInfoAvailable() const
{
	return cellsAndConditionsInfoRead;
}

inline bool TATInformation::positionDataAvailable() const
{
	return positionDataRead;
}

inline bool TATInformation::factorsRead() const
{
	return (tv_ocular_factorsRead == 2);
}

#endif
