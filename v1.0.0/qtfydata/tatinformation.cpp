/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @authors Bernhard Schauberger, Konstantin Azadov
 * @modifiedby Stavroula Skylaki
 * @datemodified 07/10/2015
 */


#include "tatinformation.h"

TATInformation * TATInformation::inst (0);

TATInformation* TATInformation::getInst()
{
	if (! inst)
		inst = new TATInformation();
	
	return inst;
}

const QString TATInformation::toString()
{
	QString result = "";
	
	result += QString ("pc = %1; ").arg (positionCount);
	result += QString ("wc = %1; ").arg (wavelengthCount);
	result += QString ("tc = %1; ").arg (timepointCount);
	result += QString ("iv = %1; ").arg (interval);
	result += QString ("ub = %1; ").arg (useBTPS);
	
	result += "\n";
	
	//print cells and conditions
	result += cellsAndConditions.toString();
	
	
	return result;
}

void TATInformation::setWavelengthInfo (int _wavelength, const WavelengthInformation &_wlInfo)
{
        if ((_wavelength < 0) | (_wavelength > MAX_WAVE_LENGTH))
		return;
	
	if (_wavelength >= (int)wavelengthsInfo.size())
		wavelengthsInfo.resize (_wavelength + 5);
	
	wavelengthsInfo [_wavelength] = _wlInfo;
}

const WavelengthInformation& TATInformation::getWavelengthInfo (int _wavelength)
{
    if (_wavelength < 0 || _wavelength >= wavelengthsInfo.size())
		return DUMMYWAVELENGTHINFORMATION;
	else
		return wavelengthsInfo [_wavelength];
	
}


void TATInformation::setCoordinateBounds (float _left, float _top, float _right, float _bottom)
{
	coordinateLeft = _left;
	coordinateRight = _right;
	coordinateTop = _top;
	coordinateBottom = _bottom;
}

bool TATInformation::wavelengthInformationExistsFor( int _wavelength )
{
	return _wavelength >= 0 && _wavelength < wavelengthsInfo.size();
}



bool TATInformation::staggerWavelengths()
{
	return hasStaggerWls;
}
