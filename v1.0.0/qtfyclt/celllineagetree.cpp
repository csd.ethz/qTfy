/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "celllineagetree.h"

// Qt
#include <QDebug>
#include <QTemporaryFile>


const char* CellLineageTree::FILE_VERSION = "0.0.4";


const char* CellLineageTree::MIN_FILE_VERSION = "0.0.4";

// Internal constants
const char* CellLineageTree::DATEFORMAT = "dd.MM.yyyy hh:mm:ss";	// Equivalent boost/STL date facet format: "%d.%m.%Y %H:%M:%S"


CellLineageTree::CellLineageTree()
    : m_currentLineNumber(0),
    m_lastError(ErrorNone),
    m_dataChanged(true),
    m_foundUnsupportedChunks(false)
{
	
}

CellLineageTree::CellLineageTree(const QVector<QString>& frameIndexElements)
    : m_currentLineNumber(0),
    m_lastError(ErrorNone),
    m_frameIndexElements(frameIndexElements),
    m_dataChanged(true),
    m_foundUnsupportedChunks(false)
{}

void CellLineageTree::clear()
{
	// Reset all data
	m_foundUnsupportedChunks = false;
	m_dataChanged = false;
	m_frameIndexElements.clear();
	m_currentLineNumber = 0;
	m_trackingDataChunk.cells.clear();
	m_trackingDataChunk.rootCellId = -1;
	m_quantificationChunks.clear();
	m_fileName = "";
	m_fileVersion = "";

	// Reset error
	unsetError();
}

void CellLineageTree::unsetError()
{
	m_lastError = ErrorNone;
	m_lastErrorDescr = "";
}

bool CellLineageTree::setFrameIndexElements(const QVector<QString>& frameIndexElements)
{
	// Check if allowed
	if(!m_frameIndexElements.isEmpty() && m_frameIndexElements.size() != frameIndexElements.size())
		return false;

	m_frameIndexElements = frameIndexElements;
	return true;
}


bool CellLineageTree::getMinMaxTrackingFrameIndex(int cellId, FrameIndex& minFrameIndex, FrameIndex& maxFrameIndex) const
{
	// Check if cell with cellId exists
	const auto itCell = m_trackingDataChunk.cells.find(cellId);
	if(itCell == m_trackingDataChunk.cells.end())
		return false;

	// Get all frame indexes of cell and sort
	QList<FrameIndex> frameIndexes = itCell->dataRows.keys();
	if(frameIndexes.size() == 0)
		return false;
	qSort(frameIndexes);

	// Extract min/max frame index
	minFrameIndex = frameIndexes[0];
	maxFrameIndex = frameIndexes.back();

	return true;
}

bool CellLineageTree::getTrackingDataPoint(int cellId, const FrameIndex& frameIndex, double* x, double* y, double* z /*= nullptr*/) const
{
	assert(x && y);

	// Check if cell with cellId exists
	const auto itCell = m_trackingDataChunk.cells.find(cellId);
	if(itCell == m_trackingDataChunk.cells.end())
		return false;

	// Get data of cell
	if(frameIndex.getDimensionality() != m_frameIndexElements.size())
		return false;
	const auto itCellData = itCell->dataRows.find(frameIndex);
	if(itCellData == itCell->dataRows.end())
		return false;

	// Return data
	*x = itCellData->x;
	*y = itCellData->y;
	if(z)
		*z = itCellData->z;
	return true;
}

int CellLineageTree::getParentCellId(int cellId) const
{
	// Check if cell with cellId exists
	const auto itCell = m_trackingDataChunk.cells.find(cellId);
	if(itCell == m_trackingDataChunk.cells.end())
		return -1;

	// Return parent cell id
	return itCell->parentCellId;
}

void CellLineageTree::getDaughterCellIds(int cellId, int& outDaughterId1, int& outDaughterId2) const
{
	// Check if cell with cellId exists
	const auto itCell = m_trackingDataChunk.cells.find(cellId);
	if(itCell == m_trackingDataChunk.cells.end()) {
		outDaughterId1 = -1;
		outDaughterId2 = -1;

		return;
	}

	// Return daughter ids
	outDaughterId1 = itCell->daughterCellId1;
	outDaughterId2 = itCell->daughterCellId2;
}

QString CellLineageTree::getCellFate(int cellId) const
{
	// Check if cell with cellId exists
	const auto itCell = m_trackingDataChunk.cells.find(cellId);
	if(itCell == m_trackingDataChunk.cells.end())
		return "";

	// Return cell fate
	return itCell->cellFate;
}

bool CellLineageTree::setUseZCoordinates(bool on)
{
	// Check if nothing to do
	if(m_trackingDataChunk.useZCoordinates == on)
		return true;

	// Tree must be empty for this function
	if(!m_trackingDataChunk.cells.isEmpty())
		return false;

	// Change
	m_trackingDataChunk.useZCoordinates = on;

	return true;
}

bool CellLineageTree::insertCell(int cellId, const QString& cellFate, int parentCellId)
{
	// Cell id must be positive
	if(cellId < 0)
		return false;

	// Check if cell already exists
	if(m_trackingDataChunk.cells.contains(cellId))
		return false;

	// Check if cell is to become root but root already exists
	if(m_trackingDataChunk.rootCellId >= 0 && parentCellId < 0)
		return false;

	// Check if parent cell exists and has only one daughter at most
	QHash<int, TrackingDataSingleCell>::iterator parentCell;
	if(parentCellId >= 0) {
		parentCell = m_trackingDataChunk.cells.find(parentCellId);
		if(parentCell == m_trackingDataChunk.cells.end())
			return false;
		if(parentCell->daughterCellId1 >= 0 && parentCell->daughterCellId2 >= 0)
			return false;
	}

	// Everything seems ok, insert cell
	TrackingDataSingleCell& newCell = m_trackingDataChunk.cells[cellId];
	newCell.cellFate = cellFate;
	if(parentCellId >= 0) {
		// Establish tree links
		newCell.parentCellId = parentCellId;
		if(parentCell->daughterCellId1 < 0)
			parentCell->daughterCellId1 = cellId;
		else
			parentCell->daughterCellId2 = cellId;
	}
	else
		// New root
		m_trackingDataChunk.rootCellId = cellId;

	return true;
}

bool CellLineageTree::setCellFate(int cellId, const QString& cellFate)
{
	// Check if cell with cellId exists
	const auto itCell = m_trackingDataChunk.cells.find(cellId);
	if(itCell == m_trackingDataChunk.cells.end())
		return false;

	// Change cell fate
	itCell->cellFate = cellFate;

	return true;
}

bool CellLineageTree::setTrackingDataPoint(int cellId, const FrameIndex& frameIndex, double x, double y, double z)
{
	// Check if cell with cellId exists
	const auto itCell = m_trackingDataChunk.cells.find(cellId);
	if(itCell == m_trackingDataChunk.cells.end())
		return false;

	// Check if index is valid
	if(frameIndex.getDimensionality() != m_frameIndexElements.size())
		return false;

	// Check if z is valid
	if(m_trackingDataChunk.useZCoordinates && z == std::numeric_limits<double>::signaling_NaN())
		return false;

	// Change tracking data
	TrackingDataTrackPoint& trackPoint = itCell->dataRows[frameIndex];
	trackPoint.x = x;
	trackPoint.y = y;
	if(m_trackingDataChunk.useZCoordinates)
		trackPoint.z = z;

	return true;
}

bool CellLineageTree::removeCell(int cellId)
{
	// Check if cell with cellId exists
	auto itCell = m_trackingDataChunk.cells.find(cellId);
	if(itCell == m_trackingDataChunk.cells.end())
		return false;

	// Unlink from tree
	if(itCell->parentCellId >= 0) {
		auto itParentCell = m_trackingDataChunk.cells.find(itCell->parentCellId);
		assert(itParentCell != m_trackingDataChunk.cells.end());
		assert(itParentCell->daughterCellId1 == cellId || itParentCell->daughterCellId2 == cellId);
		if(itParentCell->daughterCellId1 == cellId)
			itParentCell->daughterCellId1 = -1;
		else
			itParentCell->daughterCellId2 = -1;
	}

	// Delete cell and all its children
	QList<int> cellsToDelete;
	cellsToDelete.push_back(cellId);
	while(!cellsToDelete.isEmpty()) {
		int curCell = cellsToDelete.takeLast();
		auto itCurCell = m_trackingDataChunk.cells.find(curCell);
		assert(itCurCell != m_trackingDataChunk.cells.end());

		// Queue daughter cells for deletion
		if(itCurCell->daughterCellId1 >= 0)
			cellsToDelete.push_back(itCurCell->daughterCellId1);
		if(itCurCell->daughterCellId2 >= 0)
			cellsToDelete.push_back(itCurCell->daughterCellId2);

		// Delete cell from tracking data
		m_trackingDataChunk.cells.erase(itCurCell);

		// Delete cell from quantification data
		for(int iQuant = 0; iQuant < m_quantificationChunks.size(); ++iQuant)
			deleteQuantificationCell(iQuant, curCell);
	}

	return true;
}

QVector<QVariant> CellLineageTree::getQuantificationDataPoint(int quantificationChunkIndex, int cellId, const FrameIndex& frameIndex) const
{
	const QuantificationChunk& quantificationChunk = m_quantificationChunks[quantificationChunkIndex];

	// Find cell data
	auto itCell = quantificationChunk.cells.find(cellId);
	if(itCell == quantificationChunk.cells.end())
		return QVector<QVariant>();

	// Find data at frameIndex
	if(frameIndex.getDimensionality() != m_frameIndexElements.size())
		return QVector<QVariant>();
	auto itRow = itCell->dataRows.find(frameIndex);
	if(itRow == itCell->dataRows.end())
		return QVector<QVariant>();
	else
		return *itRow;
}

int CellLineageTree::insertQuantificationChunk()
{
	// Append new chunk
	int newIndex = m_quantificationChunks.size();
	m_quantificationChunks.resize(newIndex + 1);
	return newIndex;
}

bool CellLineageTree::insertQuantificationCell(int quantificationChunkIndex, int cellId)
{
	QuantificationChunk& quantificationChunk = m_quantificationChunks[quantificationChunkIndex];

	// Check if cell exists in tracking data
	if(!m_trackingDataChunk.cells.contains(cellId))
		return false;

	// Check if cell already exists
	if(quantificationChunk.cells.contains(cellId))
		return false;

	// Insert cell
    quantificationChunk.cells.insert(cellId, QuantificationDataSingleCell());

	return true;
}

bool CellLineageTree::deleteQuantificationCell(int quantificationChunkIndex, int cellId)
{
	QuantificationChunk& quantificationChunk = m_quantificationChunks[quantificationChunkIndex];

	// Find cell data
	auto it = quantificationChunk.cells.find(cellId);
	if(it == quantificationChunk.cells.end())
		return false;

	// Delete cell
	quantificationChunk.cells.erase(it);

	return true;
}

bool CellLineageTree::deteleQuantificationDataPoint(int quantificationChunkIndex, int cellId, const FrameIndex& frameIndex)
{
	QuantificationChunk& quantificationChunk = m_quantificationChunks[quantificationChunkIndex];

	// Find cell data
	auto itCell = quantificationChunk.cells.find(cellId);
	if(itCell == quantificationChunk.cells.end())
		return false;

	// Find data at frameIndex
	if(frameIndex.getDimensionality() != m_frameIndexElements.size())
		return false;
	auto itRow = itCell->dataRows.find(frameIndex);
	if(itRow == itCell->dataRows.end())
		return false;

	// Delete data point
	itCell->dataRows.erase(itRow);

	return true;
}

bool CellLineageTree::setQuantificationColumns(int quantificationChunkIndex, const QVector<QString>& columnNames, const QVector<DataType>& columnTypes, const QVector<QString>& columnDescriptions)
{
	QuantificationChunk& quantificationChunk = m_quantificationChunks[quantificationChunkIndex];
	
	// Check if already set
	if(quantificationChunk.dataColumnNames.size())
		return false;

	// Check if valid
	if(columnNames.size() != columnTypes.size())
		return false;

	// Fill descriptions
	QVector<QString> columnDescriptionsProcessed = columnDescriptions;
	columnDescriptionsProcessed.resize(columnNames.size());

	// Change
	try {
		quantificationChunk.dataColumnNames = columnNames;
		quantificationChunk.dataColumnTypes = columnTypes;
		quantificationChunk.dataColumnDescriptions = columnDescriptionsProcessed;
		quantificationChunk.dataColumnTypesQVariant.resize(columnTypes.size());
		for(int i = 0; i < quantificationChunk.dataColumnTypesQVariant.size(); ++i)
			quantificationChunk.dataColumnTypesQVariant[i] = dataTypeToQVariantTypeInternal(columnTypes[i]);
	}
	catch(const InternalException& e) {
		return false;
	}

	return true;
}

bool CellLineageTree::setQuantificationDataPoint(int quantificationChunkIndex, int cellId, const FrameIndex& frameIndex, const QVector<QVariant>& data)
{
	QuantificationChunk& quantificationChunk = m_quantificationChunks[quantificationChunkIndex];

	// Check if data is valid
	if(data.size() != quantificationChunk.dataColumnTypesQVariant.size())
		return false;
	for(int i = 0; i < data.size(); ++i) {
		if(data[i].type() != quantificationChunk.dataColumnTypesQVariant[i]) {
			qDebug() << data[i].type() << " - " << quantificationChunk.dataColumnTypesQVariant[i];
			return false;
		}
	}

	// Find cell data
	auto itCell = quantificationChunk.cells.find(cellId);
	if(itCell == quantificationChunk.cells.end())
		return false;

	// Check if frameIndex is valid
	if(frameIndex.getDimensionality() != m_frameIndexElements.size())
		return false;

	// Change/add data
	itCell->dataRows[frameIndex] = data;

	return true;
}

QVector<QString> CellLineageTree::getQuantificationNames() const
{
	// Get names
	QVector<QString> names;
	names.reserve(m_quantificationChunks.size());
	for(auto it = m_quantificationChunks.begin(); it != m_quantificationChunks.end(); ++it)
		names.push_back(it->metaInformation.name);
	return names;
}

int CellLineageTree::getQuantificationIndexFromName(const QString& name) const
{
	// Map name to (first) index
	for(int i = 0; i < m_quantificationChunks.size(); ++i) {
		if(m_quantificationChunks[i].metaInformation.name == name)
			return i;
	}
	return -1;
}

bool CellLineageTree::addQuantificationColumn(int quantificationChunkIndex, const QString& columnName, DataType columnType, QVariant defaultValue, const QString& columnDescription /*= ""*/)
{
	QuantificationChunk& q = m_quantificationChunks[quantificationChunkIndex];

	// Check if data is ok
	if(columnName.isEmpty())
		return false;
	if(q.cells.size() && (!defaultValue.isValid() || defaultValue.type() != dataTypeToQVariantTypeInternal(columnType)))
		return false;

	// Change columns
	q.dataColumnNames.push_back(columnName);
	q.dataColumnTypes.push_back(columnType);
	q.dataColumnDescriptions.push_back(columnDescription);
	q.dataColumnTypesQVariant.push_back(dataTypeToQVariantTypeInternal(columnType));

	// Append value to each data row
	for(auto itCell = q.cells.begin(); itCell != q.cells.end(); ++itCell) {
		auto& rows = itCell->dataRows;
		for(auto itRow = rows.begin(); itRow != rows.end(); ++itRow)
			itRow->push_back(defaultValue);
	}

	return true;
}





