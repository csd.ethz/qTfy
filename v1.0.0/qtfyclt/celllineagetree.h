/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef lineagetreefile_h__
#define lineagetreefile_h__

// STL
#include <cassert>

// Qt
#include <QString>
#include <QList>
#include <QVariant>
#include <QHash>
#include <QVector>
#include <QDateTime>
#include <QTextStream>
#include <QFile>
#include <QElapsedTimer>


class CellLineageTree {

public:

	/**
	 * A n-dimensional integer index referencing an individual frame. The dimensionality and meaning are set
	 * with CellLineageTree::setFrameIndexElements() and can be accessed with CellLineageTree::getFrameIndexElements().
	 */
	struct FrameIndex {
		QVector<int> index;

		// Constructors
		FrameIndex() 
		{}
		FrameIndex(const FrameIndex& other)
			: index(other.index) 
		{}
		FrameIndex(int dimensionality) 
		{
			assert(dimensionality > 0);
			index.fill(-1, dimensionality);
		}
		FrameIndex(const QVector<int>& _index) 
			: index(_index)
		{
			assert(index.size() > 0);
		}

		// Access element
		int operator[](int i) const 
		{
			assert(i >= 0 && i < index.size());
			return index[i];
		}
		int& operator[](int i) 
		{
			assert(i >= 0 && i < index.size());
			return index[i];
		}

		// Get dimensionality
		int getDimensionality() const
		{
			return index.size();
		}

		// Compare
		bool operator==(const FrameIndex& other) const
		{
			return index == other.index;
		}
		bool operator<(const FrameIndex& other) const 
		{
			for(int dim = 0; dim < index.size() && dim < other.index.size(); ++dim) {
				if(index[dim] < other.index[dim])
					return true;
				else if(index[dim] > other.index[dim])
					return false;
			}
			// Elements are equal until min(index.size(),other.index.size())
			return index.size() < other.index.size();
		}
	};

    /**
     * Types.
     */

    // Data types and their QVariant type representation
    enum DataType {
        Int32,		// QVariant::Int
        Int64,		// QVariant::LongLong
        UInt32,		// QVariant::UInt
        UInt64,		// QVariant::ULongLong
        Bool,		// QVariant::Bool
        Double,		// QVariant::Double
        String,		// QVariant::String
        Date,		// QVariant::Date
        DateTime,	// QVariant::DateTime
        Time,		// QVariant::Time
    };

    // How coordinates are specified
    enum CoordinatesType {
        GlobalMicroMeter,		// In micrometer relative to some global coordinate system
        GlobalPixel,			// In pixels relative to some global coordinate system
        LocalPixel				// In pixels relative to image origin
    };

    // Possible errors
    enum ErrorType {
        ErrorNone = 0,
        ErrorOpen = 1,			// Could not open file
        ErrorCorrupt = 2,		// File structure is invalid
        ErrorCriticalChunk = 3,	// File contains unsupported but critical chunk
        ErrorInternal = 4,		// An internal error occurred
        ErrorWrite = 5,			// Could not save file
        ErrorVersion = 6,		// Minimal required parser version of file is higher than parser version
    };

	// Types, see below for definition
	struct QuantificationChunkMetaInformation;

	/**
	 * General functions.
	 */

	// Constructor: create empty tree or read from file
	CellLineageTree();

	// Constructor: create empty tree with given frame index definition (see setFrameIndexElements())
	CellLineageTree(const QVector<QString>& frameIndexElements);

	// Set frame index definition, i.e. dimensionality (=length of vector) and name of each dimension (=elements of vector). Fails if frame index has already
	// been set (by constructor, or by reading from file or by calling this function) and has different number of dimensions
	bool setFrameIndexElements(const QVector<QString>& frameIndexElements);

	// Get frame index definition, i.e. dimensionality (=length of returned vector) and name of each dimension (=elements of returned vector)
	const QVector<QString>& getFrameIndexElements() const
	{
		return m_frameIndexElements;
	}


	// Get if unsupported but not-critical chunks were encountered (information in these chunks will be lost if the file is saved overwriting the old file)
	bool foundUnrecognizedChunksInFile() const
	{
		return m_foundUnsupportedChunks;
	}

	// Get current file name (i.e. last used for reading or saving)
	const QString& getFileName() const
	{
		return m_fileName;
	}

	// Get information about last error
	bool error() const
	{
		return m_lastError != ErrorNone;
	}
	ErrorType getErrorType() const
	{
		return m_lastError;
	}
	const QString& getErrorDescription() const
	{
		return m_lastErrorDescr;
	}
	void unsetError();

	// Get if tree has been changed since last read or write
	bool changed() const
	{
		return m_dataChanged;
	}

	// Clear tree: removes all tree data and meta information (puts tree in same state as directly after calling constructor with no parameters)
	void clear();


	/**
	 * Access tracking data.
	 */


	// Get if z-coordinates are used
	bool getUseZCoordinates() const
	{
		return m_trackingDataChunk.useZCoordinates;
	}

	// Get coordinates type
	CoordinatesType getCoordinatesType() const
	{
		return m_trackingDataChunk.coordinatesType;
	}

	// Get id of root cell of tree (returns -1 if tree is empty)
	int getRootCellId() const
	{
		return m_trackingDataChunk.rootCellId;
	}

	// Get ids of all cells in tree
	QList<int> getCellIds() const
	{
		return m_trackingDataChunk.cells.keys();
	}

	// Check if cell with provided id exists
	bool getIfCellExists(int cellId) const
	{
		return getCellIds().contains(cellId);
	}

	// Get cell parent id (returns -1 if cell is root or does not exist)
	int getParentCellId(int cellId) const;

	// Get daughter cell ids (stores -1 in both values if cell does not exist or has no children)
	void getDaughterCellIds(int cellId, int& outDaughterId1, int& outDaughterId2) const;

	// Get cell fate (returns empty string if cell fate is not set or if cell does not exist)
	QString getCellFate(int cellId) const;

	// Get all frame indexes with tracking data for specified cell (note: returned list is in random order - to sort elements, use e.g. qSort())
	// Returns empty list if cellId is invalid.
	QList<FrameIndex> getTrackingFrameIndexes(int cellId) const;

	// Get minimal/maximal frame index of cell
	bool getMinMaxTrackingFrameIndex(int cellId, FrameIndex& minFrameIndex, FrameIndex& maxFrameIndex) const;

	// Get tracking data for specific cell and frame, returned in x, y and z (optional), frameIndex corresponds to the columns returned by getFrameIndexElements()
	bool getTrackingDataPoint(int cellId, const FrameIndex& frameIndex, double* x, double* y, double* z = nullptr) const;


	/**
	 * Change tracking data.
	 */

	// Set if z-coordinates are used (fails if tree is not empty, on by default)
	bool setUseZCoordinates(bool on);

	// Set coordinates type (never fails)
	void setCoordinatesType(CoordinatesType coordinatesType)
	{
		m_trackingDataChunk.coordinatesType = coordinatesType;
	}

	// Insert cell, fails if cell with provided id already exists or root cell already exists and parentCellId is not specified or
	// if parentCellId does not exist or if parentCellId already has two daughter cells or if cell id is negative
	bool insertCell(int cellId, const QString& cellFate = "", int parentCellId = -1);

	// Set cell fate, fails if cell does not exist
	bool setCellFate(int cellId, const QString& cellFate);

	// Set tracking data at the specified frame (frameIndex corresponds to the columns returned by getFrameIndexElements()), z must be specified if 
	// z-coordinates are used, otherwise it is ignored.
	bool setTrackingDataPoint(int cellId, const FrameIndex& frameIndex, double x, double y, double z = std::numeric_limits<double>::signaling_NaN());

	// Remove cell (including child cells and quantification)
	bool removeCell(int cellId);

	/**
	 * Access quantification data.
	 */

	// Get number of quantification blocks available for tree
	int getNumberOfQuantificationChunks() const
	{
		return m_quantificationChunks.size();
	}

	// Get names of available quantification chunks (note: it is allowed that names
	// are used twice or are not specified)
	QVector<QString> getQuantificationNames() const;

	// Get index of quantification chunk with provided name (returns -1 if not found)
	// If there are multiple quantification chunks with the provided name, the one
	// with the lowest index is returned
	int getQuantificationIndexFromName(const QString& name) const;

	// Get information about quantification chunk (quantificationChunkIndex must be valid, 
	// otherwise behavior of functions is undefined)
	const QuantificationChunkMetaInformation& getQuantificationMetaInformation(int quantificationChunkIndex) const
	{
		return m_quantificationChunks[quantificationChunkIndex].metaInformation;
	}
	QString getQuantificationName(int quantificationChunkIndex) const
	{
		return m_quantificationChunks[quantificationChunkIndex].metaInformation.name;
	}
	QDateTime getQuantificationDateTimeUTC(int quantificationChunkIndex) const
	{
		return m_quantificationChunks[quantificationChunkIndex].metaInformation.dateTimeUTC;
	}
	QDateTime getQuantificationDateTimeLocal(int quantificationChunkIndex) const
	{
		return m_quantificationChunks[quantificationChunkIndex].metaInformation.dateTimeUTC.toLocalTime();
	}
	QString getQuantificationUserName(int quantificationChunkIndex) const
	{
		return m_quantificationChunks[quantificationChunkIndex].metaInformation.userName;
	}
	QString getQuantificationSoftware(int quantificationChunkIndex) const
	{
		return m_quantificationChunks[quantificationChunkIndex].metaInformation.software;
	}
	QString getQuantificationRemarks(int quantificationChunkIndex) const
	{
		return m_quantificationChunks[quantificationChunkIndex].metaInformation.remarks;
	}

	// Get available columns in quantification chunk and their data types (excluding the segmentation and quantification frame
	// index columns). quantificationChunkIndex must be valid, otherwise behavior of functions is undefined
	QVector<QString> getQuantificationColumnNames(int quantificationChunkIndex) const
	{
		return m_quantificationChunks[quantificationChunkIndex].dataColumnNames;
	}
	QVector<DataType> getQuantificationColumnTypes(int quantificationChunkIndex) const
	{
		return m_quantificationChunks[quantificationChunkIndex].dataColumnTypes;
	}
	QVector<QString> getQuantificationColumnDescriptions(int quantificationChunkIndex) const
	{
		return m_quantificationChunks[quantificationChunkIndex].dataColumnDescriptions;
	}

	// Get all frame indexes with quantification data for specified quantification chunk and cell (note: returned list is in random 
	// order - to sort elements, use e.g. qSort()); quantificationChunkIndex must be valid, otherwise behavior of functions is undefined.
	// Returns empty list if cellId is invalid.
	QList<FrameIndex> getQuantificationFrameIndexes(int quantificationChunkIndex, int cellId) const;

	// Get quantification data for specific cell and frame, frameIndex corresponds to the columns returned by getFrameIndexElements()
	// quantificationChunkIndex must be valid, otherwise behavior of functions is undefined, frameIndex and cellId can be invalid, then an 
	// empty vector is returned.
	QVector<QVariant> getQuantificationDataPoint(int quantificationChunkIndex, int cellId, const FrameIndex& frameIndex) const;

	/**
	 * Change quantification data.
	 */
	
	// Insert quantification chunk, returns index of newly inserted chunk
	int insertQuantificationChunk();

	// Insert cell into quantification chunk
	// quantificationChunkIndex must be valid, otherwise behavior of functions is undefined
	// If cell with provided id already exists, function does nothing and returns false
	// Cell with provided id must exist in tracking data
	bool insertQuantificationCell(int quantificationChunkIndex, int cellId);

	// Delete entire quantification chunk (quantificationChunkIndex must be valid, otherwise behavior is undefined)
	// Note: this function changes the index of each chunk with a higher index than quantificationChunkIndex.
	void deleteQuantificationChunk(int quantificationChunkIndex)
	{
		m_quantificationChunks.remove(quantificationChunkIndex);
	}

	// Delete entire cell from quantification chunk (does not delete quantifications for daughter cells;
	// quantificationChunkIndex must be valid, otherwise behavior is undefined, cellId doesn't have 
	// to be valid). Returns true if and only if a cell with the given id was deleted from the chunk.
	bool deleteQuantificationCell(int quantificationChunkIndex, int cellId);

	// Delete single entry (for specific cell and frame index) from a quantification chunk.
	// (quantificationChunkIndex must be valid, otherwise behavior is undefined, cellId and frameIndex don't have 
	// to be valid). Returns true if and only if a cell with the given id was deleted from the chunk.
	bool deteleQuantificationDataPoint(int quantificationChunkIndex, int cellId, const FrameIndex& frameIndex);

	// Set information about quantification chunk
	// quantificationChunkIndex must be valid, otherwise behavior of functions is undefined
	void setQuantificationMetaInformation(int quantificationChunkIndex, const QuantificationChunkMetaInformation& metaInformation)
	{
		m_quantificationChunks[quantificationChunkIndex].metaInformation = metaInformation;
	}
	void setQuantificationName(int quantificationChunkIndex, const QString& name)
	{
		m_quantificationChunks[quantificationChunkIndex].metaInformation.name = name;
	}
	void setQuantificationDateTimeLocal(int quantificationChunkIndex, const QDateTime& dateTimeLocal)
	{
		m_quantificationChunks[quantificationChunkIndex].metaInformation.dateTimeUTC = dateTimeLocal.toUTC();
	}
	void setQuantificationDateTimeUTC(int quantificationChunkIndex, const QDateTime& dateTimeUTC)
	{
		m_quantificationChunks[quantificationChunkIndex].metaInformation.dateTimeUTC = dateTimeUTC;
	}
	void setQuantificationUserName(int quantificationChunkIndex, const QString& userName)
	{
		m_quantificationChunks[quantificationChunkIndex].metaInformation.userName = userName;
	}
	void setQuantificationSoftware(int quantificationChunkIndex, const QString& software)
	{
		m_quantificationChunks[quantificationChunkIndex].metaInformation.software = software;
	}
	void setQuantificationRemarks(int quantificationChunkIndex, const QString& remarks)
	{
		m_quantificationChunks[quantificationChunkIndex].metaInformation.remarks = remarks;
	}

	// Add column to quantification chunk
	// If chunk is not empty, a default value with proper type must be provided, otherwise the function fails
	// quantificationChunkIndex must be valid, otherwise behavior of functions is undefined
	bool addQuantificationColumn(int quantificationChunkIndex, const QString& columnName, DataType columnType, QVariant defaultValue, const QString& columnDescription = "");

	// Set quantification data column layout (excluding frame index) and types (fails if already set)
	// quantificationChunkIndex must be valid, otherwise behavior of functions is undefined
	bool setQuantificationColumns(int quantificationChunkIndex, const QVector<QString>& columnNames, const QVector<DataType>& columnTypes, const QVector<QString>& columnDescriptions = QVector<QString>());

	// Set quantification data at the specified frame; frameIndex corresponds to the columns returned by getFrameIndexElements().
	// quantificationChunkIndex must be valid, otherwise behavior of functions is undefined.
	// If cell with cellId does not exist function does nothing. If data point at frameIndex does not exist, it is newly inserted.
	bool setQuantificationDataPoint(int quantificationChunkIndex, int cellId, const FrameIndex& frameIndex, const QVector<QVariant>& data);
	
	// Meta information (i.e. information that is not part of the actual data) about a quantification chunk
	struct QuantificationChunkMetaInformation {
		QString name;
		QDateTime dateTimeUTC;
		QString userName;
		QString software;
		QString remarks;

		bool operator==(const QuantificationChunkMetaInformation& o) const 
		{
			return name == o.name &&
				dateTimeUTC == o.dateTimeUTC &&
				userName == o.userName &&
				software == o.software &&
				remarks == o.remarks;
		}
	};

	/**
	 * Convenience functions.
	 */

	// Convert DataType t to corresponding QVariant type (returns QVariant::Invalid if t is invalid)
	QVariant::Type dataTypeToQVariantType(DataType t)  const
	{
		return dataTypeToQVariantTypeInternal(t, false);
	}

	
	/**
	 * Constants.
	 */
	static const char* DATEFORMAT;

private:

	/**
	 * Types.
	 */

	// Internal exception type
	struct InternalException {
		ErrorType errorType;
		QString errorDescr;
		InternalException(ErrorType _errorType, const QString& _descr) 
			: errorType(_errorType), errorDescr(_descr) 
		{}
	};

	// A single track point
	struct TrackingDataTrackPoint {

		// Coordinates
		double x;
		double y;
		double z;

		// Constructor
		TrackingDataTrackPoint(double _x = std::numeric_limits<double>::signaling_NaN(), double _y = std::numeric_limits<double>::signaling_NaN(), double _z = std::numeric_limits<double>::signaling_NaN())
			: x(_x), y(_y), z(_z)
		{}

	};

	// Tracking data for a single cell
	struct TrackingDataSingleCell {

		// Parent cell
		int parentCellId;

		// Daughter cells
		int daughterCellId1;
		int daughterCellId2;

		// Cell fate
		QString cellFate;

		// Cell data (x, y and potentially z) by frame index
		QHash<FrameIndex, TrackingDataTrackPoint> dataRows;

		// Constructor
		TrackingDataSingleCell(int _parentCellId = -1, int _daughterCellId1 = -1, int _daughterCellId2 = -1, const QString& _cellFate = "")
			: parentCellId(_parentCellId),
			daughterCellId1(_daughterCellId1),
			daughterCellId2(_daughterCellId2),
			cellFate(_cellFate)
		{}

	};

	// Tracking data chunk
	struct TrackingDataChunk {

		// Coordinates type
		CoordinatesType coordinatesType;

		// If Z-coordinates are used
		bool useZCoordinates;

		// Id of root cell
		int rootCellId;

		// Single cell data by cell id
		QHash<int, TrackingDataSingleCell> cells;

		// Constructor 
		TrackingDataChunk(CoordinatesType _coordinatesType = LocalPixel, bool _useZCoordinates = true, int _rootCellId = -1)
			: coordinatesType(_coordinatesType),
			useZCoordinates(_useZCoordinates),
			rootCellId(_rootCellId)
		{}
	};

	// Quantification data for a single cell
	struct QuantificationDataSingleCell {

		// Cell data (x, y and potentially z) by frame index
		QHash<FrameIndex, QVector<QVariant>> dataRows;

	};

	// Quantification chunk
	struct QuantificationChunk {

		// Information about chunk
		QuantificationChunkMetaInformation metaInformation;

		// Data column names and types
		QVector<QString> dataColumnNames;
		QVector<DataType> dataColumnTypes;
		QVector<QString> dataColumnDescriptions;

		// Data column QVariant types (corresponds to dataColumnTypes)
		QVector<QVariant::Type> dataColumnTypesQVariant;

		// Single cell quantification data by cell id
		QHash<int, QuantificationDataSingleCell> cells;

	};

	// Convert string key-word to DataType
	QVariant::Type dataTypeToQVariantTypeInternal(DataType t, bool throwIfInvalidParameter = true)  const
	{
		switch(t) {
		case Double:
			return QVariant::Double;
		case Int32:
			return QVariant::Int;
		case Bool:
			return QVariant::Bool;
		case String:
			return QVariant::String;
		case Int64:
			return QVariant::LongLong;
		case UInt32:
			return QVariant::UInt;
		case UInt64:
			return QVariant::ULongLong;
		case DateTime:
			return QVariant::DateTime;
		default: 
			if(throwIfInvalidParameter)
				throw InternalException(ErrorInternal, QString("Invalid DataType %1 in %2 while reading line %3.").arg(t).arg(__FUNCTION__).arg(m_currentLineNumber));
			else
				return QVariant::Invalid;
		}
	}
	
	/**
	 * Variables.
	 */

	// Current file version of API
	static const char* FILE_VERSION;

	// Minimal required version for parsers to read files generated by this API
	static const char* MIN_FILE_VERSION;

	// Parsing helper variables
	int m_currentLineNumber;

	// File name last used for reading/writing
	mutable QString m_fileName;

	// Version of file last used for reading/writing
	mutable QString m_fileVersion;

	// Last error (can be changed by const functions)
	mutable ErrorType m_lastError;
	mutable QString m_lastErrorDescr;

	// Frame index elements
	QVector<QString> m_frameIndexElements;

	// If data was changed since last read or write
	mutable bool m_dataChanged;

	// If unsupported (but not critical) chunks were encountered during last read operation (and tree was not saved in the meantime)
	mutable bool m_foundUnsupportedChunks;

	// Cell chunk
	TrackingDataChunk m_trackingDataChunk;

	// Quantification chunks (ordered according to order of appearance in file)
	QVector<QuantificationChunk> m_quantificationChunks;

	// Friends
	friend unsigned int qHash(const FrameIndex& pi);
};

// Hash a FrameIndex
inline unsigned int qHash(const CellLineageTree::FrameIndex& pi) {
	unsigned int val = 0;
	for(auto it = pi.index.begin(); it != pi.index.end(); ++it)
		val ^= qHash(*it);
	return val;
}

/**
 * Inline member function definitions.
 */
inline QList<CellLineageTree::FrameIndex> CellLineageTree::getTrackingFrameIndexes(int cellId) const
{
	// Check if cell with cellId exists
	const auto itCell = m_trackingDataChunk.cells.find(cellId);
	if(itCell == m_trackingDataChunk.cells.end())
		return QList<CellLineageTree::FrameIndex>();

	// Get all frame indexes
	return itCell->dataRows.keys();
}


inline QList<CellLineageTree::FrameIndex> CellLineageTree::getQuantificationFrameIndexes(int quantificationChunkIndex, int cellId) const
{
	const QuantificationChunk& quantificationChunk = m_quantificationChunks[quantificationChunkIndex];

	// Find cell data
	auto itCell = quantificationChunk.cells.find(cellId);
	if(itCell == quantificationChunk.cells.end())
		return QList<CellLineageTree::FrameIndex>();

	// Return all frame indexes
	return itCell->dataRows.keys();
}


#endif // lineagetreefile_h__
