/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "colortransform.h"

// Qt
#include <QGraphicsScene>
#include <QGraphicsLineItem>
#include <QMouseEvent>
#include <QDebug>

// Project
#include "imagedisplaysettings.h"

const QColor ColorTransform::LINE_COLOR = Qt::darkBlue;
const int ColorTransform::LINE_WIDTH = 2;


ColorTransform::ColorTransform( QWidget* parent /*= 0*/ )
	: QGraphicsView(parent)
{
	// Init variables
	m_mouseShifting = false;

	// Enable anti aliasing
	setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);

	// Use only one scene per view
	setScene(new QGraphicsScene(this));

	// Default settings
	m_blackPoint = 0;
	m_whitePoint = 255;

	// Disable scrollbars
	setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

	// Add elements
	QPen pen(QBrush(LINE_COLOR), LINE_WIDTH);
	pen.setCapStyle(Qt::FlatCap);
	m_leftBar = scene()->addLine(0, 0, 1, 1, pen);
	m_rightBar = scene()->addLine(0, 0, 1, 1, pen);
	pen.setCapStyle(Qt::RoundCap);
	m_centerBar =scene()->addLine(0, 0, 1, 1, pen);

	// Layout correctly
	layoutGraphicsElements();

	// Set cursor
	setCursor(Qt::CrossCursor);
}

void ColorTransform::layoutGraphicsElements()
{
	// Get width and height of drawing area (viewport) 
	float w = viewport()->width(),
		h = viewport()->height();
	float bp = m_blackPoint,
		wp = m_whitePoint;

	// X coordinate of left and right lines
	float x1 = LINE_WIDTH/2 + (bp/255.0f) * w;
	float x2 = (wp/255.0f) * w-LINE_WIDTH/2;

	// Update line items
	m_leftBar->setLine(x1, 0, x1, h);
	m_rightBar->setLine(x2, 0, x2, h);
	m_centerBar->setLine(x1, h, x2, 0);

	setSceneRect(0, 0, w, h);
}

void ColorTransform::resizeEvent( QResizeEvent* e )
{
	// Update graphics layout
	layoutGraphicsElements();
}

void ColorTransform::mousePressEvent( QMouseEvent* e )
{
	if(e->button() == Qt::LeftButton) {
		m_mouseShifting = true;

		// Set new blackpoint
		updateSettingsFromMouseEvent(e, true, false);

		e->accept();
	}
	else if(e->button() == Qt::RightButton) {
		m_mouseShifting = true;

		// Set new whitepoint
		updateSettingsFromMouseEvent(e, false, true);

		e->accept();
	}
}

void ColorTransform::updateDisplay(int blackpoint, int whitepoint)
{
	// Check if new settings are different and valid
	if((m_whitePoint != whitepoint || m_blackPoint != blackpoint) && blackpoint <= whitepoint) {
		// Change settings and update display
		m_blackPoint = blackpoint;
		m_whitePoint = whitepoint;
		layoutGraphicsElements();

		// Emit signal
		// emit histogramSettingsChanged();
	}
}

void ColorTransform::resetDisplay()
{
	// Default settings
	m_blackPoint = 0;
	m_whitePoint = 255;
	layoutGraphicsElements();

	// Emit signal
	emit histogramSettingsChanged();
}

void ColorTransform::applySettings( ImageDisplaySettings& settings )
{
	settings.setBlackAndWhitePoint(m_blackPoint, m_whitePoint);
}

void ColorTransform::mouseReleaseEvent( QMouseEvent* e )
{
	// Stop mouse shifting
	m_mouseShifting = false;
}

void ColorTransform::mouseMoveEvent( QMouseEvent* e )
{
	// Update settings
	if(m_mouseShifting) {
		if(e->buttons() & Qt::LeftButton) {
			updateSettingsFromMouseEvent(e, true, false);
			emit histogramSettingsChanged();
			e->accept();
		}
		else if(e->buttons() & Qt::RightButton) {
			updateSettingsFromMouseEvent(e, false, true);
			emit histogramSettingsChanged();
			e->accept();
		}
	}
}

void ColorTransform::updateSettingsFromMouseEvent( QMouseEvent* e, bool blackPoint, bool whitePoint )
{
	QPointF sceneCoord = mapToScene(e->pos());
	int pointParam = (sceneCoord.x() / viewport()->width()) * 255.0f + 0.5f;
	pointParam = std::max(pointParam, 0);
	pointParam = std::min(pointParam, 255);

	if(blackPoint) {
		// Set new blackpoint
		int newBlackPoint = std::min(m_whitePoint, pointParam);
		if(newBlackPoint != m_blackPoint) {
			m_blackPoint = newBlackPoint;
			layoutGraphicsElements();	

			// Emit signal
			emit histogramSettingsChanged();
		}
	}
	else if(whitePoint) {
		// Set new whitepoint
		int newWhitePoint = std::max(m_blackPoint, pointParam);
		if(newWhitePoint != m_whitePoint) {
			m_whitePoint = newWhitePoint;
			layoutGraphicsElements();	

			// Emit signal
			emit histogramSettingsChanged();
		}
	}
}
