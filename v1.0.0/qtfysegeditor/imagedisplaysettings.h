/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef imagedisplaysettings_h__
#define imagedisplaysettings_h__

// QT
#include <QVector>
#include <QImage>

// STL
#include <cassert>

/**
 * Information about how images should be displayed.
 *
 * See also ColorTransform.
 */
class ImageDisplaySettings {

public:

	/**
	 * Constructor.
	 */
	ImageDisplaySettings() 
		: m_colorTable(256) 
	{
		// Set default values
		m_blackPoint = 0;
		m_whitePoint = 255;
		updateColorTable();
	}

	/**
	 * Getters.
	 */
	int blackPoint() const 
	{
		return m_blackPoint;
	}
	int whitePoint() const 
	{
		return m_whitePoint;
	}
	const QVector<unsigned int>& colorTable() const 
	{
		return m_colorTable;
	}
	QVector<unsigned int>& colorTable() 
	{
		return m_colorTable;
	}

	/**
	 * Setters.
	 */
	void setBlackAndWhitePoint(int bp, int wp) 
	{
		m_blackPoint = bp;
		m_whitePoint = wp;
		updateColorTable();
	}

	/**
	 * Helper function: applies settings on 8bit grayscale image (only for images with format QImage::Format_Indexed8).
	 */
	void applyDisplaySettings(QImage& image) const
	{
		QImage::Format testFormat =  image.format();
		//assert(image.format() == QImage::Format_Indexed8);

		// Just set color table
		image.setColorTable(m_colorTable);
	}

private:

	/**
	 * Updates color table.
	 */
	void updateColorTable() 
	{
		for(int i = 0; i < m_blackPoint; ++i)
			m_colorTable[i] = qRgb(0,0,0);
		float fbp = m_blackPoint, fwp = m_whitePoint;
		for(int i = m_blackPoint; i < m_whitePoint; ++i) {
			int grayLevel = ((static_cast<float>(i) - fbp) / (fwp - fbp)) * 255.0f + 0.5f;
			m_colorTable[i] = qRgb(grayLevel,grayLevel,grayLevel);
		}
		for(int i = m_whitePoint; i < 256; ++i)
			m_colorTable[i] = qRgb(255,255,255);
	}

	// Blacpoint/whitepoint and color table (always precalculated for speed)
	int m_blackPoint;
	int m_whitePoint;
	QVector<unsigned int> m_colorTable;

};


#endif // imagedisplaysettings_h__
