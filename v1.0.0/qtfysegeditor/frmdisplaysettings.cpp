/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "frmdisplaysettings.h"


FrmDisplaySettings::FrmDisplaySettings( QWidget* parent )
	: QDialog(parent)
{
	// Ui
	ui.setupUi(this);

	// Init variables

	// Remove context help  button
	setWindowFlags( (windowFlags() | Qt::CustomizeWindowHint | Qt::Popup) & ~Qt::WindowContextHelpButtonHint );

	// Connections
	connect(ui.pbtClose , SIGNAL(clicked()), this, SLOT(close()));
	connect(ui.pbtReset , SIGNAL(clicked()), this, SLOT(reset()));
	connect(ui.grvSettings, SIGNAL(settingsChanged()), this, SIGNAL(settingsChanged()));
}

void FrmDisplaySettings::reset()
{
	// Set default settings
	ui.grvSettings->updateDisplay(0, 255);
}

//void FrmDisplaySettings::updateDisplay( const ImageDisplaySettings& settings )
//{
//	// Update color function
//	ui.grvSettings->updateDisplay(settings);
//}

void FrmDisplaySettings::applySettings( ImageDisplaySettings& settings )
{
	// Apply color function
	ui.grvSettings->applySettings(settings);
}
