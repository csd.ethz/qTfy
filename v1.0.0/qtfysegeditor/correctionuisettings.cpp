/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/****************************************************************************
**
** @ Laura Skylaki, 24.06.2014
**
**
****************************************************************************/

// PROJECT
#include "correctionuisettings.h"
#include "qtfygui\qtfyhelp.h"
#include "qtfygui\quantificationoptions.h"
#include "qtfydata\wavelengthinformation.h"
#include "qtfydata\tatinformation.h"

// QT
#include "QInputDialog"
#include "QMessageBox"
#include "QDebug"

QVector<int> CorrectionUISettings::corrWavelengthsToDisplay = QVector<int>();

CorrectionUISettings* CorrectionUISettings::inst (0);

CorrectionUISettings::CorrectionUISettings()
{
	// Setup ui
	ui.setupUi(this);

	// getting the available wavelengths in the experiment
	QVector<WavelengthInformation> wavelengthsInfo = TATInformation::getInst()->getAvailableWavelengthInfo();

	// populate lists 
	foreach (WavelengthInformation wlInfo, wavelengthsInfo) {

		if (wlInfo.isInitialized()) {
			int wlNumber = wlInfo.getWavelengthNumber();
			QString comment = wlInfo.getComment();

			// add to both lists as checkboxes
			QListWidgetItem *itemQ = new QListWidgetItem;
			itemQ->setData(Qt::DisplayRole, wlNumber);
			itemQ->setToolTip(comment);

			if (QuantificationOptions::wavelengthsToQuantify.contains(wlNumber)) {
				itemQ->setCheckState(Qt::Checked);
				CorrectionUISettings::corrWavelengthsToDisplay.append(wlNumber);
			}
			else
				itemQ->setCheckState(Qt::Unchecked);
			ui.lwtWavelengthToDisplay->addItem(itemQ);


		}
	}

	setupConnections();

	// Enable maximize and minimize buttons
	this->setWindowFlags(Qt::Window | Qt::WindowStaysOnTopHint);	
}


CorrectionUISettings* CorrectionUISettings::getInst()
{
	if (! inst) {
		inst = new CorrectionUISettings();
	}

	return inst;
}

CorrectionUISettings::~CorrectionUISettings()
{
}

void CorrectionUISettings::setupConnections()
{
	// setting up actions
	connect(ui.pbtSaveSettings, SIGNAL(clicked()), this, SLOT(saveSettings()));
	connect(ui.pbtCancel, SIGNAL(clicked()), this, SLOT(cancel()));
}

void CorrectionUISettings::updateDisplay()
{
	
	// go through the wavelength lists and identify user checked
	int rowNum = ui.lwtWavelengthToDisplay->count();
	QListWidgetItem* currentItem;

	for(int i = 0; i < rowNum; i++){
		currentItem = ui.lwtWavelengthToDisplay->item(i);
		if (CorrectionUISettings::corrWavelengthsToDisplay.contains(currentItem->text().toInt()))
			currentItem->setCheckState(Qt::Checked);
		else
			currentItem->setCheckState(Qt::Unchecked);			
	}

}



//SLOT:
void CorrectionUISettings::cancel()
{
	CorrectionUISettings::close();
}


//SLOT:
void CorrectionUISettings::saveSettings()
{
	QVector<int> backUpSelection = CorrectionUISettings::corrWavelengthsToDisplay;
	CorrectionUISettings::corrWavelengthsToDisplay.clear();

	// go through the wavelength lists and identify user checked
	int rowNum = ui.lwtWavelengthToDisplay->count();
	QListWidgetItem* currentItem;

	for(int i = 0; i < rowNum; i++){
		currentItem = ui.lwtWavelengthToDisplay->item(i);
		if (currentItem->checkState() == Qt::Checked)
			CorrectionUISettings::corrWavelengthsToDisplay.append(currentItem->text().toInt());
	}

	// user needs to have at least one wl for display but not required for quantification
	if(CorrectionUISettings::corrWavelengthsToDisplay.empty()) {
		QString message = "Please select at least one wavelength to display.";
		QMessageBox::warning (this, "No wavelength to display!", message, QMessageBox::Ok);
		CorrectionUISettings::corrWavelengthsToDisplay = backUpSelection;
		return;
	}
	else {
		/*segmentationMethod = ui.cboSegmentationMethods->currentText();
		segmentationWL = ui.spbSegMethodWL->value();
		emit updateWavelengthsToDisplay(this->wavelengthsToDisplay);
		emit updateWavelengthsToQuantify(this->wavelengthsToQuantify);
		emit updateSegMethodForQuantification(this->segMethodsForQuantification);
		emit updateSegmentationMethodID(segmentationMethod);
		emit updateSegmentationWL(segmentationWL);
		emit formClosed();*/
		CorrectionUISettings::hide();
		emit corrSettingsUpdated();
	}
}


void CorrectionUISettings::showHelp()
{
	// create help text
	QString txt = "<p><h2>StaTTTs Help: Segmentation Editor Settings</h2>"  
		"<p>StaTTTs derives START/END attributes from each timed attribute. By default the START/END attributes are calculated" 
		" using the first 3 available track points (TPs) for each attribute i.e. the Intensity Start Average is the average of the 3 first Intensities of"
		" the cell's lifetime. You can change this default number through the QTFy Settings form. <br><br>"
		"In QTFy you have the option to mark some TPs as inactive, meaning that the values of different attributes for this TPs won't be included in the calculation."
		" StaTTTs gives you two options of dealing with inactive TPs by either including or excluding inactive TPs in your calculation. Please see the following example: <br>"
		"<table border=\"1\">"
		"<tr><td>Track Point</td><td> Intensity </td></tr>"
		"<tr><td>TP1</td><td> 2.75 </td></tr>"
		"<tr><td>TP2</td><td> Inactive</td></tr>"
		"<tr><td>TP3</td><td> 3.25 </td></tr>"
		"<tr><td>TP4</td><td> 2.84 </td></tr>"
		"</table> </p>"
		"<p><b>Include inactive TPs:</b> Intensity Start Average = (Intensity_TP1 + Intensity_TP3)/2 = (2.75 + 3.25)/2 <br><br>"
		"<b>Exclude inactive TPs:</b> Intensity Start Average = (Intensity_TP1 + Intensity_TP3 + Intensity_TP4)/3 = (2.75 + 3.25 + 2.84)/3"
		"</p>";

	// show help form
	QTFyHelp* help = new QTFyHelp(this, txt);
	help->show();
}