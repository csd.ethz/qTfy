/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/****************************************************************************
**
** @ Laura Skylaki, 15.11.2013
**
**
****************************************************************************/

// PROJECT
#include "ImgGraphicsView.h"
#include "contourGraphicsItem.h"
#include "qtfyanalysis/positionquantification.h"
#include "qtfyeditor/segmarker.h"

// QT
#include <QPen>
#include <QWheelEvent>
#include <QApplication>
#include <QScrollBar>
#include <QMessageBox>
#include <QPixmap>
#include <QGraphicsPolygonItem>
#include <QDebug>
#include <QPair>
#include <QHash>
#include <QImageWriter>
#include <QInputDialog>

//STD
#include <algorithm>

// OPENCV
#include <opencv2/imgproc/imgproc.hpp>

ImgGraphicsView::ImgGraphicsView(QWidget *parent, bool isScribblingEnabled):
	QGraphicsView(parent),
	scribbingEnabled(isScribblingEnabled)
{
	
	myPenWidth = 2;
	
	myPenColor = QColor(253, 174, 97);
	myCurrentContourColor = QColor(215, 48, 39);
	myContoursColor = QColor(116, 173, 209);
	myWandColor = QColor(244, 109, 67);

	eraseDiameter = 5;
	selectionDiameter = 30;
	contourLength = 0;
	this->scribbling = false;
	this->isZooming = false;
	this->markersVisible = true;
	this->markerNamesVisible = true;
	markerAtCenter = -1;
	this->isContourTableVisible = true;
	this->isModificationPerformed = false;
	this->isCtrlPressed = false;
	this->isWandActionChecked = false;
	this->isEraserActionChecked = false;
	this->isAutoEraserOn = true;

	qreal h11 = 1.0;
	qreal h12 = 0;
	qreal h21 = 1.0;
	qreal h22 = 0;

	this->lowerDiff = 20;
	this->upperDiff = 20;

	// create the scene
	this->scene = new QGraphicsScene;
	this->setScene(this->scene);
	this->pixmapItem = new QGraphicsPixmapItem;
	this->scene->addItem(pixmapItem);
	
	this->markersGraphicsGroup = new QGraphicsItemGroup();	
	this->contoursGraphicsGroup = new QGraphicsItemGroup();
	this->userContoursGraphicsGroup = new QGraphicsItemGroup();
	this->currentContourGraphicsGroup = new QGraphicsItemGroup();

	this->scene->addItem(markersGraphicsGroup);
	this->scene->addItem(contoursGraphicsGroup);
	this->scene->addItem(userContoursGraphicsGroup);
	this->scene->addItem(currentContourGraphicsGroup);

	currentPath = NULL;
	currentPathItem = NULL;

	setCacheMode(CacheBackground);
	setRenderHint(QPainter::Antialiasing);

	// cursor for drawing
	QPixmap cursor(":/qtfyres/qtfyres/draw-line-2.png");
	QPixmap dcursor(":/qtfyres/qtfyres/draw-eraser-24.png");
	drawCursor = QCursor(cursor,0, cursor.height());
	eraserCursor = QCursor(dcursor,0, dcursor.height());

	QPixmap pbCursor(":/qtfyres/qtfyres/pencilBigger.png");
	QPixmap psCursor(":/qtfyres/qtfyres/pencilSmaller.png");
	adjustPenBiggerCursor = QCursor(pbCursor,0, pbCursor.height());
	adjustPenSmallerCursor = QCursor(psCursor,0, psCursor.height());

	QPixmap zcursor(":/qtfyres/qtfyres/zoom-4.png");
	zoomCursor = QCursor(zcursor, 0, zcursor.height());
	QPixmap wcursor(":/qtfyres/qtfyres/magic-wand.png");
	wandCursor = QCursor(wcursor, 0, wcursor.height());

	QPixmap pcursor(":/qtfyres/qtfyres/draw-line-2.png");
	penCursor = QCursor(pcursor, 0, pcursor.height());

	setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

	setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
	//setAlignment(Qt::AlignLeft | Qt::AlignTop);
	setResizeAnchor(QGraphicsView::AnchorUnderMouse);

	//setScene(scene);
	enableDisablePan(false);

	if	(scribbingEnabled)
		this->setCursor(penCursor);
	
}


ImgGraphicsView :: ~ImgGraphicsView()
{
}


bool ImgGraphicsView::loadImage(const Mat& _src)
{
	QTime t;
	t.start();
	
	QImage loadedImage = QTFyImageOperations::MatToQImage(_src);
	
	/*if (!loadedImage.load(fileName))
		return false;*/

	// check image format
	QImage::Format test = loadedImage.format();

	if (loadedImage.format() == QImage::Format_Invalid)
		return false;

	if (loadedImage.format() == QImage::Format_Mono) {
		this->image = loadedImage.convertToFormat(QImage::Format_ARGB32);
		this->imageBackup = loadedImage.convertToFormat(QImage::Format_ARGB32);
	}
	else {
		m_imageDisplaySettings.applyDisplaySettings(loadedImage);
		this->image = loadedImage;
		this->imageBackup = loadedImage;
	}

	this->pixmapItem->setPixmap(QPixmap::fromImage(image));
	
	qDebug() << QString("I loaded an image %1").arg(viewID);

	clearImageContours();

	update();

	this->imageFound = true;

	this->isModificationPerformed = false;
	return true;
}

bool ImgGraphicsView::loadEmptyImage(int _width, int _height, QColor _bgrColor)
{
	// create new Mat file
	Mat mask = Mat::zeros(_height, _width, CV_8UC1);

	// update the graphics scene with new image
	QImage emptyImage = QTFyImageOperations::MatToQImage(mask);

	// check image format
	/*if (loadedImage.format() == QImage::Format_Indexed8) {
		this->image = loadedImage.convertToFormat(QImage::Format_ARGB32);
		this->imageBackup = loadedImage.convertToFormat(QImage::Format_ARGB32);
	}
	else {*/
	// m_imageDisplaySettings.applyDisplaySettings(loadedImage);
	this->image = emptyImage;
	this->imageBackup = emptyImage;
	//}

	this->pixmapItem->setPixmap(QPixmap::fromImage(image));
   
	// add text to indicate that original image not present
	QGraphicsTextItem * warningText = new QGraphicsTextItem(QString("Original image not found!"));
	warningText->setFont(QFont("Helvetica", 50));
	warningText->setDefaultTextColor(QColor(255, 255, 0, 127));
	this->scene->addItem(warningText);

	clearImageContours();

	qDebug() << QString("I loaded an empty image %1").arg(viewID);

	update();

	this->isModificationPerformed = false;
	this->imageFound = false;

	return true;
}

void ImgGraphicsView::updateImage(QImage img)
{
	this->pixmapItem->setPixmap(QPixmap::fromImage(img));
}

// clear all drawing from scene and display the image only
void ImgGraphicsView::clearImage()
{
	scene->removeItem(contoursGraphicsGroup);
	scene->removeItem(currentContourGraphicsGroup);
	this->scene->clear();
	this->pixmapItem->setPixmap(QPixmap::fromImage(image));
}


void ImgGraphicsView::wheelEvent(QWheelEvent *event)
{
	int wheelDelta = event->delta();

	if (this->isCtrlPressed) {

		if (wheelDelta > 0) {
			setCursor(this->adjustPenBiggerCursor);
			int newPenWidth = this->penWidth() + 1;
			this->setPenWidth(newPenWidth);
			emit updateMsgLog(QString("> Updated pen width: %1...").arg(newPenWidth), Qt::black);
		}
		else {
			setCursor(this->adjustPenSmallerCursor);
			int newPenWidth = this->penWidth() - 1;

			if (newPenWidth<1) // cant be zero
				return;
			else {
				this->setPenWidth(newPenWidth);
				emit updateMsgLog(QString("> Updated pen width: %1...").arg(newPenWidth), Qt::black);
			}
		}
	}
	else {
		
		isZooming = true;

		const qreal delta = event->delta() / 120.0;

		if(delta < 0)
			zoomFactor--;
		else if(delta > 0)
			zoomFactor++;

		qDebug() << zoomFactor;

		zoomFactor = qBound(1.0, zoomFactor, 10.0);

		this->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
		this->setTransform(QTransform(zoomFactor, 0, 0, zoomFactor, 0, 0));

		emit scaleChanged(zoomFactor, this->horizontalScrollBar()->value(), this->verticalScrollBar()->value());
		emit zoomFactorChanged(zoomFactor);
		isZooming = false;
		
	}

	
}

// navigate with the arrows
void ImgGraphicsView::keyPressEvent(QKeyEvent *event) 
{
	if(event->key() == Qt::Key_Up)        {
		/*if (!indexAbove(currentIndex()).isValid())
			return;
		setCurrentIndex(indexAbove(currentIndex()));
		emit clicked(currentIndex());*/
		emit treeOverviewIndexChangedUp();
	}

	if(event->key() == Qt::Key_Down)        {
		/*if (!indexBelow(currentIndex()).isValid())
			return;
		setCurrentIndex(indexBelow(currentIndex()));
		emit clicked(currentIndex());*/
		emit treeOverviewIndexChangedDown();
	}

	// use left and right arrow keys to center to different markers
	// if markers are available

	if (this->markers.size()>0) {

		if (event->key() == Qt::Key_Left)
			updateMarkerPosition(true);
				

		if (event->key() == Qt::Key_Right)
			updateMarkerPosition(false);
		
	}

	if(event->key() == Qt::Key_Control) {
		this->isCtrlPressed = true;
		setCursor(this->adjustPenBiggerCursor);
		emit updateMsgLog("> Tip: Use your mouse wheel to adjust the pen width while keeping the Ctrl key pressed...", Qt::darkMagenta);
	}

	// this->setTheCursor();
}

void ImgGraphicsView::keyReleaseEvent(QKeyEvent *event) 
{
	if(event->key() == Qt::Key_Control)        {
		this->isCtrlPressed = false;
		this->setTheCursor();
	}

}

//SLOT
void ImgGraphicsView::updateMarkerPosition(bool isLeft)
{

	if (isLeft) {
		markerAtCenter--;

		if (markerAtCenter < 0)
			markerAtCenter = this->markers.size() - 1;
		SegMarker mrk = markers.at(markerAtCenter);
		this->centerViewToPoint(mrk.getCoordPoint());

		emit newCellInFocus(mrk);

	}
	else {
		markerAtCenter++;

		if (markerAtCenter >= this->markers.size())
			markerAtCenter = 0;
		SegMarker mrk = markers.at(markerAtCenter);
		this->centerViewToPoint(mrk.getCoordPoint());

		emit newCellInFocus(mrk);
	}
}


void ImgGraphicsView::mousePressEvent(QMouseEvent *event)
{
	QPointF mapToScenePoint = mapToScene(event->pos());

	if (event->button() == Qt::LeftButton && scribbingEnabled) {

		if (!this->isImageFound())
			return;

		if (this->isEraserActionChecked) {		

			setCursor(eraserCursor);
			lastPoint = mapToScenePoint.toPoint();
			scribbling = false;

			// reset
			firstPoint = QPoint(-1,-1);
			contourLength = 0;
		}
		else {

			if (this->isAutoEraserOn) {
				//delete the current contour
				this->currentContourGraphicsGroup->setVisible(false);
				// notify the segmentation image
				emit deleteContour(currentCorrectionSuggestionContour);	
				emit clearSuggestionContour();
				deleteContourFromImage(currentCorrectionSuggestionContour);				
			}
			else {
				emit moveCurrentContour(currentCorrectionSuggestionContour);
				moveCurrentContourToImageContours(currentCorrectionSuggestionContour);
			}
			
			qDeleteAll(this->currentContourGraphicsGroup->children());
			currentCorrectionSuggestionContour.clear();

			if (this->isWandActionChecked) {
				// try the magic wand
				// define the seed point  
				Point seed = cvPoint(mapToScenePoint.x(), mapToScenePoint.y());  

				// get the visible rectangular
				QRect viewport_rect(0, 0, this->viewport()->width(), this->viewport()->height());
				QRectF visible_scene_rect = this->mapToScene(viewport_rect).boundingRect();

				// get the image in the scence
				QImage viewportImage = this->pixmapItem->pixmap().toImage();
				QImage roiImage = viewportImage.copy(visible_scene_rect.x(), visible_scene_rect.y(), visible_scene_rect.width(), visible_scene_rect.height());
				// QImage adjRoiImage = roiImage.convertToFormat(QImage::Format_RGB16);

				Point adjSeed (seed.x - visible_scene_rect.x(), seed.y - visible_scene_rect.y());

				//roiImage.save("testROIqimage.jpg");

				Mat img = QTFyImageOperations::QImageToMat(roiImage, CV_8UC1);
				Mat imgROI, imgROI3C;
				img.copyTo(imgROI);
				//Mat imgROI = QTFyImageOperations::getImageROI(ffimg, seed, 100);
				cvtColor(imgROI, imgROI3C, COLOR_GRAY2BGR);

				int type = imgROI3C.type();

				//imwrite("testROI.jpg", imgROI3C);

				int ffillMode = 1;
				int connectivity = 4;
				int newMaskVal = 255;
				int lo = ffillMode == 0 ? 0 : this->lowerDiff;
				int up = ffillMode == 0 ? 0 : this->upperDiff;
				int flags = connectivity + (newMaskVal << 8) + (ffillMode == 1 ? CV_FLOODFILL_FIXED_RANGE : 0);

				Rect ccomp;

				Scalar newVal = Scalar(255, 255, 255);

				// blur
				int kernelSize = PositionQuantification::options.value("gaussianBlurKernelSize").toInt();

				Mat dst;
				//if (_options.value("useSmoothing") == "true")	
				blur( imgROI3C, dst, Size( kernelSize, kernelSize ), Point(-1,-1));


				// flood fill

				Mat mask;
				mask.create(dst.rows+2, dst.cols+2, CV_8UC1); // Operation mask that should be a single-channel 8-bit image, 2 pixels wider and 2 pixels taller than image.
				mask = Scalar::all(0);
				threshold(mask, mask, 1, 128, CV_THRESH_BINARY);
				int area = floodFill(dst, mask, adjSeed, newVal, &ccomp, Scalar(lo, lo, lo),	Scalar(up, up, up), flags);

				// find the flood fill contour
				// Mat watershedMask = QTFyImageOperations::watershedROI(mask);
				// vector<vector <Point>> contours = QTFyImageOperations::getLabeledComponentsContours(watershedMask);	
				vector<vector <Point>> contours = QTFyImageOperations::getImageContours(mask, PositionQuantification::options.value("minCellSize").toInt(), PositionQuantification::options.value("maxCellSize").toInt(), true, true);

				if (contours.empty())
					return;

				vector<Point> theWandContour = QTFyImageOperations::getClosestContour(contours, adjSeed, 20);

				if (theWandContour.empty())
					return;

				QVector<QPoint> currentQT;
				for (int j =0; j< theWandContour.size(); j++) {
					int newX = visible_scene_rect.x() + theWandContour.at(j).x ;
					int newY = visible_scene_rect.y() +  theWandContour.at(j).y ;

					QPointF test = QPoint(newX, newY);

					QPoint newPointQT (test.x(), test.y());
					currentQT.append(newPointQT);

					currentCorrectionSuggestionContour.push_back(Point(newX, newY));
				}

				emit setSuggestionContour(currentCorrectionSuggestionContour);
				emit saveNewContour(currentCorrectionSuggestionContour, myWandColor, true);
				addNewContourToImage(currentCorrectionSuggestionContour, myWandColor, true);

				this->currentContourGraphicsGroup->setVisible(true);
				// display the contour item
				this->scene->update();
			}

			// user just started drawing
			else {

				lastPoint = mapToScenePoint.toPoint();
				firstPoint = mapToScenePoint.toPoint();
				scribbling = true;	

				// start path			
				currentPath = new QPainterPath(mapToScenePoint);
				currentPathItem = new QGraphicsPathItem(*currentPath);
				currentPathItem->setPen( QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
				this->userContoursGraphicsGroup->addToGroup(currentPathItem);

				// clear the active contour to start a new
				this->activeContourPoints.clear();
			}
		}
	}

	if (event->button() == Qt::MiddleButton){
		enableDisablePan(true);
		_panStartX = event->x();
		_panStartY = event->y();
		setCursor(Qt::ClosedHandCursor);
		event->accept();
		return;
	}

	this->setTheCursor();

	event->accept();
	
}


void ImgGraphicsView::mouseMoveEvent(QMouseEvent *event)
{
	QGraphicsView::mouseMoveEvent(event);
	QPointF mapToScenePoint = mapToScene(event->pos());

	if (_pan)
    {
        horizontalScrollBar()->setValue(horizontalScrollBar()->value() - (event->x() - _panStartX));
        verticalScrollBar()->setValue(verticalScrollBar()->value() - (event->y() - _panStartY));
        _panStartX = event->x();
        _panStartY = event->y();
        event->accept();
        return;
    }

	//identifyItemsWithinRadius(mapToScenePoint);

	if (scribbingEnabled) {

		if (!this->isImageFound())
			return;

		if ((event->buttons() & Qt::LeftButton) && scribbling && !this->isWandActionChecked && !this->isEraserActionChecked)
			drawLineTo(mapToScenePoint.toPoint());
		else if ((event->buttons() & Qt::LeftButton) && this->isEraserActionChecked)
			deleteLineTo(mapToScenePoint.toPoint());
	}
	event->accept();
}


void ImgGraphicsView::mouseReleaseEvent(QMouseEvent *event)
{	
	QPointF mapToScenePoint = mapToScene(event->pos());
	if (scribbingEnabled) {

		if (!this->isImageFound())
			return;

		this->setTheCursor();
		if (event->button() == Qt::LeftButton && scribbling) {
			// drawLineTo(mapToScenePoint.toPoint());
			drawLineTo(firstPoint);
			lastPoint = mapToScenePoint.toPoint();

			this->setTheCursor();
			emit saveNewContour(activeContourPoints, myPenColor, true);
			emit setSuggestionContour(activeContourPoints);
			this->setCurrentContour(activeContourPoints); // this also creates the contourAndGraphicItem
			// addNewContourToImage(activeContourPoints, myPenColor, true);

			// clear the active contour to start a new
			this->activeContourPoints.clear();
			this->userContoursGraphicsGroup->removeFromGroup(currentPathItem);
			delete currentPath;
			delete currentPathItem;

			scribbling = false;

			// reset
			firstPoint = QPoint(-1,-1);
			contourLength = 0;
		}
	}
	if (event->button() == Qt::MiddleButton) {
		enableDisablePan(false);
		this->setTheCursor();
		event->accept();
		return;
	}
	event->accept();
}


void ImgGraphicsView::enableDisablePan(bool enable)
{
	this->_pan = enable;
}


// SLOT
void ImgGraphicsView::setScale(qreal _scaleFactor, int _hscroll, int _vscroll) 
{
	/*setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
	qreal factor = matrix().scale(scaleFactor, scaleFactor).mapRect(QRectF(0, 0, 1, 1)).width();
	if (factor < 0.07 || factor > 100)
	return;
	scale(scaleFactor, scaleFactor);*/
	this->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
	this->setTransform(QTransform(_scaleFactor, 0, 0, _scaleFactor, 0, 0));
	this->zoomFactor = _scaleFactor;
	this->horizontalScrollBar()->setValue(_hscroll);
	this->verticalScrollBar()->setValue(_vscroll);
}

// SLOT
void ImgGraphicsView::setScale(qreal _scaleFactor) 
{
	/*setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
	qreal factor = matrix().scale(scaleFactor, scaleFactor).mapRect(QRectF(0, 0, 1, 1)).width();
	if (factor < 0.07 || factor > 100)
	return;
	scale(scaleFactor, scaleFactor);*/
	this->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
	this->setTransform(QTransform(_scaleFactor, 0, 0, _scaleFactor, 0, 0));
	this->zoomFactor = _scaleFactor;
}


// drawing operations start here

void ImgGraphicsView::drawLineTo(const QPoint &endPoint)
{
	currentPath->lineTo(endPoint.x(), endPoint.y());
	currentPathItem ->setPath(*currentPath);
	contourLength++;
	
	lastPoint = endPoint;
	this->scene->update();

	Point newPoint (endPoint.x(), endPoint.y());
	activeContourPoints.push_back(newPoint);
}


void ImgGraphicsView::deleteLineTo(const QPoint &endPoint)
{
	setCursor(Qt::BusyCursor);

	// find all the graphic items that are under the eraser diameter	
	QRectF eraserRect (endPoint.x()-eraseDiameter/2, endPoint.y()-eraseDiameter/2, eraseDiameter, eraseDiameter);
	QList<QGraphicsItem*> itemsUnderCursor = scene->items(eraserRect);
	QList<QGraphicsItem*>::iterator i;

	for (i = itemsUnderCursor.begin(); i != itemsUnderCursor.end(); i++) {

		// item under the mouse pointer is NOT a) the original image, b) the whole contours graphics group, c) the whole markers graphic group or d) any marker
		// therefore item under the mouse pointer is a contour and can be erased
		if (contoursGraphicsGroup->children().contains(*i)) {
			contourGraphicsItem *contour = dynamic_cast<contourGraphicsItem*>(*i);
			vector<Point> itemsContour = this->mapGraphicItemsToContours.value(contour);
			
			// notify the segmentation image
			emit deleteContour(itemsContour);	

			deleteContourFromImage(itemsContour);
			
		}

		// it is a user drawn contour
		if (userContoursGraphicsGroup->children().contains(*i)) {
			
			// get and delete corresponding contour		
			QGraphicsPathItem* pathItem = dynamic_cast<QGraphicsPathItem*>(*i);
			contourGraphicsItem *contour = this->mapUserGraphicsPathItemToContourGraphicItem.value(pathItem);
			vector<Point> itemsContour = this->mapGraphicItemsToContours.value(contour);
			
			// find position of deleted items contour in the vector of contours
			// notify the segmentation image
			emit deleteContour(itemsContour);	
			deleteContourFromImage(itemsContour);

			// remove the graphics path item
			scene->removeItem(*i);

		}

		// it is a suggested contour
		if (currentContourGraphicsGroup->children().contains(*i)) {

			contourGraphicsItem *contour = dynamic_cast<contourGraphicsItem*>(*i);
			vector<Point> itemsContour = this->mapGraphicItemsToContours.value(contour);

			// notify the segmentation image
			emit deleteContour(itemsContour);	

			deleteContourFromImage(itemsContour);

		}
	}

	lastPoint = endPoint;
	this->setTheCursor();
}


void ImgGraphicsView::clearImageContours()
{
	// delete previous image contours
	this->segmContours.clear();
	this->segmContours.shrink_to_fit();

	// free memory from previously stored contour graphic items
	qDeleteAll(this->contoursGraphicsGroup->children());
	qDeleteAll(this->currentContourGraphicsGroup->children());

	//qDeleteAll(this->mapItemsToContours);
	this->mapGraphicItemsToContours.clear();
	this->mapGraphicItemsToContours.squeeze();
}


void ImgGraphicsView::setImageContours(vector<vector<Point> > _imgContours)
{
	setCursor(Qt::BusyCursor);
	
	this->clearImageContours();

	// transform the opencv contours to QT contours
	for( int i = 0; i< _imgContours.size(); i++ )
    {
		vector<Point> current = _imgContours.at(i);
		contourGraphicsItem* newContourGItem = createContourAndGraphicItem (current);

		// add item to group of all cell contours for convenient manipulation
		contoursGraphicsGroup->addToGroup(newContourGItem);
	}

	updateContourColors();
	this->setTheCursor();
	
}

void ImgGraphicsView::setCurrentContour(vector<Point> _currentContour)
{
	setCursor(Qt::BusyCursor);

	this->currentCorrectionSuggestionContour = _currentContour;
	contourGraphicsItem* newContourGItem = createContourAndGraphicItem (_currentContour);
	this->currentContourGraphicsGroup->addToGroup(newContourGItem);
	this->currentContourGraphicsGroup->setVisible(true);

	updateContourColors();
	this->setTheCursor();

}

void ImgGraphicsView::setCurrentCorrectionSuggestionContour(vector<Point> _contourPoints)
{
	this->currentCorrectionSuggestionContour = _contourPoints;
}


contourGraphicsItem* ImgGraphicsView::createContourAndGraphicItem (vector<Point> contour)
{
	//vector<Point> current = imgContours.at(i);
	QVector<QPoint> currentQT;
	for (int j =0; j< contour.size(); j++) {
		QPoint newPointQT (contour.at(j).x, contour.at(j).y);
		currentQT.append(newPointQT);
	}

	// create polygon of contour and store in new contour graphics item
	QPolygon contourPoly (currentQT);
	contourGraphicsItem* newContour = new contourGraphicsItem(contourPoly, contour);
	this->segmContours.push_back(contour);

	this->mapGraphicItemsToContours.insert(newContour, contour);

	return newContour;
}

void ImgGraphicsView::setPlotMarkers(QVector<SegMarker> _markers)
{
	// free memory from previous markers
	
	//qDeleteAll(this->markers); 
	this->markers.clear();
	this->markers.squeeze();
	
	this->markers = _markers;
	
	// delete graphic items belonging to previous markers
	qDeleteAll(this->markersGraphicsGroup->children());

	emit clearContoursTable();

	// add contours to scene and make invisible for now
	markersGraphicsGroup->setVisible(true);
	
	foreach(SegMarker mrk, markers)
	{
		
		// get the contour for the current marker...
		// find the overlapping contour
		// CAUTION: it can be null
		//contourGraphicsItem* currentContourItem = markerContourOverlap (mrk);

		
		double XaxisVal;
		double YaxisVal;
		int radius = 5;
				
		XaxisVal = mrk.getX();
		YaxisVal = mrk.getY();

		//if (currentContourItem) {

			QGraphicsEllipseItem* item = new QGraphicsEllipseItem(XaxisVal, YaxisVal, radius, radius);
			item->setPen(QPen(mrk.getMarkerColour(), 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
			markersGraphicsGroup->addToGroup(item);
		/*}
		else {
			QGraphicsTextItem* txt = new QGraphicsTextItem("X");
			txt->setDefaultTextColor ( mrk.getMarkerColour() );
			txt->setPos(XaxisVal - radius/2, YaxisVal);
			markersGraphicsGroup->addToGroup(txt);
		}*/

		if (markerNamesVisible) {
			
			QString name = QString::number(mrk.getCellID()); // show only the cell number on the raw image

			QGraphicsTextItem* txt = new QGraphicsTextItem(name);
			txt->setDefaultTextColor ( mrk.getMarkerColour() );
			txt->setPos(XaxisVal + radius, YaxisVal);
			markersGraphicsGroup->addToGroup(txt);
		}

	}

	// center view to first marker, if marker has not been set before
	if (markerAtCenter < 0) {
		SegMarker mrk = markers.at(0);
		markerAtCenter = 0;
		this->centerViewToPoint(mrk.getCoordPoint());
		emit newCellInFocus(mrk);
	} else {
		if (markerAtCenter >= this->markers.size())
			markerAtCenter = 0;
		SegMarker mrk = markers.at(markerAtCenter);
		this->centerViewToPoint(mrk.getCoordPoint());
		emit newCellInFocus(mrk);
	}
	
	this->update();
}

// Autosave mode on/off
void ImgGraphicsView::setAutoSave(bool state)
{
	this->isAutoSaveON = state;
}


// Autosave mode on/off
void ImgGraphicsView::setAutoEraserState(bool state)
{
	this->isAutoEraserOn = state;
}


// SLOT
// makes the group of identified contours visible to the user
void ImgGraphicsView::setContoursGroupVisible(bool isVisible)
{
	this->contoursGraphicsGroup->setVisible(isVisible);
}


// SLOT
void ImgGraphicsView::addNewContourToImage(vector<Point> _contourPoints, QColor _c, bool _isManual)
{

	ImgGraphicsView* view = (ImgGraphicsView*) sender();
	if (view != NULL)
		qDebug() << "Add new contour sent from: " << view->getViewID() << ", to: " << viewID;

	qDebug() << "View: " << viewID << " adding new contour of size: " << _contourPoints.size(); 

	// add the new contour to images contours
	contourGraphicsItem* currentContour = createContourAndGraphicItem(_contourPoints);

	// add item to group of all cell contours for convenient manipulation
	if (_isManual)
		currentContourGraphicsGroup->addToGroup(currentContour);
	else
		contoursGraphicsGroup->addToGroup(currentContour);

	// we need to identify items that are completely enclosed by the contour
	// there is a bug in qgraphicscene if too many items start to overlap
	QRectF contoursBoundingRect =  currentContour->boundingRect();
	foreach (contourGraphicsItem* ct, this->mapGraphicItemsToContours.keys()) {

		if (ct != NULL && ct != currentContour) {
			QRectF ctBoundingRect =  ct->boundingRect();
			if (contoursBoundingRect.contains(ctBoundingRect)) {
				vector<Point> overlappedCtPnts = this->mapGraphicItemsToContours.value(ct);
				emit deleteContour(overlappedCtPnts);
				deleteContourFromImage(overlappedCtPnts);
			}
		}
		
	}
	
	// do this only for the segmentation image
	if (!this->scribbingEnabled) {
		// update the image
		this->image = updateContoursOnImage(this->segmContours);

		// update the scene
		updateImage(this->image);
	}

	updateContourColors();
	this->isModificationPerformed = true;
}

void ImgGraphicsView::deleteContourFromImage(vector<Point> contourPoints)
{

	ImgGraphicsView* view = (ImgGraphicsView*) sender();

	if (view != NULL)
		qDebug() << "Delete contour sent from: " << view->getViewID() << ", to: " << viewID;

	qDebug() << "View: " << viewID << " deleting contour of size: " << contourPoints.size(); 

	// remove from the scene
	if (!this->mapGraphicItemsToContours.values().contains(contourPoints))
		return;

	contourGraphicsItem* itemToDelete = this->mapGraphicItemsToContours.key(contourPoints);

	if (this->scene->items().contains(itemToDelete))
		this->scene->removeItem(itemToDelete);

	// delete contour from image contours
	this->segmContours.erase(std::remove(this->segmContours.begin(), this->segmContours.end(), contourPoints), this->segmContours.end());


	// delete also the equivalent contour graphic item 
	this->mapGraphicItemsToContours.remove(itemToDelete);
	
	if (!this->scribbingEnabled) {
		// update the image
		this->image = updateContoursOnImage(this->segmContours);

		// update the scene
		updateImage(this->image);
	}

	updateContourColors();
	this->isModificationPerformed = true;

}


void ImgGraphicsView::moveCurrentContourToImageContours(vector<Point> contourPoints)
{
	if (!this->mapGraphicItemsToContours.values().contains(contourPoints))
		return;

	contourGraphicsItem* contourToMove = this->mapGraphicItemsToContours.key(contourPoints);

	if (contourToMove != NULL) {
		this->contoursGraphicsGroup->addToGroup(contourToMove);	
	}
	else {
		qDebug() << "Move contour: something went wrong";
	}

	this->isModificationPerformed = true;
}

void ImgGraphicsView::clearCurrentCorrectionSuggestionContour()
{
	this->currentCorrectionSuggestionContour.clear();
}


void ImgGraphicsView::updateContourColors()
{
	if (this->markers.empty())
		return;

	contourGraphicsItem* currentContourItem = markerContourOverlap (this->markers.at(0));
	if (currentContourItem != NULL) 
		currentContourItem->setContourPen(QPen(myCurrentContourColor, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

	foreach (contourGraphicsItem* contour, this->mapGraphicItemsToContours.keys()) {
		if (contour!=NULL) {
			if (contour != currentContourItem)
				contour->setContourPen(QPen(myContoursColor, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
		}
	}
}

void ImgGraphicsView::setPenColor(const QColor &newColor)
{
	myPenColor = newColor;
}

void ImgGraphicsView::setPenWidth(int newWidth)
{
	myPenWidth = newWidth;
}

void ImgGraphicsView::setEraserDiameter(int newDiameter)
{
	eraseDiameter = newDiameter;
}

// SLOT
void ImgGraphicsView::setEraserState(bool _state){
	this->isEraserActionChecked = _state;
	this->setTheCursor();
}

// SLOT
void ImgGraphicsView::setWandState(bool _state){
	this->isWandActionChecked = _state;
	this->setTheCursor();
}

// set the original image
void ImgGraphicsView::setGRVImage (QImage img)
{
	this->imageBackup = img;
}


// draw a new contour to the actual image (for mask image only)
QImage ImgGraphicsView::updateContoursOnImage(vector<vector <Point> > contours)
{
	if (this->viewID != "seg")
		return QImage();

	// create new Mat file
	Mat mask = Mat::zeros(this->image.height(), this->image.width(), CV_8UC1);

	// plot the contours
	QTFyImageOperations::plotContours(mask, contours);

	// update the graphics scene with new image
	return QTFyImageOperations::MatToQImage(mask);

}


void ImgGraphicsView::centerViewToPoint (const QPoint _point)
{
	this->centerOn(_point);
	
}

// determines whether the contour table is visible
// if true, it calculates the contour features for tracked cells according to markers
void ImgGraphicsView::setContourTableVisible(bool _isContourTableVisible)
{
	this->isContourTableVisible = _isContourTableVisible;
}



// check if marker overlaps with any of the available contours
contourGraphicsItem* ImgGraphicsView::markerContourOverlap (SegMarker mrk)
{
	if (this->segmContours.size() == 0)
		return NULL;

	QPoint markerCenter = mrk.getCoordPoint();


	// find the contour graphic items that contain the marker

	QList<contourGraphicsItem*> contourItems = this->mapGraphicItemsToContours.keys();
	QList<contourGraphicsItem*>::iterator i;

	for (i = contourItems.begin(); i != contourItems.end(); i++) {
		contourGraphicsItem* currentContourItem =  (*i);

		// contour item identified!!
		if (currentContourItem->contains(markerCenter)) {
			currentContourItem->setMarkerOverlap(true);
			if (viewID == "seg") {
				// just emit this once
				QString msg = QString("Contour found for %1 at timepoint %2.").arg(mrk.getMarkerName()).arg(mrk.getTimepointNumber());
				emit updateMsgLog(msg, Qt::darkGreen);
			}
			return currentContourItem;
		}
	}

	double maxDistance = PositionQuantification::options.value("maxDistanceFromTrackpoint").toInt();
	cv::Point mrkPoint(mrk.getX(), mrk.getY());
	contourGraphicsItem* closestContour = QTFyImageOperations::getClosestContour(contourItems, mrkPoint, maxDistance);

	if (closestContour!=NULL && !closestContour->hasMarker()) {
		closestContour->setMarkerOverlap(true);

		if (viewID == "seg") {
			// just emit this once
			QString msg = QString("Contour found for %1 at timepoint %2.").arg(mrk.getMarkerName()).arg(mrk.getTimepointNumber());
			emit updateMsgLog(msg, Qt::darkGreen);
		}

		
		return closestContour;
	}

	if (viewID == "seg") {
		// just emit this once
		/*QString msg = QString("Failure to find contour for %1 at timepoint %2.").arg(mrk.getMarkerName()).arg(mrk.getTimepointNumber());
		emit updateMsgLog(msg, Qt::red);*/
	}
	
	return NULL;

	// TODO:: check distance of marker from contour center (when user is marking far outside the cell)
}

void ImgGraphicsView::displayHistogramSettingsChanged(ColorTransform* _settingsView)
{
	if (!this->isImageFound())
		return;

	// Get settings
	_settingsView->applySettings(m_imageDisplaySettings);

	// Apply settings
	QImage copyOriginal = this->getGRVImage().convertToFormat(QImage::Format_Indexed8);
	QImage::Format testFormat =  copyOriginal.format();
	m_imageDisplaySettings.applyDisplaySettings(copyOriginal);

	// Update display
	this->updateImage(copyOriginal);

	//// Inform settings wizard
	//if(frmSettingsAssistant)
	//	frmSettingsAssistant->imageDisplaySettingsChanged(m_imageDisplaySettings);
}

void ImgGraphicsView::setTheCursor()
{

	if (this->isEraserActionChecked)
		setCursor(eraserCursor);
	else if (this->isWandActionChecked)
		setCursor(wandCursor);
	else
		setCursor(penCursor);
}

// SLOT
void ImgGraphicsView::hScrollBarChanged(int _newValue)
{
	// ask the main window to update the other views only when not zooming
	// i.e. scrollbar changes manually by the user
	// otherwise scaling takes care of the scrollbars
	if (!isZooming)
		emit hScrollBarUpdate(_newValue, isZooming, this->viewID);
}

// SLOT
void ImgGraphicsView::vScrollBarChanged(int _newValue)
{
	// ask the main window to update the other views only when not zooming
	// i.e. scrollbar changes manually by the user
	// otherwise scaling takes care of the scrollbars
	if (!isZooming)
		emit vScrollBarUpdate(_newValue, isZooming, this->viewID);
}


