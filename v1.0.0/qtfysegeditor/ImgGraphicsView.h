/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/****************************************************************************
**
** @ Laura Skylaki, 15.11.2013
**
**
****************************************************************************/


#ifndef IMGGRAPHICSVIEW_H
#define IMGGRAPHICSVIEW_H

//PROJECT
#include "qtfyanalysis/qtfyimageoperations.h"
#include "qtfyeditor/segmarker.h"
#include "imagedisplaysettings.h"
#include "frmdisplaysettings.h"

// QT
#include <QColor>
#include <QImage>
#include <QPointF>
#include <QWidget>
#include <QGraphicsView>
#include <QGraphicsPixmapItem>


// OPENCV
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"

using std::vector;
using namespace cv;

class contourGraphicsItem;


class ImgGraphicsView : public QGraphicsView
{
	Q_OBJECT

public:
	ImgGraphicsView(QWidget *parent, bool isScribblingEnabled);
	~ImgGraphicsView();

	void setPenColor(const QColor &newColor);
	void setPenWidth(int newWidth);
	void setEraserDiameter(int newDiameter);
	QColor penColor() const { return myPenColor; }
	int penWidth() const { return myPenWidth; }
	int eraserDiameter() const { return eraseDiameter; }

	// set the original image
	void setGRVImage (QImage img);

	// get the original image
	QImage getGRVImage () {return image; }

	// get the image currently displayed
	QImage getImage () {return image; }

	// get the image currently displayed
	Mat getMatImage () {return QTFyImageOperations::QImageToMat(image, 0); }

	// load the original image
	bool loadImage(const Mat& _src);

	/*! \brief loads an empty image with defined dims and background color
	//!
	//! \param _width int the width of the new empty image to create 
	//! \param _height int the height of the new empty image to create 
	//! \param _bgrColor QColor the background color of the new empty image
	//! \return bool true if image has been created successfully
	*/
	bool loadEmptyImage(int _width, int _height, QColor _bgrColor);

	/*! \brief resets the image contours and related graphic items
	*/
	void clearImageContours();

	// update the image in the scene
	void updateImage(QImage img);


	// set the image contours
	void setImageContours(vector<vector<Point> > _imgContours);

	// the contour that is closest to the current marker
	// has a red color and all other contours have a blue color
	void updateContourColors();

	// set the image contour that is assigned to current trackpoint
	void setCurrentContour(vector<Point> _currentContour);

	// plots an ellipse at the point center and the name of the cell next to it
	void setPlotMarkers( QVector<SegMarker> _markers );

	// centers the view to the point and zooms in
	void centerViewToPoint (const QPoint _point);

	bool isModifiedByUser() { return this->isModificationPerformed; }

	void setIsModifiedByUser(bool _modificationStatus) {
		this->isModificationPerformed = _modificationStatus;
	}

	// get the viewID
	QString getViewID() { return this->viewID; }

	// set the viewID
	void setViewID (QString id) { this->viewID = id; }

	// scale the zoom factor
	// void scaleView(qreal scaleFactor);

	bool isViewZooming() {
		return this->isZooming;
	}

	qreal getZoomFactor(){
		return this->zoomFactor;
	}

	void setZoomFactor(qreal _zoomFactor){
		this->zoomFactor = _zoomFactor;
	}

	bool isImageFound() {
		return this->imageFound;
	}

public slots:
	
	void addNewContourToImage(vector<Point> _contousPoints, QColor _c, bool _isManual);

	void deleteContourFromImage(vector<Point> contourPoints);

	void moveCurrentContourToImageContours(vector<Point> contourPoints);

	void clearCurrentCorrectionSuggestionContour();

	void setCurrentCorrectionSuggestionContour(vector<Point> _contourPoints);

	void setScale(qreal _scaleFactor, int _hscroll, int _vscroll);

	void setScale(qreal _scaleFactor);

	// clear all drawing from scene and display the image only
	void clearImage();

	// makes the group of identified contours visible to the user
	void setContoursGroupVisible(bool isVisible);

	// Autosave mode on/off
	void setAutoSave(bool state);

	// Autoeraser mode on/off
	void setAutoEraserState(bool state);

	// determines whether the contour table is visible
	// if true, it calculates the contour features for tracked cells according to markers
	void setContourTableVisible(bool _isContourTableVisible);

	// to change the contrast of the views-images
	void displayHistogramSettingsChanged(ColorTransform* _settingsView);

	// get view white point
	int getViewWhitePoint() {
		return this->m_imageDisplaySettings.whitePoint();
	}

	// get view white point
	int getViewBlackPoint() {
		return this->m_imageDisplaySettings.blackPoint();
	}

	//SLOT
	void updateMarkerPosition(bool isLeft);

	void setEraserState(bool _state);

	void setWandState(bool _state);

	// updates the lower/upper difference threshold for the flood fill options
	// in the qgraphicsview
	void updateWandLowerDiff(int _loDiff){
		this->lowerDiff = _loDiff;
	}

	void updateWandUpperDiff(int _upDiff){
		this->upperDiff = _upDiff;
	}

	// management of scroll bars moves and synchronization
	void hScrollBarChanged(int _newValue);
	void vScrollBarChanged(int _newValue);
	

signals:
	void saveNewContour(vector<Point> _contourPoints, QColor _c, bool _isManual);
	void deleteContour(vector<Point> _contourPoints);
	void moveCurrentContour(vector<Point> _contourPoints);
	void clearSuggestionContour();
	void setSuggestionContour(vector<Point>);
	void scaleChanged(qreal, int, int);
	void updateMsgLog(QString _msg, QColor _color);

	// navigate the tree overview through the arrow keys
	void treeOverviewIndexChangedUp();
	void treeOverviewIndexChangedDown();

	// scroll bars synchronisation
	void hScrollBarUpdate(int, bool, QString);
	void vScrollBarUpdate(int, bool, QString);
	void zoomFactorChanged(qreal);
	

	// add marker contour features to table
	void addMarkerContourToTable(SegMarker _mrk, QHash<QString, float> _features);

	// clears all entries from the contours table
	void clearContoursTable();

	// sends the signal that a new cell is in focus
	void newCellInFocus(SegMarker _mrk);

protected:
	void mousePressEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
	void wheelEvent (QWheelEvent * event );
	void keyPressEvent(QKeyEvent *event);
	void keyReleaseEvent(QKeyEvent *event);
	

private:

	
	void enableDisablePan(bool enable);
	void drawLineTo(const QPoint &endPoint);
	void deleteLineTo(const QPoint &endPoint);

	// check if marker overlaps with available contour
	contourGraphicsItem* markerContourOverlap (SegMarker mrk);

	// save modified segmentation image image, save file with cell features
	void saveModifications();

	// after the contours of the image have changed, recreates the image with updated contours
	QImage updateContoursOnImage(vector<vector <Point> > contours);

	// creates a new contour graphics item and adds it to the hash
	contourGraphicsItem* createContourAndGraphicItem (vector<Point> contour);

	// many options to set the cursor
	void setTheCursor();

	// the image can be modified by the user (for the original image only, 
	// the segmentation mask is modified automatically as a result of operations on original image)
	bool scribbingEnabled;

	// if the original wavelength image is not found
	// all user interaction with the wavelength image is prohibited
	// in the case of the segmentation image
	// a blank image is generated instead so it can be manually created by the user
	bool imageFound;


	// if the contour table is visible the contour features of tracked cells will be calculated
	// if not visible the calculation will be omitted, saving speed
	bool isContourTableVisible;
	
	// enable panning
	bool _pan;
    int _panStartX, _panStartY;
	QPoint lastPoint, firstPoint;
	QCursor drawCursor;
	QCursor eraserCursor;
	QCursor adjustPenBiggerCursor;
	QCursor adjustPenSmallerCursor;
	QCursor zoomCursor;
	QCursor wandCursor;
	QCursor penCursor;

	// zoom in/out transformation
	qreal zoomFactor;

	// flood fill options
	int lowerDiff;
	int upperDiff;

	// is the user drawing something at the moment?
	bool scribbling;

	// is the user zooming at the moment
	bool isZooming;

	// should points and names of marked objects (i.e. tracked cells) be displayed
	bool markersVisible;
	bool markerNamesVisible;
	QVector<SegMarker> markers;

	// when true each time the image changes whatever changes have been made are saved
	bool isAutoSaveON;

	// used to identify that the user has drawn a contour longer than contourLength
	int contourLength;

	// thickness of line the user is drawing
	int myPenWidth;

	// colour of the line the user is drawing
	QColor myPenColor;
	QColor myCurrentContourColor;
	QColor myContoursColor;
	QColor myWandColor;

	// diameter of the eraser to identify contours to be deleted
	int eraseDiameter;

	// TODO
	int selectionDiameter;
	
	// backup of the original image contained in the graphics view
	QImage imageBackup;

	// the image contained in the graphics view
	QImage image;

	// the underlying image graphics item
	QGraphicsPixmapItem *pixmapItem;

	// the scene
    QGraphicsScene *scene;

	// the graphics group that contains all the contours that are present in the image
	QGraphicsItemGroup *contoursGraphicsGroup;

	// the graphics group that contains all the contours that are present in the image
	QGraphicsItemGroup *userContoursGraphicsGroup;

	// the graphics group that contains all the markers that identify objects of interest on image
	QGraphicsItemGroup *markersGraphicsGroup;

	// the graphics group that contains the contour that is assigned to the current trackpoint
	QGraphicsItemGroup *currentContourGraphicsGroup;

	// the current path that the user is painting in the original image
	QPainterPath *currentPath;

	// the contour that corresponds to the current path
	vector<Point> activeContourPoints;

	// the item generated by the path the user is drawing, and added to the scene
	QGraphicsPathItem *currentPathItem;

	// the contours that are present in the scene
	vector<vector<Point> > segmContours;

	// the contour graphic items with their corresponding opencv contour
	QHash<contourGraphicsItem*, vector<Point> > mapGraphicItemsToContours;

	// the user drawing corresponds to a contour graphics item 
	// used to delete the contour item when the drawing is also deleted
	QHash<QGraphicsPathItem*, contourGraphicsItem* > mapUserGraphicsPathItemToContourGraphicItem;

	// the contour that has been identified from the help interface
	// either using flood fill through the wand tool
	// or using manual thresholding from the user
	vector<Point> currentCorrectionSuggestionContour; 

	// the index of the marker in the markers vector that is currently on the center of the image
	int markerAtCenter;

	// modifications have been performed for current image
	bool isModificationPerformed;

	// when the ctrl button is pressed it extends the functionality of the mouse wheel
	// depending on other checked actions
	bool isCtrlPressed;

	// when eraser action is checked the mouse left click is used for erasing contours
	bool isEraserActionChecked;

	// when the auto-eraser mode is on the suggested contour is automatically deleted
	// everytime a user draws or uses the magic wand (upon left mouse click)
	// otherwise the previously suggested contour is moved to the image contour list
	// and the new contour becomes the suggested contour
	bool isAutoEraserOn;

	// when wand action is checked the mouse wheel is used to give the threshold for flood fill?
	bool isWandActionChecked;

	// the ID for this view, either the number of a wavelength or "seg" for segmentation image
	QString viewID;

	// Image display settings
	ImageDisplaySettings m_imageDisplaySettings;

};

#endif