/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef colortransform_h__
#define colortransform_h__


// Qt
#include <QGraphicsView>


class ImageDisplaySettings;


/**
 * Widget that lets user specify a color transformation function.
 */
class ColorTransform : public QGraphicsView {

	Q_OBJECT

public:

	/**
	 * Constructor.
	 */
	ColorTransform(QWidget* parent = 0);

	/**
	 * Update display according to provided settings.
	 */
	void updateDisplay(int blackpoint, int whitepoint);

	/**
	 * Reset display to default settings.
	 */
	void resetDisplay();

	/**
	 * Apply currently set settings to provided settings object.
	 */
	void applySettings(ImageDisplaySettings& settings);

signals:

	/**
	 * Settings were changed.
	 */
	void histogramSettingsChanged();

protected:

	// Events
	void resizeEvent (QResizeEvent* e);
	void mousePressEvent (QMouseEvent* e);
	void mouseReleaseEvent (QMouseEvent* e);
	void mouseMoveEvent (QMouseEvent* e);

private:

	// Update size and position of graphics elements
	void layoutGraphicsElements();

	// Update settings from mouse event
	void updateSettingsFromMouseEvent(QMouseEvent* e, bool blackPoint, bool whitePoint);

	// Elements used to draw color function
	QGraphicsLineItem* m_leftBar;
	QGraphicsLineItem* m_rightBar;
	QGraphicsLineItem* m_centerBar;

	// Mouse pressed while moving to change settings
	bool m_mouseShifting;

	// Current settings
	int m_whitePoint;
	int m_blackPoint;

	// Constants
	static const int LINE_WIDTH;
	static const QColor LINE_COLOR;
};


#endif // colortransform_h__
