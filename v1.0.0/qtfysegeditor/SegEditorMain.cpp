/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/****************************************************************************
**
** @ Laura Skylaki, 24.06.2014
**
**
****************************************************************************/

// PROJECT
#include "SegEditorMain.h"
#include "correctionuisettings.h"
#include "shortcuthelp.h"
#include "qtfyeditor/treeoverviewitemview.h"
#include "qtfyeditor/treemodel.h"
#include "qtfyeditor/treedataparser.h"
#include "qtfyeditor/segmarker.h"
#include "qtfybackend/itrack.h"
#include "qtfybackend/tools.h"
#include "qtfybackend/tttmanager.h"
#include "qtfyanalysis/qtfyimageoperations.h"
#include "qtfyanalysis/positionquantification.h"
#include "qtfygui/qtfyhelp.h"
#include "qtfydata/tatinformation.h"
#include "qtfydata/wavelengthinformation.h"


// QT
#include <QInputDialog>
#include <QColorDialog>
#include <qdebug.h>
#include <QFormLayout>
#include <qscrollbar.h>
#include <qfiledialog.h>
#include <QImageWriter>
#include <QGroupBox>
#include <QThread>
#include <QLayout>
#include <QShortcut>
#include <QRadioButton>

// OPENCV
#include <opencv2/core/core.hpp>

const int SegEditorMain::ZSTACK_INDEX = 1;

SegEditorMain::SegEditorMain(QWidget *parent, const int _segWL, const int _segMethodID, const QString _windowTitle, const bool _stagExperiment) : QMainWindow(parent),
	detectionWL(_segWL),
	segmentationMethodID(_segMethodID),
	staggerExperiment(_stagExperiment)
{
	
	ui.setupUi(this);

	CorrectionUISettings* corrForm = CorrectionUISettings::getInst();
	connect (corrForm, SIGNAL ( corrSettingsUpdated() ), this, SLOT(updateDisplay()));

	this->setWindowTitle(_windowTitle);

	ui.mdiWorkingArea->setOption(QMdiArea::DontMaximizeSubWindowOnActivation);

	timeNotPresent = QPixmap(":/qtfyres/qtfyres/time_not_present.png");
	timeInspectedInactive= QPixmap(":/qtfyres/qtfyres/time_inspected_inactive.png");
	timeInspectedActive= QPixmap(":/qtfyres/qtfyres/time_inspected_active.png");
	timeNotInspectedActive= QPixmap(":/qtfyres/qtfyres/time.png");
	timeNotInspectedInactive= QPixmap(":/qtfyres/qtfyres/time_notinspected_inactive.png");

	// save changes in image every time a new image is loaded ON
	ui.actionAutoSave->setChecked(true);
	this->isAutoSaveON = true;
	ui.actSingleContourOnOff->setChecked(false);
	ui.actionAutoErase->setChecked(true);
	ui.actionPenMode->setChecked(true);
	
	// the initial zoom factor for the views TODO: dependence on image size?
	viewsZoomFactor = 4.0;

	createActions();
	createShortcuts();

	QString initMessage = "**AUTOSAVE ON** Click on the original image to correct a segmentation. Use UP/DOWN arrows to move to the previous/next timepoint.";
	emit updateMsgLog(initMessage, Qt::darkMagenta);
	QMainWindow::statusBar()->showMessage(initMessage);

	this->setAttribute( Qt::WA_DeleteOnClose, true );

	currentCellNumber = -1;
	previousCellNumber = -1;

	// an update here
	// if the segmentation methodID that comes from the selected quantification chunk is different from the default 1
	// it means the user had selected external quantification when they did the quantification 
	// and we can find the ids based on the name of the quantification chunk later
	// but here we set that existing segmentation had been selected
	this->inspectedDetectionWL = _segWL;
	this->inspectedSegmentationMethodID = _segMethodID;
	
	/*if (_segMethodID != 1) {
		PositionQuantification::options.insert("useExistingSegmentation", "true") ;
		PositionQuantification::options.insert("useDefaultExistingSegmentationOptions","false");
		PositionQuantification::options.insert("replaceDetectionWL", QString::number(_segWL));
		PositionQuantification::options.insert("replaceDetectionMethod",  QString::number(_segMethodID));
	}*/

}

SegEditorMain::~SegEditorMain()
{
	
}

void SegEditorMain::createActions()
 {
	 
	 // Menu Tools actions
	 connect(ui.actPenColor, SIGNAL(triggered()), this, SLOT(penColor()));
	 connect(ui.actPenWidth, SIGNAL(triggered()), this, SLOT(penWidth()));
	 connect(ui.actEraserDiameter, SIGNAL(triggered()), this, SLOT(eraserDiameter()));

	 // connect navigation actions in Tools
	 connect(ui.actGoNextTimepoint, SIGNAL(triggered()), this, SLOT(navigationAction()));
	 connect(ui.actGoPreviousTimepoint, SIGNAL(triggered()), this, SLOT(navigationAction()));
	 connect(ui.actionCenter, SIGNAL(triggered()), this, SLOT(navigationAction()));

	 connect(ui.actionSaveSingleCell, SIGNAL(triggered()), this, SLOT(manualSave()));
	 connect(ui.actionSaveAllCells, SIGNAL(triggered()), this, SLOT(manualSaveAll()));

	 connect(ui.actionEraserMode, SIGNAL(toggled(bool)), this, SLOT(setEraserState(bool)));
	 connect(ui.actionAutoErase, SIGNAL(toggled(bool)), this, SLOT(setAutoEraserState(bool)));
	 connect(ui.actionWand, SIGNAL(toggled(bool)), this, SLOT(setWandState(bool)));
	 connect(ui.actionPenMode, SIGNAL (toggled(bool)), this, SLOT (setPenState(bool)));
	 connect(ui.actSingleContourOnOff, SIGNAL(toggled (bool)), this, SLOT(showSingleContour(bool)));
	  connect(ui.actionResetOriginalContours, SIGNAL(triggered()), this, SLOT(resetToOriginalContours()));

	 connect(ui.pbtResetWandOptions, SIGNAL(clicked()), this, SLOT(resetWandOptions()));
	 connect(ui.pbtHelpWandOptions, SIGNAL(clicked()), this, SLOT(showHelpWandOptions()));

	
	 // Tree dock actions
	 // connect selection of position/timepoint to display of relevant images

	 connect(ui.actionAutoSave, SIGNAL (toggled(bool)), this, SLOT (setAutoSave(bool)));

	 connect(ui.actionDisplayOptions, SIGNAL (triggered()), this, SLOT (openSegmentationEditorDisplayOptions()));

	 connect(ui.actExit, SIGNAL(triggered()), this, SLOT(close()));

	 // adjust the contrast of a view
	 connect(ui.actHistogram, SIGNAL(triggered()), this, SLOT(displayHistogramSettingsClicked()));

	 connect(ui.actionShortcutsMemo, SIGNAL(triggered()), this, SLOT (displayShortcutMemo()));

	 connect(ui.pbtResetDisplaySettings , SIGNAL(clicked()), this, SLOT(resetDisplaySettings()));
	 connect(ui.grvSettings, SIGNAL(histogramSettingsChanged()), this, SLOT(histogramSettingsChanged()));

 }

void SegEditorMain::createShortcuts()
 {
	 // alternative shortcuts
	 QShortcut* shrPreviousCell = new QShortcut(QKeySequence(Qt::Key_A), this);
	 connect(shrPreviousCell, SIGNAL(activated()), ui.actGoPreviousTimepoint, SLOT(trigger()));

	 QShortcut* shrNextCell = new QShortcut(QKeySequence(Qt::Key_D), this);
	 connect(shrNextCell, SIGNAL(activated()), ui.actGoNextTimepoint, SLOT(trigger()));

	 QShortcut* shrSetTimepointActive = new QShortcut(QKeySequence("CTRL+A"), this);
	 connect(shrSetTimepointActive, SIGNAL(activated()), this, SLOT(setTimepointActive()));

	 QShortcut* shrSetTimepointInactive = new QShortcut(QKeySequence("CTRL+SHIFT+A"), this);
	 connect(shrSetTimepointInactive, SIGNAL(activated()), this, SLOT(setTimepointInactive()));

	 QShortcut* shrSetTimepointInspected = new QShortcut(QKeySequence("CTRL+Z"), this);
	 connect(shrSetTimepointInspected, SIGNAL(activated()), this, SLOT(setTimepointInspected()));

	 QShortcut* shrSetTimepointNotInspected = new QShortcut(QKeySequence("CTRL+SHIFT+Z"), this);
	 connect(shrSetTimepointNotInspected, SIGNAL(activated()), this, SLOT(setTimepointNotInspected()));

	 QShortcut* shrExitSegEditor = new QShortcut(QKeySequence(QKeySequence::Quit), this);
	 connect(shrExitSegEditor, SIGNAL(activated()), ui.actExit, SLOT(trigger()));
 }

void SegEditorMain::setAutoSave(bool on) 
{
	if (on) {
		QString initMessage = "**AUTOSAVE ON** Click on the original image to correct a segmentation. Use LEFT/RIGHT arrows or keys A/D to move to the previous/next timepoint. All changes will be saved and the timepoint will be set inspected.";
		emit updateMsgLog(initMessage, Qt::darkMagenta);
		QMainWindow::statusBar()->showMessage(initMessage);
	}
	else {
		QString initMessage = "**AUTOSAVE OFF** Please remember to save your changes manually (CTRL+S). Any changes not saved will be lost!";
		emit updateMsgLog(initMessage, Qt::darkMagenta);
		QMainWindow::statusBar()->showMessage(initMessage);
	}

	this->isAutoSaveON=on;
}

void SegEditorMain::setupInterface()
{

	// are there any subwindows available?
	QList<QMdiSubWindow*> windows = ui.mdiWorkingArea->subWindowList();

	/*foreach(QMdiSubWindow* window, windows)
	{
		window->close();
	}
	this->wavelengthViews.clear();*/
	
	// iterate through all wavelengths to display
	// for each wavelength create:
	// 1- a group box widget
	// a graphic view
	// read the image
	// add to the mdi area

	int windowSize = PositionQuantification::options.value("windowSize").toInt() + 200;
	int mdiAreaHeight = ui.mdiWorkingArea->height();
	int mdiAreaWidth = ui.mdiWorkingArea->width();

	QVector<WavelengthInformation> wavelengthsInfo = TATInformation::getInst()->getAvailableWavelengthInfo();

	// close unwanted subwindows
	foreach(QMdiSubWindow* window, windows) {

		// get the wavelength of the window
		bool conversionState = false;
		QString windowName = window->windowTitle();
		int indexOfFirstParenthesis = windowName.indexOf("(");
		windowName.truncate(indexOfFirstParenthesis);
		windowName.remove("Channel ");
		windowName.trimmed();

		int walengthID = windowName.toInt(&conversionState); //setWindowTitle("Wavelength: " + wlID);

		if (conversionState && !CorrectionUISettings::corrWavelengthsToDisplay.contains(walengthID)) {
			window->close();
		}		
	}

	
	bool doOnce = true;
	QMdiSubWindow* focusWindow = NULL;

	QVectorIterator<int> it(CorrectionUISettings::corrWavelengthsToDisplay);
	it.toBack();
	while (it.hasPrevious()){
		// create the graphic views for each wavelength image
		QString wlID = QString::number(it.previous());

		QString windowTitle = "Channel " + wlID;

		// the view has been created already, don't create it again
		if (!this->wavelengthViews.contains(wlID)) {
			QString comment = "";
			foreach (WavelengthInformation wlInfo, wavelengthsInfo) {
				if (wlInfo.isInitialized()) {
					int wlNumber = wlInfo.getWavelengthNumber();
					if (wlNumber == wlID.toInt()) {
						windowTitle.append(" (");
						windowTitle.append(wlInfo.getComment());
						windowTitle.append(")");
					}
				}
			}

				// create the mdi sub-windows
			myMdiSubWindow* subWindow = new myMdiSubWindow(ui.mdiWorkingArea);
			subWindow->resize(windowSize,windowSize); 
			subWindow->setWindowTitle(windowTitle);
			subWindow->setOption(QMdiSubWindow::RubberBandMove);
			subWindow->setAttribute(Qt::WA_DeleteOnClose);
			subWindow->setAttributeName(wlID);

			connect(subWindow, SIGNAL (closed( myMdiSubWindow*)), this, SLOT(closeSubwindow(myMdiSubWindow*)));
			connect(subWindow, SIGNAL (aboutToActivate()), this, SLOT(updateDisplaySettingsForm()));

			// create the graphics view
			ImgGraphicsView* wlDisplayView = new ImgGraphicsView(subWindow, true);
			wlDisplayView->setViewID(wlID);
			wlDisplayView->setZoomFactor(viewsZoomFactor);
			wlDisplayView->setScale(viewsZoomFactor);
			wlDisplayView->setAutoEraserState(ui.actionAutoErase->isChecked());
			wlDisplayView->setAutoSave(ui.actionAutoSave->isChecked());

			if (ui.actSingleContourOnOff->isChecked())
				wlDisplayView->setContoursGroupVisible(false);
			else
				wlDisplayView->setContoursGroupVisible(true);

			
			this->wavelengthViews.insert(wlID, wlDisplayView);	

			/*QGridLayout *gridSub = new QGridLayout();
			subWindow->setLayout(gridSub);*/
			subWindow->layout()->addWidget(wlDisplayView);
			ui.mdiWorkingArea->addSubWindow(subWindow);
			subWindow->show();

			if (doOnce) {
				focusWindow = subWindow;
				doOnce = false;
			}
		}

		if (doOnce && !ui.mdiWorkingArea->subWindowList().empty()) {
			focusWindow = ui.mdiWorkingArea->subWindowList().at(0);
			doOnce = false;
		}
		
	}
	
	// create segmentation image view, always placed on the top left position of the grid layout	
	if (!this->wavelengthViews.contains("seg")) {
		// create the mdi sub-windows
		myMdiSubWindow* subWindow = new myMdiSubWindow(ui.mdiWorkingArea);
		subWindow->resize(windowSize,windowSize); 
		subWindow->setWindowTitle("Segmentation Image");
		subWindow->setOption(QMdiSubWindow::RubberBandMove);
		subWindow->setAttribute(Qt::WA_DeleteOnClose);
		subWindow->setAttributeName("seg");
		// prevent the segmentation window from closing
		/*subWindow->setWindowFlags((windowFlags() | Qt::CustomizeWindowHint)
			& ~Qt::WindowCloseButtonHint);*/
		

		connect(subWindow, SIGNAL (closed( myMdiSubWindow*)), this, SLOT(closeSubwindow(myMdiSubWindow*)));
	
		ImgGraphicsView* wlSegmentationView = new ImgGraphicsView(subWindow, false);
		wlSegmentationView->setViewID("seg");
		// start scale zoomed
		wlSegmentationView->setZoomFactor(viewsZoomFactor);
		wlSegmentationView->setScale(viewsZoomFactor);
		wlSegmentationView->setAutoEraserState(ui.actionAutoErase->isChecked());
		wlSegmentationView->setAutoSave(ui.actionAutoSave->isChecked());

		if (ui.actSingleContourOnOff->isChecked())
			wlSegmentationView->setContoursGroupVisible(false);
		else
			wlSegmentationView->setContoursGroupVisible(true);

		this->wavelengthViews.insert("seg", wlSegmentationView);

		/*QGridLayout *gridSub = new QGridLayout();
		subWindow->setLayout(gridSub);*/
		subWindow->layout()->addWidget(wlSegmentationView);
		ui.mdiWorkingArea->addSubWindow(subWindow, Qt::Window
		/*| Qt::WindowMinimizeButtonHint
		| Qt::WindowMaximizeButtonHint*/
		| Qt::CustomizeWindowHint );
		/*subWindow->setWindowFlags(Qt::Window
		| Qt::WindowMinimizeButtonHint
		| Qt::WindowMaximizeButtonHint
		| Qt::CustomizeWindowHint);*/
		subWindow->show();
	}

	ui.mdiWorkingArea->setWindowTitle("Workspace");
	// ui.mdiWorkingArea->tileSubWindows();

	// force the height of all sub windows to be the same when the number of subwindows is odd
	QList<QMdiSubWindow*> myWindows = ui.mdiWorkingArea->subWindowList();

	// get the minimum height of available windows, since we want to bring all windows heights to the minimum
	QVector<int> winSizeHeight;
	foreach (QMdiSubWindow* aWindow, myWindows) {
		winSizeHeight.append(aWindow->size().height());
	}

	qSort(winSizeHeight);
	int minWinHeight = winSizeHeight.at(0);

	// resize the windows
	foreach (QMdiSubWindow* aWindow, myWindows) {
		aWindow->resize(aWindow->size().width(), minWinHeight); 
	}

	if (focusWindow != NULL)
		//sets the active subwindow to the first wavelength window
		ui.mdiWorkingArea->setActiveSubWindow(focusWindow);

	// synchronize all graphic views
	synchronizeViews();

	// views signals
	initializeViewsSignals();

	// connect the display widgets to pass new contours

}

void SegEditorMain::initializeViewsSignals()
{

	// For all views
	qDebug() << "Initializing view signals...";

	QList<QString> wls = this->wavelengthViews.keys();
	QList<QString>::iterator it;

	for(it = wls.begin(); it != wls.end(); it++)
	{
		ImgGraphicsView* grpView = this->wavelengthViews.value(*it);
		connect(ui.actionAutoSave, SIGNAL (toggled(bool)), grpView, SLOT (setAutoSave(bool)));
		connect(ui.actionAutoErase, SIGNAL (toggled(bool)), grpView, SLOT (setAutoEraserState(bool)));
		connect(ui.actionEraserMode, SIGNAL (toggled(bool)), grpView, SLOT (setEraserState(bool)));
		connect(ui.actionWand, SIGNAL (toggled(bool)), grpView, SLOT (setWandState(bool)));
		// wand options
		connect(ui.spbLowerDiff, SIGNAL(valueChanged(int)), grpView, SLOT(updateWandLowerDiff(int)));
		connect(ui.spbUpperDiff, SIGNAL(valueChanged(int)), grpView, SLOT(updateWandUpperDiff(int)));

		connect(grpView, SIGNAL(updateMsgLog(QString, QColor )), this, SIGNAL(updateMsgLog(QString, QColor )));

		// Tree dock actions
		// connect the arrow keys to the navigation of the tree overview
		connect (grpView, SIGNAL(treeOverviewIndexChangedUp()), m_tvwDataTreeOverview, SLOT (outIndexChangeUp()));
		connect (grpView, SIGNAL(treeOverviewIndexChangedDown()), m_tvwDataTreeOverview, SLOT (outIndexChangeDown()));
		connect (grpView, SIGNAL(newCellInFocus(SegMarker)), this, SLOT (newCellInFocus(SegMarker)));
		connect (this, SIGNAL(newMarkerInFocus(bool)), grpView, SLOT(updateMarkerPosition(bool)));
	}

}


void SegEditorMain::synchronizeViews()
{
	// iterate through all graphic views
	int numOfViews = this->wavelengthViews.keys().size();
	QList<ImgGraphicsView*> views = this->wavelengthViews.values();

	ImgGraphicsView* segView = this->wavelengthViews.value("seg");

	// do all pairwise connections for the views
	for (int i=0; i < numOfViews; i++) {

		QString viewID1 = views.at(i)->getViewID();

		/*connect(views.at(i)->horizontalScrollBar(), SIGNAL(valueChanged(int)), views.at(j)->horizontalScrollBar(), SLOT(setValue(int)));
		connect(views.at(i)->verticalScrollBar(), SIGNAL(valueChanged(int)), views.at(j)->verticalScrollBar(), SLOT(setValue(int)));*/
		connect(views.at(i)->horizontalScrollBar(), SIGNAL(valueChanged(int)), views.at(i), SLOT(hScrollBarChanged(int)));
		connect(views.at(i)->verticalScrollBar(), SIGNAL(valueChanged(int)), views.at(i), SLOT(vScrollBarChanged(int)));
		connect(views.at(i), SIGNAL(hScrollBarUpdate(int, bool, QString)), this, SLOT(updateHScrollBarInViews(int, bool, QString)));
		connect(views.at(i), SIGNAL(vScrollBarUpdate(int, bool, QString)), this, SLOT(updateVScrollBarInViews(int, bool, QString)));
		connect(views.at(i), SIGNAL(zoomFactorChanged(qreal)), this, SLOT(updateViewsZoomFactor(qreal)));

		for (int j=0; j<numOfViews; j++) {

			QString viewID2 = views.at(j)->getViewID();

			qDebug() << "Connecting view " << viewID1 << "to view " << viewID2;

			 if (i!=j) {
				connect(views.at(i), SIGNAL(scaleChanged(qreal, int, int)), views.at(j), SLOT(setScale(qreal, int, int)));
			 }

			 // also connect the signals between display views 
			 // when a contour is created/deleted in one of the display views
			 // all display views should be updated (but the segmentation view only once - see if block after this)
			 if (views.at(i)!=segView && i!=j && views.at(j)!=segView) {
				 connect(views.at(i), SIGNAL(saveNewContour(vector<Point>, QColor, bool )), views.at(j), SLOT(addNewContourToImage(vector<Point>, QColor, bool )));
				 connect(views.at(i), SIGNAL(deleteContour(vector<Point> )), views.at(j), SLOT(deleteContourFromImage(vector<Point> )));
				  connect(views.at(i), SIGNAL(moveCurrentContour(vector<Point> )), views.at(j), SLOT(moveCurrentContourToImageContours(vector<Point> )));
				  connect(views.at(i), SIGNAL (clearSuggestionContour()), views.at(j), SLOT(clearCurrentCorrectionSuggestionContour()));
				  connect(views.at(i), SIGNAL (setSuggestionContour(vector<Point>)), views.at(j), SLOT(setCurrentCorrectionSuggestionContour(vector<Point>)));
			 }
		}

		// also connect the signal for passing the contours around
		// connect the two widgets to pass new contours

		if (views.at(i)!=segView) {
			connect(views.at(i), SIGNAL(saveNewContour(vector<Point>, QColor, bool )), segView, SLOT(addNewContourToImage(vector<Point>, QColor, bool )));
			connect(views.at(i), SIGNAL(deleteContour(vector<Point> )), segView, SLOT(deleteContourFromImage(vector<Point> )));
			connect(views.at(i), SIGNAL(moveCurrentContour(vector<Point> )), segView, SLOT(moveCurrentContourToImageContours(vector<Point> )));
			connect(views.at(i), SIGNAL (clearSuggestionContour()), segView, SLOT(clearCurrentCorrectionSuggestionContour()));
			connect(views.at(i), SIGNAL (setSuggestionContour(vector<Point>)), segView, SLOT(setCurrentCorrectionSuggestionContour(vector<Point>)));
		}		
	}

}


void SegEditorMain::initializeDataQTFy(TreeOverviewItemView* _treeview, TreeModel* _treeModel)
{
	this->m_TreeDataModel = _treeModel;
	this->m_tvwDataTreeOverview = _treeview;
	
	// load initial image from first track point
	//QModelIndex firstIndex = this->m_TreeDataModel->index(0,0);
	// updateImageViewerQTFy(_selectedIndex);

	this->setCursor(Qt::ArrowCursor);
	
	emit updateMsgLog(QString("Initializing tracked cells in available trees..."), Qt::black);
	
}

/************************************************************************/
/* updates the selected index of the treeview                           */
/************************************************************************/
void SegEditorMain::updateSelectedIndex (const QModelIndex& _selectedIndex)
{
	this->m_tvwDataTreeOverview->setCurrentIndex(_selectedIndex);
	updateImageViewerQTFy(_selectedIndex);
}

void SegEditorMain::setCellsPerTimepoint (QMap<QString, QMap<int, QVector<ITrack*> > > _cellsPerTimepoint)
{
	this->cellsPerTimepoint = _cellsPerTimepoint;
}

void SegEditorMain::setCellsPerTree (QMap< QString, QMap< QString, ITrack* > > _cellsPerTree)
{
	this->cellsPerTree = _cellsPerTree;
}

// SLOT
void SegEditorMain::closeSubwindow( myMdiSubWindow* _subwindow )
{
	bool conversionState = false;
	QString windowName = _subwindow->windowTitle();
	int indexOfFirstParenthesis = windowName.indexOf("(");
	windowName.truncate(indexOfFirstParenthesis);
	windowName.remove("Channel ");
	windowName.trimmed();

	int walengthID = windowName.toInt(&conversionState); //setWindowTitle("Wavelength: " + wlID);

	if (conversionState) {
		// wavelength subwindow was closed
		this->wavelengthViews.remove(QString::number(walengthID));	
		int index = CorrectionUISettings::corrWavelengthsToDisplay.indexOf(walengthID);
		if (index >= 0) {
			CorrectionUISettings::corrWavelengthsToDisplay.remove(index);
			CorrectionUISettings::getInst()->updateDisplay();
		}
	}
	else{
		this->wavelengthViews.remove("seg");
	}
}


// SLOT
void SegEditorMain::updateImageViewerQTFy(const QModelIndex& index)
{	
	setCursor(Qt::BusyCursor);

	// check first if need to save changes
	ImgGraphicsView* grpView = this->wavelengthViews.value("seg");

	if (!grpView)
		return;

	QString position="";
	QString experimentName = "";
	QString treeName = "";
	QString cellNum="";
	QString timepoint="";

	if (grpView->isModifiedByUser() && this->isAutoSaveON) {
		
		QString path = this->pathToExperiments;
		QModelIndex index = this->previousIndex;

		if (index.isValid()) {

			TreeItem* selectedItem = static_cast<TreeItem*>(index.internalPointer());

			QVector<QString> selectionInfo = getIdentificationInfo(selectedItem);
			experimentName = selectionInfo.at(0);
			position = selectionInfo.at(1);
			treeName = selectionInfo.at(2); 
			cellNum = selectionInfo.at(3);
			timepoint = selectionInfo.at(4);
			
			QString pathToImage = path + "\\" + position + "\\";
			QString outputfilePathImage = pathToImage + "Segmentation\\" + this->imgFileNameSegmented;

			// save output files here
			QImage img = grpView->getImage(); 
			QImage::Format test = img.format();
			
			if (img.isNull() || img.format() == QImage::Format_Invalid) {
				grpView->setIsModifiedByUser(false);
				emit updateMsgLog(QString("Error > Quantification not saved. Invalid image format for segmentation image %1...").arg(this->imgFileNameSegmented), Qt::red);
				return;
			}

			if (!QTFyImageOperations::saveQImageToFile(img, outputfilePathImage)) {
				emit updateMsgLog(QString("Error > Quantification not saved. Invalid image format for segmentation image %1...").arg(this->imgFileNameSegmented), Qt::red);
				return;
			}

			// QTFyImageOperations::saveQImageToFile(img, outputfilePathImage);

			updateTimepointQuantification(position, treeName, cellNum, timepoint, true);
			updateTimepointStatusFrame(treeName, cellNum, timepoint);
			QString msg = QString("Focus set on cell: %1, timepoint: %2").arg(cellNum).arg(timepoint);
			QMainWindow::statusBar()->showMessage(msg);
		}
	}

	

	TreeItem *selectedItem;

	if (!index.isValid())
		selectedItem = this->m_TreeDataModel->getRoot();
	else
		selectedItem = static_cast<TreeItem*>(index.internalPointer());

	// need to check if the selected item can provide a timepoint
	// i.e. there are cells created automatically upon division of parent
	// but never tracked. Therefore, they cannot be displayed or corrected by the segmentation editor
	// we need to check here whether there is a grand-child/child of level 3 (timepoint) for the selected item
	// we do this in every column for the next n-1 levels

	QVector<QString> selectionInfo = getIdentificationInfo(selectedItem);
	experimentName = selectionInfo.at(0);
	position = selectionInfo.at(1);
	treeName = selectionInfo.at(2); 
	cellNum = selectionInfo.at(3);
	timepoint = selectionInfo.at(4);

	int zStackIndex = ZSTACK_INDEX;
	
	QPointF markPoint = getCurrentTrackPoint(treeName, cellNum, timepoint.toInt());

	// display the new images
	displaySelectedImage(experimentName, position, timepoint.toInt(), zStackIndex, detectionWL, markPoint);

	// plot markers of tracked objects (cells)
	createMarkersForObjectsQTFy(markPoint, treeName, cellNum, timepoint.toInt());

	currentCellNumber = cellNum.toInt();

	if (previousCellNumber < 0 || currentCellNumber!=previousCellNumber) {
		updateTimepointStatusFrame(treeName, cellNum, timepoint);
		previousCellNumber = currentCellNumber;
	}
	else {
		updateTimepointStatus(timepoint);
	}

	QString msg = QString("Focus set on cell: %1, timepoint: %2").arg(cellNum).arg(timepoint);
	QMainWindow::statusBar()->showMessage(msg);

	previousIndex = index;

	setCursor(Qt::CustomCursor);

}

////SLOT: display the image that is currently activated in the Experiment tree view
void SegEditorMain::displaySelectedImage(const QString expName, const QString pos, const int timepoint, const int zindex, const int wavelength, const QPointF _markPoint)
{
	
	QString path = this->pathToExperiments;

	QString pathToImage = path + "\\"  + pos + "\\";
	QString pathToSegmentationImage = pathToImage + "segmentation\\";

	// get the image for each wavelength
	QList<QString> wls = this->wavelengthViews.keys();
	QList<QString>::iterator it;

	int width = 0;
	int height = 0;

	for(it = wls.begin(); it != wls.end(); it++)
	{
		if (*it!="seg") {
			int wlID = (*it).toInt();
			QString imageFileName = QTFyImageOperations::getImageFileName(pos, timepoint, zindex, wlID, 3, 2, "png");
			QString imagePath = pathToImage + imageFileName;

			// find the image file
			QFile fileImageRaw(imagePath);
			
			// if it doesn't exist
			if (!fileImageRaw.exists()) {

				QString message = "> Error: Looking for file: " + imageFileName + " in " + pathToImage + ". The file cannot be found. Please inspect the path to experiments folder and correct if necessary.";
				emit updateMsgLog(message, Qt::darkRed);		
				ImgGraphicsView* grpView = this->wavelengthViews.value(*it);
				grpView->loadEmptyImage(grpView->getImage().width(), grpView->getImage().height(), Qt::white);
			}
			else {

				// Update display
				ImgGraphicsView* grpView = this->wavelengthViews.value(*it);
				QTime t;
				t.start();

				cv::Mat wlImage = cv::imread(imagePath.toStdString(), CV_LOAD_IMAGE_ANYDEPTH | CV_LOAD_IMAGE_GRAYSCALE);

				qDebug() << QString("Time elapsed: %1 ms for loading image wl %2").arg(t.elapsed()).arg(imageFileName);
				
				if (wlImage.empty()) {
					grpView->loadEmptyImage(grpView->getImage().width(), grpView->getImage().height(), Qt::white);
				}
				else {
					grpView->loadImage(wlImage);
				}

				emit updateMsgLog(QString("> Displaying original image: %1").arg(imageFileName), Qt::darkGreen);

				width = grpView->getImage().width();
				height = grpView->getImage().height();
			}

		}
	}

	//load segmentation image
	QString segImageFileName;
	QString segImagePath;
	bool imageNotFound = false;

	// check if using existing segmentation is allowed
	//if (PositionQuantification::options.value("useExistingSegmentation") == "true") {

	//	int existingDetectionWL = detectionWL;
	//	int existingSegMethod = segmentationMethodID;

	//	// check if the default options are going to be used for existing segmentation
	//	// default means that the detection wavelength and segmentation method are defined by the combination
	//	// that the user has selected, segmentation method in this scenario is always Otsu Local (mehtodID = 1)
	//	if (PositionQuantification::options.value("useDefaultExistingSegmentationOptions") == "false") {
	//		existingDetectionWL = PositionQuantification::options.value("replaceDetectionWL").toInt();
	//		existingSegMethod = PositionQuantification::options.value("replaceDetectionMethod").toInt();
	//	}

	//	segImageFileName = QTFyImageOperations::getSegmentationImageFileName(pos, timepoint, zindex, existingDetectionWL, existingSegMethod, 3, 2, "png");
		segImageFileName = QTFyImageOperations::getSegmentationImageFileName(pos, timepoint, zindex, detectionWL, segmentationMethodID, 3, 2, "png");
		segImagePath = pathToSegmentationImage + segImageFileName;
		QFile fileImageSeg(segImagePath);

		if (fileImageSeg.exists()) {
			emit updateMsgLog(QString("> Found existing segmentation image: %1...").arg(segImageFileName), Qt::black);
		} 
		else {
			emit updateMsgLog(QString("> Warning: Could not find segmentation image %1. Looking for default parameters instead...").arg(segImageFileName), Qt::darkBlue);
			segImageFileName = QTFyImageOperations::getSegmentationImageFileName(pos, timepoint, zindex, detectionWL, segmentationMethodID, 3, 2, "png"); 
			segImagePath = pathToSegmentationImage + segImageFileName;
			QFile fileImageSeg(segImagePath);
			if (!fileImageSeg.exists()) {
				QString message = "> Error: Looking for file: \n" + segImageFileName + "\nin\n" + pathToImage + "segmentation\\" + "\nThe file cannot be found. Generating an empty segmentation image instead...";
				emit updateMsgLog(message, Qt::darkRed);	
				imageNotFound = true;
			}
		}

	if (imageNotFound) {		
		this->imgFileNameSegmented = segImageFileName;
		ImgGraphicsView* grpView = this->wavelengthViews.value("seg");
		grpView->clearImageContours();
		grpView->loadEmptyImage(width, height, Qt::black);
		this->originalContours.clear();

		if (height == 0 || width == 0) 
			emit updateMsgLog(QString("Attention! No images have been found for this timepoint. Please proceed to another timepoint."), Qt::red);
		
	}
	else {
		// Update display
		ImgGraphicsView* grpView = this->wavelengthViews.value("seg");
		
		QTime t;
		t.start();

		// load image
		cv::Mat maskImg = cv::imread(segImagePath.toStdString(), CV_LOAD_IMAGE_ANYDEPTH | CV_LOAD_IMAGE_GRAYSCALE);

		qDebug() << QString("Time elapsed: %1 ms for loading image wl %2").arg(t.elapsed()).arg(segImageFileName);
			
		if (!grpView->loadImage(maskImg)) {
			this->imgFileNameSegmented = segImageFileName;
			grpView->clearImageContours();
			grpView->loadEmptyImage(grpView->getImage().width(), grpView->getImage().height(), Qt::black);
			this->originalContours.clear();

			if (grpView->getImage().width() == 0 || grpView->getImage().height() == 0)
				QMainWindow::statusBar()->showMessage(QString("Attention! No images have been found for this timepoint. Please proceed to another timepoint."));

			return;
		}
	
		this->imgFileNameSegmented = segImageFileName;

		emit updateMsgLog(QString("> Displaying segmentation image: %1...").arg(segImageFileName), Qt::black);	

		// calculate the contours in the new images
		contourOperations(maskImg, _markPoint);
	}

}

QPointF SegEditorMain::getCurrentTrackPoint(QString _treeName, QString _cellID, int timepoint)
{
	
	ITrack* selectedCell = this->cellsPerTree.value(_treeName).value(_cellID);

	QString pos = selectedCell->getITree()->getPositionIndex();
	QString findPos = Tools::getPositionNumberFromString(pos);
	QString experimentName = selectedCell->getITree()->getExperimentName();

	// get cell number
	int cellNum = selectedCell->getTrackNumber();


	// create cell name from tree number and cell number
	QString cellName = _treeName + QString("_%1").arg(_cellID);

	// get the trackpoint for this timepoint
	ITrackPoint* tp = selectedCell->getTrackPointByTimePoint(timepoint);

	if (!tp)
		return QPointF(0,0);

	// get the X,Y coordinates from the track data
	int Xcoord = tp->getX();
	int Ycoord = tp->getY();

	QPoint absCoordPoint(Xcoord, Ycoord);

	TTTPositionManager* tttpm = TTTManager::getInst().getPositionManager(pos);

	//QPointF mark = tttpm->getDisplays().at (0).calcTransformedCoords (absCoordPoint, &tttpm->positionInformation);
	QPointF markPoint = QTFyImageOperations::calcTransformedCoords (absCoordPoint, &tttpm->positionInformation, TTTManager::getInst().coordinateSystemIsInverted());

	return markPoint;
}


void SegEditorMain::createMarkersForObjectsQTFy(QPointF _markPoint, QString _treeName, QString _cellID, int timepoint)
{
	ITrack* selectedCell = this->cellsPerTree.value(_treeName).value(_cellID);

	QString pos = selectedCell->getITree()->getPositionIndex();
	QString findPos = Tools::getPositionNumberFromString(pos);
	QString experimentName = selectedCell->getITree()->getExperimentName();

	// get cell number
	int cellNum = selectedCell->getTrackNumber();


	// create cell name from tree number and cell number
	QString cellName = _treeName + QString("_%1").arg(_cellID);

	QVector< SegMarker > markers;

	SegMarker mark (_markPoint.x(), _markPoint.y(), cellNum, _treeName, experimentName, pos, timepoint, cellName, QColor(255, 0, 0, 128)); 

	//QPair<QPoint, QString> pair(mark.toPoint(), cellName);
	markers.append(mark);


	emit updateMsgLog(QString("> Setting the markers for segmented objects."), Qt::black);
	plotMarkersForObjects (markers);
}

void SegEditorMain::plotMarkersForObjects(QVector<SegMarker> markers)
{
	QList<QString> wls = this->wavelengthViews.keys();
	QList<QString>::iterator it;

	for(it = wls.begin(); it != wls.end(); it++)
	{
		int wlID = (*it).toInt();
		// Update display
		ImgGraphicsView* grpView = this->wavelengthViews.value(*it);
		grpView->setPlotMarkers(markers);
		grpView->updateContourColors();

		grpView->setSceneRect(mrkCurrentCell.getX() - 150, mrkCurrentCell.getY() - 150, 300, 300);

		grpView->centerOn(mrkCurrentCell.getX(), mrkCurrentCell.getY());

	}
}

void SegEditorMain::showSingleContour (bool on)
{
	foreach (QString viewID, this->wavelengthViews.keys()) {
		ImgGraphicsView* view = this->wavelengthViews.value(viewID);
		view->setContoursGroupVisible(!on);
	}
}

void SegEditorMain::contourOperations(Mat& _maskImg, QPointF _markPoint)
{
	QTime t;
	t.start();
	
	// get the contours in the segmentation image
	vector<vector<Point> > imgContours;
	std::vector<Point> currentContour;

	// not using the max size here - correct method in segmentation help
	if (_maskImg.depth() == CV_8U )
		imgContours = QTFyImageOperations::getImageContours(_maskImg, 5, 10000, false, false); //TODO:: correct min and max filter false?
	else if (_maskImg.depth() == CV_16U)
		imgContours = QTFyImageOperations::getLabeledComponentsContours(_maskImg, 5, 10000, false, false);

	// filter for the current cell only
	// if at least one contour has been found
	if (!imgContours.empty()) {
		// find contour containing or within distance from trackpoint
		// TODO:: make the distance here user selectable?
		// distance depends on the size of the window, winSize/2
		double maxDistance = PositionQuantification::options.value("maxDistanceFromTrackpoint").toInt(); // PositionQuantification::options.value("windowSize").toDouble()/3;
		// need to adjust the trackpoint to the ROI image

		currentContour = QTFyImageOperations::getClosestContour(imgContours, Point(_markPoint.x(), _markPoint.y()), maxDistance);
		imgContours.erase(std::remove(imgContours.begin(), imgContours.end(), currentContour), imgContours.end());
	}


	qDebug() << QString("Time elapsed: %1 ms for finding contours in image").arg(t.elapsed());

	QList<QString> wls = this->wavelengthViews.keys();
	QList<QString>::iterator it;

	for(it = wls.begin(); it != wls.end(); it++)
	{
			int wlID = (*it).toInt();
			// Update display
			ImgGraphicsView* grpView = this->wavelengthViews.value(*it);
			grpView->setImageContours(imgContours);
			grpView->setCurrentContour(currentContour);

			// set new contours visible according to user's choice

			if (ui.actSingleContourOnOff->isChecked())
				grpView->setContoursGroupVisible(false);
			else
				grpView->setContoursGroupVisible(true);

	}

	this->originalContours = imgContours;
	this->originalContours.push_back(currentContour);

	emit updateMsgLog(QString("> Cell contour read from segmentation image..."), Qt::black);
}

// SLOT 
void SegEditorMain::resetToOriginalContours()
{
	QModelIndex selectedIndex;
	if (this->m_tvwDataTreeOverview->selectedIndexes().size() == 0)
		selectedIndex = this->m_TreeDataModel->index(0,0);
	else
		selectedIndex  = this->m_tvwDataTreeOverview->selectedIndexes().at(0);

	if (selectedIndex.isValid()) {

		TreeItem* selectedItem = static_cast<TreeItem*>(selectedIndex.internalPointer());
		QVector<QString> selectionInfo = getIdentificationInfo(selectedItem);
		QString experimentName = selectionInfo.at(0);
		QString position = selectionInfo.at(1);
		QString treeName = selectionInfo.at(2); 
		QString cellNum = selectionInfo.at(3);
		QString timepoint = selectionInfo.at(4);

		int zStackIndex = ZSTACK_INDEX;

		QPointF markPoint = getCurrentTrackPoint(treeName, cellNum, timepoint.toInt());
		QList<QString> wls = this->wavelengthViews.keys();

		// update the segmentation view
		ImgGraphicsView* segView = wavelengthViews.value("seg");
		Mat oldMask = segView->getMatImage();

		// image not loaded properly or not available
		if (oldMask.rows == 0 || oldMask.cols == 0)
			return;

		Mat updateMask = Mat::zeros(oldMask.rows, oldMask.cols, oldMask.type());
		std::vector<Point> currentContour;

		// filter for the current cell only
		// if at least one contour has been found
		if (!originalContours.empty()) {
			// find contour containing or within distance from trackpoint
			// TODO:: make the distance here user selectable?
			// distance depends on the size of the window, winSize/2
			double maxDistance = PositionQuantification::options.value("maxDistanceFromTrackpoint").toInt(); // PositionQuantification::options.value("windowSize").toDouble()/3;
			// need to adjust the trackpoint to the ROI image

			currentContour = QTFyImageOperations::getClosestContour(originalContours, Point(markPoint.x(), markPoint.y()), maxDistance);
		}

		QTFyImageOperations::plotContours(updateMask, originalContours);
		segView->loadImage(updateMask);

		originalContours.erase(std::remove(originalContours.begin(), originalContours.end(), currentContour), originalContours.end());

		QList<QString>::iterator it;
		for(it = wls.begin(); it != wls.end(); it++) {
			int wlID = (*it).toInt();
			// Update display
			ImgGraphicsView* grpView = this->wavelengthViews.value(*it);
			grpView->clearImageContours();
			grpView->setImageContours(this->originalContours);
			grpView->setCurrentContour(currentContour);

			// set new contours visible according to user's choice
			if (ui.actSingleContourOnOff->isChecked())
				grpView->setContoursGroupVisible(false);
			else
				grpView->setContoursGroupVisible(true);

		}

		originalContours.push_back(currentContour);
		emit updateMsgLog(QString("> Resetting image contours to initial suggestion..."), Qt::black);
		QMainWindow::statusBar()->showMessage(QString("Resetting image contours to initial suggestion..."));
	}
}

void SegEditorMain::setExperimentsFilesFolder (QString path)
{
	// check if file exists
	if (QDir(path).exists())
		this->pathToExperiments = path;
	// cannot find the folder -> prompt for correction
	else {
		QString s = QFileDialog::getExistingDirectory(this, tr("Choose the root folder of experiments with the original image files"), "./", QFileDialog::ShowDirsOnly);
		this->pathToExperiments = s;
	}
}

//SLOT
void SegEditorMain::penColor()
{
	// update all views
	QList<ImgGraphicsView*> views = this->wavelengthViews.values();

	if (views.size()==0)
		return;

	QColor newColor = QColorDialog::getColor( views.at(0)->penColor());
	if (newColor.isValid()) {

		for (int i=0; i < views.size(); i++) {
			views.at(i)->setPenColor(newColor);
		}
		
		emit updateMsgLog(QString("Pen color changed..."), newColor);
		QMainWindow::statusBar()->showMessage(QString("Pen color changed..."));
	}
}

//SLOT
void SegEditorMain::penWidth()
{
	// update all views
	QList<ImgGraphicsView*> views = this->wavelengthViews.values();
	
	if (views.size()==0)
		return;

	bool ok;
	int newWidth = QInputDialog::getInteger(this, tr("SegCorrect"),
		tr("Select pen width:"),
		views.at(0)->penWidth(),
		1, 50, 1, &ok);

	if (ok) {
		for (int i=0; i < views.size(); i++) {
			views.at(i)->setPenWidth(newWidth);
		}
		emit updateMsgLog(QString("Pen width set to: %1.").arg(newWidth), Qt::black);
		QMainWindow::statusBar()->showMessage(QString("Pen width set to: %1.").arg(newWidth));
	}
}

//SLOT
void SegEditorMain::eraserDiameter()
{
	// update all views
	QList<ImgGraphicsView*> views = this->wavelengthViews.values();

	if (views.size()==0)
		return;

	bool ok;
	int newDiameter = QInputDialog::getInteger(this, tr("SegCorrect"),
		tr("Select eraser diameter:"),
		views.at(0)->eraserDiameter(),
		1, 100, 1, &ok);

	if (ok) {
		for (int i=0; i < views.size(); i++) {
			views.at(i)->setEraserDiameter(newDiameter);
		}
		
		emit updateMsgLog(QString("Eraser diameter set to: %1.").arg(newDiameter), Qt::black);
		QMainWindow::statusBar()->showMessage(QString("Eraser diameter set to: %1.").arg(newDiameter));
	}

}


/**
 * Inits and shows the segmentation editor using the current selection of trees/ cells
 */
void SegEditorMain::openSegmentationEditorDisplayOptions()
{
	// show segmentation editor settings
	CorrectionUISettings* corrForm = CorrectionUISettings::getInst();
	corrForm->show();
	//emit showCorrectionUISettings();

}



void SegEditorMain::updateDisplay()
{
	
	this->setupInterface();

	if (!this->m_tvwDataTreeOverview->selectedIndexes().isEmpty()) {
		updateImageViewerQTFy(this->m_tvwDataTreeOverview->selectedIndexes().at(0));
	}
	else {
		this->m_tvwDataTreeOverview->setCurrentIndex(this->m_TreeDataModel->index(0,0));
		updateImageViewerQTFy(this->m_tvwDataTreeOverview->selectedIndexes().at(0));
	}
}

// SLOT
void SegEditorMain::displayHistogramSettingsClicked()
{
	// find the selected view
	QString viewToChange = ui.mdiWorkingArea->activeSubWindow()->windowTitle();

	ImgGraphicsView* currentView;

	if (viewToChange == "Segmentation Image") { 
		currentView = this->wavelengthViews.value("seg");
	}
	else {
		QStringList splits = viewToChange.split( " " );
		currentView = this->wavelengthViews.value(splits.at(1));
	}

	if (!currentView)
		return;

	ui.lblDisplaySettingsFor->setText(viewToChange);
	ui.dockAdditionalOptions->setVisible(true);
	
	// get the current black and white points
	int whitePoint = currentView->getViewWhitePoint();
	int blackPoint = currentView->getViewBlackPoint();
	ui.grvSettings->updateDisplay(blackPoint, whitePoint);

}


// SLOT
void SegEditorMain::updateDisplaySettingsForm()
{
	// every time a new subwindow is activated
	// the histogram display settings need to be set 
	// to the specific settings of the active window
	myMdiSubWindow* currentWindow = (myMdiSubWindow*)sender();
	QString viewToChange = currentWindow->windowTitle();

	ImgGraphicsView* currentView;

	if (viewToChange == "Segmentation Image") { 
		currentView = this->wavelengthViews.value("seg");
	}
	else {
		QStringList splits = viewToChange.split( " " );
		currentView = this->wavelengthViews.value(splits.at(1));
	}

	if (!currentView)
		return;

	// get the current black and white points
	int whitePoint = currentView->getViewWhitePoint();
	int blackPoint = currentView->getViewBlackPoint();
	ui.grvSettings->updateDisplay(blackPoint, whitePoint);

	ui.lblDisplaySettingsFor->setText(viewToChange);

}

// SLOT
void SegEditorMain::histogramSettingsChanged()
{
	
	QMdiSubWindow* activeWindow = ui.mdiWorkingArea->activeSubWindow();

	if (!activeWindow)
		return;

	// find the selected view
	QString viewToChange = activeWindow->windowTitle();

	ImgGraphicsView* currentView;

	if (viewToChange == "Segmentation Image") { 
		currentView = this->wavelengthViews.value("seg");
	}
	else {
		QStringList splits = viewToChange.split( " " );
		currentView = this->wavelengthViews.value(splits.at(1));
	}

	if (!currentView)
		return;

	currentView->displayHistogramSettingsChanged(ui.grvSettings);

}

// SLOT
void SegEditorMain::resetDisplaySettings()
{
	// Set default settings
	ui.grvSettings->resetDisplay();
}

// SLOT
void SegEditorMain::displayShortcutMemo()
{
	ShortcutHelp* help = new ShortcutHelp(this);
	help->show();
}


/**
*	Updates the current cell in focus based on the marker in focus
*/
// SLOT
void SegEditorMain::newCellInFocus(SegMarker _mrk)
{
	this->mrkCurrentCell = _mrk;
	// get the X,Y coordinates from the track data
	int Xcoord = this->mrkCurrentCell.getX();
	int Ycoord = this->mrkCurrentCell.getY();

	QPoint markPoint(Xcoord, Ycoord);
	//emit updateMsgLog(QString("> Cell in focus: %1.").arg(_mrk.getMarkerName()), Qt::black);
	QMainWindow::statusBar()->showMessage(QString("Cell in focus: %1.").arg(_mrk.getMarkerName()));
}


//SLOT
void SegEditorMain::navigationAction() 
{

	if ( sender() == ui.actGoPreviousTimepoint ) /*if(event->key() == Qt::Key_Up)  */ {
		this->m_tvwDataTreeOverview->outIndexChangeUp();
	}

	if ( sender() == ui.actGoNextTimepoint ) /*if(event->key() == Qt::Key_Down)  */  {
		this->m_tvwDataTreeOverview->outIndexChangeDown();
	}


	if ( sender() == ui.actionCenter ) /*if (event->key() == Qt::Key_Left)*/
		emit newMarkerInFocus(true);

}

// SLOT
void SegEditorMain::setEraserState(bool _state)
{
	if (_state){
		if (ui.actionWand->isChecked())
			ui.actionWand->setChecked(false);
		if (ui.actionPenMode->isChecked())	
			ui.actionPenMode->setChecked(false);
		emit updateMsgLog("> Eraser mode on: Drag eraser over a contour to delete it...", Qt::black);
		QMainWindow::statusBar()->showMessage("Eraser mode on: Drag eraser over a contour to delete it...");
	}
	else {
		if (!ui.actionWand->isChecked())
			ui.actionPenMode->setChecked(true);
	}
	
}

// SLOT
void SegEditorMain::setAutoEraserState(bool _state)
{
	if (_state){
		emit updateMsgLog("> Auto-Eraser On: Suggested contours will be erased and replaced with new contours automatically...", Qt::black);
		QMainWindow::statusBar()->showMessage("Auto-Eraser On: Suggested contours will be erased and replaced with new contours automatically... The suggested contour is highlighted with red color.");
	}
	else {
		emit updateMsgLog("> Auto-Eraser Off: Erase the old contour manually and create a new one with the pencil or the magic wand...", Qt::black);
		QMainWindow::statusBar()->showMessage("Auto-Eraser Off: Erase the old contour manually and create a new one with the pencil or the magic wand...");
	}

}

// SLOT
void SegEditorMain::setWandState(bool _state)
{

	if (_state){
		emit updateMsgLog("> Wand mode on: Click on the cell for local segmentation...", Qt::black);
		QMainWindow::statusBar()->showMessage("Wand mode on: Click on the cell for local segmentation...");
		if (ui.actionPenMode->isChecked())
			ui.actionPenMode->setChecked(false);
		if (ui.actionEraserMode->isChecked())
			ui.actionEraserMode->setChecked(false);
	}
	else {
		if (!ui.actionEraserMode->isChecked())
			ui.actionPenMode->setChecked(true);
	}
}

// SLOT
void SegEditorMain::setPenState(bool _state)
{

	if (_state){
		emit updateMsgLog("> Pencil mode on: Drag and draw a new contour...", Qt::black);
		QMainWindow::statusBar()->showMessage("Pencil mode on: Drag and draw a new contour...");
		if (ui.actionWand->isChecked())
			ui.actionWand->setChecked(false);
		
		if (ui.actionEraserMode->isChecked())
			ui.actionEraserMode->setChecked(false);
	}
	else {
		if (!ui.actionEraserMode->isChecked())
			ui.actionWand->setChecked(true);
	}
}

// SLOT
void SegEditorMain::invertWandState()
{

	if (ui.actionWand->isChecked()) 
		ui.actionWand->setChecked(false);
	else
		ui.actionWand->setChecked(true);

}

void SegEditorMain::resetWandOptions()
{
	ui.spbLowerDiff->setValue(20);
	ui.spbUpperDiff->setValue(50);
}


void SegEditorMain::updateTimepointStatusFrame(QString _treeName, QString _cellNumber, QString _timepoint)
{

	ITrack* cell = this->cellsPerTree.value(_treeName).value(_cellNumber);
	CellLineageTree* cltData = cell->getITree()->getCellLineageTree();

	QString positionIndex = cell->getITree()->getPositionIndex();
	QString positionToken = positionIndex.split("_").at(1);
	int positionNumber = positionToken.remove("p").toInt();

	int quantIndex = PositionQuantification::getQuantificationChunkID(cltData, detectionWL);
	
	if (quantIndex < 0)
		return;

	QVector<QString> chunckColumns = cltData->getQuantificationColumnNames(quantIndex);

	int timePoint = _timepoint.toInt();
	// if it's not the first time we load
	QList<int> availableTimepoints = this->buttonPerTimepoint.keys();

	if (!this->buttonPerTimepoint.isEmpty() && availableTimepoints.contains(timePoint)) {

		// find the button for the selected timepoint
		MyTimepointButton* theButton = this->buttonPerTimepoint.value(timePoint);

		QString buttonsTree, buttonsCell;
		buttonsTree = theButton->getTreeName();
		buttonsCell = theButton->getCellID();

		// check if we are still at the same tree and cell
		// otherwise we need to chuck all timepoint buttons and load new ones
		if (buttonsCell==_cellNumber && buttonsTree==_treeName) {

			// if we are still at the same tree and cell
			// then we just need to update the button that belongs to relevant timepoint
			// create the relevant frame index for this timepoint
			int zIndex = 1; // default z index
			QVector<int> frameIndex;
			//frame index matrix
			//TimePoint
			frameIndex << timePoint;
			//FieldOfView(pos)
			frameIndex << positionNumber;
			//ZIndex
			frameIndex << zIndex;
			//ImagingChannel - this is the wavelength of the segmentation image
			frameIndex << detectionWL;

			QVector<QVariant> data = cltData->getQuantificationDataPoint(quantIndex, cell->getTrackNumber(), frameIndex);

			int isInspected = -1;
			int isActive = -1;
			bool isAvailable = true;

			if (!data.empty()) {
				isInspected = data.at(chunckColumns.indexOf("inspected")).toInt();
				isActive = data.at(chunckColumns.indexOf("active")).toInt();
			}
			else
				isAvailable = false;

			// update the button
			if (isAvailable) {

				theButton->setTimepointAvailable(true);

				if (isInspected && isActive) {
					theButton->setIcon(timeInspectedActive);
					theButton->setTimepointActive(true);
					theButton->setTimepointInspected(true);
				}
				if (isInspected && !isActive) {
					theButton->setIcon(timeInspectedInactive);
					theButton->setTimepointActive(false);
					theButton->setTimepointInspected(true);
				}
				if (!isInspected && isActive) {
					theButton->setIcon(timeNotInspectedActive);
					theButton->setTimepointActive(true);
					theButton->setTimepointInspected(false);
				}
				if (!isInspected && !isActive) {
					theButton->setIcon(timeNotInspectedInactive);
					theButton->setTimepointActive(false);
					theButton->setTimepointInspected(false);
				}
			}
			else {
				theButton->setIcon(timeNotPresent);
				theButton->setTimepointAvailable(false);
			}


			if (_timepoint.toInt() == timePoint){
				theButton->setDown(true);
				this->currentSelectedButton = theButton;
				this->populateMenuTimepoints();
			}	

			return;
		}

	}


	// redo everything new cell and/or tree
	// empty layout and delete first
	removeLayout(ui.frmTimepointStatus->layout());
	delete ui.frmTimepointStatus->layout();
	this->buttonPerTimepoint.clear();

	// create the frame layout
	QHBoxLayout* frameLayout = new QHBoxLayout();

	// check and add the active and inspected columns
	int indexColActive = chunckColumns.indexOf("active");
	// if active column has not been created
	if (indexColActive < 0){
		if(!cltData->addQuantificationColumn(quantIndex, "active", CellLineageTree::Double, false))
			qDebug()<<"Error with adding quantification column active, updateCell method in PositionQuantification class, quantIndex = -1 case.";
	}

	int indexColInspected = chunckColumns.indexOf("inspected");
	// if inspected column has not been created
	if (indexColInspected < 0){
		if(!cltData->addQuantificationColumn(quantIndex, "inspected", CellLineageTree::Double, false))
			qDebug()<<"Error with adding quantification column inspected, updateCell method in PositionQuantification class, quantIndex = -1 case.";
	}

	QMap<int, ITrackPoint*> trackPTs = cell->getTrackPointsRange(-1,-1);

	foreach(ITrackPoint* tp, trackPTs)
	{
		QVBoxLayout* colLayout = new QVBoxLayout();

		int timePoint = tp->getTimePoint();

		// create the relevant frame index for this timepoint
		int zIndex = 1; // default z index
		QVector<int> frameIndex;
		//frame index matrix
		//TimePoint
		frameIndex << timePoint;
		//FieldOfView(pos)
		frameIndex << positionNumber;
		//ZIndex
		frameIndex << zIndex;
		//ImagingChannel - this is the wavelength of the segmentation image
		frameIndex << detectionWL;

		QVector<QVariant> data = cltData->getQuantificationDataPoint(quantIndex, cell->getTrackNumber(), frameIndex);

		int isInspected = -1;
		int isActive = -1;
		bool isAvailable = true;

		if (!data.empty()) {
			isInspected = data.at(chunckColumns.indexOf("inspected")).toInt();
			isActive = data.at(chunckColumns.indexOf("active")).toInt();
		}
		else
			isAvailable = false;

		// create a button
		MyTimepointButton* newButton = new MyTimepointButton(this);
		newButton->setMaximumSize(30, 30);
		newButton->setFlat(true);
		newButton->setToolTip(QString("Timepoint %1").arg(timePoint));
		newButton->setTimepointID(timePoint);
		newButton->setCellID(_cellNumber);
		newButton->setTreeName(_treeName);

		connect(newButton, SIGNAL(clicked()), this, SLOT(updateSelectedIndexTimepointFrame()));

		if (isAvailable) {

			newButton->setTimepointAvailable(true);

			if (isInspected && isActive) {
				newButton->setIcon(timeInspectedActive);
				newButton->setTimepointActive(true);
				newButton->setTimepointInspected(true);
			}
			if (isInspected && !isActive) {
				newButton->setIcon(timeInspectedInactive);
				newButton->setTimepointActive(false);
				newButton->setTimepointInspected(true);
			}
			if (!isInspected && isActive) {
				newButton->setIcon(timeNotInspectedActive);
				newButton->setTimepointActive(true);
				newButton->setTimepointInspected(false);
			}
			if (!isInspected && !isActive) {
				newButton->setIcon(timeNotInspectedInactive);
				newButton->setTimepointActive(false);
				newButton->setTimepointInspected(false);
			}
		}
		else {
			newButton->setIcon(timeNotPresent);
			newButton->setTimepointAvailable(false);
		}

		newButton->setContextMenuPolicy(Qt::CustomContextMenu);
		connect(newButton, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(timepointContextMenu(const QPoint&)));

		this->buttonPerTimepoint.insert(timePoint, newButton);

		colLayout->addWidget(newButton);

		if (_timepoint.toInt() == timePoint){
			newButton->setDown(true);
			this->currentSelectedButton = newButton;
			this->populateMenuTimepoints();
		}	

		colLayout->addStretch();		
		frameLayout->addLayout(colLayout);
	}

	frameLayout->addStretch();
	ui.frmTimepointStatus->setLayout(frameLayout);

	
}


void SegEditorMain::updateTimepointStatus(QString _timepoint)
{
	QList<int> timepoints = this->buttonPerTimepoint.keys();
	foreach(int timePoint, timepoints) {
		QPushButton* aButton = this->buttonPerTimepoint.value(timePoint);
		if (timePoint==_timepoint.toInt()) {
			aButton->setDown(true);
			this->currentSelectedButton = (MyTimepointButton*) aButton;
			this->populateMenuTimepoints();
		}
		else
			aButton->setDown(false);
	}
	
}

// SLOT
void SegEditorMain::updateSelectedIndexTimepointFrame()
{
	MyTimepointButton* theSender = (MyTimepointButton*) sender();
	
	theSender->setDown(true);

	int timepointID = theSender->getTimepointID();
	QModelIndexList timepoints;

	QStringList list = this->windowTitle().split(": ");
	QString chunk = list[1];

	QString treeChunk = this->m_TreeDataModel->getChunkName();

	int timepointIndexInCell = this->buttonPerTimepoint.values().indexOf(theSender);
	int selectedTimepoint = this->buttonPerTimepoint.keys().at(timepointIndexInCell);

	QModelIndex selectedIndex;
	QModelIndex newIndex;
	// find the index that is currently selected at the tree view
	// use it as a hint for the search of appropriate index (see method findIndex)
	// if no index is selected use the first index
	if (this->m_tvwDataTreeOverview->selectedIndexes().size() == 0)
		selectedIndex = this->m_TreeDataModel->index(0,0);
	else
		selectedIndex  = this->m_tvwDataTreeOverview->selectedIndexes().at(0);

	TreeItem *item = static_cast<TreeItem*>(selectedIndex.internalPointer());

	QModelIndexList cells;

	QModelIndex start;
	// find if the user has previously selected an experiment, tree, cell or timepoint
	// and accordingly define the starting point for the search
	switch (item->column()) {
	case 0:  // experiment
		// it's the first tree
		start = selectedIndex.child(0,0).child(0,0);
		break;
	case 1: // tree
		start = selectedIndex.child(0,0);
		break;
	case 2: // cell
		start = selectedIndex.parent();
		break;
	case 3: // timepoint
		start = selectedIndex.parent().parent();
		break;
	default:
		break;
	}

	QString cell = "Cell_" + QString::number(this->currentCellNumber);
	cells = this->m_TreeDataModel->match(start, Qt::DisplayRole,  QVariant::fromValue(cell), 1, Qt::MatchRecursive);
	if (cells.size() > 0) {
		start = cells.at(0);
/*		newIndex = cells.at(0).child(timepointIndexInCell, 0);
		updateSelectedIndex(newIndex);
		emit treeviewIndexChanged(newIndex);*/	
	}

	QString timepoint = "Timepoint_" + QString::number(timepointID);
	timepoints = this->m_TreeDataModel->match(start, Qt::DisplayRole,  QVariant::fromValue(timepoint), 1, Qt::MatchRecursive);
	if(timepoints.size()>0){
		newIndex = timepoints.at(0);
		if(this->staggerExperiment){
			QStringList list = this->windowTitle().split(": ");
			QString chunk ;
			if(list.size()>1)
				chunk = list[1];
			updateSelectedIndexStagger(newIndex, chunk);
			emit treeviewIndexChangedStagger(newIndex, chunk);
		}
		else{
			updateSelectedIndex(newIndex);
			emit treeviewIndexChanged(newIndex);
		}
	}
}


 //SLOT
void SegEditorMain::updateTimepointQuantification(QString _posIndex, QString _treeName, QString _cellNumber, QString _timepoint, bool _isSingleCell)
{
	
	bool frmExistingQuantification = false;
	QApplication::setOverrideCursor( QCursor(Qt::WaitCursor) );

	ITrack* cell = this->cellsPerTree.value(_treeName).value(_cellNumber);
	
	
	QMap<int, QVector<ITrack*>> cellInTimepoint;
	QList<QSharedPointer<ITree>> treeOfCurrentCell;

	QVector<ITrack*> cellList;
	if (_isSingleCell) {
		cellList.append(cell);
	} 
	else {
		cellList = this->cellsPerTimepoint.value(_treeName).value(_timepoint.toInt());
	}

	cellInTimepoint.insert(_timepoint.toInt(), cellList);

	QSharedPointer<ITree> treePointer(cell->getITree());
	treeOfCurrentCell.append(treePointer);

	PositionQuantification* worker = new PositionQuantification(_posIndex, this->pathToExperiments);

	worker->initializeData( cellInTimepoint, treeOfCurrentCell);

	worker->setQuantificationOptions(PositionQuantification::options);

	worker->setInspectedMode(true);

	worker->setInspectedDetectionWL(this->inspectedDetectionWL);
	worker->setInspectedSegmentationMethodID(this->inspectedSegmentationMethodID);

	// if the user has opened an existing quantification, no combinations are available
	// we need to find all the wavelengths to quantify from the quantification chunk attribute names
	if (PositionQuantification::wlCombos.isEmpty()) {
		QMap<int, QList<int>> wlCombos = PositionQuantification::getQuantificationCombinationsFromChunk(cell, detectionWL);
		worker->setCombinations(wlCombos); 
		frmExistingQuantification = true;
	}
	
	// pass all the loaded images already to the quantification module
	// to avoid loading them again
	QMap<QString, Mat> loadedImages;
	foreach (QString viewID, this->wavelengthViews.keys()) {
		ImgGraphicsView* view = this->wavelengthViews.value(viewID);
		
		Mat image = view->getMatImage();

		// image not loaded properly or not available
		if (image.rows == 0 || image.cols == 0)
			continue;

		if (view->isImageFound())
			loadedImages.insert(viewID, image);
	}

	worker ->setPreloadedImages (loadedImages);

	worker->run();

	if (_isSingleCell) {
		emit cellUpdated(cell);
		QString msg = QString("> Quantification updated for cell %1 at timepoint %2...").arg(_cellNumber).arg(_timepoint);
		emit updateMsgLog(msg, Qt::darkGreen);
		QMainWindow::statusBar()->showMessage(msg);
	} 
	else {

		foreach(ITrack* c, cellList) 
			emit cellUpdated(c);

		QString msg = QString("> Quantification updated for all cells at timepoint %1...").arg(_timepoint);
		emit updateMsgLog(msg, Qt::darkGreen);
		QMainWindow::statusBar()->showMessage(msg);
	}
	
	// if the user has opened an existing quantification, we need to empty the position quantification wavelength combo
	// to be sure for different seg windows with different detection wlgs it will be calculated correctly
	if(frmExistingQuantification)
		worker->setCombinations(QMap<int,QList<int>>());

	QApplication::restoreOverrideCursor();
}


void SegEditorMain::removeLayout(QLayout* layout, bool deleteWidgets)
{

	if ( layout == NULL )
		return;

	while (QLayoutItem* item = layout->takeAt(0))
	{
		if (deleteWidgets)
		{
			if (QWidget* widget = item->widget())
				delete widget;
		}
		if (QLayout* childLayout = item->layout())
			removeLayout(childLayout, deleteWidgets);
		delete item;
	}

}

// SLOT
void SegEditorMain::populateMenuTimepoints()
{
	ui.menuTimepoints->clear();

	bool isAvailable = this->currentSelectedButton->isTimepointAvailable();
	bool isActive = this->currentSelectedButton->isTimepointActive();
	bool isInspected = this->currentSelectedButton->isTimepointInspected();

	if (isAvailable) {

		if (isInspected && isActive) {
			QAction *actSetInspectedInactive = new QAction(QIcon(timeInspectedInactive), "Set Inactive (Ctrl+Shift+A)", this);
			ui.menuTimepoints->addAction(actSetInspectedInactive);
			connect(actSetInspectedInactive, SIGNAL(triggered()),this, SLOT(setTimepointInactive()) );
			
			QAction *actSetNotInspectedActive = new QAction(QIcon(timeNotInspectedActive), "Set Not Inspected (Ctrl+Shift+Z)", this);
			ui.menuTimepoints->addAction(actSetNotInspectedActive);
			connect(actSetNotInspectedActive, SIGNAL(triggered()),this, SLOT(setTimepointNotInspected()) );
			
		}
		if (isInspected && !isActive) {
			QAction *actSetInspectedActive = new QAction(QIcon(timeInspectedActive), "Set Active (Ctrl+A)", this);
			ui.menuTimepoints->addAction(actSetInspectedActive);
			connect(actSetInspectedActive, SIGNAL(triggered()),this, SLOT(setTimepointActive()) );
			

			QAction *actSetNotInspectedInactive = new QAction(QIcon(timeNotInspectedInactive), "Set Not Inspected (Ctrl+Shift+Z)", this);
			ui.menuTimepoints->addAction(actSetNotInspectedInactive);
			connect(actSetNotInspectedInactive, SIGNAL(triggered()),this, SLOT(setTimepointNotInspected()) );
			
		}
		if (!isInspected && isActive) {
			QAction *actSetNotInspectedInactive = new QAction(QIcon(timeNotInspectedInactive), "Set Inactive (Ctrl+Shift+A)", this);
			ui.menuTimepoints->addAction(actSetNotInspectedInactive);
			connect(actSetNotInspectedInactive, SIGNAL(triggered()),this, SLOT(setTimepointInactive()) );
			

			QAction *actSetInspectedActive = new QAction(QIcon(timeInspectedActive), "Set Inspected (Ctrl+Z)", this);
			ui.menuTimepoints->addAction(actSetInspectedActive);
			connect(actSetInspectedActive, SIGNAL(triggered()),this, SLOT(setTimepointInspected()) );
		}
		if (!isInspected && !isActive) {
			QAction *actSetNotInspectedActive = new QAction(QIcon(timeNotInspectedActive), "Set Active (Ctrl+A)", this);
			ui.menuTimepoints->addAction(actSetNotInspectedActive);
			connect(actSetNotInspectedActive, SIGNAL(triggered()),this, SLOT(setTimepointActive()) );
			

			QAction *actSetInspectedInactive = new QAction(QIcon(timeInspectedInactive), "Set Inspected (Ctrl+Z)", this);
			ui.menuTimepoints->addAction(actSetInspectedInactive);
			connect(actSetInspectedInactive, SIGNAL(triggered()),this, SLOT(setTimepointInspected()) );
			
		}
	}
	else {
		QAction *actQuantifyNotAvailable = new QAction(QIcon(":/qtfyres/qtfyres/sqtfy.png"), "Quantify timepoint (Ctrl+S)", this);
		ui.menuTimepoints->addAction(actQuantifyNotAvailable);
		connect(actQuantifyNotAvailable, SIGNAL(triggered()),this, SLOT(quantifyTimepoint()) );
		
	}

	ui.menuTimepoints->addSeparator();

	QAction *actSetAllActive = new QAction(QIcon(":/qtfyres/qtfyres/outliers_active.png"), "Set All Active", this);
	ui.menuTimepoints->addAction(actSetAllActive);
	connect(actSetAllActive, SIGNAL(triggered()),this, SLOT(setAllTimepointsActive()) );
	

	QAction *actSetAllInactive = new QAction(QIcon(":/qtfyres/qtfyres/outliers_allinactive.png"), "Set All Inactive", this);
	ui.menuTimepoints->addAction(actSetAllInactive);
	connect(actSetAllInactive, SIGNAL(triggered()),this, SLOT(setAllTimepointsInactive()) );


	QAction *actSetAllInspected = new QAction(QIcon(":/qtfyres/qtfyres/checkbox-2.png"), "Set All Inspected", this);
	ui.menuTimepoints->addAction(actSetAllInspected);
	connect(actSetAllInspected, SIGNAL(triggered()),this, SLOT(setAllTimepointsInspected()) );
	

	QAction *actSetAllNotInspected = new QAction(QIcon(":/qtfyres/qtfyres/checkbox-0.png"), "Set All Not Inspected", this);
	ui.menuTimepoints->addAction(actSetAllNotInspected);
	connect(actSetAllNotInspected, SIGNAL(triggered()),this, SLOT(setAllTimepointsNotInspected()) );
	

}

// SLOT
void SegEditorMain::timepointContextMenu(const QPoint& pnt)
{
	// for most widgets
	ui.menuTimepoints->clear();

	MyTimepointButton* timepointButton = (MyTimepointButton*) sender();
	QPoint globalPos = timepointButton->mapToGlobal(pnt);

	QMenu myMenu;

	bool isAvailable = timepointButton->isTimepointAvailable();
	bool isActive = timepointButton->isTimepointActive();
	bool isInspected = timepointButton->isTimepointInspected();

	if (isAvailable) {

		if (isInspected && isActive) {
			QAction *actSetInspectedInactive = new QAction(QIcon(timeInspectedInactive), "Set Inactive (Ctrl+Shift+A)", this);
			myMenu.addAction(actSetInspectedInactive);
			connect(actSetInspectedInactive, SIGNAL(triggered()),this, SLOT(setTimepointInactive()) );
			ui.menuTimepoints->addAction(actSetInspectedInactive);

			QAction *actSetNotInspectedActive = new QAction(QIcon(timeNotInspectedActive), "Set Not Inspected (Ctrl+Shift+Z)", this);
			myMenu.addAction(actSetNotInspectedActive);
			connect(actSetNotInspectedActive, SIGNAL(triggered()),this, SLOT(setTimepointNotInspected()) );
			ui.menuTimepoints->addAction(actSetNotInspectedActive);

			/*QAction *actSetNotInspectedInactive = new QAction(QIcon(timeNotInspectedInactive), "Set Not Inspected/ Inactive", this);
			myMenu.addAction(actSetNotInspectedInactive);
			connect(actSetNotInspectedInactive, SIGNAL(triggered()),this, SLOT(openSegmentationEditor()) );*/
		}
		if (isInspected && !isActive) {
			QAction *actSetInspectedActive = new QAction(QIcon(timeInspectedActive), "Set Active (Ctrl+A)", this);
			myMenu.addAction(actSetInspectedActive);
			connect(actSetInspectedActive, SIGNAL(triggered()),this, SLOT(setTimepointActive()) );
			ui.menuTimepoints->addAction(actSetInspectedActive);

			QAction *actSetNotInspectedInactive = new QAction(QIcon(timeNotInspectedInactive), "Set Not Inspected (Ctrl+Shift+Z)", this);
			myMenu.addAction(actSetNotInspectedInactive);
			connect(actSetNotInspectedInactive, SIGNAL(triggered()),this, SLOT(setTimepointNotInspected()) );
			ui.menuTimepoints->addAction(actSetNotInspectedInactive);

			/*QAction *actSetNotInspectedActive = new QAction(QIcon(timeNotInspectedActive), "Set Not Inspected/ Active", this);
			myMenu.addAction(actSetNotInspectedActive);
			connect(actSetNotInspectedActive, SIGNAL(triggered()),this, SLOT(openSegmentationEditor()) );*/
			
		}
		if (!isInspected && isActive) {
			QAction *actSetNotInspectedInactive = new QAction(QIcon(timeNotInspectedInactive), "Set Inactive (Ctrl+Shift+A)", this);
			myMenu.addAction(actSetNotInspectedInactive);
			connect(actSetNotInspectedInactive, SIGNAL(triggered()),this, SLOT(setTimepointInactive()) );
			ui.menuTimepoints->addAction(actSetNotInspectedInactive);

			QAction *actSetInspectedActive = new QAction(QIcon(timeInspectedActive), "Set Inspected (Ctrl+Z)", this);
			myMenu.addAction(actSetInspectedActive);
			connect(actSetInspectedActive, SIGNAL(triggered()),this, SLOT(setTimepointInspected()) );
			ui.menuTimepoints->addAction(actSetInspectedActive);
			/*QAction *actSetInspectedInactive = new QAction(QIcon(timeInspectedInactive), "Set Inspected/ Inactive", this);
			myMenu.addAction(actSetInspectedInactive);
			connect(actSetInspectedInactive, SIGNAL(triggered()),this, SLOT(openSegmentationEditor()) );*/
		}
		if (!isInspected && !isActive) {
			QAction *actSetNotInspectedActive = new QAction(QIcon(timeNotInspectedActive), "Set Active (Ctrl+A)", this);
			myMenu.addAction(actSetNotInspectedActive);
			connect(actSetNotInspectedActive, SIGNAL(triggered()),this, SLOT(setTimepointActive()) );
			ui.menuTimepoints->addAction(actSetNotInspectedActive);

			QAction *actSetInspectedInactive = new QAction(QIcon(timeInspectedInactive), "Set Inspected (Ctrl+Z)", this);
			myMenu.addAction(actSetInspectedInactive);
			connect(actSetInspectedInactive, SIGNAL(triggered()),this, SLOT(setTimepointInspected()) );
			ui.menuTimepoints->addAction(actSetInspectedInactive);

			/*QAction *actSetInspectedActive = new QAction(QIcon(timeInspectedActive), "Set Inspected/ Active", this);
			myMenu.addAction(actSetInspectedActive);
			connect(actSetInspectedActive, SIGNAL(triggered()),this, SLOT(openSegmentationEditor()) );*/
		}
	}
	else {
		QAction *actQuantifyNotAvailable = new QAction(QIcon(":/qtfyres/qtfyres/sqtfy.png"), "Quantify timepoint (Ctrl+S)", this);
		myMenu.addAction(actQuantifyNotAvailable);
		connect(actQuantifyNotAvailable, SIGNAL(triggered()),this, SLOT(quantifyTimepoint()) );
		ui.menuTimepoints->addAction(actQuantifyNotAvailable);
	}

	myMenu.addSeparator();
	ui.menuTimepoints->addSeparator();

	QAction *actSetAllActive = new QAction(QIcon(":/qtfyres/qtfyres/outliers_active.png"), "Set All Active", this);
	myMenu.addAction(actSetAllActive);
	connect(actSetAllActive, SIGNAL(triggered()),this, SLOT(setAllTimepointsActive()) );
	ui.menuTimepoints->addAction(actSetAllActive);

	QAction *actSetAllInactive = new QAction(QIcon(":/qtfyres/qtfyres/outliers_allinactive.png"), "Set All Inactive", this);
	myMenu.addAction(actSetAllInactive);
	connect(actSetAllInactive, SIGNAL(triggered()),this, SLOT(setAllTimepointsInactive()) );
	ui.menuTimepoints->addAction(actSetAllInactive);

	QAction *actSetAllInspected = new QAction(QIcon(":/qtfyres/qtfyres/checkbox-2.png"), "Set All Inspected", this);
	myMenu.addAction(actSetAllInspected);
	connect(actSetAllInspected, SIGNAL(triggered()),this, SLOT(setAllTimepointsInspected()) );
	ui.menuTimepoints->addAction(actSetAllInspected);

	QAction *actSetAllNotInspected = new QAction(QIcon(":/qtfyres/qtfyres/checkbox-0.png"), "Set All Not Inspected", this);
	myMenu.addAction(actSetAllNotInspected);
	connect(actSetAllNotInspected, SIGNAL(triggered()),this, SLOT(setAllTimepointsNotInspected()) );
	ui.menuTimepoints->addAction(actSetAllNotInspected);
	
	myMenu.exec(globalPos);
}


// SLOT
void SegEditorMain::quantifyTimepoint ()
{
	QModelIndex index = this->m_tvwDataTreeOverview->selectedIndexes().at(0);

	if (index.isValid()) {

		QString position="";
		QString experimentName = "";
		QString treeName = "";
		QString cellNum="";
		QString timepoint="";

		TreeItem* selectedItem = static_cast<TreeItem*>(index.internalPointer());
		QVector<QString> selectionInfo = getIdentificationInfo(selectedItem);
		experimentName = selectionInfo.at(0);
		position = selectionInfo.at(1);
		treeName = selectionInfo.at(2); 
		cellNum = selectionInfo.at(3);
		timepoint = selectionInfo.at(4);

		updateTimepointQuantification(position, treeName, cellNum, timepoint, true);
		updateTimepointStatusFrame(treeName, cellNum, timepoint);

		// 
		ImgGraphicsView* grpView = this->wavelengthViews.value("seg");

		if (!grpView)
			return;

		// reset the flag for modifications when the timepoint has been already quantified
		// i.e. when the user pressed Ctrl+S
		grpView->setIsModifiedByUser(false);
		
	}
}

// SLOT
void SegEditorMain::setTimepointActive()
{
	QModelIndex index = this->m_tvwDataTreeOverview->selectedIndexes().at(0);
	double newValue = 1.0;
	QString colToChange = "active";
	setTimepointStatus(colToChange, newValue, index);
}

// SLOT
void SegEditorMain::setTimepointInactive()
{
	QModelIndex index = this->m_tvwDataTreeOverview->selectedIndexes().at(0);
	double newValue = 0.0;
	QString colToChange = "active";
	setTimepointStatus(colToChange, newValue, index);
}

// SLOT
void SegEditorMain::setTimepointInspected()
{
	QModelIndex index = this->m_tvwDataTreeOverview->selectedIndexes().at(0);
	double newValue = 1.0;
	QString colToChange = "inspected";
	setTimepointStatus(colToChange, newValue, index);
}

// SLOT
void SegEditorMain::setTimepointNotInspected()
{
	QModelIndex index = this->m_tvwDataTreeOverview->selectedIndexes().at(0);
	double newValue = 0.0;
	QString colToChange = "inspected";
	setTimepointStatus(colToChange, newValue, index);
	
}


void SegEditorMain::setTimepointStatus(QString _colName, double _newStatus, QModelIndex _index)
{
	if (_index.isValid()) {

		QString position="";
		QString experimentName = "";
		QString treeName = "";
		QString cellNum="";
		QString timepoint="";

		TreeItem* selectedItem = static_cast<TreeItem*>(_index.internalPointer());
		QVector<QString> selectionInfo = getIdentificationInfo(selectedItem);
		experimentName = selectionInfo.at(0);
		position = selectionInfo.at(1);
		treeName = selectionInfo.at(2); 
		cellNum = selectionInfo.at(3);
		timepoint = selectionInfo.at(4);

		ITrack* cell = this->cellsPerTree.value(treeName).value(cellNum);
		CellLineageTree* cltData = cell->getITree()->getCellLineageTree();

		QString positionIndex = cell->getITree()->getPositionIndex();
		QString positionToken = positionIndex.split("_").at(1);
		int positionNumber = positionToken.remove("p").toInt();

		int quantIndex = PositionQuantification::getQuantificationChunkID(cltData, detectionWL);
		
		if (quantIndex < 0)
			return;
		
		QVector<QString> chunckColumns = cltData->getQuantificationColumnNames(quantIndex);

		int zIndex = 1; // default z index
		QVector<int> frameIndex;
		//frame index matrix
		//TimePoint
		frameIndex << timepoint.toInt();
		//FieldOfView(pos)
		frameIndex << positionNumber;
		//ZIndex
		frameIndex << zIndex;
		//ImagingChannel - this is the wavelength of the segmentation image
		frameIndex << detectionWL;

		QVector<QVariant> data = cltData->getQuantificationDataPoint(quantIndex, cell->getTrackNumber(), frameIndex);

		double currentValue = -1.0;
		bool isChanged = false;

		if (!data.empty()) {
			int colIndx = chunckColumns.indexOf(_colName);
			currentValue = data.at(colIndx).toDouble();
			if (currentValue != _newStatus) {
				data.replace(colIndx, _newStatus);
				//// add the attribute for the cell
				cltData->setQuantificationDataPoint(quantIndex,  cell->getTrackNumber(), frameIndex, data);
				isChanged = true;
			}
		}

		if (isChanged) {
			updateTimepointStatusFrame(treeName, cellNum, timepoint);
			emit cellUpdated(cell);
		}
	}
}


// SLOT
void SegEditorMain::setAllTimepointsActive()
{
	QModelIndex index = this->m_tvwDataTreeOverview->selectedIndexes().at(0);
	double newValue = 1.0;
	QString colToChange = "active";
	setAllTimepointsStatus(colToChange, newValue, index);
	
}

// SLOT
void SegEditorMain::setAllTimepointsInactive()
{
	QModelIndex index = this->m_tvwDataTreeOverview->selectedIndexes().at(0);
	double newValue = 0.0;
	QString colToChange = "active";
	setAllTimepointsStatus(colToChange, newValue, index);
}

// SLOT
void SegEditorMain::setAllTimepointsInspected()
{
	QModelIndex index = this->m_tvwDataTreeOverview->selectedIndexes().at(0);
	double newValue = 1.0;
	QString colToChange = "inspected";
	setAllTimepointsStatus(colToChange, newValue, index);
}

// SLOT
void SegEditorMain::setAllTimepointsNotInspected()
{
	QModelIndex index = this->m_tvwDataTreeOverview->selectedIndexes().at(0);
	double newValue = 0.0;
	QString colToChange = "inspected";
	setAllTimepointsStatus(colToChange, newValue, index);
}

void SegEditorMain::setAllTimepointsStatus(QString _colName, double _newStatus, QModelIndex _index)
{
	if (_index.isValid()) {

		QString position="";
		QString experimentName = "";
		QString treeName = "";
		QString cellNum="";
		QString timepoint="";

		TreeItem* selectedItem = static_cast<TreeItem*>(_index.internalPointer());
		QVector<QString> selectionInfo = getIdentificationInfo(selectedItem);
		experimentName = selectionInfo.at(0);
		position = selectionInfo.at(1);
		treeName = selectionInfo.at(2); 
		cellNum = selectionInfo.at(3);
		timepoint = selectionInfo.at(4);

		ITrack* cell = this->cellsPerTree.value(treeName).value(cellNum);
		CellLineageTree* cltData = cell->getITree()->getCellLineageTree();

		QString positionIndex = cell->getITree()->getPositionIndex();
		QString positionToken = positionIndex.split("_").at(1);
		int positionNumber = positionToken.remove("p").toInt();

		int quantIndex = PositionQuantification::getQuantificationChunkID(cltData, detectionWL);

		if (quantIndex < 0)
			return;

		QVector<QString> chunckColumns = cltData->getQuantificationColumnNames(quantIndex);

		//get the quantification frame indexes 
		//int quantIndex = this->m_quantIndex;
		QList<CellLineageTree::FrameIndex> frameIndex = cltData->getQuantificationFrameIndexes(quantIndex, cell->getTrackNumber());
		qSort(frameIndex);

		//if error with the frame indexes of this cell continue to the next one
		if(frameIndex.isEmpty()){
			qDebug()<<"Error getting the quantification frame index list in updateDataEntry of CLTAttribute";
			return;		
		}

		//iterate through the frame indexes of the specific cell and quantification chunk
		for(int i = 0; i < frameIndex.size(); i++){
			//keep the timepoint
			CellLineageTree::FrameIndex frameData = frameIndex[i];
			int timepoint = frameData[0];


			//get the value of the attribute
			QVector<QVariant> data = cltData->getQuantificationDataPoint(quantIndex, cell->getTrackNumber(), frameIndex[i]);

			//check for invalid values
			if(!data.isEmpty()) {
				int colIndx = chunckColumns.indexOf(_colName);
				double currentValue = data.at(colIndx).toDouble();
				if (currentValue != _newStatus) {
					data.replace(colIndx, _newStatus);
					//// add the attribute for the cell
					cltData->setQuantificationDataPoint(quantIndex,  cell->getTrackNumber(), frameIndex[i], data);
					updateTimepointStatusFrame(treeName, cellNum, QString::number(timepoint));
					emit cellUpdated(cell);
				}
			}
		}		
	}
}

QVector<QString> SegEditorMain::getIdentificationInfo (TreeItem* _selectedItem)
{
	QVector<QString> out;

	QString position="";
	QString experimentName = "";
	QString treeName = "";
	QString cellNum="";
	QString timepoint="";


	if (_selectedItem->column() == 0) {
		// when the user clicks on an experiment it loads the first timepoint of the first tree
		experimentName = _selectedItem->data(0).toString();
		treeName = _selectedItem->child(0)->data(0).toString();
		position = _selectedItem->child(0)->data(1).toString();
		cellNum = _selectedItem->child(0)->child(0)-> data(1).toString();
		if (_selectedItem->child(0)->child(0)->child(0)){
			timepoint = _selectedItem->child(0)->child(0)->child(0)->data(1).toString();
		}			
	}
	// when the user clicks on a tree it loads the first timepoint of the tree
	else if (_selectedItem->column() == 1) {
		position = _selectedItem->data(1).toString();
		treeName = _selectedItem->data(0).toString();
		experimentName = _selectedItem->parent()->data(0).toString();
		cellNum = _selectedItem->child(0)-> data(1).toString();
		if (_selectedItem->child(0)->child(0)){
			timepoint = _selectedItem->child(0)->child(0)->data(1).toString();
		}			
	}
	// when the user clicks on a cell
	else if (_selectedItem->column() == 2) {
		position = _selectedItem->parent()->data(1).toString();
		treeName = _selectedItem->parent()->data(0).toString();
		cellNum = _selectedItem-> data(1).toString();
		experimentName = _selectedItem->parent()->parent()->data(0).toString();
		if (_selectedItem->child(0)) {
			timepoint = _selectedItem->child(0)->data(1).toString();
		}			
	}
	// when the user clicks on a timepoint
	else  {
		position = _selectedItem->parent()->parent()->data(1).toString();
		treeName = _selectedItem->parent()->parent()->data(0).toString();
		cellNum = _selectedItem->parent()->data(1).toString();
		experimentName = _selectedItem->parent()->parent()->parent()->data(0).toString();
		timepoint = _selectedItem->data(1).toString();
	}

	// check if the cell has jumped to a different position

	ITrack* selectedCell = this->cellsPerTree.value(treeName).value(cellNum);
	ITrackPoint* currentTrackpoint;

	if (selectedCell!=NULL)
		currentTrackpoint = selectedCell->getTrackPointByTimePoint(timepoint.toInt());

	if (currentTrackpoint!=NULL) {
		int newPosNum = currentTrackpoint->getPositionNumber();
		position = TTTManager::getInst().getExperimentBasename() + "_p" +  Tools::convertIntPositionToString(newPosNum);
	}

	
	out << experimentName;
	out << position;
	out << treeName;
	out << cellNum;
	out << timepoint;

	return out;
}

void SegEditorMain::updateHScrollBarInViews(int _newValue, bool _isZooming, QString _viewID)
{
	// iterate through all graphic views
	int numOfViews = this->wavelengthViews.keys().size();
	QList<ImgGraphicsView*> views = this->wavelengthViews.values();

	for (int i=0; i < numOfViews; i++) {
		if (views.at(i)->isViewZooming())
			return;
	}

	// the update scroll bars signal is only processed if it's coming from the active view
	bool processSignal = false;

	// find the active subwindow
	QList<QMdiSubWindow*> myWindows = ui.mdiWorkingArea->subWindowList();

	myMdiSubWindow* activeWindow = (myMdiSubWindow*) ui.mdiWorkingArea->activeSubWindow();

	foreach(QMdiSubWindow* aWindow, myWindows) {

		myMdiSubWindow* mywin = ((myMdiSubWindow*) aWindow);
		QString windowID = mywin->getAttributeName();

		if (windowID == _viewID && mywin == activeWindow)
			processSignal = true;

	}

	if (!processSignal) 
		return;

	
	// do all pairwise connections for the views
	for (int i=0; i < numOfViews; i++) {

		QString currentView = (views.at(i))->getViewID();

		if (currentView == _viewID)
			continue;

		views.at(i)->horizontalScrollBar()->setValue(_newValue);
	}
}

void SegEditorMain::updateVScrollBarInViews(int _newValue, bool _isZooming, QString _viewID)
{
	// iterate through all graphic views
	int numOfViews = this->wavelengthViews.keys().size();
	QList<ImgGraphicsView*> views = this->wavelengthViews.values();

	// if at least one view is in zooming mode the scale function
	// will take care of the updates in the scroll bars
	for (int i=0; i < numOfViews; i++) {
		if (views.at(i)->isViewZooming())
			return;
	}

	// the update scroll bars signal is only processed if it's coming from the active view
	bool processSignal = false;

	// find the active subwindow
	QList<QMdiSubWindow*> myWindows = ui.mdiWorkingArea->subWindowList();

	myMdiSubWindow* activeWindow = (myMdiSubWindow*) ui.mdiWorkingArea->activeSubWindow();

	foreach(QMdiSubWindow* aWindow, myWindows) {

		myMdiSubWindow* mywin = ((myMdiSubWindow*) aWindow);
		QString windowID = mywin->getAttributeName();

		if (windowID == _viewID && mywin == activeWindow)
			processSignal = true;

	}

	if (!processSignal) 
		return;

	// do all pairwise connections for the views
	for (int i=0; i < numOfViews; i++) {

		QString currentView = (views.at(i))->getViewID();

		if (currentView == _viewID)
			continue;

		views.at(i)->verticalScrollBar()->setValue(_newValue);

	}

}


void SegEditorMain::updateViewsZoomFactor(qreal _newZoomFactor)
{
	this->viewsZoomFactor = _newZoomFactor;
}

// SLOT
void SegEditorMain::showHelpWandOptions()
{
	// create help text
	QString txt = "<p><h2>QTFy Help: Magic Wand Options</h2> </p>"  
		"<p>The Magic Wand can be used to semi-automatically identify the cell contour. </p>"
		"<p>The Magic Wands works better if you first adjust the contrast of the image until the cell clearly stands out from the background. "
		"To adjust the contrast of an image make sure that the image window is the active window (or click on the window title) and press the "
		"\"Adjust Histogram\" button <img src=\":view-object-histogram.png\"> or CTRL+H.</p>"
		"<p>You can adjust the magic wand to the image intensity profile by adjusting the two parameters Lower/Upper limit: <br>" 
		"<b>Lower Limit:</b> the maximal lower brightness difference between the pixels in the cell area.<br>"
		"<b>Upper Limit:</b> the maximal upper brightness difference between the pixels in the cell area.<br>"
		"As a result, the performance of the magic wand depends on the range of intensities from the histogram as well as the seed pixel (the pixel that the user has selected with the wand).</p>"

		"<p><img src=\":hint.png\"> Did you know that you can also press the button \"W\" to quickly pick up or drop the magic wand? </p>";

	QMap<QString, QImage> imgResources;
	QImage img(":/qtfyres/qtfyres/hint.png");
	imgResources.insert("hint.png", img);
	QImage img2(":/qtfyres/qtfyres/view-object-histogram.png");
	imgResources.insert("view-object-histogram.png", img2);
	
	// show help form
	QString windowTitle = "QTFy help: Magic Wand Options";

	QTFyHelp* help = new QTFyHelp(this, windowTitle);
	help->setHTMLHelp(txt, imgResources);
	help->show();
}

//SLOT
void SegEditorMain::updateTpStatusFromQTFyEditor(){
	
	QModelIndex index = this->m_tvwDataTreeOverview->selectedIndexes().at(0);

	if (index.isValid()) {
		QString position="";
		QString experimentName = "";
		QString treeName = "";
		QString cellNum="";
		QString timepoint="";

		TreeItem* selectedItem = static_cast<TreeItem*>(index.internalPointer());
		QVector<QString> selectionInfo = getIdentificationInfo(selectedItem);
		experimentName = selectionInfo.at(0);
		position = selectionInfo.at(1);
		treeName = selectionInfo.at(2); 
		cellNum = selectionInfo.at(3);
		timepoint = selectionInfo.at(4);

		if (cellNum.toInt() != currentCellNumber)
			return; // the index is different from the current cell - something went wrong

		ITrack* cell = this->cellsPerTree.value(treeName).value(cellNum);
		CellLineageTree* cltData = cell->getITree()->getCellLineageTree();

		QString positionIndex = cell->getITree()->getPositionIndex();
		QString positionToken = positionIndex.split("_").at(1);
		int positionNumber = positionToken.remove("p").toInt();

		int quantIndex = PositionQuantification::getQuantificationChunkID(cltData, detectionWL);

		if (quantIndex < 0)
			return;

		QVector<QString> chunckColumns = cltData->getQuantificationColumnNames(quantIndex);

		
		// redo everything new cell and/or tree
		// empty layout and delete first
		removeLayout(ui.frmTimepointStatus->layout());
		delete ui.frmTimepointStatus->layout();
		this->buttonPerTimepoint.clear();

		// create the frame layout
		QHBoxLayout* frameLayout = new QHBoxLayout();

		// check and add the active and inspected columns
		int indexColActive = chunckColumns.indexOf("active");
		// if active column has not been created
		if (indexColActive < 0){
			if(!cltData->addQuantificationColumn(quantIndex, "active", CellLineageTree::Double, false))
				qDebug()<<"Error with adding quantification column active, updateCell method in PositionQuantification class, quantIndex = -1 case.";
		}

		int indexColInspected = chunckColumns.indexOf("inspected");
		// if inspected column has not been created
		if (indexColInspected < 0){
			if(!cltData->addQuantificationColumn(quantIndex, "inspected", CellLineageTree::Double, false))
				qDebug()<<"Error with adding quantification column inspected, updateCell method in PositionQuantification class, quantIndex = -1 case.";
		}

		QMap<int, ITrackPoint*> trackPTs = cell->getTrackPointsRange(-1,-1);

		foreach(ITrackPoint* tp, trackPTs)
		{
			QVBoxLayout* colLayout = new QVBoxLayout();

			int timePoint = tp->getTimePoint();

			// create the relevant frame index for this timepoint
			int zIndex = 1; // default z index
			QVector<int> frameIndex;
			//frame index matrix
			//TimePoint
			frameIndex << timePoint;
			//FieldOfView(pos)
			frameIndex << positionNumber;
			//ZIndex
			frameIndex << zIndex;
			//ImagingChannel - this is the wavelength of the segmentation image
			frameIndex << detectionWL;

			QVector<QVariant> data = cltData->getQuantificationDataPoint(quantIndex, cell->getTrackNumber(), frameIndex);

			int isInspected = -1;
			int isActive = -1;
			bool isAvailable = true;

			if (!data.empty()) {
				isInspected = data.at(chunckColumns.indexOf("inspected")).toInt();
				isActive = data.at(chunckColumns.indexOf("active")).toInt();
			}
			else
				isAvailable = false;

			// create a button
			MyTimepointButton* newButton = new MyTimepointButton(this);
			newButton->setMaximumSize(30, 30);
			newButton->setFlat(true);
			newButton->setToolTip(QString("Timepoint %1").arg(timePoint));
			newButton->setTimepointID(timePoint);
			newButton->setCellID(cellNum);
			newButton->setTreeName(treeName);

			connect(newButton, SIGNAL(clicked()), this, SLOT(updateSelectedIndexTimepointFrame()));

			if (isAvailable) {

				newButton->setTimepointAvailable(true);

				if (isInspected && isActive) {
					newButton->setIcon(timeInspectedActive);
					newButton->setTimepointActive(true);
					newButton->setTimepointInspected(true);
				}
				if (isInspected && !isActive) {
					newButton->setIcon(timeInspectedInactive);
					newButton->setTimepointActive(false);
					newButton->setTimepointInspected(true);
				}
				if (!isInspected && isActive) {
					newButton->setIcon(timeNotInspectedActive);
					newButton->setTimepointActive(true);
					newButton->setTimepointInspected(false);
				}
				if (!isInspected && !isActive) {
					newButton->setIcon(timeNotInspectedInactive);
					newButton->setTimepointActive(false);
					newButton->setTimepointInspected(false);
				}
			}
			else {
				newButton->setIcon(timeNotPresent);
				newButton->setTimepointAvailable(false);
			}

			newButton->setContextMenuPolicy(Qt::CustomContextMenu);
			connect(newButton, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(timepointContextMenu(const QPoint&)));

			this->buttonPerTimepoint.insert(timePoint, newButton);

			colLayout->addWidget(newButton);

			if (timepoint.toInt() == timePoint){
				newButton->setDown(true);
				this->currentSelectedButton = newButton;
				this->populateMenuTimepoints();
			}	

			colLayout->addStretch();		
			frameLayout->addLayout(colLayout);
		}

		frameLayout->addStretch();
		ui.frmTimepointStatus->setLayout(frameLayout);
	}
}

void SegEditorMain::manualSave()
{

	QModelIndex selectedIndex  = this->m_tvwDataTreeOverview->selectedIndexes().at(0);

	if (!selectedIndex.isValid())
		return;

	// check first if need to save changes
	ImgGraphicsView* grpView = this->wavelengthViews.value("seg");

	if (!grpView)
		return;

	QString position="";
	QString experimentName = "";
	QString treeName = "";
	QString cellNum="";
	QString timepoint="";


	QString path = this->pathToExperiments;

	if (selectedIndex.isValid()) {

		TreeItem* selectedItem = static_cast<TreeItem*>(selectedIndex.internalPointer());

		QVector<QString> selectionInfo = getIdentificationInfo(selectedItem);
		experimentName = selectionInfo.at(0);
		position = selectionInfo.at(1);
		treeName = selectionInfo.at(2); 
		cellNum = selectionInfo.at(3);
		timepoint = selectionInfo.at(4);

		QString pathToImage = path + "\\" + position + "\\";
		QString outputfilePathImage = pathToImage + "Segmentation\\" + this->imgFileNameSegmented;

		QImage img = grpView->getImage();
		QImage::Format test = img.format();
		if (img.isNull() || img.format() == QImage::Format_Invalid) {
			grpView->setIsModifiedByUser(false);
			emit updateMsgLog(QString("Error > Quantification not saved. Invalid image format for segmentation image %1...").arg(this->imgFileNameSegmented), Qt::red);
			return;
		}

		if (!QTFyImageOperations::saveQImageToFile(img, outputfilePathImage)) {
			emit updateMsgLog(QString("Error > Quantification not saved. Invalid image format for segmentation image %1...").arg(this->imgFileNameSegmented), Qt::red);
			return;
		}

		updateTimepointQuantification(position, treeName, cellNum, timepoint, true);
		updateTimepointStatusFrame(treeName, cellNum, timepoint);
		QString msg = QString("Focus set on cell: %1, timepoint: %2").arg(cellNum).arg(timepoint);
		QMainWindow::statusBar()->showMessage(msg);

		grpView->setIsModifiedByUser(false);
	}

}

void SegEditorMain::manualSaveAll()
{

	QModelIndex selectedIndex  = this->m_tvwDataTreeOverview->selectedIndexes().at(0);

	if (!selectedIndex.isValid())
		return;

	// check first if need to save changes
	ImgGraphicsView* grpView = this->wavelengthViews.value("seg");

	if (!grpView)
		return;

	QString position="";
	QString experimentName = "";
	QString treeName = "";
	QString cellNum="";
	QString timepoint="";


	QString path = this->pathToExperiments;

	if (selectedIndex.isValid()) {

		TreeItem* selectedItem = static_cast<TreeItem*>(selectedIndex.internalPointer());

		QVector<QString> selectionInfo = getIdentificationInfo(selectedItem);
		experimentName = selectionInfo.at(0);
		position = selectionInfo.at(1);
		treeName = selectionInfo.at(2); 
		cellNum = selectionInfo.at(3);
		timepoint = selectionInfo.at(4);

		QString pathToImage = path + "\\" + position + "\\";
		QString outputfilePathImage = pathToImage + "Segmentation\\" + this->imgFileNameSegmented;

		// save output files here
		QImage img = grpView->getImage();
		if (img.isNull() || img.format() == QImage::Format_Invalid) {
			grpView->setIsModifiedByUser(false);
			emit updateMsgLog(QString("Error > Quantification not saved. Invalid image format for segmentation image %1...").arg(this->imgFileNameSegmented), Qt::red);
			return;
		}

		if (!QTFyImageOperations::saveQImageToFile(img, outputfilePathImage)) {
			emit updateMsgLog(QString("Error > Quantification not saved. Invalid image format for segmentation image %1...").arg(this->imgFileNameSegmented), Qt::red);
			return;
		}

		updateTimepointQuantification(position, treeName, cellNum, timepoint, false);
		updateTimepointStatusFrame(treeName, cellNum, timepoint);
		QString msg = QString("Focus set on cell: %1, timepoint: %2").arg(cellNum).arg(timepoint);
		QMainWindow::statusBar()->showMessage(msg);
	}

}

//SLOT
void SegEditorMain::updateSelectedIndexStagger(const QModelIndex &_selectedIndex, QString _chunkName){
	//check if window of the same chunk
	QStringList list = this->windowTitle().split(": ");
	QString chunk = list[1];
	if(chunk == _chunkName){
		this->m_tvwDataTreeOverview->setCurrentIndex(_selectedIndex);
		updateImageViewerQTFy(_selectedIndex);
	}
}