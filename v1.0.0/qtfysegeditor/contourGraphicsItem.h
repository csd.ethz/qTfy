/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/****************************************************************************
**
** @ Laura Skylaki, 20.11.2013
**
**
****************************************************************************/


#ifndef CONTOURGRAPHICSITEM_H
#define CONTOURGRAPHICSITEM_H


// QT
#include <QPainter>
#include <QImage>
#include <QPointF>
#include <QWidget>
#include <QGraphicsItem>
#include <QGraphicsPixmapItem>


// OPENCV
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"

using std::vector;
using namespace cv;

class contourGraphicsItem : public QGraphicsItem
{

public:
	contourGraphicsItem(QPolygon contour, vector<Point> contourPoints);
	~contourGraphicsItem(){};

	void paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *) override;
    QRectF boundingRect() const override;

	void setContourPolygon(QPolygon _polygon) {
		this->m_contour = _polygon;
	}

	void setContourPoints(vector<Point> _points) {
		this->m_contourPoints = _points;
	}

	// set the contour pen
	void setHovered(bool isWithinMouseRadius);

	//set the contour pen when it overlaps with marker
	void setMarkerOverlap (bool isOverlappedByMarker);

	//set the contour pen when it overlaps with marker
	void setContourPen (QPen _pen){
		this->pen = _pen;
	}

	vector<Point> getContourPoints() {return this->m_contourPoints; }

	bool hasMarker() {return m_isMarkerOverlapped; }

protected:
	//void mousePressEvent(QMouseEvent *event);
	//void mouseMoveEvent(QMouseEvent *event);
	//void mouseReleaseEvent(QMouseEvent *event);
	//void wheelEvent (QWheelEvent * event );


private:

	// the path through the points that comprise the contour
	QPolygon m_contour;
	bool m_isWithinMouseRadius;
	bool m_isMarkerOverlapped;

	// the pen used to draw the contour
	QPen pen;

	// the contour that corresponds to the current path
	vector<Point> m_contourPoints;

};

#endif // CONTOURGRAPHICSITEM_H