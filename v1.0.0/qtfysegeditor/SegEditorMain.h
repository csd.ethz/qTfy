/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SEGEDITORMAIN_H
#define SEGEDITORMAIN_H


// PROJECT
#include "ImgGraphicsView.h"
#include "imagedisplaysettings.h"
#include "mytimepointbutton.h"
#include "qtfyeditor/mymdisubwindow.h"
#include "qtfyeditor/treeitem.h"
#include "ui_frmSegEditorGUI.h"

// QT
#include <QtGui/QMainWindow>
#include <QScrollArea>
#include <QGraphicsPixmapItem>
#include <QThreadPool>
#include <QPushButton>


// OPENCV
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"

// Forward declarations
class FrmDisplaySettings;
class TreeOverviewItemView;
class TreeModel;
class ITrack;
class SegMarker;

class SegEditorMain : public QMainWindow
{
	Q_OBJECT

public:
	SegEditorMain(QWidget *parent, const int _segWL, const int _segMethodID, const QString _windowTitle = "Segmentation Editor", const bool _stagExperiment = false);
	~SegEditorMain();


	void setExperimentsFilesFolder (QString path);

	void setupInterface();

	/**
	*
	**/
	void setCellsPerTimepoint (QMap<QString, QMap<int, QVector<ITrack*> > > _cellsPerTimepoint);

	void setCellsPerTree (QMap< QString, QMap< QString, ITrack* > > _cellsPerTree);

	// initialisation for tree model coming from QTFy
	// treemodel based on tree >> cell >> timepoint representation
	void initializeDataQTFy(TreeOverviewItemView* _treeview, TreeModel* _treeModel);

	public slots:
		/**
		*	Opens the segmentation editor display options form
		*/
		void openSegmentationEditorDisplayOptions();

		/************************************************************************/
		/* updates the selected index of the treeview                           */
		/************************************************************************/
		void updateSelectedIndex (const QModelIndex& _selectedIndex);

		void updateSelectedIndexStagger(const QModelIndex& _selectedIndex, QString _chunkName);

		/**
		*	Updates the current cell in focus based on the marker in focus
		*/
		void newCellInFocus(SegMarker _mrk);

signals:
		void treeviewIndexChanged(const QModelIndex &_selectedIndex);

		void treeviewIndexChangedStagger(const QModelIndex &_selectedIndex, QString _chunkName);

		void cellUpdated(ITrack* _cell);

		void newMarkerInFocus(bool isLeft);

		// updates the message log text box in the loading form
		void updateMsgLog(QString, QColor);

		// updates the progress bar in the loading form
		void updateProgressBar();

		// updates the maximum value of the progress bar in the loading form
		void setProgressBarMax(int);

		//tells qtfy editor to update the tree model (relevant only in staggering experiments)
		void updateTreeModel(QString _detectionWavelength);


private slots:
	
	/**
	 * sets the autosave on/off
	 */
	void setAutoSave(bool on);

	// should the interface display all contours
	// or just the current cell?
	void showSingleContour (bool on);

	void manualSave();
	void manualSaveAll();

	void setEraserState(bool _state);
	void setAutoEraserState(bool _state);
	void setWandState(bool _state);
	void setPenState(bool _state);
	void invertWandState();
	void showHelpWandOptions();

	void resetWandOptions();

	void penColor();
	void penWidth();
	void eraserDiameter();

	void closeSubwindow( myMdiSubWindow* _subwindow );

	/**
	* load new images to the graphic views
	* when the tree model comes from QTFy
	*/
	void updateImageViewerQTFy(const QModelIndex& index);

	// when the selection of display/quantify wavelengths has been changed
	void updateDisplay();

	// updates the quantification after a modification has been performed by the user
	void updateTimepointQuantification(QString _posIndex, QString _treeName, QString _cellNumber, QString _timepoint, bool _isSingleCell);

	// update scrollbars coordination in views
	void updateHScrollBarInViews(int _newValue, bool _isZooming, QString _viewID);
	void updateVScrollBarInViews(int _newValue, bool _isZooming, QString _viewID);
	void updateViewsZoomFactor(qreal _newZoomFactor);

	// to change the contrast of the views-images
	void displayHistogramSettingsClicked();

	// to update the display settings form
	void updateDisplaySettingsForm();

	// reset the display settings to 0 -255 black white points
	void resetDisplaySettings();

	// reset all views to the original contours
	void resetToOriginalContours();

	/**
	 * histogram display settings were changed.
	 */
	void histogramSettingsChanged();

	// to display the memo of seg editors shortcuts
	void displayShortcutMemo();

	// manages the arrow clicks for the user to navigate between cells and timepoints
	void navigationAction();

	// update selected index through timepoint push button
	void updateSelectedIndexTimepointFrame();

	// the context menu for the timepoint buttons
	void timepointContextMenu(const QPoint& pnt);
	// populate the timepoint submenu in the top main menu bar
	void populateMenuTimepoints();

	// slots for updating timepoint status
	void quantifyTimepoint();
	void setTimepointActive();
	void setTimepointInactive();
	void setTimepointInspected();
	void setTimepointNotInspected();
	void setTimepointStatus(QString _colName, double _newStatus, QModelIndex _index);
	
	void setAllTimepointsActive();
	void setAllTimepointsInactive();
	void setAllTimepointsInspected();
	void setAllTimepointsNotInspected();
	void setAllTimepointsStatus(QString _colName, double _newStatus, QModelIndex _index);

	void updateTpStatusFromQTFyEditor();
private:
	
	void createActions();

	void createShortcuts();

	// initializes view specific signals after the views have been created
	void initializeViewsSignals();
	
	// synchronizes all the wavelength views to behave similarly when zoom in/out
	void synchronizeViews();

	void displaySelectedImage(const QString expName, const QString pos, const int timepoint, const int zindex, const int wavelength, const QPointF _markPoint);	

	// set of operations for the contours identification and plotting
	void contourOperations(Mat& _maskImg, QPointF _markPoint);

	// the user has made modifications to the contour images
	// bool isModifiedByUser;

	// setups the initial options for the segmentation correction suggestions
	void setupOptionsForSegmentationCorrection();

	// create markers for objects
	// this will be the objects (cells) that will be marked on the screen
	// this will be the selected cell coming from the QTFy editor
	void createMarkersForObjectsQTFy(QPointF _markPoint, QString _treeName, QString _cellID, int timepoint);

	// get the trackpoint of the current cell in current timepoint
	QPointF getCurrentTrackPoint(QString _treeName, QString _cellID, int timepoint);
	
	// plot markers for objects i.e. the markers for the tracked cells for this position/timepoint
	void plotMarkersForObjects(QVector<SegMarker> markers);

	// updates the frame that contains the radio buttons with the timpoint status 
	// i.e. active/inactive, inspected/not inspected
	void updateTimepointStatusFrame(QString _treeName, QString _cellNumber, QString _timepoint);

	// identifies the button that represents the current timepoint
	void updateTimepointStatus(QString _timepoint);

	// removes the layout and deletes all children of the layout
	void removeLayout (QLayout* layout, bool deleteWidgets = true);


	QVector<QString> getIdentificationInfo (TreeItem* _selectedItem);

	
	// the data tree view
	TreeOverviewItemView *m_tvwDataTreeOverview;

	// the tree data model
	TreeModel *m_TreeDataModel;

	QHash<QString, ImgGraphicsView*> wavelengthViews;
	//ImgGraphicsView *gvImageViewerSeg;
	//ImgGraphicsView *gvImageViewerRaw;
	QString imgFileNameOriginal;
	QString imgFileNameSegmented;
	QString imgFileNameReplaced;


	// timepoint status pixmaps
	QPixmap timeNotPresent;
	QPixmap timeInspectedInactive;
	QPixmap timeInspectedActive;
	QPixmap timeNotInspectedActive;
	QPixmap timeNotInspectedInactive;
	
	// running log and messages
	QString msgLog;

	// the file path to the folder holding all original TAT experiments
	// required to get access to the original images of tracked trees
	QString pathToExperiments;

	// the collection of cells per tree
	QMap< QString, QMap< QString, ITrack* > > cellsPerTree;

	// the collection of cells per timepoint
	QMap<QString, QMap<int, QVector<ITrack*> > > cellsPerTimepoint;

	// stores the collection of buttons available in the timepoint status frame
	QMap<int, MyTimepointButton*> buttonPerTimepoint;

	MyTimepointButton* currentSelectedButton;
	
	// is the automatic save function selected
	bool isAutoSaveON;

	// should just the current cell contour be displayed?
	bool singleContourDisplay;


	// detection wavelength and segmentation method ID for quantification updates
	int inspectedDetectionWL;
	int inspectedSegmentationMethodID;

	// the current zoom factor of all views
	qreal viewsZoomFactor;
	
	
	// wavelength used for detection (based on the quantification chunk name)
	const int detectionWL;

	// method used to produce the segmentation image (default is 1 for Otsu local)
	const int segmentationMethodID;

	// used to update the timepoints of a cell frame
	int currentCellNumber;
	int previousCellNumber;
	QModelIndex previousIndex;

	// the collection of contour in the initial image
	vector<vector <Point> > originalContours;

	// the marker of the current cell
	SegMarker mrkCurrentCell;

	const bool staggerExperiment;

	// constants
	static const int ZSTACK_INDEX;

	Ui::SegEditorGUI ui;


};

#endif // SEGEDITORMAIN_H
