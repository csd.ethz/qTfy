/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/****************************************************************************
**
** @ Stavroula Skylaki 15.11.2015
**
**
****************************************************************************/

#ifndef	MYTIMEPOINTBUTTON_H
#define MYTIMEPOINTBUTTON_H

//PROJECT

//QT
#include <QWidget>
#include <QPushButton>

class MyTimepointButton:public QPushButton
{
	Q_OBJECT

public:

	//constructor
	MyTimepointButton( QWidget * parent = 0);

	//destructor
	~MyTimepointButton();

	void setTimepointActive (bool _isActive) {
		this->isActive = _isActive;
	}

	void setTimepointInspected (bool _isInspected) {
		this->isInspected = _isInspected;
	}

	void setTimepointAvailable (bool _isAvailable) {
		this->isAvailable = _isAvailable;
	}

	bool isTimepointActive () {
		return this->isActive;
	}

	bool isTimepointInspected () {
		return this->isInspected;
	}

	bool isTimepointAvailable () {
		return this->isAvailable;
	}

	void setTimepointID (int _id){
		this->timepointID = _id;
	}

	int getTimepointID() {
		return this->timepointID;
	}

	void setCellID (QString _id){
		this->cellID = _id;
	}

	QString getCellID() {
		return this->cellID;
	}

	void setTreeName (QString _treename){
		this->treeName = _treename;
	}

	QString getTreeName() {
		return this->treeName;
	}

signals:
	

private:

	bool isActive;
	bool isInspected;
	bool isAvailable;

	int timepointID;
	QString cellID;
	QString treeName;


};
#endif //MYTIMEPOINTBUTTON_H