/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CORRECTIONUISETTINGS_H
#define CORRECTIONUISETTINGS_H


#include <QDialog>
#include "QVector"
#include "ui_frmCorrectionUISettings.h"

class CorrectionUISettings : public QDialog
{
	Q_OBJECT

public:
	/**
	 * returns the static instance of this class; if it does not yet exist, it is created and initialized
	 * @return the (unique) CorrectionUISettings instance
	 */
	static CorrectionUISettings* getInst();
	~CorrectionUISettings();

	static QVector<int> corrWavelengthsToDisplay;

	// setups the signal/slot connections
	void setupConnections();

	// update selected checkboxes based on elements of corrWavelengthsToDisplay
	void updateDisplay();

signals:
	// emitted when the wavelengths selection has been changed
	// and the segmentation editor needs to display the new selection
	void corrSettingsUpdated();

private slots:
	void cancel();	
	void saveSettings();
	void showHelp();
	

private:

	//the static global instance (unique in the complete program)
	static CorrectionUISettings *inst;

	// Private Constructors to prevent instatiation outside of getInst()
	CorrectionUISettings();	

	// Private Copy Constructor to prevent copying
	CorrectionUISettings(const CorrectionUISettings &);
	CorrectionUISettings& operator=(const CorrectionUISettings &);

	
	// The ui
	Ui::frmCorrectionUISettings ui;


};

#endif CORRECTIONUISETTINGS_H