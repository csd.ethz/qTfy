/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/****************************************************************************
**
** @ Laura Skylaki, 20.11.2013
**
**
****************************************************************************/

// PROJECT
#include "contourGraphicsItem.h"

// QT
#include <QDebug>


contourGraphicsItem::contourGraphicsItem(QPolygon contour, vector<Point> contourPoints)
{
	this->m_contourPoints = contourPoints;
	this->m_contour = contour;
	this->m_isWithinMouseRadius = false;
	this->m_isMarkerOverlapped = false;

	// setAcceptHoverEvents(true);
}

void contourGraphicsItem::paint(QPainter *painter,
                                     const QStyleOptionGraphicsItem *, QWidget*) 
{

	painter->setPen(this->pen);
	painter->drawPolygon(this->m_contour);
}

QRectF contourGraphicsItem::boundingRect() const 
{
	return this->m_contour.boundingRect();
}

// set the contour pen
void contourGraphicsItem::setHovered(bool isWithinMouseRadius)
{
	this->m_isWithinMouseRadius = isWithinMouseRadius;
}

//set the contour pen when it overlaps with marker
void contourGraphicsItem::setMarkerOverlap (bool isOverlappedByMarker)
{
	this->m_isMarkerOverlapped = isOverlappedByMarker;
	this->update();
}


// sample code for mouse hover
// also need to set in qgraphicsview 
// setMouseTracking(true);
// viewport()->setMouseTracking(true);
//void MyGraphicsItem::hoverEnterEvent(QGraphicsSceneHoverEvent* event)
//{
//    update (boundingRect());
//}