/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Project
#include "shortcuthelp.h"


// QT
#include <QAbstractItemView>


ShortcutHelp::ShortcutHelp(QWidget *parent)
{
	// Setup ui
	ui.setupUi(this);
	connect ( ui.pbtClose, SIGNAL (clicked()), this, SLOT (ok()));
	// Enable maximize and minimize buttons
	this->setWindowFlags(Qt::Window);

	// setup the table
	QStringList m_TableHeader;
	m_TableHeader<<"Shortcut"<<"Description";

	ui.twgShortcutMemo->setRowCount(20);
	ui.twgShortcutMemo->setColumnCount(2);


	ui.twgShortcutMemo->setHorizontalHeaderLabels(m_TableHeader);
	ui.twgShortcutMemo->verticalHeader()->setVisible(false);
	ui.twgShortcutMemo->setEditTriggers(QAbstractItemView::NoEditTriggers);
	ui.twgShortcutMemo->setSelectionMode(QAbstractItemView::NoSelection);
	
	int count = 0;
	QIcon previous(":/qtfyres/qtfyres/go-previous-7.png");
	ui.twgShortcutMemo->setItem(count, 0, new QTableWidgetItem("A or Left"));
	ui.twgShortcutMemo->setItem(count, 1, new QTableWidgetItem(previous, "Go to previous timepoint"));
	count++;

	QIcon next(":/qtfyres/qtfyres/go-next-7.png");
	ui.twgShortcutMemo->setItem(count, 0, new QTableWidgetItem("D or Right"));
	ui.twgShortcutMemo->setItem(count, 1, new QTableWidgetItem(next, "Go to next timepoint"));
	count++;

	QIcon center(":/qtfyres/qtfyres/center.png");
	ui.twgShortcutMemo->setItem(count, 0, new QTableWidgetItem("X or Up or Down"));
	ui.twgShortcutMemo->setItem(count, 1, new QTableWidgetItem(center, "Bring cell in focus"));
	count++;

	QIcon wand(":/qtfyres/qtfyres/magic-wand.png");
	ui.twgShortcutMemo->setItem(count, 0, new QTableWidgetItem("W"));
	ui.twgShortcutMemo->setItem(count, 1, new QTableWidgetItem(wand, "Pick/drop magic wand"));
	count++;

	QIcon eraser(":/qtfyres/qtfyres/draw-eraser-24.png");
	ui.twgShortcutMemo->setItem(count, 0, new QTableWidgetItem("E"));
	ui.twgShortcutMemo->setItem(count, 1, new QTableWidgetItem(eraser,"Pick/drop eraser. Drag over contour to delete."));
	count++;

	QIcon save(":/qtfyres/qtfyres/singleCell-save-big.png");
	ui.twgShortcutMemo->setItem(count, 0, new QTableWidgetItem("Ctrl+S"));
	ui.twgShortcutMemo->setItem(count, 1, new QTableWidgetItem(save, "Saves the contour of the selected cell, performs quantification and sets the cell as inspected."));
	count++;

	QIcon saveAll(":/qtfyres/qtfyres/multipleCell-save-big.png");
	ui.twgShortcutMemo->setItem(count, 0, new QTableWidgetItem("Ctrl+Shift+S"));
	ui.twgShortcutMemo->setItem(count, 1, new QTableWidgetItem(saveAll, "Saves the contours of all the cells in the image, performs quantification and sets the cells as inspected (slower)."));
	count++;

	ui.twgShortcutMemo->setItem(count, 0, new QTableWidgetItem("Ctrl+A"));
	ui.twgShortcutMemo->setItem(count, 1, new QTableWidgetItem("Set current timepoint active"));
	count++;

	ui.twgShortcutMemo->setItem(count, 0, new QTableWidgetItem("Ctrl+Shift+A"));
	ui.twgShortcutMemo->setItem(count, 1, new QTableWidgetItem("Set current timepoint inactive"));
	count++;

	ui.twgShortcutMemo->setItem(count, 0, new QTableWidgetItem("Ctrl+Z"));
	ui.twgShortcutMemo->setItem(count, 1, new QTableWidgetItem("Set current timepoint inspected"));
	count++;

	ui.twgShortcutMemo->setItem(count, 0, new QTableWidgetItem("Ctrl+Shift+Z"));
	ui.twgShortcutMemo->setItem(count, 1, new QTableWidgetItem("Set current timepoint not inspected"));
	count++;

	QIcon histo(":/qtfyres/qtfyres/view-object-histogram.png");
	ui.twgShortcutMemo->setItem(count, 0, new QTableWidgetItem("Ctrl+H"));
	ui.twgShortcutMemo->setItem(count, 1, new QTableWidgetItem(histo, "Open histogram adjustment form"));
	count++;

	QIcon contour(":/qtfyres/qtfyres/cell_SingleContour.png");
	ui.twgShortcutMemo->setItem(count, 0, new QTableWidgetItem("Ctrl+C"));
	ui.twgShortcutMemo->setItem(count, 1, new QTableWidgetItem(contour, "Contours visible on/off"));
	count++;

	QIcon pen(":/qtfyres/qtfyres/pencilBigger.png");
	ui.twgShortcutMemo->setItem(count, 0, new QTableWidgetItem("Ctrl + Mouse Wheel"));
	ui.twgShortcutMemo->setItem(count, 1, new QTableWidgetItem(pen, "Adjust pen width (when you are in freehand mode, magic wand not checked)"));
	count++;

	ui.twgShortcutMemo->setItem(count, 0, new QTableWidgetItem("Mouse Wheel"));
	ui.twgShortcutMemo->setItem(count, 1, new QTableWidgetItem("Zoom in/out in views"));
	count++;

	ui.twgShortcutMemo->setItem(count, 0, new QTableWidgetItem("Middle mouse button pressed down"));
	ui.twgShortcutMemo->setItem(count, 1, new QTableWidgetItem("Pan"));
	count++;

	ui.twgShortcutMemo->setItem(count, 0, new QTableWidgetItem("Ctrl + W"));
	ui.twgShortcutMemo->setItem(count, 1, new QTableWidgetItem("Close active window"));
	count++;

	ui.twgShortcutMemo->resizeColumnsToContents();


}

ShortcutHelp::~ShortcutHelp()
{
}

//SLOT:
void ShortcutHelp::ok()
{
	ShortcutHelp::close();
}
