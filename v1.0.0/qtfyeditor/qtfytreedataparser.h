/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Eleni Skylaki
 * @date 
 *
 * This class provides functions to list trees, cells and timepoints
 * in order to create the tree structure of the tree data Overview
 */


#ifndef TREEDATAPARSER_H
#define TREEDATAPARSER_H

// PROJECT
#include "treemodel.h"
#include "../qtfybackend/tttmanager.h"
#include "../qtfybackend/tttpositionmanager.h"
#include "../qtfybackend/fileinfoarray.h"

// QT
#include <QStringList>
#include <QDir>
#include <QFileSystemModel>


class ITrack;
class ITree;

class QtfyTreeDataParser : public QObject
{
    Q_OBJECT

public:

    QtfyTreeDataParser(TreeModel* ExperimentModel, QObject *parent = 0);

	/**
     * Function based on QDir::entrylist, used in case of errors
     * @param _path the path to search in
     * @param _filters file extensions to use as filter
	 * @return a collection with all cells (itracks) that are present per timepoint
     * the result can be obtained by getModel()
     */
     void CreateExperimentModel(const QVector<QSharedPointer<ITree>> _trees);/*, QString _chunkName = "");*/


    /**
     * @brief getModel
     * @return the TreeModel that will be used to populate the experiment overview QTreeView
     */
    TreeModel getModel() const;

	/**
     * @brief 
     * @return 
     */
	QMap< QString, QMap<ITrack*, QVector<int> > > getTimepointsPerCell();

	QMap< QString, QMap< QString, ITrack* > > getCellsPerTree();

	QMap< QString, QMap<int, QVector<ITrack*> > > getCellsPerTimepoint();

	void isStaggeringExperiment(bool _value, QString _wavelength = "0");

private:

	// the map with the timepoints per cell per tree
	// the key is the tree name
	QMap< QString, QMap<ITrack*, QVector<int> > > timepointsPerCell;

	// the map with the cells per tree
	// the key is the tree name
	// the second key is the cellNumber
	QMap< QString, QMap< QString, ITrack* > > cellsPerTree;


	// the map with the cells per timepoint
	QMap< QString, QMap<int, QVector<ITrack*> > > cellsPerTimepoint;

    // the FileSystemModel
    TreeModel *ExperimentModel;

	bool stagger;
	QString stagWavelength;
};

#endif // TREEDATAPARSER_H
