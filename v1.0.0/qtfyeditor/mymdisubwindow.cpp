/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/****************************************************************************
**
** @ Eleni Skylaki 12.08.2014
**
**
****************************************************************************/

// PROJECT
#include "mymdisubwindow.h"

// QWT


// QT
#include <QCloseEvent>
#include <QResizeEvent>
#include <QToolTip>
#include <qlayoutitem.h>
#include <qlayout.h>


myMdiSubWindow::myMdiSubWindow(QWidget *parent, Qt::WindowFlags flags):
	QMdiSubWindow(parent,flags)
{
	this->windowActive = true;
}

myMdiSubWindow::~myMdiSubWindow()
{
}

void myMdiSubWindow::closeEvent ( QCloseEvent * closeEvent )
{
	emit closed( this );
	this->close();

}

void myMdiSubWindow::setWindowActive(bool _activationState)
{
	this->windowActive = _activationState;
}

bool  myMdiSubWindow::isWindowActive()
{
	return this->windowActive;
}
