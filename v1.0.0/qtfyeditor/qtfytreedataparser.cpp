/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Eleni Skylaki
 * @date 10/07/2014
 */

// PROJECT
#include "qtfytreedataparser.h"
#include "qtfybackend/itrack.h"
#include "qtfybackend/tttmanager.h"
#include "qtfybackend/itrackpoint.h"
#include "qtfybackend/itrack.h"
#include "qtfydata/tree.h"
#include "qtfydata/treestructuralhelper.h"

// QT
#include <QDirIterator>
#include <QHash>
#include <QElapsedTimer>
#include <QDebug>


QtfyTreeDataParser::QtfyTreeDataParser(TreeModel *ExperimentModel, QObject *parent)
    :QObject(parent)
{
    this->ExperimentModel = ExperimentModel;
	this->stagger = false;
	this->stagWavelength = "0";
}


void QtfyTreeDataParser::CreateExperimentModel(const QVector<QSharedPointer<ITree>> _trees)/*, QString _chunkName)*/
{
	// store here discovered positions
	QHash<QString, TreeItem*> discoveredExperiments;	

	//the map with the timepoints per cell
	QMap<ITrack*, QVector<int> > timepointspc;

	// the map with the cells per tree
	QMap< QString, ITrack*> cells;

	// go through the list of trees 
	for (QVector<QSharedPointer<ITree>>::const_iterator treeIterator = _trees.constBegin(); treeIterator != _trees.constEnd(); ++treeIterator) {
		ITree* currentTree = (*treeIterator).data();

		// find the data path for the images
		QString experimentName = currentTree->getExperimentName();
		QString treeName = currentTree->getTreeName();

		// if it's a new position, add it to list and create tree item
		if (!discoveredExperiments.contains(experimentName)) {

			// create the tree node
			QList<QVariant> columnData;
			columnData << experimentName;
			TreeItem* newExperiment = new TreeItem(columnData, this->ExperimentModel->getRoot());
			this->ExperimentModel->getRoot()->appendChild(newExperiment);

			// add it to the hash
			discoveredExperiments.insert(experimentName, newExperiment);

			// add it to the timepointsPerCell per position
			this->timepointsPerCell.insert(treeName, timepointspc);
		}
		//create tree node
		QString positionIndex = currentTree->getPositionIndex();

		QList<QVariant> treeData;
		treeData << treeName << positionIndex;
		TreeItem* newTree = new TreeItem( treeData, discoveredExperiments.value(experimentName));

		discoveredExperiments.value(experimentName)->appendChild(newTree);

		// get the final timepoint of the tree
		int lastTimepoint = TreeStructuralHelper::getLastTreeTimePoint(currentTree);
		QMap<int, QVector<ITrack*>> cellsOfTimepoint;

		for (int i =0; i <= lastTimepoint; i++)
			cellsOfTimepoint.insert(i, QVector<ITrack*>());

		//get max generation
		int maxGeneration = currentTree->getTreeProperty(ITree::TREE_OPT_NUMBER_OF_GENERATIONS).toInt();

		//the list of all the cells of the tree
		QVector<ITrack*> cellList = TreeStructuralHelper::getTracksInGenerations(currentTree, 0, maxGeneration);

		// find the cells in the tree
		foreach(ITrack* cell, cellList)
		{
			int trackNum = cell->getTrackNumber();
			QString cellName = "Cell_" + QString::number(trackNum);

			cells.insert(QString::number(trackNum), cell);

			QList<QVariant> cellData;
			cellData<< cellName << QString::number(trackNum); 
			TreeItem* newCell = new TreeItem(cellData, newTree);

			newTree->appendChild(newCell);

			QMap<int, ITrackPoint*> trackPTs = cell->getTrackPointsRange(-1,-1);

			QVector<int> timepoints;

			// find the trackpoints where cell is present
			foreach(ITrackPoint* tp, trackPTs)
			{
				int tpNum = trackPTs.key(tp);

				//if timepointspc doesn't contain current cell
				if (!timepointspc.contains(cell)) {
					//add timepoint to the cell timepoints vector
					timepoints.append(tpNum);
				}

				if (cellsOfTimepoint.contains(tpNum)) {
					QVector<ITrack*> alreadyStored = cellsOfTimepoint.value(tpNum);
					alreadyStored.append(cell);
					cellsOfTimepoint.insert(tpNum, alreadyStored);
				}

				//TODO check if wlg has this timepoint
				//if staggering
				if(stagger){
					//find the experiment position
					QString pos = currentTree->getPositionIndex();
					int wavelength = this->stagWavelength.toInt();
					const FileInfoArray* fileInfo = TTTManager::getInst().getPositionManager(pos)->getFiles();
					if(!fileInfo->exists(tpNum, 1, wavelength))
 						continue;
					
				}

 				QString trackpointName = "Timepoint_" + QString::number(tpNum);
				QList<QVariant> tpData;
				tpData << trackpointName << QString::number(tpNum);
				TreeItem* newTP = new TreeItem(tpData, newCell);
				newCell->appendChild(newTP);

				// add the timepoint to this cell collection of timepoints
				timepointspc.insert(cell,timepoints);

				//clear timepoints vector
				timepoints.clear();
				this->timepointsPerCell.insert(treeName, timepointspc);

			}

		}	
		this->cellsPerTree.insert(treeName,cells);
		this->cellsPerTimepoint.insert(treeName, cellsOfTimepoint);
	}
}


QMap< QString, QMap<ITrack*, QVector<int> > > QtfyTreeDataParser::getTimepointsPerCell()
{
	return this->timepointsPerCell;
}

QMap< QString, QMap< QString, ITrack* > > QtfyTreeDataParser::getCellsPerTree()
{
	return this->cellsPerTree;
}


QMap< QString, QMap<int, QVector<ITrack*> > > QtfyTreeDataParser::getCellsPerTimepoint()
{
	return this->cellsPerTimepoint;
}

void QtfyTreeDataParser::isStaggeringExperiment(bool _value, QString _wavelength)
{
	this->stagger = true;
	this->stagWavelength = _wavelength;
}
