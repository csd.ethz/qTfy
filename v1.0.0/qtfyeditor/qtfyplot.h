/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/****************************************************************************
**
** @ Eleni Skylaki, Laura Skylaki, 12.08.2014
**
**
****************************************************************************/


#ifndef	QTFYPLOT_H
#define QTFYPLOT_H

//PROJECT
#include "qtfybackend/itrack.h"
#include "qtfybackend/mathfunctions.h"
#include "mymdisubwindow.h"

//QT
#include <QWidget>
#include <qmdisubwindow.h>
#include <qpixmap.h>

//QWT
#include "qwt/qwt_plot.h"
#include "qwt/qwt_plot_curve.h" 
#include "qwt/qwt_curve_fitter.h"
#include "qwt/qwt_plot_grid.h"
#include "qwt/qwt_plot_marker.h" 
#include "qwt/qwt_plot_zoomer.h" 
#include "qwt/qwt_symbol.h"
#include "qwt/qwt_scale_div.h"


class QTFyPlot:public QwtPlot
{
	Q_OBJECT

public:

	QTFyPlot(QWidget *parent = 0);	
	
	~QTFyPlot();

	void installPlotPicker (int _XaxisId, int _YaxisId) ;

	void getPlotPicker ();

	//clears plot
	/*void clearPlot();*/

	void changeCurveColor(ITrack* _cell, QColor _color/*, bool _transparent = false*/);

	void hideBranch(/*ITrack* _cell*/QVector<ITrack*> _cellList, bool _transparent, bool _isTreePlot = false);

	void detachCurve(QString _curveTitle);

	void boldCurve(QString _curveTitle, bool _fromTreePlot, int _width);

	myMdiSubWindow* getSubWindow();

	void setSubWindow(myMdiSubWindow* _window);

	QwtPlotCurve* getCurve(QString _curveName);

	/*void*/QPixmap exportPNG();

signals:

	void clickedPoint(const QwtDoublePoint &pnt);

protected:
    
	void mousePressEvent( QMouseEvent * event ); 

	void mouseReleaseEvent( QMouseEvent * event ); 

	void mouseMoveEvent(QMouseEvent *event);

private slots:
	
	void plotPointSelected(const QwtDoublePoint &) ;

	void scaleDivChangedSlot ();

private:

	myMdiSubWindow* subwindow;

};

#endif  // QTFYPLOT_H