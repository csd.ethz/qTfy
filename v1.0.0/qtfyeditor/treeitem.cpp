/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Laura Skylaki
 * @date 14/05/2013
 */

#include "treeitem.h"

// creates a new tree item with parent
TreeItem::TreeItem(const QList<QVariant> &data, TreeItem *parent)
{
    parentItem = parent;
    itemData = data;
	hidden = true;
}

TreeItem::~TreeItem()
{
    qDeleteAll(childItems);
}

// add a child
void TreeItem::appendChild(TreeItem *item)
{
    childItems.append(item);
}

// returns the child item in row _row
TreeItem *TreeItem::child(int row)
{
    return childItems.value(row);
}

//The number of child items held
int TreeItem::childCount() const
{
    return childItems.count();
}

// reports the item's location within its parent's list of items
int TreeItem::row() const
{
    if (parentItem)
        return parentItem->childItems.indexOf(const_cast<TreeItem*>(this));

    return 0;
}

// reports the item's column, -1 for root
int TreeItem::column() const
{
    if (!parentItem)
        return -1;
    else if (!parentItem->parentItem)
        return 0;
    else if (!parentItem->parentItem->parentItem)
        return 1;
	else if (!parentItem->parentItem->parentItem->parentItem)
		return 2;
    else 
		return 3;
}

// The number of columns of data in the item
int TreeItem::columnCount() const
{
    return itemData.count();
}

// Returns the column data
QVariant TreeItem::data(int column) const
{
    return itemData.value(column);
}

// The item's parent
TreeItem *TreeItem::parent()
{
    return parentItem;
}

void TreeItem::setHidden(bool hidden) {
	this->hidden = hidden; 
}

bool TreeItem::isIndexHidden() 
{
	return hidden;
}
