/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef	OUTLIERMANAGER_H
#define OUTLIERMANAGER_H

//PROJECT
#include "plotpickerhelp.h"
#include "qtfybackend/itrack.h"
#include "plottingoptions.h"
#include "qtfyplot.h"

//QT
#include <QPolygonF>
#include <qmap.h>


//QWT
#include "qwt/qwt_plot.h"
#include "qwt/qwt_plot_marker.h" 
#include "qwt/qwt_symbol.h"
#include "qwt/qwt_plot_curve.h"

class OutlierManager
{

public:
	//PUT CHECKS IF VECTORS ARE EMPTY!!!
	//constructor
	OutlierManager();
	//destructor
	~OutlierManager();
	
	// the map with the cells per tree
	// the key is the tree name
	// the second key is the cellNumber
	bool findOutliers(QTFyPlot* _plot, QString _attribute, QString _quantChunkName, ITree* _tree, QVector<int> _regardedGenerations/*QMap< QString, QMap< QString, ITrack* > > _cellsPerTree*/);

	QPolygonF findOriginalPoints(ITrack* _cell, QString _attribute, QString _quantChunkName);

	void showOutliers(QTFyPlot* _plot, ITree* _tree);

	//clears outliers everytime this method is called
	void deactivateOutliers(QString _attribute, ITree* _tree, QString _chunkName);

	void savePreviousDataEntry();

	QPolygonF findOutliersOfCell(QTFyPlot* _plot, QString _attribute, QString _quantChunkName, ITrack* _cell);

	//setters

	void setOutliers();

	void setSmoothingThreshold(int _sValue);

	void setDetectionThreshold(int _dValue);

	//getters

	void getOutliers();

	QPolygonF getOutliersOfTree(ITree* _tree);

	void undoDeactivation();

	QPolygonF findSmoothPoints(QPolygonF _originalPoints);

	QPolygonF detectOutliers(QTFyPlot* _plot, QPolygonF _originalPoints, QPolygonF _smoothedPoints);

	QPolygonF convertToPoints(QMap<int, QVariant> data);

	void drawOutliers(QTFyPlot* _plot, QPolygonF _outliers, QString _cellName);

	QPolygonF applyXFactorToData(double _xFactor, QPolygonF _data);

	QPolygonF detectOutliersDistance( QPolygonF _originalPoints, QPolygonF _smoothedPoints);

	void setMinMax(double _min, double _max);

	void setPlottingOptions(PlottingOptions* _opt);

	QwtPlotCurve* getCellOutlierCurve(QPolygonF _outliers, QString _cellName);

	//QPolygonF getOutliers(QString _attribute, ITree* _tree, QwtPlot* _plot);

private:

	int smoothThreshold;

	int detectionThreshold;

	double min, max;

	QHash<ITree*, QHash<ITrack*, QPolygonF>> outliers;

	QHash<QString, QHash<ITree*, QHash<ITrack*, QPolygonF>>> attrOutliers;

	//AMTAttribute* previousEntry;

	PlottingOptions* mOptions;

};
#endif