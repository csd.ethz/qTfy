/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Stavroula Skylaki
 * @date 17.11.2015
 */


// PROJECT
#include "attributedisplayselection.h"

// QT
#include <QStandardItemModel>
#include <QListView>


AttributeDisplaySelection* AttributeDisplaySelection::inst (0);

AttributeDisplaySelection::AttributeDisplaySelection(QWidget *parent)
{
	// Setup ui
	ui.setupUi(this);
	connect ( ui.pbtCancel, SIGNAL (clicked()), this, SLOT (cancel()));
	connect ( ui.pbtOK, SIGNAL(clicked()), this, SLOT(plotSelectedAttributes()));
	connect( ui.cbxQuantificationChunks, SIGNAL(currentIndexChanged(const QString &)), this, SLOT(updateAttributeTreeView(const QString &)));
	connect(ui.twgAvailableAttributes, SIGNAL(itemChanged(QTreeWidgetItem*, int)), this, SLOT(updateChecks(QTreeWidgetItem*, int)));

	// Enable maximize and minimize buttons
	this->setWindowFlags(Qt::Window);

}


AttributeDisplaySelection* AttributeDisplaySelection::getInst(QWidget *parent)
{
	if (! inst) {
		inst = new AttributeDisplaySelection(parent);
	}

	return inst;
}

AttributeDisplaySelection::~AttributeDisplaySelection()
{
}


void AttributeDisplaySelection::initializeData(QHash<QString, QVector<QString>> _chunkAttributes, QHash<QString, QString> _chunkSoftware, QHash<QString, QString> _chunkRemarks)
{
	this->allAttributes = _chunkAttributes;
	this->allChunkSoftware = _chunkSoftware;
	this->allChunkRemarks = _chunkRemarks;


	int totalChunks = _chunkAttributes.keys().size();

	QStandardItemModel* cbxModel = new  QStandardItemModel(totalChunks, 3);
	for (int i = 0; i < cbxModel->rowCount(); ++i)
	{
		QString chunkName = _chunkAttributes.keys().at(i);
		QString chunkSoftware = _chunkSoftware.value(chunkName);
		QString chunkRemark = _chunkRemarks.value(chunkName);
		QStandardItem* col0 = new QStandardItem(chunkName);
		if (chunkSoftware == "QTFy")
			col0->setIcon(QIcon(":/qtfyres/qtfyres/qtfylogoIcon.png"));
		col0->setToolTip(chunkRemark);
		//QStandardItem* col1 = new QStandardItem(chunkSoftware);
		/*QStandardItem* col1 = new QStandardItem(chunkRemark);*/
		cbxModel->setItem(i, 0, col0);
		//cbxModel->setItem(i, 1, col1);
		//cbxModel->setItem(i, 2, col2);
	}

	ui.cbxQuantificationChunks->setModel(cbxModel);

	QListView* myCbxView = new QListView(this);
	ui.cbxQuantificationChunks->setView(myCbxView);

}

//SLOT
void AttributeDisplaySelection::updateAttributeTreeView(const QString & _chunkName)
{
	/*if (!ui.twgAvailableAttributes->children().empty())*/
	ui.twgAvailableAttributes->clear();

	// create the treeview
	// get the attributes of the chunk
	QVector<QString> attributes = this->allAttributes.value(_chunkName);

	// is the chunk a QTFy chunk? then I have an idea about the attribute names
	bool isQTFyChunk = false;
	QString chunkSoftware = this->allChunkSoftware.value(_chunkName);
	if (chunkSoftware == "QTFy")
		isQTFyChunk = true;

	if (isQTFyChunk) {

		QVector<QString> attributeCategories;
		attributeCategories << "NoBgCorrected" << "BgCorrected" << "ImgDivByBgMinus1" << "ImgMinusBg" << "Morphology" << "Calculator";

		QHash<QString, QVector<QString>> attributesPerCategory;

		QVector<QString> attributeCategoriesCommonNames;
		attributeCategoriesCommonNames << "No background correction" << "Background correction (with gain)" << 
			"Background correction (image divided by background minus 1)" << "Background correction (image minus background)"<<"Morphology" << "Calculator";

		QVector<QString> attributeTypes;
		attributeTypes << "Sum" << "Mean" << "StdDev" << "CV" << "Area" << "Perimeter" << "Add" << "Subtract" << "Multiply" << "Divide";

		QVector<QTreeWidgetItem *> parents;

		foreach(QString attributeName, attributes) {
			foreach(QString attributeCategory, attributeCategories) {
				foreach (QString attributeType, attributeTypes) {
					QString specificSubName = attributeType + attributeCategory;
					if (attributeName.contains(specificSubName)) {
						QVector<QString> attributesInCategory = attributesPerCategory.value(specificSubName);
						attributesInCategory.append(attributeName);
						attributesPerCategory.insert(specificSubName, attributesInCategory);
					}
				}
			}
		}

		QTreeWidgetItem *bgNoCorrectedTreeWidgetItem = new QTreeWidgetItem(ui.twgAvailableAttributes);
		bgNoCorrectedTreeWidgetItem->setText(0, tr("No background correction"));
		bgNoCorrectedTreeWidgetItem->setText(1, "NoBgCorrected");
		bgNoCorrectedTreeWidgetItem->setChildIndicatorPolicy(QTreeWidgetItem::DontShowIndicatorWhenChildless);
		parents.append(bgNoCorrectedTreeWidgetItem);

		QTreeWidgetItem *bgCorrectedTreeWidgetItem = new QTreeWidgetItem(ui.twgAvailableAttributes);
		bgCorrectedTreeWidgetItem->setText(0, tr("Background correction (with gain)"));
		bgCorrectedTreeWidgetItem->setText(1, "BgCorrected");
		bgCorrectedTreeWidgetItem->setChildIndicatorPolicy(QTreeWidgetItem::DontShowIndicatorWhenChildless);
		bgCorrectedTreeWidgetItem->setExpanded(true);
		parents.append(bgCorrectedTreeWidgetItem);

		QTreeWidgetItem *bgImgDivByBgMinus1TreeWidgetItem = new QTreeWidgetItem(ui.twgAvailableAttributes);
		bgImgDivByBgMinus1TreeWidgetItem->setText(0, tr("Background correction (image divided by background minus 1)"));
		bgImgDivByBgMinus1TreeWidgetItem->setText(1, "ImgDivByBgMinus1");
		bgImgDivByBgMinus1TreeWidgetItem->setChildIndicatorPolicy(QTreeWidgetItem::DontShowIndicatorWhenChildless);
		parents.append(bgImgDivByBgMinus1TreeWidgetItem);

		QTreeWidgetItem *bgImgMinusBgTreeWidgetItem = new QTreeWidgetItem(ui.twgAvailableAttributes);
		bgImgMinusBgTreeWidgetItem->setText(0, tr("Background correction (image minus background)"));
		bgImgMinusBgTreeWidgetItem->setText(1, "ImgMinusBg");
		bgImgMinusBgTreeWidgetItem->setChildIndicatorPolicy(QTreeWidgetItem::DontShowIndicatorWhenChildless);
		parents.append(bgImgMinusBgTreeWidgetItem);

		
		QTreeWidgetItem *morphologyTreeWidgetItem = new QTreeWidgetItem(ui.twgAvailableAttributes);
		morphologyTreeWidgetItem->setText(0, tr("Morphology"));
		morphologyTreeWidgetItem->setText(1, "Morphology");
		morphologyTreeWidgetItem->setChildIndicatorPolicy(QTreeWidgetItem::DontShowIndicatorWhenChildless);
		parents.append(morphologyTreeWidgetItem);

		QTreeWidgetItem *calculatorTreeWidgetItem = new QTreeWidgetItem(ui.twgAvailableAttributes);
		calculatorTreeWidgetItem->setText(0, tr("Calculator"));
		calculatorTreeWidgetItem->setText(1, "Calculator");
		calculatorTreeWidgetItem->setChildIndicatorPolicy(QTreeWidgetItem::DontShowIndicatorWhenChildless);
		parents.append(calculatorTreeWidgetItem);



		foreach(QTreeWidgetItem *parentTreeItem, parents) {
			QString parentCategory = parentTreeItem->text(1);

			
			if(parentCategory == "Morphology") {
				//different categories
				//test only area and perimeter
				QTreeWidgetItem *area = new QTreeWidgetItem(parentTreeItem);
				area->setText(0, tr("Area"));
				area->setCheckState(0,Qt::Unchecked);
				area->setChildIndicatorPolicy(QTreeWidgetItem::DontShowIndicatorWhenChildless);
				QString subtext = "Area" + parentCategory;
				populateTreeWidgetChildren(area, attributesPerCategory, subtext);

				QTreeWidgetItem *perimeter = new QTreeWidgetItem(parentTreeItem);
				perimeter->setText(0, tr("Perimeter"));
				perimeter->setCheckState(0, Qt::Unchecked);
				perimeter->setChildIndicatorPolicy(QTreeWidgetItem::DontShowIndicatorWhenChildless);
				subtext = "Perimeter" + parentCategory;
				populateTreeWidgetChildren(perimeter, attributesPerCategory, subtext);

			}
			else if(parentCategory == "Calculator") {
				QString subtext = "";
				QTreeWidgetItem *AddMath = new QTreeWidgetItem(parentTreeItem);
				AddMath->setText(0, tr("Add"));
				AddMath->setCheckState(0, Qt::Unchecked);
				AddMath->setChildIndicatorPolicy(QTreeWidgetItem::DontShowIndicatorWhenChildless);
				subtext = "Add" + parentCategory;
				populateTreeWidgetChildren(AddMath, attributesPerCategory, subtext);

				QTreeWidgetItem *SubtractMath = new QTreeWidgetItem(parentTreeItem);
				SubtractMath->setText(0, tr("Subtract"));
				SubtractMath->setCheckState(0, Qt::Unchecked);
				SubtractMath->setChildIndicatorPolicy(QTreeWidgetItem::DontShowIndicatorWhenChildless);
				subtext = "Subtract" + parentCategory;
				populateTreeWidgetChildren(SubtractMath, attributesPerCategory, subtext);

				QTreeWidgetItem *MultiplyMath = new QTreeWidgetItem(parentTreeItem);
				MultiplyMath->setText(0, tr("Multiply"));
				MultiplyMath->setCheckState(0, Qt::Unchecked);
				MultiplyMath->setChildIndicatorPolicy(QTreeWidgetItem::DontShowIndicatorWhenChildless);
				subtext = "Multiply" + parentCategory;
				populateTreeWidgetChildren(MultiplyMath, attributesPerCategory, subtext);

				QTreeWidgetItem *DivideMath = new QTreeWidgetItem(parentTreeItem);
				DivideMath->setText(0, tr("Divide"));
				DivideMath->setCheckState(0, Qt::Unchecked);
				DivideMath->setChildIndicatorPolicy(QTreeWidgetItem::DontShowIndicatorWhenChildless);
				subtext = "Divide" + parentCategory;
				populateTreeWidgetChildren(DivideMath, attributesPerCategory, subtext);

				// is any calculator attribute present?
				int sumOfChildren = AddMath->childCount() + SubtractMath->childCount() + MultiplyMath->childCount() + DivideMath->childCount();

				if (AddMath->childCount() == 0)
					delete AddMath;
				if (SubtractMath->childCount() == 0)
					delete SubtractMath;
				if (MultiplyMath->childCount() == 0)
					delete MultiplyMath;
				if (DivideMath->childCount() == 0)
					delete DivideMath;

				if (sumOfChildren == 0)
					delete calculatorTreeWidgetItem;

			}
			else {
				QTreeWidgetItem *sum = new QTreeWidgetItem(parentTreeItem);
				sum->setText(0, tr("Sum of pixel intensity"));
				sum->setCheckState(0, Qt::Unchecked);
				sum->setChildIndicatorPolicy(QTreeWidgetItem::DontShowIndicatorWhenChildless);
				QString subtext = "Sum" + parentCategory;
				populateTreeWidgetChildren(sum, attributesPerCategory, subtext);

				QTreeWidgetItem *mean = new QTreeWidgetItem(parentTreeItem);
				mean->setText(0, tr("Average of pixel intensity"));
				mean->setCheckState(0, Qt::Unchecked);
				mean->setChildIndicatorPolicy(QTreeWidgetItem::DontShowIndicatorWhenChildless);
				subtext = "Mean" + parentCategory;
				populateTreeWidgetChildren(mean, attributesPerCategory, subtext);

				QTreeWidgetItem *std = new QTreeWidgetItem(parentTreeItem);
				std->setText(0, tr("Standard deviation of pixel intensity"));
				std->setCheckState(0, Qt::Unchecked);
				std->setChildIndicatorPolicy(QTreeWidgetItem::DontShowIndicatorWhenChildless);
				subtext = "StdDev" + parentCategory;
				populateTreeWidgetChildren(std, attributesPerCategory, subtext);

				QTreeWidgetItem *cv = new QTreeWidgetItem(parentTreeItem);
				cv->setText(0, tr("Coefficient of variation of pixel intensity"));
				cv->setCheckState(0, Qt::Unchecked);
				cv->setChildIndicatorPolicy(QTreeWidgetItem::DontShowIndicatorWhenChildless);
				subtext = "CV" + parentCategory;
				populateTreeWidgetChildren(cv, attributesPerCategory, subtext);

			}
		}
	}
	else {
		foreach(QString attributeName, attributes) {
			QTreeWidgetItem *newAttributeTreeItem = new QTreeWidgetItem(ui.twgAvailableAttributes);
			newAttributeTreeItem->setText(0, attributeName);
			newAttributeTreeItem->setText(1, "Attribute" );
			newAttributeTreeItem->setCheckState(0, Qt::Unchecked);
		}
	}

	QStringList headerLabels;
	headerLabels << "Select the parameter(s) to plot:";
	ui.twgAvailableAttributes->setHeaderLabels(headerLabels);

}

void AttributeDisplaySelection::populateTreeWidgetChildren (QTreeWidgetItem* _parentItem, QHash<QString, QVector<QString>> _attributesPerCategory, QString _categoryName)
{
	QVector<QString> relevantAttributes = _attributesPerCategory.value(_categoryName);
	foreach(QString attribute, relevantAttributes) {
		if (attribute.contains(_categoryName)) {
			QTreeWidgetItem *newAttributeTreeItem = new QTreeWidgetItem(_parentItem);
			newAttributeTreeItem->setText(0, attribute);
			newAttributeTreeItem->setText(1, "Attribute" );
			newAttributeTreeItem->setCheckState(0, Qt::Unchecked);
		}
	}
}

QVector<QString> AttributeDisplaySelection::getAttributeSelection()
{
	//the return data
	QVector<QString> names;

	QTreeWidgetItemIterator it(ui.twgAvailableAttributes);
	while (*it) {
		if ((*it)->checkState(0) == Qt::Checked) {
			if ((*it)->text(1) == "Attribute")
				names.append((*it)->text(0));
		}
		++it;
	}

	return names;
}

//SLOT
void AttributeDisplaySelection::plotSelectedAttributes()
{
	QVector<QString> selectedAttributes;

	// send the quantification chunk name
	QString	chunkName = ui.cbxQuantificationChunks->currentText();

	//find the selected attributes
	selectedAttributes = this->getAttributeSelection();

	//send data to the parent widget
	emit successfulAttributeSelection(selectedAttributes, chunkName);

	AttributeDisplaySelection::hide();

}

void AttributeDisplaySelection::updateChecks(QTreeWidgetItem* item, int column)
{
	// Checkstate is stored on column 0
	if(column != 0)
		return;
	if (item->checkState(0) == Qt::Checked)
		item->setExpanded(true);

	recursiveChecks(item);
}

void AttributeDisplaySelection::recursiveChecks(QTreeWidgetItem* parent)
{
	Qt::CheckState checkState = parent->checkState(0);
	for(int i = 0; i < parent->childCount(); ++i)
	{ 
		parent->child(i)->setCheckState(0, checkState);
		recursiveChecks(parent->child(i));
	}

}

// SLOT
void AttributeDisplaySelection::uncheckAttribute(QString _quantChunk, QString _quantAttribute)
{
	
	QTreeWidgetItemIterator it(ui.twgAvailableAttributes);
	while (*it) {
		if ((*it)->checkState(0) == Qt::Checked && (*it)->text(0)==_quantAttribute) {
			(*it)->setCheckState(0, Qt::Unchecked);
		}
		++it;
	}
}

void AttributeDisplaySelection::updateView(QString _quantChunk)
{
	updateAttributeTreeView(_quantChunk);
}

void AttributeDisplaySelection::addNewParameterInChunk(QString _quantChunk, QString _newParameterName)
{
	QVector<QString> parametersInChunk = this->allAttributes.value(_quantChunk);
	parametersInChunk.append(_newParameterName);
	this->allAttributes.insert(_quantChunk, parametersInChunk);
}


//SLOT:
void AttributeDisplaySelection::cancel()
{
	AttributeDisplaySelection::hide();
}

