/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Stavroula Skylaki
 * @date 17.11.2015
 */


#ifndef ATTRIBUTEDISPLAYSELECTION_H
#define ATTRIBUTEDISPLAYSELECTION_H

#include <QDialog>
#include "ui_frmAttributeDisplaySelection.h"

class AttributeDisplaySelection : public QDialog
{
	Q_OBJECT

public:

	/**
	 * returns the static instance of this class; if it does not yet exist, it is created and initialized
	 * @return the (unique) CorrectionUISettings instance
	 */
	static AttributeDisplaySelection* getInst(QWidget *parent = 0);
	
	~AttributeDisplaySelection();

	void initializeData(QHash<QString, QVector<QString>> _chunkAttributes, QHash<QString, QString> _chunkSoftware, QHash<QString, QString> _chunkRemarks);

	//it's called when a new parameter has been added by user
	// and the parameter selection form needs to be updated
	void updateView(QString _quantChunk);

	// it adds the new parameter in the initialized map of this class
	void addNewParameterInChunk(QString _quantChunk, QString _newParameterName);

	public slots:
		void uncheckAttribute(QString _quantChunk, QString _quantAttribute);
		void cancel();			


signals:
	void successfulAttributeSelection(QVector<QString>&, QString);

private slots:
	void plotSelectedAttributes();
	void updateAttributeTreeView(const QString & _chunkName);
	void updateChecks(QTreeWidgetItem* item, int column);

private:	

	// the static global instance (unique in the complete program)
	static AttributeDisplaySelection *inst;

	// Private Constructors to prevent instatiation outside of getInst()
	AttributeDisplaySelection(QWidget *parent = 0);

	// Private Copy Constructor to prevent copying
	AttributeDisplaySelection(const AttributeDisplaySelection &);
	AttributeDisplaySelection& operator=(const AttributeDisplaySelection &);

	QVector<QString> getAttributeSelection();

	void populateTreeWidgetChildren  (QTreeWidgetItem* _parentItem, QHash<QString, QVector<QString>> _attributesPerCategory, QString _categoryName);

	void recursiveChecks(QTreeWidgetItem* parent);
	//@key the quantification chunk name
	//@value vector of quantification chunk col names
	QHash<QString, QVector<QString>> allAttributes;
	QHash<QString, QString> allChunkSoftware;
	QHash<QString, QString> allChunkRemarks;

	// The ui
	Ui::frmAttributeDisplaySelection ui;

public:

};


#endif // ATTRIBUTEDISPLAYSELECTION_H