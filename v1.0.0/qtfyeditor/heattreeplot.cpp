/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/****************************************************************************
**
** @ Laura Skylaki, 16.01.2016
**
**
****************************************************************************/


//PROJECT
#include "heattreeplot.h"
#include "qtfyattributetools.h"
#include "qtfydata/cellwrapper.h"
#include "qtfydata/attributeinfo.h"

//QT
#include <QColor>
#include <QPen>
#include <QGraphicsRectItem>
#include <QFileDialog>
#include <QCoreApplication>


HeattreePlot::HeattreePlot(QVector<QString> treeSetInformation, QString attribute, QString quantiChunk, QWidget* parent, QString _timeUnit):
	QGraphicsView(parent)

{
	timeUnit = _timeUnit;
	xFactor = getXFactor(); //default x axis time factor = hours
	timepointFound = false;

	//initialize data
	treeName = " ";
	cellName = " ";
	timepoint = -1;
	minCellHeight = 5;
	basicCellHeight = 5;
	minCellWidth=5;
	basicCellWidth=5;
	treeMaxX=0;

	attributeName = attribute;
	quantificationChunk = quantiChunk;

	if(treeSetInformation.size() > 0)
	{
		treeName = treeSetInformation[1];
		cellName = " ";
		if(treeSetInformation.size() > 2)
		{
			cellName = treeSetInformation[2];
			cNum = cellName.split("_")[1].toInt();

			if(treeSetInformation.size() > 3)
				timepoint = treeSetInformation[3].split("_")[1].toInt();
		}
	}

	this->mOptions = new PlottingOptions;

	// create the scene
	this->scene = new QGraphicsScene;
	this->setScene(this->scene);

}

HeattreePlot::~HeattreePlot()
{
}


void HeattreePlot::getHeatColorRange(QVector<ITrack*> _cellList)
{
	allValues.clear();
	foreach (ITrack* cell, _cellList) {
		QMap<int, QVariant> data = QTFyAttributeTools::getTimedValue(*cell, attributeName, quantificationChunk);
		
		foreach (QVariant aval, data.values()) {
			double dval = aval.toDouble();
			allValues.append(dval);
		}
	}

	int totalVals = allValues.size();
	if (totalVals == 0)
		return;

	qSort(allValues);
	
	double min = allValues.at(0);
	double max = allValues.at(totalVals-1);

}


void HeattreePlot::createPlot(ITrack* cell,int generations)
{	
	//initialize data
	maxGeneration = generations;

	int maxY = qPow(2, maxGeneration) * minCellHeight;

	if (maxY < 800)
		maxY = 800;

	maxCellY = maxY;
	minCellY = 0;

	int timeCounter = 0; // first plot chunk

	calculateCell(cell, minCellY, maxCellY, 1, 0);

	int legendX = treeMaxX + 20;

	plotLegend(legendX, 20, 40, 120);

}

void HeattreePlot::calculateCell(ITrack* _cell, int _minY, int _maxY, uint i, uint _timeCounter)
{

	//initialize data to be used
	QColor c;
	//set color
	c = this->mOptions->getCellColor(_cell);
	
	//hide branch
	if(c.alpha() == 0)
		return;

	int cellID = _cell->getTrackNumber();

	// iterate through all the timepoints of the cell
	int cellEndTime = plotCellConcatenate(_cell, _minY, _maxY, c, _timeCounter);

	i++;

	int yMiddle = (_maxY-_minY) / 2;
	int parentCellEndtime = cellEndTime;

	//does the cell have a child1?
	if(_cell->getChild1())
	{
		//Set default curve (Child1 always up)																						   
		//set y
		// we identify the number of partitions for this generation
		// as the number of children in the generation + 1
		// as the branches in the current generation need to fall in the middle
		// of two such partitions
		// int requiredPartitions = qPow(2, cellGeneration) + 1;

		calculateCell(_cell->getChild1(), _minY, _minY + yMiddle -1 , i, parentCellEndtime);
		i++;

		//test
		if(yMiddle > maxCellY)
			maxCellY = yMiddle;
	}

	//does the cell have a child2?
	if(_cell->getChild2())
	{					
		calculateCell(_cell->getChild2(), _minY + yMiddle, _maxY, i, parentCellEndtime);
		
		i++;

		//test
		if(yMiddle < minCellY)
			minCellY =yMiddle;
	}

}

// version that depends on the exact timepoint
void HeattreePlot::plotCell(ITrack* _cell, int _minY, int _maxY , QColor _c)
{
	QMap<int, ITrackPoint*> cellTPs = _cell->getTrackPointsRange();
	int counter = 0;
	foreach (int tpID, cellTPs.keys()) {
		// new x for this timepoint 
		int timepointLocation = _cell->getAbsoluteSecondsForTP(tpID)*xFactor;
		int nextTimepointLocation;
		int nextTimepointID;
		counter++;
		if (counter < cellTPs.size()) {
			nextTimepointID = cellTPs.keys().at(counter);
			nextTimepointLocation = _cell->getAbsoluteSecondsForTP(nextTimepointID)*xFactor;
		} 
		else {
			nextTimepointID = tpID;
			nextTimepointLocation = timepointLocation;
		}

		// by axis
		// createTimepointCurve(timepointLocation, nextTimepointLocation, _minY, _maxY, _c);

		// by timepoint number
		createTimepointRect(tpID, nextTimepointID, _minY, _maxY, _c);		
	}
}

int HeattreePlot::plotCellConcatenate(ITrack* _cell, int _minY, int _maxY , QColor _c, uint _timeCounter)
{
	int startTime = _timeCounter;
	int startBorder = _timeCounter;
	QMap<int, QVariant> data = QTFyAttributeTools::getTimedValue(*_cell, attributeName, quantificationChunk);

	foreach (int tpID, data.keys()) {
		// by timepoint number
		_timeCounter++;
		
		// createTimepointRect(startTime, _timeCounter, _minY, _maxY, _c);	

		// find colour
		double currentVal = data.value(tpID).toDouble();
		float percent = getValuePercentile(currentVal, allValues);
		QColor currColor = getColorForTimepoint(percent);
		createTimepointRect(startTime, _timeCounter, _minY, _maxY, currColor);	
		startTime++;
	}

	int endBorder = _timeCounter;
	createCellBorder(startBorder, endBorder, _minY, _maxY, Qt::black);

	return _timeCounter;
}


void HeattreePlot::createTimepointRect(int _xTimepoint, int _nextTimepoint, int _yMin, int _yMax, QColor _c)
{
	// rectangular dimensions
	qreal x = _xTimepoint * basicCellWidth;
	qreal width = (_nextTimepoint - _xTimepoint) * basicCellWidth;
	qreal y = _yMin;
	qreal height = _yMax - _yMin;


	// create rectangular shape
	QGraphicsRectItem* item = new QGraphicsRectItem(x, y, width, height);
	item->setPen(QPen(_c, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	item->setBrush(QBrush(_c));  
	scene->addItem(item);

	if ( (x+width) > treeMaxX)
		treeMaxX = x+width;

}

void HeattreePlot::createCellBorder(int _startTimepoint, int _endTimepoint, int _yMin, int _yMax, QColor _borderColor)
{
	// rectangular dimensions
	qreal x = _startTimepoint * basicCellWidth;
	qreal width = (_endTimepoint - _startTimepoint) * basicCellWidth;
	qreal y = _yMin;
	qreal height = _yMax - _yMin;


	// create rectangular shape
	QGraphicsRectItem* item = new QGraphicsRectItem(x, y, width, height);
	item->setPen(QPen(_borderColor, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	item->setBrush(Qt::NoBrush);
	scene->addItem(item);

}

float HeattreePlot::getValuePercentile(double _aVal, QList<double> allValues)
{
	float index = allValues.indexOf(_aVal);
	float total = allValues.size();
	float percent = index / total;
	return percent;
}

QColor HeattreePlot::getColorForTimepoint (float _percent)
{
	if (_percent<0 || _percent>1) 
		return QColor(Qt::gray);

	int r = 255, g;
	g = 255 - (int)(255*_percent);

	// red to yellow to green
	//if (_percent<0.5) {
	//	r=255;
	//	g = (int)(255*_percent/0.5);  //closer to 0.5, closer to yellow (255,255,0)
	//}
	//else {
	//	g=255;
	//	r = 255 - (int)(255*(_percent-0.5)/0.5); //closer to 1.0, closer to green (0,255,0)
	//}

	return QColor(r, g, 0);
}

void HeattreePlot::plotLegend(int _x, int _y, int _width, int _height)
{
	// create the gradient - for now yellow to red 
	QLinearGradient grad(_x, _y, _x + _width, _y + _height);

	grad.setColorAt(0.1, Qt::yellow);
	grad.setColorAt(0.9, Qt::red);

	// create rectangular shape with heat gradient
	QGraphicsRectItem* item = new QGraphicsRectItem(_x, _y, _width, _height);
	item->setPen(QPen(Qt::black, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	item->setBrush(grad);
	scene->addItem(item);

	float thresholdUp = this->allValues.at(allValues.size()-1);
	float thresholdLow = this->allValues.at(0);

 	QGraphicsTextItem* thresholdUpLabel = new QGraphicsTextItem(QString::number(thresholdUp, 'f', 2)); 
	thresholdUpLabel->setPos(_x+_width+2, _y + _height - 15);
	QGraphicsTextItem* thresholdDownLabel = new QGraphicsTextItem(QString::number(thresholdLow, 'f', 2));
	thresholdDownLabel->setPos(_x+_width+2, _y - 5);

	scene->addItem(thresholdUpLabel);
	scene->addItem(thresholdDownLabel);
}

QColor HeattreePlot::getColor(int nr)
{
	QVector<QString> namesOfStandardColors = AttributeInfo::getNamesOfStandardColors();

	if(nr > 19)
		nr = nr % 19;

	return QColor(namesOfStandardColors[nr]);
}


void HeattreePlot::setXFactor(float xf)
{
	xFactor = xf;
}

void HeattreePlot::setPlottingOptions(PlottingOptions* _opt)
{
	this->mOptions->setPreferences(_opt->getPreferences());
}

PlottingOptions* HeattreePlot::getPlottingOptions()
{
	return this->mOptions;
}

float HeattreePlot::getXFactor()
{
	float factor;

	if(timeUnit == "Hours")
		factor = 1.0/3600.0;
	else if(timeUnit == "Minutes")
		factor = 1.0/60.0;
	else 
		factor = 1.0;

	return factor;
}

void HeattreePlot::exportToImage ()
{
	QString fileName= QFileDialog::getSaveFileName(this, "Save image", QCoreApplication::applicationDirPath(), "PNG (*.png);;JPEG (*.JPEG)" );
	if (!fileName.isNull())	{
		/*QPixmap pixMap = QPixmap::grabWidget(this);
		pixMap.save(fileName);*/
		int width = scene->width();
		int height = scene->height();
		QImage img(width, height, QImage::Format_ARGB32_Premultiplied);
		QPainter p(&img);
		scene->render(&p);
		p.end();
		img.save(fileName);
	}
}
