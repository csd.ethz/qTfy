/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PLOTPICKERHELP_H
#define PLOTPICKERHELP_H

/****************************************************************************
**
** @ Laura Skylaki, 07.08.2014
**
**
****************************************************************************/

// PROJECT
#include "qwt/qwt_plot_curve.h"
#include "qwt/qwt_plot.h"

// QT



class PlotPickerHelp
{

public:
	PlotPickerHelp();
	~PlotPickerHelp();

	// finds the closest timepoint of a curve to the pnt by actual values of the plot
	// return the closest timepoint (index of timepoint in the curve data)
	// also if dist!=0 returns the distance of the distance from the closest timepoint
	int findClosestCurvePointByValue(const QwtPlotCurve* _curve, const QwtDoublePoint &_pnt, double* dist = 0);

	// finds the closest timepoint of a curve to the pnt by pixel coordinates on the plot
	// return the closest timepoint (index of timepoint in the curve data)
	// also if dist!=0 returns the distance of the distance from the closest timepoint
	int findClosestCurvePointByPixelDistance(const QwtPlot* _plot, const QwtPlotCurve* _curve, const QwtDoublePoint &_pnt, double* dist = 0);


	// transform the point of a curve to pixel coordinates of the plot canvas
	QPoint transformPointToPixelCoords(const QwtPlot* _plot, const QwtDoublePoint &_pnt) const;

	QPoint invTransform(const QwtPlot* _plot, const QPoint &pos) const;

};

#endif // PLOTPICKERHELP_H