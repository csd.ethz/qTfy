/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Stavroula Skylaki
 * @date 12.04.2016
 */


// PROJECT
#include "parametercalculator.h"
#include "qtfygui\qtfyhelp.h"

// QT
#include <QStandardItemModel>
#include <QListView>


ParameterCalculator::ParameterCalculator(QWidget *parent)
{
	// Setup ui
	ui.setupUi(this);
	connect ( ui.pbtCancel, SIGNAL (clicked()), this, SLOT (cancel()));
	connect ( ui.pbtAddNewParameter, SIGNAL(clicked()), this, SLOT(getParameterSelection()));

	// initialize basic operations
	QStandardItemModel* cbxBasicOperationsModel = new  QStandardItemModel(4, 3);

	QStandardItem* col0 = new QStandardItem("+ Add");
	col0->setToolTip("New parameter from sum of parameters 1 and 2");
	cbxBasicOperationsModel->setItem(0, 0, col0);

	QStandardItem* col1 = new QStandardItem("- Subtract");
	col1->setToolTip("New parameter from subtraction of parameter 2 from 1");
	cbxBasicOperationsModel->setItem(1, 0, col1);

	QStandardItem* col2 = new QStandardItem("* Multiply");
	col2->setToolTip("New parameter from multiplication of parameters 1 and 2");
	cbxBasicOperationsModel->setItem(2, 0, col2);

	QStandardItem* col3 = new QStandardItem("/ Divide");
	col3->setToolTip("New parameter from division of parameters 1 by 2");
	cbxBasicOperationsModel->setItem(3, 0, col3);
		

	ui.cbxArithmeticOperations->setModel(cbxBasicOperationsModel);

	QListView* myCbxView = new QListView(this);
	ui.cbxArithmeticOperations->setView(myCbxView);

	// Enable maximize and minimize buttons
	this->setWindowFlags(Qt::Window);

	connect(ui.pbtHelpParameterCalculator, SIGNAL(clicked()), this, SLOT(showHelpCalculator()));

}


ParameterCalculator::~ParameterCalculator()
{
}


void ParameterCalculator::initializeData(QHash<QString, QVector<QString>> _chunkAttributes, QHash<QString, QString> _chunkSoftware, QHash<QString, QString> _chunkRemarks)
{
	this->allAttributes = _chunkAttributes;
	this->allChunkSoftware = _chunkSoftware;
	this->allChunkRemarks = _chunkRemarks;

	int rowCounter = 0;
	QStandardItemModel* cbxParametersModel = new  QStandardItemModel(rowCounter, 2);
	QVector<QString> totalAttributeNames;

	foreach (QString chunkname, _chunkAttributes.keys()) {

		QVector<QString> chunkAttributes = _chunkAttributes.value(chunkname);
		foreach (QString attributeName, chunkAttributes) {
			if (!attributeName.contains("CV") && !attributeName.contains("StdDev")) {
				QStandardItem* col0 = new QStandardItem(attributeName);
				QStandardItem* col1 = new QStandardItem(chunkname);
				col0->setToolTip(attributeName);

				QList<QStandardItem*> list = QList<QStandardItem*>() << col0 << col1;
				cbxParametersModel->insertRow(rowCounter, list);
				rowCounter++;
			}
		}
	}


	ui.cbxParameterList1->setModel(cbxParametersModel);
	ui.cbxParameterList2->setModel(cbxParametersModel);

	QListView* myParametersView1 = new QListView(this);
	ui.cbxParameterList1->setView(myParametersView1);

	QListView* myParametersView2 = new QListView(this);
	ui.cbxParameterList2->setView(myParametersView2);

}

//SLOT
void ParameterCalculator::getParameterSelection()
{
	QString parameterName1 = ui.cbxParameterList1->currentText();
	QString parameterName2 = ui.cbxParameterList2->currentText();

	int index1 = ui.cbxParameterList1->currentIndex();
	int index2 = ui.cbxParameterList2->currentIndex();

	QModelIndex mIndex1 = ui.cbxParameterList1->model()->index(index1, 1);
	QModelIndex mIndex2 = ui.cbxParameterList2->model()->index(index2, 1);

	QString chunkName1 = ui.cbxParameterList1->model()->data(mIndex1).toString();
	QString chunkName2 = ui.cbxParameterList2->model()->data(mIndex2).toString();

	QString operationName = ui.cbxArithmeticOperations->currentText();

	QString newParameterName = createDefaultNewParameterName(parameterName1, chunkName1, parameterName2, chunkName2, operationName);

	emit successfullParameterSelection(parameterName1, chunkName1, parameterName2, chunkName2, operationName, newParameterName);

	ParameterCalculator::close();
}

QString ParameterCalculator::createDefaultNewParameterName (QString _parameterName1, QString _chunkName1, QString _parameterName2, QString _chunkName2, QString _operationName)
{
	QString paramName;
	QString operation;

	if (_operationName.contains("Add"))
		operation = "Add";
	else if (_operationName.contains("Subtract"))
		operation = "Subtract";
	else if (_operationName.contains("Multiply"))
		operation = "Multiply";
	else if (_operationName.contains("Divide"))
		operation = "Divide";

	paramName = operation + "Calculator" +  _parameterName1 + "_" + _parameterName2;

	return paramName;
}

// SLOT
void ParameterCalculator::showHelpCalculator()
{
	// create help text
	QString txt = "<p><h2>Parameter Calculator</h2> </p>"  
		"<p>Use the \"Parameter Calculator\" module to generate derived parameters based on mathematical operations between existing parameters.</p>"

		"<p><img src=\":hint.png\"> To plot the new parameter go to the form \"Add parameter\". The new parameter can be found under the category \"Calculator\". " 
		"The new parameter is also exported in the same quantification file as the first existing parameter selected by the user (Parameter 1). </p>";

	QMap<QString, QImage> imgResources;
	QImage img(":/qtfyres/qtfyres/hint.png");
	imgResources.insert("hint.png", img);

	// show help form
	QString windowTitle = "QTFy help: Parameter Calculator";

	QTFyHelp* help = new QTFyHelp(this, windowTitle);
	help->setHTMLHelp(txt, imgResources);
	help->show();
}


//SLOT:
void ParameterCalculator::cancel()
{
	ParameterCalculator::close();
}

