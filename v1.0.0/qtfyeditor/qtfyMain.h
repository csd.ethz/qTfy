/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef QTFYMAIN_H
#define QTFYMAIN_H

//project
#include "ui_frmQtfyEditor.h"
#include "treemodel.h"
#include "treeoverviewitemview.h"
#include "qtfybackend/itrack.h"
#include "treeplot.h"
#include "attributeplot.h"
#include "mymdisubwindow.h" 
#include "outliermanager.h"
#include "plottingoptions.h"
#include "qtfysegeditor/correctionuisettings.h"
#include "qtfysegeditor/SegEditorMain.h"
#include "qwt/qwt_plot_magnifier.h"
#include "qwt/qwt_plot_panner.h"
#include "qwt/qwt_plot_zoomer.h"

//QT
#include <QtGui/QMainWindow>
#include <QPainter>
#include <QFrame>
#include <QMdiSubWindow>
#include <QMap>
#include <QProgressBar>

//open cv
#include <opencv2/core/core.hpp>

//forward declarations
class TreeOverviewItemView;
class TreeModel;

class QtfyMain : public QMainWindow
{
	Q_OBJECT

public:

	QtfyMain(QWidget *parent = 0);

	~QtfyMain();

	//sets the path to the experiment folder
	void setExperimentsFilesFolder (QString path);

	//initialize the data
	void initializeData(QVector<QSharedPointer<ITree>> _trees);

	//calculates timepoints per cell
	void setTimepointsPerCell (QMap<QString, QMap<ITrack*, QVector<int> > > _timepointsPerCell); 

	// sets the cells per tree
	void setCellsPerTree(QMap< QString, QMap< QString, ITrack* > > _cellsPerTree);

	// sets the cells per timepoint
	void setCellsPerTimepoint(QMap< QString, QMap< int, QVector<ITrack*> > > _cellsPerTimepoint);

	/*
	*displays tree the user selected from the tree data view
	*/
	void displaySelectedTree(bool _widthChange = false);

	void initializeViewsSignals();

	/*
	creates the mdisubwindows for the quantified attributes
	*/
	void SetUpInterface();

	//displays the quantification plot with the attribute the user added
	void displayAttributePlot(QString _attrName, QString _attrChunkName = "", QTFyPlot* _previousPlot = 0, myMdiSubWindow* _previousSubwindow = 0);

	//setters
	void setSmoothingValue(int value);

	void setDetectionValue(int value);

	//void createOutliers(QwtPlot* myPlot, QPolygonF outliers);

	QPoint transformPointToPixelCoords(const QwtPlot* _plot, const QPointF &_pnt) const;

	void tileVerticallySubwindows();

	
	//set if the experiment has stagger wavelengths or not
	void hasStaggerWavelengths(bool _value);

public slots:
		/************************************************************************/
		/* updates the selected index of the treeview                           */
		/************************************************************************/
		void updateSelectedIndex (const QModelIndex& _selectedIndex);

		
		void updateSelectedIndexStagger(const QModelIndex& _selectedIndex, QString _chunk);

		// updates the message log text box
		void updateMessageLogText (QString _msg, QColor _color);

		// updates the progress bar
		void updateProgressBar ();

		// updates the maximum value of the progress bar
		// based on position count
		void setProgressBarMax (int _value);

		// exporting finished so allow re-exporting
		void exportFinished();

private slots:

		// tree model view right click pop up menu
		void slotRtClickTreeOverview(const QPoint& pnt); 

		// context menu for the plots
		void plotRtClickContextMenu(const QPoint& pnt);
		// context menu for tree plot
		void treePlotRtClickContextMenu(const QPoint& pnt);
		//void plotRtClickContextMenu(const QwtDoublePoint& pnt);

		// context menu for y axis
		void plotRtClickYAxis(const QPoint& pnt);

		//context menu for x axis
		void plotRtClickXAxis(const QPoint& pnt);

		//set y axis to global scale
		void setYAxisToGlobalScale();

		//set y axis to local scale
		void setYAxisToLocalScale();

		//set x axis scale manually
		 void setXAxisScale();

		//set y axis scale manually
		 void setYAxisScale();

		 //reset axis scales
		 void resetAxis();

		// add an attribute to plot
		//void addPlotAttribute();
		 void addPlotAttribute(QVector<QString>& _attributes, QString _chunkName);
		
		// opens the segmentation editor 
		void openSegmentationEditor();

		// opens the parameter calculator 
		void openParameterCalculator();

		// updates all the mdisubwindows based on the index selected on the tree data view
		void updateAllViewers(const QModelIndex& index);

		// when a point is selected on the plot
		void plotPointSelected(const QwtDoublePoint &_pnt);

		void outlierSelected(bool checkState);

		void smoothLinesSelected(bool checkState);

		void updateSmoothingThres(int sValue);

		void updateDetectionThres(int dValue);

		void showPointsSelected(bool checkState);

		void autoAlignSelected();

		void closeSubwindow( myMdiSubWindow* _subwindow);

		void deactivateOutliersSelected();

		void applyToAll(ITree* _selectedTree);
		
		void exportPlotImages();
		
		void exportMessageLog();

		void exportHeattreePlot();

		void changeColor();  

		void hideBranch();

		void showBranch();

		void showAllCells();

		void setSelectedAttributes(QVector<QString>& _attributes);

		void openAttributeSelectionDialog();

		// opens the form for setting up the correction interface
		void openCorrectionUISettings();

		// exit qtfy application
		void cancel();

		// saves the quantified trees
		void exportTrees();

		// updates the message log dock
		void updateMsgLog(QString _msg, QColor _color);

		// updates a cell that has been corrected in the segmentation editor
		void updateCell(ITrack* cell);

		// resets all timepoints to active.
		void setAllTimepointsActive();

		//left click cell from tree plot
		void plotPointSelectedTreePlot(const QwtDoublePoint &_pnt);

		//set the x axis time unit
		void setXFactor();

		// the help 
		void openQTFyHelp();

		// show start screen with About info
		void showStartScreen();

		// shows the changelog
		void showChangeLog();

		//depending the check state show/hide treeplot
		void checkTreePlot(bool checkState);

		//restore the default colors of the scheme
		void restoreDefaultColors();

		// the help form for the outliers detection menu
		void showHelpOutliers();

		void editLineWidthAttributePlot();

		void editLineWidthTreePlot();

		
		void updateTreeModelRequest(QString _detectionWavelength);

		// creates a new derived parameter as designed in the parameter calculator
		void createDerivedParameter(QString _parameterName1, QString _chunkName1, QString _parameterName2, QString _chunkName2, QString _operationName, QString _newParameterName);

signals:
	void treeviewIndexChanged(const QModelIndex &_selectedIndex);

	
	void treeviewIndexChangedStagger(const QModelIndex &_selectedIndex, const QString _chunkName);

	void attributeRemoved(QString _chunkName, QString _attributeName);

	void activationStatusChanged();

protected:
	void closeEvent( QCloseEvent* event );

private:

	//private functions
	//-------------------
	ITrack* findSelectedCell();

	void createActions();

	void createShortcuts();

	/** finds the index of the model that has the specified cell id and corresponds to the timepoint at row _timepointRow
	 *  i.e. fist timepoint is at row 0 
	 *	currentIndex is the index that is currently selected at the tree view
	 *	returns the index that corresponds to the experiment > tree > cell > timepoint specified
	 **/	
	QModelIndex findIndexOfTimepoint (const QModelIndex &_currentIndex, QString _cellID, int _timepointRow);

	QModelIndex findClickedIndexFromPlot (const QwtPlot* myPlot, const QwtDoublePoint &_pnt, bool _showTimepoint = false);

	//method to find the min and max points of the treeset
	QPair<double, double> findMinMax(QString _attrName, QString _quantChunkName);

	//find the selected cell for the tree plot
	ITrack* findCellSelectedFromTreePlot(const QwtPlot* myPlot, const QwtDoublePoint &_pnt);

	// the children of the cell
	QVector<ITrack*> findChildren(ITrack* _cell);

	//plot the timepoint line
	void plotTimepoint(int _timepoint);

	//bold cell curve
	void highlightCell(ITrack* _cell);

	//test
	void synchronizePlots();

	//test 
	void updatePlotAxis();

	//get the time factor
	float getTimeFactor();

	//replots all the attribute plots 
	void updatePlots();

	//test function to refresh attribute plots
	void refreshAttributePlots();

	 
	void setWindowFocus(QString _currTitle);

	//boolean
	//--------------------

	//has the user selected outlier detection?
	bool OutlierDetection;

	//has the user selected smooth lines?
	bool smoothLines;

	bool showOriginalPoints;

	bool isAutoAlign;

	bool deactivateOutliers;

	bool applyToAllSelected;

	bool yAxisToLocalScale;

	bool manualXAxisSettings;

	bool manualYAxisSettings;

	bool mustReplot;

	bool restoreColors;

	bool closingPending;

	
	bool staggerWls;

	
	QString pathToExperiments;

	
	int hidenSubwindowsCounter;

	//the outlier detection threshold
	int dValue;

	//the smoothing threshold
	int sValue;

	double minX, maxX, minY, maxY;


	// the collection of timepoints per cell
	QMap<QString, QMap<ITrack*, QVector<int> > > timepointsPerCell;

	// the collection of cells per tree
	QMap< QString, QMap< QString, ITrack* > > cellsPerTree;

	// the collection of cells present at each timepoint of a tree
	QMap< QString, QMap<int, QVector<ITrack*> > > cellsPerTimepoint;

	//key: attribute name, value: mdiSubwindow
	QMap<QString, myMdiSubWindow*> mSubwindows;

	//QVector<QString> addedAttributes;
	//key: attribute name, value: quantification chunk name
	QMap<QString, QString> addedAttributes;

	
	QMap<QString, TreeModel*> m_TreeModels;

	// store min-max y-axis for each attribute (maybe same attribute from different chunks) QString -> subwindow name
	//always for QPair, first = min, second = max
	QHash<QString, QPair<double, double>> globalMinMaxMap;
	QHash<QString, QPair<double, double>> yManualMinMaxMap;
	// QHash<QString, QPair<double, double>> localMinMaxMap;

	//QVector
	//-------------------

	QVector<QSharedPointer<ITree>> trees;
	QVector<ITrack*> tracks;

	//vector with all the treeset information: [0]->experiment name, [1]->tree name, [2]->cell name, [3]->timepoint
	QVector<QString> treeSetInformation;

	//other
	//--------------------

	//the data tree view
	TreeOverviewItemView *m_tvwDataTreeOverview;

	//the tree data model
	TreeModel *m_TreeDataModel;


	//a hash that all the activation information of every cell of the treeset is stored
	QHash<ITree*, QHash<ITrack*, QPolygonF>> cellsActivationStatus;

	//the outlier manager
	OutlierManager* mOutlierManager;

	//test
	ITrack* selectedCell;

	//plotting options
	PlottingOptions* mPlottingOptions;

	//the attributes
	QList<QString> selectedAttributes;

	int alignYaxis;
	double extent;
	double yAxisExtent;
	int yAxisLength;

	QSize size;

	// is export of tree quantification done?
	bool csvExportDone;

	// is the export of the csv files still running?
	bool csvExportRunning;

	// the progress bar appearing at the bottom status bar
	QProgressBar *progressBar;

	QString currentTreeInPlots;

	//the time unit of the plots (hours/mins/secs), default unit is hour
	QString mTimeUnit;

	QString currSubwindowTitle;

	QString lastSelectedDirectory;

	// the available attributes
	QHash<QString, QVector<QString>> chunkAttributes;
	QHash<QString, QString> chunkSoftware;
	QHash<QString, QString> chunkRemarks;

	 
	double defaultXMax;

	//the ui
	Ui::QtfyEditor ui;

};
#endif //QTFYMAIN_H
