/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Laura Skylaki
 * @date 17/04/2013
 *
 * This class provides functions to list trees, cells and timepoints
 * in order to create the tree structure of the tree data Overview
 */


#ifndef TREEDATAPARSER_H
#define TREEDATAPARSER_H

// PROJECT
#include "treemodel.h"

// QT
#include <QStringList>
#include <QDir>
#include <QFileSystemModel>


class ITrack;
class ITree;

class TreeDataParser : public QObject
{
    Q_OBJECT

public:

    TreeDataParser(TreeModel* ExperimentModel, QObject *parent = 0);

	/**
     * Function based on QDir::entrylist, used in case of errors
     * @param _path the path to search in
     * @param _filters file extensions to use as filter
	 * @return a collection with all cells (itracks) that are present per timepoint
     * the result can be obtained by getModel()
     */
     QMap< QString, QMap<int, QVector<ITrack*> > > CreateExperimentModel(const QVector<QSharedPointer<ITree>> _trees, const QVector<ITrack*> _tracks, QVector<int> _regardedGenerations, QString pathToExperiments);


    /**
     * @brief getModel
     * @return the TreeModel that will be used to populate the experiment overview QTreeView
     */
    TreeModel getModel() const;


public slots:
    /**
     * @brief starts the worker
     */
    void process();

    /**
     * @brief stops the worker
     */
    void stop();

signals:

    void finished();

    // sends updates to the MainSegmentation progress bar about the progress on positions being processed
    void positionCompleted(QString);

    // the experiment tree model has been constructed
    //void modelReady(TreeModel*);

    // void error();

private:

    /**
     * Function based on QDir::entrylist, used in case of errors
     * @param _path the path to search in
     * @param _filters file extensions to use as filter
     * @return UNSORTED list of filenames without path that have been found
     */
    QStringList listFiles(const QString &path, const QStringList &filters);


    /**
     * Checks if the given file which is of the right file type i.e. an image
     * also complies with the experiment filename setup i.e. i.e. "130824LS7_pXXXX_tXXXXX_zXXX_wXX.png"
     * @param filename the filename to check for validity
     * @return true if the filename format complies
     */
    bool CheckFilename (const QString& filename);

    /**
     * Method to create ExperimentOverview tree structure
     * @param dir the directory to look for the images i.e. "C:\someFolder"
     */
    void initializePictures(const QString &dir);

    // the path to the experiment files
    QString dir;

    // boolean that determines that the worker has been requested to stop
    volatile bool stopped;


    // the FileSystemModel
    TreeModel *ExperimentModel;

};

#endif // TREEDATAPARSER_H
