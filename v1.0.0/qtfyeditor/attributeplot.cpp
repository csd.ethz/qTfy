/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/****************************************************************************
**
** @ Eleni Skylaki, 29.07.2014
**
**
****************************************************************************/

// PROJECT
#include "attributeplot.h"
#include "qtfydata/attributeinfo.h"
#include "qtfybackend/mathfunctions.h"
#include "qtfydata/experiment.h"
#include "qtfyattributetools.h"
#include "qtfybackend/tools.h"

// QT
#include "QDebug"

// ARGLIB
#include "alglib-3.8.2/stdafx.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "alglib-3.8.2/interpolation.h"

using namespace alglib;


AttributePlot::AttributePlot(){}

AttributePlot::~AttributePlot(){}

QwtPlotCurve* AttributePlot::createOriginalCurve(QString title, QColor c, QVector<QPointF> data, ITrack* _cell, int _width)
{
	QPen pen(c);
	pen.setWidth(_width);
	//pen.setWidth(2);

	QwtPlotCurve* curve = new QwtPlotCurve(title);

	curve->setPaintAttribute(QwtPlotCurve::PaintFiltered);
	curve->setPen(pen);
	curve->setData(data);

	curve->setStyle(QwtPlotCurve::Lines);
	return curve;

}

QVector <QVector<QPointF>> AttributePlot::getValues(QVector<ITrack*> _cells, const QString _attributeName, QString _quantChunkName, float _xFactor)
{
	// Collect the data
	QVector<QVector<QPointF>> result;

	foreach(ITrack* cell, _cells)
	{
		int number = cell->getTrackNumber();
		// Get the data
		QMap<int, QVariant> attrData = QTFyAttributeTools::getTimedValue(*cell, _attributeName, _quantChunkName);

		// Save data
		QVector<QPointF> data = convertToPoints(attrData, _xFactor);

		result.push_back(data);
	}

	return result;
}

QVector<QPointF> AttributePlot::convertToPoints(QMap<int, QVariant> _attrData, float _xFactor)
{
	QVector<QPointF> points;

	for(QMap<int, QVariant>::iterator i = _attrData.constBegin(); i != _attrData.constEnd(); ++i) {
		float yValue = i.value().toFloat();
			//check for nan or infinite values
		if(!Tools::isValidNumber(yValue))
			continue; //skip nan and +/-inf values
		else {
			float xValue = ((float)i.key())*_xFactor;
			points.append(QPointF(xValue, yValue));
		}			
	}
	return points;
}

QTFyPlot* AttributePlot::createPlot(QVector<ITrack*> _cellList, QString _attributeName, QString _quantChunkName, QTFyPlot* _plot, PlottingOptions* _options, QString _timeUnit, bool _smooth, bool _points, int _smoothValue)
{
	//get the time unit
	float xFactor;
	if(_timeUnit == "Hours")
		xFactor = 1.0/3600.0;
	else if(_timeUnit == "Minutes")
		xFactor = 1.0/60.0;
	else 
		xFactor = 1.0;

	
	QVector <QVector<QPointF>> points = getValues(_cellList,  _attributeName, _quantChunkName, xFactor); 

	QwtDoublePoint maxPoint = MathFunctions::getMaxValue(points);
	float maxPointY = maxPoint.ry();
	QwtDoublePoint minPoint = MathFunctions::getMinValue(points);
	float minPointY = minPoint.ry();

	//set up the axis
	_plot->setAxisScale(QwtPlot::yLeft, minPointY, maxPointY);
    _plot->setAxisTitle(QwtPlot::yLeft,  _attributeName );
	_plot->setAxisTitle(QwtPlot::xBottom, _timeUnit);
	
	if(!points.isEmpty()){
		if(!points.last().isEmpty()){
			double lastXP = points.last().last().rx();
			lastXP += 1;
			_plot->setAxisScale(QwtPlot::xBottom, 0, lastXP);
		}
	}

	//Get pen width
	int w;
	w = _options->getLineWidthForAttributePlot();
	//if(_cellList.isEmpty())
	//	w = 0;
	//else
	//	w = _options->getLineWidthForAttributePlot(_cellList[0]->getITree());

	 //For each cell create a new curve
	for (uint i = 0; i < points.size(); i++) {

		// Generate a color
		QColor c = _options->getCellColor(_cellList[i]);

		//test hide branch
		if(_options->getCellColor(_cellList[i]).alpha() == 0)
			continue;

		if(points[i].isEmpty())
			continue;

		// Create curve
		QwtPlotCurve* curve;
		//check if smooth curve
		if(_smooth)
			curve = createSmoothedCurve(points[i], "S" + QString::number(_cellList[i]->getTrackNumber()), _smoothValue, c, w);
		else
			curve = createOriginalCurve(QString::number(_cellList[i]->getTrackNumber()), c, points[i], _cellList[i], w);

		if(!curve)
			continue;
		//check if original points plot
		if(_points){
			QwtPlotCurve* pointCurve = plotOriginalPoints("P" + QString::number(_cellList[i]->getTrackNumber()), c, points[i], _cellList[i], w);
			pointCurve->attach(_plot);
		}

		curve->attach(_plot);
	}

	QPen gPen(Qt::lightGray);
	gPen.setStyle(Qt::DotLine);
	QwtPlotGrid* mGrid = new QwtPlotGrid();
	mGrid->setPen(gPen);
	mGrid->attach(_plot);
	_plot->setCanvasBackground(Qt::white);

	_plot->installPlotPicker(QwtPlot::xBottom, QwtPlot::yLeft);

	return _plot;
}

QwtPlotCurve* AttributePlot::createSmoothedCurve(QVector<QPointF> _originalPoints, QString _name, int _smoothValue, QColor _color, int _width)
{
	QwtPlotCurve* smoothedCurve;
	//check if valid smoothing value
	if(_smoothValue < 0)
		return smoothedCurve;

	//check if empty original points
	if(_originalPoints.isEmpty())
		return NULL;

	//check for nan or infinite values
	for(int i = 0; i < _originalPoints.size(); i++){
		double currentValue = _originalPoints[i].ry();
		if(!Tools::isValidNumber(currentValue))
			_originalPoints.remove(i); //skip nan and +/-inf values
	}
	//create smoothed curve
	smoothedCurve = new QwtPlotCurve(_name);
	smoothedCurve->setStyle(QwtPlotCurve::Lines);
	smoothedCurve->setPaintAttribute(QwtPlotCurve::PaintFiltered);
	QPen pen(_color);
	//pen.setWidth(2);
	pen.setWidth(_width);
	smoothedCurve->setPen(pen);

	QVector<QPointF> smoothedPoints;
	QPointF tmpPoint;

	ae_int_t info;
	double v;
	spline1dinterpolant s;
	spline1dfitreport rep;
	double rho;


	real_1d_array x1;
	real_1d_array y1;

	int size = _originalPoints.size();

	if(size == 1){
		smoothedPoints.append(_originalPoints[0]);
	}else{

		double *xData = new double [size];
		double *yData = new double [size];
		int count = 0;
		foreach (QPointF pnt, _originalPoints) {
			xData[count] = pnt.x();
			yData[count] = pnt.y();
			count++;
		}

		x1.setcontent(size, xData);
		y1.setcontent(size, yData);

		rho = _smoothValue;
		spline1dfitpenalized(x1, y1, 50, rho, info, s, rep);

		for(int i = 0; i<size; i++)
		{
			v = spline1dcalc(s, xData[i]);
			tmpPoint.setX(xData[i]);
			tmpPoint.setY(v);

			smoothedPoints.append(tmpPoint);
		}

		smoothedCurve->setData(smoothedPoints);
		
		delete xData;
		delete yData;

	}
	return smoothedCurve;
}

QwtPlotCurve* AttributePlot::plotOriginalPoints(QString _curveName, QColor _color, QVector<QPointF> _points, ITrack* _cell, int _width)
{

	QwtPlotCurve* curve = new QwtPlotCurve(_curveName);
	int s = _width*3;
	QwtSymbol sym(QwtSymbol::Ellipse,QBrush(_color),QPen(_color),QSize(s,s));

	//shows the points in the curve
	curve->setPaintAttribute(QwtPlotCurve::PaintFiltered);
	curve->setData(_points);
	curve->setSymbol(sym);


	curve->setStyle(QwtPlotCurve::NoCurve);

	return curve;
}
