/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
//Eleni Skylaki 30.07.2015

//PROJECT
#include "plottingoptions.h"
#include "qtfydata/attributeinfo.h"


PlottingOptions::PlottingOptions()
{
	//initiliaze line widths
	this->mAttributePenWidth = 2;
	this->mTreePenWidth = 3;
}

PlottingOptions::~PlottingOptions()
{

}

void PlottingOptions::initializeOptions(ITree* _tree)
{
//	this->isChanged = false;
	PlotPreferences options;
	//default colors
	QVector<QString> namesOfStandardColors = AttributeInfo::getNamesOfStandardColors();

	//initialiaze cell colors
	ITrack* cell = _tree->getRootNode();

	int maxCellNum = _tree->getMaxTrackNumber();
	for(int i = 0; i<= maxCellNum; i++){

		if(_tree->getTrackByNumber(i) == 0)
			continue;
		else{
			int nr; 
			if( i >19)
				nr = i % 19;
			else 
				nr =i;

			QColor c = QColor(namesOfStandardColors[nr]);

			//add cell to tree preferences
			options.colorPerCell.insert(i, c);
		}

	}
	//initilize lines width
	//options.lineWidthAttribute = 2;
	//options.lineWidthTree = 3;

	mTreePref.insert(_tree, options);

}

void PlottingOptions::customizePlot(QwtPlot* _plot, ITree* _tree)
{
	
	QwtPlotItemList listOfPlotItems = _plot->itemList();

	for (QwtPlotItemList::iterator it = listOfPlotItems.begin(); it != listOfPlotItems.end(); ++it) { 

		// find the type of the plot item	
		int type = (*it)->rtti();
		//if curve
		if (type == QwtPlotItem::Rtti_PlotCurve) {
			QwtPlotCurve* currentCurve = (QwtPlotCurve*) (*it);
			QString cellNum = currentCurve->title().text();

			//options
			if(!this->isChanged){
				//default options

			}

		}
	}
}

void PlottingOptions::setCellColor(ITrack* _cell, QColor c)
{
	ITree* currentTree = _cell->getITree();
	PlotPreferences currentPref; 
	int cellNum = _cell->getTrackNumber();
	
	if(!this->mTreePref.isEmpty()){
		//where changes made to the cells of this tree before?
		if(this->mTreePref.contains(currentTree))
		{
			currentPref = this->mTreePref.value(currentTree);
			//where changes made to the specific cell before?
			if(currentPref.colorPerCell.contains(cellNum))
			{
				//update color
				if(currentPref.colorPerCell.remove(cellNum)){
					currentPref.colorPerCell.insert(cellNum, c);
					//qDebug()<<currentPref.colorPerCell.value(cellNum);
				}
				//this->mTreePref.value(currentTree) = currentPref;
				//return;
			}
			else
				currentPref.colorPerCell.insert(cellNum, c);
				//this->mTreePref.value(currentTree) = currentPref;
		

			//update preferences
			this->mTreePref.remove(currentTree);
			this->mTreePref.insert(currentTree, currentPref);

			return;
		}	
	}
	//create new tree preferences
	currentPref.colorPerCell.insert( cellNum, c);
	this->mTreePref.insert(currentTree, currentPref);

	return;
}

QColor PlottingOptions::getCellColor(ITrack* _cell)
{
	ITree* currentTree = _cell->getITree();
	PlotPreferences currentPref; 
	int cellNum = _cell->getTrackNumber();
	QColor c;
	
	//where changes made to the cells of this tree?
	if(this->mTreePref.contains(currentTree))
	{
		currentPref = mTreePref.value(currentTree);
		//where changes made to the specific cell?
		if(currentPref.colorPerCell.contains(cellNum))
		{
			c = currentPref.colorPerCell.value(cellNum);
			return c;
		}

	}

	return getColor(cellNum);
}

QColor PlottingOptions::getColor(int nr)
{
	QVector<QString> namesOfStandardColors = AttributeInfo::getNamesOfStandardColors();

	if( nr >19)
		nr = nr % 19;

	return QColor(namesOfStandardColors[nr]);
}

void PlottingOptions::addTreePreferences(ITree* _tree)
{
	//setCellColor does it so far
}

void PlottingOptions::setPreferences(QHash<ITree*, /*PlottingOptions::*/PlotPreferences> _pref)
{
	this->mTreePref = _pref;
}

QHash<ITree*, PlottingOptions::PlotPreferences> PlottingOptions::getPreferences()
{
	return this->mTreePref;
}
/*********************************************
* Line Width Operations
*********************************************/
//void PlottingOptions::setLineWidthForAttributePlot(ITree* _tree, int _linew)
//{
//	PlotPreferences currentPref = this->mTreePref.value(_tree);
//	currentPref.lineWidthAttribute = _linew;
//
//	//update preferences
//	this->mTreePref.remove(_tree);
//	this->mTreePref.insert(_tree, currentPref);
//}
//
//void PlottingOptions::setLineWidthForTreePlot(ITree* _tree, int _linew)
//{
//	PlotPreferences currentPref = this->mTreePref.value(_tree);
//	currentPref.lineWidthTree = _linew;
//
//	//update preferences
//	this->mTreePref.remove(_tree);
//	this->mTreePref.insert(_tree, currentPref);
//}
//
//int PlottingOptions::getLineWidthForAttributePlot(ITree* _tree)
//{
//	return this->mTreePref.value(_tree).lineWidthAttribute;
//}
//
//int PlottingOptions::getLineWidthForTreePlot(ITree* _tree)
//{
//	return this->mTreePref.value(_tree).lineWidthTree;
//}

void PlottingOptions::setLineWidthForAttributePlot(int _linew)
{
	//update preferences
	this->mAttributePenWidth = _linew;
}

void PlottingOptions::setLineWidthForTreePlot(int _linew)
{
	//update preferences
	this->mTreePenWidth = _linew;
}

int PlottingOptions::getLineWidthForAttributePlot()
{
	return this->mAttributePenWidth;
}

int PlottingOptions::getLineWidthForTreePlot()
{
	return this->mTreePenWidth;
}