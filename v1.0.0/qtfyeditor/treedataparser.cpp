/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Laura Skylaki
 * @date 17/04/2013
 */

// PROJECT
#include "treedataparser.h"
#include "qtfybackend/itrack.h"
#include "qtfybackend/tttmanager.h"
#include "qtfydata/tree.h"
#include "qtfydata/treestructuralhelper.h"

// QT
#include <QDirIterator>
#include <QHash>
#include <QElapsedTimer>
#include <QDebug>


TreeDataParser::TreeDataParser(TreeModel *ExperimentModel, QObject *parent)
    :QObject(parent)
{
    this->ExperimentModel = ExperimentModel;
}


QMap<QString, QMap<int, QVector<ITrack*> > > TreeDataParser::CreateExperimentModel(const QVector<QSharedPointer<ITree>> _trees, const QVector<ITrack*> _tracks, QVector<int> _regardedGenerations, QString pathToExperiments)
{

	// the map with the cells per trackpoint
	QMap< QString, QMap<int, QVector<ITrack*> > > cellsPerTrackpoint;

	// store here discovered positions
	QHash<QString, TreeItem*> discoveredPositions;
	// store here discovered timepoints per position
	QHash <int, QVector<QString> > discoveredTimepointsPerPosition;

	// go through the list of trees 
	for (QVector<QSharedPointer<ITree>>::const_iterator treeIterator = _trees.constBegin(); treeIterator != _trees.constEnd(); ++treeIterator) {
		ITree* currentTree = (*treeIterator).data();


		// find the data path for the images
		QString experimentName = currentTree->getExperimentName();
		QString positionIndex = currentTree->getPositionIndex();
		QString experimentsFileFolder = pathToExperiments;
		//QString treeName = currentTree->getTreeName();

		// if it's a new position, add it to list and create tree item
		if (!discoveredPositions.contains(positionIndex)) {

			// create the tree node
			QList<QVariant> columnData;
			columnData << positionIndex << experimentName;
			TreeItem* newPosition = new TreeItem(columnData, this->ExperimentModel->getRoot());
			this->ExperimentModel->getRoot()->appendChild(newPosition);

			// add it to the hash
			discoveredPositions.insert(positionIndex, newPosition);

			// add it to the cellsPerTrackpoint per position
			QMap<int, QVector<ITrack*> > cellsptp;
			cellsPerTrackpoint.insert(positionIndex, cellsptp);
		}

		QVector<ITrack*> cellList = TreeStructuralHelper::getTracksInGenerations(currentTree, _regardedGenerations.at(0), _regardedGenerations.at(_regardedGenerations.size()-1));


		// create the tree node
		//QString treeName = currentTree->getTreeName();
		//QList<QVariant> columnData;
		//columnData << treeName; // << imagePath;
		//TreeItem* newTree = new TreeItem(columnData, this->ExperimentModel->getRoot());
		//this->ExperimentModel->getRoot()->appendChild(newTree);

		// find the cells in the tree
		foreach(ITrack* cell, cellList)
		{
			//int cellNum = cell->getTrackNumber();

			//QString cellName = treeName + "_" +QString::number(cell->getTrackNumber());
			//QList<QVariant> cellData;
			//cellData << cellName; //<< imagePath;
			//TreeItem* newCell = new TreeItem(cellData, newTree);
			//newTree->appendChild(newCell);

			QMap<int, ITrackPoint*> trackPTs = cell->getTrackPointsRange(-1,-1);

			// find the trackpoints where cell is present
			foreach(ITrackPoint* tp, trackPTs)
			{
				int tpNum = trackPTs.key(tp);

				QMap<int, QVector<ITrack*> > cellsptp = cellsPerTrackpoint.value(positionIndex);
				if (!cellsptp.contains(tpNum)) {
					QVector<ITrack*> newEntry;
					cellsptp.insert(tpNum, newEntry);
				}

				if (!discoveredTimepointsPerPosition.value(tpNum).contains(positionIndex)) {

					/*QString trackpointName = "Timepoint_" + QString::number(tpNum);
					QList<QVariant> tpData;
					tpData << trackpointName << QString::number(tpNum);
					TreeItem* newTP = new TreeItem(tpData, discoveredPositions.value(positionIndex));
					discoveredPositions.value(positionIndex)->appendChild(newTP);*/

					// add it to discovered timepoints
					QVector<QString> positions = discoveredTimepointsPerPosition.value(tpNum);
					positions.append(positionIndex);
					discoveredTimepointsPerPosition.insert(tpNum, positions);
				}

				// add the cell to this trackpoints collection of cells
				QVector<ITrack*> test = cellsptp.value(tpNum);
				test.append(cell);
				cellsptp.insert(tpNum, test);

				cellsPerTrackpoint.insert(positionIndex, cellsptp);
			}

		}	
		
		 ////create a parent item for current position
   //             QList<QVariant> columnData;
   //             columnData << currentPosition << imagePath;
   //             TreeItem* newPosition = new TreeItem(columnData, this->ExperimentModel->getRoot());
   //             this->ExperimentModel->getRoot()->appendChild(newPosition);

   //             positionsFound.insert(position, newPosition);

   //             // add first timepoint (child) to newly identified position
   //             QList<QVariant> timepointData;
   //             timepointData << currentTimepoint << imagePath;
   //             TreeItem* newTimepoint = new TreeItem(timepointData, newPosition);
   //             newPosition->appendChild(newTimepoint);

   //             QString key = QString::number(position) + QString("x") + QString::number(timepoint);
   //             timepointsFound.insert(key, newTimepoint);

   //             // add wavelength child to timepoint
   //             QList<QVariant> leafData;
   //             leafData << imageFile << imagePath;

   //             TreeItem* newImage = new TreeItem(leafData, newTimepoint);
   //             newTimepoint->appendChild(newImage);

	}

	// create the tree structure based on the discovered timepoints and the discovered positions
	QList<int> allDiscoveredTimepoints = discoveredTimepointsPerPosition.keys();
	qSort(allDiscoveredTimepoints.begin(), allDiscoveredTimepoints.end());

	foreach(int tp, allDiscoveredTimepoints)
	{
		QString trackpointName = "Timepoint_" + QString::number(tp);
		QList<QVariant> tpData;
		tpData << trackpointName << QString::number(tp);

		// positions to append this timepoint
		QVector<QString> positionIndeces = discoveredTimepointsPerPosition.value(tp);

		foreach(QString positionIndex, positionIndeces) {
			TreeItem* newTP = new TreeItem(tpData, discoveredPositions.value(positionIndex));
			discoveredPositions.value(positionIndex)->appendChild(newTP);
		}
	}

	return cellsPerTrackpoint;

}

//    //get current time
//    QElapsedTimer timer;
//    timer.start();
//
//    // set dir iterator
//    QDirIterator dirIterator(dir,
//                 fileExtensions,
//                 QDir::AllDirs|QDir::Files|QDir::NoSymLinks,
//                 QDirIterator::Subdirectories);
//
//    qDebug() << "The QDirIterator setup took" << timer.elapsed() << "milliseconds";
//
//    timer.restart();
//
//    // Create list with positions that have been already added in parents
//    QHash<int, TreeItem*> positionsFound;
//
//    // Create list with timepoints that have been already added in parents
//    QHash<QString, TreeItem*> timepointsFound;
//
//    while(dirIterator.hasNext() && !this->stopped)
//    {
//
//        // make sure to call next, failing todo so will result in infinite loop
//        dirIterator.next();
//        QString imageFile = dirIterator.fileName();
//        QString imagePath = dirIterator.filePath();
//
//        // does it comply to the example file format?
//
//        // get position
//
//        int pos_p = imageFile.indexOf (this->filenameExample->getPositionToken());
//        int position =imageFile.mid(pos_p + 2, this->filenameExample->getPositionTokenDigits()).toInt();
//
//        // get timepoint
//        int pos_t = imageFile.indexOf (this->filenameExample->getTimeToken());
//        int timepoint =imageFile.mid(pos_t + 2, this->filenameExample->getTimeTokenDigits()).toInt();
//
//        // get wavelength
//        int pos_w = imageFile.indexOf (this->filenameExample->getWavelengthToken());
//        //int wavelength =imageFile.mid(pos_w + 2, this->filenameExample->getWavelengthTokenDigits()).toInt();
//
//        // get z-stack?
//
//
//        // it is a valid image name because it contains all the tokens
//        if (pos_p > 0 && pos_t > 0 && pos_w > 0){
//
//            // create position name i.e. Position 1
//            QString currentPosition = QString("Position ") + QString::number(position);
//            QString currentTimepoint = QString("Timepoint ") + QString::number(timepoint);
//
//            // if current position is discovered for the first time added in the list of found positions
//            // and create a parent item for this position
//            if (!positionsFound.contains(position)) {
//
//                // create a parent item for current position
//                QList<QVariant> columnData;
//                columnData << currentPosition << imagePath;
//                TreeItem* newPosition = new TreeItem(columnData, this->ExperimentModel->getRoot());
//                this->ExperimentModel->getRoot()->appendChild(newPosition);
//
//                positionsFound.insert(position, newPosition);
//
//                // add first timepoint (child) to newly identified position
//                QList<QVariant> timepointData;
//                timepointData << currentTimepoint << imagePath;
//                TreeItem* newTimepoint = new TreeItem(timepointData, newPosition);
//                newPosition->appendChild(newTimepoint);
//
//                QString key = QString::number(position) + QString("x") + QString::number(timepoint);
//                timepointsFound.insert(key, newTimepoint);
//
//                // add wavelength child to timepoint
//                QList<QVariant> leafData;
//                leafData << imageFile << imagePath;
//
//                TreeItem* newImage = new TreeItem(leafData, newTimepoint);
//                newTimepoint->appendChild(newImage);
//
//            }
//            else {
//                // the position has been found before
//                // but new timepoint for this position
//                QString key = QString::number(position) + QString("x") + QString::number(timepoint);
//                if (!timepointsFound.contains(key)) {
//                    // add first timepoint (child) to newly identified position
//                    QList<QVariant> timepointData;
//                    timepointData << currentTimepoint << imagePath;
//                    TreeItem* newTimepoint = new TreeItem(timepointData, positionsFound.value(position));
//                    positionsFound.value(position)->appendChild(newTimepoint);
//
//                    timepointsFound.insert(key, newTimepoint);
//
//                    // add wavelength child to timepoint
//                    QList<QVariant> leafData;
//                    leafData << imageFile << imagePath;
//
//                    TreeItem* newImage = new TreeItem(leafData, newTimepoint);
//                    newTimepoint->appendChild(newImage);
//                }
//                else {
//                    // just add the wavelengths
//                    // add wavelength child to timepoint
//                    QList<QVariant> leafData;
//                    leafData << imageFile << imagePath;
//
//                    TreeItem* newImage = new TreeItem(leafData, timepointsFound.value(key));
//                    timepointsFound.value(key)->appendChild(newImage);
//                }
//            }
//            if (!stopped) {
//                QString progressUpdate = QString("Loading data... Processing ") + currentPosition;
//                emit positionCompleted(progressUpdate);
//            }
//        }
//
////        QByteArray ba = imageFile.toLatin1();
////        const char *c_str2 = ba.data();
////        qDebug(c_str2);
//
//    }
//
//    qDebug() << "Parsing the directories took" << timer.elapsed() << "milliseconds";
//}



// SLOT
void TreeDataParser::process()
{
    qDebug("Worker has started...");
    if (!stopped)
        initializePictures(dir);
    emit finished();
}

// SLOT
void TreeDataParser::stop()
{
    qDebug("Worker needs to stop now...");
    stopped = true;

}

//TreeModel ExprFolderParser::getModel()
//{
//   return this->ExperimentModel;
//}

bool TreeDataParser::CheckFilename (const QString& filename)
{
    return true;
}

void TreeDataParser::initializePictures(const QString &path)
{

    ////Create list of file extensions
    //QStringList extensions;
    //if (filenameExample->getImageFormatSelected() == "any") {
    //    extensions = filenameExample->getImageFormatSupported();
    //}
    //else {
    //    extensions << filenameExample->getImageFormatSelected();
    //}

    //// Convert fileExtensions and filePrefix to QDir::entryList filters
    //QStringList filters;
    //for(int i = 0; i < extensions.size(); ++i) {
    //    filters.append("*" + extensions[i]);
    //}

    //// Get unsorted file list
    //this->ExperimentModel = ExprFolderParser::CreateExperimentModel(path, filters);

}