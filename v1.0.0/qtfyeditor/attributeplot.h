/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef	ATTRIBUTEPLOT_H
#define ATTRIBUTEPLOT_H

//PROJECT
#include "qtfybackend/itrack.h"
#include "qtfybackend/mathfunctions.h"
#include "qtfyplot.h"
#include "plottingoptions.h"
#include "../qtfybackend/tttmanager.h"
#include "../qtfybackend/tttpositionmanager.h"


//QT

//QWT
#include "qwt/qwt_plot.h"
#include "qwt/qwt_plot_curve.h" 
#include "qwt/qwt_curve_fitter.h"
#include "qwt/qwt_plot_grid.h"
#include "qwt/qwt_plot_marker.h" 

class AttributePlot
{

public:

	//the constructor
	AttributePlot();
	//the destructor
	~AttributePlot();

	/**
	Creates the cell curve to be displayed in the attribute plot.
	@param _title, the name of the curve (cell name)
	@param _c, the color of the curve
	@param _data, the points of the curve
	@param _cell, the cell of which the curve will be plotted
	@param _width, the pen width of the curve
	@return QwtPlotCurve*  the curve of the cell
	*/
	static QwtPlotCurve* AttributePlot::createOriginalCurve(QString _title, QColor _c, QVector<QPointF> _data, ITrack* _cell, int _width);

	/**
	Calculates the attribute values of all the given cells.
	@param _cells, the vector of the given cells
	@param _attributeName, the attribute which values it will calculate
	@param _quantChunkName, the chunk where the attribute is reffered to
	@param _xFactor, the time unit we calculate the time (Hours/Minutes/Seconds)
	@return QVector <QVector<QPointF>> the vector with the attribute points of the given cells
	*/
	static QVector <QVector<QPointF>> getValues(QVector<ITrack*> _cells, const QString _attributeName, QString _quantChunkName, float _xFactor);

	/**
	Converts to plot points the given attribute values.
	@param _attrData, the attribute values
	@param _xFactor, the time unit we calculate the time (Hours/Minutes/Seconds)
	@return QVector<QPointF> the converted points of the attribute values
	*/
	static QVector<QPointF> convertToPoints(QMap<int, QVariant> _attrData, float _xFactor);

	/**
	Creates the attribute plot.
	@param _cellList, the list of all the cells to plot for this attribute
	@param _attributeName, the name of the attribute we want to plot
	@param _quantChunkName, the chunk where the attribute is reffered to
	@param _options, the plotting options the user has selected
	@param _timeUnit, the time unit we want to calculate the time (Hours/Minutes/Seconds)
	@param _smooth, flag to check if the curves should be smoothed
	@param _points, flag to check if the points should be plotted as well
	@param _smoothValue, if smoothed curved is checked this value indicates the smoothing threshold, if not checked is invalid (-1)
	@return QTFyPlot* the attribute plot
	*/
	static QTFyPlot* createPlot(QVector<ITrack*> _cellList, QString _attributeName, QString _quantChunkName, QTFyPlot* _plot, PlottingOptions* _options, QString _timeUnit = "Hours", bool _smooth = false, bool _points = false, int _smoothValue = -1);

	/**
	Creates a smoothed cell curve to be displayed in the attribute plot.
	@param _originalPoints, the original values of the cell
	@param _name, the name of the curve (cell name)
	@param _smoothValue, the smoothing threshold
	@param _color, the color of the curve
	@param _width, the pen width of the curve
	@return QwtPlotCurve*  the smoothed curve
	*/
	static QwtPlotCurve* createSmoothedCurve(QVector<QPointF> _originalPoints, QString _name, int _smoothValue, QColor _color, int _width);

	/**
	Creates the points of the cell curve to be displayed in the attribute plot.
	@param _curveName, the name of the curve (cell name)
	@param _color, the color of the curve
	@param _points, the original values of the cell
	@param _cell, the cell 
	@param _width, the pen width of the curve
	@return QwtPlotCurve*  the points curve
	*/
	static QwtPlotCurve* plotOriginalPoints(QString _curveName, QColor _color, QVector<QPointF> _points, ITrack* _cell, int _width);

};
#endif