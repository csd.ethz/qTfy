/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/****************************************************************************
**
** @ Eleni Skylaki, 14.11.2014
**
**
****************************************************************************/
// PROJECT
#include "outliermanager.h"
#include "qtfydata/treestructuralhelper.h"
#include "qtfyattributetools.h"
#include "qtfybackend/tools.h"

// QT
#include "QDebug"
#include <QMessageBox>

// ARGLIB
#include "alglib-3.8.2/stdafx.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "alglib-3.8.2/interpolation.h"

using namespace alglib;

OutlierManager::OutlierManager(){}

bool OutlierManager::findOutliers(QTFyPlot* _plot, QString _attribute, QString _quantChunkName, ITree* _tree, QVector<int> _regardedGenerations)
{
	outliers.clear();

	QPolygonF originalPoints;
	QPolygonF smoothPoints;
	QPolygonF outlierPoints;
	QHash<ITrack* , QPolygonF> outliersPerCell;

	// Get the cells of the tree
	QVector<ITrack*> cellList = TreeStructuralHelper::getTracksInGenerations(_tree, _regardedGenerations.at(0), _regardedGenerations.at(_regardedGenerations.size()-1));

		//for all cells of the tree find the outliers
		foreach(ITrack* cell, cellList){

			//check if the cell is hidden
			if(mOptions->getCellColor(cell).alpha() == 0)
				continue;

			originalPoints = findOriginalPoints(cell, _attribute, _quantChunkName);

			if(originalPoints.isEmpty())
				continue;

			smoothPoints = findSmoothPoints(originalPoints);

			outlierPoints = detectOutliersDistance( originalPoints, smoothPoints);
			if(!outlierPoints.isEmpty())
				outliersPerCell.insert(cell, outlierPoints);
		}
		
		if(!outliersPerCell.isEmpty()){
			this->outliers.insert(_tree, outliersPerCell);

		}

		if(this->outliers.isEmpty())
			return false;
		//free memory
		originalPoints.clear();
		smoothPoints.clear();
		outliersPerCell.clear();

		return true;
}

void OutlierManager::showOutliers(QTFyPlot* _plot, ITree* _tree)
{
	if(outliers.isEmpty())
		return;

	for(QHash<ITree*, QHash<ITrack*, QPolygonF>>::const_iterator outliersIt = outliers.constBegin(); outliersIt != outliers.end(); ++outliersIt){
		//find correct tree
		if(outliersIt.key() == _tree){
			//for every cell, plot the outliers
			for(QHash<ITrack*, QPolygonF>::const_iterator cellOutliersIt = outliersIt.value().constBegin(); cellOutliersIt != outliersIt.value().end(); ++cellOutliersIt){
				QString cellName = QString::number(cellOutliersIt.key()->getTrackNumber());
				drawOutliers(_plot, cellOutliersIt.value(), cellName);
			}
		}
	}
}

//works for specific tree not all treeset
void OutlierManager::deactivateOutliers(QString _attribute, ITree* _tree, QString _chunkName)
{
	//data of the outliers		
	QHash<ITrack*, QVector<int>> cellsToDeactivate;
	//ITree* currentTree;

	if (outliers.empty()) {
		QMessageBox::information (0,  "Outliers warning", _attribute +": No outliers have been identified. No timepoints will become deactivated.", QMessageBox::Ok);
		return;
	}
	
	//for(QHash<QString, QHash<ITree*, QHash<ITrack*, QPolygonF>>>::const_iterator attrIt = attrOutliers.constBegin(); attrIt != attrOutliers.end(); ++attrIt){
		//QString attrName = attrIt.key();
		//if(attrName == _attribute){
			for(QHash<ITree*, QHash<ITrack*, QPolygonF>>::const_iterator outliersIt = outliers.constBegin(); outliersIt != outliers.end(); ++outliersIt){
				//find correct tree
				if(outliersIt.key() == _tree){
					//vector with the timepoints of the outliers
					QVector<int> tmpTimepoints;
					//for every cell of the tree
					for(QHash<ITrack*, QPolygonF>::const_iterator cellOutliersIt = outliersIt.value().constBegin(); cellOutliersIt != outliersIt.value().end(); ++cellOutliersIt){
						//find the timepoints of the outliers
						for(QPolygonF::const_iterator it = cellOutliersIt.value().constBegin(); it != cellOutliersIt.value().end(); ++it){
							int timepoint = it->x();
							tmpTimepoints.append(timepoint);
						}
						cellsToDeactivate.insert(cellOutliersIt.key(), tmpTimepoints);
						tmpTimepoints.clear();
					}
				}
			}
		//}
//	}

	//check if amt or clt attribute
	//TODO:check for not valid attribute type
	//if (attributeUnitName.contains("AMT")){
	//	const AMTAttribute* tempAttr = dynamic_cast<const AMTAttribute*> (_attribute);
	//	tempAttr->updateDataEntry(cellsToDeactivate, _tree);
	//}
	//else if(attributeUnitName.contains("CLT")){
	//	const CLTAttribute* tempAttr = dynamic_cast<const CLTAttribute*> (_attribute);
	//	tempAttr->updateDataEntry(cellsToDeactivate, _tree);
	//}

	//update the activation status of the timepoints
	
	QTFyAttributeTools::updateDataEntry(cellsToDeactivate, _tree, _attribute, _chunkName);

	outliers.clear();
}

QPolygonF OutlierManager::findSmoothPoints(QPolygonF _originalPoints)
{
	//the data to return
	QPolygonF smoothedPoints;

	//check if empty original points
	if(_originalPoints.isEmpty())
		return smoothedPoints;

	//check for nan or infinite values
	for(int i = 0; i < _originalPoints.size(); i++){
		double currentValue = _originalPoints[i].ry();
		if(!Tools::isValidNumber(currentValue))
			_originalPoints.remove(i); //skip nan and +/-inf values
	}

	//the smoothing threshold
	double rho = this->smoothThreshold;

		QPointF tmpPoint;

		ae_int_t info;
		double v;
		spline1dinterpolant s;
		spline1dfitreport rep;
		//double rho;

		real_1d_array x1;
		real_1d_array y1;

		int size = _originalPoints.size();
		double *xData = new double [size];
		double *yData = new double [size];
		int count = 0;
		foreach (QPointF pnt, _originalPoints) {
			xData[count] = pnt.x();
			yData[count] = pnt.y();
			count++;
		}

		x1.setcontent(size, xData);
		y1.setcontent(size, yData);

		spline1dfitpenalized(x1, y1, 50, rho, info, s, rep);

		for(int i = 0; i<size; i++)
		{
			v = spline1dcalc(s, xData[i]);
			tmpPoint.setX(xData[i]);
			tmpPoint.setY(v);

			smoothedPoints.append(tmpPoint);
		}

		//free memory 
		delete xData;
		delete yData;

	return smoothedPoints;
}

//QPolygonF OutlierManager::detectOutliers(QwtPlot* _plot, QPolygonF _originalPoints, QPolygonF _smoothedPoints)
//{
//	//data to return
//	QVector<QPointF> outlierPoints;
//	PlotPickerHelp helper;
//
//	if (!_plot)
//		return outlierPoints;
//
//	for (int i = 0; i < _originalPoints.size(); ++i) {
//		// curve point values
//		
//		QPointF pntCurve;
//		//transform the original point to pixels
//		pntCurve.setX(_originalPoints[i].rx());
//		pntCurve.setY(_originalPoints[i].ry());
//
//		
//		QPointF pntOriginal = helper.transformPointToPixelCoords(_plot, pntCurve);
//
//		//transform the smoothed point to pixels
//		pntCurve.setX(_smoothedPoints[i].rx());
//		pntCurve.setY(_smoothedPoints[i].ry());
//
//		//QPoint pntSmoothed = /*transformPointToPixelCoords(pntCurve);*/ pntCurve.toPoint();
//		QPointF pntSmoothed = helper.transformPointToPixelCoords(_plot, pntCurve);
//
//
//		//calculate the distance between the original and the smoothed point
//		//if greater than minDist the original point is an outlier and must be converted
//		int actualDist;
//
//		if(pntOriginal.ry() > pntSmoothed.ry())
//		
//			actualDist = abs(pntOriginal.ry() - pntSmoothed.ry());
//		else
//			actualDist = abs(pntSmoothed.ry() - pntOriginal.ry());
//
//		if(actualDist > this->detectionThreshold)
//			outlierPoints.append(_originalPoints[i]);
//	}		
//	return outlierPoints;
//}

QPolygonF OutlierManager::findOriginalPoints(ITrack* _cell, QString _attribute, QString _quantChunkName)
{
	//the return data
	QPolygonF originalPoints;

	// Get the data
	QMap<int, QVariant> attrData = QTFyAttributeTools::getTimedValue(*_cell, _attribute, _quantChunkName);
	//QMap<int, QVariant> attrData = _attribute->getTimedValue(*_cell, _unit, "Experiment time");

	// Save data
	QVector<QPointF> data = convertToPoints(attrData);
	if(data.size() > 1)
		originalPoints = data;
		
	return originalPoints;
}

QPolygonF OutlierManager::convertToPoints(QMap<int, QVariant> data)
{
	QPolygonF points;

	for(QMap<int, QVariant>::iterator i = data.constBegin(); i != data.constEnd(); ++i) {
		if(i.value().toDouble() != i.value().toDouble())
			continue; //skip nan values
		else
			points.append(QPointF(i.key(), i.value().toDouble()));
	}
	return points;
}

void OutlierManager::drawOutliers(QTFyPlot* _plot, QPolygonF _outliers, QString _cellName)
{
	QwtPlotCurve* curve = new QwtPlotCurve("O"+ _cellName);
	QwtSymbol sym( QwtSymbol::Diamond, Qt::black, Qt::NoPen, QSize( 7, 7 ) );
	QPen pen(Qt::black);

	//default xFator Hours, TO BE CHANGED!!
	double xFactor = 1.0/3600.0;
	_outliers = applyXFactorToData(xFactor, _outliers);


	curve->setSymbol(sym);
	curve->setPen(pen);
	curve->setData(_outliers);
	curve->setStyle(QwtPlotCurve::NoCurve);
	curve->attach(_plot);

}

QwtPlotCurve* OutlierManager::getCellOutlierCurve(QPolygonF _outliers, QString _cellName){
	QwtPlotCurve* curve = new QwtPlotCurve("O"+ _cellName);
	QwtSymbol sym( QwtSymbol::Diamond, Qt::black, Qt::NoPen, QSize( 7, 7 ) );
	QPen pen(Qt::black);

	//default xFator Hours, TO BE CHANGED!!
	double xFactor = 1.0/3600.0;
	_outliers = applyXFactorToData(xFactor, _outliers);


	curve->setSymbol(sym);
	curve->setPen(pen);
	curve->setData(_outliers);
	curve->setStyle(QwtPlotCurve::NoCurve);
	return curve;
}

void OutlierManager::setSmoothingThreshold(int _sValue)
{
	this->smoothThreshold = _sValue;
}

void OutlierManager::setDetectionThreshold(int _dValue)
{
	this->detectionThreshold = _dValue;
}

//return the outliers with the x factor adjasted on the timepoints
QPolygonF OutlierManager::getOutliersOfTree(ITree* _tree)
{
	//the return data
	QPolygonF data;

	if(outliers.isEmpty())
		return data;

	for(QHash<ITree*, QHash<ITrack*, QPolygonF>>::const_iterator it = outliers.constBegin(); it != outliers.end(); ++it){
		if(it.key() == _tree){
			for(QHash<ITrack*, QPolygonF>::const_iterator cellIt = it.value().constBegin(); cellIt != it.value().end(); ++cellIt){
				QVector<QPointF> points = cellIt.value();
				foreach(QPointF pnt, points){
					QPointF tmpPoint;
					tmpPoint.setX(pnt.rx());
					tmpPoint.setY(pnt.ry());
					data.append(pnt);
				}
			}
		}
	}
	return data;
}

//change according the cases of xFactor we can have
QPolygonF OutlierManager::applyXFactorToData(double _xFactor, QPolygonF _data)
{
	for(QPolygonF::iterator it = _data.begin(); it != _data.end(); ++it){
		it->setX(it->x()*_xFactor);
	}

	return _data;
}

QPolygonF OutlierManager::detectOutliersDistance( QPolygonF _originalPoints, QPolygonF _smoothedPoints)
{
	double distance;

	QPolygonF outliers;
	int smoothIter = 0;
	for(int i = 0; i < _originalPoints.size(); i++){
		//check if same point
		double ox = _originalPoints[i].rx();
		double sx = _smoothedPoints[smoothIter].rx();

		if(_originalPoints[i].rx() != _smoothedPoints[smoothIter].rx())
			continue;

		distance = abs(_originalPoints[i].ry() - _smoothedPoints[i].ry());
		distance = (distance * 100)/max;

		if(distance > this->detectionThreshold)
			outliers.append(_originalPoints[i]);

		smoothIter++;
		if(smoothIter == _smoothedPoints.size())
			break;
	}

	return outliers;
}

void OutlierManager::setMinMax(double _min, double _max)
{
	this->min = _min;
	this->max = _max;

	if (min < 0)
		max = max + min;
	else
		max = max - min; 
}

//set the plotting options of the user
void OutlierManager::setPlottingOptions(PlottingOptions* _opt)
{
	this->mOptions = _opt;
}

QPolygonF OutlierManager::findOutliersOfCell(QTFyPlot* _plot, QString _attribute, QString _quantChunkName, ITrack* _cell)
{
	QPolygonF originalPoints;
	QPolygonF smoothPoints;
	QPolygonF outlierPoints;

	//check if the cell is hidden
	if(mOptions->getCellColor(_cell).alpha() == 0)
		return outlierPoints;

	originalPoints = findOriginalPoints(_cell, _attribute, _quantChunkName);

	if(originalPoints.isEmpty())
		return outlierPoints; 

	smoothPoints = findSmoothPoints(originalPoints);

	outlierPoints = detectOutliersDistance( originalPoints, smoothPoints);

	//return
	return outlierPoints;

}
