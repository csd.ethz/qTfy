/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/****************************************************************************
**
** @ Eleni Skylaki, 07.07.2014
**
**
****************************************************************************/

// PROJECT
#include "qtfyMain.h"
#include "qtfytreedataparser.h"
#include "attributedisplayselection.h"
#include "plotpickerhelp.h"
#include "qtfyattributetools.h"
#include "scrollzoomer.h"
#include "qtfyplot.h"
#include "heattreeplot.h"
#include "parametercalculator.h"
#include "qtfygui/quantificationoptions.h"
#include "qtfygui/qtfyhelp.h"
#include "qtfygui/startscreen.h"
#include "qtfygui/viewchangelog.h"
#include "qtfydata/treestructuralhelper.h"
#include "qtfybackend/tools.h"
#include "qtfybackend/tttmanager.h"
#include "qtfyio/exportworker.h"


//QT
#include <QInputDialog>
#include <QColorDialog>
#include <QDebug>
#include <QFormLayout>
#include <qscrollbar.h>
#include <qfiledialog.h>
#include <QImageWriter>
#include <QGroupBox>
#include <QPen>
#include <QMdiSubWindow>
#include <QSvgGenerator>
#include <QToolTip>
#include <QMessageBox>
#include <QShortcut>
#include <QPixmap>
#include <QApplication>

//QWT
#include "qwt/qwt_plot.h"
#include "qwt/qwt_plot_curve.h"
#include "qwt/qwt_data.h"
#include "qwt/qwt_legend.h"
#include "qwt/qwt_symbol.h"
#include "qwt/qwt_plot_zoomer.h" 
#include "qwt/qwt_scale_engine.h"
#include "qwt/qwt_scale_widget.h"
#include "qwt/qwt_scale_div.h"
#include "qwt/qwt_plot_magnifier.h"
#include "qwt/qwt_plot_panner.h"
#include "qwt/qwt_plot_zoomer.h"

// ARGLIB
#include "alglib-3.8.2/stdafx.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "alglib-3.8.2/interpolation.h"

using namespace alglib;

QtfyMain::QtfyMain(QWidget *parent)
	: QMainWindow(parent)
{

	ui.setupUi(this);

	ui.mdiArea->setOption(QMdiArea::DontMaximizeSubWindowOnActivation);
	ui.txtMessageLog->setReadOnly(true);

	//Toolbar
	ui.actAutoAlign->setChecked(true);
	ui.actionContinuous_mode->setChecked(true);
	ui.actionShow_contour_overlay->setChecked(true);
	ui.actTreePlot->setChecked(true);

	// also add progress bar at the status bar
	//Insert progress bar on status bar
	progressBar = new QProgressBar(NULL);
	progressBar->setTextVisible(false);
	progressBar->setVisible(false);

	this->statusBar()->addWidget(progressBar, 100);
	

	progressBar->setValue(0);
	progressBar->setMinimum(0);
	progressBar->setMaximum(0);

	// initialize the export bools
	this->csvExportDone = false;
	this->csvExportRunning = false;
	this->closingPending = false;

	//inialize radio button (Show original points) -> true
	this->showOriginalPoints = true;
	ui.actShowPoints->setChecked(true);
	//ui.chkShowPoints->setChecked(true);

	//initialize auto align true
	this->isAutoAlign = true;

	//Outlier Detection
	this->OutlierDetection = false;
	this->deactivateOutliers = false;
	this->applyToAllSelected = false;
	ui.spbOutlierDetection->setRange(0, 100);
	ui.sldOutlierDetection->setRange(0, 100);
	ui.spbOutlierDetection->setValue(10);
	this->setDetectionValue(10);

	//Smooth curves 
	this->smoothLines = false;
	ui.spbSmoothCurves->setRange(0,10); //doesn't work
	ui.sldSmoothCurves->setRange(0,10);
	ui.spbSmoothCurves->setValue(3);
	this->sValue = 3;

	//the outlier manager
	this->mOutlierManager = new OutlierManager();

	// initialize the tree overview dock widget
	m_tvwDataTreeOverview = new TreeOverviewItemView(ui.dockData);
	ui.dockData->setWidget(m_tvwDataTreeOverview);
	this->addDockWidget(Qt::LeftDockWidgetArea, ui.dockData);
	this->addDockWidget(Qt::LeftDockWidgetArea, ui.dockOutlierDetection);
	this->addDockWidget(Qt::LeftDockWidgetArea, ui.dockMessageLog);

	// initialize the experiment tree model

	QList<QVariant> rootData;
	rootData << "Trees" ;
	this->m_TreeDataModel = new TreeModel(rootData);
	this->m_TreeDataModel->setIsCellBased(false);
	 
	this->m_TreeModels.insert("0", m_TreeDataModel);

	// connect the experiment tree model to the view
	this->m_tvwDataTreeOverview->setModel(this->m_TreeDataModel);

	// tree model view connections
	this->m_tvwDataTreeOverview->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(this->m_tvwDataTreeOverview, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(slotRtClickTreeOverview(const QPoint&)));

	// mdi area connections
	ui.mdiArea->setContextMenuPolicy(Qt::CustomContextMenu);

	setExperimentsFilesFolder (TTTManager::getInst().getExperimentPath());

	createActions();
	createShortcuts();
	
	//the default y axis scale will be global
	this->yAxisToLocalScale = false;

	//manual x,y axis settings default false
	this->manualXAxisSettings = false;
	this->manualYAxisSettings = false;
	this->maxX = std::numeric_limits<double>::quiet_NaN(); //set nan unless the user change them
	this->minX = std::numeric_limits<double>::quiet_NaN();
	this->maxY = std::numeric_limits<double>::quiet_NaN();
	this->minY = std::numeric_limits<double>::quiet_NaN();

	//plotting options
	this->mPlottingOptions = new PlottingOptions();
	mOutlierManager->setPlottingOptions(mPlottingOptions);

	this->showMaximized();

	ui.txtMessageLog->setMaximumHeight(16777215);
	ui.dockMessageLog->setMaximumHeight(524287);

	//selectedCell 
	this->selectedCell = 0;
	//should replot
	this->mustReplot = true;
	//length
	this->yAxisLength = 0;

	//time unit
	this->mTimeUnit = "Hours";

	//default restore colors flag
	this->restoreColors = false;

	lastSelectedDirectory = TTTManager::getInst().getExperimentPath();

	// ui.menuAbout->menuAction()->setVisible(false);
	ui.actionQTFyHelp->setVisible(false);
}

QtfyMain::~QtfyMain()
{	
	
}

void QtfyMain::createActions()
{

	//tree dock actions
	connect (this->m_tvwDataTreeOverview, SIGNAL(clicked (const QModelIndex&)), this, SLOT (updateAllViewers(const QModelIndex&)));
	
	connect(this->m_tvwDataTreeOverview, SIGNAL(clicked (const QModelIndex&, QString)), this, SIGNAL (treeviewIndexChangedStagger(const QModelIndex&, QString)));
	connect (this->m_tvwDataTreeOverview, SIGNAL(clicked (const QModelIndex&)), this, SIGNAL (treeviewIndexChanged(const QModelIndex&)));
	connect(ui.actAddAttribute, SIGNAL(triggered()),this, SLOT(openAttributeSelectionDialog()) );
	connect(ui.actTreePlot, SIGNAL(toggled(bool)), this, SLOT(checkTreePlot(bool)));
	connect(ui.actionOpenSegEditor, SIGNAL(triggered()), this, SLOT(openSegmentationEditor()));

	connect(ui.actionParameterCalculator, SIGNAL(triggered()), this, SLOT(openParameterCalculator()));

	//Outlier Detection
	connect(ui.chkOutlierDetection, SIGNAL(toggled(bool)), this, SLOT(outlierSelected(bool))); 
	connect(ui.spbOutlierDetection, SIGNAL(valueChanged(int)), ui.sldOutlierDetection, SLOT(setValue(int)));
	connect(ui.sldOutlierDetection, SIGNAL(valueChanged(int)), ui.spbOutlierDetection, SLOT(setValue(int)));
	connect(ui.spbOutlierDetection, SIGNAL(valueChanged(int)), this, SLOT(updateDetectionThres(int)));
	connect(ui.pbtDeactivateOutliers, SIGNAL(clicked()), this, SLOT(deactivateOutliersSelected()));
	//Reset inactive timepoints 
	connect(ui.pbtSetAllTimepointsToActive, SIGNAL(clicked()), this, SLOT(setAllTimepointsActive()));
	connect(ui.pbtHelpOutliers, SIGNAL(clicked()), this, SLOT(showHelpOutliers()));

	//Smooth curves
	connect(ui.chkSmoothLines, SIGNAL (toggled(bool)), this, SLOT(smoothLinesSelected(bool)));
	connect(ui.spbSmoothCurves, SIGNAL(valueChanged(int)), ui.sldSmoothCurves, SLOT(setValue(int)));
	connect(ui.sldSmoothCurves, SIGNAL(valueChanged(int)), ui.spbSmoothCurves, SLOT(setValue(int)));
	connect(ui.spbSmoothCurves,SIGNAL (valueChanged(int)), this, SLOT(updateSmoothingThres(int)));

	//show original points
	connect(ui.actShowPoints, SIGNAL(triggered(bool)), this, SLOT(showPointsSelected(bool)));
	//connect(ui.chkShowPoints, SIGNAL(toggled(bool)), this, SLOT(showPointsSelected(bool)));

	// plot submenu
	connect(ui.actionChangeCellColor, SIGNAL(triggered()), this, SLOT(changeColor()));
	connect(ui.actionHideCell, SIGNAL(triggered()), this, SLOT(hideBranch()));
	connect(ui.actionShowCell, SIGNAL(triggered()), this, SLOT(showBranch()));
	connect(ui.actionShowAll, SIGNAL(triggered()), this, SLOT(showAllCells()));
	//connect(ui.actChangeLineWidth, SIGNAL(triggered()), this, SLOT(editLineWidth()));

	//Auto align
	connect(ui.actAutoAlign, SIGNAL(triggered()), this, SLOT(autoAlignSelected()));

	//Save
	connect(ui.actionExport_QTfy_trees, SIGNAL(triggered()), this, SLOT(exportTrees()));
	connect(ui.actExportImages, SIGNAL(triggered()), this, SLOT(exportPlotImages()));
	connect(ui.actionExportHeattreePlot, SIGNAL(triggered()), this, SLOT(exportHeattreePlot()));
	connect(ui.actExportMessageLog, SIGNAL(triggered()), this, SLOT(exportMessageLog()));

	// Cancel
	connect(ui.actionExit_QTFy, SIGNAL(triggered()), this, SLOT(cancel()));

	// Help submenu
	//connect(ui.actionQTFyHelp, SIGNAL(triggered()), this, SLOT(openQTFyHelp()));
	connect(ui.actionAboutQTFy, SIGNAL(triggered()), this, SLOT(showStartScreen()));
	connect(ui.actionViewChangeLog, SIGNAL(triggered()), this, SLOT(showChangeLog()));

	//Restore default color of the scheme
	connect(ui.actRestoreDefaultColors, SIGNAL(triggered()), this, SLOT(restoreDefaultColors()));
	

}

void QtfyMain::createShortcuts()
{
	// alternative shortcuts
	/*QShortcut* shrPreviousCell = new QShortcut(QKeySequence(Qt::Key_A), this);
	connect(shrPreviousCell, SIGNAL(activated()), ui.actGoPreviousCell, SLOT(trigger()));

	QShortcut* shrNextCell = new QShortcut(QKeySequence(Qt::Key_D), this);
	connect(shrNextCell, SIGNAL(activated()), ui.actGoNextCell, SLOT(trigger()));*/

	QShortcut* shrNextTimepoint = new QShortcut(QKeySequence(Qt::Key_D), this);
	connect(shrNextTimepoint, SIGNAL(activated()), this->m_tvwDataTreeOverview, SLOT(outIndexChangeDown()));

	QShortcut* shrPreviousTimepoint = new QShortcut(QKeySequence(Qt::Key_A), this);
	connect(shrPreviousTimepoint, SIGNAL(activated()), this->m_tvwDataTreeOverview, SLOT(outIndexChangeUp()));  

	/*QShortcut* shrManualSave = new QShortcut(QKeySequence::Save, this);
	connect(shrManualSave, SIGNAL(activated()), ui.actGoPreviousTimepoint, SLOT(trigger()));

	QShortcut* shrExitSegEditor = new QShortcut(QKeySequence(QKeySequence::Quit), this);
	connect(shrExitSegEditor, SIGNAL(activated()), ui.actExit, SLOT(trigger()));*/
}

void QtfyMain::setExperimentsFilesFolder (QString path)
{
	// check if file exists
	if (QDir(path).exists())
		this->pathToExperiments = path;
	// cannot find the folder -> prompt for correction
	else {
		QString s = QFileDialog::getExistingDirectory(this, tr("Choose the root folder of experiments with the original image files"), "./", QFileDialog::ShowDirsOnly);
		this->pathToExperiments = s;
	}
}

void QtfyMain::initializeData(QVector<QSharedPointer<ITree>> _trees)
{
	// Get the trees of the selected tree set
	trees = _trees;
	
	for(QVector<QSharedPointer<ITree>>::iterator it = trees.begin(); it != trees.end(); ++it){
		ITree* cTree = it->data();
		this->mPlottingOptions->initializeOptions(cTree);
		//Get the cells of the selected tree set
		//get max generation
		int maxGeneration = cTree->getTreeProperty(ITree::TREE_OPT_NUMBER_OF_GENERATIONS).toInt();
		tracks += TreeStructuralHelper::getTracksInGenerations(cTree, 0, maxGeneration);

	}
	

	this->setCursor(Qt::WaitCursor);
	//Data parser
	QtfyTreeDataParser dataparser (m_TreeDataModel);

	dataparser.CreateExperimentModel(trees);

	this->setTimepointsPerCell(dataparser.getTimepointsPerCell());
	this->setCellsPerTree(dataparser.getCellsPerTree());
	this->setCellsPerTimepoint(dataparser.getCellsPerTimepoint());

	this->m_tvwDataTreeOverview->setHideMode(false);
	this->m_TreeDataModel->setIsIndexColourAvailable(false);

	// load initial tree 
	updateAllViewers(this->m_TreeDataModel->index(0,0));

	//find ALL the attributes of ALL the trees
	

	QTFyAttributeTools::findAttributesPerQuantificationChunk(this->trees, this->chunkAttributes, this->chunkSoftware, this->chunkRemarks);

	//setup dialog
	AttributeDisplaySelection* selection = AttributeDisplaySelection::getInst(this);
	selection->initializeData(this->chunkAttributes, this->chunkSoftware, this->chunkRemarks);
	// signal to get the slected attributes from the dialog
	connect(selection, SIGNAL(successfulAttributeSelection(QVector<QString>&, QString)), this, SLOT(addPlotAttribute(QVector<QString>&, QString)));
	connect(this, SIGNAL(attributeRemoved(QString, QString)), selection, SLOT(uncheckAttribute(QString, QString)));
	this->setCursor(Qt::CustomCursor);
}

void QtfyMain::setTimepointsPerCell(QMap<QString, QMap<ITrack*, QVector<int> > > _timepointsPerCell)
{
	this->timepointsPerCell = _timepointsPerCell;
}

void QtfyMain::setCellsPerTree(QMap< QString, QMap< QString, ITrack* > > _cellsPerTree)
{
	this->cellsPerTree = _cellsPerTree;
}

void QtfyMain::setCellsPerTimepoint(QMap< QString, QMap< int, QVector<ITrack*> > > _cellsPerTimepoint)
{
	this->cellsPerTimepoint = _cellsPerTimepoint;
}

void QtfyMain::updateAllViewers(const QModelIndex& index)
{	
	/*****************************************************
	** Update tree model index and treeSetInformation
	*****************************************************/
	treeSetInformation.clear();

	int timepointCount = 0;

	setCursor(Qt::BusyCursor);

	// get the tree item for the index or the root if index is invalid
	TreeItem *selectedItem;

	if (!index.isValid())
		selectedItem = this->m_TreeDataModel->getRoot();
	else
		selectedItem = static_cast<TreeItem*>(index.internalPointer());

	// is this a tree item? it is if the parent is the root
	if (selectedItem->column() == 0) {
		treeSetInformation.append(selectedItem->data(0).toString()); //experiment name
		treeSetInformation.append(selectedItem->child(0)->data(0).toString()); //tree name
	}
	else if (selectedItem->column() == 1) {
		treeSetInformation.append(selectedItem->parent()->data(0).toString());
		treeSetInformation.append(selectedItem->data(0).toString());

	}
	else if (selectedItem->column() == 2){
		treeSetInformation.append(selectedItem->parent()->parent()->data(0).toString());
		treeSetInformation.append(selectedItem->parent()->data(0).toString());
		treeSetInformation.append(selectedItem->data(0).toString()); //cell name (number)

	}else if (selectedItem->column() == 3){
		treeSetInformation.append(selectedItem->parent()->parent()->parent()->data(0).toString());
		treeSetInformation.append(selectedItem->parent()->parent()->data(0).toString());
		treeSetInformation.append(selectedItem->parent()->data(0).toString());
		treeSetInformation.append(selectedItem->data(0).toString()); //timepoint name (number)
	}
	else return;

	if(treeSetInformation.size() < 3)
		selectedCell = 0;
	/*****************************************************
	** Initialize selected cell
	*****************************************************/	
	bool update = false;
	//check if selected cell is updated
	if(selectedCell){
		QString scID = QString::number(selectedCell->getTrackNumber());
		//if the currently selected cell isn't the selected cell stored here, update
		if(treeSetInformation[2] != scID)
			update = true;
	}
	//check if the selected cell isn't initialized although the user selected a cell or if the selected cell must be updated
	//and set the selected cell the currently selected cell
	if(selectedCell == 0 && selectedItem->column() >= 2 || update){
		//iterate through trees of the tree set
		for(QMap< QString, QMap< QString, ITrack* > >::iterator treeIt = cellsPerTree.begin(); treeIt != cellsPerTree.end(); ++treeIt){
			QString currTree = treeIt.key();
			//find current tree
			if(currTree == treeSetInformation[1]){
				//get the cells of the tree
				QMap<QString, ITrack*> cells = treeIt.value();
				//iterate cells 
				for(QMap<QString, ITrack*>::iterator cellIt = cells.begin(); cellIt != cells.end(); ++cellIt){
					//find selected cell
					QString cellName = cellIt.key();
					cellName = "Cell_" + cellName;
					if(treeSetInformation[2] == cellName){
						selectedCell = cellIt.value();
						break;
					}
				}
			}
		}
	}

	/*****************************************************
	** Create tree plot for the first time
	*****************************************************/	
	if(mSubwindows.isEmpty() && ui.actTreePlot->isChecked()) { // if the first time the editor is opened
		this->displaySelectedTree();
		ui.mdiArea->tileSubWindows();
		AttributeDisplaySelection::getInst()->show();
		return;
	}

	/*****************************************************
	** Update tree and attribute plots
	*****************************************************/	
	if(mSubwindows.contains("treeplot"))
		this->displaySelectedTree();
	if(currentTreeInPlots != treeSetInformation[1])
		this->refreshAttributePlots();

	/*****************************************************
	** Plot timepoint line
	*****************************************************/	
	if(selectedItem->column() == 3){
		//find timepoint number
		QString timepointName =treeSetInformation[3];
		int numIndex = timepointName.length() - (timepointName.lastIndexOf("_") + 1);
		int timepoint = (timepointName.right(numIndex)).toInt();
		//draw timepoint line to all existing plots
		plotTimepoint(timepoint);
	}
	else 
		plotTimepoint(0);

	/*****************************************************
	** Highligh selected cell
	*****************************************************/
	if (selectedCell != 0){
		highlightCell(selectedCell);
	}

	/*****************************************************
	** Align subwindows
	*****************************************************/
	if(ui.actAutoAlign->isChecked())
		this->tileVerticallySubwindows();

	this->m_tvwDataTreeOverview->setFocus();

	this->currentTreeInPlots = treeSetInformation[1];

	this->setCursor(Qt::CustomCursor);
}

// SLOT
void QtfyMain::plotRtClickContextMenu(const QPoint& pnt)
{

	QwtPlotCanvas* selectedPlotCanvas = (QwtPlotCanvas*) (sender());
	QwtPlot* selectedPlot = selectedPlotCanvas->plot();
	//Update subwindow focus only if staggering experiment
	if(this->staggerWls){
		QString currWindowTitle = selectedPlot->windowTitle();
		this->setWindowFocus(currWindowTitle);
	}

	PlotPickerHelp helper;
	QPoint mapToPlot = helper.invTransform(selectedPlot, pnt);

	//test
	//trasform the qpoint to qwtdouble point
	QSize canvasSize = selectedPlotCanvas->size();
	QSize plotSize = selectedPlot->size();

	int xDiff = plotSize.width() - canvasSize.width();

	QwtDoublePoint newPos;
	newPos.setX(pnt.x() - xDiff);
	newPos.setY(pnt.y());

	//QModelIndex indexToSelectedTimepoint = this->findClickedIndexFromPlot(selectedPlot, mapToPlot);
	QModelIndex indexToSelectedTimepoint = this->findClickedIndexFromPlot(selectedPlot, pnt);
	QPoint globalPos = selectedPlotCanvas->mapToGlobal(pnt);
	
	QMenu myMenu;

	if (indexToSelectedTimepoint.isValid()) {
		//seg editor
		QAction *actOpenSegEditor = new QAction(QIcon(":/qtfyres/qtfyres/seg.png"), "Open the Segmentation Editor", this);
		//color change
		QAction *actColorOptions = new QAction(QIcon(":/qtfyres/qtfyres/colorize-2.png"), "Change cell color", this);
		//hide branch
		QAction *actHideBranch = new QAction(QIcon(":/qtfyres/qtfyres/hide-branch.png"), "Hide cell and progeny", this);
		//line width change
		QAction *actEditLineWidth = new QAction(QIcon(":/qtfyres/qtfyres/pencil.png"), "Edit line Width", this);
		

		//add actions
		myMenu.addAction(actOpenSegEditor);
		myMenu.addSeparator();
		myMenu.addAction(actColorOptions);
		myMenu.addAction(actHideBranch);
		myMenu.addAction(actEditLineWidth);

		//connect actions
		connect(actOpenSegEditor, SIGNAL(triggered()),this, SLOT(openSegmentationEditor()) );
		connect(actColorOptions, SIGNAL(triggered()), this, SLOT(changeColor()));
		connect(actHideBranch, SIGNAL(triggered()), this, SLOT(hideBranch()));
		connect(actEditLineWidth, SIGNAL(triggered()), this, SLOT(editLineWidthAttributePlot()));

	}

	// restore plot
	//QAction *actRestorePlot = new QAction(QIcon(":/qtfyres/qtfyres/view-refresh-8.png"), "Restore plot", this);
	//connect(actRestorePlot, SIGNAL(triggered()), this, SLOT(hideBranch()));
	myMenu.addAction(ui.actRestoreDefaultColors);
	//reset axis
	QAction *actResetAxis = new QAction("Reset axis", this);
	myMenu.addAction(actResetAxis);
	connect(actResetAxis, SIGNAL(triggered()), this, SLOT(resetAxis()));

	myMenu.addSeparator();
	myMenu.addAction(ui.actionExportHeattreePlot);

	myMenu.exec(globalPos);
}

// SLOT
void QtfyMain::addPlotAttribute(QVector<QString>& _attributes, QString _chunkName)
{

	QVector<QString> alreadyPlottedAttr;
	//iterate through the selected attributes
	for(QVector<QString>::iterator attrIt = _attributes.begin(); attrIt != _attributes.end(); ++attrIt){
		QString currentAttr = (*attrIt);
		//check if attribute already plotted
		//MAYBE NOT GIVING THE USER THE OPTION OF ADDING ALREADY PLOTTED ATTRIBUTES FROM THE DIALOG
		
		//check if the attribute from this chunk is already plotted
		//for(QMap<QString, QString>::iterator it = addedAttributes.begin(); it != addedAttributes.end(); ++it){
		for(QMap<QString, myMdiSubWindow*>::iterator it = mSubwindows.begin(); it != mSubwindows.end(); ++it){
			QString attr = it.value()->getAttributeName();
			if(attr == currentAttr){
				QString chunk = it.value()->getAttributeChunkName();
				if( _chunkName == chunk)
					alreadyPlottedAttr.append(currentAttr);
			}
		}

		
		if(this->staggerWls){
			//update the tree model
			//Find the detection wlg
			int indexOfDetect = _chunkName.indexOf("Ch") + 2;
			QString detectionW = _chunkName.mid(indexOfDetect, 1);
			//is there already a tree model for this detection wlg?
			if(this->m_TreeModels.contains(detectionW)){//if yes
				//update the tree view
				this->m_TreeDataModel = m_TreeModels.value(detectionW);
				this->m_tvwDataTreeOverview->setModel(m_TreeModels.value(detectionW));
			}else{//if no make the new tree model and update the interface
				QList<QVariant> rootData;
				rootData << "Trees" ;
				TreeModel* newModel = new TreeModel(rootData);
				this->m_TreeDataModel = newModel;
				//initiliaze data
				this->m_TreeDataModel->setQuantificationInformation(_chunkName, detectionW);
				//this->m_TreeDataModel = new TreeModel(rootData);
				this->m_TreeDataModel->setIsCellBased(false);
				//Data parser
				QtfyTreeDataParser dataparser (m_TreeDataModel);
				dataparser.isStaggeringExperiment(this->staggerWls, detectionW);
				dataparser.CreateExperimentModel(trees);/*, _chunkName);*/
				//set cell and tree data stuctures
				this->setTimepointsPerCell(dataparser.getTimepointsPerCell());
				this->setCellsPerTree(dataparser.getCellsPerTree());
				this->setCellsPerTimepoint(dataparser.getCellsPerTimepoint());
				//add tree model to map
				m_TreeModels.insert(detectionW, newModel);
				this->m_tvwDataTreeOverview->setModel(m_TreeModels.value(detectionW));
				this->m_tvwDataTreeOverview->setHideMode(false);
				this->m_TreeDataModel->setIsIndexColourAvailable(false);

			}
		}
		if(alreadyPlottedAttr.contains(currentAttr))
			continue; 

		addedAttributes.insert(currentAttr, _chunkName);
		QPair<double, double> minMax;
		minMax = findMinMax(currentAttr, _chunkName);
		//key to global/manual maps the subwindow title
		QString subwindowTitle = _chunkName + ": " + currentAttr;
		globalMinMaxMap.insert(subwindowTitle, minMax);
		displayAttributePlot(currentAttr, _chunkName);
	}

	this->tileVerticallySubwindows();	

	//message if user selected already plotted attributes
	if(!alreadyPlottedAttr.isEmpty()){
		/*QString messageStr;
		if(alreadyPlottedAttr.size() > 1)
			messageStr = "Already plotted Attributes!";
		else
			messageStr = "Already plotted Attribute!";

		QMessageBox::warning (this,  "Attribute Warning", messageStr, QMessageBox::Ok);*/
	}

	synchronizePlots();

	//update viewers
	if (this->m_tvwDataTreeOverview->selectedIndexes().size() == 0)
		updateAllViewers(this->m_TreeDataModel->index(0,0));
    else
		updateAllViewers(this->m_tvwDataTreeOverview->selectedIndexes().at(0));
}

//SLOT
void QtfyMain::openSegmentationEditor()
{

	// initialize the correction ui settings
	CorrectionUISettings::getInst();

	// only if there is an attribute plotted
	if (this->addedAttributes.isEmpty()) {
		QString message = "No attribute has been plotted. Please first select an attribute to proceed.";
		QMessageBox::warning (this, "No attribute available!", message, QMessageBox::Ok);
		return;
	}

	// from quantification chunk 
	// find the selected view
	int segmentationWavelength = 2;
	int segmentationMethodID = 1; 

	myMdiSubWindow* theWindow = dynamic_cast<myMdiSubWindow*> (ui.mdiArea->activeSubWindow());
	QString chunkName = theWindow->getAttributeChunkName();
	int indexOfCurrentWindow = this->mSubwindows.values().indexOf(theWindow);
	QString attributeName = this->mSubwindows.keys().at(indexOfCurrentWindow);

	QString quantiWL = attributeName.right(2);

	bool conversionState = false;
	int quantiWLInt = quantiWL.toInt(&conversionState);
	
	int indexDw = chunkName.indexOf("DetectionCh");
	int indexSegm = chunkName.indexOf("SegMethod");

	QString detectionWL = chunkName.mid(indexDw,indexSegm-indexDw).remove("DetectionCh");
	QString segmentationMethod = chunkName.right(chunkName.length() - indexSegm).remove("SegMethod");

	bool conversionStateDetect = false;
	int detectionWLInt = detectionWL.toInt(&conversionStateDetect);

	bool conversionStateMethod = false;
	int segmentationMethodInt = segmentationMethod.toInt(&conversionStateMethod);

	if (conversionStateDetect && conversionStateMethod) {
		segmentationWavelength = detectionWLInt;
		segmentationMethodID = segmentationMethodInt;

		// add to the display
		if (!CorrectionUISettings::corrWavelengthsToDisplay.contains(detectionWLInt)) 
			CorrectionUISettings::corrWavelengthsToDisplay.append(detectionWLInt);
			
		if (!CorrectionUISettings::corrWavelengthsToDisplay.contains(quantiWLInt)) 
			CorrectionUISettings::corrWavelengthsToDisplay.append(quantiWLInt);
		

	}
	
	CorrectionUISettings::getInst()->updateDisplay();
	
	if (this->m_tvwDataTreeOverview->selectedIndexes().size() == 0)
		updateSelectedIndex(this->m_TreeDataModel->index(0,0));

	QString segEditorWindowTitle = "Segmentation Editor: " + chunkName;

	SegEditorMain* segEditor = new SegEditorMain(this, segmentationWavelength, segmentationMethodID, segEditorWindowTitle, this->staggerWls);
	
	connect(segEditor, SIGNAL(updateTreeModel(QString)), this, SLOT(updateTreeModelRequest(QString)));
	
	//signal to update index of the tree view in a staggering experiment
	connect(this, SIGNAL(treeviewIndexChangedStagger(const QModelIndex&, QString)), segEditor, SLOT(updateSelectedIndexStagger(const QModelIndex&, QString)));
	// when the index of the tree view changes in qtfy editor it should also change in segeditor
	connect (this, SIGNAL(treeviewIndexChanged(const QModelIndex&)), segEditor, SLOT(updateSelectedIndex(const QModelIndex&)));

	// when the index of the tree view changes in segeditor it should also change in qtfy editor
	connect (segEditor, SIGNAL(treeviewIndexChanged(const QModelIndex&)), this, SLOT(updateSelectedIndex(const QModelIndex&)));
	
	connect(segEditor,SIGNAL(treeviewIndexChangedStagger(const QModelIndex&, QString)), this, SLOT(updateSelectedIndexStagger(const QModelIndex&, QString)));
	// signal to update the message log
	connect(segEditor, SIGNAL(updateMsgLog(QString, QColor)), this, SLOT(updateMsgLog(QString, QColor)));
	// signal to update the plot for a cell
	connect(segEditor, SIGNAL(cellUpdated(ITrack*)), this, SLOT(updateCell(ITrack*)));
	//signal to update segEditor if activation status of cells has changed
	connect(this, SIGNAL(activationStatusChanged()), segEditor, SLOT(updateTpStatusFromQTFyEditor()));

	// set the path to the root folder that contains the experiments that contain the original images of the tracked trees
	segEditor->setExperimentsFilesFolder (this->pathToExperiments);
	// initialize the segmentationEditor with the current selection dataset
	segEditor->initializeDataQTFy(this->m_tvwDataTreeOverview, this->m_TreeDataModel);
	segEditor->setCellsPerTree(this->cellsPerTree);
	segEditor->setCellsPerTimepoint(this->cellsPerTimepoint);
	segEditor->setupInterface();


	segEditor->show();
	if(this->staggerWls)
		emit treeviewIndexChangedStagger(this->m_tvwDataTreeOverview->selectedIndexes().at(0), chunkName);
	else
		emit treeviewIndexChanged(this->m_tvwDataTreeOverview->selectedIndexes().at(0));

}

// SLOT
void QtfyMain::slotRtClickTreeOverview(const QPoint& pnt) 
{
	// for most widgets
	QPoint globalPos = this->m_tvwDataTreeOverview->viewport()->mapToGlobal(pnt);

	QMenu myMenu;
	QAction *actOpenSegEditor = new QAction(QIcon(":/qtfyres/qtfyres/seg.png"), "Open the Segmentation Editor", this);
	myMenu.addAction(actOpenSegEditor);
	connect(actOpenSegEditor, SIGNAL(triggered()),this, SLOT(openSegmentationEditor()) );
	myMenu.exec(globalPos);
}

void QtfyMain::displaySelectedTree(bool _widthChanged)
{

	ITree* currentTree;
	QString cellName = " ";
	bool successfullyPlotted = false;

	if(treeSetInformation.size() > 2 && this->currentTreeInPlots == treeSetInformation[1])//we are in the same tree as before, show cell
		cellName = treeSetInformation[2];

	//get the right tree
	for(QVector<QSharedPointer<ITree>>::const_iterator treeIterator = trees.constBegin(); treeIterator != trees.constEnd(); ++treeIterator)
	{	
		currentTree = (*treeIterator).data();
		QString c_tn = currentTree->getTreeName();
		if (currentTree->getTreeName() == treeSetInformation[1])
			break;
	}

	// the tree is already plotted but the user doesn't want to restore the default colors , check only if the x  axis time unit has changed
	if (this->currentTreeInPlots == treeSetInformation[1] && mSubwindows.contains("treeplot") && !this->restoreColors &&!_widthChanged){
		myMdiSubWindow* cSubwindow = mSubwindows.value("treeplot");
		QLayoutItem* item = cSubwindow->layout()->itemAt(0);
		QwtPlot* existingPlot = dynamic_cast<QwtPlot*>(item->widget());
		QString currTimeUnit = existingPlot->axisTitle(QwtPlot::xBottom).text();

		if(mTimeUnit == currTimeUnit)
			return;
	}

	// a new tree has been clicked or the user wants to restore the default color scheme to the plots
	// generate new tree plot
	
	// find subwindow that will contain the treeplot

	myMdiSubWindow* treesubwindow;
	QwtPlot* myTreePlot = new QwtPlot(this);
	QSize cSize(0,0);

	if(mSubwindows.contains("treeplot"))//existing subwindow
	{

		myMdiSubWindow* plotSubwindow = mSubwindows.value("treeplot");
		QLayoutItem* item = plotSubwindow->layout()->itemAt(0);
		QwtPlot* previousPlot = dynamic_cast<QwtPlot*>(item->widget());
		// get the size of the subwindow to replot it with the same size
		cSize = plotSubwindow->size();
		treesubwindow = plotSubwindow;

		// iterate through all curves and detach them
		previousPlot->detachItems();
		// delete old plot
		delete previousPlot;
			

	}
	else {
		// should I create a new tree plot 
		if (ui.actTreePlot->isChecked()){
			// creates a subwindow for the tree structure plot
			treesubwindow = new myMdiSubWindow(this);
			ui.mdiArea->addSubWindow(treesubwindow);
			mSubwindows.insert("treeplot", treesubwindow);
			treesubwindow->show();
		}
		else {
			// no tree plot will be displayed
			return;
		}
	}

	// create a new Tree Plot to add to the tree subwindow
	//get max generation
	int maxGeneration = currentTree->getTreeProperty(ITree::TREE_OPT_NUMBER_OF_GENERATIONS).toInt();

	// Get the cells of the tree
	QVector<ITrack*> cellList = TreeStructuralHelper::getTracksInGenerations(currentTree, 0, maxGeneration);
	TreePlot* aTree = new TreePlot(treeSetInformation, this, mTimeUnit);
	//set plotting options :: very important!!! that way colors always updated!
	aTree->setPlottingOptions(this->mPlottingOptions);
	myTreePlot = aTree->createPlot(cellList[0], maxGeneration, cellName);
	
	myTreePlot->setWindowTitle(treeSetInformation[1]);
	//add pop up menu
	myTreePlot->canvas()->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(myTreePlot->canvas(), SIGNAL(customContextMenuRequested(const QPoint&)),this,SLOT (treePlotRtClickContextMenu(const QPoint&)));
	//attack plot to subwindow
	treesubwindow->layout()->addWidget(myTreePlot);

	treesubwindow->setWindowTitle(treeSetInformation[1]);
	

	if(!ui.actAutoAlign->isChecked())
		treesubwindow->resize(ui.mdiArea->width(), ui.mdiArea->height()/mSubwindows.size());

	

	connect(treesubwindow, SIGNAL (closed( myMdiSubWindow*)), this, SLOT(closeSubwindow(myMdiSubWindow*)));
	connect (myTreePlot, SIGNAL(clickedPoint (const QwtDoublePoint &) ), this, SLOT (plotPointSelectedTreePlot(const QwtDoublePoint &)));
	
	myTreePlot->axisScaleDraw(QwtPlot::yLeft)->enableComponent(QwtAbstractScaleDraw::Labels, false);
	myTreePlot->axisScaleDraw(QwtPlot::yLeft)->enableComponent(QwtAbstractScaleDraw::Ticks, false);
	
	
}

//To do: check for cleaner code
void QtfyMain::displayAttributePlot( QString _attrName, QString _attrChunkName, QTFyPlot* _previousPlot, myMdiSubWindow* _plotSubWindow )
{

	//test make QPolugonF with all the points of the outliers
	ITree* currentTree;
	QVector<int> mRegardedGenerations;

	//get the right tree
	for(QVector<QSharedPointer<ITree>>::const_iterator treeIterator = trees.constBegin(); treeIterator != trees.constEnd(); ++treeIterator)
	{	
		currentTree = (*treeIterator).data();
		QString c_tn = currentTree->getTreeName();
		if (currentTree->getTreeName() == treeSetInformation[1])
			break;
	}

	qDebug() << treeSetInformation[1];

	// the tree and the attribute are already plotted 
	// do nothing
	//if (this->currentTreeInPlots == treeSetInformation[1] && _previousPlot)
		//return;


	// Get the cells of the tree
	int maxGeneration = currentTree->getTreeProperty(ITree::TREE_OPT_NUMBER_OF_GENERATIONS).toInt();
	for(int g = 0; g <= maxGeneration; g++)
		mRegardedGenerations<<g;

	QVector<ITrack*> cellList = TreeStructuralHelper::getTracksInGenerations(currentTree, 0, maxGeneration);

	//QwtPlot* myPlot;
	QTFyPlot* myPlot = new QTFyPlot(this);
	AttributePlot* mAttributePlot;
	QPair<double, double> minMax;

	//get the subwindow title
	QString subwindowTitle;
	if(_plotSubWindow == 0)
		subwindowTitle = _attrChunkName + ": " + _attrName;
	else
		subwindowTitle = _plotSubWindow->windowTitle();

	//set min and max y axis values
	if(!this->yAxisToLocalScale){
		if(globalMinMaxMap.contains(subwindowTitle))
			minMax = globalMinMaxMap[subwindowTitle];
	}else{
		minMax = findMinMax(_attrName, _attrChunkName);
	}

	QSize cSize(0,0);
	// the attribute plot subwindow
	myMdiSubWindow *attributesubwindow; 

	// it's the first time the plot is created
	// create the subwindow
	if(!_previousPlot)	{	

		// creates a subwindow for the quantification plot
		attributesubwindow = new myMdiSubWindow(ui.mdiArea);

		attributesubwindow->setWindowTitle(subwindowTitle);
		attributesubwindow->setAttributeChunkName(_attrChunkName);
		attributesubwindow->setAttributeName(_attrName);

		ui.mdiArea->addSubWindow(attributesubwindow);
		mSubwindows.insertMulti(_attrName, attributesubwindow);
		attributesubwindow->show();

		ui.mdiArea->setActiveSubWindow(attributesubwindow);

		connect(attributesubwindow, SIGNAL (closed( myMdiSubWindow*)), this, SLOT(closeSubwindow(myMdiSubWindow*)));
	}

	// a plot for this attribute already exists
	// delete the plot and get the subwindow
	if(_previousPlot) {
		// get the size of the subwindow to replot it with the same size
		cSize = _plotSubWindow->size();
		attributesubwindow = _plotSubWindow;

		// iterate through all curves and detach them
		_previousPlot->detachItems();
		// delete old plot
		delete _previousPlot;
	}


	// create the attribute plot for the new tree
	// and add it to the subwindow
	myPlot = mAttributePlot->createPlot(cellList, _attrName, attributesubwindow->getAttributeChunkName(), myPlot, this->mPlottingOptions, this->mTimeUnit, this->smoothLines, this->showOriginalPoints, this->sValue);

	myPlot->setWindowTitle(attributesubwindow->windowTitle());
	myPlot->canvas()->setContextMenuPolicy(Qt::CustomContextMenu);
	if(!this->yAxisToLocalScale){
		myPlot->setAxisScale(QwtPlot::yLeft, minMax.first, minMax.second);
	}

	if (this->manualXAxisSettings)
		myPlot->setAxisScale(QwtPlot::xBottom, this->minX, this->maxX);

	if (this->manualYAxisSettings){
		if(this->yManualMinMaxMap.contains(attributesubwindow->windowTitle())){
			QPair<double, double> pair = this->yManualMinMaxMap.value(subwindowTitle);
			double min = pair.first;
			double max = pair.second;
			myPlot->setAxisScale(QwtPlot::yLeft, min, max);
		}
	}

	//click options
	connect(myPlot->canvas(), SIGNAL(customContextMenuRequested(const QPoint&)),this,SLOT (plotRtClickContextMenu(const QPoint&)));
	connect (myPlot, SIGNAL(clickedPoint (const QwtDoublePoint &) ), this, SLOT (plotPointSelected(const QwtDoublePoint &)));

	//axis
	//set y axis for easy manual settings
	//myPlot->axisWidget(QwtPlot::yLeft)->setParent(attributesubwindow);
	myPlot->axisWidget(QwtPlot::yLeft)->setParent(myPlot);
	myPlot->setSubWindow(attributesubwindow);
	myPlot->axisWidget(QwtPlot::yLeft)->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(myPlot->axisWidget(QwtPlot::yLeft),  SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(plotRtClickYAxis(const QPoint&)));
	myPlot->axisWidget(QwtPlot::xBottom)->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(myPlot->axisWidget(QwtPlot::xBottom), SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(plotRtClickXAxis(const QPoint&)));

	int tmplength = myPlot->axisScaleDraw(QwtPlot::yLeft)->length();
	if(tmplength > yAxisLength)
		yAxisLength = tmplength;
	
	attributesubwindow->layout()->addWidget(myPlot);
	int windowSize = this->addedAttributes.size() + 1;

	if(!ui.actAutoAlign->isChecked())
		attributesubwindow->resize(ui.mdiArea->width(), ui.mdiArea->height()/mSubwindows.size()); 

	//if Outlier Detection selected, plot the outliers
	if(this->OutlierDetection && !this->deactivateOutliers)
	{
		//set thresholds
		mOutlierManager->setDetectionThreshold(this->dValue);
		mOutlierManager->setSmoothingThreshold(this->sValue);
		
		mOutlierManager->setMinMax(minMax.first, minMax.second);

		if(mOutlierManager->findOutliers(_previousPlot, _attrName,_attrChunkName, currentTree, mRegardedGenerations)){
			QPolygonF outliers = mOutlierManager->getOutliersOfTree(currentTree);
			//createOutliers(myPlot, outliers);
			mOutlierManager->showOutliers(myPlot, currentTree);
		}
	}

	myPlot->replot();
}

void QtfyMain::plotPointSelected(const QwtDoublePoint &_pnt)
{
	
	QTFyPlot *myPlot = (QTFyPlot *) sender();

	//Update subwindow focus if staggering experiment
	QString chunkName;
	if(this->staggerWls){
		QString currWindowTitle = myPlot->windowTitle();
		QStringList list = currWindowTitle.split(":");
		if(list.size()>1){
			chunkName = list[0];
			this->setWindowFocus(currWindowTitle);
		}
	}

	QModelIndex newIndex = findClickedIndexFromPlot(myPlot, _pnt, true);

	if (!newIndex.isValid())
		return;

	updateSelectedIndex(newIndex);

	
	if(this->staggerWls)
		emit treeviewIndexChangedStagger(newIndex, chunkName);
	else
		emit treeviewIndexChanged(newIndex);

	this->m_tvwDataTreeOverview->setFocus();
}

QModelIndex QtfyMain::findClickedIndexFromPlot (const QwtPlot* _myPlot, const QwtDoublePoint &_pnt, bool _showTimepoint)
{
	QwtPlotItemList listOfPlotItems = _myPlot->itemList();

	QwtPlotCurve* closestCurve = 0;

	//find the tree
	ITree* currentTree;
	for(QVector<QSharedPointer<ITree>>::const_iterator treeIterator = trees.constBegin(); treeIterator != trees.constEnd(); ++treeIterator) {	
		currentTree = (*treeIterator).data();
		QString c_tn = currentTree->getTreeName();
		if (currentTree->getTreeName() == treeSetInformation[1])
			break;
	}

	double distThreshold = 10; // distance of pix from selected timepoint
	double  minDist = distThreshold;
	int closestCurvePoint = -1;

	for (QwtPlotItemList::iterator it = listOfPlotItems.begin(); it != listOfPlotItems.end(); ++it) { 
		// find the type of the plot item	
		int type = (*it)->rtti();
		//look for the closest point for curves only
		if (type == QwtPlotItem::Rtti_PlotCurve) {
			
			QwtPlotCurve* currentCurve = (QwtPlotCurve*) (*it);
			QString curveTitle = currentCurve->title().text();

			// if the curve is transparent don't consider it for a match
			QPen curvePen = currentCurve->pen();
			if(curvePen.color().alpha() == 0)
				continue;

			if(curveTitle.contains("O"))
				continue;

			if(curveTitle.contains("P")){
				QwtSymbol newSymbol = currentCurve->symbol();
				if(newSymbol.brush().color().alpha() == 0)
					continue;
			}

			double* dist = new double();

			PlotPickerHelp helper;
			int closestPointOfCurve = helper.findClosestCurvePointByPixelDistance(_myPlot, currentCurve, _pnt, dist);

			// we require the minimum distance to be smaller than distThreshold
			// that is the user should have clicked closer enough to the timepoint
			if (*dist < minDist && *dist < distThreshold) {
				minDist = *dist;
				closestCurve = currentCurve;
				closestCurvePoint = closestPointOfCurve;
			}
		}			
	}

	// a curve has been found	
	if (closestCurvePoint > -1) {

		//if closest curve transparent, don't show menu
		QPen newPen = closestCurve->pen();
		if(newPen.color().alpha() == 0)
			return QModelIndex();

		//adjust line width
		//int lineWidth = this->mPlottingOptions->getLineWidthForAttributePlot(currentTree);
		int lineWidth = this->mPlottingOptions->getLineWidthForAttributePlot();
		newPen.setWidth(lineWidth + 2);
		closestCurve->setPen(newPen);

		QString cellName = closestCurve->title().text();

		//get correct cell name from smooth or point curves
		if(cellName.contains("S"))
			cellName.remove("S");
		else if (cellName.contains("P"))
			cellName.remove("P");

		int cellNum = cellName.toInt();

		// get the timepoint that corresponds to the closest curve point
		// it might not be the same when the curve does not contain all timepoints
		// i.e. in the case where the quantification of a timepoint has failed
		int closestTimepoint = -1;
		double yValAtClosestPoint = closestCurve->y(closestCurvePoint);
		double xValAtClosestPoint = closestCurve->x(closestCurvePoint);
		double roundedXVal=static_cast<double>(static_cast<int>(xValAtClosestPoint*100+0.5))/100.0;

		ITrack* currentCell = this->cellsPerTree.value(this->currentTreeInPlots).value(cellName);
		if (!currentCell)
			return QModelIndex();

		QMap<int, ITrackPoint*> trackpointRange = currentCell->getTrackPointsRange();

		int index = 0;
		foreach (int trackpoint, trackpointRange.keys()) {
			int preIncTime = (Experiment::isUsePreincubationTime()) ? currentCell->getITree()->getTreeProperty(ITree::TREE_PREINCUBATION_TIME).toInt() : 0;
			int firstCellEvent = currentCell->getAbsoluteSecondsForTP(currentCell->getFirstTimePoint());
			int cellSec = currentCell->getSecondsForTimePoint(trackpoint);
			double timepointTime = (preIncTime + firstCellEvent + cellSec) * this->getTimeFactor();

			// round value to 2 decimal
			double roundedTime=static_cast<double>(static_cast<int>(timepointTime*100+0.5))/100.0;
			
			if (roundedTime == roundedXVal) 
				closestTimepoint = trackpointRange.value(trackpoint)->getTimePoint();
			
			index ++;
		}

		if (closestTimepoint == -1)
			return QModelIndex();

		QModelIndex selectedIndex;
		QModelIndex newIndex;

		// find the index that is currently selected at the tree view
		// use it as a hint for the search of appropriate index (see method findIndex)
		// if no index is selected use the first index
		if (this->m_tvwDataTreeOverview->selectedIndexes().size() == 0)
			selectedIndex = this->m_TreeDataModel->index(0,0);
		else
			selectedIndex  = this->m_tvwDataTreeOverview->selectedIndexes().at(0);

		// find the new index to update the selection of the tree view
		newIndex = this->findIndexOfTimepoint (selectedIndex, cellName, closestTimepoint);

		if (!newIndex.isValid())
			return QModelIndex();

		TreeItem *selectedItem = static_cast<TreeItem*>(newIndex.internalPointer());
		//update message log
		if(_showTimepoint){
			
			if (selectedItem->column() == 3){
				QString tp = selectedItem->data(0).toString(); //timepoint name (number)
				QString cll = selectedItem->parent()->data(0).toString();
				QString message =  cll + ", " + tp;
				QMainWindow::statusBar()->showMessage(message);
			}
		}

		QString treeName;
		if (selectedItem->column() == 3){
			treeName = selectedItem->parent()->parent()->data(0).toString();//tree name
		}

		for(QVector<QSharedPointer<ITree>>::iterator i = trees.begin(); i != trees.end(); ++i){
			ITree* currentTree = i->data();

			if(treeName == currentTree->getTreeName())
				selectedCell = currentTree->getTrackByNumber(cellNum);

		}

		return newIndex;
	}
	return QModelIndex();
}

QModelIndex QtfyMain::findIndexOfTimepoint (const QModelIndex &_currentIndex, QString _cellID, int _timepointRow)
{
	TreeItem *item = static_cast<TreeItem*>(_currentIndex.internalPointer());
	
	QModelIndexList cells;
	QModelIndexList timepoints;
	QModelIndex newIndex;

	QModelIndex start;
	// find if the user has previously selected an experiment, tree, cell or timepoint
	// and accordingly define the starting point for the search
	switch (item->column()) {
	case 0:  // experiment
		// it's the first tree
		start = _currentIndex.child(0,0).child(0,0);
		break;
	case 1: // tree
		start = _currentIndex.child(0,0);
		break;
	case 2: // cell
		start = _currentIndex.parent();
		break;
	case 3: // timepoint
		start = _currentIndex.parent().parent();
		break;
	default:
		break;
	}

	QString cell = "Cell_" + _cellID;
	QString timepoint = "Timepoint_" + QString::number(_timepointRow);
	cells = this->m_TreeDataModel->match(start, Qt::DisplayRole,  QVariant::fromValue(cell), 1, Qt::MatchRecursive);
	if(cells.size() > 0)
		start = cells.at(0);
	timepoints = this->m_TreeDataModel->match(start, Qt::DisplayRole,  QVariant::fromValue(timepoint), 1, Qt::MatchRecursive);
	
	//if (cells.size() > 0) {
	//	newIndex = cells.at(0).child(_timepointRow,0);
	//	return newIndex;	
	//}
	if(timepoints.size()>0){
		newIndex = timepoints.at(0);
		return newIndex;
	}
	else {
		//qDebug() << "Timepoint not found";
		return QModelIndex();
	}
}

/************************************************************************/
/* updates the selected index of the treeview                           */
/************************************************************************/
void QtfyMain::updateSelectedIndex(const QModelIndex& _selectedIndex)
{
	this->m_tvwDataTreeOverview->setCurrentIndex(_selectedIndex);
	this->updateAllViewers(_selectedIndex);
}

void QtfyMain::updateSelectedIndexStagger(const QModelIndex& _selectedIndex, QString _chunk)
{
	//Find the detection wlg
	int indexOfDetect = _chunk.indexOf("Ch") + 2;
	QString detectionW = _chunk.mid(indexOfDetect, 1);
	//update the tree model
	this->m_TreeDataModel = m_TreeModels.value(detectionW);
	this->updateAllViewers(_selectedIndex);
	//this->m_tvwDataTreeOverview->setCurrentIndex(_selectedIndex);
	//this->m_tvwDataTreeOverview->setCurrentIndex(newIndex);
}

/************************************************************************/
/* smooth lines functions						                        */
/************************************************************************/
//SLOT
void QtfyMain::smoothLinesSelected(bool checkState)
{
	if(checkState) {
		this->smoothLines = true;
		sValue = ui.spbSmoothCurves->value();
	}
	else{
		this->smoothLines = false;
	}


	QApplication::setOverrideCursor(Qt::WaitCursor);

	ITree* currentTree;
	for(QVector<QSharedPointer<ITree>>::const_iterator treeIterator = trees.constBegin(); treeIterator != trees.constEnd(); ++treeIterator) {	
		currentTree = (*treeIterator).data();
		QString c_tn = currentTree->getTreeName();
		if (currentTree->getTreeName() == treeSetInformation[1])
			break;
	}
	//get the tree generations
	int maxGeneration = currentTree->getTreeProperty(ITree::TREE_OPT_NUMBER_OF_GENERATIONS).toInt();
	QVector<int> mRegardedGenerations;
	for(int g = 0; g <= maxGeneration; g++){
		mRegardedGenerations<<g;
	}
	//get the cells of the tree
	QVector<ITrack*> cellList = TreeStructuralHelper::getTracksInGenerations(currentTree, 0, maxGeneration);
	int width = this->mPlottingOptions->getLineWidthForAttributePlot();
	//int width = this->mPlottingOptions->getLineWidthForAttributePlot(currentTree);
	//for every attribute plot, smooth the cell curves
	//iterate through plots
	for(QMap<QString, myMdiSubWindow*>::Iterator it = mSubwindows.begin(); it != mSubwindows.end(); ++it){
		// find current subwindow
		myMdiSubWindow* cSubwindow = it.value();
		QLayoutItem* item = cSubwindow->layout()->itemAt(0);

		// get the plot
		QTFyPlot* plot = dynamic_cast<QTFyPlot*>(item->widget());

		if(!plot)
			continue;

		//if tree plot, continue
		if(it.key() == "treeplot")
			continue;

		QString attrName = it.key();
		QString quantChunkName = cSubwindow->getAttributeChunkName();

		//fix x factor
		float xFactor = getTimeFactor();

		AttributePlot* test;
		
		QVector<QVector<QPointF>> points = test->getValues(cellList, attrName, quantChunkName, xFactor);
		//For each cell create a new curve
		for (uint i = 0; i < points.size(); i++) {

			ITrack* currCell = cellList[i];
			// Generate a color
			QColor c = this->mPlottingOptions->getCellColor(currCell);

			if(points[i].isEmpty())
				continue;

			//detach previous original curve
			plot->detachCurve(QString::number(currCell->getTrackNumber()));
			//detach previous smoothed curve
			plot->detachCurve("S" + QString::number(currCell->getTrackNumber()));

			// Create curve
			QwtPlotCurve* curve;
			if(this->smoothLines)				
				curve = test->createSmoothedCurve(points[i],"S" + QString::number(currCell->getTrackNumber()), this->sValue, c,width);
			else
				curve = test->createOriginalCurve(QString::number(currCell->getTrackNumber()), c, points[i], currCell, width);

			if(!curve)
				continue;
			curve->attach(plot);
		}

	}

	//update the attribute plots
	if(this->mustReplot)
		updatePlots();

	QApplication::restoreOverrideCursor();
}
//SLOT
void QtfyMain::updateSmoothingThres(int sValue)
{
	this->setSmoothingValue(sValue);
	this->smoothLinesSelected(this->smoothLines);
}

void QtfyMain::setSmoothingValue(int value)
{
	this->sValue = value;
}

/************************************************************************/
/* outlier detection functions						                    */
/************************************************************************/
//SLOT
void QtfyMain::outlierSelected(bool checkState)
{
	if(checkState)
	{
		this->OutlierDetection = true;
		dValue = ui.spbOutlierDetection->value();
	}

	else 
		this->OutlierDetection = false;

	//if outlier detection = true and smooth lines = false, smooth the lines first
	if(OutlierDetection && !this->smoothLines)
		ui.chkSmoothLines->setChecked(true);

	//get current tree
	ITree* currentTree;
	for(QVector<QSharedPointer<ITree>>::const_iterator treeIterator = trees.constBegin(); treeIterator != trees.constEnd(); ++treeIterator)
	{	
		currentTree = (*treeIterator).data();
		QString c_tn = currentTree->getTreeName();
		if (currentTree->getTreeName() == treeSetInformation[1])
			break;
	}
	//get the tree generations
	int maxGeneration = currentTree->getTreeProperty(ITree::TREE_OPT_NUMBER_OF_GENERATIONS).toInt();
	QVector<int> mRegardedGenerations;
	for(int g = 0; g <= maxGeneration; g++){
		mRegardedGenerations<<g;
	}
	//get the cells of the tree
	QVector<ITrack*> cellList = TreeStructuralHelper::getTracksInGenerations(currentTree, 0, maxGeneration);
	QApplication::setOverrideCursor(Qt::WaitCursor);
	if(OutlierDetection){
		
		//set thresholds
		mOutlierManager->setDetectionThreshold(this->dValue);
		mOutlierManager->setSmoothingThreshold(this->sValue);


		//iterate through plots
		for(QMap<QString, myMdiSubWindow*>::Iterator it = mSubwindows.begin(); it != mSubwindows.end(); ++it){
			// find current subwindow
			myMdiSubWindow* cSubwindow = it.value();
			QLayoutItem* item = cSubwindow->layout()->itemAt(0);

			// get the plot
			QTFyPlot* plot = dynamic_cast<QTFyPlot*>(item->widget());

			if(!plot)
				continue;

			//if tree plot, continue
			if(it.key() == "treeplot")
				continue;

			//if already plotted outliers, remove the previous ones from the plot
			foreach(ITrack* cell, cellList){
				QString cellName = QString::number(cell->getTrackNumber());
				plot->detachCurve("O"+cellName);
			}

			//get the attribute name
			QString attrName = it.key();
			QString quantChunkName = cSubwindow->getAttributeChunkName();

			//set min, max
			QPair<double, double> minMax;

			//get subwindow title
			QString subwindowTitle = cSubwindow->windowTitle();

			if(!this->yAxisToLocalScale){
				if(globalMinMaxMap.contains(subwindowTitle))
					minMax = globalMinMaxMap[subwindowTitle];
			}else{
				minMax = findMinMax(attrName, quantChunkName);
			}
			mOutlierManager->setMinMax(minMax.first, minMax.second);

			//find outliers
			if(mOutlierManager->findOutliers(plot, attrName, quantChunkName, currentTree, mRegardedGenerations)){
				//get outlier points
				QPolygonF outliers = mOutlierManager->getOutliersOfTree(currentTree);

				//plot outliers
				mOutlierManager->showOutliers(plot, currentTree);
			}

		}
	}else{ //iterate through plots and detach the outliers curve
		//iterate through plots
		for(QMap<QString, myMdiSubWindow*>::Iterator it = mSubwindows.begin(); it != mSubwindows.end(); ++it){
			// find current subwindow
			myMdiSubWindow* cSubwindow = it.value();
			QLayoutItem* item = cSubwindow->layout()->itemAt(0);

			// get the plot
			QTFyPlot* plot = dynamic_cast<QTFyPlot*>(item->widget());

			if(!plot)
				continue;

			//if tree plot, continue
			if(it.key() == "treeplot")
				continue;


			//if already plotted outliers, remove the previous ones from the plot
			foreach(ITrack* cell, cellList){
				QString cellName = QString::number(cell->getTrackNumber());
				plot->detachCurve("O"+cellName);
			}
		}
	}

	//iterate through plots to replot them at the same time
	for(QMap<QString, myMdiSubWindow*>::Iterator it = mSubwindows.begin(); it != mSubwindows.end(); ++it){
		// find current subwindow
		myMdiSubWindow* cSubwindow = it.value();
		QLayoutItem* item = cSubwindow->layout()->itemAt(0);

		// get the plot
		QTFyPlot* plot = dynamic_cast<QTFyPlot*>(item->widget());

		if(!plot)
			continue;

		//if tree plot, continue
		if(it.key() == "treeplot")
			continue;

		plot->replot();
	}
	
	QApplication::restoreOverrideCursor();
}

void QtfyMain::setDetectionValue(int value)
{
	this->dValue = value;
}
//SLOT
void QtfyMain::updateDetectionThres(int dValue)
{	
	this->setDetectionValue(dValue);
	this->outlierSelected(this->OutlierDetection);
}

QPoint QtfyMain::transformPointToPixelCoords(const QwtPlot* _plot, const QPointF &_pnt) const
{
	// transform the coordinates -- this gives pixels
	QwtScaleMap xMap(_plot->canvasMap(QwtPlot::xBottom));
	QwtScaleMap yMap(_plot->canvasMap(QwtPlot::yLeft));			

	QPoint transfCoord (xMap.transform(_pnt.x()), yMap.transform(_pnt.y())) ;

	return transfCoord;
}

//SLOT
void QtfyMain::showPointsSelected(bool checkState)
{
	if(!checkState)
		showOriginalPoints = false;
	else{
		showOriginalPoints = true;
	}

	//get current tree
	ITree* currentTree;
	for(QVector<QSharedPointer<ITree>>::const_iterator treeIterator = trees.constBegin(); treeIterator != trees.constEnd(); ++treeIterator)
	{	
		currentTree = (*treeIterator).data();
		QString c_tn = currentTree->getTreeName();
		if (currentTree->getTreeName() == treeSetInformation[1])
			break;
	}
	//get the tree generations
	int maxGeneration = currentTree->getTreeProperty(ITree::TREE_OPT_NUMBER_OF_GENERATIONS).toInt();
	QVector<int> mRegardedGenerations;
	for(int g = 0; g <= maxGeneration; g++){
		mRegardedGenerations<<g;
	}
	//get the cells of the tree
	QVector<ITrack*> cellList = TreeStructuralHelper::getTracksInGenerations(currentTree, 0, maxGeneration);

	//test
	int width = this->mPlottingOptions->getLineWidthForAttributePlot();
	//int width = this->mPlottingOptions->getLineWidthForAttributePlot(currentTree);

	//for every attribute plot, smooth the cell curves
	//iterate through plots
	for(QMap<QString, myMdiSubWindow*>::Iterator it = mSubwindows.begin(); it != mSubwindows.end(); ++it){
		// find current subwindow
		myMdiSubWindow* cSubwindow = it.value();
		QLayoutItem* item = cSubwindow->layout()->itemAt(0);

		// get the plot
		QTFyPlot* plot = dynamic_cast<QTFyPlot*>(item->widget());

		if(!plot)
			continue;

		//if tree plot, continue
		if(it.key() == "treeplot")
			continue;

		QString attrName = it.key();
		QString quantChunkName = cSubwindow->getAttributeChunkName();
		float xFactor = getTimeFactor();

		AttributePlot* test;
		QVector<QVector<QPointF>> points = test->getValues(cellList, attrName, quantChunkName, xFactor);
		//For each cell create a new curve
		for (uint i = 0; i < points.size(); i++) {

			if(!showOriginalPoints){
				plot->detachCurve("P"+QString::number(cellList[i]->getTrackNumber()));
				continue;
			}
			// Generate a color
			QColor c = this->mPlottingOptions->getCellColor(cellList[i]);

			if(points[i].isEmpty())
				continue;


				// Create curve
				QwtPlotCurve* curve = test->plotOriginalPoints("P"+QString::number(cellList[i]->getTrackNumber()), c, points[i], cellList[i], width);
				curve->attach(plot);

				//reattach outlier curve
				if(this->OutlierDetection){
					QwtPlotCurve* outlier = plot->getCurve("O"+QString::number(cellList[i]->getTrackNumber()));
					if(outlier){
						plot->detachCurve("O"+QString::number(cellList[i]->getTrackNumber()));
						outlier->attach(plot);
					}
				}
					
		}
		if(this->mustReplot)
			plot->replot();
	}
}

void QtfyMain::tileVerticallySubwindows()
{
	if(!ui.mdiArea->subWindowList().isEmpty()){

		QPoint position(0, 0);
		foreach (QMdiSubWindow *window, mSubwindows/*ui.mdiArea->subWindowList()*/) {
			window->setWindowState(Qt::WindowNoState);
			QRect rect(0, 0, ui.mdiArea->width(), ui.mdiArea->height() / mSubwindows.size()/*ui.mdiArea->subWindowList().count()*/);
			window->setGeometry(rect);
			window->move(position);
			position.setY(position.y() + window->height());
		}
	}
}

//SLOT
void QtfyMain::autoAlignSelected()
{
	this->isAutoAlign = true;
	tileVerticallySubwindows();
}

void QtfyMain::closeSubwindow( myMdiSubWindow* _subwindow )
{

	//if subwindow closed is not the tree plot subwindow
	if(_subwindow->windowTitle() != treeSetInformation[1]){
		//also remove attribute from addedAttributes
		for(QMap<QString, QString>::iterator i = addedAttributes.begin(); i != addedAttributes.end(); ++i) {
			QString attrName = i.key();
			QString chunkName = i.value();
			if(_subwindow->getAttributeName() == attrName) {
				if(_subwindow->getAttributeChunkName() == chunkName){
					// create updated attribute plot for current subwindow
					addedAttributes.erase(i);
					emit attributeRemoved(chunkName, attrName);
					break;
				}
			}
		}
	}else{ //erase treeplot from mSubwindows
		//mSubwindows.remove("treeplot");
		ui.actTreePlot->setChecked(false);
		return;
	}

	//erase subwindow 
	for(QMap<QString, myMdiSubWindow*>::iterator j = mSubwindows.constBegin(); j != mSubwindows.constEnd(); ++j) {
		QString title = j.value()->windowTitle();
		if(_subwindow->windowTitle() == title){
			mSubwindows.erase(j);
			break;
		}

	}

	//synchronize plots
	this->synchronizePlots();

	if (this->m_tvwDataTreeOverview->selectedIndexes().size() == 0)
		updateAllViewers(this->m_TreeDataModel->index(0,0));
	else
		updateAllViewers(this->m_tvwDataTreeOverview->selectedIndexes().at(0));

	this->refreshAttributePlots();

}

//SLOT
void QtfyMain::deactivateOutliersSelected()
{
	this->mustReplot = false;
	bool outliersFound = false;

	QApplication::setOverrideCursor(Qt::WaitCursor);
	//get current tree
	ITree* currentTree;
	for(QVector<QSharedPointer<ITree>>::const_iterator treeIterator = trees.constBegin(); treeIterator != trees.constEnd(); ++treeIterator)
	{	
		currentTree = (*treeIterator).data();
		QString c_tn = currentTree->getTreeName();
		if (currentTree->getTreeName() == treeSetInformation[1])
			break;
	}

	int maxGeneration = currentTree->getTreeProperty(ITree::TREE_OPT_NUMBER_OF_GENERATIONS).toInt();
	QVector<int> mRegardedGenerations;
	for(int g = 0; g <= maxGeneration; g++){
		mRegardedGenerations<<g;
	}
	QVector<ITrack*> cellList = TreeStructuralHelper::getTracksInGenerations(currentTree, 0, maxGeneration);
	//set thresholds
	mOutlierManager->setDetectionThreshold(this->dValue);
	mOutlierManager->setSmoothingThreshold(this->sValue);
		
	//iterate through plots
	for(QMap<QString, myMdiSubWindow*>::Iterator it = mSubwindows.begin(); it != mSubwindows.end(); ++it){
		// find current subwindow
		myMdiSubWindow* cSubwindow = it.value();
		QLayoutItem* item = cSubwindow->layout()->itemAt(0);

		// get the plot
		QTFyPlot* plot = dynamic_cast<QTFyPlot*>(item->widget());

		if(!plot)
			continue;

		//if tree plot, continue
		if(it.key() == "treeplot")
			continue;

		//if already plotted outliers, remove the previous ones from the plot
		foreach(ITrack* cell, cellList){
			QString cellName = QString::number(cell->getTrackNumber());
			plot->detachCurve("O"+cellName);
		}

		//get the attribute name
		QString attrName = it.key();
		QString quantChunkName = cSubwindow->getAttributeChunkName();
		//set min, max
		QPair<double, double> minMax;

		//get subwindow title
		QString subwindowTitle = cSubwindow->windowTitle();
		if(!this->yAxisToLocalScale){
			if(globalMinMaxMap.contains(subwindowTitle))
				minMax = globalMinMaxMap[subwindowTitle];
		}else{
			minMax = findMinMax(attrName, quantChunkName);
		}
		mOutlierManager->setMinMax(minMax.first, minMax.second);

		//find outliers
		if(mOutlierManager->findOutliers(plot, attrName, quantChunkName, currentTree, mRegardedGenerations)){
			//get outlier points
			mOutlierManager->deactivateOutliers(attrName, currentTree, quantChunkName);
			outliersFound = true;
		}

	}

	//update original points
	if(this->showOriginalPoints){
		this->showPointsSelected(false); //delete previous points
		this->showPointsSelected(true); //plot new points
	}else{
		this->showPointsSelected(false);
	}

	//update smooth curves
	this->smoothLinesSelected(this->smoothLines);

	ui.chkOutlierDetection->setChecked(false);

	this->updatePlots();
	this->mustReplot = true;

	//check if "Deactivate outliers in all selected trees" is checked
	if(ui.optDeactiveOutliersForTreeset->isChecked())
		this->applyToAll(currentTree);

	QApplication::restoreOverrideCursor();

	if(outliersFound)
		emit activationStatusChanged();
}

void QtfyMain::applyToAll(ITree* _selectedTree)
{

	//get current tree
	ITree* currentTree;
	for(QVector<QSharedPointer<ITree>>::const_iterator treeIterator = trees.constBegin(); treeIterator != trees.constEnd(); ++treeIterator)
	{	
		currentTree = (*treeIterator).data();
		//skip the selected tree because changes are already made for it
		if(currentTree->getTreeName() == _selectedTree->getTreeName())
			continue;

		int maxGeneration = currentTree->getTreeProperty(ITree::TREE_OPT_NUMBER_OF_GENERATIONS).toInt();
		QVector<int> mRegardedGenerations;
		for(int g = 0; g <= maxGeneration; g++){
			mRegardedGenerations<<g;
		}
		QVector<ITrack*> cellList = TreeStructuralHelper::getTracksInGenerations(currentTree, 0, maxGeneration);
		//set thresholds
		mOutlierManager->setDetectionThreshold(this->dValue);
		mOutlierManager->setSmoothingThreshold(this->sValue);
		
		//iterate through plots
		for(QMap<QString, myMdiSubWindow*>::Iterator it = mSubwindows.begin(); it != mSubwindows.end(); ++it){
			// find current subwindow
			myMdiSubWindow* cSubwindow = it.value();
			QLayoutItem* item = cSubwindow->layout()->itemAt(0);

			// get the plot
			QTFyPlot* plot = dynamic_cast<QTFyPlot*>(item->widget());

			if(!plot)
				continue;

			//if tree plot, continue
			if(it.key() == "treeplot")
				continue;

			////if already plotted outliers, remove the previous ones from the plot
			//foreach(ITrack* cell, cellList){
			//	QString cellName = QString::number(cell->getTrackNumber());
			//	plot->detachCurve("O"+cellName);
			//}

			//get the attribute name
			QString attrName = it.key();
			QString quantChunkName = cSubwindow->getAttributeChunkName();
			////set min, max
			//QPair<double, double> minMax;

			////get subwindow title
			//QString subwindowTitle = cSubwindow->windowTitle();
			//if(!this->yAxisToLocalScale){
			//	if(globalMinMaxMap.contains(subwindowTitle))
			//		minMax = globalMinMaxMap[subwindowTitle];
			//}else{
			//	minMax = findMinMax(attrName, quantChunkName);
			//}
			//mOutlierManager->setMinMax(minMax.first, minMax.second);

			//find outliers
			if(mOutlierManager->findOutliers(plot, attrName, quantChunkName, currentTree, mRegardedGenerations)){
				//get outlier points
				mOutlierManager->deactivateOutliers(attrName, currentTree, quantChunkName);
				//outliersFound = true;
			}

		}

		////update original points
		//if(this->showOriginalPoints){
		//	this->showPointsSelected(false); //delete previous points
		//	this->showPointsSelected(true); //plot new points
		//}else{
		//	this->showPointsSelected(false);
		//}

		////update smooth curves
		//this->smoothLinesSelected(this->smoothLines);

		//ui.chkOutlierDetection->setChecked(false);

		//this->updatePlots();
		//this->mustReplot = true;
	}
	////if there are no attributes to deactivate, do nothing
	//if(addedAttributes.isEmpty()){
	//	QMessageBox::warning (this,  "QTFy warning", "There is no attribute selected, to deactivate any outliers!", QMessageBox::Ok);
	//	return;
	//}

	//this->deactivateOutliers = true;
	//ITree* currentTree;
	//QVector<int> mRegardedGenerations;

	////for all the trees of the treset
	//for(QVector<QSharedPointer<ITree>>::const_iterator itTree = trees.constBegin(); itTree != trees.constEnd(); ++itTree){
	//	//deactivate the outliers of the given threshold
	//	setCursor(Qt::BusyCursor);
	//	currentTree = itTree->data();
	//	//set up the current tree
	//	this->treeSetInformation.clear();
	//	treeSetInformation.append(currentTree->getExperimentName()); //experiment name
	//	treeSetInformation.append(currentTree->getTreeName()); //tree name

	//	int maxGeneration = (*itTree)->getTreeProperty(ITree::TREE_OPT_NUMBER_OF_GENERATIONS).toInt();
	//
	//	for(int g = 0; g <= maxGeneration; g++){//regarded generations
	//		mRegardedGenerations<<g;
	//	}

	//	// iterate through available subwindows
	//	for(QMap<QString, myMdiSubWindow*> ::iterator it = mSubwindows.constBegin(); it != mSubwindows.constEnd(); ++it)
	//	{
	//		// find current subwindow
	//		myMdiSubWindow* cSubwindow = it.value();
	//		QLayoutItem* item = cSubwindow->layout()->itemAt(0);
	//		// get the plot
	//		QTFyPlot* previousPlot = dynamic_cast<QTFyPlot*>(item->widget());

	//		//if not the treeplot
	//		if(it.key() != "treeplot"){
	//			//if the user has added attributes
	//			if(!addedAttributes.empty()) {

	//				// find the attribute of subwindow
	//				//for(QMap<QString, QString>::iterator i = addedAttributes.begin(); i != addedAttributes.end(); ++i) {
	//					//QString attrName = (*i);
	//				QString attrName = it.key();
	//				QString quantChunkName = cSubwindow->getAttributeChunkName();

	//					//if(it.key() == attrName) {
	//						// create updated attribute plot for current subwindow
	//					
	//						//set thresholds
	//						mOutlierManager->setDetectionThreshold(this->dValue);
	//						mOutlierManager->setSmoothingThreshold(this->sValue);

	//						mOutlierManager->findOutliers(previousPlot, attrName, quantChunkName, currentTree, mRegardedGenerations);
	//						mOutlierManager->deactivateOutliers(attrName, currentTree);
	//					
	//					//}
	//				//}
	//			}
	//		}
	//	}
	//	mRegardedGenerations.clear();
	//}

	////update viewers
	//if (this->m_tvwDataTreeOverview->selectedIndexes().size() == 0)
	//	updateAllViewers(this->m_TreeDataModel->index(0,0));
	//else
	//	updateAllViewers(this->m_tvwDataTreeOverview->selectedIndexes().at(0));


	//this->deactivateOutliers = false;
	
}

QPair<double, double> QtfyMain::findMinMax(QString _attrName, QString _quantChunkName)
{

	QPair<double,double> minMaxPair;
	double min = 10000;
	double max = 0;

	
	////find min-max for all the cells of the treeset
	for(int j = 0; j<tracks.size(); j++){
		
		QMap<int, QVariant> data;
		data = QTFyAttributeTools::getTimedValue(*tracks[j], _attrName, _quantChunkName);

		for(QMap<int, QVariant>::const_iterator i = data.constBegin(); i != data.constEnd(); ++i){
			double currentValue = i.value().toDouble();
			if(!Tools::isValidNumber(currentValue))
				continue; //skip nan and +/-inf values
	
			if(min > currentValue)
				min = currentValue;

			if(max < currentValue)
				max = currentValue;
		}
	}

	minMaxPair.first = min;
	minMaxPair.second = max;

	return minMaxPair;
}

//SLOT
void QtfyMain::plotRtClickYAxis(const QPoint& pnt)
{
	QwtScaleWidget* selectedAxis = (QwtScaleWidget*) (sender());

	//get the subwindow title of the window the user selected
	//myMdiSubWindow* currWindow = dynamic_cast<myMdiSubWindow*> (selectedAxis->parent());
	QTFyPlot* plot = dynamic_cast<QTFyPlot*> (selectedAxis->parent());

	//find the subwindow the plot is in 
	this->currSubwindowTitle = plot->getSubWindow()->windowTitle();

	QPoint globalPos = selectedAxis->mapToGlobal(pnt);
	QMenu myMenu;
	QMenu *manualMenu = new QMenu("Manual Settings");

	//set global/local scale
	QAction *actGlobalYScale = new QAction("Global scale", this);
	QAction *actLocalYScale = new QAction("Local scale", this);
	myMenu.addAction(actLocalYScale);
	myMenu.addAction(actGlobalYScale);

	//add x,y axis manual setting
	//QAction *actXManual = new QAction("Set x axis scale", this);
	QAction *actYManual = new QAction("Set y axis scale", this);
	QAction *actResetScales = new QAction("Reset axis", this);
	//manualMenu->addAction(actXManual);
	manualMenu->addAction(actYManual);
	manualMenu->addAction(actResetScales);
	myMenu.addSeparator();
	myMenu.addMenu(manualMenu);

	//connect local/global scale actions
	connect(actGlobalYScale, SIGNAL(triggered()),this, SLOT(setYAxisToGlobalScale()));
	connect(actLocalYScale, SIGNAL(triggered()),this, SLOT(setYAxisToLocalScale()));

	//connect x,y axis manual settings actions
	//connect(actXManual, SIGNAL(triggered()), this, SLOT(setXAxisScale()));
	connect(actYManual, SIGNAL(triggered()), this, SLOT(setYAxisScale()));
	connect(actResetScales, SIGNAL(triggered()), this, SLOT(resetAxis()));

	myMenu.exec(globalPos);
}

//SLOT
void QtfyMain::setYAxisToGlobalScale()
{
	this->yAxisToLocalScale =false;
	this->manualYAxisSettings = false;
	this->updatePlotAxis();
}

void QtfyMain::setYAxisToLocalScale()
{
	this->yAxisToLocalScale = true;
	this->manualYAxisSettings = false;
	this->updatePlotAxis();
}

//SLOT
void QtfyMain::setXAxisScale()
{

	bool ok1, ok2;    
	QString xMin = QInputDialog::getText(this, tr("X Axis"), tr("Give desired x minimum value :"), QLineEdit::Normal, "0", &ok2);
	QString xMax = QInputDialog::getText(this, tr("X Axis"), tr("Give desired x maximum value :"), QLineEdit::Normal, "", &ok1);

	if (ok1 && ok2 && !xMax.isEmpty() && !xMin.isEmpty()){
		this->maxX = xMax.toDouble();
		this->minX = xMin.toDouble();
		this->manualXAxisSettings = true;
		this->yAxisToLocalScale = false;
	 }else{
		 QMessageBox msgBox;
		 msgBox.setText("Something went wrong, retry.");
		 msgBox.exec();
	 }

	this->updatePlotAxis();	
}

//SLOT
void QtfyMain::setYAxisScale()
{
	bool ok1, ok2;
	QString yMin = QInputDialog::getText(this, tr("Y Axis"), tr("Give desired y minimum value :"), QLineEdit::Normal, "0", &ok2);
	QString yMax = QInputDialog::getText(this, tr("Y Axis"), tr("Give desired y maximum value :"), QLineEdit::Normal, "", &ok1);

     if (ok1 && ok2 && !yMax.isEmpty() && !yMin.isEmpty()){
		 this->maxY = yMax.toDouble();
		 this->minY = yMin.toDouble();
		 this->manualYAxisSettings = true;
		this->yAxisToLocalScale = false;
	 }else{
		 QMessageBox msgBox;
		 msgBox.setText("Something went wrong, retry.");
		 msgBox.exec();
		 this->currSubwindowTitle = "";
		 return;
	 }

	 //save the changed y axis min/max values
	 QPair<double, double> minMaxValues;
	 minMaxValues.first = minY;
	 minMaxValues.second = maxY;

	 QString subwindowTitle;
	 yManualMinMaxMap.insert(this->currSubwindowTitle, minMaxValues); 
	this->updatePlotAxis();	 
	//update viewers
	//if (this->m_tvwDataTreeOverview->selectedIndexes().size() == 0)
	//	updateAllViewers(this->m_TreeDataModel->index(0,0));
	//else
	//	updateAllViewers(this->m_tvwDataTreeOverview->selectedIndexes().at(0));
}

//SLOT
void QtfyMain::resetAxis()
{
	//set nan unless the user change them
	this->maxX = std::numeric_limits<double>::quiet_NaN(); 
	this->minX = std::numeric_limits<double>::quiet_NaN();
	this->maxY = std::numeric_limits<double>::quiet_NaN();
	this->minY = std::numeric_limits<double>::quiet_NaN();

	this->manualXAxisSettings = false;
	this->manualYAxisSettings = false;
	this->yAxisToLocalScale = false;

	this->updatePlotAxis();	
}

//SLOT 
void QtfyMain::exportPlotImages()
{
	QVector<QPixmap> images;
	QVector<QString> fileNames;
	QMessageBox msgBox;

	//message for merged or individually exported images
	msgBox.setWindowTitle("Export image");
	msgBox.setText("Do you want to export the plots merged or separately?");
	QAbstractButton *mergeImages = msgBox.addButton("Merged images", QMessageBox::YesRole);
	QAbstractButton *separateImages = msgBox.addButton("Separate images", QMessageBox::NoRole);
	msgBox.addButton(QMessageBox::Cancel);
	msgBox.setDefaultButton(QMessageBox::Cancel);

	//if cancel return
	if(msgBox.exec() == QMessageBox::Cancel)
		return;

	bool mergeSelected;
	if(msgBox.clickedButton() == mergeImages){
		mergeSelected = true;
	}
	else if(msgBox.clickedButton() == separateImages){
		mergeSelected = false;
	}


	for(QMap<QString, myMdiSubWindow*>::Iterator it = mSubwindows.begin(); it != mSubwindows.end(); ++it){
		// find current subwindow
		myMdiSubWindow* cSubwindow = it.value();
		QLayoutItem* item = cSubwindow->layout()->itemAt(0);

		// get the plot
		QTFyPlot* plot = dynamic_cast<QTFyPlot*>(item->widget());
		
		//if plot exported separately set the default file names
		if(!mergeSelected){
			if(plot->getSubWindow()->windowTitle() == "")
				plot->getSubWindow()->setWindowTitle("CellLineage");
			fileNames.append(plot->getSubWindow()->windowTitle());
		}

		if(!plot)
			continue;
			
		QPixmap img = plot->exportPNG();
		if(!img)
			continue;
		else
			images.append(img);
	}		


	//if no images were successfully made, return
	if(images.isEmpty())
		return;//message?


	
	QString treeName =  treeSetInformation[1];

	if(mergeSelected){
		QString types("JPEG File (*.jpeg);;" "JPG File (*jpg);;" "PNG File (*.png);;" "TIF File (*.tif);;" "TIFF File (*.tiff)");// Set up the possible graphics formats
		//the file path
		QString fileName = lastSelectedDirectory + "/" + treeName.remove(".ttt").trimmed();
		QString path = QFileDialog::getSaveFileName(this,tr("Save Image"),fileName,types);

		//check if user cancelled
		if(path.isEmpty())
			return;

		QDir d = QFileInfo(path).absoluteDir();
		if ( ! QDir(d).exists() )
			return;
		lastSelectedDirectory = d.absolutePath();

		//merge images with qpainter
		//then export image to path
		QPainter painter;
		QSize size = images[0].size();
		int x = size.width();
		int y = size.height();
		int currentHeight= 0;
		int heightSum  = images.size() * y;
		QImage finalImg(x, heightSum, QImage::Format_ARGB32);
		int dpm = 300 / 0.0254; // ~300 DPI
		finalImg.setDotsPerMeterX(dpm);
		finalImg.setDotsPerMeterY(dpm);
		painter.begin(&finalImg);
		
		for(int i = 0; i < images.size(); i++){
			QImage img = images[i].toImage();
			painter.drawImage(0, currentHeight,img);
			currentHeight = currentHeight + y;
		}
		painter.end();

		if(finalImg.save(path))
			emit updateMsgLog(QString("> Successful exporting of image: %1...").arg(path), Qt::darkGreen);
		else {
			emit updateMsgLog(QString(">Write error: Failed to export: %1...").arg(path), Qt::red);
			return;
		}
	}
	else {
		//folder selection by user 
		QString folder = QFileDialog::getExistingDirectory (this, "Select directory to save images", lastSelectedDirectory);
		//if user canceled
		if(folder.isEmpty())
			return;
		//if invalid input return
		if ( ! QDir(folder).exists() )
			return;
		lastSelectedDirectory = folder;

		//file format selection by user
		QStringList filters;
		filters << "JPEG File (*.jpeg)"
			<<"JPG File (*.jpg)"
			<< "PNG File (*.png)"
			<< "TIFF File (*.tiff)"
			<< "TIF File (*.tif)";	
		bool ok;
		QString item = QInputDialog::getItem(this, tr("File format selection"), tr("Save files as: "),filters, 0, false, &ok);
		//if invalid input return
		if (!ok || item.isEmpty())
			return;
		QString suffix;
		if(item.contains("JPEG"))
			suffix = "jpeg";
		else if(item.contains("JPG"))
			suffix = "jpg";
		else if(item.contains("PNG"))
			suffix = "png";
		else if(item.contains("TIFF"))
			suffix = "tiff";
		else if (item.contains("TIF"))
			suffix = "tif";
		else//ivalid format, return
			return;

		for(int i = 0; i < images.size(); i++){
			//the file path
			QString imageFilePath = folder + "/" + fileNames[i].remove(":").trimmed() + "." + suffix;
			
			QImage img = images[i].toImage();
			int dpm = 300 / 0.0254; // ~300 DPI
			img.setDotsPerMeterX(dpm);
			img.setDotsPerMeterY(dpm);	
			if(img.save(imageFilePath))
				emit updateMsgLog(QString("> Successful exporting of image: %1...").arg(imageFilePath), Qt::darkGreen);
			else
				emit updateMsgLog(QString(">Write error: Failed to export: %1...").arg(imageFilePath), Qt::red);
		}
	}

	Tools::displayMessageBoxWithOpenFolder("Export completed.", "QTFy", lastSelectedDirectory, false);
	
}

// SLOT
void QtfyMain::exportHeattreePlot()
{
	// identify the quantification chunk to get the values from

	// only if there is an attribute plotted
	if (this->addedAttributes.isEmpty()) {
		QString message = "No attribute has been plotted. Please first select an attribute to proceed.";
		QMessageBox::warning (this, "No attribute available!", message, QMessageBox::Ok);
		return;
	}

	QApplication::setOverrideCursor(Qt::WaitCursor);

	// from quantification chunk 
	// find the selected view
	int segmentationWavelength = 2;
	int segmentationMethodID = 1; 

	myMdiSubWindow* theWindow = dynamic_cast<myMdiSubWindow*> (ui.mdiArea->activeSubWindow());
	QString chunkName = theWindow->getAttributeChunkName();
	int indexOfCurrentWindow = this->mSubwindows.values().indexOf(theWindow);
	QString attributeName = this->mSubwindows.keys().at(indexOfCurrentWindow);

	QString quantiWL = attributeName.right(2);

	bool conversionState = false;
	int quantiWLInt = quantiWL.toInt(&conversionState);

	int indexDw = chunkName.indexOf("DetectionCh");
	int indexSegm = chunkName.indexOf("SegMethod");

	QString detectionWL = chunkName.mid(indexDw,indexSegm-indexDw).remove("DetectionCh");
	QString segmentationMethod = chunkName.right(chunkName.length() - indexSegm).remove("SegMethod");

	bool conversionStateDetect = false;
	int detectionWLInt = detectionWL.toInt(&conversionStateDetect);

	bool conversionStateMethod = false;
	int segmentationMethodInt = segmentationMethod.toInt(&conversionStateMethod);

	if (conversionStateDetect && conversionStateMethod) {
		segmentationWavelength = detectionWLInt;
		segmentationMethodID = segmentationMethodInt;
	}

	ITree* currentTree;
	QString cellName = " ";

	if(treeSetInformation.size() > 2 && this->currentTreeInPlots == treeSetInformation[1])//we are in the same tree as before, show cell
		cellName = treeSetInformation[2];

	//get the right tree
	for(QVector<QSharedPointer<ITree>>::const_iterator treeIterator = trees.constBegin(); treeIterator != trees.constEnd(); ++treeIterator)
	{	
		currentTree = (*treeIterator).data();
		QString c_tn = currentTree->getTreeName();
		if (currentTree->getTreeName() == treeSetInformation[1])
			break;
	}


	// QwtPlot* myTreePlot = new QwtPlot(this);

	// create a new Tree Plot to add to the tree subwindow
	//get max generation
	int maxGeneration = currentTree->getTreeProperty(ITree::TREE_OPT_NUMBER_OF_GENERATIONS).toInt();

	// Get the cells of the tree
	QVector<ITrack*> cellList = TreeStructuralHelper::getTracksInGenerations(currentTree, 0, maxGeneration);
	HeattreePlot* heatTree = new HeattreePlot(treeSetInformation, attributeName,chunkName, this, mTimeUnit);
	heatTree->getHeatColorRange(cellList);

	//SegEditorMain* segEditor = new SegEditorMain(this, segmentationWavelength, segmentationMethodID, segEditorWindowTitle);

	//set plotting options :: very important!!! that way colors always updated!
	heatTree->setPlottingOptions(this->mPlottingOptions);
	// myTreePlot = heatTree->createPlot(cellList[0], maxGeneration, cellName);
	heatTree->createPlot(cellList[0], maxGeneration);

	heatTree->exportToImage();

	QApplication::restoreOverrideCursor();
	

	//attach plot to subwindow
	//QDialog* testWindow = new QDialog(this);
	//QHBoxLayout *pLayout = new QHBoxLayout(testWindow);
	//// pLayout->addWidget(myTreePlot);
	//pLayout->addWidget(heatTree);
	//pLayout->setAlignment(Qt::AlignCenter);
	//pLayout->setContentsMargins(0,0,0,0);
	//testWindow->setLayout(pLayout);	

	//QString heattreeTitle = "Heattree_"  +  chunkName + "_" + attributeName + "_" + treeSetInformation[1];
	//testWindow->setWindowTitle(heattreeTitle);

	///*myTreePlot->axisScaleDraw(QwtPlot::yLeft)->enableComponent(QwtAbstractScaleDraw::Labels, false);
	//myTreePlot->axisScaleDraw(QwtPlot::yLeft)->enableComponent(QwtAbstractScaleDraw::Ticks, false);*/

	//testWindow->show();
}

//SLOT
void QtfyMain::changeColor()
{
	
	QColor newColor;
	//if selectedCell NULL find selectedCell from treeView
	//if(selectedCell == NULL && treeSetInformation.size()>2)
	//	selectedCell = findSelectedCell();

	//check again if selected cell was found correctly
	if(this->selectedCell/* != NULL*/){
		newColor = QColorDialog::getColor( Qt::black, this);
		this->mPlottingOptions->setCellColor(selectedCell, newColor);
	}
	else{
		QString msg = "Warning > Please select a cell!";
		updateMsgLog(msg, Qt::red);
		return;
	}

	//update the cell color in all the plots
	if(mSubwindows.isEmpty())
		return;
	//this->mSubwindows;
	for(QMap<QString, myMdiSubWindow*>::Iterator it = mSubwindows.begin(); it != mSubwindows.end(); ++it){
		// find current subwindow
		myMdiSubWindow* cSubwindow = it.value();
		QLayoutItem* item = cSubwindow->layout()->itemAt(0);

		// get the plot
		QTFyPlot* plot = dynamic_cast<QTFyPlot*>(item->widget());

		if(!plot)
			continue;

		plot->changeCurveColor(selectedCell, newColor);
	}

	//update viewers
	//if (this->m_tvwDataTreeOverview->selectedIndexes().size() == 0)
		//updateAllViewers(this->m_TreeDataModel->index(0,0));
	//else
		//updateAllViewers(this->m_tvwDataTreeOverview->selectedIndexes().at(0));
}

//SLOT //maybe never called
void QtfyMain::setSelectedAttributes(QVector<QString>& _attributes)
{
	int test = _attributes.size();

}

//SLOT
void QtfyMain::openAttributeSelectionDialog()
{
	AttributeDisplaySelection::getInst()->show();
}

//SLOT
void QtfyMain::openParameterCalculator()
{
	ParameterCalculator* paramCalculator = new ParameterCalculator(this);
	connect(paramCalculator, SIGNAL(successfullParameterSelection(QString, QString, QString, QString, QString, QString)), this, SLOT(createDerivedParameter(QString, QString, QString, QString, QString, QString)));
	
	paramCalculator->initializeData(this->chunkAttributes, this->chunkSoftware, this->chunkRemarks);
	paramCalculator->show();

}

//SLOT
//pop up menu for tree plot
void QtfyMain::treePlotRtClickContextMenu(const QPoint& pnt)
{

	QwtPlotCanvas* selectedPlotCanvas = (QwtPlotCanvas*) (sender());
	QwtPlot* selectedPlot = selectedPlotCanvas->plot();

	//trasform the qpoint to qwtdouble point
	QSize canvasSize = selectedPlotCanvas->size();
	QSize plotSize = selectedPlot->size();

	int xDiff = plotSize.width() - canvasSize.width();

	QwtDoublePoint newPos;
	newPos.setX(pnt.x() - xDiff);
	newPos.setY(pnt.y());	//the plot

	selectedCell = findCellSelectedFromTreePlot(selectedPlot, pnt);

	//if selected cell not valid 
	if(!selectedCell)
		return;

	//find where to show the pop-up menu
	PlotPickerHelp helper;
	QPoint mapToPlot = helper.invTransform(selectedPlot, pnt);
	QPoint globalPos = selectedPlotCanvas->mapToGlobal(pnt);

	QMenu myMenu;

	//color change
	QAction *actColorOptions = new QAction(QIcon(":/qtfyres/qtfyres/colorize-2.png"), "Change cell color", this);
	//hide branch
	QAction *actHideBranch = new QAction(QIcon(":/qtfyres/qtfyres/hide-branch.png"), "Hide cell and progeny", this);

	//show cell and it's children
	QAction *actShowBranch = new QAction(QIcon(":/qtfyres/qtfyres/show-branch.png"), "Show cell and progeny", this);

	//show all cells of the tree
	QAction *actShowAll = new QAction(QIcon(":/qtfyres/qtfyres/show-complete-tree.png"), "Show complete tree", this);

	QAction *actEditLineWidth = new QAction(QIcon(":/qtfyres/qtfyres/pencil.png"), "Edit line width", this);

	//add actions
	myMenu.addAction(actColorOptions);
	//if(selectedCell->getTrackNumber() != 1)
	myMenu.addAction(actHideBranch);
	myMenu.addSeparator();
	myMenu.addAction(actShowBranch);
	myMenu.addAction(actShowAll);
	myMenu.addAction(actEditLineWidth);

	//connect actions
	connect(actColorOptions, SIGNAL(triggered()), this, SLOT(changeColor()));
	//if(selectedCell->getTrackNumber() != 1)
	connect(actHideBranch, SIGNAL(triggered()), this, SLOT(hideBranch()));
	connect(actShowBranch, SIGNAL(triggered()), this, SLOT(showBranch()));
	connect(actShowAll, SIGNAL(triggered()), this, SLOT(showAllCells()));
	connect(actEditLineWidth, SIGNAL(triggered()), this, SLOT(editLineWidthTreePlot()));

	myMenu.addAction(ui.actRestoreDefaultColors);


	myMenu.exec(globalPos);

}

ITrack* QtfyMain::findCellSelectedFromTreePlot(const QwtPlot* myPlot, const QwtDoublePoint &_pnt)
{
	//return cell
	ITrack* cell;

	//find tree
	ITree* currentTree;
	for(QVector<QSharedPointer<ITree>>::const_iterator treeIterator = trees.constBegin(); treeIterator != trees.constEnd(); ++treeIterator) {	
		currentTree = (*treeIterator).data();
		QString c_tn = currentTree->getTreeName();
		if (currentTree->getTreeName() == treeSetInformation[1])
			break;
	}

	//convert to QPoint
	QPoint pnt;
	pnt.setX(_pnt.x());
	pnt.setY(_pnt.y());

	//transform point to plot coordinates
	PlotPickerHelp helper;

	QPoint tmp = helper.invTransform(myPlot, pnt);			
	double y = tmp.ry(); //the y coordinate of the selected point
	double x = tmp.rx(); //the x coordinate of the selected point

	//find the cell selected
	QwtPlotItemList listOfPlotItems = myPlot->itemList();

	QwtPlotCurve* closestCurve = 0;

	double distThreshold = 10; // distance of pix from cell curve
	double  minDist = distThreshold;

	for (QwtPlotItemList::iterator it = listOfPlotItems.begin(); it != listOfPlotItems.end(); ++it) { 
		// find the type of the plot item	
		int type = (*it)->rtti();
		//look for the closest point for curves only
		if (type == QwtPlotItem::Rtti_PlotCurve) {
			QwtPlotCurve* currentCurve = (QwtPlotCurve*) (*it);

			// set the pen to normal width (not selected)
			QPen newPen = currentCurve->pen();
			//adjust the line width
			//int lineWidth = this->mPlottingOptions->getLineWidthForTreePlot(currentTree);
			int lineWidth = this->mPlottingOptions->getLineWidthForTreePlot();
			newPen.setWidth(lineWidth);
			newPen.setWidth(lineWidth);
			currentCurve->setPen(newPen);

			//if default curve ignore, not a cell curve
			if(currentCurve->title().text() == "Default Curve")
				continue;

			if(currentCurve->title().text() == "timepoint")
				continue;

			QwtData &data = currentCurve->data();

			double yCurve = data.y(0); 
			double x1Curve = data.x(0);
			double x2Curve = data.x(1);

			if(!(x1Curve <= x && x <= x2Curve)) //check that user clicked between a specific line
				continue;

			double dist = abs(y - yCurve);

			if(dist < minDist && dist <= distThreshold){
				minDist = dist;
				closestCurve = currentCurve;
			}
		}			
	}

	// a curve has been found	
	if (closestCurve) {

		//if closest curve transparent, don't show menu
		QPen newPen = closestCurve->pen();
		if(newPen.color().alpha() == 0)
			return NULL;

		//adjust line width
		/*int lineWidth = this->mPlottingOptions->getLineWidthForTreePlot(currentTree);*/
		int lineWidth = this->mPlottingOptions->getLineWidthForTreePlot();
		newPen.setWidth(lineWidth + 2);
		closestCurve->setPen(newPen);

		QString cellName = closestCurve->title().text();
		int numIndex = cellName.length() - (cellName.lastIndexOf("_") + 1);
		int cellNum = (cellName.right(numIndex)).toInt();

		QString message =  cellName;
		QMainWindow::statusBar()->showMessage(message);

		QModelIndex selectedIndex;
		QModelIndex newIndex;

		// find the index that is currently selected at the tree view
		// use it as a hint for the search of appropriate index (see method findIndex)
		// if no index is selected use the first index
		if (this->m_tvwDataTreeOverview->selectedIndexes().size() == 0)
			selectedIndex = this->m_TreeDataModel->index(0,0).child(0,0);
		else
			selectedIndex  = this->m_tvwDataTreeOverview->selectedIndexes().at(0);

		//find cell
		TreeItem *selectedItem;
		QString treeName;

		selectedItem = static_cast<TreeItem*>(selectedIndex.internalPointer());
		
		// is this a tree item? it is if the parent is the root
		if (selectedItem->column() == 0) 
			treeName = selectedItem->child(0)->data(0).toString(); //tree name
		else if (selectedItem->column() == 1) 
			treeName = selectedItem->data(0).toString();//tree name
		else if (selectedItem->column() == 2)
			treeName = selectedItem->parent()->data(0).toString();//tree name
		else if (selectedItem->column() == 3)
			treeName = selectedItem->parent()->parent()->data(0).toString();//tree name

		//find the selected cell's ITrack object
		for(QVector<QSharedPointer<ITree>>::iterator i = trees.begin(); i != trees.end(); ++i){
			ITree* currentTree = i->data();

			if(treeName == currentTree->getTreeName()){
				cell = currentTree->getTrackByNumber(cellNum);
				return cell;
			}

		}
	}

	//if no cell found return 0
	return 0;
}

//SLOT
void QtfyMain::showBranch()
{
	//if selectedCell NULL find selectedCell from treeView
	//if(selectedCell == NULL && treeSetInformation.size() >2)
	//	selectedCell = findSelectedCell();

	//check again if selected cell was found correctly
	if(!this->selectedCell /*== NULL*/){
		QString msg = "Warning > Please select a cell!";
		updateMsgLog(msg, Qt::red);
		return;
	}
	////check if there is a selected cell from the treeview
	//if(this->selectedCell == NULL && treeSetInformation.size() < 3){
	//	QString msg = "Warning > Please select a cell!";
	//	updateMsgLog(msg, Qt::red);
	//	return;
	//}else{
	//	selectedCell = this->findSelectedCell();
	//	//if for some reasoned failed to find the cell from the tree view show the message
	//	if(selectedCell == NULL){
	//		QString msg = "Warning > Please select a cell!";
	//		updateMsgLog(msg, Qt::red);
	//		return;
	//	}
	//}

	QVector<ITrack*> cellList = findChildren(selectedCell);
	cellList<<selectedCell;
	//update plotting options
	foreach(ITrack* cell, cellList){
		int cellNum = cell->getTrackNumber();
		QColor color = this->mPlottingOptions->getCellColor(cell);
		color.setAlpha(255);
		this->mPlottingOptions->setCellColor(cell, color);
	}
	
	//update the cell color in all the plots
	if(mSubwindows.isEmpty())
		return;
	
	for(QMap<QString, myMdiSubWindow*>::Iterator it = mSubwindows.begin(); it != mSubwindows.end(); ++it){
		// find current subwindow
		myMdiSubWindow* cSubwindow = it.value();
		QLayoutItem* item = cSubwindow->layout()->itemAt(0);

		// get the plot
		QTFyPlot* plot = dynamic_cast<QTFyPlot*>(item->widget());

		if(!plot)
			continue;
		plot->detachCurve("timepoint");

		if(it.key() == "treeplot")
			plot->hideBranch(cellList, false, true);						
		else	
			plot->hideBranch(cellList, false);
	}
	this->mustReplot = false;
	//this->showPointsSelected(this->showOriginalPoints);
	this->updatePlots();
	this->mustReplot = true;
}
//SLOT
void QtfyMain::hideBranch()
{
	//if selectedCell NULL find selectedCell from treeView
	//if(selectedCell == NULL && treeSetInformation.size() >2)
	//	selectedCell = findSelectedCell();

	//check again if selected cell was found correctly
	if(!this->selectedCell /*== NULL*/){
		QString msg = "Warning > Please select a cell!";
		updateMsgLog(msg, Qt::red);
		return;
	}

	QVector<ITrack*> cellList = findChildren(selectedCell);
	cellList<<selectedCell;

	//update plotting options
	foreach(ITrack* cell, cellList){
		int cellNum = cell->getTrackNumber();
		QColor color = this->mPlottingOptions->getCellColor(cell);
		color.setAlpha(0);
		this->mPlottingOptions->setCellColor(cell, color);
	}

	//update the cell color in all the plots
	if(mSubwindows.isEmpty())
		return;
	
	for(QMap<QString, myMdiSubWindow*>::Iterator it = mSubwindows.begin(); it != mSubwindows.end(); ++it){
		// find current subwindow
		myMdiSubWindow* cSubwindow = it.value();
		QLayoutItem* item = cSubwindow->layout()->itemAt(0);

		// get the plot
		QTFyPlot* plot = dynamic_cast<QTFyPlot*>(item->widget());
		plot->detachCurve("timepoint");
		if(!plot)
			continue;

		plot->hideBranch(cellList, true);
	}
	this->updatePlots();
}
//SLOT
void QtfyMain::showAllCells()
{
	//find current tree
	if(treeSetInformation.isEmpty())
		return;

	QString treeName = treeSetInformation[1];
	ITree* currentTree;
	bool treeFound = false;

	for(QVector<QSharedPointer<ITree>>::iterator it = trees.begin(); it != trees.end(); ++it){
		currentTree = it->data(); 

		if(treeName == currentTree->getTreeName()){
			treeFound = true;
			break;
		}	
	}

	if(!treeFound)
		return;

	//get root cell of the tree
	ITrack* root = currentTree->getRootNode();
	//test
	selectedCell = root;
	showBranch();
	//setCellTransparent(root, false);

	//update viewers
	/*if (this->m_tvwDataTreeOverview->selectedIndexes().size() == 0)
		updateAllViewers(this->m_TreeDataModel->index(0,0));
	else
		updateAllViewers(this->m_tvwDataTreeOverview->selectedIndexes().at(0));*/
}

void QtfyMain::plotRtClickXAxis(const QPoint& pnt)
{

	QwtScaleWidget* selectedAxis = (QwtScaleWidget*) (sender());
	QPoint globalPos = selectedAxis->mapToGlobal(pnt);
	//QwtPlotCanvas* selectedPlotCanvas = (QwtPlotCanvas*) (sender());
	//QPoint globalPos = selectedPlotCanvas->mapToGlobal(pnt);
	//QPoint globalPos = ui.mdiArea->mapToGlobal(pnt);
	QMenu myMenu;
	QMenu *manualMenu = new QMenu("Manual Settings");

	//set global/local scale
	//QAction *actGlobalYScale = new QAction("Global scale", this);
	//QAction *actLocalYScale = new QAction("Local scale", this);
	//myMenu.addAction(actLocalYScale);
	//myMenu.addAction(actGlobalYScale);

	//add x,y axis manual setting
	QAction *actXManual = new QAction("Set x axis scale", this);
	//QAction *actYManual = new QAction("Set y axis scale", this);
	QAction *actResetScales = new QAction("Reset axis", this);
	QMenu *timeUnitMenu = new QMenu("Set axis time unit");
	QAction *actUnitHours = new QAction("Hours", this);
	QAction *actUnitMins = new QAction("Minutes", this);
	QAction *actUnitSecs = new QAction("Seconds", this);

	manualMenu->addAction(actXManual);
	//manualMenu->addAction(actYManual);
	manualMenu->addAction(actResetScales);
	manualMenu->addSeparator();
	
	//x axis time unit menu
	timeUnitMenu->addAction(actUnitHours);
	timeUnitMenu->addAction(actUnitMins);
	timeUnitMenu->addAction(actUnitSecs);
	manualMenu->addMenu(timeUnitMenu);
	myMenu.addMenu(manualMenu);


	//connect x,y axis manual settings actions
	connect(actXManual, SIGNAL(triggered()), this, SLOT(setXAxisScale()));
	//connect(actYManual, SIGNAL(triggered()), this, SLOT(setYAxisScale()));
	connect(actResetScales, SIGNAL(triggered()), this, SLOT(resetAxis()));

	//connect the time unit actions
	connect(actUnitHours, SIGNAL(triggered()), this, SLOT(setXFactor()));
	connect(actUnitMins, SIGNAL(triggered()), this, SLOT(setXFactor()));
	connect(actUnitSecs, SIGNAL(triggered()), this, SLOT(setXFactor()));

	myMenu.exec(globalPos);
}

void QtfyMain::cancel()
{
	if (!this->csvExportDone) {

		QMessageBox msgBox;
		msgBox.setWindowTitle("Quantification not saved!");
		msgBox.setText(tr("You are about to quit QTFy without saving the existing quantifications.\n Are you sure you want to quit without saving?"));
		QAbstractButton* pButtonYes = msgBox.addButton(tr("Save and exit"), QMessageBox::YesRole);
		msgBox.addButton(tr("Exit without saving"), QMessageBox::NoRole);
		QAbstractButton* pButtonCancel = msgBox.addButton(tr("Cancel"), QMessageBox::RejectRole);

		msgBox.exec();

		if (msgBox.clickedButton()==pButtonYes) {
			this->exportTrees();
			this->closingPending = true;
			return;
		}

		if (msgBox.clickedButton()==pButtonCancel) {
			return;
		}
	}	
	
	/*AttributeDisplaySelection* selection = AttributeDisplaySelection::getInst(this);
	selection->close();*/
	qApp->quit();
}

// SLOT
void QtfyMain::updateMessageLogText (QString _msg, QColor _color)
{
	ui.txtMessageLog->setTextColor(_color);
	ui.txtMessageLog->append(_msg);
	ui.txtMessageLog->moveCursor (QTextCursor::End);
}

// SLOT
void QtfyMain::updateProgressBar ()
{
	int newval = progressBar->value() + 1;
	progressBar->setValue(newval);
}

// SLOT
void QtfyMain::setProgressBarMax (int _value)
{
	progressBar->setMaximum(_value);
}

// SLOT
void QtfyMain::exportFinished ()
{
	QApplication::restoreOverrideCursor();
	this->csvExportRunning = false;
	this->csvExportDone = true;
	this->progressBar->setVisible(false);

	if (this->closingPending)
		qApp->quit();
}

void QtfyMain::exportTrees()
{
	// if no experiment has been loaded yet
	this->progressBar->setVisible(true);
	// QApplication::setOverrideCursor( QCursor(Qt::WaitCursor) );

	QThread     *workerThread;
	ExportWorker *worker;

	if (csvExportRunning) {
		updateMessageLogText("> Warning: export is already running.", Qt::darkBlue);
		return;
	}

	workerThread = new QThread();
	worker       = new ExportWorker();

	worker->setTreesForExport(this->trees);

	worker->moveToThread(workerThread);
	connect(workerThread, SIGNAL(started()), worker, SLOT(startExporting()));
	connect(worker, SIGNAL(finished()), workerThread, SLOT(quit()));
	connect(worker, SIGNAL(finished()), worker, SLOT(deleteLater()));
	connect(worker, SIGNAL(finished()), this, SLOT(exportFinished()));
	connect(workerThread, SIGNAL(finished()), workerThread, SLOT(deleteLater()));

	connect(worker, SIGNAL(updateMsgLog(QString, QColor)), this, SLOT(updateMsgLog(QString, QColor)));
	connect(worker, SIGNAL(updateProgressBar()), this, SLOT(updateProgressBar()));
	connect(worker, SIGNAL(setProgressBarMax(int)), this, SLOT(setProgressBarMax(int)));

	workerThread->start();

	csvExportRunning = true;
}

// SLOT
void QtfyMain::openCorrectionUISettings()
{
	CorrectionUISettings::getInst()->show();
}

void QtfyMain::synchronizePlots()
{
	if(mSubwindows.size()<=2)
		return;
	bool firstAttrPlotFound = false;
	QTFyPlot* firstPlot;
	QTFyPlot* plot;

	for(QMap<QString, myMdiSubWindow*> ::iterator it = mSubwindows.constBegin(); it != mSubwindows.constEnd(); ++it){
		// find current subwindow
		myMdiSubWindow* cSubwindow = it.value();
		QLayoutItem* item = cSubwindow->layout()->itemAt(0);

		//not for the treeplot
		if(it.key() == "treeplot")
			continue;

		// get the plot
		/*QTFyPlot**/ plot = dynamic_cast<QTFyPlot*>(item->widget());

		if(!plot)
			continue;

		if(!firstAttrPlotFound){
			QString n = it.key();
			firstPlot = plot;
			firstAttrPlotFound = true;
			continue;
		}

		//connect all plots with the first plot
		//connect(firstPlot->axisWidget(QwtPlot::xBottom) , SIGNAL(scaleDivChanged () ), plot, SLOT(scaleDivChangedSlot () ));
		//connect(plot->axisWidget(QwtPlot::xBottom) , SIGNAL(scaleDivChanged () ), firstPlot, SLOT(scaleDivChangedSlot () ));
				
	}
}

void QtfyMain::updateMsgLog(QString _msg, QColor _color)
{
	ui.txtMessageLog->setTextColor(_color);
	ui.txtMessageLog->append(_msg);
	ui.txtMessageLog->moveCursor (QTextCursor::End);
}

QVector<ITrack*> QtfyMain::findChildren(ITrack* _cell){

	//return data
	QVector<ITrack*> children;

	if(_cell->getChild1()){
		children = findChildren(_cell->getChild1());
		children<< _cell->getChild1();
	}

	if(_cell->getChild2()){
		children = children + findChildren(_cell->getChild2());
		children<< _cell->getChild2();
	}

	return children;
}

void QtfyMain::plotTimepoint(int _timepoint){

	ITrack* _cell;
	//if _cell invalid find the selected cell for the treeSetInformation
	if(!selectedCell && _timepoint!=0){
		QString treeName = treeSetInformation[1];
		QString cellName = treeSetInformation[2];
		int numIndex = cellName.length() - (cellName.lastIndexOf("_") + 1);
		int cellNum = (cellName.right(numIndex)).toInt();

		//find cell
		//get the right tree
		ITree* currentTree;
		for(QVector<QSharedPointer<ITree>>::const_iterator treeIterator = trees.constBegin(); treeIterator != trees.constEnd(); ++treeIterator)
		{	
			currentTree = (*treeIterator).data();
			QString c_tn = currentTree->getTreeName();
			if (currentTree->getTreeName() == treeName)
				break;
		}

		// Get the cells of the tree
		int maxGeneration = currentTree->getTreeProperty(ITree::TREE_OPT_NUMBER_OF_GENERATIONS).toInt();
		QVector<int> mRegardedGenerations;
		for(int g = 0; g <= maxGeneration; g++){
			mRegardedGenerations<<g;
		}

		QVector<ITrack*> cellList = TreeStructuralHelper::getTracksInGenerations(currentTree, 0, maxGeneration);
		for(QVector<ITrack*>::iterator cellIterator = cellList.begin(); cellIterator != cellList.end(); ++cellIterator){
			if(cellNum == (*cellIterator)->getTrackNumber()){
				_cell = (*cellIterator);
				break;
			}
		}

	}
	else
		_cell = selectedCell;

	//update the cell color in all the plots
	if(mSubwindows.isEmpty())
		return;
	
	for(QMap<QString, myMdiSubWindow*>::Iterator it = mSubwindows.begin(); it != mSubwindows.end(); ++it){
		// find current subwindow
		myMdiSubWindow* cSubwindow = it.value();
		QLayoutItem* item = cSubwindow->layout()->itemAt(0);

		// get the plot
		QTFyPlot* plot = dynamic_cast<QTFyPlot*>(item->widget());

		if(!plot)
			continue;

		

		if(_timepoint == 0){//if no timepoint was found
			plot->detachCurve("timepoint");
			continue;
		}

		//delete previous timepoint curve, if there is one
		plot->detachCurve("timepoint");
		//find the time of the specific timepoint
		float timepointValue;
		float xFactor = getTimeFactor();

		//test 
		int num = _cell->getTrackNumber();

		int preIncTime = (Experiment::isUsePreincubationTime()) ? _cell->getITree()->getTreeProperty(ITree::TREE_PREINCUBATION_TIME).toInt() : 0;
		int firstCellEvent = _cell->getAbsoluteSecondsForTP(_cell->getFirstTimePoint());
		int cellSec = _cell->getSecondsForTimePoint(_timepoint);
		timepointValue = (preIncTime + firstCellEvent + cellSec) *xFactor;

		//set data
		QVector<QPointF> data;
		QPointF tmp;
		tmp.setX(timepointValue);
		double minValue = plot->axisScaleDiv(QwtPlot::yLeft)->interval().minValue();
		double maxValue = plot->axisScaleDiv(QwtPlot::yLeft)->interval().maxValue();
	
		//set minimum point
		tmp.setY(minValue);
		data.append(tmp);

		//se maximum point
		tmp.setY(maxValue);
		data.append(tmp);

		QPen pen(Qt::red);

		QwtPlotCurve* timepointCurve = new QwtPlotCurve("timepoint");
		pen.setStyle(Qt::DashLine);

		timepointCurve->setStyle(QwtPlotCurve::Lines);
		timepointCurve->setPaintAttribute(QwtPlotCurve::PaintFiltered);
		timepointCurve->setPen(pen);
		timepointCurve->setData(data);

		timepointCurve->attach(plot);

		//replot
		plot->replot();
	}
}

void QtfyMain::updatePlotAxis(){
	//iterate through plots
	for(QMap<QString, myMdiSubWindow*>::Iterator it = mSubwindows.begin(); it != mSubwindows.end(); ++it){
			// find current subwindow
			myMdiSubWindow* cSubwindow = it.value();
			QLayoutItem* item = cSubwindow->layout()->itemAt(0);

			// get the plot
			QTFyPlot* plot = dynamic_cast<QTFyPlot*>(item->widget());

			if(!plot)
				continue;

			//if tree plot, continue
			if(it.key() == "treeplot")
				continue;

			QString attrName = it.key();
			QString quantChunkName = cSubwindow->getAttributeChunkName();
			//get subwindow title
			QString subwindowTitle = cSubwindow->windowTitle();

			QPair<double, double> minMax;
			if(!this->yAxisToLocalScale){
				if(globalMinMaxMap.contains(subwindowTitle))
					minMax = globalMinMaxMap[subwindowTitle];
			}else{
				minMax = findMinMax(attrName, quantChunkName);
			}

			//settings for x axis
			//--------------------
			//if x manual settings
			if (this->manualXAxisSettings) 
				plot->setAxisScale(QwtPlot::xBottom, minX, maxX);
			else
				plot->setAxisAutoScale(QwtPlot::xBottom);

			//settings for y axis
			//--------------------
			//is manual and recently updatted?? 
			if(this->manualYAxisSettings && this->currSubwindowTitle != " "){
				if(this->currSubwindowTitle == subwindowTitle){
					if(this->yManualMinMaxMap.contains(subwindowTitle)){
						QPair<double, double> pair = this->yManualMinMaxMap.value(subwindowTitle);
						double min = pair.first;
						double max = pair.second;
						plot->setAxisScale(QwtPlot::yLeft, min, max);
						this->currSubwindowTitle = "";
						plot->replot();
						return;
					}
				}
			}	
			else if(!this->yAxisToLocalScale)//is global?
				plot->setAxisScale(QwtPlot::yLeft, minMax.first, minMax.second);
			else if(this->yAxisToLocalScale)// is local
				plot->setAxisAutoScale(QwtPlot::yLeft);
			
			plot->replot();
	}
}

void QtfyMain::setAllTimepointsActive()
{
	// user chooses if he wants to reset the activation status of all the trees or only the selected one
	QMessageBox msgBox;
	msgBox.setIcon(QMessageBox::Question);
	msgBox.setText("Reset all timepoints to active.");
	msgBox.setInformativeText("Do you want to activate the timepoints for all the trees of the treeset, or only the current tree?");
	QAbstractButton *allTrees = msgBox.addButton("For all trees", QMessageBox::YesRole);
	QAbstractButton *selectedTree = msgBox.addButton("Only for current tree", QMessageBox::NoRole);
	msgBox.addButton(QMessageBox::Cancel);
	msgBox.setDefaultButton(QMessageBox::Cancel);

	//if cancel return
	if(msgBox.exec() == QMessageBox::Cancel)
		return;

	bool allTreesSelected;
	if(msgBox.clickedButton() == allTrees){
		allTreesSelected = true;
	}
	else if(msgBox.clickedButton() == selectedTree){
		allTreesSelected = false;
	}

	QApplication::setOverrideCursor(Qt::WaitCursor);

	//the trees to activate the timepoints
	QVector<ITree*> selectedTrees;
	QString currentTree = treeSetInformation[1];

	//find which trees to consider
	for(QVector<QSharedPointer<ITree>>::iterator treeIt = trees.begin(); treeIt != trees.end(); ++treeIt){
		ITree* curTree = treeIt->data();
		if(allTreesSelected)
			selectedTrees.append(curTree);
		else{
			if(currentTree == curTree->getTreeName()){
				selectedTrees.append(curTree);
				break;
			}
		}
	}

	QTFyAttributeTools* helper;

	//foreach attribute that's plotted
	for(QMap<QString, myMdiSubWindow*>::iterator windIt = mSubwindows.begin(); windIt != mSubwindows.end(); ++windIt){
		//get attribute name
		QString attrName, chunkName;
		if(windIt.key() == "treeplot")
			continue;
		
		attrName = windIt.key();
		chunkName = windIt.value()->getAttributeChunkName();

		//iterate through selected trees
		foreach(ITree* tree, selectedTrees){

			//get celllineagetree
			CellLineageTree* cltData = tree->getCellLineageTree(); 

			//activate the timepoints
			if(!helper->resetActivation(attrName, cltData, chunkName))
				qDebug()<<"Reset timepoints failed!!!";
		}

	}

	//replot plots

	for(QMap<QString, myMdiSubWindow*>::Iterator it = mSubwindows.begin(); it != mSubwindows.end(); ++it){
		// find current subwindow
		myMdiSubWindow* cSubwindow = it.value();
		QLayoutItem* item = cSubwindow->layout()->itemAt(0);

		// get the plot
		QTFyPlot* plot = dynamic_cast<QTFyPlot*>(item->widget());

		if(!plot)
			continue;

		if(it.key() == "treeplot")
			continue;

		QString attrName = it.key();

		this->displayAttributePlot(attrName, "", plot, cSubwindow);
	}
	this->mustReplot = false;
	//retrieve plot with previous checks
	this->smoothLinesSelected(smoothLines);
	this->showPointsSelected(showOriginalPoints);
	this->outlierSelected(OutlierDetection);

	updatePlots();

	this->mustReplot = true;

	QApplication::restoreOverrideCursor();
	emit activationStatusChanged();
}

void QtfyMain::highlightCell(ITrack* _cell)
{
	//find cell curve in every plot
	//make pen width bigger
	//if selectedCell = 0, check if cell is 
	
	//the curve name
	QString curveName = QString::number(_cell->getTrackNumber());

	for(QMap<QString, myMdiSubWindow*>::Iterator it = mSubwindows.begin(); it != mSubwindows.end(); ++it){
		// find current subwindow
		myMdiSubWindow* cSubwindow = it.value();
		QLayoutItem* item = cSubwindow->layout()->itemAt(0);

		// get the plot
		QTFyPlot* plot = dynamic_cast<QTFyPlot*>(item->widget());

		if(!plot)
			continue;	

		int lineWidth;
		if(it.key() == "treeplot"){
			//lineWidth = this->mPlottingOptions->getLineWidthForTreePlot(_cell->getITree());
			lineWidth = this->mPlottingOptions->getLineWidthForTreePlot();
			plot->boldCurve(curveName, true, lineWidth);
		}
		else{
			if(this->smoothLines == true)
				curveName = "S" + curveName;
			/*lineWidth = this->mPlottingOptions->getLineWidthForAttributePlot(_cell->getITree());*/
			lineWidth = this->mPlottingOptions->getLineWidthForAttributePlot();
			plot->boldCurve(curveName, false, lineWidth);
		}

	}

}

//SLOT
void QtfyMain::plotPointSelectedTreePlot(const QwtDoublePoint &_pnt)
{
	QTFyPlot *myPlot = (QTFyPlot *) sender();

	QString chunkName;
	if(this->staggerWls){
		QString currWindowTitle = myPlot->windowTitle();
		QStringList list = currWindowTitle.split(":");
		if(list.size()>1){
			chunkName = list[0];
			this->setWindowFocus(currWindowTitle);
		}
	}
	selectedCell = this->findCellSelectedFromTreePlot(myPlot,_pnt);

	//if no cell found return
	if(!selectedCell)
		return;

	QString cellName = QString::number(selectedCell->getTrackNumber());
	int firstTimepoint = selectedCell->getFirstTimePoint();
	QModelIndex selectedIndex;
	QModelIndex newIndex;

	// find the index that is currently selected at the tree view
	// use it as a hint for the search of appropriate index (see method findIndex)
	// if no index is selected use the first index
	if (this->m_tvwDataTreeOverview->selectedIndexes().size() == 0)
		selectedIndex = this->m_TreeDataModel->index(0,0);
	else
		selectedIndex  = this->m_tvwDataTreeOverview->selectedIndexes().at(0);

	// find the new index to update the selection of the tree view
	newIndex = this->findIndexOfTimepoint (selectedIndex, cellName, 0);

	//get the cell index
	newIndex = newIndex.parent();

	QString message =  cellName + ", Timepoint_" + QString::number(firstTimepoint);
	QMainWindow::statusBar()->showMessage(message);

	if (!newIndex.isValid())
		return;

	updateSelectedIndex(newIndex);

	if(this->staggerWls)
		emit treeviewIndexChangedStagger(newIndex, chunkName);
	else
		emit treeviewIndexChanged(newIndex);
	this->m_tvwDataTreeOverview->setFocus();

}

//SLOT
void QtfyMain::updateCell(ITrack* _cell)
{

	this->setCursor(Qt::WaitCursor);

	int width = this->mPlottingOptions->getLineWidthForAttributePlot();

	//for every attribute plot, find the selected cell
	//iterate through plots
	for(QMap<QString, myMdiSubWindow*>::Iterator it = mSubwindows.begin(); it != mSubwindows.end(); ++it){
		// find current subwindow
		myMdiSubWindow* cSubwindow = it.value();
		QLayoutItem* item = cSubwindow->layout()->itemAt(0);

		// get the plot
		QTFyPlot* plot = dynamic_cast<QTFyPlot*>(item->widget());

		if(!plot)
			continue;

		//if tree plot, continue
		if(it.key() == "treeplot")
			continue;

		QString attrName = it.key();
		QString quantChunkName = cSubwindow->getAttributeChunkName();

		// Generate a color
		QColor c = this->mPlottingOptions->getCellColor(_cell);

		//initialize outlier manager
		
		mOutlierManager->setPlottingOptions(this->mPlottingOptions);
		mOutlierManager->setSmoothingThreshold(this->sValue);
		mOutlierManager->setDetectionThreshold(this->dValue);

		// get updated values for this cell

		QMap<int, QVariant> data = QTFyAttributeTools::getTimedValue(*_cell, attrName, quantChunkName);
		QString cellNumber = QString::number(_cell->getTrackNumber());

		//detach previous curve with the same name, check if smoothed line
		if(this->smoothLines)
			cellNumber = "S" + cellNumber;
		plot->detachCurve(cellNumber);

		if(data.isEmpty()){ //when data is empty, everything is deactivated, detach all curves
			//detach point curve as well
			cellNumber = "P" + QString::number(_cell->getTrackNumber());
			plot->detachCurve(cellNumber);
			continue;
		}

		// Create curve
		AttributePlot* helper;

		//find the x factor
		float xFactor = getTimeFactor();

		QVector<QPointF> points = helper->convertToPoints(data, xFactor);
		QwtPlotCurve* curve;
		//check for smoothed curve
		if(cellNumber.contains("S"))
			curve = helper->createSmoothedCurve(points, cellNumber, this->sValue, c, width);
		else
			curve = helper->createOriginalCurve(QString::number(_cell->getTrackNumber()), c, points, _cell, width);
	
		if(!curve)
			continue;

		curve->attach(plot);

		//if original points plotted, remove previous points and attach to the plot the new ones
		if(this->showOriginalPoints){
			QString curveName = "P" + QString::number(_cell->getTrackNumber());
			plot->detachCurve(curveName);
			QwtPlotCurve* pointsCurve = helper->plotOriginalPoints(curveName, c, points, _cell, width);
			pointsCurve->attach(plot);
		}
		//if outlier detection selected, remove previous outliers and attach to the plot the new ones
		if(this->OutlierDetection){
			QString curveName = "O" + QString::number(_cell->getTrackNumber());
			plot->detachCurve(curveName);
			QPolygonF outlierPoints = mOutlierManager->findOutliersOfCell(plot, attrName, quantChunkName, _cell);
			mOutlierManager->drawOutliers(plot, outlierPoints, QString::number(_cell->getTrackNumber()));

		}
	}

	this->updatePlots();
	setCursor(Qt::CustomCursor);

}

void QtfyMain::closeEvent ( QCloseEvent * closeEvent )
{
	this->cancel();
	closeEvent->ignore();
}

float QtfyMain::getTimeFactor()
{
	float factor;

	if(mTimeUnit == "Hours")
		factor = 1.0/3600.0;
	else if(mTimeUnit == "Minutes")
		factor = 1.0/60.0;
	else 
		factor = 1.0;

	return factor;
}

void QtfyMain::updatePlots()
{
	for(QMap<QString, myMdiSubWindow*>::Iterator it = mSubwindows.begin(); it != mSubwindows.end(); ++it){
		// find current subwindow
		myMdiSubWindow* cSubwindow = it.value();
		QLayoutItem* item = cSubwindow->layout()->itemAt(0);

		// get the plot
		QTFyPlot* plot = dynamic_cast<QTFyPlot*>(item->widget());

		if(!plot)
			continue;
				
		plot->replot();
	}
}

//SLOT
void QtfyMain::setXFactor()
{
	QAction *act =(QAction*) (sender());
	QString text = act->text();
	
	this->mTimeUnit = text;

	this->displaySelectedTree();
	refreshAttributePlots();
	//update viewers
	if (this->m_tvwDataTreeOverview->selectedIndexes().size() == 0)
		updateAllViewers(this->m_TreeDataModel->index(0,0));
    else
		updateAllViewers(this->m_tvwDataTreeOverview->selectedIndexes().at(0));
}

void QtfyMain::refreshAttributePlots()
{
	//set selected cell to invalid, it is referred to previous tree
	this->selectedCell = NULL;

	//iterate through plots
	for(QMap<QString, myMdiSubWindow*>::Iterator it = mSubwindows.begin(); it != mSubwindows.end(); ++it){
		// find current subwindow
		myMdiSubWindow* cSubwindow = it.value();
		QLayoutItem* item = cSubwindow->layout()->itemAt(0);

		// get the plot
		QTFyPlot* plot = dynamic_cast<QTFyPlot*>(item->widget());

		if(!plot)
			continue;

		//if tree plot, continue
		if(it.key() == "treeplot")
			continue;
			//this->displaySelectedTree();	
		else {
			//get the attribute name
			QString attrName = it.key();
			this->displayAttributePlot(attrName, cSubwindow->getAttributeChunkName(), plot, cSubwindow);
		}		
	}
}

void QtfyMain::openQTFyHelp()
{
	/*HelpMainWindow* helpWindow = new HelpMainWindow();
	helpWindow->show();*/

}

//SLOT
void QtfyMain::checkTreePlot(bool checkState){

	//if unchecked
	//find treeplot in subwindows (if already plotted)
	//and remove it
	if(!checkState){
		if(mSubwindows.contains("treeplot")){
			myMdiSubWindow* treeWindow = mSubwindows.value("treeplot");
			mSubwindows.remove("treeplot");
			ui.mdiArea->removeSubWindow(treeWindow);
		}
	}else{
	//check if already plotted
	//if yes do nothing, if no display tree plot
		if(!mSubwindows.contains("treeplot")){
			this->displaySelectedTree();
		}
		else
			return;
	}

	//synchronize plots
	this->synchronizePlots();

	//update viewers
	if (this->m_tvwDataTreeOverview->selectedIndexes().size() == 0)
		updateAllViewers(this->m_TreeDataModel->index(0,0));
	else
		updateAllViewers(this->m_tvwDataTreeOverview->selectedIndexes().at(0));

}

//SLOT
void QtfyMain::restoreDefaultColors()
{
	QApplication::setOverrideCursor(Qt::WaitCursor);
	//get current tree
	ITree* currentTree;
	for(QVector<QSharedPointer<ITree>>::const_iterator treeIterator = trees.constBegin(); treeIterator != trees.constEnd(); ++treeIterator)
	{	
		currentTree = (*treeIterator).data();
		QString c_tn = currentTree->getTreeName();
		if (currentTree->getTreeName() == treeSetInformation[1])
			break;
	}

	//set plotting options to default settings 
	this->mPlottingOptions->initializeOptions(currentTree);


	//update views
	this->restoreColors = true;
	this->displaySelectedTree();
	this->restoreColors = false;
	this->refreshAttributePlots();
	QApplication::restoreOverrideCursor();
}

// SLOT
void QtfyMain::showHelpOutliers()
{
	// create help text
	QString txt = "<p><h2>Outlier Detection</h2> </p>"  
		"<p>Check \"Smooth plot lines\" to generate the smoothed lines for each plot using penalized cubic regression splines. "
		"Adjust the amount of smoothing through the smoothing threshold. Check \"Detect outliers based on distance from smoothed line\". "
		"QTFy will first calculate the distance of each original point to the generated smoothed point and then calculate the percentage of "
		"this distance to the maximum available distance in the plot. If the percentage is higher than the detection threshold "
		"the quantification point is marked as an outlier and highlighted with black color.</p>"
		
		"<p><img src=\":hint.png\"> Did you notice that the outlier detection is a dockable window? "
		"This means you can pick it up and dock it in a different part of the main window or let it float independently.	</p>";

	QMap<QString, QImage> imgResources;
	QImage img(":/qtfyres/qtfyres/hint.png");
	imgResources.insert("hint.png", img);

	// show help form
	QString windowTitle = "QTFy help: Outlier Detection";

	QTFyHelp* help = new QTFyHelp(this, windowTitle);
	help->setHTMLHelp(txt, imgResources);
	help->show();
}

ITrack* QtfyMain::findSelectedCell(){
	for(QMap< QString, QMap< QString, ITrack* > >::iterator treeIt = cellsPerTree.begin(); treeIt != cellsPerTree.end(); ++treeIt){
		QString currTree = treeIt.key();
		if(currTree == treeSetInformation[1]){
			QMap<QString, ITrack*> cells = treeIt.value();
			for(QMap<QString, ITrack*>::iterator cellIt = cells.begin(); cellIt != cells.end(); ++cellIt){
				QString cellName = cellIt.key();
				cellName = "Cell_" + cellName;
				if(treeSetInformation[2] == cellName){
					return cellIt.value();
				}
			}
		}
	}
	return NULL;
}

void QtfyMain::exportMessageLog(){
	//get the text written in the message log
	QString text = ui.txtMessageLog->toPlainText();
	QStringList messages = text.split(">");
	//folder selection by user 
	QString folder = QFileDialog::getExistingDirectory (this, "Select directory to save text file", lastSelectedDirectory);

	//if user canceled return
	if(folder.isEmpty())
		return;
	//if invalid input return
	if ( ! QDir(folder).exists() )
		return;
	lastSelectedDirectory = folder;
	QString filename= folder + "/" + "Editor_Message_Log.txt";
	QFile file( filename );

	if (!filename.isEmpty())
	{
		file.setFileName(filename);
		file.open(QIODevice::WriteOnly);
		QTextStream output(&file);
		output.device()->setTextModeEnabled(true);
		output<<ui.txtMessageLog->toPlainText();
		file.close(); 
		emit updateMsgLog(QString("> Successful exporting of: %1...").arg(filename), Qt::darkGreen);
	} 
	else
		emit updateMsgLog(QString(">Write error: Failed to export: %1...").arg(filename), Qt::red);
}
//SLOT
void QtfyMain::editLineWidthAttributePlot()
{
	bool ok;
	//find current tree
	ITree* currentTree;
	for(QVector<QSharedPointer<ITree>>::const_iterator treeIterator = trees.constBegin(); treeIterator != trees.constEnd(); ++treeIterator)
	{	
		currentTree = (*treeIterator).data();
		QString c_tn = currentTree->getTreeName();
		if (currentTree->getTreeName() == treeSetInformation[1])
			break;
	}
	//get previous width
	/*int oldWidth = this->mPlottingOptions->getLineWidthForAttributePlot(currentTree);*/
	int oldWidth = this->mPlottingOptions->getLineWidthForAttributePlot();
	//get new width
    int newWidth = QInputDialog::getInt(this, tr("Edit Line Width"), tr("Select pen width:"), oldWidth, 1, 20, 1, &ok);

    if (ok)
		this->mPlottingOptions->setLineWidthForAttributePlot(newWidth);
		//this->mPlottingOptions->setLineWidthForAttributePlot(currentTree, newWidth);

	//refresh the attribute plots
	this->refreshAttributePlots();
}

//SLOT
void QtfyMain::editLineWidthTreePlot()
{
	bool ok;

	//find current tree
	ITree* currentTree;
	for(QVector<QSharedPointer<ITree>>::const_iterator treeIterator = trees.constBegin(); treeIterator != trees.constEnd(); ++treeIterator)
	{	
		currentTree = (*treeIterator).data();
		QString c_tn = currentTree->getTreeName();
		if (currentTree->getTreeName() == treeSetInformation[1])
			break;
	}
	//get previous width
	//int oldWidth = this->mPlottingOptions->getLineWidthForTreePlot(currentTree);
	int oldWidth = this->mPlottingOptions->getLineWidthForTreePlot();

	//get new width
    int newWidth = QInputDialog::getInt(this, tr("Edit Line Width"), tr("Select pen width:"), oldWidth, 1, 20, 1, &ok);

    if (ok)
		//this->mPlottingOptions->setLineWidthForTreePlot(currentTree, newWidth);
		this->mPlottingOptions->setLineWidthForTreePlot(newWidth);

	//refresh tree plot
	this->displaySelectedTree(true);
}

void QtfyMain::hasStaggerWavelengths(bool _value)
{
	this->staggerWls = _value;
}


void QtfyMain::setWindowFocus(QString _currTitle)
{
	for(QMap<QString, myMdiSubWindow*>::Iterator it = mSubwindows.begin(); it != mSubwindows.end(); ++it){
		myMdiSubWindow* currWindow = it.value();
		if(currWindow->windowTitle() == _currTitle)
			currWindow->setWindowActive(true);
		else
			currWindow->setWindowActive(false);
	}

	if(_currTitle != treeSetInformation[1]){
		
		if(this->staggerWls){
			QStringList list = _currTitle.split(":");
			QString chunkName = list[0];
			int indexOfDetect = chunkName.indexOf("Ch") + 2;
			QString detectionW = chunkName.mid(indexOfDetect, 1);

			this->m_TreeDataModel = m_TreeModels.value(detectionW);
			this->m_tvwDataTreeOverview->setModel(m_TreeModels.value(detectionW));
		}
	}
}


//SLOT
void QtfyMain::updateTreeModelRequest(QString _detectionWavelegth)
{
	this->m_TreeDataModel = m_TreeModels.value(_detectionWavelegth);
	this->m_tvwDataTreeOverview->setModel(m_TreeModels.value(_detectionWavelegth));
	this->updateAllViewers(this->m_tvwDataTreeOverview->selectedIndexes().at(0));
}


// SLOT
void QtfyMain::createDerivedParameter(QString _parameterName1, QString _chunkName1, QString _parameterName2, QString _chunkName2, QString _operationName, QString _newParameterName)
{
	// iterate through all trees in the treeset
	ITree* tree;
	for(QVector<QSharedPointer<ITree>>::const_iterator treeIterator = trees.constBegin(); treeIterator != trees.constEnd(); ++treeIterator)
	{	
		tree = (*treeIterator).data();
		QTFyAttributeTools::createCalculatorParameter(tree, _parameterName1, _chunkName1, _parameterName2, _chunkName2, _operationName, _newParameterName);

	}

	// when done with creation of new derived parameter or else math parameter
	// update also the attribute selection display
	AttributeDisplaySelection::getInst(this)->addNewParameterInChunk(_chunkName1, _newParameterName);
	AttributeDisplaySelection::getInst(this)->updateView(_chunkName1);

	// also update qtfymain memory of chunk parameters
	QVector<QString> parametersInChunk = this->chunkAttributes.value(_chunkName1);
	parametersInChunk.append(_newParameterName);
	this->chunkAttributes.insert(_chunkName1, parametersInChunk);

	// update message log
	emit updateMsgLog(QString(">New parameter %1 has been added under the category \"Calculator\". Please go to \"Add parameter\" to plot it...").arg(_newParameterName), Qt::darkMagenta);
}

// SLOT
void QtfyMain::showStartScreen()
{
	StartScreen* intro = new StartScreen(this, false);
	intro->show();
}

// SLOT
void QtfyMain::showChangeLog()
{
	ViewChangeLog* log = new ViewChangeLog();
	log->show();
}
