/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/****************************************************************************
**
** @ Laura Skylaki, 07.08.2014
**
**
****************************************************************************/

// PROJECT
#include "plotpickerhelp.h"

// QT
#include "QDebug"

PlotPickerHelp::PlotPickerHelp()
{

}

PlotPickerHelp::~PlotPickerHelp()
{

}

int PlotPickerHelp::findClosestCurvePointByValue(const QwtPlotCurve* _curve, const QwtDoublePoint &_pnt, double* _dist)
{
	double  minDist = _curve->maxYValue();
	int closestPoint = -1;

	int curveSize = _curve->dataSize();
	for (int i = 0; i < curveSize; ++i) {
		// curve point values
		double x1 = _curve->x(i);
		double y1 = _curve->y(i);

		// euclidean distance
		double x = x1 - _pnt.x();
		double y = y1 - _pnt.y();
		double dist;

		dist = pow(x,2)+pow(y,2);           //calculating distance by euclidean formula
		dist = sqrt(dist);  

		if (dist < minDist) {
			minDist = dist;
			closestPoint = i;
		}
	}
	
	if (_dist)
		*_dist = minDist;
	return closestPoint;
}


int PlotPickerHelp::findClosestCurvePointByPixelDistance(const QwtPlot* _plot, const QwtPlotCurve* _curve, const QwtDoublePoint &_pnt, double* _dist)
{
	double  minDist = 2000;
	int closestPoint = -1;

	int curveSize = _curve->dataSize();
	for (int i = 0; i < curveSize; ++i) {
		
		QwtDoublePoint pntCurve;
		pntCurve.setX(_curve->x(i));
		pntCurve.setY(_curve->y(i));

		QPoint pntCurveTransf = transformPointToPixelCoords(_plot, pntCurve);

		// euclidean distance

		double x = pntCurveTransf.x() - _pnt.x();
		double y = pntCurveTransf.y() - _pnt.y();
		double dist;

		dist = pow(x,2)+pow(y,2);           //calculating distance by euclidean formula
		dist = sqrt(dist);  

		if (dist < minDist) {
			minDist = dist;
			closestPoint = i;
		}
	}
	
	if (_dist)
		*_dist = minDist;
	return closestPoint;
}


QPoint PlotPickerHelp::transformPointToPixelCoords(const QwtPlot* _plot, const QwtDoublePoint &_pnt) const
{

	// transform the coordinates -- this gives pixels
	QwtScaleMap xMap = _plot->canvasMap(QwtPlot::xBottom);
	QwtScaleMap yMap = _plot->canvasMap(QwtPlot::yLeft);			

	QPoint transfCoord (xMap.transform(_pnt.x()), yMap.transform(_pnt.y())) ;

	return transfCoord;
}

/*!
    Translate a point from pixel into plot coordinates
    \return Point in plot coordinates
*/
QPoint PlotPickerHelp::invTransform(const QwtPlot* _plot, const QPoint &pos) const
{
    QwtScaleMap xMap = _plot->canvasMap(QwtPlot::xBottom);
    QwtScaleMap yMap = _plot->canvasMap(QwtPlot::yLeft);

    return QPoint(
        xMap.invTransform(pos.x()),
        yMap.invTransform(pos.y())
    );
}