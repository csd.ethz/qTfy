/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/****************************************************************************
**
** @ Eleni Skylaki, 16.07.2014
**
**
****************************************************************************/

#ifndef TREEPLOT_H
#define TREEPLOT_H

//PROJECT
#include "treeitem.h"
#include "qtfybackend/itree.h"
#include "plottingoptions.h"
#include "qtfyplot.h"

//QT
#include <qframe.h>
#include "qpainter.h"
#include <qpalette>

//QWT
#include "qwt/qwt_scale_map.h"
#include "qwt/qwt_plot_curve.h"
#include "qwt/qwt_symbol.h"
#include "qwt/qwt_math.h"
#include "qwt/qwt_data.h"
#include "qwt/qwt_legend.h"
#include "qwt/qwt_plot.h"

class TreePlot
{

public:

	/**
	* Constructor.
	* Initializes the data of the plot, current cell, current tree, current timepoint, sets the x axis time factor to the default (Hours)
	* @param treeSetInformation, vector with the current information of the selected tree/cell 
	*/
	TreePlot(QVector<QString> treeSetInformation, QWidget* parent = 0, QString _timeUnit = "Hours");

	/**
	* Destructor
	*/
	~TreePlot();

	/**
	* creates the tree model by using the calculateCell method and the returns the plot that was created.
	* @param cell, the first cell of the tree
	* @param maxGeneration, the max generation of the tree
	* @return the tree plot
	*/
	QwtPlot* createPlot(ITrack* cell, int maxGeneration, QString cName, QWidget *parent = 0);

	/**
	* returns a color from the namesOfStandardColors vector
	* @param nr, the color to be displayed
	* @return the color
	*/
	QColor getColor(int nr);

	/**
	*creates curve and then attaches it to the plot
	*@param title, the title of the curve
	*@param color, the color of the curve
	*@param data, the points of the curve
	*@return the created curve
	*/
	QwtPlotCurve* createCurve(QString title, QColor color, QVector<QPointF> data, int width);

	/** 
	*creates the  treePlot, by calculating the cell points and initializing the curves to be created.
	* @param lastPoint, the last point that was calculated
	* @param cell, the cell to be calculated
	 *@param partitionHeight, the height of the partition currently being plotted (always half of previous partition)
	 *@param i, an iterator for the color getter
	*/
	void calculateCell(QPointF lastPoint, ITrack* cell, int partitionHeight, uint i);

	/**
	* set the time factor of the tree plot (xFactor), for instance {Hours,Minutes,Seconds} etc
	* @param xf, the choosen x axis factor
	*/
	void setXFactor(float xf);

	void setPlottingOptions(PlottingOptions* _opt);

	PlottingOptions* getPlottingOptions();

	float getXFactor();

private:
	////clears plot
	//void TreePlot::ClearPlot();
	
	//list with all the created curves of the plot
    QList<QwtPlotCurve*> curves;

	//the plot
	QTFyPlot* plot;
	//QwtPlot* plot;  
	
	//max generation of the selected tree
	int maxGeneration;

	//selected cell
	QString cellName;

	//selected tree
	QString treeName;

	//selected timepoint
	int timepoint;

	//number of the selected cell
	int cNum;
	
	//has the user selected a timepoint?
	bool timepointFound;

	int colorCount;

	//the x axis time factor
	float xFactor;
	QString timeUnit;

	double timepointValue;

	double sumLifetime;

	int maxCellY;
	int minCellY;

	PlottingOptions* mOptions;
};
#endif