/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TREEOVERVIEWITEMVIEW_H
#define TREEOVERVIEWITEMVIEW_H

#include <QTreeView>
#include <QKeyEvent>

class TreeOverviewItemView: public QTreeView
{
	Q_OBJECT

public:
    TreeOverviewItemView(QWidget *parent = 0):QTreeView(parent)
    {}

	QModelIndexList selectedIndexes() const;

	void setHideMode(bool hidden) {this->hideMode = hidden; }
	bool isHideModeOn() {return hideMode;}

public slots:
	void outIndexChangeUp();
	void outIndexChangeDown();


protected:
    void keyPressEvent(QKeyEvent * event);
	

private:

	QModelIndex findNextVisibleIndexAbove(const QModelIndex _currentIndex);
	QModelIndex findNextVisibleIndexBelow(const QModelIndex _currentIndex);

	bool hideMode;
	
};



#endif // TREEOVERVIEWITEMVIEW_H
