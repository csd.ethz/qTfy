/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Stavroula Skylaki
 * @date 12.04.2016
 */


#ifndef PARAMETERCALCULATOR_H
#define PARAMETERCALCULATOR_H

#include <QDialog>
#include "ui_frmParameterCalculator.h"

class ParameterCalculator : public QDialog
{
	Q_OBJECT

public:

	ParameterCalculator(QWidget *parent = 0);	
	~ParameterCalculator();

	void initializeData(QHash<QString, QVector<QString>> _chunkAttributes, QHash<QString, QString> _chunkSoftware, QHash<QString, QString> _chunkRemarks);

	public slots:
		void cancel();	


signals:
	void successfullParameterSelection(QString, QString, QString, QString, QString, QString);

private slots:
	void getParameterSelection();
	void showHelpCalculator();
	

private:	

	QString createDefaultNewParameterName(QString _parameterName1, QString _chunkName1, QString _parameterName2, QString _chunkName2, QString _operationName);

	//@key the quantification chunk name
	//@value vector of quantification chunk col names
	QHash<QString, QVector<QString>> allAttributes;
	QHash<QString, QString> allChunkSoftware;
	QHash<QString, QString> allChunkRemarks;

	// The ui
	Ui::frmParameterCalculator ui;

public:

};


#endif // PARAMETERCALCULATOR_H