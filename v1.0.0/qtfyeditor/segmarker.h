/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/****************************************************************************
**
** @ Laura Skylaki, 09.01.2014
**
**
****************************************************************************/


#ifndef SEGMARKER_H
#define SEGMARKER_H

// QT
#include <QColor>
#include <qpoint.h>

// PROJECT
#include "qtfybackend/tools.h"


class SegMarker
{

public:

	// constructor
	SegMarker() {};

	SegMarker(int _X, int _Y, int _cellID, QString _treeName,  QString _expName, QString _positionIndex, int _timepointNumber, QString _markerName, QColor _markerColour):
	  X(_X), Y(_Y), cellID(_cellID), treeName(_treeName), experimentName(_expName), positionIndex(_positionIndex), timepointNumber(_timepointNumber), 
		  markerName(_markerName), markerColour(_markerColour), coord (QPoint(_X, _Y))
	  {};

	  ~SegMarker() {};

	// the getters
	int getX() { return X;}
	int getY() { return Y;}
	QPoint getCoordPoint() { return coord;}
	int getCellID() { return cellID;}
	QString getTreeName() { return treeName;}
	QString getExperimentName() { return experimentName;}
	QString getPositionIndex() {return positionIndex;}
	int getPositionNumber() {
		QString findPos = Tools::getPositionNumberFromString(this->positionIndex);
		return findPos.toInt();}
	int getTimepointNumber() { return timepointNumber;}
	QString getMarkerName() { return markerName;}
	QColor getMarkerColour() { return markerColour;}

	// the setters

	void setX(int _x) { X = _x;}
	void setY(int _y) { Y = _y;}
	void setCoordPoint (int _x, int _y) { coord = QPoint (_x , _y);}
	void setCellID(int _cellID) { cellID = _cellID;}
	void setTreeName(QString _treeName) {treeName = _treeName;}
	void setExperimentName(QString _expName) {experimentName = _expName;}
	void setPositionIndex(QString _positionIndex) {positionIndex = _positionIndex;}
	void setTimepointNumber(int _timepointNumber) {timepointNumber = _timepointNumber;}
	void setMarkerName(QString _markerName) {markerName = _markerName;}
	void setMarkerColour(QColor _markerColour) {markerColour = _markerColour;}

private:

	// marker coordinates
	int X;
	int Y;

	// the coordinates as a point
	QPoint coord;
	
	// the cell it belongs to
	int cellID;

	// the tree it belongs to
	QString treeName;

	// the experiment it belongs to
	QString experimentName;

	// the position it appears
	QString positionIndex;

	// the timepoint it appears
	int timepointNumber;

	// the marker name as it appears on screen
	QString markerName;

	// the colour used when it's painted on screen
	QColor markerColour;

};

#endif