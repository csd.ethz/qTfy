/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Laura Skylaki
 * @date 14/05/2013
 */

#include "treemodel.h"
#include <QIcon>

TreeModel::TreeModel(const QList<QVariant> &RootData, QObject *parent)
    : QAbstractItemModel(parent)
{
    rootItem = new TreeItem(RootData);
	setIsCellBased(true);
	setIsIndexColourAvailable(true);
	this->colouredIndexes = new QMap<int, QModelIndex>();
}


TreeModel::~TreeModel()
{
    delete rootItem;
	delete colouredIndexes;
}

//returns specific index

QModelIndex TreeModel::index(int row, int column, const QModelIndex &parent) const
{
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    TreeItem *parentItem;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    TreeItem *childItem = parentItem->child(row);
    if (childItem)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();
}

//returns the parent of an index

QModelIndex TreeModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    TreeItem *childItem = static_cast<TreeItem*>(index.internalPointer());
    TreeItem *parentItem = childItem->parent();

    if (parentItem == rootItem)
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
}

//returns the number of children of parent item

int TreeModel::rowCount(const QModelIndex &parent) const
{
    TreeItem *parentItem;
    if (parent.column() > 0)
        return 0;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    return parentItem->childCount();
}

//returns number of colums of data of parent item

int TreeModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return static_cast<TreeItem*>(parent.internalPointer())->columnCount();
    else
        return rootItem->columnCount();
}

QVariant TreeModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if ((role != Qt::DisplayRole) && (role != Qt::DecorationRole) && (role != Qt::ForegroundRole))
            return QVariant();

    TreeItem *item = static_cast<TreeItem*>(index.internalPointer());

	if(isCellBased){
		if (role == Qt::DecorationRole){
			switch (item->column()) {
			case 0:
				return QIcon(":/qtfyres/qtfyres/tree.png");
				break;
			case 1:
				return QIcon(":/qtfyres/qtfyres/cell.png");
				break;
			case 2:
				return QIcon(":/qtfyres/qtfyres/time.png");
				break;
			default:
				return QVariant();
			}
		}
	}else{
		if (role == Qt::DecorationRole){
			switch (item->column()) {
			case 0:
				return QIcon(":/qtfyres/qtfyres/position.png");
				break;
			case 1:
				return QIcon(":/qtfyres/qtfyres/tree.png");
				break;
			case 2:
				return QIcon(":/qtfyres/qtfyres/cell.png");
				break;
			case 3:
				return QIcon(":/qtfyres/qtfyres/time.png");
				break;
			default:
				return QVariant();
			}
		}

		

	}
    if (role == Qt::DisplayRole) {
        return item->data(index.column());
    }

	if (role == Qt::ForegroundRole && isIndexColourAvailable && item->column()==3 ){
		if ( this->colouredIndexes->values().contains(index) ) {
			return QVariant( QColor( Qt::red ) );
		}
		else {
			return QVariant( QColor( Qt::gray ) );
		}
	}
	else {
		return QVariant( QColor( Qt::black ) );
	}


}


Qt::ItemFlags TreeModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return 0;

    return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}

QVariant TreeModel::headerData(int section, Qt::Orientation orientation,
                               int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return rootItem->data(section);

    return QVariant();
}

TreeItem* TreeModel::getRoot () const
{
    return rootItem;
}

bool TreeModel::removeRows(int position, int rows, const QModelIndex &parent)
{

    beginRemoveRows(QModelIndex(), position, position+rows-1);

//    QModelIndex startNode = index(position, 0);

//    for (int row = 0; row < rows; ++row) {
//        stringList.removeAt(position);
//    }

    endRemoveRows();
    return true;

}

void TreeModel::setIsCellBased (bool isCB)
{
	isCellBased = isCB;
}

bool TreeModel::getIsCellBased ()
{
	return isCellBased;
}


void TreeModel::setQuantificationInformation(QString _chunkName, QString _detectionWavelength, QString _segMethodID)
{
	this->quantificationInformation << _chunkName << _detectionWavelength << _segMethodID;
}

QString TreeModel::getChunkName()
{
	if(quantificationInformation.isEmpty())
		return "";
	else
		return quantificationInformation[0];
}

QString TreeModel::getDetectionWavelength()
{
	if(!quantificationInformation.isEmpty()){
		if(quantificationInformation.size()>1)
			return quantificationInformation[1];
	}
	return "";
}

QString TreeModel::getSegmentationMethodID()
{
	if(!quantificationInformation.isEmpty()){
		if(quantificationInformation.size()>2)
			return quantificationInformation[2];
	}
	return "";
}
