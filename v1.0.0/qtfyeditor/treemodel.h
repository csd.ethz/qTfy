/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TREEMODEL_H
#define TREEMODEL_H

/**
 * @author Laura Skylaki
 * @date 14/05/2013
 */

// PROJECT
#include "treeitem.h"

// QT
#include <QAbstractItemModel>
#include <QMap>
#include <QVector>

class TreeModel : public QAbstractItemModel
 {
     Q_OBJECT

 public:

    /**
      * @brief TreeModel constructor
      * @param parent
      */
     TreeModel(const QList<QVariant> &RootData, QObject *parent = 0);

     /**
      * TreeModel destructor
      * destruction of the root item and all its descendants
      */
     ~TreeModel();

     /**
      * @brief data
      * @param index
      * @param role
      * @return
      */
     QVariant data(const QModelIndex &index, int role) const;

     /**
      * @brief flags
      * @param index
      * @return
      */
     Qt::ItemFlags flags(const QModelIndex &index) const;

     /**
      * @brief headerData
      * @param section
      * @param orientation
      * @param role
      * @return
      */
     QVariant headerData(int section, Qt::Orientation orientation,
                         int role = Qt::DisplayRole) const;

     /**
      * @brief index
      * @param row
      * @param column
      * @param parent
      * @return
      */
     QModelIndex index(int row, int column,
                       const QModelIndex &parent = QModelIndex()) const;

     /**
      * @brief parent
      * @param index
      * @return
      */
     QModelIndex parent(const QModelIndex &index) const;

     /**
      * @brief getRoot
      * @return the model's root item
      */
     TreeItem* getRoot () const;

     int rowCount(const QModelIndex &parent = QModelIndex()) const;
     int columnCount(const QModelIndex &parent = QModelIndex()) const;
     bool removeRows(int position, int rows, const QModelIndex &parent);
	 
	 void setIsCellBased (bool isCB);
	 bool getIsCellBased ();

	 void setIsIndexColourAvailable (bool _indexesWithColour) { isIndexColourAvailable = _indexesWithColour; }
	 bool getIsIndexColourAvailable () { return isIndexColourAvailable; }

	 QMap<int, QModelIndex>* getColouredIndexes() const { return colouredIndexes; }
	 void setColouredIndexes(QMap<int, QModelIndex>* _val) { colouredIndexes = _val; }

	 void setQuantificationInformation(QString _chunkName="", QString _detectionWavelength = "", QString _segMethodID = "");
	 QString getChunkName();
	 QString getDetectionWavelength();
	 QString getSegmentationMethodID();

 private:

	 //the quantification information consist of : 1) chunk name, 2) detection wavelength, 3) segmentation method ID
	 QVector<QString> quantificationInformation;

     TreeItem *rootItem;
	 bool isCellBased;

	 bool isIndexColourAvailable;
	 QMap <int, QModelIndex> *colouredIndexes;
	 
};
#endif // TREEMODEL_H
