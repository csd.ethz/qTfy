/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/****************************************************************************
**
** @ Laura Skylaki, 16.01.2016
**
**
****************************************************************************/

#ifndef HEATTREEPLOT_H
#define HEATTREEPLOT_H

//PROJECT
#include "treeitem.h"
#include "qtfybackend/itree.h"
#include "plottingoptions.h"
#include "qtfyplot.h"

//QT
#include <QGraphicsView>


class HeattreePlot : public QGraphicsView
{
	Q_OBJECT

public:

	/**
	* Constructor.
	* Initializes the data of the plot, current cell, current tree, current timepoint, sets the x axis time factor to the default (Hours)
	* @param treeSetInformation, vector with the current information of the selected tree/cell 
	*/
	HeattreePlot(QVector<QString> treeSetInformation, QString attribute, QString quantiChunk, QWidget* parent = 0, QString _timeUnit = "Hours");

	/**
	* Destructor
	*/
	~HeattreePlot();

	/**
	* creates the tree model by using the calculateCell method and the returns the plot that was created.
	* @param cell, the first cell of the tree
	* @param maxGeneration, the max generation of the tree
	* @return the tree plot
	*/
	void createPlot(ITrack* cell, int maxGeneration);

	void exportToImage ();

	/**
	* returns a color from the namesOfStandardColors vector
	* @param nr, the color to be displayed
	* @return the color
	*/
	QColor getColor(int nr);

	/**
	*creates curve and then attaches it to the plot
	*@param title, the title of the curve
	*@param color, the color of the curve
	*@param data, the points of the curve
	*@return the created curve
	*/
	void createTimepointRect(int _xTimepoint, int _nextTimepoint, int _yMin, int _yMax, QColor _c);

	void createCellBorder(int _startTimepoint, int _endTimepoint, int _yMin, int _yMax, QColor _borderColor);

	void plotCell(ITrack* _cell, int _minY, int _maxY , QColor _c);

	int plotCellConcatenate(ITrack* _cell, int _minY, int _maxY , QColor _c, uint _timeCounter);

	void getHeatColorRange(QVector<ITrack*> _cellList);

	float getValuePercentile(double _aVal, QList<double> allValues);

	void plotLegend(int _x, int _y, int _width, int _height);

	QColor getColorForTimepoint (float _percent);

	/** 
	*creates the  treePlot, by calculating the cell points and initializing the curves to be created.
	* @param lastPoint, the last point that was calculated
	* @param cell, the cell to be calculated
	*@param partitionHeight, the height of the partition currently being plotted (always half of previous partition)
	*@param i, an iterator for the color getter
	*/
	void calculateCell(ITrack* _cell, int _minY, int _maxY, uint i, uint _timeCounter);

	/**
	* set the time factor of the tree plot (xFactor), for instance {Hours,Minutes,Seconds} etc
	* @param xf, the choosen x axis factor
	*/
	void setXFactor(float xf);

	void setPlottingOptions(PlottingOptions* _opt);

	PlottingOptions* getPlottingOptions();

	float getXFactor();

private:

	// the minimum height of a cell bar
	int minCellHeight;
	// the minimum width of a cell bar
	int minCellWidth;
	// the default height of a cell bar
	int basicCellHeight;
	// the default width of a cell bar
	int basicCellWidth;

	// the end of the tree
	int treeMaxX;

	//max generation of the selected tree
	int maxGeneration;

	//selected cell
	QString cellName;

	//selected tree
	QString treeName;

	// the attribute
	QString attributeName;

	// the quantification chunk
	QString quantificationChunk;

	//selected timepoint
	int timepoint;

	//number of the selected cell
	int cNum;

	// the values
	QList<double> allValues;

	//has the user selected a timepoint?
	bool timepointFound;

	int colorCount;

	//the x axis time factor
	float xFactor;
	QString timeUnit;

	double timepointValue;

	double sumLifetime;

	int maxCellY;
	int minCellY;

	// the scene
	QGraphicsScene *scene;

	PlottingOptions* mOptions;
};
#endif