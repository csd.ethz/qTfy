/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/****************************************************************************
**
** @ Eleni Skylaki 12.08.2014
**
**
****************************************************************************/

#ifndef	MYMDISUBWINDOW_H
#define MYMDISUBWINDOW_H

//PROJECT
//#include "qtfyplot.h"
//QT
#include <QWidget>
#include <QMdiSubWindow>

class myMdiSubWindow:public QMdiSubWindow
{
	Q_OBJECT

public:

	//constructor
	myMdiSubWindow( QWidget * parent = 0, Qt::WindowFlags flags = 0 );
	//destructor
	~myMdiSubWindow();

	/**
	Sets the activation state of this window
	@param _activationState, flag for active or inactive subwindow
	*/
	void setWindowActive(bool _activationState);

	/**
	Sets the chunk name if an attribute is plotted in this subwindow
	@param _chunkName, the chunk name of the attribute
	*/
	void setAttributeChunkName(QString _chunkName)
	{this->attributeChunkName = _chunkName;}

	/**
	Returns the chunk name of the attribute that is plotted in this subwindow
	@return QString the chunk name
	*/
	QString getAttributeChunkName(){
		return this->attributeChunkName;
	}

	/**
	Sets the attribute name if an attribute is plotted in this subwindow
	@param _attrName, the attribute name
	*/
	void setAttributeName(QString _attrName)
	{this->attributeName = _attrName;}

	/**
	Returns the attribute name of the attribute that is plotted in this subwindow
	@return QString the attribute name
	*/
	QString getAttributeName()
	{ return this->attributeName;}

	bool isWindowActive();

signals:
    void closed( myMdiSubWindow* _subwindow);
	
	void sizeChanged( const QWidget* _widg);

protected:

	void closeEvent ( QCloseEvent * closeEvent );

private:

	bool windowActive;

	QString attributeChunkName;

	QString attributeName;

};
#endif //MYMDISUBWINDOW_H
