/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/****************************************************************************
**
** @ Eleni Skylaki, Laura Skylaki, 12.08.2014
**
**
****************************************************************************/


// PROJECT
#include "qtfyplot.h"
#include "plotpickerhelp.h"
#include "qtfydata/experiment.h"
#include "qtfybackend/itree.h"
#include "mymdisubwindow.h"
#include "qtfyplotmarker.h"

// QWT
#include "qwt/qwt_scale_widget.h"
#include "qwt/qwt_plot.h"

// QT
#include <QWheelEvent>
#include <QMouseEvent>
#include <QToolTip>
#include <QFileDialog>
#include <qdebug.h>
#include <qpixmap.h>

QTFyPlot::QTFyPlot (QWidget *parent):
QwtPlot(parent)
{
	this->setMouseTracking(true);
	subwindow = new myMdiSubWindow();

}

QTFyPlot::~QTFyPlot () 
{
	// TODO
	// detachAll();

	//delete myPlotPicker;
}

void QTFyPlot::installPlotPicker (int _XaxisId, int _YaxisId)
{
}

void QTFyPlot::mousePressEvent(QMouseEvent *event)
{
	QPoint pointPos = event->pos();

	// left click from user
	if (event->button() == Qt::LeftButton) {

		QSize canvasSize = this->canvas()->size();
		QSize plotSize = this->size();

		int xDiff = plotSize.width() - canvasSize.width();

		QwtDoublePoint newPos;
		newPos.setX(pointPos.x() - xDiff);
		newPos.setY(pointPos.y());
		
		emit clickedPoint(newPos);
		
	}
	// right click from user
	else if (event->button() == Qt::RightButton){

	}		

	if (event->button() == Qt::MiddleButton){

		setCursor(Qt::ClosedHandCursor);
		event->accept();
		return;
	}

	event->accept();

}

void QTFyPlot::mouseReleaseEvent(QMouseEvent *event)
{
	event->accept();
}

void QTFyPlot::mouseMoveEvent(QMouseEvent *event)
{
	event->accept();
}

// SLOT
void QTFyPlot::plotPointSelected(const QwtDoublePoint &pnt) 
{
	emit clickedPoint(pnt);

}

//SLOT
void QTFyPlot::scaleDivChangedSlot ()
{
	
	QwtPlot* plot = static_cast<QwtPlot*>(sender()->parent());

	if(!plot)
		return;

	QwtDoubleInterval intv = plot->axisScaleDiv(QwtPlot::xBottom)->interval();//axisInterval (QwtPlot::xBottom);
	double min = intv.minValue();
	double max = intv.maxValue();
	this->setAxisScale(QwtPlot::xBottom, intv.minValue(), intv.maxValue());
	intv = plot->axisScaleDiv(QwtPlot::yLeft)->interval();//plt->axisInterval (QwtPlot::yLeft);
	this->setAxisScale(QwtPlot::yLeft, intv.minValue(), intv.maxValue());
	this->replot();

}

void QTFyPlot::changeCurveColor(ITrack* _cell, QColor _color/*, bool _transparent*/)
{
	//curve name we want to change
	QString curveName = QString::number(_cell->getTrackNumber());
	//in case of smooth lines or original points set the correct name of the curve as well
	QString smoothCurveName = "S" + curveName;
	QString pointsCurveName = "P" + curveName;

	QwtPlotItemList listOfPlotItems = itemList();

	for (QwtPlotItemList::iterator it = listOfPlotItems.begin(); it != listOfPlotItems.end(); ++it) { 

		// find the type of the plot item	
		int type = (*it)->rtti();
		//if curve
		if (type == QwtPlotItem::Rtti_PlotCurve) {
			QwtPlotCurve* currentCurve = (QwtPlotCurve*) (*it);
			QString currentTitle = currentCurve->title().text();
			//if it is the curve we want
			if(curveName == currentTitle || currentTitle == smoothCurveName || currentTitle == pointsCurveName){
				QPen newPen = currentCurve->pen();
				newPen.setColor(_color);
				currentCurve->setPen(newPen);

				if(currentTitle == pointsCurveName){
				//change the symbol color
				QwtSymbol newSymbol = currentCurve->symbol();
				QBrush newBrush = newSymbol.brush();
				newBrush.setColor(_color);
				newSymbol.setBrush(newBrush);
				newSymbol.setPen(newPen);
				currentCurve->setSymbol(newSymbol);
				}
			}

		}
			
	}
	replot();
	return;
}

void QTFyPlot::hideBranch(QVector<ITrack*> _cellList, bool _transparent, bool _isTreePlot)
{
	int transparentLevel;
	if(_transparent)
		transparentLevel = 0;
	else
		transparentLevel = 255;

	QVector<int> cellNumbers;
	for(QVector<ITrack*>::iterator i = _cellList.begin(); i != _cellList.end(); ++i){
		ITrack* currCell = (*i);
		cellNumbers<<currCell->getTrackNumber();
	}

	QwtPlotItemList listOfPlotItems = itemList();

	for (QwtPlotItemList::iterator it = listOfPlotItems.begin(); it != listOfPlotItems.end(); ++it) { 

		// find the type of the plot item	
		int type = (*it)->rtti();

		//if curve
		if (type == QwtPlotItem::Rtti_PlotCurve) {
			QwtPlotCurve* currentCurve = (QwtPlotCurve*) (*it);
			QString currentTitle = currentCurve->title().text();
			if(currentTitle == "timepoint")
				continue;
			//get correct cellNum
			if(!currentTitle.contains("D")){
				int cellNum;
				//if it is a points only curve
				QString tmp = currentTitle;
				if(currentTitle.contains("P"))
					cellNum = tmp.remove("P").toInt();
				else if(currentTitle.contains("S"))
					cellNum = tmp.remove("S").toInt();
				else if(currentTitle.contains("O"))
					cellNum = tmp.remove("O").toInt();
				else 
					cellNum = currentTitle.toInt();

				//if it is the curve of one of the given cells
				if(cellNumbers.contains(cellNum)){
					//if attribute plot and hide
					if(currentTitle.contains("P")){
						//change color in the symbol
						QwtSymbol newSymbol = currentCurve->symbol();
						QBrush newBrush = newSymbol.brush();
						QColor c = newBrush.color();
						c.setAlpha(transparentLevel);
						newBrush.setColor(c);
						newSymbol.setBrush(newBrush);
						newSymbol.setPen(QPen(c));
						//newSymbol.setPen(newPen);

						currentCurve->setSymbol(newSymbol);
					}else{

					//change the transparency level of the color
					QPen newPen = currentCurve->pen();
					QColor color = newPen.color();
					color.setAlpha(transparentLevel);
					newPen.setColor(color);
					currentCurve->setPen(newPen);
					}
					//for cell curves, if we want to show the cell, make marker for the label
					//if(!_transparent && _isTreePlot)
					//{
					//	QwtPlotMarker *m_marker = new QwtPlotMarker();

					//	m_marker->setLabel(currentTitle);
					//	m_marker->setLabelAlignment(Qt::AlignCenter|Qt::AlignTop);
					//	m_marker->setYValue(currentCurve->maxYValue());
					//	m_marker->setXValue(currentCurve->minXValue() + ((currentCurve->maxXValue() - currentCurve->minXValue())/2));//(data[1].rx() + data[0].rx())/2);
					//	m_marker->attach(this);
					//}
				}
			}
			else{
				QString cellName = currentCurve->title().text();
				int numIndex = currentTitle.length() - (currentTitle.lastIndexOf("D") + 1);
				int cellNum = (currentTitle.right(numIndex)).toInt();
				//if it is the default curve of one of the given cells
				if(cellNumbers.contains(cellNum)){
					//change the transparency level of the color
					QPen newPen = currentCurve->pen();
					QColor color = newPen.color();
					color.setAlpha(transparentLevel);
					newPen.setColor(color);
					currentCurve->setPen(newPen);

				}

			}
		}else if(type == QwtPlotItem::Rtti_PlotMarker){//if QwtPlotMarker
			//check if fate marker
			QTFyPlotMarker* qtfymarker =dynamic_cast<QTFyPlotMarker*>(*it);
			if(!qtfymarker){//cell number label
				QwtPlotMarker* marker = (QwtPlotMarker*) (*it);
				QString label = marker->label().text();
				int cellNum = label.toInt();
				//if the label of the given cells
				if(cellNumbers.contains(cellNum)){
					marker->setVisible(!_transparent);
				}
			}else{ //fate label
				QString label = qtfymarker->getTitle();
				int cellNum = label.toInt();
				//if the label of the given cells
				if(cellNumbers.contains(cellNum)){
					qtfymarker->setVisible(!_transparent);
				}
			}
		}
	}
}

void QTFyPlot::detachCurve(QString _curveTitle){

	QwtPlotItemList listOfPlotItems = itemList();

	for (QwtPlotItemList::iterator it = listOfPlotItems.begin(); it != listOfPlotItems.end(); ++it) { 

		// find the type of the plot item	
		int type = (*it)->rtti();

		//if curve
		if (type == QwtPlotItem::Rtti_PlotCurve) {
			QwtPlotCurve* currentCurve = (QwtPlotCurve*) (*it);
			QString currentTitle = currentCurve->title().text();

			if(currentTitle == _curveTitle){
				currentCurve->detach();
			}
		}
	}
}

void QTFyPlot::boldCurve(QString _curveTitle, bool _fromTreePlot, int _width)
{
	QwtPlotItemList listOfPlotItems = itemList();

	for (QwtPlotItemList::iterator it = listOfPlotItems.begin(); it != listOfPlotItems.end(); ++it) { 

		// find the type of the plot item	
		int type = (*it)->rtti();

		//if curve
		if (type == QwtPlotItem::Rtti_PlotCurve) {
			QwtPlotCurve* currentCurve = (QwtPlotCurve*) (*it);
			QString currentTitle = currentCurve->title().text();

			if(currentTitle == "timepoint")
				continue;

			if(_fromTreePlot){
				if(_curveTitle.contains("S"))
					_curveTitle.remove("S");
			}

			QPen newPen = currentCurve->pen();
			//if the curve selected, higlight
			if(currentTitle == _curveTitle){
				//detach curve from plot
				currentCurve->detach();
				//if(_fromTreePlot)
					//newPen.setWidth(_width + 2);
				//else
					newPen.setWidth(_width + 2);
				currentCurve->setPen(newPen);
				//reattach curve
				currentCurve->attach(this);
			}else if (newPen.width() == _width +2 ){//|| newPen.width() == 4){ //if not the cell selected, in case it was previously highlighted set as normal, don't do if points curve
				//if(_fromTreePlot)
					//newPen.setWidth(3);
				//else
					newPen.setWidth(_width);

				currentCurve->setPen(newPen);
			}

			//bold original points for this curve as well
			if(currentTitle.contains("P")){
				currentTitle.remove("P");
				QwtSymbol sym = currentCurve->symbol();

				QString tmp = _curveTitle;
				if(_curveTitle.contains("S"))
					tmp = tmp.remove("S");

				if(currentTitle == tmp){//if selected cell points, bold them
					//sym.setSize(8);
					sym.setSize(3*_width);
					currentCurve->setSymbol(sym);
				}else if (sym.size().width() == _width/3){//else if previously bolded points, set to normal
					sym.setSize(6);
					currentCurve->setSymbol(sym);
					currentCurve->setPen(newPen);
				}
			}

		}
	}
	replot();
}

//set the subwindow where the plot is
void QTFyPlot::setSubWindow(myMdiSubWindow* _window)
{
	this->subwindow = _window;
}

//get the plot's subwindow
myMdiSubWindow* QTFyPlot::getSubWindow()
{
	return subwindow;
}

QPixmap QTFyPlot::exportPNG()
{
		QwtPlotPrintFilter filter;

		int   options = QwtPlotPrintFilter::PrintFrameWithScales;
 		options   |=  QwtPlotPrintFilter::PrintBackground;
		filter.setOptions(options);
 
		QPixmap pixmap= QPixmap::grabWidget(this);
		
		if(!pixmap)
			return NULL;
		else{

			return pixmap;
		}

}

QwtPlotCurve* QTFyPlot::getCurve(QString _curveName)
{
	QwtPlotItemList listOfPlotItems = itemList();

	for (QwtPlotItemList::iterator it = listOfPlotItems.begin(); it != listOfPlotItems.end(); ++it) { 

		// find the type of the plot item	
		int type = (*it)->rtti();
		//if curve
		if (type == QwtPlotItem::Rtti_PlotCurve) {
			QwtPlotCurve* currentCurve = (QwtPlotCurve*) (*it);
			QString currentTitle = currentCurve->title().text();

			if(currentTitle == _curveName)
				return currentCurve;
		}
	}

	return NULL;
}
