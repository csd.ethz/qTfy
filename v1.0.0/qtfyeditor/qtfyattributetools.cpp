/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "qtfyattributetools.h"

//PROJECT
#include "qtfydata/experiment.h"
#include "qtfydata/treestructuralhelper.h"
#include "qtfyanalysis/positionquantification.h"
//QT

QTFyAttributeTools::QTFyAttributeTools()
{}

QTFyAttributeTools::~QTFyAttributeTools(){}

void QTFyAttributeTools::findAttributesPerQuantificationChunk(QVector<QSharedPointer<ITree>> _trees, 
	QHash<QString, QVector<QString>>& _chunkAttributes, 
	QHash<QString, QString>& _chunkSoftware, 
	QHash<QString, QString>& _chunkRemarks)
{
	/*iterate through trees,
	 *when new attribute (column name of a quantification chunk) is found
	 */


	for(QVector<QSharedPointer<ITree>>::iterator it = _trees.begin(); it != _trees.end(); ++it){
		ITree* currentTree = it->data();
		CellLineageTree* currentData = currentTree->getCellLineageTree();

		int chunksNum = currentData->getNumberOfQuantificationChunks();

		//iterate through chunks
		for(int i = 0; i < chunksNum; i++){
			QString quantName = currentData->getQuantificationName(i);
				
			int sumOfMorph= 0;
			if (!quantName.contains("QTFy"))
				continue;

			QString quantSoftware = currentData->getQuantificationSoftware(i);
			QString quantRemarks = currentData->getQuantificationRemarks(i);

			_chunkSoftware.insert(quantName,quantSoftware);
			_chunkRemarks.insert(quantName,quantRemarks);


			QVector<QString> columnNames = currentData->getQuantificationColumnNames(i);
			for(int column = 0; column < columnNames.size(); column++){
				//check if active or inspected column
				if(columnNames[column].toLower() == "active" || columnNames[column].toLower() == "activetps")
					continue;				
				if(columnNames[column].toLower() == "inspected")
					continue;

				QString attrName = columnNames[column];
				if(_chunkAttributes.contains(quantName)){
					QVector<QString> existingAttributes = _chunkAttributes.value(quantName);
					if(!existingAttributes.contains(attrName)){
						existingAttributes.append(attrName);
						_chunkAttributes.insert(quantName, existingAttributes);
					}else
						continue;
				}else{

					QVector<QString> existingAttributes;
					existingAttributes.append(attrName);
					_chunkAttributes.insert(quantName, existingAttributes);
				}
			}

		}
	}

}


//TODO::checks
//+ checks for NaN values
QMap<int, QVariant> QTFyAttributeTools::getTimedValue(const ITrack &_cell, QString _attrName, QString _quantChunkName)
{
	//the return item
	QMap<int, QVariant> data;

	//CellLineage object of the current tree
	CellLineageTree* cltData = _cell.getITree()->getCellLineageTree();

	//if invalid cell lineage tree object
	if(!cltData){
		qDebug()<<"Error:: Ivalid CellLineageTree object, getTimedValues.";
		return data;
	}

	// Get the tree name
	QString treeName = _cell.getITree()->getTreeName();

	//Get the cell number
	int number = _cell.getTrackNumber();

	//get the quantification index

	int quantIndex;// = findQuantificationIndex(_attrName, cltData);
	quantIndex = cltData->getQuantificationIndexFromName(_quantChunkName);

	//invalid quantification index
	if(quantIndex == -1){
		qDebug()<<"Invalid quantification index, getTimedValue.";
		return data;
	}

	//get active column index
	int activeIndex = -1;
	
	QList<CellLineageTree::FrameIndex> frameIndex = cltData->getQuantificationFrameIndexes(quantIndex, _cell.getTrackNumber());
	qSort(frameIndex);

	if(frameIndex.isEmpty()){
		return data;		}

	//keep attribute index colum
	int attrIndex = -1;
	QVector<QString> columnNames;
	columnNames = cltData->getQuantificationColumnNames(quantIndex);

	//if there is no active/ispected column, create them
	if(!columnNames.contains("active")){
		//if addition of column failed, return
		if(!addActiveColumn(quantIndex, cltData)){
			qDebug()<<"Error with the addition of active column!";
			return data;
		}
	}

	if(!columnNames.contains("inspected")){
		//if addition of column failed, return
		if(!addInspectedColumn(quantIndex, cltData)){
			qDebug()<<"Error with the addition of inspected column!";
			return data;
		}
	}

	//update data
	columnNames = cltData->getQuantificationColumnNames(quantIndex);

	for(int id = 0; id < columnNames.size(); id++){
		if(columnNames[id] == _attrName)
			attrIndex = id;
	}

	activeIndex = findActivationIndex(quantIndex, cltData);

	if(attrIndex < 0){
		qDebug()<<"Couldn't find attribute in the colum names of the quantification chunk.";
		return data;
	}else if(activeIndex == -1){
		qDebug()<<"Couldn't find active attribute in the colum names of the quantification chunk.";
		return data;
	}


	//iterate through the frame indexes of the specific cell and quantification chunk
	for(int i = 0; i < frameIndex.size(); i++){
		//keep the timepoint
		CellLineageTree::FrameIndex frameData = frameIndex[i];
		int timepoint = frameData[0];

		int preIncTime = (Experiment::isUsePreincubationTime()) ?_cell.getITree()->getTreeProperty(ITree::TREE_PREINCUBATION_TIME).toInt() : 0;
		int firstCellEvent = _cell.getAbsoluteSecondsForTP(_cell.getFirstTimePoint());
		int time = _cell.getSecondsForTimePoint(timepoint) + firstCellEvent + preIncTime;
		//int time = _cell.getSecondsForTimePoint(timepoint) + firstCellEvent;

		QVariant value;
		bool active;

		//get the value of the attribute
		QVector<QVariant> values = cltData->getQuantificationDataPoint(quantIndex, _cell.getTrackNumber(), frameIndex[i]);

		//check for invalid values
		if(values.isEmpty())
			qDebug()<<"In quantification data, returned invalid value";

		//check if timepoint is deactivated
		active = values[activeIndex].toBool();
		if(!active)
			continue;

		//check if value is NaN
		value = values[attrIndex];

		//PRINT CASES
		////if we don't want to print the data and the value is empty, continue
		//if(value.type() == QVariant::Invalid && !this->forPrint)
		//	continue;

		//double tmpValue = value.toDouble();

		////if we don't want to print the data and the value is NaN, continue
		//if(tmpValue != tmpValue && !this->forPrint)
		//	continue;

		data.insert(time, value);

	}

	return data;
}

int QTFyAttributeTools::findQuantificationIndex(QString _attrName, CellLineageTree* _cltData)
{
	//if no suitable quantification index was found the function will return -1
	int quantIndex;

	int chunkCount = _cltData->getNumberOfQuantificationChunks();

	//if no quantification chunks return -1
	if(chunkCount < 1)
		return -1;

	//iterate through chunks
	for(int i = 0; i < chunkCount; i++){
		//Get column names
		QVector<QString> columnNames = _cltData->getQuantificationColumnNames(i);

		//if quantification chunk contains the attribute, return index
		if(columnNames.contains(_attrName))
			return i;
	}

	//if no quantification index was found
	return -1;
}

int QTFyAttributeTools::findActivationIndex(int _quantIndex, CellLineageTree* _cltData)
{
	QVector<QString> columnNames = _cltData->getQuantificationColumnNames(_quantIndex);
	int activeIndex;
	//iterate through columns and find the active column index
	for(int id = 0; id < columnNames.size(); id++){
		if(columnNames[id].toLower() == "active" || columnNames[id].toLower() == "activetps"){
			activeIndex = id;
			return activeIndex;
		}
	}
	
	//if no active index was found return -1
	return -1;
}

bool QTFyAttributeTools::addActiveColumn(int _quantIndex, CellLineageTree* _cltData)
{
	return _cltData->addQuantificationColumn(_quantIndex, "active", CellLineageTree::Bool, true);
}

bool QTFyAttributeTools::addInspectedColumn(int _quantIndex, CellLineageTree* _cltData)
{
	return _cltData->addQuantificationColumn(_quantIndex, "inspected", CellLineageTree::Bool, false);
}

void QTFyAttributeTools::updateDataEntry(QHash<ITrack*, QVector<int>> _cellList, ITree* _currentTree, QString _attrName, QString _chunkName)
{

	//CellLineage object of the current tree
	CellLineageTree* cltData;

	// Get correct clt data
	QString treeName = _currentTree->getTreeName();

	cltData = _currentTree->getCellLineageTree();

	if(!cltData){
		qDebug()<<"Error with updating activation status, invalid CLT object.";
		return;
	}

	//get the quantification index
	int quantIndex = cltData->getQuantificationIndexFromName(_chunkName);
	//if index invalid return
	if(quantIndex < 0){
		qDebug()<<"Invalid quantification index, updateDataEntry.";
		return;
	}

	//get the activation index
	int activeIndex = findActivationIndex(quantIndex, cltData);

	//check if invalid
	if(activeIndex < 0){
		qDebug()<<"Invalid active index, updateDataEntry.";
		return;
	}

	QVector<int> timepoints;

	for(QHash<ITrack*, QVector<int>>::iterator it = _cellList.begin(); it != _cellList.end(); ++it){

		//current cell
		ITrack* _cell = it.key();

		int number = _cell->getTrackNumber();

		//current timepoints that must be deactivated
		timepoints = it.value();

		//get quantification frame indexes for this cell
		//int quantIndex = this->m_quantIndex;
		QList<CellLineageTree::FrameIndex> frameIndex = cltData->getQuantificationFrameIndexes(quantIndex, _cell->getTrackNumber());
		qSort(frameIndex);

		//if error with the frame indexes of this cell continue to the next one
		if(frameIndex.isEmpty()){
			qDebug()<<"Error getting the quantification frame index list in updateDataEntry of CLTAttribute";
			continue;		
		}

		//iterate through the frame indexes of the specific cell and quantification chunk
		for(int i = 0; i < frameIndex.size(); i++){
			//keep the timepoint
			CellLineageTree::FrameIndex frameData = frameIndex[i];
			int timepoint = frameData[0];
			
			int preIncTime = (Experiment::isUsePreincubationTime()) ?_cell->getITree()->getTreeProperty(ITree::TREE_PREINCUBATION_TIME).toInt() : 0;
			int firstCellEvent = _cell->getAbsoluteSecondsForTP(_cell->getFirstTimePoint());
			
			int time = (preIncTime + firstCellEvent + _cell->getSecondsForTimePoint(timepoint));
			//int time = _cell->getSecondsForTimePoint(timepoint) + firstCellEvent;

			bool active;

			//get the value of the attribute
			QVector<QVariant> values = cltData->getQuantificationDataPoint(quantIndex, _cell->getTrackNumber(), frameIndex[i]);

			//check for invalid values
			if(values.isEmpty())
				qDebug()<<"In quantification data, returned invalid value";

			//check if timepoint is deactivated
			active = values[activeIndex].toBool();

			if((active && timepoints.contains(time))){
				values[activeIndex] = 0.0;
				if(!cltData->deteleQuantificationDataPoint(quantIndex, _cell->getTrackNumber(), frameData))
					qDebug()<<"Error with deleting quantification data point (CLTAttribute, updateDataEntry)";

				if(!cltData->setQuantificationDataPoint(quantIndex, _cell->getTrackNumber(), frameData, values))
					qDebug()<<"Error with updating the active column in updateDataEntry function.";

			}

		}
		timepoints.clear();
	}
}

bool QTFyAttributeTools::resetActivation(QString _attrName, CellLineageTree* _cltData, QString _chunkName)
{
	int quantIndex = _cltData->getQuantificationIndexFromName(_chunkName);

	//if no quantification chunk was found with this attribute return false
	if(quantIndex == -1)
		return false;

	int actIndex = findActivationIndex(quantIndex, _cltData);

	//if no activation column was found return false
	if(actIndex == -1)
		return false;

	//get cells
	QList<int> cells = _cltData->getCellIds();
	qSort(cells);

	//foreach cell
	foreach(int cellID, cells){
		//get the frame indexes
		QList<CellLineageTree::FrameIndex> frameIndexes = _cltData->getQuantificationFrameIndexes(quantIndex, cellID);
		qSort(frameIndexes);

		//foreach frame index
		foreach(CellLineageTree::FrameIndex frIndex, frameIndexes){
			//activate timepoint
			QVector<QVariant> values = _cltData->getQuantificationDataPoint(quantIndex, cellID, frIndex);
			values[actIndex] = 1.0;

			//if activation failed return
			if(!_cltData->setQuantificationDataPoint(quantIndex, cellID, frIndex, values))
				return false;
		}

	}

	return true;
}

void QTFyAttributeTools::createCalculatorParameter(ITree* _tree, QString _parameterName1, QString _chunkName1, QString _parameterName2, 
	QString _chunkName2, QString _operationName, QString _newParameterName) 
{

	// get the clt object
	CellLineageTree* cltData = _tree->getCellLineageTree();

	if(!cltData){
		qDebug()<<"Error with updating activation status, invalid CLT object.";
		return;
	}

	//get the quantification index of each of the chunks
	int quantIndex1 = cltData->getQuantificationIndexFromName(_chunkName1); 
	int quantIndex2 = cltData->getQuantificationIndexFromName(_chunkName2); 

	//if even one of the indices is invalid return
	if(quantIndex1 < 0 || quantIndex2 < 0) {
		qDebug()<<"Invalid quantification index, updateDataEntry.";
		return;
	}

	// check if the new parameter already exists in the first chunk
	// and if not create it
	// for now we put the new parameter in the first chunk 
	// but in the future we can think of a different way to store derived parameters
	QVector<QString> attr;
	attr << _newParameterName;

	if (!cltData->getQuantificationColumnNames(quantIndex1).contains(_newParameterName)) {
		PositionQuantification::addColsToQuantificationChunk(cltData, quantIndex1, attr, "");
	}
	// find the index of the newly added column (or existing column) for the new parameter
	int newParameterIndex = cltData->getQuantificationColumnNames(quantIndex1).indexOf(_newParameterName);

	// get the column index for each of the two parameters in their respective chunks
	int parameterIndex1 = cltData->getQuantificationColumnNames(quantIndex1).indexOf(_parameterName1);
	int parameterIndex2 = cltData->getQuantificationColumnNames(quantIndex2).indexOf(_parameterName2);

	//get all the available cells in the tree
	int maxGeneration = _tree->getTreeProperty(ITree::TREE_OPT_NUMBER_OF_GENERATIONS).toInt();
	QVector<ITrack*> cellsInTree = TreeStructuralHelper::getTracksInGenerations(_tree, 0, maxGeneration);

	foreach(ITrack* cell, cellsInTree) {

		int cellID = cell->getTrackNumber();

		//get quantification frame indexes for this cell
		//int quantIndex = this->m_quantIndex;
		QList<CellLineageTree::FrameIndex> frameIndex1 = cltData->getQuantificationFrameIndexes(quantIndex1, cell->getTrackNumber());
		qSort(frameIndex1);

		//if error with the frame indexes of this cell continue to the next one
		if(frameIndex1.isEmpty()){
			qDebug()<<"Error getting the quantification frame index list in updateDataEntry of CLTAttribute";
			return;		
		}

		//iterate through the frame indexes of the specific cell and quantification chunk
		for(int i = 0; i < frameIndex1.size(); i++) {
			//keep the timepoint
			CellLineageTree::FrameIndex frameData = frameIndex1[i];
			int timepoint = frameData[0];

			//get the value of the attribute
			QVector<QVariant> values1 = cltData->getQuantificationDataPoint(quantIndex1, cell->getTrackNumber(), frameIndex1[i]);
			QVector<QVariant> values2 = cltData->getQuantificationDataPoint(quantIndex2, cell->getTrackNumber(), frameIndex1[i]);

			//check for invalid values
			// i.e. quantification does not exist in both values for the frameIndex
			// the new attribute won't contain any value for this frameIndex either
			if(values1.isEmpty() || values2.isEmpty())
				qDebug()<<"In quantification data, returned invalid value";

			double val1 = values1.at(parameterIndex1).toDouble();
			double val2 = values2.at(parameterIndex2).toDouble();

			double newValue = 0;

			if (_operationName.contains("Add")) {
				newValue = val1 + val2;
			}
			else if (_operationName.contains("Subtract")){
				newValue = val1 - val2;
			}
			else if (_operationName.contains("Multiply")){
				newValue = val1 * val2;
			}
			else if (_operationName.contains("Divide")){
				// check for division with zero
				newValue = val1 / val2;
			}
			
			// get the data in the row with frameIndex
			QVector<QVariant> dataRow = cltData->getQuantificationDataPoint(quantIndex1, cellID, frameData);
			dataRow.replace(newParameterIndex, newValue);
			// add the attribute for the cell
			cltData->setQuantificationDataPoint(quantIndex1, cellID, frameData, dataRow);

		}
	}
	
}

//QMap<int, QVariant> CLTAttribute::getTimedValueForPrint(const ITrack &_cell) const
//{
//	//the return item
//	QMap<int, QVariant> data;
//
//	this->forPrint = true;
//
//	data = getTimedValue(_cell);
//
//	this->forPrint = false;
//
//	return data;
//}
//
//QMap<int, QVariant> CLTAttribute::getValuesCellNr(int cellNum) const
//{
//	//the return item
//	QMap<int, QVariant> data;
//	return data;
//}
//
////if an error occures returns empty QMap
//QMap<int, QVariant> CLTAttribute::getValuesTrackingData(const ITrack &_cell, QString name) const
//{
//	//the return item
//	QMap<int, QVariant> data;
//
//	//CellLineageTree object
//	CellLineageTree cltData;
//
//	// Get the tree name
//	QString treeName = _cell.getITree()->getTreeName();
//
//	if(!m_cellLineageTrees.contains(treeName))
//		return data;
//	else
//		cltData = (*m_cellLineageTrees.value(treeName));
//
//	//get the frame index 
//	QList<CellLineageTree::FrameIndex> frameIndex = cltData.getTrackingFrameIndexes(_cell.getTrackNumber());
//	qSort(frameIndex);
//
//
//	if(frameIndex.isEmpty()){
//		qDebug()<<"Error getting the tracking frame index list in getTimedValue of CLTAttribute";
//		return data;
//	}
//
//	//checks if data ara invalid!!!
//	for(int i = 0; i < frameIndex.size(); i++){
//		//frame data
//		//QVector<int> frameData = frameIndex[i];
//		CellLineageTree::FrameIndex frameData = frameIndex[i];
//
//		int timepoint = frameData[0];
//
//		int preIncTime = (Experiment::isUsePreincubationTime()) ?_cell.getITree()->getTreeProperty(ITree::TREE_PREINCUBATION_TIME).toInt() : 0;
//		int firstCellEvent = _cell.getAbsoluteSecondsForTP(_cell.getFirstTimePoint());
//			
//		int time = _cell.getSecondsForTimePoint(timepoint);
//
//		//tracking data
//		double x, y, z;
//
//		//if valid data
//		if(cltData.getTrackingDataPoint(_cell.getTrackNumber(), frameIndex[i], &x, &y, &z)){
//			
//			if( name == "x")
//				data.insert(time, x);
//			else if ( name == "y")
//				data.insert(time, y);
//			else{
//				if( cltData.getUseZCoordinates())
//					//data.insert(frameData[0], z);
//					data.insert(time, z);
//			}
//		}
//
//	}
//
//	return data;
//}
