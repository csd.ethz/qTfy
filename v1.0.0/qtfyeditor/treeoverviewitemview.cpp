/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/****************************************************************************
**
** @ Laura Skylaki, 09.12.2013
**
**
****************************************************************************/

// PROJECT
#include "treeoverviewitemview.h"
#include "treeitem.h"

void TreeOverviewItemView::keyPressEvent(QKeyEvent * event)
{
	if(event->key() == Qt::Key_Up)        {
		if (!indexAbove(currentIndex()).isValid())
			return;

		if (hideMode) {
			setCurrentIndex (findNextVisibleIndexAbove(currentIndex()));	
			emit clicked(currentIndex());
			return;
		}
		
		setCurrentIndex(indexAbove(currentIndex()));
		emit clicked(currentIndex());
	}

	if(event->key() == Qt::Key_Down)        {
		if (!indexBelow(currentIndex()).isValid())
			return;

		if (hideMode) {
			setCurrentIndex (findNextVisibleIndexBelow(currentIndex()));	
			emit clicked(currentIndex());
			return;
		}		

		setCurrentIndex(indexBelow(currentIndex()));
		emit clicked(currentIndex());
	}
}



void TreeOverviewItemView::outIndexChangeUp()
{
	if (!indexAbove(currentIndex()).isValid())
		return;

	setCurrentIndex(indexAbove(currentIndex()));
	emit clicked(currentIndex());
}


void TreeOverviewItemView::outIndexChangeDown()
{
	if (!indexBelow(currentIndex()).isValid())
		return;

	setCurrentIndex(indexBelow(currentIndex()));
	emit clicked(currentIndex());
}


QModelIndexList TreeOverviewItemView::selectedIndexes() const
{
	QModelIndexList viewSelected;
	QModelIndexList modelSelected;
	if (selectionModel())
		modelSelected = selectionModel()->selectedIndexes();
	for (int i = 0; i < modelSelected.count(); ++i) {
		// check that neither the parents nor the index is hidden before we add
		QModelIndex index = modelSelected.at(i);
		while (index.isValid() && !isIndexHidden(index))
			index = index.parent();
		if (index.isValid())
			continue;
		viewSelected.append(modelSelected.at(i));
	}
	return viewSelected;

}

QModelIndex TreeOverviewItemView::findNextVisibleIndexAbove(const QModelIndex _currentIndex)
{
	TreeItem *item = static_cast<TreeItem*>(indexAbove(currentIndex()).internalPointer());

	if (!item->isIndexHidden())
		return indexAbove(currentIndex());
	
	setCurrentIndex(indexAbove(currentIndex()));

	if (!indexAbove(currentIndex()).isValid())
		return _currentIndex;

	QModelIndex output = findNextVisibleIndexAbove(indexAbove(currentIndex()));

	return output;

}

QModelIndex TreeOverviewItemView::findNextVisibleIndexBelow(const QModelIndex _currentIndex)
{
	TreeItem *item = static_cast<TreeItem*>(indexBelow(currentIndex()).internalPointer());

	if (!item->isIndexHidden())
		return indexBelow(currentIndex());

	setCurrentIndex(indexBelow(currentIndex()));

	if (!indexBelow(currentIndex()).isValid())
		return _currentIndex;

	QModelIndex output = findNextVisibleIndexBelow(indexBelow(currentIndex()));

	return output;

}
