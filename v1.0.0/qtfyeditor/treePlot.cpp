/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/****************************************************************************
**
** @ Eleni Skylaki, 16.07.2014
**
**
****************************************************************************/

//include <cmath>
//PROJECT
#include "treeplot.h"
#include "qtfydata/cellwrapper.h"
#include "qtfydata/attributeinfo.h"
#include "qtfyplotmarker.h"

//QT
#include <QColor>
#include <QCheckBox>
#include <QLineEdit>
#include <QPen>


//QWT
#include "qwt/qwt_legend.h"
#include "qwt/qwt_curve_fitter.h"
#include "qwt/qwt_scale_engine.h"


 
TreePlot::TreePlot(QVector<QString> treeSetInformation, QWidget* parent, QString _timeUnit)

{
	timeUnit = _timeUnit;
	xFactor = getXFactor(); //default x axis time factor = hours
	timepointFound = false;

	//initialize data
	treeName = " ";
	cellName = " ";
	timepoint = -1;

	ITrack* currentCell;

	if(treeSetInformation.size() > 0)
	{
		treeName = treeSetInformation[1];
		cellName = " ";
		if(treeSetInformation.size() > 2)
		{
			cellName = treeSetInformation[2];
			cNum = cellName.split("_")[1].toInt();

			if(treeSetInformation.size() > 3)
				timepoint = treeSetInformation[3].split("_")[1].toInt();
		}
	}

	this->mOptions = new PlottingOptions;

	plot = new QTFyPlot(parent);

}

TreePlot::~TreePlot()
{
}

QwtPlot* TreePlot::createPlot(ITrack* cell,int generations, QString cName, QWidget *parent)
{	
	//initialize data
	maxGeneration = generations;
	cellName = cName;
	
	//plot = new QTFyPlot();
	plot->setCanvasBackground(Qt::white);

	//set maxY - pixels corresponding to one cell on the y axis
	int cellY = 5; 	
	//int maxY = qPow(2, maxGeneration) * cellY;
	int maxY = qPow(2, maxGeneration) * cellY;

	maxCellY = maxY;
	minCellY = 0;

	//set start of tree model
	QPointF tmp;
	
	int preIncTime = (Experiment::isUsePreincubationTime()) ? cell->getITree()->getTreeProperty(ITree::TREE_PREINCUBATION_TIME).toInt() : 0;
	int firstCellEvent = cell->getAbsoluteSecondsForTP(cell->getFirstTimePoint());
	float time = (preIncTime + firstCellEvent) *xFactor;

	tmp.setX(time);
	//tmp.setY(maxY);
	tmp.setY(maxY/2);

	calculateCell(tmp, cell, maxY/2, 1);

    // finally, refresh the plot
    plot->replot();

	//set axis
    plot->setAxisTitle(QwtPlot::xBottom, timeUnit);
	plot->setAxisTitle(QwtPlot::yLeft, "Cell Lineage");
	plot->setAxisAutoScale(QwtPlot::xBottom);
	plot->setAxisScale(QwtPlot::yLeft, minCellY, maxCellY);

	//has the user selected a timepoint?
	if(timepointFound && timepoint!= -1)
	{
		//create the timepoint curve

		QwtScaleDiv* mDiv = plot->axisScaleDiv(QwtPlot::yLeft);
		int minY = mDiv->lBound();
		//maxY = mDiv->hBound();

		//set data
		QVector<QPointF> data;
		QPointF tmp;
		tmp.setX(timepointValue);
		//tmp.setY(minY);
		tmp.setY(0);
		data.append(tmp);

		tmp.setY(maxY);
		data.append(tmp);

		QPen pen(Qt::red);

		QwtPlotCurve* curve = new QwtPlotCurve("timepoint");
		pen.setStyle(Qt::DashLine);

		curve->setStyle(QwtPlotCurve::Lines);
		curve->setPaintAttribute(QwtPlotCurve::PaintFiltered);
		curve->setPen(pen);
		curve->setData(data);

		curve->attach(plot);
	}

	return plot;

}

void TreePlot::calculateCell(QPointF lastPoint, ITrack* cell, int partitionHeight, uint i)
{

	//initialize data to be used
	QVector<QPointF> currentPoints;
	QPointF tmp;
	float x = lastPoint.rx();
	float y = lastPoint.ry();
	QColor c;
	//int width = this->mOptions->getLineWidthForTreePlot(cell->getITree());
	int width = this->mOptions->getLineWidthForTreePlot();

	//find the generation of the cell
	int cellGeneration = cell->getTrackProperty(ITrack::TRACK_GENERATION).toInt();

	int firstCellEvent = cell->getAbsoluteSecondsForTP(cell->getFirstTimePoint());
	int preIncTime = (Experiment::isUsePreincubationTime()) ? cell->getITree()->getTreeProperty(ITree::TREE_PREINCUBATION_TIME).toInt() : 0;

	//if timepoint was clicked
	if(timepoint>0 && timepointFound == false)
	{

		if(cell->getTrackNumber() == cNum)
		{
			timepointValue = (firstCellEvent + cell->getSecondsForTimePoint(timepoint))*xFactor;
			timepointFound = true;
		}
	}

	//set start point of cell
	x = cell->getAbsoluteSecondsForTP((cell->getFirstTimePoint()))*xFactor;
	lastPoint.setX(x);
	currentPoints.append(lastPoint);

	//increase x = cell's lifetime
	float lifetime = cell->getSecondsForTimePoint(cell->getLastTimePoint())*xFactor;

	if( lifetime < 0)
	{
		return;
	}
	x = x + lifetime;
	
	//test
	//sumLifetime = sumLifetime + lifetime;

	//set end point
	lastPoint.setX(x);

	currentPoints.append(lastPoint);

	//set color
	c = this->mOptions->getCellColor(cell);
	//hide branch
	if(c.alpha() == 0)
		return;
	

	//add cell to curves
	QwtPlotCurve* curve = createCurve(QString::number(cell->getTrackNumber()), c, currentPoints, width);
	
	curves.append(curve);

	currentPoints.clear();

	i++;

	//does the cell have a child1?
	if(cell->getChild1())
	{
		//Set default curve (Child1 always up)																						   
		currentPoints.append(lastPoint);								

		//set y
		// we identify the number of partitions for this generation
		// as the number of children in the generation + 1
		// as the branches in the current generation need to fall in the middle
		// of two such partitions
		// int requiredPartitions = qPow(2, cellGeneration) + 1;
		int y1 = partitionHeight / 2;

		tmp.setY(lastPoint.ry() + y1);
		tmp.setX(lastPoint.rx());
		currentPoints.append(tmp);

		//default curve always black
		c = getColor(0);

		//test for hide branch
		//if(mOptions->getCellColor(cell->getChild1()).alpha() == 0)
			//return;

		QwtPlotCurve* defaultcurve = createCurve("D" + QString::number(cell->getChild1()->getTrackNumber()), c, currentPoints, width);
		curves.append(defaultcurve);

		currentPoints.clear();

		calculateCell(tmp, cell->getChild1(), partitionHeight/2, i);
		i++;

		//test
		if(y1 > maxCellY)
			maxCellY = y1;

	}

	//does the cell have a child2?
	if(cell->getChild2())
	{
		//Set default curve (Child2 always down)																					   
		currentPoints.append(lastPoint);								

		//set y
		int y2 = partitionHeight / 2;

		tmp.setY(lastPoint.ry() - y2);
		tmp.setX(lastPoint.rx());	

		currentPoints.append(tmp);

		//default curve always black
		c = getColor(0);

		//test for hide branch
		///if(mOptions->getCellColor(cell->getChild2()).alpha() == 0)
			//return;

		QwtPlotCurve* defaultcurve = createCurve("D" + QString::number(cell->getChild2()->getTrackNumber()), c, currentPoints, width);
		//defaultcurve->title().
		curves.append(defaultcurve);

		currentPoints.clear();


		calculateCell(tmp, cell->getChild2(),  partitionHeight/2, i);
		i++;

		//test
		if(y2 < minCellY)
			minCellY = y2;

	}

	//check stop reason
	//if division do nothing
	//if "Death" mark an X
	//if "Lost" mark an ?
	//if "None" 
	TrackStopReason fate = cell->getStopReason();
	if(fate == 2 ){ //death
		QTFyPlotMarker* label = new QTFyPlotMarker();
		label->setLabel(QwtText("X"));
		label->setTitle(QString::number(cell->getTrackNumber()));
		label->setLabelAlignment(Qt::AlignCenter);
		label->setYValue(lastPoint.ry());
		label->setXValue(lastPoint.rx() + 1);
		label->attach(plot);
	}
	else if (fate == 3){ //lost
		QTFyPlotMarker *label = new QTFyPlotMarker();
		label->setLabel(QwtText("?"));
		label->setTitle(QString::number(cell->getTrackNumber()));
		label->setLabelAlignment(Qt::AlignCenter);
		label->setYValue(lastPoint.ry());
		label->setXValue(lastPoint.rx() + 1);
		label->attach(plot);
	}
}

QColor TreePlot::getColor(int nr)
{
	QVector<QString> namesOfStandardColors = AttributeInfo::getNamesOfStandardColors();

	if(nr > 19)
		nr = nr % 19;

	return QColor(namesOfStandardColors[nr]);
}

QwtPlotCurve* TreePlot::createCurve(QString title, QColor color, QVector<QPointF> data, int width)
{
	//if user clicked on a cell the cell's curve becomes bold
	QPen pen(color);
	if(title == cellName)
		pen.setWidth(width + 2);
	else
		pen.setWidth(width);

	//set curve label

	if(!title.contains("D"))
	{
		QwtPlotMarker *m_marker = new QwtPlotMarker();

		m_marker->setLabel(title);
		m_marker->setLabelAlignment(Qt::AlignCenter|Qt::AlignTop);
		m_marker->setYValue(data[0].ry() + 1);
		m_marker->setXValue((data[1].rx() + data[0].rx())/2);
		m_marker->attach(plot);
	}

	QwtPlotCurve* curve1 = new QwtPlotCurve(title);
	QwtSymbol *sym=new QwtSymbol(QwtSymbol::Diamond,QBrush(Qt::red),QPen(Qt::red),QSize(5,5));
	curve1->setStyle(QwtPlotCurve::Lines);
	curve1->setPaintAttribute(QwtPlotCurve::PaintFiltered);
	curve1->setPen(pen);
	curve1->setData(data);

	curve1->attach(plot);
	return curve1;
}

void TreePlot::setXFactor(float xf)
{
	xFactor = xf;
}

void TreePlot::setPlottingOptions(PlottingOptions* _opt)
{
	this->mOptions->setPreferences(_opt->getPreferences());
	//add line width preferences
	this->mOptions->setLineWidthForTreePlot(_opt->getLineWidthForTreePlot());
}

PlottingOptions* TreePlot::getPlottingOptions()
{
	return this->mOptions;
}

float TreePlot::getXFactor()
{
	float factor;

	if(timeUnit == "Hours")
		factor = 1.0/3600.0;
	else if(timeUnit == "Minutes")
		factor = 1.0/60.0;
	else 
		factor = 1.0;

	return factor;
}
