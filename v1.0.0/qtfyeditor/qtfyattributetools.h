/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Eleni Skylaki
 * @date 16/10/2015
 */


#ifndef QTFYATTRIBUTETOOLS_H
#define QTFYATTRIBUTETOOLS_H

// PROJECT
#include "qtfybackend/itree.h"
#include "qtfyclt/celllineagetree.h"
#include "qtfybackend/itrack.h"
#include "qtfydata/positioninformation.h"

// QT
#include "QSharedPointer"
#include "QHash"
#include "QPointF"
#include "QImage"


// OPENCV
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"

using namespace cv;

class QTFyAttributeTools
{
public:

	QTFyAttributeTools();
	~QTFyAttributeTools();

	/*
	*
	*/
	static void findAttributesPerQuantificationChunk(QVector<QSharedPointer<ITree>> _trees, 
		QHash<QString, QVector<QString>>& _chunkAttributes, 
		QHash<QString, QString>& _chunkSoftware, 
		QHash<QString, QString>& _chunkRemarks
		);

	/**
	 * Returns the value of this attribute for the given cell.
	 */
	static QMap<int, QVariant> getTimedValue(const ITrack &_cell, QString _attrName, QString _quantChunkName);

	/**
	 * Finds the quantification index, of the chunk this attribute belong to.
	 */
	static int findQuantificationIndex(QString _attrName, CellLineageTree* _cltData);

	/**
	 * Finds the index of the active attribute, in the specific chunk.
	 * If the chunk doesn't have an active attribute, returns -1. 
	 */
	static int findActivationIndex(int _quantIndex, CellLineageTree* _cltData);

	/**
	 * Adds an active attribute column in the given quantification chunk.
	 * Returns false if the addition of the column wasn't successful.
	 */
	static bool addActiveColumn(int _quantIndex, CellLineageTree* _cltData);

	/**
	 * Adds an ispected attribute column in the given quantification chunk.
	 * Returns false if the addition of the column wasn't successful.
	 */
	static bool addInspectedColumn(int _quantIndex, CellLineageTree* _cltData);

	/**
	 * Updates the active column 
	 */
	static void updateDataEntry(QHash<ITrack*, QVector<int>> _cellList, ITree* _currentTree, QString _attrName, QString _chunkName);

	/**
	 * Resets all values of the active attribute of the quantification chunk to 1 (true).
	 */
	static bool resetActivation(QString _attrName, CellLineageTree* _cltData, QString _chunkName);

	/**
	 * Creates a new parameter from the mathematical operation defined in operationName (+,-,*,/)
	 * between parameters 1 & 2 and stores it in the quantification chunk of parameter 1 (for now)
	 */
	static void createCalculatorParameter(ITree* _tree, 
		QString _parameterName1, 
		QString _chunkName1, 
		QString _parameterName2, 
		QString _chunkName2, 
		QString _operationName, 
		QString _newParameterName);

	///**
	// * Calculates the value of this attribute for the given cell and returns it for print
	// */
	//QMap<int, QVariant> getTimedValueForPrint(const ITrack &_cell, int _quantIndex) const;

	//void initializeIndexes(CellLineageTree* data);

	//void addCellLineageTree(QString treeName, CellLineageTree* cltData, int quantIndex);


};

#endif // QTFYATTRIBUTETOOLS_H