/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
//Eleni Skylaki 30.07.2015

#ifndef PLOTTINGOPTIONS_H
#define PLOTTINGOPTIONS_H

//project
#include "qtfybackend/itree.h"
#include "qtfybackend/itrack.h"

//qt
#include <qcolor.h>
#include <qhash.h>

//qwt
#include "qwt/qwt_plot.h"
#include "qwt/qwt_plot_curve.h" 
#include "qwt/qwt_curve_fitter.h"
#include "qwt/qwt_plot_grid.h"
#include "qwt/qwt_plot_marker.h" 

class PlottingOptions
{

public:

	//constructor
	PlottingOptions(/*ITree* _tree*/);

	//destructor
	~PlottingOptions();

	void initializeOptions(ITree* _tree);

	void customizePlot(QwtPlot* _plot, ITree* _tree);

	/**
	 *The plotting preferences for each tree 
	 */
	struct PlotPreferences{
		//the color of each cell
		QHash<int, QColor> colorPerCell;
		//int lineWidthAttribute;
		//int lineWidthTree;

		//Constructor
		PlotPreferences(){}
		//change color of a cell
		void setCellColor(int cellNum, QColor c)
		{
			//find the cell by the cell number
			for(QHash<int, QColor>::iterator i = colorPerCell.begin(); i != colorPerCell.end(); ++i)
			{
				if(i.key()== cellNum){
					//if already the same color retun, else change color
					if(i.value() == c)
						return;
					else
						i.value() = c;
					return;
				}
			}
		}

	};

	void setCellColor(ITrack* _cell, QColor c);

	QColor getCellColor(ITrack* _cell);

	QColor getColor(int nr);

	/*void setLineWidthForAttributePlot(ITree* _tree, int _linew);
	void setLineWidthForTreePlot(ITree* _tree, int _linew);

	int getLineWidthForAttributePlot(ITree* _tree);
	int getLineWidthForTreePlot(ITree* tree);*/

	void setLineWidthForAttributePlot(int _linew);
	void setLineWidthForTreePlot(int _linew);

	int getLineWidthForAttributePlot();
	int getLineWidthForTreePlot();

	void addTreePreferences(ITree* _tree);

	QHash<ITree*, PlotPreferences> getPreferences();

	void setPreferences(QHash<ITree*, PlotPreferences> _pref);

private:

	QHash<ITree*, PlotPreferences> mTreePref;

	bool isChanged;

	QwtPlotCurve::CurveStyle mCurveStyle;

	//int mPenWidth;
	int mAttributePenWidth;
	int mTreePenWidth;

};
#endif