/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/****************************************************************************
**
**
****************************************************************************/

#ifndef HELPMAINWINDOW_H
#define HELPMAINWINDOW_H

// PROJECT
#include "ui_frmHelpMainWindow.h"
#include "helpbrowser.h"

#include <QMainWindow>
#include <QHelpEngine>

class Assistant;
class TextEdit;

class HelpMainWindow : public QMainWindow
{
	Q_OBJECT

public:
	HelpMainWindow();
	~HelpMainWindow();
	void showDocumentation(const QString &file);

	private slots:
		void about();
		void showDocumentation();
		void open();


private:
	void createActions();
	void createMenus();

	QHelpEngine* helpEngine;
	HelpBrowser* textViewer;

	Ui::frmHelpMainWindow ui;

};

#endif