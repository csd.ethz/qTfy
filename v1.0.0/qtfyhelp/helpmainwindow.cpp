/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/****************************************************************************
**
**
****************************************************************************/

#include <QLibraryInfo>
#include <QApplication>
#include <QAction>
#include <QMenu>
#include <QMenuBar>
#include <QMessageBox>
#include <QUrl>
#include <QTabWidget>
#include <QSplitter>
#include <QHelpContentWidget>
#include <QHelpIndexWidget>
#include <QDebug>

#include "helpmainwindow.h"
#include "textedit.h"

// ![0]
HelpMainWindow::HelpMainWindow()
{
	ui.setupUi(this);

	QString txt = QApplication::applicationDirPath() + "/qtfyhelp/qtfyhelp.qhc";
	helpEngine = new QHelpEngine(txt, this);
	if (!helpEngine->setupData()) {
		qDebug() << "Something went wrong setting up the data for the help engine";
		qDebug() << helpEngine->error();
	}

	this->setAttribute( Qt::WA_DeleteOnClose, true );

	// Create a help content widget:
	QHelpContentWidget* contentWidget = helpEngine->contentWidget();
	QHelpIndexWidget* indexWidget = helpEngine->indexWidget();

	QTabWidget* tWidget = new QTabWidget();
	tWidget->addTab( contentWidget, "Contents");
	tWidget->addTab(indexWidget, "Index");

	textViewer = new HelpBrowser(helpEngine, this);
	textViewer->setSource(QUrl("qthelp://csd.qtfy.qtfyhelp/doc/welcome.html"));

	ui.splitter->insertWidget(0, tWidget);
	ui.splitter->insertWidget(1, textViewer);

	connect(contentWidget, SIGNAL(linkActivated(QUrl)), textViewer, SLOT(setSource(QUrl)));

	connect(indexWidget, SIGNAL(linkActivated(QUrl, QString)), textViewer, SLOT(setSource(QUrl)));

	createActions();
	
	tWidget->resize(200, tWidget->size().height());
}

HelpMainWindow::~HelpMainWindow()
{
	delete helpEngine;
}

void HelpMainWindow::about()
{
	QMessageBox::about(this, tr("About QTFy Help"),
		tr("Use the QTFy Help contents "
		"to optimize your QTFy experience\n"
		"and get insights on how to perform your data analysis."));
}

void HelpMainWindow::showDocumentation()
{
	//assistant->showDocumentation("index.html");
}

void HelpMainWindow::open()
{
	//FindFileDialog dialog(textViewer, assistant);
	//dialog.exec();
}

void HelpMainWindow::createActions()
{
	ui.actionClose->setShortcuts(QKeySequence::Quit);
	connect(ui.actionClose, SIGNAL(triggered()), this, SLOT(close()));
	
	connect(ui.actionAbout, SIGNAL(triggered()), this, SLOT(about()));

}
