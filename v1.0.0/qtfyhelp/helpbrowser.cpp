/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "helpbrowser.h"

#include <QDebug>


HelpBrowser::HelpBrowser(QHelpEngine* helpEngine,
	QWidget* parent):QTextBrowser(parent),
	helpEngine(helpEngine)
{
}

QVariant HelpBrowser::loadResource(int type, const QUrl &name){
	if (name.scheme() == "qthelp")
		return QVariant(helpEngine->fileData(name));
	else
		return QTextBrowser::loadResource(type, name);
}

void HelpBrowser::setSource(const QUrl &url)
{
	if (helpEngine != 0)
	{
		qDebug() << "setSource = " << url.toString();

		QByteArray helpData = helpEngine->fileData(url);
		// show the documentation to the user
		if (!helpData.isEmpty())
		{
			setHtml(QLatin1String(helpData));
		}

		int anchorPos = url.toString().indexOf(QChar('#'));
		if (anchorPos >= 0)
		{
			QString anchor = url.toString().mid(anchorPos+1);
			//qDebug() << "Anchor = " << anchor;
			scrollToAnchor(anchor);
		}
	}
}