/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Stavroula Skylaki
 * @date 15/04/2016
 */


#ifndef VIEWCHANGELOG_H
#define VIEWCHANGELOG_H


// PROJECT
#include "ui_frmViewChangeLog.h"



class ViewChangeLog : public QDialog
{
	Q_OBJECT

public:
	ViewChangeLog(QWidget *parent = 0);
	~ViewChangeLog();

private:
	
	Ui::frmViewChangeLog ui;

};

#endif // VIEWCHANGELOG_H