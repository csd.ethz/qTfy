/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/****************************************************************************
**
** @ Laura Skylaki, 29.11.2015
**
**
****************************************************************************/

#ifndef QTFYHELP_H
#define QTFYHELP_H

#include <QDialog>
#include "ui_frmHelp.h"

class QTFyHelp : public QDialog
{
	Q_OBJECT

public:
	QTFyHelp(QWidget *parent = 0, QString windowTitle = "");
	~QTFyHelp();

	void setHTMLHelp(QString _helpTxt, QMap<QString, QImage> _imgResources = QMap<QString, QImage>());

public slots:
	void ok();	


private:	
	// The ui
	Ui::frmHelpGUI ui;

	

};


#endif // QTFYHELP_H