/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Eleni Skylaki
 * @date 18/02/2016
 */


#ifndef STARTSCREEN_H
#define STARTSCREEN_H


// PROJECT
#include "ui_frmStartScreen.h"

// QT
#include <qtimer.h>
#include <qstring.h>
#include <qlabel.h>
#include <qdir.h>
#include <qfile.h>
#include <QTextStream>
#include <qmessagebox.h>
//Added by qt3to4:
#include <QMouseEvent>
#include <QDialog>
#include <QDate>

#ifndef DEBUGMODE
const int ShowSplasherTime = 5;		//seconds
#else
const int ShowSplasherTime = 1;		//seconds
#endif

const QString Version = "1.0.0";

class StartScreen : public QDialog
{
	Q_OBJECT

public:
	StartScreen(QWidget *parent = 0, bool first = true);
	~StartScreen();

private slots:
	
	// starts a new quantification
	void startNewQuantification();

	// opens existing trees which have been already quantified
	void loadExistingQuantification();

	// closes the application
	//void exitQTFy();

private:

	
	// running log and messages
	//QString msgLog;

	// the file path to the folder holding all original TAT experiments
	// required to get access to the original images of tracked trees
	// QString pathToExperiments;
	virtual void mousePressEvent (QMouseEvent * _event);
	
	void createActions();
	QTimer mTimer;

	// is it the launch of the app
	// or is it coming from the about submenu?
	bool firstOnStartup;

	Ui::frmStartScreen ui;

};

#endif // STARTSCREEN_H