/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/****************************************************************************
**
** @ Laura Skylaki, 15.11.2013
**
**
****************************************************************************/

// PROJECT
#include "testwindowdialog.h"

// QT
#include <QPixmap>


TestWindowDialog::TestWindowDialog(QWidget *parent)
{

	// Setup ui
	ui.setupUi(this);

	// create the scene
	this->scene = new QGraphicsScene;
	ui.grvTestWindow->setScene(this->scene);
	this->pixmapItem = new QGraphicsPixmapItem;
	this->scene->addItem(pixmapItem);
	//setCacheMode(CacheBackground);
	//setRenderHint(QPainter::Antialiasing);
	
}


TestWindowDialog :: ~TestWindowDialog()
{
	
}


void TestWindowDialog::loadEmptyImage(int _width, int _height, QColor _bgrColor)
{
	QImage emptyImage( _width, _height, QImage::Format_Indexed8);
	emptyImage.fill(_bgrColor);

	this->pixmapItem->setPixmap(QPixmap::fromImage(emptyImage));

}

void TestWindowDialog::updateImage(QImage img)
{
	this->pixmapItem->setPixmap(QPixmap::fromImage(img));
}

