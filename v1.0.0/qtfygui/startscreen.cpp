/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Eleni Skylaki
 * @date 18/02/2016
 */


// PROJECT
#include "startscreen.h"
//#include "ui_frmStartScreen.h"
#include "loadexperiment.h"


// QT

QString QTFyVersionLong;

StartScreen::StartScreen(QWidget *parent, bool first)
{
	
	ui.setupUi(this);

	this->firstOnStartup = first;

	// Disable context-help button
	setWindowFlags( (windowFlags() | Qt::CustomizeWindowHint) & (~Qt::WindowContextHelpButtonHint));

	// Build long version string
	if(sizeof(int*) == 4)
		QTFyVersionLong = Version + " (32 bit)";
	else if (sizeof(int*) == 8)
		QTFyVersionLong = Version + " (64 bit)";
	else
		QTFyVersionLong = Version;

	if (this->firstOnStartup) {
		mTimer.setInterval(ShowSplasherTime * 1000);
		mTimer.setSingleShot(true);
		connect(&mTimer, SIGNAL(timeout()), SLOT(startNewQuantification()));
		mTimer.start();
	}
		
	ui.lblVersion->setText ("");
	ui.lblVersion->setText (QString ("<font size=""+1""><b>Version %1</b></font>").arg (QTFyVersionLong));
	
	//the executable's directory is not yet available here (qApp == 0)
	//look for latest version file on the net
	//if this version and the net version file do not correspond, a message is displayed that there is a later version
	//QDir dir (SystemInfo::defaultNAS());
	//QDir dir (SystemInfo::getExecutablePath());
	
	//QString version = "";
	//if (dir.exists()) {
	//	QFile versionFile (dir.absolutePath() + "/tttversion");
	//	if (versionFile.open (QIODevice::ReadOnly)) {
 //                       QTextStream stream (&versionFile);
	//		if (! stream.atEnd())
	//			version = stream.readLine(); 
	//		
	//		versionFile.close();
	//	}
	//}
	//
	//if (! version.isEmpty()) {
	//	if (version > internalVersion)
	//		QMessageBox::warning (this, "Version out of date", "The TTT version you are using is out of date. \nPlease try to use the latest one, as support cannot be guaranteed.", "OK");
	//}
}

StartScreen::~StartScreen()
{
	
}

// SLOT
void StartScreen::startNewQuantification()
{
	LoadExperiment* loadexperimentform = new LoadExperiment();
	loadexperimentform->show();
	this->close();
}

// SLOT
void StartScreen::loadExistingQuantification()
{


}

void StartScreen::mousePressEvent (QMouseEvent * _event)
{
	if (this->firstOnStartup) {
		mTimer.stop();
		startNewQuantification();
	}	
}
