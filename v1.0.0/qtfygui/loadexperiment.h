/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Stavroula Skylaki
 * @date 06/10/2015
 */


#ifndef LOADEXPERIMENT_H
#define LOADEXPERIMENT_H


// PROJECT
#include "ui_frmLoadExperiment.h"
#include "qtfybackend/itree.h"
#include "qtfyio/tttloadingoperations.h"

// QT
#include <QString>


class LoadExperiment : public QDialog
{
	Q_OBJECT


public:
	LoadExperiment(QWidget *parent = 0);

	~LoadExperiment();
		
	/**
    * @brief loads the selected experiment based on the user input paths
	* uses the TTTManager to find the required info for paths/experiment name etc
	* @return true if experiment is loaded successfully
	*/
	bool loadSelectedExperimentInfo();

	public slots:

		// updates the message log text box
		void updateMessageLogText (QString _msg, QColor _color);

		// updates the progress bar
		void updateProgressBar (int _value);

		// updates the maximum value of the progress bar
		// based on position count
		void setProgressBarMax (int _value);

		// loading finished so allow reloading
		void loadingFinished();

		// loading the trees done
		void TTTloadingFinished();


signals:
		void terminationRequested();
		

private slots:
	
	// gets the TTTfiles folder
	void setTTTFilesFolder();

	// gets the experiments folder
	void setExperimentFolder();

	// shows help info for choosing the right folder paths
	void showHelpFolderPaths();

	// closes the form
	void cancel();

	/**
    * @brief loads the selected experiment based on the user input paths
	* uses the TTTManager to find the required info for paths/experiment name etc
	* updates the table of ttt files found
	*/
	void startLoadingExperiment();

	/**
    * @brief loads the selected experiment based on the user input paths
	* uses the TTTManager to find the required info for paths/experiment name etc
	* updates the table of ttt files found
	*/
	void startLoadingSelectedTrees();

	/**
    * @brief opens the quantification option forms 
	* if there is at least one tree loaded
	*/
	void quantifySelection();

	// updates the table model with the identified trees from the loading thread
	void updateTreeTable (QStringList _treesFound);

	// get pointers to the successfully loaded trees
	void getLoadedTrees(TreePointers);

	// updates the selected checkboxes based on the selected rows of the table widget
	void updateTreeCheckboxes();

	// unchecks the selected rows from the context menu
	void uncheckSelectionTable ();

	// the right click menu for the table
	void tableRtClickMenu (const QPoint& _pnt);


private:

	// check if the given path is a valid path
	bool isPathValid(const QString _filepath);

	// create the actions for the UI
	void createActions();


	// the user selection of trees
	// it gets updated every time the selection of the tablewidget is changed
	QStringList selectedTrees;

	// the paths to the files
	// initialized to user's document folders
	QString logFilesFolder;
	QString tttWorkFolder;
	QString experimentsFilesFolder;

	bool tttFilesPathValid;
	bool experimentPathValid;
	
	// running log and messages
	QString msgLog;

	// the user has already started loading in a thread
	bool loadingRunning;

	// loading of experiment info is done
	// the available trees are displayed for the user
	bool loadingDone;

	// loading of ttt trees is done
	bool loadingTreesDone;

	// monitors if the user manually started quantification
	bool quantificationStarted;

	// the tree pointers for the selected trees
	QVector<QSharedPointer<ITree>> treePointers;

	//// the user has already started loading the .ttt files in a thread
	//bool loadingTreesRunning;

	//// loading of the trees (.ttt files) is done
	//// the user can proceed to quantification options
	//bool loadingTreesDone;

	// the file path to the folder holding all original TAT experiments
	// required to get access to the original images of tracked trees
	// QString pathToExperiments;


	Ui::LoadExperimentGUI ui;



};

#endif // LOADEXPERIMENT_H
