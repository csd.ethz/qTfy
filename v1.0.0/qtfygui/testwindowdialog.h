/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/****************************************************************************
**
** @ Laura Skylaki, 28.10.2015
**
**
****************************************************************************/


#ifndef TESTWINDOWDIALOG_H
#define TESTWINDOWDIALOG_H

// PROJECT
#include "ui_frmTestWindow.h"

// QT
#include <QColor>
#include <QImage>
#include <QPointF>
#include <QWidget>
#include <QGraphicsView>
#include <QGraphicsPixmapItem>


class TestWindowDialog : public QDialog
{
	Q_OBJECT

public:
	TestWindowDialog(QWidget *parent);
	~TestWindowDialog();

	/*! \brief loads an empty image with defined dims and background color
	//!
	//! \param _width int the width of the new empty image to create 
	//! \param _height int the height of the new empty image to create 
	//! \param _bgrColor QColor the background color of the new empty image
	//! \return bool true if image has been created successfully
	*/
	void loadEmptyImage(int _width, int _height, QColor _bgrColor);

	// update the image in the scene
	void updateImage(QImage img);


public slots:
	
signals:

private:

	// the underlying image graphics item
	QGraphicsPixmapItem *pixmapItem;

	// the scene
    QGraphicsScene *scene;

	// The ui
	Ui::frmTestWindow ui;

};

#endif // TESTWINDOWDIALOG_H