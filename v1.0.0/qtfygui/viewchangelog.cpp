/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Stavroula Skylaki
 * @date 15/04/2016
 */


// PROJECT
#include "viewchangelog.h"

// QT
#include <QFile>
#include <QTextStream>


ViewChangeLog::ViewChangeLog(QWidget *parent)
{
	
	ui.setupUi(this);

	// Disable context-help button
	setWindowFlags( (windowFlags() | Qt::CustomizeWindowHint) & (~Qt::WindowContextHelpButtonHint));

	// Read changelog
	QFile changelog(":/qtfyres/qtfy_changelog.txt");
	if(!changelog.open(QIODevice::ReadOnly))
		return;

	// Read changelog from resource
	QTextStream in(&changelog);
	const QString changelogString = changelog.readAll();

	// Add to text field
	ui.txeChangeLog->setText(changelogString);

	

}

ViewChangeLog::~ViewChangeLog()
{
	
}

