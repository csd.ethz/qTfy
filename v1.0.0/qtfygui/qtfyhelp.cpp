/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/****************************************************************************
**
** @ Laura Skylaki, 29.11.2015
**
**
****************************************************************************/

// PROJECT
#include "qtfyhelp.h"

// QT
#include <QUrl>

QTFyHelp::QTFyHelp(QWidget *parent, QString windowTitle)
{
	// Setup ui
	ui.setupUi(this);
	
	this->setWindowTitle(windowTitle);
	
	connect ( ui.pbtOK, SIGNAL (clicked()), this, SLOT (ok()));
	
	// Enable maximize and minimize buttons
	this->setWindowFlags(Qt::Window);
	this->setAttribute( Qt::WA_DeleteOnClose, true );
}

QTFyHelp::~QTFyHelp()
{
}

void QTFyHelp::setHTMLHelp(QString _helpTxt, QMap<QString, QImage> _imgResources)
{
	foreach (QString imgUrl, _imgResources.keys()) {
		ui.txeHelpText->document()->addResource(QTextDocument::ImageResource, QUrl(imgUrl), _imgResources.value(imgUrl));
	}

	ui.txeHelpText->setHtml(_helpTxt);
}

//SLOT:
void QTFyHelp::ok()
{
	QTFyHelp::close();
}
