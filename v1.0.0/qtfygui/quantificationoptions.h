/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/****************************************************************************
**
** @ Laura Skylaki, 06.08.2015
**
**
****************************************************************************/

#ifndef QUANTIFICATIONOPTIONS_H
#define QUANTIFICATIONOPTIONS_H


// PROJECT
#include "ui_frmQuantificationOptions.h"
#include "loadexperiment.h"
#include "testwindowdialog.h"
#include "qtfybackend\itree.h"
#include "qtfyclt\celllineagetree.h"
#include "qtfyanalysis\positionquantification.h"

// QT
#include <QInputDialog>
#include <QMessageBox>
#include <QDebug>
#include <QSharedPointer>
#include <QMap>
#include <QThreadPool>


class QuantificationOptions : public QDialog
{
	Q_OBJECT

public:
	QuantificationOptions(QWidget *parent = 0);
	~QuantificationOptions();


	static QVector<int> wavelengthsToQuantify;
	static QVector<int> wavelengthsForDetection;


	/**
     * creates the list display of loaded trees
     * @param _treeSetName  the selected treeset
	 * @param _treeFilterName the selected tree filter (if any)
     */
	void createTreeList(QVector<QSharedPointer<ITree>> treePointers);
	
	/*
	* where are the raw images and the segmentation images stored, the root folder of the experiment
	*/
	void setExperimentsFilesFolder (QString path);

	public slots:

		// updates the message log text box
		void updateMessageLogText (QString _msg, QColor _color);

		// updates the progress bar
		// updates it by one index
		void updateProgressBar ();

		// updates the maximum value of the progress bar
		// based on position count
		void setProgressBarMax (int _value);

		// keeping track of how many quantifications are still running
		void addJobRunning();
		void deleteJobRunning();


signals:
	// the user has requested to cancel the quantification
	// all threads must stop
	void terminationRequested();

private slots:
	
	// creates the actions/signals connections for the ui
	void createConnections();

	// closes the form if no quantification is running
	// otherwise cancels the quantification
	void cancel();	

	void showHelpQuantificationOptions();
	void showHelpWavelengthSelection();
	void showHelpExistingSegmentation();
	
	// removes the selected row(s) from the combinations table
	void removeCombinationFromTable();

	// the right click menu for the table
	void tableRtClickMenu (const QPoint& _pnt);

	// updates the checkboxes based on user selection
	void updateWLCheckboxes();

	// opens the QTFy editor, main window of the application
	void openQTFyEditor();	

	/**
	* it will add a new row to the table that displays the quantification options 
	*/
	void addCombination();

	/*! \brief display the ROI around the cell that corresponds to the user's selected window size
	*/
	void showExampleWinSize();

	/*! \brief if manual threshold checkbox is checked enables the lineEdit for manual therhold input
	*/
	void manualThresholdChecked(bool _isAutoThresholdChecked);

	/*! \brief if not use default existing segmentation is checked, enables the lineEdits for the custom options
	*	for detection wavelength and segmentation method		
	*/
	void useExistingSeg(bool _isUseDefaultExistingChecked);

	/*! \brief if use existing segmentation when possible is unchecked 
	*	the rest of options for existing segmentation become disabled		
	*/
	void activateExistingSeg(bool _toActivate);

	/*! \brief display the segmentation of a random cell based on the advanced settings
	*/
	// void showExampleCellSeg();

	/*
	* starts the quantification of selected trees
	*/
	void runQuantification();

	void exportMessageLog();

	
private:

	// setups the initial options for the segmentation correction suggestions
	void setupOptionsForQuantification();

	/**
     * displays the number of trees for quantification
     * @param _num  the number of trees that are in the lwtTreeListForQuantification at any point
     */
	void setNumberOfTrees(int _num);

	/**
	From the collection of trees that will be quantified, it identifies the ones that belong to a specific position
	@param _posIndex the position index
	@param _treesForQuantification the collection of trees that will be quantified
	@return QList<QSharedPointer<ITree>>  a list with the pointers of the trees in position _posIndex
	*/
	QList<QSharedPointer<ITree>> findTreesPerPosition(QString _posIndex, QVector<QSharedPointer<ITree>> _treesForQuantification);

	QVector<int> getTrackingTimepoints(QList<CellLineageTree::FrameIndex> _qtrackingIndexes);

	QVector<int> getQuantificationTimepoints(QList<CellLineageTree::FrameIndex> _quantificationIndexes);

	QMap<QString, QMap< int, QVector<ITrack*>>> findCellsPerTimePointPerPosition(QVector<ITrack*> _tracks);

	/*! \brief loads an empty image with defined dims and background color
	//!
	//! \param _posID QString the position ID where the cell has been tracked 
	//! \param _timepointNum int the timepoint where the cell has been tracked
	//! \param _cell ITrack* the cell that is going to be segmented
	//! \param _options QMap<QString, QString> the set of options to be considered during the segmentation
	//! \param _x the x coordinate on the screen where the window will appear (need to control for opencv)
	//! \param _y the y coordinate on the screen where the window will appear
	*/
	void displayCell(QString _expName, QString _posID, int _timepointNum, ITrack* _cell, QMap<QString, QString> _options, int _x, int _y, bool _doSegmentation);

	// returns a map with each detection wavelenght and the list of the corresponding quantification wavelengths
	// performs all the checks that user input is valid
	// @return QString the combination of wavelengths for detection and quantification
	QMap<int, QList<int>> getQuantificationCombinations();

	QVector<QVector<int>> getSelectedWavelengthsToQuantify();

	// if at least one loaded tree has been quantified before
	// it will return true;
	bool previousQuantificationExists();

	// the thread pool that will manage the runnables for each position 
	// during morphology quantification
	QThreadPool *pool;

	// combinations of wavelength(s) and segmentation method(s) to use for quantification
	// key: the segmentation method id i.e. 0 for CAT, 2 for ESCseg, Otsu for Otsu etc
	// value: the numbers of the wavelengths to be quantified with the corresponding method-the key
	QMultiMap <int, QVector<int>> combinations;

	// the tree pointers for the selected trees
	QVector<QSharedPointer<ITree>> treePointers;

	// the root folder of the experiment
	QString pathToExperiments;

	// the numbers of the available wavelenghts in the experiment
	// used to stop the user from manually inputing an invalid wavelength number
	QVector<int> availableWavelengths;

	// checks if a quantification is already running
	// stops the user from starting a new one
	bool isQuantificationRunning;

	// hold here the pointer to the load experiment form
	// in order to go back if the user cancels
	LoadExperiment* loadExpForm;

	// the tab indeces for easy retrieval
	int tabIndexCombinations;
	int tabIndexOptions;
	int tabIndexTreeList;
	int tabIndexMsgLog;
	int tabIndexAdvSettings;

	// the number of threads still running
	int activeThreads;

	// the quantification options from the options tab 
	QMap<QString, QString> options;

	// the test window dialog for checking the segmentation options
	TestWindowDialog* testDisplay;

	// The ui
	Ui::frmQuantificationOptions ui;


};

#endif QUANTIFICATIONOPTIONS_H