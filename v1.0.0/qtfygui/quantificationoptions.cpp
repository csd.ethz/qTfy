/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/****************************************************************************
**
** @ Laura Skylaki, 06.08.2015
**
**
****************************************************************************/

// PROJECT
#include "quantificationoptions.h"
#include "qtfyhelp.h"
#include "qtfydata/tatinformation.h"
#include "qtfybackend/tttmanager.h"
#include "qtfydata/treestructuralhelper.h"
#include "qtfydata/wavelengthinformation.h"
#include "qtfyanalysis/positionquantification.h"
#include "qtfyeditor/qtfyMain.h"


// QT
#include <QInputDialog>
#include <QMessageBox>
#include <QFileDialog>
#include <QMenu>


QVector<int> QuantificationOptions::wavelengthsToQuantify = QVector<int>();
QVector<int> QuantificationOptions::wavelengthsForDetection = QVector<int>();


QuantificationOptions::QuantificationOptions(QWidget *parent)
{
	// Setup ui
	ui.setupUi(this);

	ui.txeMessageLog->setReadOnly(true);

	this->setAttribute( Qt::WA_DeleteOnClose, true );	

	isQuantificationRunning = false;

	createConnections();

	tabIndexCombinations = 0;
	tabIndexOptions = 1;
	tabIndexTreeList = 2;
	tabIndexMsgLog = 3;
	tabIndexAdvSettings = 4;

	ui.tabTreesetQuantification->setCurrentIndex(tabIndexCombinations);
	
	// create the thread pool to manage the quantifications per position
	pool = new QThreadPool(this);
	// setup the thread count
	int threadNum = QThread::idealThreadCount();
	int numOfPhysicalCores = SystemInfo::getNumPhysicalCores();
	/*QString findOut = QString("The number of available threads in your system is: %1").arg(threadNum);
	QMessageBox::information(this, "Finished", findOut);
	findOut = QString("The number of available cores in your system is: %1").arg(numOfPhysicalCores);
	QMessageBox::information(this, "Finished", findOut);*/


	if (numOfPhysicalCores > 8)
		numOfPhysicalCores = 8;

	pool->setMaxThreadCount(numOfPhysicalCores);


	loadExpForm = (LoadExperiment*) parent;

	// set test display window
	this->testDisplay = new TestWindowDialog(this);
	// put it always on top
	this->testDisplay->setWindowFlags(Qt::Window | Qt::WindowStaysOnTopHint);

	//setup the segmentation and quantification options
	setupOptionsForQuantification();

	// initialize combinations table headers
	// Column headers
	QStringList columnHeaders;
	// File name column
	columnHeaders  << "Select" << "Channel" << "Comment";

	// Set headers
	ui.tbwtWavelenthgsToQuantify->setColumnCount(columnHeaders.size());
	ui.tbwtWavelenthgsToQuantify->setHorizontalHeaderLabels(columnHeaders);
	ui.tbwtWavelenthgsToQuantify->verticalHeader()->setVisible(false); // hide row numbers
	ui.tbwWLforDetection->setColumnCount(columnHeaders.size());
	ui.tbwWLforDetection->setHorizontalHeaderLabels(columnHeaders);
	ui.tbwWLforDetection->verticalHeader()->setVisible(false); // hide row numbers

	
	// set selection behavior
	ui.tbwtWavelenthgsToQuantify->setSelectionBehavior(QAbstractItemView::SelectRows);
	ui.tbwWLforDetection->setSelectionBehavior(QAbstractItemView::SelectRows);

	// getting the available wavelengths in the experiment
	QVector<WavelengthInformation> wavelengthsInfo = TATInformation::getInst()->getAvailableWavelengthInfo();
	int initialisedWLs = 0;
	foreach (WavelengthInformation wlInfo, wavelengthsInfo) {
		if (wlInfo.isInitialized()) 
			initialisedWLs++;
	}

	// set the row count
	ui.tbwtWavelenthgsToQuantify->setRowCount(initialisedWLs);
	ui.tbwWLforDetection->setRowCount(initialisedWLs);

	int rowCounter = 0;
	// populate lists 
	foreach (WavelengthInformation wlInfo, wavelengthsInfo) {

		if (wlInfo.isInitialized()) {
			int wlNumber = wlInfo.getWavelengthNumber();
			QString comment = wlInfo.getComment();

			// first column the check box
			QCheckBox *newChk = new QCheckBox();
			ui.tbwtWavelenthgsToQuantify->setCellWidget(rowCounter, 0, newChk);
			newChk->setProperty("row", rowCounter);
			// second column the wavelength name
			QTableWidgetItem *wavelengthItem = new QTableWidgetItem(QString::number(wlNumber));
			wavelengthItem->setToolTip(QString::number(wlNumber));
			wavelengthItem->setFlags(wavelengthItem->flags() ^ Qt::ItemIsEditable); // not editable
			ui.tbwtWavelenthgsToQuantify->setItem(rowCounter, 1, wavelengthItem);
			// finally add the comment for this wavelength			
			QTableWidgetItem *wlCommItem = new QTableWidgetItem(comment);
			wlCommItem->setFlags(wlCommItem->flags() ^ Qt::ItemIsEditable); // not editable
			// posCommItem->setToolTip("The position comment from the TAT xml file.");
			ui.tbwtWavelenthgsToQuantify->setItem(rowCounter, 2, wlCommItem);

			// second table
			// first column the check box
			QCheckBox *newChk2 = new QCheckBox();
			ui.tbwWLforDetection->setCellWidget(rowCounter, 0, newChk2);
			newChk2->setProperty("row", rowCounter);
			// second column the wavelength name
			QTableWidgetItem *wavelengthItem2 = new QTableWidgetItem(QString::number(wlNumber));
			wavelengthItem2->setToolTip(QString::number(wlNumber));
			wavelengthItem2->setFlags(wavelengthItem2->flags() ^ Qt::ItemIsEditable); // not editable
			ui.tbwWLforDetection->setItem(rowCounter, 1, wavelengthItem2);
			// finally add the comment for this wavelength
			QTableWidgetItem *wlCommItem2 = new QTableWidgetItem(comment);
			wlCommItem2->setFlags(wlCommItem2->flags() ^ Qt::ItemIsEditable); // not editable
			// posCommItem->setToolTip("The position comment from the TAT xml file.");
			ui.tbwWLforDetection->setItem(rowCounter, 2, wlCommItem2);

			availableWavelengths << wlNumber;
			rowCounter++;
		}
	}

	// Resize to fit contents
	ui.tbwtWavelenthgsToQuantify->resizeColumnsToContents();
	ui.tbwWLforDetection->resizeColumnsToContents();

	// Enable maximize and minimize buttons
	//this->setWindowFlags(Qt::Window | Qt::WindowStaysOnTopHint);

	
	// initialize combinations table headers
	// Column headers
	QStringList columnHeadersQ;

	// File name column
	columnHeadersQ  << "Channels for quantification"  << "Channel for detection" ;

	// Set headers
	ui.tbtQuantificationCombinations->setColumnCount(columnHeadersQ.size());
	ui.tbtQuantificationCombinations->setHorizontalHeaderLabels(columnHeadersQ);

	// Resize to fit headers
	ui.tbtQuantificationCombinations->resizeColumnsToContents();
	ui.tbtQuantificationCombinations->setContextMenuPolicy(Qt::CustomContextMenu);
	
	
}

QuantificationOptions::~QuantificationOptions()
{
	// delete the pointers
	delete pool;
	delete testDisplay;
}

void QuantificationOptions::createConnections()
{
	// wavelength selection tab
	// connect the wavelength lists selection to the check boxes
	// connect the table selection to the check boxes
	connect(ui.tbwtWavelenthgsToQuantify, SIGNAL(itemSelectionChanged()), this, SLOT (updateWLCheckboxes()));
	connect(ui.tbwWLforDetection, SIGNAL(itemSelectionChanged()), this, SLOT (updateWLCheckboxes()));

	// add a new combination of wavelength(s) and segmentation method for quantification
	connect( ui.pbtAddCombination, SIGNAL (clicked()), this, SLOT (addCombination()));
	// get context menu for the table
	connect(ui.tbtQuantificationCombinations, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(tableRtClickMenu(const QPoint&)));

	// connections in Quantifications Options tab
	connect ( ui.pbtCheckWindowSize, SIGNAL (clicked()), this, SLOT (showExampleWinSize()));
	connect ( ui.pbtCheckSegmentation, SIGNAL (clicked()), this, SLOT (showExampleWinSize()));
	connect ( ui.chkUseAutomaticThreshold, SIGNAL (toggled(bool)), this, SLOT (manualThresholdChecked(bool)));
	connect ( ui.chkUseExistingSegmentation, SIGNAL (toggled(bool)), this, SLOT (activateExistingSeg(bool)));
	connect ( ui.chkUseDefaultExistingSeg, SIGNAL (toggled(bool)), this, SLOT (useExistingSeg(bool)));

	connect ( ui.pbtQuantificationOptionsHint, SIGNAL (clicked()), this, SLOT (showHelpQuantificationOptions()));
	connect ( ui.pbtHelpExistingSegmentation, SIGNAL (clicked()), this, SLOT (showHelpExistingSegmentation()));
	connect ( ui.pbtWavelengthSelectionHint, SIGNAL (clicked()), this, SLOT (showHelpWavelengthSelection()));

	//Run/Cancel buttons
	connect(ui.pbtRunQuantification, SIGNAL(clicked()), this, SLOT(runQuantification()));
	connect(ui.pbtCancel, SIGNAL(clicked()), this, SLOT(cancel()));

	// open QTFy Editor
	connect(ui.pbtOpenQTFyEditor, SIGNAL(clicked()), this, SLOT(openQTFyEditor()));

	//export message log
	connect(ui.pbtExportMessageLog, SIGNAL(clicked()), this, SLOT(exportMessageLog()));

}

void QuantificationOptions::createTreeList(QVector<QSharedPointer<ITree>> _treePointers)
{

	setNumberOfTrees(_treePointers.size());

	if (!_treePointers.isEmpty()) {
		foreach (QSharedPointer<ITree> tree, _treePointers) {
			QString treeName = tree->getTreeName();
			ui.lwtTreeListForQuantification->addItem(treeName);
		}
	}

	this->treePointers = _treePointers;

	if (previousQuantificationExists()) {
		ui.pbtOpenQTFyEditor->setEnabled(true);
	}	

}

void QuantificationOptions::setNumberOfTrees(int _num)
{
	ui.lneNumberOfTrees->setText(QString("%1").arg(_num));
}

// SLOT:
void QuantificationOptions::updateWLCheckboxes()
{
	if (sender() == ui.tbwtWavelenthgsToQuantify) {
		int rowNum = ui.tbwtWavelenthgsToQuantify->rowCount();
		QList<QTableWidgetItem*> selectedWidgets = ui.tbwtWavelenthgsToQuantify->selectedItems();

		for(int i = 0; i < rowNum; i++){
			QTableWidgetItem* currentTree = ui.tbwtWavelenthgsToQuantify->item(i, 1);

			// is the item selected 
			if (selectedWidgets.contains(currentTree)) {
				QCheckBox* chkbx = dynamic_cast<QCheckBox*>(ui.tbwtWavelenthgsToQuantify->cellWidget(i, 0));
				chkbx->setCheckState(Qt::Checked);
			}		
		}
	}
	else if (sender() == ui.tbwWLforDetection) {
		int rowNum = ui.tbwWLforDetection->rowCount();
		QList<QTableWidgetItem*> selectedWidgets = ui.tbwWLforDetection->selectedItems();

		for(int i = 0; i < rowNum; i++){
			QTableWidgetItem* currentTree = ui.tbwWLforDetection->item(i, 1);

			// is the item selected 
			if (selectedWidgets.contains(currentTree)) {
				QCheckBox* chkbx = dynamic_cast<QCheckBox*>(ui.tbwWLforDetection->cellWidget(i, 0));
				chkbx->setCheckState(Qt::Checked);
			}		
		}
	}
}


// SLOT:
void QuantificationOptions::addCombination()
{
	int previousRowCount = ui.tbtQuantificationCombinations->rowCount();
	int rowCounter = 0;

	// key the wavelength for detection, value the quantification wavelengths separated by comma
	QMap<QString, QString> combinations; 

	// get the selected wavelegnths for quantification
	int rowNum = ui.tbwWLforDetection->rowCount();
	
	for(int i = 0; i < rowNum; i++){
		// QTableWidgetItem* currentItemD = ui.tbwWLforDetection->item(i, 0);
		QCheckBox* chkbx = dynamic_cast<QCheckBox*>(ui.tbwWLforDetection->cellWidget(i, 0));

		if (chkbx == NULL)
			continue;
		
		// is the item selected 
		if (chkbx->checkState() == Qt::Checked) {
			
			// get the selected wavelengths for quantification
			int rowNumQ = ui.tbwtWavelenthgsToQuantify->rowCount();
			QStringList waveTemp;

			for(int j = 0; j < rowNumQ; j++){
				//QTableWidgetItem* currentItemQ = ui.tbwtWavelenthgsToQuantify->item(j, 0);
				QCheckBox* currentItemQ = dynamic_cast<QCheckBox*>(ui.tbwtWavelenthgsToQuantify->cellWidget(j, 0));

				if (currentItemQ == NULL)
					continue;

				// is the item selected 
				if (currentItemQ->checkState() == Qt::Checked) {
					waveTemp << ui.tbwtWavelenthgsToQuantify->item(j, 1)->text();
				}
			}
			QString s = waveTemp.join(",");
			if (!s.isEmpty()){
				combinations.insert(ui.tbwWLforDetection->item(i, 1)->text(), s);
				rowCounter++;
			}
		}		
	}

	// adjust the table to contents
	if (rowCounter == 0)
		ui.tbtQuantificationCombinations->setRowCount(previousRowCount);
	else 
		ui.tbtQuantificationCombinations->setRowCount(rowCounter + previousRowCount);

	rowCounter = previousRowCount;  // add new combinations at the end of the table
	QMap<QString, QString>::iterator iter;
	for (iter = combinations.begin(); iter != combinations.end(); ++iter) {
		QTableWidgetItem *wlListitem = new QTableWidgetItem(iter.value());
		ui.tbtQuantificationCombinations->setItem(rowCounter, 0, wlListitem);
		wlListitem->setToolTip("These are the channels that will be quantified.");

		// second column
		QTableWidgetItem *wlDetectionItem = new QTableWidgetItem(iter.key());
		ui.tbtQuantificationCombinations->setItem(rowCounter, 1, wlDetectionItem);
		wlDetectionItem->setToolTip("This is the channel that will be used to detect the cells and create the mask file.");
		rowCounter ++;
	}
}

// SLOT
void QuantificationOptions::tableRtClickMenu (const QPoint& _pnt)
{
	QPoint globalPos = ui.tbtQuantificationCombinations->viewport()->mapToGlobal(_pnt);

	QMenu myMenu;
	QAction *actUncheckSelected = new QAction(QIcon(":/qtfyres/qtfyres/delete.png"), "Remove selected", this);
	myMenu.addAction(actUncheckSelected);
	connect(actUncheckSelected, SIGNAL(triggered()),this, SLOT(removeCombinationFromTable()) );
	myMenu.exec(globalPos);

}


//SLOT:
void QuantificationOptions::removeCombinationFromTable()
{
	// remove current row
	ui.tbtQuantificationCombinations->removeRow(ui.tbtQuantificationCombinations->currentRow());

}


//SLOT:
void QuantificationOptions::cancel()
{
	// if a thread is running need to cancel the thread
	if (this->isQuantificationRunning) {
		updateMessageLogText("> Warning: Quantification has been stopped... Please wait until all positions are notified...", Qt::darkBlue);
		emit terminationRequested();
		this->isQuantificationRunning = false;
		ui.pbtCancel->setEnabled(false);
		QApplication::restoreOverrideCursor();
	}
	// go back to intro screen
	else {
		loadExpForm->show();
		QuantificationOptions::close();
		this->testDisplay->close();
	}
	
}

// SLOT
void QuantificationOptions::showHelpExistingSegmentation()
{
	// create help text
	QString txt = "<p><h2>Existing Segmentation Options</h2> </p>"
		"<p><h3>Existing segmentation</h3> </p>"
		"<p>The <b>\"Use existing segmentation if possible when a mask image is present\"</b> "
		"option is by default checked. This means that QTFy will look for previously generated mask images "
		"from the selected detection channel instead of segmenting the detection channel and generating new mask image files. "
		"When this option is not checked previously generated mask files will be overwritten when present.</p>" 

		"<p><b>What is a mask image file? </b><br>"
		"We refer to mask image files the binary image files where pixels that belong to a "
		"cell have a value of 1 (white) while pixels that belong to the background have a value of 0 (black)."

		"<p><b>Where are the mask image files saved?</b><br>"
		"The mask image files can be found in each position folder in the subfolder \"segmentation\" "
		"i.e. for the experiment 111115AF6, position 3, the mask image files can be found under the path: 111115AF6\\111115AF6_p0003\\segmentation</p>"

		"<p><b>What does the mask image file format mean?</b><br>"
		"The first part of the mask image filename is identical to the filename of the channel image that has been used for detection. "
		"By the first part of the mask image filename, you can know the experiment, the position (_p), the timepoint (_t), "
		"the z-stack index (_z) and the detection channel (_w) identifiers. The segmentation method identifier (_m) is always \"01\" "
		"for mask image files that have been generated in QTFy. Finally, the identifier \"mask\" is used to indicate that this is a mask file. </p>"

		"<p><b>Please provide the detection channel and the segmentation method that were used to generate the segmentation image.</b></p>"

		"<p>Use the default option \"Use selected detection channel from Channel Selection tab and QTFy segmentation (local entropy-based threshold).\" to use existing image files where the channel identifier is equal "
		"to the detection channel identifier selected in the channel Selection tab and the segmentation method is the QTFy default \"01\". "
		"Alternatively, if you have your custom generated mask image files, please indicate the detection channel identifier and segmentation method id of your files.</p>"

		"<p><img src=\":exclamation.png\"> Discrepancies in the filename formats will cause QTFy to fail to load the existing mask image files and will " 
		"by default try to perform a new segmentation using the QTFy default method.</p>"

		"<p><img src=\":hint.png\"> Have a look at the Message Log! It should indicate if the segmentation images (mask image files) can be found and/or the generated segmentation "
		"can be saved (no image will be saved if no cell has been found). </p>"

		"<p><img src=\":hint.png\"> QTFy can also support connected component labeling files (16-bit) as existing segmentation. </p>"

		;

	QMap<QString, QImage> imgResources;

	QImage img(":/qtfyres/qtfyres/exclamation.png");
	imgResources.insert("exclamation.png", img);

	QImage img2(":/qtfyres/qtfyres/hint.png");
	imgResources.insert("hint.png", img2);

	// show help form
	QString windowTitle = "QTFy help: Existing segmentation options";

	QTFyHelp* help = new QTFyHelp(this, windowTitle);
	help->setHTMLHelp(txt, imgResources);
	help->show();
}


// SLOT
void QuantificationOptions::showHelpQuantificationOptions()
{
	// create help text
	QString txt = "<p><h2>Segmentation Options</h2> </p>"		
		"<p><h3>Window/cell size</h3> </p>"
		"<p><b>Window size (px): </b> <br>"
		"Define the window size that you expect to fit each cell. It will create a x by x square window around the trackpoint of the cell during segmentation and quantification operations.</p>"

		"<p><b>Minimum/Maximum cell size (px):</b> <br>"
		"The sum of pixels that the cell occupies. The area of the cell. </p>"

		"<p><b>Maximum distance of trackpoint from cell:</b> <br>"
		"What is the maximum distance that the trackpoint should have from a cell? "
		"This value becomes important in the case where the trackpoint has been erroneously placed outside the cell. "
		"QTFy will look for candidate identified cells within the distance and assign the closest one to the trackpoint.</p>"
		
		
		
		"<p><h3>Segmentation options</h3> </p>"
		"<p><b>Use smoothing:</b><br>"
		"Smoothing is routinely used to improve the segmentation accuracy. Uncheck only when there are problems with the segmentation. </p>"
		"<p><b>Automatic/manual threshold:</b> <br>"
		"Leave checked for QTFy to automatically identify the optimal threshold for segmentation in the window that contains the cell. It applies local contrast adjustment."
		"Uncheck to specify a manual threshold (it might be affected from variations in illumination). </p>"
		"<p><b>Split merged cells:</b> <br>"
		"Works well when the cells are round objects and their joined contour pixels don't overlap largely. </p>"
		;

	// show help form
	QString windowTitle = "QTFy help: Segmentation options";

	QTFyHelp* help = new QTFyHelp(this, windowTitle);
	help->setHTMLHelp(txt);
	help->show();
}

void QuantificationOptions::showHelpWavelengthSelection()
{
	// create help text
	QString txt = "<p><h2>Channel selection</h2> </p>"  
		"<p>In the Channel Selection tab, you can create the combination of channels that will be used for cell detection "
		"and the channels that will be used for quantification of the intensity of the pixels inside the area of the cell. </p>"
		"<p>For cell detection, it is recommended that you choose a fluorescence image where the cells are distinct against the background. "
		"The detection images will be used to identify the contour of each tracked cell. The extracted contour of each cell will be then super-imposed"
		" to each quantification channel in order to generate statistics for the intensity of the pixels that fall inside the contour that has been assigned to that cell.</p>"
		"<p>You can create more than one such combinations using different detection channels depending on your imaging configuration. "
		"For each detection channel a new quantification \"chunk\" will be generated containing the quantification statistics that have been generated using the specific detection channel.</p>"
		"<p>You need to create a combination and add it to the list before you can Run a quantification. <br>"
		"If at least one previous quantification has been identified during experiment loading, then you also have the option to directly proceed to examine your data using the QTFy Editor.</p>"
		"<p><img src=\":hint.png\"> Don't forget to have a look at the Segmentation Options! </p>";

	QImage img(":/qtfyres/qtfyres/hint.png");

	QMap<QString, QImage> imgResources;
	imgResources.insert("hint.png", img);

	// show help form
	QString windowTitle = "QTFy help: Channel selection";

	QTFyHelp* help = new QTFyHelp(this, windowTitle);
	help->setHTMLHelp(txt, imgResources);
	help->show();
}


//SLOT
void QuantificationOptions::runQuantification()
{

	setupOptionsForQuantification();

	double roiSize = PositionQuantification::options.value("windowSize").toDouble();

	if (roiSize < 5) {
		QString msg= "Window size less by 5x5 pixels is not supported. Please choose a bigger window size.";
			QMessageBox::warning (this,  "Invalid window size!", msg, QMessageBox::Ok);
		return;
	}

	ui.pbtOpenQTFyEditor->setEnabled(false);

	if (isQuantificationRunning) {
		updateMessageLogText(QString("> Warning: A quantification is already running. Please stop the current quantification before starting a new one..."), Qt::darkBlue);
		ui.tabTreesetQuantification->setCurrentIndex(tabIndexMsgLog);
		return;
	}

	// get the combinations from the table
	// intially stored as qstrings but then test if they are integers
	QMap<int, QList<int>> wlCombos = getQuantificationCombinations();

	if (wlCombos.empty()) {		
		ui.tabTreesetQuantification->setCurrentIndex(tabIndexCombinations);		
		QString msg = "Please create the quantification by selecting the appropriate detection and quantification channels and press \"Add quantification to list\" in order to proceed.";
		QMessageBox::warning (this,  "Add quantification to list!", msg, QMessageBox::Ok);
		ui.pbtAddCombination->setFocus();
		return;
	}

	// reset active threads
	this->activeThreads = 0;

	// find the cells that exist in each timepoint
	// do this for each position
	// this helps the quantification because we quantify simultaneously all trees of a position
	// and therefore we don't need to load an image multiple times
	QMap<QString, QMap<int, QVector<ITrack*>>> cellsPerTimepointPerPosition;	
	QVector<ITrack*> tracks;

	// get all tracks (cells) in the collection of trees.
	foreach (QSharedPointer<ITree> tree, this->treePointers) {
		tracks+=tree->getTracksInTree();
	}

	cellsPerTimepointPerPosition = this->findCellsPerTimePointPerPosition(tracks);

	
	//set message log tab to see info while quantification is running
	ui.tabTreesetQuantification-> setCurrentIndex(tabIndexMsgLog);

	// runnable per position
	//// for each position available in the treeset

	QList<QString> positions = cellsPerTimepointPerPosition.keys();

	isQuantificationRunning = true;
	QApplication::setOverrideCursor( QCursor(Qt::WaitCursor) );

	foreach (QString posIndex, positions) {

		updateMessageLogText(QString("> Starting quantification of position: %1").arg(posIndex), Qt::black);

		
		QList<QSharedPointer<ITree>> treesPerPositions = this->findTreesPerPosition(posIndex, this->treePointers);

		PositionQuantification* worker = new PositionQuantification(posIndex, this->pathToExperiments);

		worker->initializeData( cellsPerTimepointPerPosition.value(posIndex), /*cellsWithMissingTimepoints,*/ treesPerPositions);

		worker->setQuantificationOptions(PositionQuantification::options);

		worker->setCombinations(wlCombos);

		connect(worker, SIGNAL(updateProgressBar()), this, SLOT(updateProgressBar()));
		connect(worker, SIGNAL(updateMsgLog(QString, QColor)), this, SLOT(updateMessageLogText(QString,QColor)));
		connect(worker, SIGNAL(finished()), this, SLOT(deleteJobRunning()));
		connect(this, SIGNAL(terminationRequested()), worker, SLOT(setQuantificationTerminationRequest()), Qt::DirectConnection);

		worker->setAutoDelete(true);

		// TODO: qthreadpool check when all positions are done!!! I need to have a runnable count
		pool->start(worker);
		addJobRunning();

	}
}


QList<QSharedPointer<ITree>>  QuantificationOptions::findTreesPerPosition(QString _posIndex, QVector<QSharedPointer<ITree>> _treesForQuantification)
{
	QList<QSharedPointer<ITree>> result;

	foreach (QSharedPointer<ITree> atree, _treesForQuantification) {
		QString atreename = atree->getTreeName();
		if (atreename.contains(_posIndex)) {
			result.append(atree);
		}
	}

	return result;

}


QVector<int> QuantificationOptions::getTrackingTimepoints(QList<CellLineageTree::FrameIndex> _trackingIndexes)
{
	QVector<int> timepoints;

	for(int i = 0; i < _trackingIndexes.size(); i++){
		QVector<int> frmData = _trackingIndexes.at(i).index;
		timepoints.append(frmData[0]);
	}

	return timepoints;
}

QVector<int> QuantificationOptions::getQuantificationTimepoints(QList<CellLineageTree::FrameIndex> _quantificationIndexes)
{
	QVector<int> timepoints;

	for(int i = 0; i < _quantificationIndexes.size(); i++){
		QVector<int> frmData = _quantificationIndexes.at(i).index;
		timepoints.append(frmData[0]);
	}

	return timepoints;
}

QMap< QString, QMap<int, QVector<ITrack*>>> QuantificationOptions::findCellsPerTimePointPerPosition(QVector<ITrack*> _tracks)
{
	QMap<QString, QMap<int, QVector<ITrack*>>> cellsPerTimepointPerPosition;

	foreach(ITrack* track, _tracks){

		ITree* tree = track->getITree();
		QString pos = tree->getPositionIndex();
		QList<ITrackPoint*> trackpoints = track->getTrackPointsRange(-1, -1).values();
		//first time position found
		if(!cellsPerTimepointPerPosition.contains(pos)){

			QMap<int, QVector<ITrack*>> tracksPerTimepoint;

			foreach(ITrackPoint* tpoint, trackpoints){
				int tmp = tpoint->getTimePoint();
				QVector<ITrack*> tmpTracks;
				tmpTracks.append(track);
				tracksPerTimepoint.insert(tmp, tmpTracks);
				// maxNumForProgressBar++;
			}

			cellsPerTimepointPerPosition.insert(pos, tracksPerTimepoint);
		}
		else{//position already found
			QMap<int, QVector<ITrack*>> tracksPerTimepoint = cellsPerTimepointPerPosition.value(pos);
			foreach(ITrackPoint* tpoint, trackpoints){
				int tmp = tpoint->getTimePoint();
				//first time timepoint found
				QVector<ITrack*> tmpTracks;
				if(!tracksPerTimepoint.contains(tmp)){
					tmpTracks.append(track);
					tracksPerTimepoint.insert(tmp, tmpTracks);
				}
				else{//timepoint already found
					tmpTracks = tracksPerTimepoint.value(tmp);
					tmpTracks.append(track);
					tracksPerTimepoint.remove(tmp);
					tracksPerTimepoint.insert(tmp, tmpTracks);
				}
				//remove entry to update it
				cellsPerTimepointPerPosition.remove(pos);
				cellsPerTimepointPerPosition.insert(pos, tracksPerTimepoint);
				// maxNumForProgressBar++;
			}
		}
	}

	// store here the number of timepoints per position
	// this will be the maximum value of the progress bar
	int maxNumForProgressBar = 0; 
	QMap<QString, int> maxNumberOfTpPerPos;

	foreach (QSharedPointer<ITree> tree, this->treePointers) {
		QString posIndex = tree->getPositionIndex();
		int maxTimepoint = TreeStructuralHelper::getLastTreeTimePoint(tree.data());

		if (maxNumberOfTpPerPos.contains(posIndex)) {
			int previousNum = maxNumberOfTpPerPos.value(posIndex);
			if (previousNum < maxTimepoint)
				maxNumberOfTpPerPos .insert(posIndex, maxTimepoint);
		}
		else{
			maxNumberOfTpPerPos .insert(posIndex, maxTimepoint);
		}
	}

	foreach (QString pos, maxNumberOfTpPerPos.keys()) {
		int update = maxNumberOfTpPerPos.value(pos);
		maxNumForProgressBar = maxNumForProgressBar + update;
	}
	
	// now I need to update the progress bar every time I finish a timepoint in a position

	this->setProgressBarMax(maxNumForProgressBar);
	return cellsPerTimepointPerPosition;
}


bool QuantificationOptions::previousQuantificationExists(){
	
	bool isQuantifiedBefore = false;

	//find the clt trees of this treeset
	foreach(QSharedPointer<ITree> tree, this->treePointers){
		QString treePath = tree->getFilename();
		isQuantifiedBefore = Tools::isTreeQuantifiedBefore(treePath);
		if (isQuantifiedBefore)
			return true;
	}
	
	return isQuantifiedBefore;
}

QMap<int, QList<int>> QuantificationOptions::getQuantificationCombinations()
{
	QMap<int, QList<int>> wlCombos;

	for (int row =0; row < ui.tbtQuantificationCombinations->rowCount(); row++) {

		QTableWidgetItem* quantiWLitem = ui.tbtQuantificationCombinations->item(row, 0);
		QStringList quantiWLs = quantiWLitem->text().replace( " ", "" ).split(",");
		QList<int> quantiWLs2;

		bool conversionStateQ = true;
		bool isValidWLQ = true;
		for (int index = 0; index < quantiWLs.size(); index++) {
			// try to convert the wavelength to int value
			int currwl = quantiWLs.at(index).toInt(&conversionStateQ);			

			if (!this->availableWavelengths.contains(currwl)) {
				isValidWLQ = false;
				break;
			}

			if(!conversionStateQ) break;

			quantiWLs2.append(currwl);
			QuantificationOptions::wavelengthsToQuantify.append(currwl);
		}

		// if the table contains text that cannot be converted to integer
		if(!conversionStateQ) {
			// highlight problematic cell in table row 
			ui.tbtQuantificationCombinations->item(row, 0)->setTextColor(Qt::red);
			QString message = "The list of channels to quantify contains wrong entries.\nPlease select channel numbers separated by comma.";
			QMessageBox::warning (this, "Invalid input", message, QMessageBox::Ok);
			updateMessageLogText("> Error: The list of channels to quantify contains wrong entries. Please select channel numbers separated by comma...", Qt::red);
			ui.tbtQuantificationCombinations->clearSelection();
			return QMap<int, QList<int>>();
		}

		// the wavelength number is not available for the experiment
		if (!isValidWLQ) {
			// highlight problematic cell in table row 
			ui.tbtQuantificationCombinations->item(row, 0)->setTextColor(Qt::red);
			QString message = "The list of channels to quantify contains wrong entries.\nPlease make sure that all channel numbers are available for this experiment.";
			QMessageBox::warning (this, "Invalid input", message, QMessageBox::Ok);
			updateMessageLogText("> Error: The list of channels to quantify contains wrong entries. Please make sure that all channel numbers are available for this experiment...", Qt::red);
			ui.tbtQuantificationCombinations->clearSelection();
			return QMap<int, QList<int>>();
		}

		bool conversionStateD = true;
		bool isValidWLD = true;
		QTableWidgetItem* detectionWLitem = ui.tbtQuantificationCombinations->item(row, 1);
		int detectWL = detectionWLitem->text().trimmed().toInt(&conversionStateD);

		if (!this->availableWavelengths.contains(detectWL)) {
			isValidWLD = false;
			// highlight problematic cell in table row 
			ui.tbtQuantificationCombinations->item(row, 1)->setTextColor(Qt::red);
			QString message = "The number of channel of detection is not valid.\nPlease make sure that the channel number is available for this experiment.";
			QMessageBox::warning (this, "Invalid input", message, QMessageBox::Ok);
			updateMessageLogText("> Error: The number of channel of detection is not valid. Please make sure that the channel number is available for this experiment...", Qt::red);
			ui.tbtQuantificationCombinations->clearSelection();
			return QMap<int, QList<int>>();
		}

		// check that the wavelength of detection was a valid integer otherwise stop
		if(!conversionStateD) {
			ui.tbtQuantificationCombinations->item(row, 1)->setTextColor(Qt::red);
			QString message = "The number of channel of detection is not valid.\nPlease give a single number for the detection channel.";
			QMessageBox::warning (this, "Invalid input", message, QMessageBox::Ok);
			updateMessageLogText("> Error: The number of channel of detection is not valid. Please give a single number for the detection channel...", Qt::red);
			ui.tbtQuantificationCombinations->clearSelection();
			return QMap<int, QList<int>>();

		}

		wlCombos.insert(detectWL, quantiWLs2);
		QuantificationOptions::wavelengthsForDetection.append(detectWL);
	}
	

	return wlCombos;
}

QVector<QVector<int>> QuantificationOptions::getSelectedWavelengthsToQuantify()
{
	QVector<QVector<int>> result;

	QTableWidget* table = ui.tbtQuantificationCombinations;

	int rowNum = table->rowCount();

	for(int i = 0; i < rowNum; i++){

		QStringList wlgths = ui.tbtQuantificationCombinations->item(i, 0)->text().split(",");
		QVector<int> wavelengths;
		foreach(QString wl, wlgths){
			wavelengths.append(wl.toInt());
		}

		result.append(wavelengths);
	}

	return result;
}

void QuantificationOptions::setExperimentsFilesFolder (QString path)
{
	// check if file exists
	if (QDir(path).exists())
		this->pathToExperiments = path;
	// cannot find the folder -> prompt for correction
	else {
		QString s = QFileDialog::getExistingDirectory(this, tr("Choose the root folder of experiments with the original image files"), "./", QFileDialog::ShowDirsOnly);
		this->pathToExperiments = s;
	}
}

// SLOT
void QuantificationOptions::updateMessageLogText (QString _msg, QColor _color)
{
	ui.txeMessageLog->setTextColor(_color);
	ui.txeMessageLog->append(_msg);
	ui.txeMessageLog->moveCursor (QTextCursor::End);
}

// SLOT
void QuantificationOptions::updateProgressBar ()
{
	int newval = ui.pgbProgressLoading->value() + 1;
	ui.pgbProgressLoading->setValue(newval);
}

// SLOT
void QuantificationOptions::setProgressBarMax (int _value)
{
	ui.pgbProgressLoading->setMaximum(_value);
}

// SLOT
void QuantificationOptions::manualThresholdChecked(bool _isAutoThresholdChecked)
{
	if (_isAutoThresholdChecked)
		ui.lneIntensityThreshold->setEnabled(false);
	else
		ui.lneIntensityThreshold->setEnabled(true);
}

// SLOT
void QuantificationOptions::useExistingSeg(bool _isUseDefaultExistingChecked)
{
	if (_isUseDefaultExistingChecked) {
		ui.lneSegReplaceWL->setEnabled(false);
		ui.lneSegReplaceMethod->setEnabled(false);
	}
	else {
		ui.lneSegReplaceWL->setEnabled(true);
		ui.lneSegReplaceMethod->setEnabled(true);
	}
}

// SLOT
void QuantificationOptions::activateExistingSeg(bool _toActivate)
{
	if (_toActivate) {
		ui.chkUseDefaultExistingSeg->setEnabled(true);
		if (!ui.chkUseDefaultExistingSeg->isChecked()) {
			ui.lneSegReplaceWL->setEnabled(true);
			ui.lneSegReplaceMethod->setEnabled(true);
		}
		else {
			ui.lneSegReplaceWL->setEnabled(false);
			ui.lneSegReplaceMethod->setEnabled(false);
		}
	}
	else {
		ui.chkUseDefaultExistingSeg->setEnabled(false);
		ui.lneSegReplaceWL->setEnabled(false);
		ui.lneSegReplaceMethod->setEnabled(false);
		
	}

	/*if (_toDeactivate) {
	ui.chkUseDefaultExistingSeg->setEnabled(false);
	ui.lneSegReplaceWL->setEnabled(false);
	ui.lneSegReplaceMethod->setEnabled(false);
	}
	else {
	ui.chkUseDefaultExistingSeg->setEnabled(true);
	if (ui.chkUseDefaultExistingSeg->isChecked()) {
	ui.lneSegReplaceWL->setEnabled(true);
	ui.lneSegReplaceMethod->setEnabled(true);
	}
	else {
	ui.lneSegReplaceWL->setEnabled(false);
	ui.lneSegReplaceMethod->setEnabled(false);
	}

	}*/
}


// SLOT
void QuantificationOptions::showExampleWinSize()
{
	// pick the options for the segmentation optimization from the interface
	setupOptionsForQuantification();

	double roiSize = PositionQuantification::options.value("windowSize").toDouble();

	if (roiSize < 5) {
		QString msg= "Window size less by 5x5 pixels is not supported. Please choose a bigger window size.";
			QMessageBox::warning (this,  "Invalid window size!", msg, QMessageBox::Ok);
		return;
	}

	QPushButton* theSender = (QPushButton*) sender();

	// pick a random tree
	int minTree = 0;
	int maxTree = this->treePointers.size()-1;

	ITrack* randomTrack = NULL;
	ITrackPoint* tp = NULL;
	int randomInt = 0;
	QString posID = "";
	int count = 0;

	while (!tp && count <= 100) {
		int randomInt = minTree + (rand() % (int)(maxTree - minTree + 1));
		// get the random tree
		QSharedPointer<ITree> randomTree = this->treePointers.at(randomInt);

		posID = randomTree->getPositionIndex();

		// get random cell
		int minTrack = 0;
		int maxTrack = randomTree->getTracksInTree().size() - 1;
		randomInt = minTrack + (rand() % (int)(maxTrack - minTrack + 1));

		randomTrack = randomTree->getTracksInTree().at(randomInt);

		// get random timepoint
		int minTP = randomTrack->getFirstTimePoint() + 1;
		int maxTP = randomTrack->getLastTimePoint() - 1;

		if (maxTP<minTP)
			randomInt = maxTP;
		else
			randomInt = minTP + (rand() % (int)(maxTP - minTP + 1));

		// get the trackpoint for this timepoint
		tp = randomTrack->getTrackPointByTimePoint(randomInt);
		count ++; 
	}

	if (count > 100) {
		QString msg = "Something went wrong. No valid trackpoints identified after 100 tries. Quitting...";
		QMessageBox::warning (this,  "No valid trackpoing found!", msg, QMessageBox::Ok);
		return;
	}
	 
	// get first timepoint
	// int timepointNum = randomTrack->getFirstTimePoint() + 1;
	
	if (randomTrack == NULL)
		return;

	QString expName = randomTrack->getITree()->getExperimentName();

	QPoint locationOnScreenOfSender = theSender->mapToGlobal(theSender->rect().bottomRight());

	bool doSegmentation = false;
	if (theSender == ui.pbtCheckSegmentation)
		doSegmentation = true;

	displayCell(expName, posID, randomInt, randomTrack, PositionQuantification::options, locationOnScreenOfSender.x(), locationOnScreenOfSender.y(), doSegmentation);
}


void QuantificationOptions::displayCell(QString _expName, QString _posID, int _timepointNum, ITrack* _cell, QMap<QString, QString> _options, int _x, int _y, bool _doSegmentation)
{
	QString pathToImage = TTTManager::getInst().getExperimentPath() + "\\" + _posID + "\\";

	// get the trackpoint for this timepoint
	ITrackPoint* tp = _cell->getTrackPointByTimePoint(_timepointNum);
	
	while (!tp && _timepointNum < _cell->getLastTimePoint()) {		
		tp = _cell->getTrackPointByTimePoint(_timepointNum);
		_timepointNum++;
	}

	if (!tp)
		return;

	// get the X,Y coordinates from the track data
	int Xcoord = tp->getX();
	int Ycoord = tp->getY();

	QPoint absCoordPoint(Xcoord, Ycoord);

	TTTPositionManager* tttpm = TTTManager::getInst().getPositionManager(_posID);

	QPointF markPoint = QTFyImageOperations::calcTransformedCoords (absCoordPoint, &tttpm->positionInformation, TTTManager::getInst().coordinateSystemIsInverted()); 

	double roiSize = PositionQuantification::options.value("windowSize").toDouble();

	if (roiSize < 5) {
		QString msg= "Window size less by 5x5 pixels is not supported. Please choose a bigger window.";
		QMessageBox::warning (this,  "Invalid window size!", msg, QMessageBox::Ok);
		return;
	}

	int zindex = 1;

	Mat imgOut, roiOut;

	if (!_doSegmentation) {

		int wlP10ID = PositionQuantification::options.value("checkWinSizeWL").toInt();
		QString imageFileName10P = QTFyImageOperations::getImageFileName(_posID, _timepointNum, zindex, wlP10ID, 3, 2, "png");
		QString imagePath10P = pathToImage + imageFileName10P;

		// check if the image files exist
		QFile fileImage10P(imagePath10P);

		if (!fileImage10P.exists()) 
			return; // TODO:: Put error message here

		// read the images
		imgOut = imread(imagePath10P.toStdString(), CV_LOAD_IMAGE_ANYDEPTH | CV_LOAD_IMAGE_GRAYSCALE);
		roiOut = QTFyImageOperations::getImageROI(imgOut, Point(markPoint.x(), markPoint.y()), roiSize);
	}
	else {
		int detectWL = PositionQuantification::options.value("checkSegDetectionWL").toInt();
		QString imageFileName10P = QTFyImageOperations::getImageFileName(_posID, _timepointNum, zindex, detectWL, 3, 2, "png");
		QString imagePath10P = pathToImage + imageFileName10P;

		// check if the image files exist
		QFile fileImage10P(imagePath10P);

		if (!fileImage10P.exists()) 
			return; // TODO:: Put error message here

		// read the images
		Mat rawImg = imread(imagePath10P.toStdString(), CV_LOAD_IMAGE_ANYDEPTH | CV_LOAD_IMAGE_GRAYSCALE);
		Mat detectionROI = QTFyImageOperations::getImageROI(rawImg, Point(markPoint.x(), markPoint.y()), roiSize);

		Mat detectionROI8bit;
		cv::normalize(detectionROI, detectionROI8bit, 0, 255, NORM_MINMAX, CV_8UC1);

		roiOut = detectionROI8bit;

		Mat thresholdROI = QTFyImageOperations::thresholdROI(detectionROI8bit, PositionQuantification::options);
		std::vector< std::vector<Point> > contoursAll;

		if (PositionQuantification::options.value("splitMergedCells") == "false") {
			// find contours in ROI image
			contoursAll = QTFyImageOperations::getImageContours(thresholdROI, PositionQuantification::options.value("minCellSize").toInt(), PositionQuantification::options.value("maxCellSize").toInt(), true, true);			
		}
		else {
			// if watershed has been applied the thresholdROI image is a labeled object image 
			QList<int> visitedLabels;

			// Fill labeled objects with random colors
			for (int i = 0; i < thresholdROI.rows; i++)
			{
				for (int j = 0; j < thresholdROI.cols; j++)
				{
					int index = thresholdROI.at<int>(i,j); // the label
					if (!visitedLabels.contains(index) && index >= 0) {
						visitedLabels.append(index);
						cv::Mat regionIndex = thresholdROI == index; // get a mask of one labeled component only
						// find contours in ROI image
						// should be only one
						std::vector< std::vector<Point> > contoursIndex = QTFyImageOperations::getImageContours(regionIndex, PositionQuantification::options.value("minCellSize").toInt(), PositionQuantification::options.value("maxCellSize").toInt(), true, true);
						contoursAll.insert(contoursAll.end(), contoursIndex.begin(), contoursIndex.end());
					}
				}
			}
		}

		// if at least one contour has been found
		if (!contoursAll.empty()) {

			QTFyImageOperations::plotContours(detectionROI8bit, contoursAll, false, Scalar(0, 0, 0));
			// find contour containing or within distance from trackpoint
			// TODO:: make the distance here user selectable?
			// distance depends on the size of the window, winSize/2
			double maxDistance = PositionQuantification::options.value("maxDistanceFromTrackpoint").toInt(); // PositionQuantification::options.value("windowSize").toDouble()/3;
			// need to adjust the trackpoint to the ROI image

			Point pntAdj (roiSize/2, roiSize/2);
			std::vector<Point> theContour = QTFyImageOperations::getClosestContour(contoursAll, pntAdj, maxDistance);
			contoursAll.clear();
			contoursAll.push_back(theContour);

			// if at least one contour has been found
			if (!theContour.empty()) {
				QTFyImageOperations::plotContours(detectionROI8bit, contoursAll, false, Scalar(255, 255, 255));

			}
			
			roiOut = detectionROI8bit;
		}
	}

	if (roiOut.empty())
		int laura = 0;

	testDisplay->updateImage(QTFyImageOperations::MatToQImage(roiOut));
	testDisplay->show();
	
}


void QuantificationOptions::setupOptionsForQuantification()
{

	//chkUseExistingSegmentation
	if (ui.chkUseExistingSegmentation->isChecked())
		PositionQuantification::options.insert("useExistingSegmentation", "true");
	else
		PositionQuantification::options.insert("useExistingSegmentation", "false");


	// chkUseDefaultExistingSeg
	if (ui.chkUseDefaultExistingSeg->isChecked())
		PositionQuantification::options.insert("useDefaultExistingSegmentationOptions", "true");
	else
		PositionQuantification::options.insert("useDefaultExistingSegmentationOptions", "false");
	
	// lneSegReplaceWL
	PositionQuantification::options.insert("replaceDetectionWL", ui.lneSegReplaceWL->text());

	// lneSegReplaceMethod
	PositionQuantification::options.insert("replaceDetectionMethod", ui.lneSegReplaceMethod->text());


	// lneWinSize
	PositionQuantification::options.insert("windowSize", ui.lneWinSize->text());
		
	//lneCheckWinWL
	PositionQuantification::options.insert("checkWinSizeWL", ui.lneCheckWinWL->text());
	
	// lneMinCellSize
	PositionQuantification::options.insert("minCellSize", ui.lneMinCellSize->text());

	//lneMaxCellSize
	PositionQuantification::options.insert("maxCellSize", ui.lneMaxCellSize->text());

	// lneMaxDistanceFromTrackpoint
	PositionQuantification::options.insert("maxDistanceFromTrackpoint", ui.lneMaxDistanceFromTrackpoint->text());
	
	//chkUseSmoothing
	if (ui.chkUseSmoothing->isChecked())
		PositionQuantification::options.insert("useSmoothing", "true");
	else
		PositionQuantification::options.insert("useSmoothing", "false");


	//lneMaxCellSize
	PositionQuantification::options.insert("checkSegDetectionWL", ui.lneCheckSegWL->text());
	
	//lneGBkernelSize
	PositionQuantification::options.insert("gaussianBlurKernelSize", ui.lneGBkernelSize->text());
	//lneGBsigma
	// PositionQuantification::options.insert("gaussianBlurSigma", ui.lneGBsigma->text());

	//chkUseAutomaticThreshold
	if (ui.chkUseAutomaticThreshold->isChecked())
		PositionQuantification::options.insert("useAutomaticThreshold", "true");
	else
		PositionQuantification::options.insert("useAutomaticThreshold", "false");

	//lneIntensityThreshold
	PositionQuantification::options.insert("intensityThreshold", ui.lneIntensityThreshold->text());

	//chkSplitMergedCells
	if (ui.chkSplitMergedCells->isChecked())
		PositionQuantification::options.insert("splitMergedCells", "true");
	else
		PositionQuantification::options.insert("splitMergedCells", "false");
	
	//// clahe options
	PositionQuantification::options.insert("claheWinSize", "8");
	PositionQuantification::options.insert("claheClipLimit", "4");

	//// erosion options
	PositionQuantification::options.insert("erosionSize", "1");
	
}

// SLOT
void QuantificationOptions::addJobRunning()
{
	this->activeThreads++;
	qDebug() << "Number of active threads: " << this->activeThreads;
}

 // SLOT
void QuantificationOptions::deleteJobRunning()
{
	this->activeThreads--;
	if (activeThreads == 0) {  // quantification is done
		this->isQuantificationRunning = false;
		ui.pbtOpenQTFyEditor->setEnabled(true);
		ui.pbtCancel->setEnabled(true);

		ui.pgbProgressLoading->reset();
		this->updateProgressBar();

		QApplication::restoreOverrideCursor();
	}
}


void QuantificationOptions::openQTFyEditor()
{

	//check if the experiment has stagger wavelengths
	bool isStaggering = TATInformation::getInst()->staggerWavelengths();

	QtfyMain* qtfyEditor = new QtfyMain();
	qtfyEditor->hasStaggerWavelengths(isStaggering);

	qtfyEditor->initializeData(this->treePointers);

	qtfyEditor->showMaximized();

	// closes and deletes itself
	this->setAttribute( Qt::WA_DeleteOnClose, true );
	this->close();
}

void QuantificationOptions::exportMessageLog()
{	//get the text written in the message log
	QString text = ui.txeMessageLog->toPlainText();
	QStringList messages = text.split(">");
	//folder selection by user 
	QString folder = QFileDialog::getExistingDirectory (this, "Select directory to save text file");

	//if user canceled return
	if(folder.isEmpty())
		return;
	//if invalid input return
	if ( ! QDir(folder).exists() )
		return;
	//lastSelectedDirectory = folder;
	QString filename= folder + "/" + "Quantification_Message_Log.txt";
	QFile file( filename );

	if (!filename.isEmpty())
	{
		file.setFileName(filename);
		file.open(QIODevice::WriteOnly);
		QTextStream output(&file);
		//output.setDevice(QIODevice::Text);
		output.device()->setTextModeEnabled(true);
		output<<ui.txeMessageLog->toPlainText();
		file.close(); 
		ui.txeMessageLog->append(QString("> Successful exporting of: %1...").arg(filename));
		ui.txeMessageLog->setTextColor((Qt::darkGreen));
	} 
	else{
		ui.txeMessageLog->append(QString(">Write error: Failed to export: %1...").arg(filename));
		ui.txeMessageLog->setTextColor((Qt::red));
	}
}