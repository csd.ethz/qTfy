/**
 Copyright (c) 2016 ETH Zurich, 2015-2016 Stavroula Skylaki, Eleni Skylaki, Oliver Hilsenbeck, Michael Schwarzfischer, Timm Schroeder
  
 This file is part of qTfy.
  
 qTfy is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Stavroula Skylaki
 * @date 06/10/2015
 */


// PROJECT
#include "loadexperiment.h"
#include "qtfyhelp.h"
#include "quantificationoptions.h"
#include "qtfyio/loadingoperations.h"
#include "qtfybackend/tttmanager.h"
#include "qtfybackend/tools.h"
#include "qtfydata/systeminfo.h"


// QT
#include <QFileDialog>
#include <QMessageBox>
#include <QThread>
#include <QCheckBox>
#include <QMenu>
#include <QPixmap>


LoadExperiment::LoadExperiment(QWidget *parent)
{

	ui.setupUi(this);

	createActions();

	this->setAttribute( Qt::WA_DeleteOnClose, true );

	tttFilesPathValid = false;
	experimentPathValid = false;

	ui.txeMessageLog->setReadOnly(true);


	// initialize combinations table headers
	// Column headers
	QStringList columnHeaders;
	// File name column
	columnHeaders  << "Select" << "Tree name" << "QTFy exists" << "Position comment";

	// Set headers
	ui.tbwTreesFound->setColumnCount(columnHeaders.size());
	ui.tbwTreesFound->setHorizontalHeaderLabels(columnHeaders);
	ui.tbwTreesFound->verticalHeader()->setVisible(false);

	// Resize to fit headers
	ui.tbwTreesFound->resizeColumnsToContents();

	// set selection behavior
	ui.tbwTreesFound->setSelectionBehavior(QAbstractItemView::SelectRows);

	// connect the table selection to the check boxes
	connect(ui.tbwTreesFound, SIGNAL(itemSelectionChanged()), this, SLOT (updateTreeCheckboxes()));

	// get the right click context menu for the table
	ui.tbwTreesFound->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(ui.tbwTreesFound, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(tableRtClickMenu(const QPoint&)));
	connect(ui.tbwTreesFound, SIGNAL(itemClicked(QTableWidgetItem *)), ui.tbwTreesFound, SIGNAL(itemSelectionChanged()));

	loadingRunning = false;
	loadingDone = false;
	loadingTreesDone = false;
	quantificationStarted = false;

}



LoadExperiment::~LoadExperiment()
{
	treePointers.clear();
}

void LoadExperiment::createActions()
{

	// user starts a new quantification
	connect(ui.pbtSetTTTFilesFolder, SIGNAL(clicked()), this, SLOT (setTTTFilesFolder()));

	// user will open existing trees which have been already quantified
	// csv files for the trees should be present in the tree-position folder
	connect(ui.pbtSetExperimentsFileFolder, SIGNAL(clicked()), this, SLOT (setExperimentFolder()));

	// get the help for choosing the right folder paths
	connect(ui.pbtHelpFolderPaths, SIGNAL(clicked()), this, SLOT(showHelpFolderPaths()));

	// start the data loading
	connect(ui.pbtLoadExperiment, SIGNAL(clicked()), this, SLOT (startLoadingExperiment()));

	// start the data loading
	connect(ui.pbtLoadSelectedTrees, SIGNAL(clicked()), this, SLOT (startLoadingSelectedTrees()));

	// opens the quantification options form
	connect(ui.pbtQuantifySelection, SIGNAL(clicked()), this, SLOT (quantifySelection()));

	// just closes the form
	connect(ui.pbtCancel, SIGNAL(clicked()), this, SLOT (cancel()));

}

// SLOT
void LoadExperiment::setTTTFilesFolder()
{

	QApplication::setOverrideCursor( QCursor(Qt::WaitCursor) );

	QString existingUserInput = ui.lneTTTFilesFolder->text();

	QString tmp = QFileDialog::getExistingDirectory (this, "Select the TTTWorkFolder path", 
		existingUserInput, QFileDialog::ShowDirsOnly );

	if(!tmp.isEmpty()) {
		tttWorkFolder = tmp;
		ui.lneTTTFilesFolder->setText(tttWorkFolder);
	}

	QApplication::restoreOverrideCursor();
}


// SLOT
void LoadExperiment::setExperimentFolder()
{
	QString existingUserInput = ui.lneExperimentsFilesFolder->text();

	QString tmp = QFileDialog::getExistingDirectory (this, "Select the directory which contains the experiment image files", 
		existingUserInput, QFileDialog::ShowDirsOnly );

	if(!tmp.isEmpty()) {
		experimentsFilesFolder = tmp;
		ui.lneExperimentsFilesFolder->setText(experimentsFilesFolder);
	}
}


// SLOT
void LoadExperiment::cancel()
{
	// if a thread is running need to cancel the thread
	if (loadingRunning) {
		updateMessageLogText("> Warning: Loading has been stopped.", Qt::darkBlue);
		emit terminationRequested();
	
	}
	// go back to intro screen
/*	else {
		IntroScreen* intro = new IntroScreen();
		intro->show();
		this->close();
	}*/	

	//close window
	this->close();
}

// SLOT
void LoadExperiment::startLoadingExperiment()
{
	loadingDone = false;

	tttWorkFolder = ui.lneTTTFilesFolder->text().trimmed();
	experimentsFilesFolder = ui.lneExperimentsFilesFolder->text().trimmed();

	QString tttFilesFolder = tttWorkFolder + "\\TTTfiles"; 

	// if no experiment has been loaded yet
	if (!loadingDone) {

		QApplication::setOverrideCursor( QCursor(Qt::WaitCursor) );

		QThread     *workerThread;
		LoadingOperations *worker;

		if (loadingRunning) {
			updateMessageLogText("> Warning: loading is already running. Please stop loading before starting a new one.", Qt::darkBlue);
			return;
		}

		workerThread = new QThread();
		worker       = new LoadingOperations();

		worker->setExperimentsFilesFolder(experimentsFilesFolder);
		worker->setLogFilesFolder(logFilesFolder);
		worker->setTTTFilesFolder(tttFilesFolder);

		worker->moveToThread(workerThread);
		connect(workerThread, SIGNAL(started()), worker, SLOT(startLoading()));
		connect(worker, SIGNAL(finished()), workerThread, SLOT(quit()));
		connect(worker, SIGNAL(finished()), worker, SLOT(deleteLater()));
		connect(worker, SIGNAL(finished()), this, SLOT(loadingFinished()));
		connect(workerThread, SIGNAL(finished()), workerThread, SLOT(deleteLater()));

		connect(worker, SIGNAL(updateMsgLog(QString, QColor)), this, SLOT(updateMessageLogText(QString, QColor)));
		connect(worker, SIGNAL(updateProgressBar(int)), this, SLOT(updateProgressBar(int)));
		connect(worker, SIGNAL(setProgressBarMax(int)), this, SLOT(setProgressBarMax(int)));
		connect(worker, SIGNAL(treesFound(QStringList)), this, SLOT(updateTreeTable(QStringList)));
		connect(this, SIGNAL(terminationRequested()), worker, SLOT(setTerminationRequest()), Qt::DirectConnection);

		workerThread->start();
		ui.pbtCancel->setText("Stop loading...");

		loadingRunning = true;
	}
	
}

// SLOT
void LoadExperiment::startLoadingSelectedTrees()
{

	tttWorkFolder = ui.lneTTTFilesFolder->text().trimmed();
	experimentsFilesFolder = ui.lneExperimentsFilesFolder->text().trimmed();

	QString tttFilesFolder = tttWorkFolder + "\\TTTfiles";

	// if an experiment has been loaded already
	if (loadingDone) {

		QApplication::setOverrideCursor( QCursor(Qt::WaitCursor) );

		// empty any old trees 
		this->selectedTrees.clear();

		// get selected trees 		
		for(int i = 0; i < ui.tbwTreesFound->rowCount(); i++){

			QCheckBox* chkbx = dynamic_cast<QCheckBox*>(ui.tbwTreesFound->cellWidget(i, 0));
			// get the current state
			bool checkState = chkbx->isChecked();

			if (checkState) {
				QTableWidgetItem* currentTree = ui.tbwTreesFound->item(i, 1);
				QString treeName = currentTree->toolTip();
				this->selectedTrees.append(treeName);
			}
		}

		if (this->selectedTrees.isEmpty()) {
			this->updateMessageLogText("> No trees have been selected! Please select a set of trees for quantification...", Qt::red);
			QApplication::restoreOverrideCursor();
			return;
		} 
		else {
			this->updateMessageLogText(QString("> Number of selected trees for loading: %1").arg(this->selectedTrees.size()), Qt::black);
		}

		this->updateMessageLogText(QString("> Starting tree loading..."), Qt::black);

		// start the loading thread
		QThread     *workerThread;
		TTTLoadingOperations *tttworker;

		if (loadingRunning) {
			updateMessageLogText("> Warning: loading is already running. Please stop loading before starting a new one.", Qt::darkBlue);
			return;
		}

		workerThread = new QThread();
		tttworker       = new TTTLoadingOperations();

		tttworker->setExperimentsFilesFolder(experimentsFilesFolder);
		tttworker->setLogFilesFolder(logFilesFolder);
		tttworker->setTTTFilesFolder(tttFilesFolder);
		tttworker->setlistTTTtrees(this->selectedTrees);

		tttworker->moveToThread(workerThread);
		connect(workerThread, SIGNAL(started()), tttworker, SLOT(startTTTLoading()));
		connect(tttworker, SIGNAL(finished()), workerThread, SLOT(quit()));
		connect(tttworker, SIGNAL(finished()), tttworker, SLOT(deleteLater()));
		connect(tttworker, SIGNAL(finished()), this, SLOT(TTTloadingFinished()));
		connect(workerThread, SIGNAL(finished()), workerThread, SLOT(deleteLater()));

		connect(tttworker, SIGNAL(updateMsgLog(QString, QColor)), this, SLOT(updateMessageLogText(QString, QColor)));
		connect(tttworker, SIGNAL(updateProgressBar(int)), this, SLOT(updateProgressBar(int)));
		connect(tttworker, SIGNAL(setProgressBarMax(int)), this, SLOT(setProgressBarMax(int)));
		connect(tttworker, SIGNAL(treesLoadedSuccess(TreePointers)), this, SLOT(getLoadedTrees(TreePointers)));
		connect(this, SIGNAL(terminationRequested()), tttworker, SLOT(setTerminationRequest()), Qt::DirectConnection);

		workerThread->start();
		ui.pbtCancel->setText("Stop loading...");

		loadingRunning = true;		
	}

}



// SLOT
void LoadExperiment::quantifySelection()
{

	if (!loadingRunning) {
		QuantificationOptions* qtfyOptions = new QuantificationOptions(this);
		qtfyOptions->createTreeList(treePointers);
		qtfyOptions->show();
		this->hide();
		ui.pgbProgressLoading->setValue(0);
	}
	else {
		updateMessageLogText("> Warning: Loading trees is still running. Please wait to finish loading before you start the quantification...", Qt::darkBlue);
	}

	QApplication::restoreOverrideCursor();
	// this->close();
}

bool LoadExperiment::isPathValid(const QString _filepath) {

	QDir nas (_filepath.trimmed());

	if ( nas.exists()) {
		return true;
	}

	return false;
}

// SLOT
void LoadExperiment::updateTreeTable (QStringList _treesFound)
{

	ui.tbwTreesFound->clearContents();
	ui.tbwTreesFound->setRowCount(0);

	int rowCounter = ui.tbwTreesFound->rowCount();

	if (rowCounter == 0)
		ui.tbwTreesFound->setRowCount(_treesFound.size());
	/*else 
		ui.tbwTreesFound->setRowCount(rowCounter + tbwTreesFound.size());*/

	foreach(QString treePath, _treesFound) {

		// first column the check box
		QCheckBox *newChk = new QCheckBox();
		ui.tbwTreesFound->setCellWidget(rowCounter, 0, newChk);
		newChk->setProperty("row", rowCounter);

		// second column the tree name
		// pick the name from the path
		// but store the path as a tooltip
		QFileInfo fileInfo(treePath);
		QString treeName(fileInfo.fileName());
		QTableWidgetItem *treenameItem = new QTableWidgetItem(treeName);
		treenameItem->setText(treeName);
		treenameItem->setToolTip(treePath);
		treenameItem->setFlags(treenameItem->flags() ^ Qt::ItemIsEditable); // not editable
		ui.tbwTreesFound->setItem(rowCounter, 1, treenameItem);
		
		// TODO: GET IF IT IS QUANTIFIED BEFORE

		bool isQuantifiedBefore = Tools::isTreeQuantifiedBefore(treePath);

		
		// has it been quantified before?
		if (isQuantifiedBefore) {	
			QIcon status(":/qtfyres/qtfyres/qtfylogoIcon.png");	
			QWidget *pWidget = new QWidget();
			QLabel *label = new QLabel;
			label->setMaximumSize(status.actualSize(QSize(16, 16)));
			label->setScaledContents(true);
			label->setPixmap(status.pixmap(status.actualSize(QSize(16, 16))));
			QHBoxLayout *pLayout = new QHBoxLayout(pWidget);
			pLayout->addWidget(label);
			pLayout->setAlignment(Qt::AlignCenter);
			pLayout->setContentsMargins(0,0,0,0);
			pWidget->setLayout(pLayout);	
			ui.tbwTreesFound->setCellWidget(rowCounter, 2, pWidget);
		}
		else {
			QTableWidgetItem *quantItem = new QTableWidgetItem(" ");
			quantItem->setFlags(quantItem->flags() ^ Qt::ItemIsEditable); 
			ui.tbwTreesFound->setItem(rowCounter, 2, quantItem);
		}
		
		

		// finaly add the comment of the position the tree belongs to
		// i.e. a culture condition used for the cells etc

		QString positionIndex = Tools::getPositionIndex (treePath);
		QString positionComment = TTTManager::getInst().getPositionManager(positionIndex)->positionInformation.getComment();

		QTableWidgetItem *posCommItem = new QTableWidgetItem(positionComment);
		posCommItem->setFlags(posCommItem->flags() ^ Qt::ItemIsEditable); // not editable
		// posCommItem->setToolTip("The position comment from the TAT xml file.");
		ui.tbwTreesFound->setItem(rowCounter, 3, posCommItem);

		rowCounter++;
	}

	// Resize to fit headers
	ui.tbwTreesFound->resizeColumnsToContents();

	QHeaderView* header = ui.tbwTreesFound->horizontalHeader();
	header->setStretchLastSection(true);
	ui.tbwTreesFound->setHorizontalHeader(header);

	// change the button text
	ui.pbtLoadSelectedTrees->setEnabled(true);
	QApplication::restoreOverrideCursor();

	loadingDone = true;

}

// SLOT
void LoadExperiment::getLoadedTrees(TreePointers _treePointers)
{
	// at least one tree has been loaded
	if (_treePointers.size() > 0) {
		loadingTreesDone = true;
		updateMessageLogText(QString("> Number of trees loaded successfully: %1...").arg(_treePointers.size()), Qt::darkGreen);
		ui.pbtQuantifySelection->setEnabled(true);
		ui.pbtQuantifySelection->setFocus();
		treePointers = _treePointers;
		QApplication::restoreOverrideCursor();

	} 
	else {
		updateMessageLogText(QString("> Warning: no trees have been loaded..."), Qt::darkBlue);
	}

	// wait for the user to read the output
	/*QTime dieTime= QTime::currentTime().addSecs(10);
	while (QTime::currentTime() < dieTime)
	QCoreApplication::processEvents(QEventLoop::AllEvents, 100);*/

	
}

// SLOT
void LoadExperiment::updateMessageLogText (QString _msg, QColor _color)
{
	ui.txeMessageLog->setTextColor(_color);
	ui.txeMessageLog->append(_msg);
	ui.txeMessageLog->moveCursor (QTextCursor::End);
}

// SLOT
void LoadExperiment::updateProgressBar (int _value)
{
	ui.pgbProgressLoading->setValue(_value);
}

// SLOT
void LoadExperiment::loadingFinished ()
{
	this->loadingRunning = false;
	ui.pbtCancel->setText("Cancel");
	QApplication::restoreOverrideCursor();
}

// SLOT
void LoadExperiment::TTTloadingFinished ()
{
	this->loadingRunning = false;
	ui.pbtCancel->setText("Cancel");
	QApplication::restoreOverrideCursor();

	// this->close() and quantification options form
}

// SLOT
void LoadExperiment::setProgressBarMax (int _value)
{
	ui.pgbProgressLoading->setMaximum(_value);
}

// SLOT
void LoadExperiment::tableRtClickMenu (const QPoint& _pnt)
{
	QPoint globalPos = ui.tbwTreesFound->viewport()->mapToGlobal(_pnt);

	QMenu myMenu;
	QAction *actUncheckSelected = new QAction(QIcon(":/qtfyres/qtfyres/checkbox-0.png"), "Uncheck selected", this);
	myMenu.addAction(actUncheckSelected);
	connect(actUncheckSelected, SIGNAL(triggered()),this, SLOT(uncheckSelectionTable()) );
	myMenu.exec(globalPos);

}

// SLOT
void LoadExperiment::uncheckSelectionTable ()
{
	int rowNum = ui.tbwTreesFound->rowCount();
	QList<QTableWidgetItem*> selectedWidgets = ui.tbwTreesFound->selectedItems();

	for(int i = 0; i < rowNum; i++){
		QTableWidgetItem* currentTree = ui.tbwTreesFound->item(i, 1);

		// is the item selected 
		if (selectedWidgets.contains(currentTree)) {
			QCheckBox* chkbx = dynamic_cast<QCheckBox*>(ui.tbwTreesFound->cellWidget(i, 0));
			// get the current state
			chkbx->setCheckState(Qt::Unchecked);
		}		
	}

}

// SLOT
void LoadExperiment::updateTreeCheckboxes()
{
	int rowNum = ui.tbwTreesFound->rowCount();
	QList<QTableWidgetItem*> selectedWidgets = ui.tbwTreesFound->selectedItems();

	for(int i = 0; i < rowNum; i++){
		QTableWidgetItem* currentTree = ui.tbwTreesFound->item(i, 1);

		// is the item selected 
		if (selectedWidgets.contains(currentTree)) {
			QCheckBox* chkbx = dynamic_cast<QCheckBox*>(ui.tbwTreesFound->cellWidget(i, 0));
			chkbx->setCheckState(Qt::Checked);
		}		
	}
}

// SLOT
void LoadExperiment::showHelpFolderPaths()
{
	// create help text
	QString txt = "<p><h2>QTFy Help: Choose the folder paths for data loading</h2> </p>"  
		"<p>Click \"Browse\" to choose the appropriate folder path for: </p>"
		"<p><b>TTTWorkFolder:</b> the required folder here is named TTTWorkFolder. The TTTWorkFolder contains all the TTT and QTFy input/output folders. " 
		"The TTTFiles folder contains the tracked trees files (.ttt) and the quantification output files (.csv). The folder structure of the TTTFiles folder is the following: <br>"
		" TTTFiles\\[Year]\\[Experiment]\\[Position]. <br> The ttt files can be found in the positions sub-folders. <br>"
		"<img src=\":exclamation.png\"> <b>You must provide the path to the parent TTTWorkFolder only and NOT to any subfolders. </b></p>"
		"<b>Experiment folder:</b> the folder of the experiment you want to analyze. This folder contains the position sub-folders and the acquired images per position and timepoint.</p>"
		"<p>Example: <br> "
		"TTTWorkFolder: <b>C:\\Data\\TTTWorkFolder</b> <br>"
		"Experiment folder: <b>C:\\Data\\TTTimageData\\111115AF6</b></p>"
		;

	QMap<QString, QImage> imgResources;
	QImage img(":/qtfyres/qtfyres/exclamation.png");
	imgResources.insert("exclamation.png", img);

	// show help form
	QString windowTitle = "QTFy help: Analysis Folder Paths";

	QTFyHelp* help = new QTFyHelp(this, windowTitle);
	help->setHTMLHelp(txt, imgResources);
	help->show();
}
